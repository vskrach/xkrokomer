EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L krokomer:STM8Nucleo STM8?
U 1 1 624DB306
P 5850 3900
F 0 "STM8?" H 5775 4823 50  0000 C CNN
F 1 "STM8Nucleo" H 5650 4250 50  0001 C CNN
F 2 "" H 5850 4150 50  0001 C CNN
F 3 "" H 5850 4150 50  0001 C CNN
	1    5850 3900
	1    0    0    -1  
$EndComp
$Sheet
S 4500 3300 500  550 
U 624DC1A1
F0 "sensorshake" 50
F1 "sensorshake.sch" 50
$EndSheet
$EndSCHEMATC
