                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.1.0 #12072 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module stm8s_gpio
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _assert_failed
                                     12 	.globl _GPIO_DeInit
                                     13 	.globl _GPIO_Init
                                     14 	.globl _GPIO_Write
                                     15 	.globl _GPIO_WriteHigh
                                     16 	.globl _GPIO_WriteLow
                                     17 	.globl _GPIO_WriteReverse
                                     18 	.globl _GPIO_ReadOutputData
                                     19 	.globl _GPIO_ReadInputData
                                     20 	.globl _GPIO_ReadInputPin
                                     21 	.globl _GPIO_ExternalPullUpConfig
                                     22 ;--------------------------------------------------------
                                     23 ; ram data
                                     24 ;--------------------------------------------------------
                                     25 	.area DATA
                                     26 ;--------------------------------------------------------
                                     27 ; ram data
                                     28 ;--------------------------------------------------------
                                     29 	.area INITIALIZED
                                     30 ;--------------------------------------------------------
                                     31 ; absolute external ram data
                                     32 ;--------------------------------------------------------
                                     33 	.area DABS (ABS)
                                     34 
                                     35 ; default segment ordering for linker
                                     36 	.area HOME
                                     37 	.area GSINIT
                                     38 	.area GSFINAL
                                     39 	.area CONST
                                     40 	.area INITIALIZER
                                     41 	.area CODE
                                     42 
                                     43 ;--------------------------------------------------------
                                     44 ; global & static initialisations
                                     45 ;--------------------------------------------------------
                                     46 	.area HOME
                                     47 	.area GSINIT
                                     48 	.area GSFINAL
                                     49 	.area GSINIT
                                     50 ;--------------------------------------------------------
                                     51 ; Home
                                     52 ;--------------------------------------------------------
                                     53 	.area HOME
                                     54 	.area HOME
                                     55 ;--------------------------------------------------------
                                     56 ; code
                                     57 ;--------------------------------------------------------
                                     58 	.area CODE
                           000000    59 	Sstm8s_gpio$GPIO_DeInit$0 ==.
                                     60 ;	drivers/src/stm8s_gpio.c: 53: void GPIO_DeInit(GPIO_TypeDef* GPIOx)
                                     61 ;	-----------------------------------------
                                     62 ;	 function GPIO_DeInit
                                     63 ;	-----------------------------------------
      008DA4                         64 _GPIO_DeInit:
                           000000    65 	Sstm8s_gpio$GPIO_DeInit$1 ==.
                           000000    66 	Sstm8s_gpio$GPIO_DeInit$2 ==.
                                     67 ;	drivers/src/stm8s_gpio.c: 55: GPIOx->ODR = GPIO_ODR_RESET_VALUE; /* Reset Output Data Register */
      008DA4 16 03            [ 2]   68 	ldw	y, (0x03, sp)
      008DA6 90 7F            [ 1]   69 	clr	(y)
                           000004    70 	Sstm8s_gpio$GPIO_DeInit$3 ==.
                                     71 ;	drivers/src/stm8s_gpio.c: 56: GPIOx->DDR = GPIO_DDR_RESET_VALUE; /* Reset Data Direction Register */
      008DA8 93               [ 1]   72 	ldw	x, y
      008DA9 5C               [ 1]   73 	incw	x
      008DAA 5C               [ 1]   74 	incw	x
      008DAB 7F               [ 1]   75 	clr	(x)
                           000008    76 	Sstm8s_gpio$GPIO_DeInit$4 ==.
                                     77 ;	drivers/src/stm8s_gpio.c: 57: GPIOx->CR1 = GPIO_CR1_RESET_VALUE; /* Reset Control Register 1 */
      008DAC 93               [ 1]   78 	ldw	x, y
      008DAD 6F 03            [ 1]   79 	clr	(0x0003, x)
                           00000B    80 	Sstm8s_gpio$GPIO_DeInit$5 ==.
                                     81 ;	drivers/src/stm8s_gpio.c: 58: GPIOx->CR2 = GPIO_CR2_RESET_VALUE; /* Reset Control Register 2 */
      008DAF 93               [ 1]   82 	ldw	x, y
      008DB0 6F 04            [ 1]   83 	clr	(0x0004, x)
                           00000E    84 	Sstm8s_gpio$GPIO_DeInit$6 ==.
                                     85 ;	drivers/src/stm8s_gpio.c: 59: }
                           00000E    86 	Sstm8s_gpio$GPIO_DeInit$7 ==.
                           00000E    87 	XG$GPIO_DeInit$0$0 ==.
      008DB2 81               [ 4]   88 	ret
                           00000F    89 	Sstm8s_gpio$GPIO_DeInit$8 ==.
                           00000F    90 	Sstm8s_gpio$GPIO_Init$9 ==.
                                     91 ;	drivers/src/stm8s_gpio.c: 71: void GPIO_Init(GPIO_TypeDef* GPIOx, GPIO_Pin_TypeDef GPIO_Pin, GPIO_Mode_TypeDef GPIO_Mode)
                                     92 ;	-----------------------------------------
                                     93 ;	 function GPIO_Init
                                     94 ;	-----------------------------------------
      008DB3                         95 _GPIO_Init:
                           00000F    96 	Sstm8s_gpio$GPIO_Init$10 ==.
      008DB3 52 05            [ 2]   97 	sub	sp, #5
                           000011    98 	Sstm8s_gpio$GPIO_Init$11 ==.
                           000011    99 	Sstm8s_gpio$GPIO_Init$12 ==.
                                    100 ;	drivers/src/stm8s_gpio.c: 77: assert_param(IS_GPIO_MODE_OK(GPIO_Mode));
      008DB5 0D 0B            [ 1]  101 	tnz	(0x0b, sp)
      008DB7 26 03            [ 1]  102 	jrne	00237$
      008DB9 CC 8E 19         [ 2]  103 	jp	00116$
      008DBC                        104 00237$:
      008DBC 7B 0B            [ 1]  105 	ld	a, (0x0b, sp)
      008DBE A1 40            [ 1]  106 	cp	a, #0x40
      008DC0 26 03            [ 1]  107 	jrne	00239$
      008DC2 CC 8E 19         [ 2]  108 	jp	00116$
      008DC5                        109 00239$:
                           000021   110 	Sstm8s_gpio$GPIO_Init$13 ==.
      008DC5 7B 0B            [ 1]  111 	ld	a, (0x0b, sp)
      008DC7 A1 20            [ 1]  112 	cp	a, #0x20
      008DC9 26 03            [ 1]  113 	jrne	00242$
      008DCB CC 8E 19         [ 2]  114 	jp	00116$
      008DCE                        115 00242$:
                           00002A   116 	Sstm8s_gpio$GPIO_Init$14 ==.
      008DCE 7B 0B            [ 1]  117 	ld	a, (0x0b, sp)
      008DD0 A1 60            [ 1]  118 	cp	a, #0x60
      008DD2 26 03            [ 1]  119 	jrne	00245$
      008DD4 CC 8E 19         [ 2]  120 	jp	00116$
      008DD7                        121 00245$:
                           000033   122 	Sstm8s_gpio$GPIO_Init$15 ==.
      008DD7 7B 0B            [ 1]  123 	ld	a, (0x0b, sp)
      008DD9 A1 A0            [ 1]  124 	cp	a, #0xa0
      008DDB 26 03            [ 1]  125 	jrne	00248$
      008DDD CC 8E 19         [ 2]  126 	jp	00116$
      008DE0                        127 00248$:
                           00003C   128 	Sstm8s_gpio$GPIO_Init$16 ==.
      008DE0 7B 0B            [ 1]  129 	ld	a, (0x0b, sp)
      008DE2 A1 E0            [ 1]  130 	cp	a, #0xe0
      008DE4 27 33            [ 1]  131 	jreq	00116$
                           000042   132 	Sstm8s_gpio$GPIO_Init$17 ==.
      008DE6 7B 0B            [ 1]  133 	ld	a, (0x0b, sp)
      008DE8 A1 80            [ 1]  134 	cp	a, #0x80
      008DEA 27 2D            [ 1]  135 	jreq	00116$
                           000048   136 	Sstm8s_gpio$GPIO_Init$18 ==.
      008DEC 7B 0B            [ 1]  137 	ld	a, (0x0b, sp)
      008DEE A1 C0            [ 1]  138 	cp	a, #0xc0
      008DF0 27 27            [ 1]  139 	jreq	00116$
                           00004E   140 	Sstm8s_gpio$GPIO_Init$19 ==.
      008DF2 7B 0B            [ 1]  141 	ld	a, (0x0b, sp)
      008DF4 A1 B0            [ 1]  142 	cp	a, #0xb0
      008DF6 27 21            [ 1]  143 	jreq	00116$
                           000054   144 	Sstm8s_gpio$GPIO_Init$20 ==.
      008DF8 7B 0B            [ 1]  145 	ld	a, (0x0b, sp)
      008DFA A1 F0            [ 1]  146 	cp	a, #0xf0
      008DFC 27 1B            [ 1]  147 	jreq	00116$
                           00005A   148 	Sstm8s_gpio$GPIO_Init$21 ==.
      008DFE 7B 0B            [ 1]  149 	ld	a, (0x0b, sp)
      008E00 A1 90            [ 1]  150 	cp	a, #0x90
      008E02 27 15            [ 1]  151 	jreq	00116$
                           000060   152 	Sstm8s_gpio$GPIO_Init$22 ==.
      008E04 7B 0B            [ 1]  153 	ld	a, (0x0b, sp)
      008E06 A1 D0            [ 1]  154 	cp	a, #0xd0
      008E08 27 0F            [ 1]  155 	jreq	00116$
                           000066   156 	Sstm8s_gpio$GPIO_Init$23 ==.
      008E0A 4B 4D            [ 1]  157 	push	#0x4d
                           000068   158 	Sstm8s_gpio$GPIO_Init$24 ==.
      008E0C 5F               [ 1]  159 	clrw	x
      008E0D 89               [ 2]  160 	pushw	x
                           00006A   161 	Sstm8s_gpio$GPIO_Init$25 ==.
      008E0E 4B 00            [ 1]  162 	push	#0x00
                           00006C   163 	Sstm8s_gpio$GPIO_Init$26 ==.
      008E10 4B 13            [ 1]  164 	push	#<(___str_0+0)
                           00006E   165 	Sstm8s_gpio$GPIO_Init$27 ==.
      008E12 4B 81            [ 1]  166 	push	#((___str_0+0) >> 8)
                           000070   167 	Sstm8s_gpio$GPIO_Init$28 ==.
      008E14 CD 84 F3         [ 4]  168 	call	_assert_failed
      008E17 5B 06            [ 2]  169 	addw	sp, #6
                           000075   170 	Sstm8s_gpio$GPIO_Init$29 ==.
      008E19                        171 00116$:
                           000075   172 	Sstm8s_gpio$GPIO_Init$30 ==.
                                    173 ;	drivers/src/stm8s_gpio.c: 78: assert_param(IS_GPIO_PIN_OK(GPIO_Pin));
      008E19 0D 0A            [ 1]  174 	tnz	(0x0a, sp)
      008E1B 26 0F            [ 1]  175 	jrne	00151$
      008E1D 4B 4E            [ 1]  176 	push	#0x4e
                           00007B   177 	Sstm8s_gpio$GPIO_Init$31 ==.
      008E1F 5F               [ 1]  178 	clrw	x
      008E20 89               [ 2]  179 	pushw	x
                           00007D   180 	Sstm8s_gpio$GPIO_Init$32 ==.
      008E21 4B 00            [ 1]  181 	push	#0x00
                           00007F   182 	Sstm8s_gpio$GPIO_Init$33 ==.
      008E23 4B 13            [ 1]  183 	push	#<(___str_0+0)
                           000081   184 	Sstm8s_gpio$GPIO_Init$34 ==.
      008E25 4B 81            [ 1]  185 	push	#((___str_0+0) >> 8)
                           000083   186 	Sstm8s_gpio$GPIO_Init$35 ==.
      008E27 CD 84 F3         [ 4]  187 	call	_assert_failed
      008E2A 5B 06            [ 2]  188 	addw	sp, #6
                           000088   189 	Sstm8s_gpio$GPIO_Init$36 ==.
      008E2C                        190 00151$:
                           000088   191 	Sstm8s_gpio$GPIO_Init$37 ==.
                                    192 ;	drivers/src/stm8s_gpio.c: 81: GPIOx->CR2 &= (uint8_t)(~(GPIO_Pin));
      008E2C 16 08            [ 2]  193 	ldw	y, (0x08, sp)
      008E2E 93               [ 1]  194 	ldw	x, y
      008E2F 1C 00 04         [ 2]  195 	addw	x, #0x0004
      008E32 1F 01            [ 2]  196 	ldw	(0x01, sp), x
      008E34 F6               [ 1]  197 	ld	a, (x)
      008E35 88               [ 1]  198 	push	a
                           000092   199 	Sstm8s_gpio$GPIO_Init$38 ==.
      008E36 7B 0B            [ 1]  200 	ld	a, (0x0b, sp)
      008E38 43               [ 1]  201 	cpl	a
      008E39 6B 04            [ 1]  202 	ld	(0x04, sp), a
      008E3B 84               [ 1]  203 	pop	a
                           000098   204 	Sstm8s_gpio$GPIO_Init$39 ==.
      008E3C 14 03            [ 1]  205 	and	a, (0x03, sp)
      008E3E 1E 01            [ 2]  206 	ldw	x, (0x01, sp)
      008E40 F7               [ 1]  207 	ld	(x), a
                           00009D   208 	Sstm8s_gpio$GPIO_Init$40 ==.
                                    209 ;	drivers/src/stm8s_gpio.c: 98: GPIOx->DDR |= (uint8_t)GPIO_Pin;
      008E41 93               [ 1]  210 	ldw	x, y
      008E42 5C               [ 1]  211 	incw	x
      008E43 5C               [ 1]  212 	incw	x
      008E44 1F 04            [ 2]  213 	ldw	(0x04, sp), x
                           0000A2   214 	Sstm8s_gpio$GPIO_Init$41 ==.
                                    215 ;	drivers/src/stm8s_gpio.c: 87: if ((((uint8_t)(GPIO_Mode)) & (uint8_t)0x80) != (uint8_t)0x00) /* Output mode */
      008E46 0D 0B            [ 1]  216 	tnz	(0x0b, sp)
      008E48 2A 1E            [ 1]  217 	jrpl	00105$
                           0000A6   218 	Sstm8s_gpio$GPIO_Init$42 ==.
                                    219 ;	drivers/src/stm8s_gpio.c: 91: GPIOx->ODR |= (uint8_t)GPIO_Pin;
      008E4A 90 F6            [ 1]  220 	ld	a, (y)
                           0000A8   221 	Sstm8s_gpio$GPIO_Init$43 ==.
                           0000A8   222 	Sstm8s_gpio$GPIO_Init$44 ==.
                                    223 ;	drivers/src/stm8s_gpio.c: 89: if ((((uint8_t)(GPIO_Mode)) & (uint8_t)0x10) != (uint8_t)0x00) /* High level */
      008E4C 88               [ 1]  224 	push	a
                           0000A9   225 	Sstm8s_gpio$GPIO_Init$45 ==.
      008E4D 7B 0C            [ 1]  226 	ld	a, (0x0c, sp)
      008E4F A5 10            [ 1]  227 	bcp	a, #0x10
      008E51 84               [ 1]  228 	pop	a
                           0000AE   229 	Sstm8s_gpio$GPIO_Init$46 ==.
      008E52 27 06            [ 1]  230 	jreq	00102$
                           0000B0   231 	Sstm8s_gpio$GPIO_Init$47 ==.
                           0000B0   232 	Sstm8s_gpio$GPIO_Init$48 ==.
                                    233 ;	drivers/src/stm8s_gpio.c: 91: GPIOx->ODR |= (uint8_t)GPIO_Pin;
      008E54 1A 0A            [ 1]  234 	or	a, (0x0a, sp)
      008E56 90 F7            [ 1]  235 	ld	(y), a
                           0000B4   236 	Sstm8s_gpio$GPIO_Init$49 ==.
      008E58 20 04            [ 2]  237 	jra	00103$
      008E5A                        238 00102$:
                           0000B6   239 	Sstm8s_gpio$GPIO_Init$50 ==.
                           0000B6   240 	Sstm8s_gpio$GPIO_Init$51 ==.
                                    241 ;	drivers/src/stm8s_gpio.c: 95: GPIOx->ODR &= (uint8_t)(~(GPIO_Pin));
      008E5A 14 03            [ 1]  242 	and	a, (0x03, sp)
      008E5C 90 F7            [ 1]  243 	ld	(y), a
                           0000BA   244 	Sstm8s_gpio$GPIO_Init$52 ==.
      008E5E                        245 00103$:
                           0000BA   246 	Sstm8s_gpio$GPIO_Init$53 ==.
                                    247 ;	drivers/src/stm8s_gpio.c: 98: GPIOx->DDR |= (uint8_t)GPIO_Pin;
      008E5E 1E 04            [ 2]  248 	ldw	x, (0x04, sp)
      008E60 F6               [ 1]  249 	ld	a, (x)
      008E61 1A 0A            [ 1]  250 	or	a, (0x0a, sp)
      008E63 1E 04            [ 2]  251 	ldw	x, (0x04, sp)
      008E65 F7               [ 1]  252 	ld	(x), a
                           0000C2   253 	Sstm8s_gpio$GPIO_Init$54 ==.
      008E66 20 08            [ 2]  254 	jra	00106$
      008E68                        255 00105$:
                           0000C4   256 	Sstm8s_gpio$GPIO_Init$55 ==.
                           0000C4   257 	Sstm8s_gpio$GPIO_Init$56 ==.
                                    258 ;	drivers/src/stm8s_gpio.c: 103: GPIOx->DDR &= (uint8_t)(~(GPIO_Pin));
      008E68 1E 04            [ 2]  259 	ldw	x, (0x04, sp)
      008E6A F6               [ 1]  260 	ld	a, (x)
      008E6B 14 03            [ 1]  261 	and	a, (0x03, sp)
      008E6D 1E 04            [ 2]  262 	ldw	x, (0x04, sp)
      008E6F F7               [ 1]  263 	ld	(x), a
                           0000CC   264 	Sstm8s_gpio$GPIO_Init$57 ==.
      008E70                        265 00106$:
                           0000CC   266 	Sstm8s_gpio$GPIO_Init$58 ==.
                                    267 ;	drivers/src/stm8s_gpio.c: 112: GPIOx->CR1 |= (uint8_t)GPIO_Pin;
      008E70 93               [ 1]  268 	ldw	x, y
      008E71 1C 00 03         [ 2]  269 	addw	x, #0x0003
      008E74 F6               [ 1]  270 	ld	a, (x)
                           0000D1   271 	Sstm8s_gpio$GPIO_Init$59 ==.
                                    272 ;	drivers/src/stm8s_gpio.c: 110: if ((((uint8_t)(GPIO_Mode)) & (uint8_t)0x40) != (uint8_t)0x00) /* Pull-Up or Push-Pull */
      008E75 88               [ 1]  273 	push	a
                           0000D2   274 	Sstm8s_gpio$GPIO_Init$60 ==.
      008E76 7B 0C            [ 1]  275 	ld	a, (0x0c, sp)
      008E78 A5 40            [ 1]  276 	bcp	a, #0x40
      008E7A 84               [ 1]  277 	pop	a
                           0000D7   278 	Sstm8s_gpio$GPIO_Init$61 ==.
      008E7B 27 05            [ 1]  279 	jreq	00108$
                           0000D9   280 	Sstm8s_gpio$GPIO_Init$62 ==.
                           0000D9   281 	Sstm8s_gpio$GPIO_Init$63 ==.
                                    282 ;	drivers/src/stm8s_gpio.c: 112: GPIOx->CR1 |= (uint8_t)GPIO_Pin;
      008E7D 1A 0A            [ 1]  283 	or	a, (0x0a, sp)
      008E7F F7               [ 1]  284 	ld	(x), a
                           0000DC   285 	Sstm8s_gpio$GPIO_Init$64 ==.
      008E80 20 03            [ 2]  286 	jra	00109$
      008E82                        287 00108$:
                           0000DE   288 	Sstm8s_gpio$GPIO_Init$65 ==.
                           0000DE   289 	Sstm8s_gpio$GPIO_Init$66 ==.
                                    290 ;	drivers/src/stm8s_gpio.c: 116: GPIOx->CR1 &= (uint8_t)(~(GPIO_Pin));
      008E82 14 03            [ 1]  291 	and	a, (0x03, sp)
      008E84 F7               [ 1]  292 	ld	(x), a
                           0000E1   293 	Sstm8s_gpio$GPIO_Init$67 ==.
      008E85                        294 00109$:
                           0000E1   295 	Sstm8s_gpio$GPIO_Init$68 ==.
                                    296 ;	drivers/src/stm8s_gpio.c: 81: GPIOx->CR2 &= (uint8_t)(~(GPIO_Pin));
      008E85 1E 01            [ 2]  297 	ldw	x, (0x01, sp)
      008E87 F6               [ 1]  298 	ld	a, (x)
                           0000E4   299 	Sstm8s_gpio$GPIO_Init$69 ==.
                                    300 ;	drivers/src/stm8s_gpio.c: 123: if ((((uint8_t)(GPIO_Mode)) & (uint8_t)0x20) != (uint8_t)0x00) /* Interrupt or Slow slope */
      008E88 88               [ 1]  301 	push	a
                           0000E5   302 	Sstm8s_gpio$GPIO_Init$70 ==.
      008E89 7B 0C            [ 1]  303 	ld	a, (0x0c, sp)
      008E8B A5 20            [ 1]  304 	bcp	a, #0x20
      008E8D 84               [ 1]  305 	pop	a
                           0000EA   306 	Sstm8s_gpio$GPIO_Init$71 ==.
      008E8E 27 07            [ 1]  307 	jreq	00111$
                           0000EC   308 	Sstm8s_gpio$GPIO_Init$72 ==.
                           0000EC   309 	Sstm8s_gpio$GPIO_Init$73 ==.
                                    310 ;	drivers/src/stm8s_gpio.c: 125: GPIOx->CR2 |= (uint8_t)GPIO_Pin;
      008E90 1A 0A            [ 1]  311 	or	a, (0x0a, sp)
      008E92 1E 01            [ 2]  312 	ldw	x, (0x01, sp)
      008E94 F7               [ 1]  313 	ld	(x), a
                           0000F1   314 	Sstm8s_gpio$GPIO_Init$74 ==.
      008E95 20 05            [ 2]  315 	jra	00113$
      008E97                        316 00111$:
                           0000F3   317 	Sstm8s_gpio$GPIO_Init$75 ==.
                           0000F3   318 	Sstm8s_gpio$GPIO_Init$76 ==.
                                    319 ;	drivers/src/stm8s_gpio.c: 129: GPIOx->CR2 &= (uint8_t)(~(GPIO_Pin));
      008E97 14 03            [ 1]  320 	and	a, (0x03, sp)
      008E99 1E 01            [ 2]  321 	ldw	x, (0x01, sp)
      008E9B F7               [ 1]  322 	ld	(x), a
                           0000F8   323 	Sstm8s_gpio$GPIO_Init$77 ==.
      008E9C                        324 00113$:
                           0000F8   325 	Sstm8s_gpio$GPIO_Init$78 ==.
                                    326 ;	drivers/src/stm8s_gpio.c: 131: }
      008E9C 5B 05            [ 2]  327 	addw	sp, #5
                           0000FA   328 	Sstm8s_gpio$GPIO_Init$79 ==.
                           0000FA   329 	Sstm8s_gpio$GPIO_Init$80 ==.
                           0000FA   330 	XG$GPIO_Init$0$0 ==.
      008E9E 81               [ 4]  331 	ret
                           0000FB   332 	Sstm8s_gpio$GPIO_Init$81 ==.
                           0000FB   333 	Sstm8s_gpio$GPIO_Write$82 ==.
                                    334 ;	drivers/src/stm8s_gpio.c: 141: void GPIO_Write(GPIO_TypeDef* GPIOx, uint8_t PortVal)
                                    335 ;	-----------------------------------------
                                    336 ;	 function GPIO_Write
                                    337 ;	-----------------------------------------
      008E9F                        338 _GPIO_Write:
                           0000FB   339 	Sstm8s_gpio$GPIO_Write$83 ==.
                           0000FB   340 	Sstm8s_gpio$GPIO_Write$84 ==.
                                    341 ;	drivers/src/stm8s_gpio.c: 143: GPIOx->ODR = PortVal;
      008E9F 1E 03            [ 2]  342 	ldw	x, (0x03, sp)
      008EA1 7B 05            [ 1]  343 	ld	a, (0x05, sp)
      008EA3 F7               [ 1]  344 	ld	(x), a
                           000100   345 	Sstm8s_gpio$GPIO_Write$85 ==.
                                    346 ;	drivers/src/stm8s_gpio.c: 144: }
                           000100   347 	Sstm8s_gpio$GPIO_Write$86 ==.
                           000100   348 	XG$GPIO_Write$0$0 ==.
      008EA4 81               [ 4]  349 	ret
                           000101   350 	Sstm8s_gpio$GPIO_Write$87 ==.
                           000101   351 	Sstm8s_gpio$GPIO_WriteHigh$88 ==.
                                    352 ;	drivers/src/stm8s_gpio.c: 154: void GPIO_WriteHigh(GPIO_TypeDef* GPIOx, GPIO_Pin_TypeDef PortPins)
                                    353 ;	-----------------------------------------
                                    354 ;	 function GPIO_WriteHigh
                                    355 ;	-----------------------------------------
      008EA5                        356 _GPIO_WriteHigh:
                           000101   357 	Sstm8s_gpio$GPIO_WriteHigh$89 ==.
                           000101   358 	Sstm8s_gpio$GPIO_WriteHigh$90 ==.
                                    359 ;	drivers/src/stm8s_gpio.c: 156: GPIOx->ODR |= (uint8_t)PortPins;
      008EA5 1E 03            [ 2]  360 	ldw	x, (0x03, sp)
      008EA7 F6               [ 1]  361 	ld	a, (x)
      008EA8 1A 05            [ 1]  362 	or	a, (0x05, sp)
      008EAA F7               [ 1]  363 	ld	(x), a
                           000107   364 	Sstm8s_gpio$GPIO_WriteHigh$91 ==.
                                    365 ;	drivers/src/stm8s_gpio.c: 157: }
                           000107   366 	Sstm8s_gpio$GPIO_WriteHigh$92 ==.
                           000107   367 	XG$GPIO_WriteHigh$0$0 ==.
      008EAB 81               [ 4]  368 	ret
                           000108   369 	Sstm8s_gpio$GPIO_WriteHigh$93 ==.
                           000108   370 	Sstm8s_gpio$GPIO_WriteLow$94 ==.
                                    371 ;	drivers/src/stm8s_gpio.c: 167: void GPIO_WriteLow(GPIO_TypeDef* GPIOx, GPIO_Pin_TypeDef PortPins)
                                    372 ;	-----------------------------------------
                                    373 ;	 function GPIO_WriteLow
                                    374 ;	-----------------------------------------
      008EAC                        375 _GPIO_WriteLow:
                           000108   376 	Sstm8s_gpio$GPIO_WriteLow$95 ==.
      008EAC 88               [ 1]  377 	push	a
                           000109   378 	Sstm8s_gpio$GPIO_WriteLow$96 ==.
                           000109   379 	Sstm8s_gpio$GPIO_WriteLow$97 ==.
                                    380 ;	drivers/src/stm8s_gpio.c: 169: GPIOx->ODR &= (uint8_t)(~PortPins);
      008EAD 1E 04            [ 2]  381 	ldw	x, (0x04, sp)
      008EAF F6               [ 1]  382 	ld	a, (x)
      008EB0 6B 01            [ 1]  383 	ld	(0x01, sp), a
      008EB2 7B 06            [ 1]  384 	ld	a, (0x06, sp)
      008EB4 43               [ 1]  385 	cpl	a
      008EB5 14 01            [ 1]  386 	and	a, (0x01, sp)
      008EB7 F7               [ 1]  387 	ld	(x), a
                           000114   388 	Sstm8s_gpio$GPIO_WriteLow$98 ==.
                                    389 ;	drivers/src/stm8s_gpio.c: 170: }
      008EB8 84               [ 1]  390 	pop	a
                           000115   391 	Sstm8s_gpio$GPIO_WriteLow$99 ==.
                           000115   392 	Sstm8s_gpio$GPIO_WriteLow$100 ==.
                           000115   393 	XG$GPIO_WriteLow$0$0 ==.
      008EB9 81               [ 4]  394 	ret
                           000116   395 	Sstm8s_gpio$GPIO_WriteLow$101 ==.
                           000116   396 	Sstm8s_gpio$GPIO_WriteReverse$102 ==.
                                    397 ;	drivers/src/stm8s_gpio.c: 180: void GPIO_WriteReverse(GPIO_TypeDef* GPIOx, GPIO_Pin_TypeDef PortPins)
                                    398 ;	-----------------------------------------
                                    399 ;	 function GPIO_WriteReverse
                                    400 ;	-----------------------------------------
      008EBA                        401 _GPIO_WriteReverse:
                           000116   402 	Sstm8s_gpio$GPIO_WriteReverse$103 ==.
                           000116   403 	Sstm8s_gpio$GPIO_WriteReverse$104 ==.
                                    404 ;	drivers/src/stm8s_gpio.c: 182: GPIOx->ODR ^= (uint8_t)PortPins;
      008EBA 1E 03            [ 2]  405 	ldw	x, (0x03, sp)
      008EBC F6               [ 1]  406 	ld	a, (x)
      008EBD 18 05            [ 1]  407 	xor	a, (0x05, sp)
      008EBF F7               [ 1]  408 	ld	(x), a
                           00011C   409 	Sstm8s_gpio$GPIO_WriteReverse$105 ==.
                                    410 ;	drivers/src/stm8s_gpio.c: 183: }
                           00011C   411 	Sstm8s_gpio$GPIO_WriteReverse$106 ==.
                           00011C   412 	XG$GPIO_WriteReverse$0$0 ==.
      008EC0 81               [ 4]  413 	ret
                           00011D   414 	Sstm8s_gpio$GPIO_WriteReverse$107 ==.
                           00011D   415 	Sstm8s_gpio$GPIO_ReadOutputData$108 ==.
                                    416 ;	drivers/src/stm8s_gpio.c: 191: uint8_t GPIO_ReadOutputData(GPIO_TypeDef* GPIOx)
                                    417 ;	-----------------------------------------
                                    418 ;	 function GPIO_ReadOutputData
                                    419 ;	-----------------------------------------
      008EC1                        420 _GPIO_ReadOutputData:
                           00011D   421 	Sstm8s_gpio$GPIO_ReadOutputData$109 ==.
                           00011D   422 	Sstm8s_gpio$GPIO_ReadOutputData$110 ==.
                                    423 ;	drivers/src/stm8s_gpio.c: 193: return ((uint8_t)GPIOx->ODR);
      008EC1 1E 03            [ 2]  424 	ldw	x, (0x03, sp)
      008EC3 F6               [ 1]  425 	ld	a, (x)
                           000120   426 	Sstm8s_gpio$GPIO_ReadOutputData$111 ==.
                                    427 ;	drivers/src/stm8s_gpio.c: 194: }
                           000120   428 	Sstm8s_gpio$GPIO_ReadOutputData$112 ==.
                           000120   429 	XG$GPIO_ReadOutputData$0$0 ==.
      008EC4 81               [ 4]  430 	ret
                           000121   431 	Sstm8s_gpio$GPIO_ReadOutputData$113 ==.
                           000121   432 	Sstm8s_gpio$GPIO_ReadInputData$114 ==.
                                    433 ;	drivers/src/stm8s_gpio.c: 202: uint8_t GPIO_ReadInputData(GPIO_TypeDef* GPIOx)
                                    434 ;	-----------------------------------------
                                    435 ;	 function GPIO_ReadInputData
                                    436 ;	-----------------------------------------
      008EC5                        437 _GPIO_ReadInputData:
                           000121   438 	Sstm8s_gpio$GPIO_ReadInputData$115 ==.
                           000121   439 	Sstm8s_gpio$GPIO_ReadInputData$116 ==.
                                    440 ;	drivers/src/stm8s_gpio.c: 204: return ((uint8_t)GPIOx->IDR);
      008EC5 1E 03            [ 2]  441 	ldw	x, (0x03, sp)
      008EC7 E6 01            [ 1]  442 	ld	a, (0x1, x)
                           000125   443 	Sstm8s_gpio$GPIO_ReadInputData$117 ==.
                                    444 ;	drivers/src/stm8s_gpio.c: 205: }
                           000125   445 	Sstm8s_gpio$GPIO_ReadInputData$118 ==.
                           000125   446 	XG$GPIO_ReadInputData$0$0 ==.
      008EC9 81               [ 4]  447 	ret
                           000126   448 	Sstm8s_gpio$GPIO_ReadInputData$119 ==.
                           000126   449 	Sstm8s_gpio$GPIO_ReadInputPin$120 ==.
                                    450 ;	drivers/src/stm8s_gpio.c: 213: BitStatus GPIO_ReadInputPin(GPIO_TypeDef* GPIOx, GPIO_Pin_TypeDef GPIO_Pin)
                                    451 ;	-----------------------------------------
                                    452 ;	 function GPIO_ReadInputPin
                                    453 ;	-----------------------------------------
      008ECA                        454 _GPIO_ReadInputPin:
                           000126   455 	Sstm8s_gpio$GPIO_ReadInputPin$121 ==.
                           000126   456 	Sstm8s_gpio$GPIO_ReadInputPin$122 ==.
                                    457 ;	drivers/src/stm8s_gpio.c: 215: return ((BitStatus)(GPIOx->IDR & (uint8_t)GPIO_Pin));
      008ECA 1E 03            [ 2]  458 	ldw	x, (0x03, sp)
      008ECC E6 01            [ 1]  459 	ld	a, (0x1, x)
      008ECE 14 05            [ 1]  460 	and	a, (0x05, sp)
                           00012C   461 	Sstm8s_gpio$GPIO_ReadInputPin$123 ==.
                                    462 ;	drivers/src/stm8s_gpio.c: 216: }
                           00012C   463 	Sstm8s_gpio$GPIO_ReadInputPin$124 ==.
                           00012C   464 	XG$GPIO_ReadInputPin$0$0 ==.
      008ED0 81               [ 4]  465 	ret
                           00012D   466 	Sstm8s_gpio$GPIO_ReadInputPin$125 ==.
                           00012D   467 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$126 ==.
                                    468 ;	drivers/src/stm8s_gpio.c: 225: void GPIO_ExternalPullUpConfig(GPIO_TypeDef* GPIOx, GPIO_Pin_TypeDef GPIO_Pin, FunctionalState NewState)
                                    469 ;	-----------------------------------------
                                    470 ;	 function GPIO_ExternalPullUpConfig
                                    471 ;	-----------------------------------------
      008ED1                        472 _GPIO_ExternalPullUpConfig:
                           00012D   473 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$127 ==.
      008ED1 88               [ 1]  474 	push	a
                           00012E   475 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$128 ==.
                           00012E   476 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$129 ==.
                                    477 ;	drivers/src/stm8s_gpio.c: 228: assert_param(IS_GPIO_PIN_OK(GPIO_Pin));
      008ED2 0D 06            [ 1]  478 	tnz	(0x06, sp)
      008ED4 26 0F            [ 1]  479 	jrne	00107$
      008ED6 4B E4            [ 1]  480 	push	#0xe4
                           000134   481 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$130 ==.
      008ED8 5F               [ 1]  482 	clrw	x
      008ED9 89               [ 2]  483 	pushw	x
                           000136   484 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$131 ==.
      008EDA 4B 00            [ 1]  485 	push	#0x00
                           000138   486 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$132 ==.
      008EDC 4B 13            [ 1]  487 	push	#<(___str_0+0)
                           00013A   488 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$133 ==.
      008EDE 4B 81            [ 1]  489 	push	#((___str_0+0) >> 8)
                           00013C   490 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$134 ==.
      008EE0 CD 84 F3         [ 4]  491 	call	_assert_failed
      008EE3 5B 06            [ 2]  492 	addw	sp, #6
                           000141   493 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$135 ==.
      008EE5                        494 00107$:
                           000141   495 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$136 ==.
                                    496 ;	drivers/src/stm8s_gpio.c: 229: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
      008EE5 0D 07            [ 1]  497 	tnz	(0x07, sp)
      008EE7 27 14            [ 1]  498 	jreq	00109$
      008EE9 7B 07            [ 1]  499 	ld	a, (0x07, sp)
      008EEB 4A               [ 1]  500 	dec	a
      008EEC 27 0F            [ 1]  501 	jreq	00109$
                           00014A   502 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$137 ==.
      008EEE 4B E5            [ 1]  503 	push	#0xe5
                           00014C   504 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$138 ==.
      008EF0 5F               [ 1]  505 	clrw	x
      008EF1 89               [ 2]  506 	pushw	x
                           00014E   507 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$139 ==.
      008EF2 4B 00            [ 1]  508 	push	#0x00
                           000150   509 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$140 ==.
      008EF4 4B 13            [ 1]  510 	push	#<(___str_0+0)
                           000152   511 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$141 ==.
      008EF6 4B 81            [ 1]  512 	push	#((___str_0+0) >> 8)
                           000154   513 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$142 ==.
      008EF8 CD 84 F3         [ 4]  514 	call	_assert_failed
      008EFB 5B 06            [ 2]  515 	addw	sp, #6
                           000159   516 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$143 ==.
      008EFD                        517 00109$:
                           000159   518 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$144 ==.
                                    519 ;	drivers/src/stm8s_gpio.c: 233: GPIOx->CR1 |= (uint8_t)GPIO_Pin;
      008EFD 1E 04            [ 2]  520 	ldw	x, (0x04, sp)
      008EFF 1C 00 03         [ 2]  521 	addw	x, #0x0003
      008F02 F6               [ 1]  522 	ld	a, (x)
                           00015F   523 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$145 ==.
                                    524 ;	drivers/src/stm8s_gpio.c: 231: if (NewState != DISABLE) /* External Pull-Up Set*/
      008F03 0D 07            [ 1]  525 	tnz	(0x07, sp)
      008F05 27 05            [ 1]  526 	jreq	00102$
                           000163   527 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$146 ==.
                           000163   528 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$147 ==.
                                    529 ;	drivers/src/stm8s_gpio.c: 233: GPIOx->CR1 |= (uint8_t)GPIO_Pin;
      008F07 1A 06            [ 1]  530 	or	a, (0x06, sp)
      008F09 F7               [ 1]  531 	ld	(x), a
                           000166   532 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$148 ==.
      008F0A 20 0A            [ 2]  533 	jra	00104$
      008F0C                        534 00102$:
                           000168   535 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$149 ==.
                           000168   536 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$150 ==.
                                    537 ;	drivers/src/stm8s_gpio.c: 236: GPIOx->CR1 &= (uint8_t)(~(GPIO_Pin));
      008F0C 88               [ 1]  538 	push	a
                           000169   539 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$151 ==.
      008F0D 7B 07            [ 1]  540 	ld	a, (0x07, sp)
      008F0F 43               [ 1]  541 	cpl	a
      008F10 6B 02            [ 1]  542 	ld	(0x02, sp), a
      008F12 84               [ 1]  543 	pop	a
                           00016F   544 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$152 ==.
      008F13 14 01            [ 1]  545 	and	a, (0x01, sp)
      008F15 F7               [ 1]  546 	ld	(x), a
                           000172   547 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$153 ==.
      008F16                        548 00104$:
                           000172   549 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$154 ==.
                                    550 ;	drivers/src/stm8s_gpio.c: 238: }
      008F16 84               [ 1]  551 	pop	a
                           000173   552 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$155 ==.
                           000173   553 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$156 ==.
                           000173   554 	XG$GPIO_ExternalPullUpConfig$0$0 ==.
      008F17 81               [ 4]  555 	ret
                           000174   556 	Sstm8s_gpio$GPIO_ExternalPullUpConfig$157 ==.
                                    557 	.area CODE
                                    558 	.area CONST
                           000000   559 Fstm8s_gpio$__str_0$0_0$0 == .
                                    560 	.area CONST
      008113                        561 ___str_0:
      008113 64 72 69 76 65 72 73   562 	.ascii "drivers/src/stm8s_gpio.c"
             2F 73 72 63 2F 73 74
             6D 38 73 5F 67 70 69
             6F 2E 63
      00812B 00                     563 	.db 0x00
                                    564 	.area CODE
                                    565 	.area INITIALIZER
                                    566 	.area CABS (ABS)
                                    567 
                                    568 	.area .debug_line (NOLOAD)
      0010E2 00 00 02 3C            569 	.dw	0,Ldebug_line_end-Ldebug_line_start
      0010E6                        570 Ldebug_line_start:
      0010E6 00 02                  571 	.dw	2
      0010E8 00 00 00 79            572 	.dw	0,Ldebug_line_stmt-6-Ldebug_line_start
      0010EC 01                     573 	.db	1
      0010ED 01                     574 	.db	1
      0010EE FB                     575 	.db	-5
      0010EF 0F                     576 	.db	15
      0010F0 0A                     577 	.db	10
      0010F1 00                     578 	.db	0
      0010F2 01                     579 	.db	1
      0010F3 01                     580 	.db	1
      0010F4 01                     581 	.db	1
      0010F5 01                     582 	.db	1
      0010F6 00                     583 	.db	0
      0010F7 00                     584 	.db	0
      0010F8 00                     585 	.db	0
      0010F9 01                     586 	.db	1
      0010FA 43 3A 5C 50 72 6F 67   587 	.ascii "C:\Program Files\SDCC\bin\..\include\stm8"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
             5C 73 74 6D 38
      001122 00                     588 	.db	0
      001123 43 3A 5C 50 72 6F 67   589 	.ascii "C:\Program Files\SDCC\bin\..\include"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
      001146 00                     590 	.db	0
      001147 00                     591 	.db	0
      001148 64 72 69 76 65 72 73   592 	.ascii "drivers/src/stm8s_gpio.c"
             2F 73 72 63 2F 73 74
             6D 38 73 5F 67 70 69
             6F 2E 63
      001160 00                     593 	.db	0
      001161 00                     594 	.uleb128	0
      001162 00                     595 	.uleb128	0
      001163 00                     596 	.uleb128	0
      001164 00                     597 	.db	0
      001165                        598 Ldebug_line_stmt:
      001165 00                     599 	.db	0
      001166 05                     600 	.uleb128	5
      001167 02                     601 	.db	2
      001168 00 00 8D A4            602 	.dw	0,(Sstm8s_gpio$GPIO_DeInit$0)
      00116C 03                     603 	.db	3
      00116D 34                     604 	.sleb128	52
      00116E 01                     605 	.db	1
      00116F 09                     606 	.db	9
      001170 00 00                  607 	.dw	Sstm8s_gpio$GPIO_DeInit$2-Sstm8s_gpio$GPIO_DeInit$0
      001172 03                     608 	.db	3
      001173 02                     609 	.sleb128	2
      001174 01                     610 	.db	1
      001175 09                     611 	.db	9
      001176 00 04                  612 	.dw	Sstm8s_gpio$GPIO_DeInit$3-Sstm8s_gpio$GPIO_DeInit$2
      001178 03                     613 	.db	3
      001179 01                     614 	.sleb128	1
      00117A 01                     615 	.db	1
      00117B 09                     616 	.db	9
      00117C 00 04                  617 	.dw	Sstm8s_gpio$GPIO_DeInit$4-Sstm8s_gpio$GPIO_DeInit$3
      00117E 03                     618 	.db	3
      00117F 01                     619 	.sleb128	1
      001180 01                     620 	.db	1
      001181 09                     621 	.db	9
      001182 00 03                  622 	.dw	Sstm8s_gpio$GPIO_DeInit$5-Sstm8s_gpio$GPIO_DeInit$4
      001184 03                     623 	.db	3
      001185 01                     624 	.sleb128	1
      001186 01                     625 	.db	1
      001187 09                     626 	.db	9
      001188 00 03                  627 	.dw	Sstm8s_gpio$GPIO_DeInit$6-Sstm8s_gpio$GPIO_DeInit$5
      00118A 03                     628 	.db	3
      00118B 01                     629 	.sleb128	1
      00118C 01                     630 	.db	1
      00118D 09                     631 	.db	9
      00118E 00 01                  632 	.dw	1+Sstm8s_gpio$GPIO_DeInit$7-Sstm8s_gpio$GPIO_DeInit$6
      001190 00                     633 	.db	0
      001191 01                     634 	.uleb128	1
      001192 01                     635 	.db	1
      001193 00                     636 	.db	0
      001194 05                     637 	.uleb128	5
      001195 02                     638 	.db	2
      001196 00 00 8D B3            639 	.dw	0,(Sstm8s_gpio$GPIO_Init$9)
      00119A 03                     640 	.db	3
      00119B C6 00                  641 	.sleb128	70
      00119D 01                     642 	.db	1
      00119E 09                     643 	.db	9
      00119F 00 02                  644 	.dw	Sstm8s_gpio$GPIO_Init$12-Sstm8s_gpio$GPIO_Init$9
      0011A1 03                     645 	.db	3
      0011A2 06                     646 	.sleb128	6
      0011A3 01                     647 	.db	1
      0011A4 09                     648 	.db	9
      0011A5 00 64                  649 	.dw	Sstm8s_gpio$GPIO_Init$30-Sstm8s_gpio$GPIO_Init$12
      0011A7 03                     650 	.db	3
      0011A8 01                     651 	.sleb128	1
      0011A9 01                     652 	.db	1
      0011AA 09                     653 	.db	9
      0011AB 00 13                  654 	.dw	Sstm8s_gpio$GPIO_Init$37-Sstm8s_gpio$GPIO_Init$30
      0011AD 03                     655 	.db	3
      0011AE 03                     656 	.sleb128	3
      0011AF 01                     657 	.db	1
      0011B0 09                     658 	.db	9
      0011B1 00 15                  659 	.dw	Sstm8s_gpio$GPIO_Init$40-Sstm8s_gpio$GPIO_Init$37
      0011B3 03                     660 	.db	3
      0011B4 11                     661 	.sleb128	17
      0011B5 01                     662 	.db	1
      0011B6 09                     663 	.db	9
      0011B7 00 05                  664 	.dw	Sstm8s_gpio$GPIO_Init$41-Sstm8s_gpio$GPIO_Init$40
      0011B9 03                     665 	.db	3
      0011BA 75                     666 	.sleb128	-11
      0011BB 01                     667 	.db	1
      0011BC 09                     668 	.db	9
      0011BD 00 04                  669 	.dw	Sstm8s_gpio$GPIO_Init$42-Sstm8s_gpio$GPIO_Init$41
      0011BF 03                     670 	.db	3
      0011C0 04                     671 	.sleb128	4
      0011C1 01                     672 	.db	1
      0011C2 09                     673 	.db	9
      0011C3 00 02                  674 	.dw	Sstm8s_gpio$GPIO_Init$44-Sstm8s_gpio$GPIO_Init$42
      0011C5 03                     675 	.db	3
      0011C6 7E                     676 	.sleb128	-2
      0011C7 01                     677 	.db	1
      0011C8 09                     678 	.db	9
      0011C9 00 08                  679 	.dw	Sstm8s_gpio$GPIO_Init$48-Sstm8s_gpio$GPIO_Init$44
      0011CB 03                     680 	.db	3
      0011CC 02                     681 	.sleb128	2
      0011CD 01                     682 	.db	1
      0011CE 09                     683 	.db	9
      0011CF 00 06                  684 	.dw	Sstm8s_gpio$GPIO_Init$51-Sstm8s_gpio$GPIO_Init$48
      0011D1 03                     685 	.db	3
      0011D2 04                     686 	.sleb128	4
      0011D3 01                     687 	.db	1
      0011D4 09                     688 	.db	9
      0011D5 00 04                  689 	.dw	Sstm8s_gpio$GPIO_Init$53-Sstm8s_gpio$GPIO_Init$51
      0011D7 03                     690 	.db	3
      0011D8 03                     691 	.sleb128	3
      0011D9 01                     692 	.db	1
      0011DA 09                     693 	.db	9
      0011DB 00 0A                  694 	.dw	Sstm8s_gpio$GPIO_Init$56-Sstm8s_gpio$GPIO_Init$53
      0011DD 03                     695 	.db	3
      0011DE 05                     696 	.sleb128	5
      0011DF 01                     697 	.db	1
      0011E0 09                     698 	.db	9
      0011E1 00 08                  699 	.dw	Sstm8s_gpio$GPIO_Init$58-Sstm8s_gpio$GPIO_Init$56
      0011E3 03                     700 	.db	3
      0011E4 09                     701 	.sleb128	9
      0011E5 01                     702 	.db	1
      0011E6 09                     703 	.db	9
      0011E7 00 05                  704 	.dw	Sstm8s_gpio$GPIO_Init$59-Sstm8s_gpio$GPIO_Init$58
      0011E9 03                     705 	.db	3
      0011EA 7E                     706 	.sleb128	-2
      0011EB 01                     707 	.db	1
      0011EC 09                     708 	.db	9
      0011ED 00 08                  709 	.dw	Sstm8s_gpio$GPIO_Init$63-Sstm8s_gpio$GPIO_Init$59
      0011EF 03                     710 	.db	3
      0011F0 02                     711 	.sleb128	2
      0011F1 01                     712 	.db	1
      0011F2 09                     713 	.db	9
      0011F3 00 05                  714 	.dw	Sstm8s_gpio$GPIO_Init$66-Sstm8s_gpio$GPIO_Init$63
      0011F5 03                     715 	.db	3
      0011F6 04                     716 	.sleb128	4
      0011F7 01                     717 	.db	1
      0011F8 09                     718 	.db	9
      0011F9 00 03                  719 	.dw	Sstm8s_gpio$GPIO_Init$68-Sstm8s_gpio$GPIO_Init$66
      0011FB 03                     720 	.db	3
      0011FC 5D                     721 	.sleb128	-35
      0011FD 01                     722 	.db	1
      0011FE 09                     723 	.db	9
      0011FF 00 03                  724 	.dw	Sstm8s_gpio$GPIO_Init$69-Sstm8s_gpio$GPIO_Init$68
      001201 03                     725 	.db	3
      001202 2A                     726 	.sleb128	42
      001203 01                     727 	.db	1
      001204 09                     728 	.db	9
      001205 00 08                  729 	.dw	Sstm8s_gpio$GPIO_Init$73-Sstm8s_gpio$GPIO_Init$69
      001207 03                     730 	.db	3
      001208 02                     731 	.sleb128	2
      001209 01                     732 	.db	1
      00120A 09                     733 	.db	9
      00120B 00 07                  734 	.dw	Sstm8s_gpio$GPIO_Init$76-Sstm8s_gpio$GPIO_Init$73
      00120D 03                     735 	.db	3
      00120E 04                     736 	.sleb128	4
      00120F 01                     737 	.db	1
      001210 09                     738 	.db	9
      001211 00 05                  739 	.dw	Sstm8s_gpio$GPIO_Init$78-Sstm8s_gpio$GPIO_Init$76
      001213 03                     740 	.db	3
      001214 02                     741 	.sleb128	2
      001215 01                     742 	.db	1
      001216 09                     743 	.db	9
      001217 00 03                  744 	.dw	1+Sstm8s_gpio$GPIO_Init$80-Sstm8s_gpio$GPIO_Init$78
      001219 00                     745 	.db	0
      00121A 01                     746 	.uleb128	1
      00121B 01                     747 	.db	1
      00121C 00                     748 	.db	0
      00121D 05                     749 	.uleb128	5
      00121E 02                     750 	.db	2
      00121F 00 00 8E 9F            751 	.dw	0,(Sstm8s_gpio$GPIO_Write$82)
      001223 03                     752 	.db	3
      001224 8C 01                  753 	.sleb128	140
      001226 01                     754 	.db	1
      001227 09                     755 	.db	9
      001228 00 00                  756 	.dw	Sstm8s_gpio$GPIO_Write$84-Sstm8s_gpio$GPIO_Write$82
      00122A 03                     757 	.db	3
      00122B 02                     758 	.sleb128	2
      00122C 01                     759 	.db	1
      00122D 09                     760 	.db	9
      00122E 00 05                  761 	.dw	Sstm8s_gpio$GPIO_Write$85-Sstm8s_gpio$GPIO_Write$84
      001230 03                     762 	.db	3
      001231 01                     763 	.sleb128	1
      001232 01                     764 	.db	1
      001233 09                     765 	.db	9
      001234 00 01                  766 	.dw	1+Sstm8s_gpio$GPIO_Write$86-Sstm8s_gpio$GPIO_Write$85
      001236 00                     767 	.db	0
      001237 01                     768 	.uleb128	1
      001238 01                     769 	.db	1
      001239 00                     770 	.db	0
      00123A 05                     771 	.uleb128	5
      00123B 02                     772 	.db	2
      00123C 00 00 8E A5            773 	.dw	0,(Sstm8s_gpio$GPIO_WriteHigh$88)
      001240 03                     774 	.db	3
      001241 99 01                  775 	.sleb128	153
      001243 01                     776 	.db	1
      001244 09                     777 	.db	9
      001245 00 00                  778 	.dw	Sstm8s_gpio$GPIO_WriteHigh$90-Sstm8s_gpio$GPIO_WriteHigh$88
      001247 03                     779 	.db	3
      001248 02                     780 	.sleb128	2
      001249 01                     781 	.db	1
      00124A 09                     782 	.db	9
      00124B 00 06                  783 	.dw	Sstm8s_gpio$GPIO_WriteHigh$91-Sstm8s_gpio$GPIO_WriteHigh$90
      00124D 03                     784 	.db	3
      00124E 01                     785 	.sleb128	1
      00124F 01                     786 	.db	1
      001250 09                     787 	.db	9
      001251 00 01                  788 	.dw	1+Sstm8s_gpio$GPIO_WriteHigh$92-Sstm8s_gpio$GPIO_WriteHigh$91
      001253 00                     789 	.db	0
      001254 01                     790 	.uleb128	1
      001255 01                     791 	.db	1
      001256 00                     792 	.db	0
      001257 05                     793 	.uleb128	5
      001258 02                     794 	.db	2
      001259 00 00 8E AC            795 	.dw	0,(Sstm8s_gpio$GPIO_WriteLow$94)
      00125D 03                     796 	.db	3
      00125E A6 01                  797 	.sleb128	166
      001260 01                     798 	.db	1
      001261 09                     799 	.db	9
      001262 00 01                  800 	.dw	Sstm8s_gpio$GPIO_WriteLow$97-Sstm8s_gpio$GPIO_WriteLow$94
      001264 03                     801 	.db	3
      001265 02                     802 	.sleb128	2
      001266 01                     803 	.db	1
      001267 09                     804 	.db	9
      001268 00 0B                  805 	.dw	Sstm8s_gpio$GPIO_WriteLow$98-Sstm8s_gpio$GPIO_WriteLow$97
      00126A 03                     806 	.db	3
      00126B 01                     807 	.sleb128	1
      00126C 01                     808 	.db	1
      00126D 09                     809 	.db	9
      00126E 00 02                  810 	.dw	1+Sstm8s_gpio$GPIO_WriteLow$100-Sstm8s_gpio$GPIO_WriteLow$98
      001270 00                     811 	.db	0
      001271 01                     812 	.uleb128	1
      001272 01                     813 	.db	1
      001273 00                     814 	.db	0
      001274 05                     815 	.uleb128	5
      001275 02                     816 	.db	2
      001276 00 00 8E BA            817 	.dw	0,(Sstm8s_gpio$GPIO_WriteReverse$102)
      00127A 03                     818 	.db	3
      00127B B3 01                  819 	.sleb128	179
      00127D 01                     820 	.db	1
      00127E 09                     821 	.db	9
      00127F 00 00                  822 	.dw	Sstm8s_gpio$GPIO_WriteReverse$104-Sstm8s_gpio$GPIO_WriteReverse$102
      001281 03                     823 	.db	3
      001282 02                     824 	.sleb128	2
      001283 01                     825 	.db	1
      001284 09                     826 	.db	9
      001285 00 06                  827 	.dw	Sstm8s_gpio$GPIO_WriteReverse$105-Sstm8s_gpio$GPIO_WriteReverse$104
      001287 03                     828 	.db	3
      001288 01                     829 	.sleb128	1
      001289 01                     830 	.db	1
      00128A 09                     831 	.db	9
      00128B 00 01                  832 	.dw	1+Sstm8s_gpio$GPIO_WriteReverse$106-Sstm8s_gpio$GPIO_WriteReverse$105
      00128D 00                     833 	.db	0
      00128E 01                     834 	.uleb128	1
      00128F 01                     835 	.db	1
      001290 00                     836 	.db	0
      001291 05                     837 	.uleb128	5
      001292 02                     838 	.db	2
      001293 00 00 8E C1            839 	.dw	0,(Sstm8s_gpio$GPIO_ReadOutputData$108)
      001297 03                     840 	.db	3
      001298 BE 01                  841 	.sleb128	190
      00129A 01                     842 	.db	1
      00129B 09                     843 	.db	9
      00129C 00 00                  844 	.dw	Sstm8s_gpio$GPIO_ReadOutputData$110-Sstm8s_gpio$GPIO_ReadOutputData$108
      00129E 03                     845 	.db	3
      00129F 02                     846 	.sleb128	2
      0012A0 01                     847 	.db	1
      0012A1 09                     848 	.db	9
      0012A2 00 03                  849 	.dw	Sstm8s_gpio$GPIO_ReadOutputData$111-Sstm8s_gpio$GPIO_ReadOutputData$110
      0012A4 03                     850 	.db	3
      0012A5 01                     851 	.sleb128	1
      0012A6 01                     852 	.db	1
      0012A7 09                     853 	.db	9
      0012A8 00 01                  854 	.dw	1+Sstm8s_gpio$GPIO_ReadOutputData$112-Sstm8s_gpio$GPIO_ReadOutputData$111
      0012AA 00                     855 	.db	0
      0012AB 01                     856 	.uleb128	1
      0012AC 01                     857 	.db	1
      0012AD 00                     858 	.db	0
      0012AE 05                     859 	.uleb128	5
      0012AF 02                     860 	.db	2
      0012B0 00 00 8E C5            861 	.dw	0,(Sstm8s_gpio$GPIO_ReadInputData$114)
      0012B4 03                     862 	.db	3
      0012B5 C9 01                  863 	.sleb128	201
      0012B7 01                     864 	.db	1
      0012B8 09                     865 	.db	9
      0012B9 00 00                  866 	.dw	Sstm8s_gpio$GPIO_ReadInputData$116-Sstm8s_gpio$GPIO_ReadInputData$114
      0012BB 03                     867 	.db	3
      0012BC 02                     868 	.sleb128	2
      0012BD 01                     869 	.db	1
      0012BE 09                     870 	.db	9
      0012BF 00 04                  871 	.dw	Sstm8s_gpio$GPIO_ReadInputData$117-Sstm8s_gpio$GPIO_ReadInputData$116
      0012C1 03                     872 	.db	3
      0012C2 01                     873 	.sleb128	1
      0012C3 01                     874 	.db	1
      0012C4 09                     875 	.db	9
      0012C5 00 01                  876 	.dw	1+Sstm8s_gpio$GPIO_ReadInputData$118-Sstm8s_gpio$GPIO_ReadInputData$117
      0012C7 00                     877 	.db	0
      0012C8 01                     878 	.uleb128	1
      0012C9 01                     879 	.db	1
      0012CA 00                     880 	.db	0
      0012CB 05                     881 	.uleb128	5
      0012CC 02                     882 	.db	2
      0012CD 00 00 8E CA            883 	.dw	0,(Sstm8s_gpio$GPIO_ReadInputPin$120)
      0012D1 03                     884 	.db	3
      0012D2 D4 01                  885 	.sleb128	212
      0012D4 01                     886 	.db	1
      0012D5 09                     887 	.db	9
      0012D6 00 00                  888 	.dw	Sstm8s_gpio$GPIO_ReadInputPin$122-Sstm8s_gpio$GPIO_ReadInputPin$120
      0012D8 03                     889 	.db	3
      0012D9 02                     890 	.sleb128	2
      0012DA 01                     891 	.db	1
      0012DB 09                     892 	.db	9
      0012DC 00 06                  893 	.dw	Sstm8s_gpio$GPIO_ReadInputPin$123-Sstm8s_gpio$GPIO_ReadInputPin$122
      0012DE 03                     894 	.db	3
      0012DF 01                     895 	.sleb128	1
      0012E0 01                     896 	.db	1
      0012E1 09                     897 	.db	9
      0012E2 00 01                  898 	.dw	1+Sstm8s_gpio$GPIO_ReadInputPin$124-Sstm8s_gpio$GPIO_ReadInputPin$123
      0012E4 00                     899 	.db	0
      0012E5 01                     900 	.uleb128	1
      0012E6 01                     901 	.db	1
      0012E7 00                     902 	.db	0
      0012E8 05                     903 	.uleb128	5
      0012E9 02                     904 	.db	2
      0012EA 00 00 8E D1            905 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$126)
      0012EE 03                     906 	.db	3
      0012EF E0 01                  907 	.sleb128	224
      0012F1 01                     908 	.db	1
      0012F2 09                     909 	.db	9
      0012F3 00 01                  910 	.dw	Sstm8s_gpio$GPIO_ExternalPullUpConfig$129-Sstm8s_gpio$GPIO_ExternalPullUpConfig$126
      0012F5 03                     911 	.db	3
      0012F6 03                     912 	.sleb128	3
      0012F7 01                     913 	.db	1
      0012F8 09                     914 	.db	9
      0012F9 00 13                  915 	.dw	Sstm8s_gpio$GPIO_ExternalPullUpConfig$136-Sstm8s_gpio$GPIO_ExternalPullUpConfig$129
      0012FB 03                     916 	.db	3
      0012FC 01                     917 	.sleb128	1
      0012FD 01                     918 	.db	1
      0012FE 09                     919 	.db	9
      0012FF 00 18                  920 	.dw	Sstm8s_gpio$GPIO_ExternalPullUpConfig$144-Sstm8s_gpio$GPIO_ExternalPullUpConfig$136
      001301 03                     921 	.db	3
      001302 04                     922 	.sleb128	4
      001303 01                     923 	.db	1
      001304 09                     924 	.db	9
      001305 00 06                  925 	.dw	Sstm8s_gpio$GPIO_ExternalPullUpConfig$145-Sstm8s_gpio$GPIO_ExternalPullUpConfig$144
      001307 03                     926 	.db	3
      001308 7E                     927 	.sleb128	-2
      001309 01                     928 	.db	1
      00130A 09                     929 	.db	9
      00130B 00 04                  930 	.dw	Sstm8s_gpio$GPIO_ExternalPullUpConfig$147-Sstm8s_gpio$GPIO_ExternalPullUpConfig$145
      00130D 03                     931 	.db	3
      00130E 02                     932 	.sleb128	2
      00130F 01                     933 	.db	1
      001310 09                     934 	.db	9
      001311 00 05                  935 	.dw	Sstm8s_gpio$GPIO_ExternalPullUpConfig$150-Sstm8s_gpio$GPIO_ExternalPullUpConfig$147
      001313 03                     936 	.db	3
      001314 03                     937 	.sleb128	3
      001315 01                     938 	.db	1
      001316 09                     939 	.db	9
      001317 00 0A                  940 	.dw	Sstm8s_gpio$GPIO_ExternalPullUpConfig$154-Sstm8s_gpio$GPIO_ExternalPullUpConfig$150
      001319 03                     941 	.db	3
      00131A 02                     942 	.sleb128	2
      00131B 01                     943 	.db	1
      00131C 09                     944 	.db	9
      00131D 00 02                  945 	.dw	1+Sstm8s_gpio$GPIO_ExternalPullUpConfig$156-Sstm8s_gpio$GPIO_ExternalPullUpConfig$154
      00131F 00                     946 	.db	0
      001320 01                     947 	.uleb128	1
      001321 01                     948 	.db	1
      001322                        949 Ldebug_line_end:
                                    950 
                                    951 	.area .debug_loc (NOLOAD)
      001D3C                        952 Ldebug_loc_start:
      001D3C 00 00 8F 17            953 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$155)
      001D40 00 00 8F 18            954 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$157)
      001D44 00 02                  955 	.dw	2
      001D46 78                     956 	.db	120
      001D47 01                     957 	.sleb128	1
      001D48 00 00 8F 13            958 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$152)
      001D4C 00 00 8F 17            959 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$155)
      001D50 00 02                  960 	.dw	2
      001D52 78                     961 	.db	120
      001D53 02                     962 	.sleb128	2
      001D54 00 00 8F 0D            963 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$151)
      001D58 00 00 8F 13            964 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$152)
      001D5C 00 02                  965 	.dw	2
      001D5E 78                     966 	.db	120
      001D5F 03                     967 	.sleb128	3
      001D60 00 00 8E FD            968 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$143)
      001D64 00 00 8F 0D            969 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$151)
      001D68 00 02                  970 	.dw	2
      001D6A 78                     971 	.db	120
      001D6B 02                     972 	.sleb128	2
      001D6C 00 00 8E F8            973 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$142)
      001D70 00 00 8E FD            974 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$143)
      001D74 00 02                  975 	.dw	2
      001D76 78                     976 	.db	120
      001D77 08                     977 	.sleb128	8
      001D78 00 00 8E F6            978 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$141)
      001D7C 00 00 8E F8            979 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$142)
      001D80 00 02                  980 	.dw	2
      001D82 78                     981 	.db	120
      001D83 07                     982 	.sleb128	7
      001D84 00 00 8E F4            983 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$140)
      001D88 00 00 8E F6            984 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$141)
      001D8C 00 02                  985 	.dw	2
      001D8E 78                     986 	.db	120
      001D8F 06                     987 	.sleb128	6
      001D90 00 00 8E F2            988 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$139)
      001D94 00 00 8E F4            989 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$140)
      001D98 00 02                  990 	.dw	2
      001D9A 78                     991 	.db	120
      001D9B 05                     992 	.sleb128	5
      001D9C 00 00 8E F0            993 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$138)
      001DA0 00 00 8E F2            994 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$139)
      001DA4 00 02                  995 	.dw	2
      001DA6 78                     996 	.db	120
      001DA7 03                     997 	.sleb128	3
      001DA8 00 00 8E EE            998 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$137)
      001DAC 00 00 8E F0            999 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$138)
      001DB0 00 02                 1000 	.dw	2
      001DB2 78                    1001 	.db	120
      001DB3 02                    1002 	.sleb128	2
      001DB4 00 00 8E E5           1003 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$135)
      001DB8 00 00 8E EE           1004 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$137)
      001DBC 00 02                 1005 	.dw	2
      001DBE 78                    1006 	.db	120
      001DBF 02                    1007 	.sleb128	2
      001DC0 00 00 8E E0           1008 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$134)
      001DC4 00 00 8E E5           1009 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$135)
      001DC8 00 02                 1010 	.dw	2
      001DCA 78                    1011 	.db	120
      001DCB 08                    1012 	.sleb128	8
      001DCC 00 00 8E DE           1013 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$133)
      001DD0 00 00 8E E0           1014 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$134)
      001DD4 00 02                 1015 	.dw	2
      001DD6 78                    1016 	.db	120
      001DD7 07                    1017 	.sleb128	7
      001DD8 00 00 8E DC           1018 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$132)
      001DDC 00 00 8E DE           1019 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$133)
      001DE0 00 02                 1020 	.dw	2
      001DE2 78                    1021 	.db	120
      001DE3 06                    1022 	.sleb128	6
      001DE4 00 00 8E DA           1023 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$131)
      001DE8 00 00 8E DC           1024 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$132)
      001DEC 00 02                 1025 	.dw	2
      001DEE 78                    1026 	.db	120
      001DEF 05                    1027 	.sleb128	5
      001DF0 00 00 8E D8           1028 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$130)
      001DF4 00 00 8E DA           1029 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$131)
      001DF8 00 02                 1030 	.dw	2
      001DFA 78                    1031 	.db	120
      001DFB 03                    1032 	.sleb128	3
      001DFC 00 00 8E D2           1033 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$128)
      001E00 00 00 8E D8           1034 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$130)
      001E04 00 02                 1035 	.dw	2
      001E06 78                    1036 	.db	120
      001E07 02                    1037 	.sleb128	2
      001E08 00 00 8E D1           1038 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$127)
      001E0C 00 00 8E D2           1039 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$128)
      001E10 00 02                 1040 	.dw	2
      001E12 78                    1041 	.db	120
      001E13 01                    1042 	.sleb128	1
      001E14 00 00 00 00           1043 	.dw	0,0
      001E18 00 00 00 00           1044 	.dw	0,0
      001E1C 00 00 8E CA           1045 	.dw	0,(Sstm8s_gpio$GPIO_ReadInputPin$121)
      001E20 00 00 8E D1           1046 	.dw	0,(Sstm8s_gpio$GPIO_ReadInputPin$125)
      001E24 00 02                 1047 	.dw	2
      001E26 78                    1048 	.db	120
      001E27 01                    1049 	.sleb128	1
      001E28 00 00 00 00           1050 	.dw	0,0
      001E2C 00 00 00 00           1051 	.dw	0,0
      001E30 00 00 8E C5           1052 	.dw	0,(Sstm8s_gpio$GPIO_ReadInputData$115)
      001E34 00 00 8E CA           1053 	.dw	0,(Sstm8s_gpio$GPIO_ReadInputData$119)
      001E38 00 02                 1054 	.dw	2
      001E3A 78                    1055 	.db	120
      001E3B 01                    1056 	.sleb128	1
      001E3C 00 00 00 00           1057 	.dw	0,0
      001E40 00 00 00 00           1058 	.dw	0,0
      001E44 00 00 8E C1           1059 	.dw	0,(Sstm8s_gpio$GPIO_ReadOutputData$109)
      001E48 00 00 8E C5           1060 	.dw	0,(Sstm8s_gpio$GPIO_ReadOutputData$113)
      001E4C 00 02                 1061 	.dw	2
      001E4E 78                    1062 	.db	120
      001E4F 01                    1063 	.sleb128	1
      001E50 00 00 00 00           1064 	.dw	0,0
      001E54 00 00 00 00           1065 	.dw	0,0
      001E58 00 00 8E BA           1066 	.dw	0,(Sstm8s_gpio$GPIO_WriteReverse$103)
      001E5C 00 00 8E C1           1067 	.dw	0,(Sstm8s_gpio$GPIO_WriteReverse$107)
      001E60 00 02                 1068 	.dw	2
      001E62 78                    1069 	.db	120
      001E63 01                    1070 	.sleb128	1
      001E64 00 00 00 00           1071 	.dw	0,0
      001E68 00 00 00 00           1072 	.dw	0,0
      001E6C 00 00 8E B9           1073 	.dw	0,(Sstm8s_gpio$GPIO_WriteLow$99)
      001E70 00 00 8E BA           1074 	.dw	0,(Sstm8s_gpio$GPIO_WriteLow$101)
      001E74 00 02                 1075 	.dw	2
      001E76 78                    1076 	.db	120
      001E77 01                    1077 	.sleb128	1
      001E78 00 00 8E AD           1078 	.dw	0,(Sstm8s_gpio$GPIO_WriteLow$96)
      001E7C 00 00 8E B9           1079 	.dw	0,(Sstm8s_gpio$GPIO_WriteLow$99)
      001E80 00 02                 1080 	.dw	2
      001E82 78                    1081 	.db	120
      001E83 02                    1082 	.sleb128	2
      001E84 00 00 8E AC           1083 	.dw	0,(Sstm8s_gpio$GPIO_WriteLow$95)
      001E88 00 00 8E AD           1084 	.dw	0,(Sstm8s_gpio$GPIO_WriteLow$96)
      001E8C 00 02                 1085 	.dw	2
      001E8E 78                    1086 	.db	120
      001E8F 01                    1087 	.sleb128	1
      001E90 00 00 00 00           1088 	.dw	0,0
      001E94 00 00 00 00           1089 	.dw	0,0
      001E98 00 00 8E A5           1090 	.dw	0,(Sstm8s_gpio$GPIO_WriteHigh$89)
      001E9C 00 00 8E AC           1091 	.dw	0,(Sstm8s_gpio$GPIO_WriteHigh$93)
      001EA0 00 02                 1092 	.dw	2
      001EA2 78                    1093 	.db	120
      001EA3 01                    1094 	.sleb128	1
      001EA4 00 00 00 00           1095 	.dw	0,0
      001EA8 00 00 00 00           1096 	.dw	0,0
      001EAC 00 00 8E 9F           1097 	.dw	0,(Sstm8s_gpio$GPIO_Write$83)
      001EB0 00 00 8E A5           1098 	.dw	0,(Sstm8s_gpio$GPIO_Write$87)
      001EB4 00 02                 1099 	.dw	2
      001EB6 78                    1100 	.db	120
      001EB7 01                    1101 	.sleb128	1
      001EB8 00 00 00 00           1102 	.dw	0,0
      001EBC 00 00 00 00           1103 	.dw	0,0
      001EC0 00 00 8E 9E           1104 	.dw	0,(Sstm8s_gpio$GPIO_Init$79)
      001EC4 00 00 8E 9F           1105 	.dw	0,(Sstm8s_gpio$GPIO_Init$81)
      001EC8 00 02                 1106 	.dw	2
      001ECA 78                    1107 	.db	120
      001ECB 01                    1108 	.sleb128	1
      001ECC 00 00 8E 8E           1109 	.dw	0,(Sstm8s_gpio$GPIO_Init$71)
      001ED0 00 00 8E 9E           1110 	.dw	0,(Sstm8s_gpio$GPIO_Init$79)
      001ED4 00 02                 1111 	.dw	2
      001ED6 78                    1112 	.db	120
      001ED7 06                    1113 	.sleb128	6
      001ED8 00 00 8E 89           1114 	.dw	0,(Sstm8s_gpio$GPIO_Init$70)
      001EDC 00 00 8E 8E           1115 	.dw	0,(Sstm8s_gpio$GPIO_Init$71)
      001EE0 00 02                 1116 	.dw	2
      001EE2 78                    1117 	.db	120
      001EE3 07                    1118 	.sleb128	7
      001EE4 00 00 8E 7B           1119 	.dw	0,(Sstm8s_gpio$GPIO_Init$61)
      001EE8 00 00 8E 89           1120 	.dw	0,(Sstm8s_gpio$GPIO_Init$70)
      001EEC 00 02                 1121 	.dw	2
      001EEE 78                    1122 	.db	120
      001EEF 06                    1123 	.sleb128	6
      001EF0 00 00 8E 76           1124 	.dw	0,(Sstm8s_gpio$GPIO_Init$60)
      001EF4 00 00 8E 7B           1125 	.dw	0,(Sstm8s_gpio$GPIO_Init$61)
      001EF8 00 02                 1126 	.dw	2
      001EFA 78                    1127 	.db	120
      001EFB 07                    1128 	.sleb128	7
      001EFC 00 00 8E 52           1129 	.dw	0,(Sstm8s_gpio$GPIO_Init$46)
      001F00 00 00 8E 76           1130 	.dw	0,(Sstm8s_gpio$GPIO_Init$60)
      001F04 00 02                 1131 	.dw	2
      001F06 78                    1132 	.db	120
      001F07 06                    1133 	.sleb128	6
      001F08 00 00 8E 4D           1134 	.dw	0,(Sstm8s_gpio$GPIO_Init$45)
      001F0C 00 00 8E 52           1135 	.dw	0,(Sstm8s_gpio$GPIO_Init$46)
      001F10 00 02                 1136 	.dw	2
      001F12 78                    1137 	.db	120
      001F13 07                    1138 	.sleb128	7
      001F14 00 00 8E 3C           1139 	.dw	0,(Sstm8s_gpio$GPIO_Init$39)
      001F18 00 00 8E 4D           1140 	.dw	0,(Sstm8s_gpio$GPIO_Init$45)
      001F1C 00 02                 1141 	.dw	2
      001F1E 78                    1142 	.db	120
      001F1F 06                    1143 	.sleb128	6
      001F20 00 00 8E 36           1144 	.dw	0,(Sstm8s_gpio$GPIO_Init$38)
      001F24 00 00 8E 3C           1145 	.dw	0,(Sstm8s_gpio$GPIO_Init$39)
      001F28 00 02                 1146 	.dw	2
      001F2A 78                    1147 	.db	120
      001F2B 07                    1148 	.sleb128	7
      001F2C 00 00 8E 2C           1149 	.dw	0,(Sstm8s_gpio$GPIO_Init$36)
      001F30 00 00 8E 36           1150 	.dw	0,(Sstm8s_gpio$GPIO_Init$38)
      001F34 00 02                 1151 	.dw	2
      001F36 78                    1152 	.db	120
      001F37 06                    1153 	.sleb128	6
      001F38 00 00 8E 27           1154 	.dw	0,(Sstm8s_gpio$GPIO_Init$35)
      001F3C 00 00 8E 2C           1155 	.dw	0,(Sstm8s_gpio$GPIO_Init$36)
      001F40 00 02                 1156 	.dw	2
      001F42 78                    1157 	.db	120
      001F43 0C                    1158 	.sleb128	12
      001F44 00 00 8E 25           1159 	.dw	0,(Sstm8s_gpio$GPIO_Init$34)
      001F48 00 00 8E 27           1160 	.dw	0,(Sstm8s_gpio$GPIO_Init$35)
      001F4C 00 02                 1161 	.dw	2
      001F4E 78                    1162 	.db	120
      001F4F 0B                    1163 	.sleb128	11
      001F50 00 00 8E 23           1164 	.dw	0,(Sstm8s_gpio$GPIO_Init$33)
      001F54 00 00 8E 25           1165 	.dw	0,(Sstm8s_gpio$GPIO_Init$34)
      001F58 00 02                 1166 	.dw	2
      001F5A 78                    1167 	.db	120
      001F5B 0A                    1168 	.sleb128	10
      001F5C 00 00 8E 21           1169 	.dw	0,(Sstm8s_gpio$GPIO_Init$32)
      001F60 00 00 8E 23           1170 	.dw	0,(Sstm8s_gpio$GPIO_Init$33)
      001F64 00 02                 1171 	.dw	2
      001F66 78                    1172 	.db	120
      001F67 09                    1173 	.sleb128	9
      001F68 00 00 8E 1F           1174 	.dw	0,(Sstm8s_gpio$GPIO_Init$31)
      001F6C 00 00 8E 21           1175 	.dw	0,(Sstm8s_gpio$GPIO_Init$32)
      001F70 00 02                 1176 	.dw	2
      001F72 78                    1177 	.db	120
      001F73 07                    1178 	.sleb128	7
      001F74 00 00 8E 19           1179 	.dw	0,(Sstm8s_gpio$GPIO_Init$29)
      001F78 00 00 8E 1F           1180 	.dw	0,(Sstm8s_gpio$GPIO_Init$31)
      001F7C 00 02                 1181 	.dw	2
      001F7E 78                    1182 	.db	120
      001F7F 06                    1183 	.sleb128	6
      001F80 00 00 8E 14           1184 	.dw	0,(Sstm8s_gpio$GPIO_Init$28)
      001F84 00 00 8E 19           1185 	.dw	0,(Sstm8s_gpio$GPIO_Init$29)
      001F88 00 02                 1186 	.dw	2
      001F8A 78                    1187 	.db	120
      001F8B 0C                    1188 	.sleb128	12
      001F8C 00 00 8E 12           1189 	.dw	0,(Sstm8s_gpio$GPIO_Init$27)
      001F90 00 00 8E 14           1190 	.dw	0,(Sstm8s_gpio$GPIO_Init$28)
      001F94 00 02                 1191 	.dw	2
      001F96 78                    1192 	.db	120
      001F97 0B                    1193 	.sleb128	11
      001F98 00 00 8E 10           1194 	.dw	0,(Sstm8s_gpio$GPIO_Init$26)
      001F9C 00 00 8E 12           1195 	.dw	0,(Sstm8s_gpio$GPIO_Init$27)
      001FA0 00 02                 1196 	.dw	2
      001FA2 78                    1197 	.db	120
      001FA3 0A                    1198 	.sleb128	10
      001FA4 00 00 8E 0E           1199 	.dw	0,(Sstm8s_gpio$GPIO_Init$25)
      001FA8 00 00 8E 10           1200 	.dw	0,(Sstm8s_gpio$GPIO_Init$26)
      001FAC 00 02                 1201 	.dw	2
      001FAE 78                    1202 	.db	120
      001FAF 09                    1203 	.sleb128	9
      001FB0 00 00 8E 0C           1204 	.dw	0,(Sstm8s_gpio$GPIO_Init$24)
      001FB4 00 00 8E 0E           1205 	.dw	0,(Sstm8s_gpio$GPIO_Init$25)
      001FB8 00 02                 1206 	.dw	2
      001FBA 78                    1207 	.db	120
      001FBB 07                    1208 	.sleb128	7
      001FBC 00 00 8E 0A           1209 	.dw	0,(Sstm8s_gpio$GPIO_Init$23)
      001FC0 00 00 8E 0C           1210 	.dw	0,(Sstm8s_gpio$GPIO_Init$24)
      001FC4 00 02                 1211 	.dw	2
      001FC6 78                    1212 	.db	120
      001FC7 06                    1213 	.sleb128	6
      001FC8 00 00 8E 04           1214 	.dw	0,(Sstm8s_gpio$GPIO_Init$22)
      001FCC 00 00 8E 0A           1215 	.dw	0,(Sstm8s_gpio$GPIO_Init$23)
      001FD0 00 02                 1216 	.dw	2
      001FD2 78                    1217 	.db	120
      001FD3 06                    1218 	.sleb128	6
      001FD4 00 00 8D FE           1219 	.dw	0,(Sstm8s_gpio$GPIO_Init$21)
      001FD8 00 00 8E 04           1220 	.dw	0,(Sstm8s_gpio$GPIO_Init$22)
      001FDC 00 02                 1221 	.dw	2
      001FDE 78                    1222 	.db	120
      001FDF 06                    1223 	.sleb128	6
      001FE0 00 00 8D F8           1224 	.dw	0,(Sstm8s_gpio$GPIO_Init$20)
      001FE4 00 00 8D FE           1225 	.dw	0,(Sstm8s_gpio$GPIO_Init$21)
      001FE8 00 02                 1226 	.dw	2
      001FEA 78                    1227 	.db	120
      001FEB 06                    1228 	.sleb128	6
      001FEC 00 00 8D F2           1229 	.dw	0,(Sstm8s_gpio$GPIO_Init$19)
      001FF0 00 00 8D F8           1230 	.dw	0,(Sstm8s_gpio$GPIO_Init$20)
      001FF4 00 02                 1231 	.dw	2
      001FF6 78                    1232 	.db	120
      001FF7 06                    1233 	.sleb128	6
      001FF8 00 00 8D EC           1234 	.dw	0,(Sstm8s_gpio$GPIO_Init$18)
      001FFC 00 00 8D F2           1235 	.dw	0,(Sstm8s_gpio$GPIO_Init$19)
      002000 00 02                 1236 	.dw	2
      002002 78                    1237 	.db	120
      002003 06                    1238 	.sleb128	6
      002004 00 00 8D E6           1239 	.dw	0,(Sstm8s_gpio$GPIO_Init$17)
      002008 00 00 8D EC           1240 	.dw	0,(Sstm8s_gpio$GPIO_Init$18)
      00200C 00 02                 1241 	.dw	2
      00200E 78                    1242 	.db	120
      00200F 06                    1243 	.sleb128	6
      002010 00 00 8D E0           1244 	.dw	0,(Sstm8s_gpio$GPIO_Init$16)
      002014 00 00 8D E6           1245 	.dw	0,(Sstm8s_gpio$GPIO_Init$17)
      002018 00 02                 1246 	.dw	2
      00201A 78                    1247 	.db	120
      00201B 06                    1248 	.sleb128	6
      00201C 00 00 8D D7           1249 	.dw	0,(Sstm8s_gpio$GPIO_Init$15)
      002020 00 00 8D E0           1250 	.dw	0,(Sstm8s_gpio$GPIO_Init$16)
      002024 00 02                 1251 	.dw	2
      002026 78                    1252 	.db	120
      002027 06                    1253 	.sleb128	6
      002028 00 00 8D CE           1254 	.dw	0,(Sstm8s_gpio$GPIO_Init$14)
      00202C 00 00 8D D7           1255 	.dw	0,(Sstm8s_gpio$GPIO_Init$15)
      002030 00 02                 1256 	.dw	2
      002032 78                    1257 	.db	120
      002033 06                    1258 	.sleb128	6
      002034 00 00 8D C5           1259 	.dw	0,(Sstm8s_gpio$GPIO_Init$13)
      002038 00 00 8D CE           1260 	.dw	0,(Sstm8s_gpio$GPIO_Init$14)
      00203C 00 02                 1261 	.dw	2
      00203E 78                    1262 	.db	120
      00203F 06                    1263 	.sleb128	6
      002040 00 00 8D B5           1264 	.dw	0,(Sstm8s_gpio$GPIO_Init$11)
      002044 00 00 8D C5           1265 	.dw	0,(Sstm8s_gpio$GPIO_Init$13)
      002048 00 02                 1266 	.dw	2
      00204A 78                    1267 	.db	120
      00204B 06                    1268 	.sleb128	6
      00204C 00 00 8D B3           1269 	.dw	0,(Sstm8s_gpio$GPIO_Init$10)
      002050 00 00 8D B5           1270 	.dw	0,(Sstm8s_gpio$GPIO_Init$11)
      002054 00 02                 1271 	.dw	2
      002056 78                    1272 	.db	120
      002057 01                    1273 	.sleb128	1
      002058 00 00 00 00           1274 	.dw	0,0
      00205C 00 00 00 00           1275 	.dw	0,0
      002060 00 00 8D A4           1276 	.dw	0,(Sstm8s_gpio$GPIO_DeInit$1)
      002064 00 00 8D B3           1277 	.dw	0,(Sstm8s_gpio$GPIO_DeInit$8)
      002068 00 02                 1278 	.dw	2
      00206A 78                    1279 	.db	120
      00206B 01                    1280 	.sleb128	1
      00206C 00 00 00 00           1281 	.dw	0,0
      002070 00 00 00 00           1282 	.dw	0,0
                                   1283 
                                   1284 	.area .debug_abbrev (NOLOAD)
      000353                       1285 Ldebug_abbrev:
      000353 06                    1286 	.uleb128	6
      000354 0F                    1287 	.uleb128	15
      000355 00                    1288 	.db	0
      000356 0B                    1289 	.uleb128	11
      000357 0B                    1290 	.uleb128	11
      000358 49                    1291 	.uleb128	73
      000359 13                    1292 	.uleb128	19
      00035A 00                    1293 	.uleb128	0
      00035B 00                    1294 	.uleb128	0
      00035C 04                    1295 	.uleb128	4
      00035D 35                    1296 	.uleb128	53
      00035E 00                    1297 	.db	0
      00035F 49                    1298 	.uleb128	73
      000360 13                    1299 	.uleb128	19
      000361 00                    1300 	.uleb128	0
      000362 00                    1301 	.uleb128	0
      000363 07                    1302 	.uleb128	7
      000364 05                    1303 	.uleb128	5
      000365 00                    1304 	.db	0
      000366 02                    1305 	.uleb128	2
      000367 0A                    1306 	.uleb128	10
      000368 03                    1307 	.uleb128	3
      000369 08                    1308 	.uleb128	8
      00036A 49                    1309 	.uleb128	73
      00036B 13                    1310 	.uleb128	19
      00036C 00                    1311 	.uleb128	0
      00036D 00                    1312 	.uleb128	0
      00036E 0D                    1313 	.uleb128	13
      00036F 01                    1314 	.uleb128	1
      000370 01                    1315 	.db	1
      000371 01                    1316 	.uleb128	1
      000372 13                    1317 	.uleb128	19
      000373 0B                    1318 	.uleb128	11
      000374 0B                    1319 	.uleb128	11
      000375 49                    1320 	.uleb128	73
      000376 13                    1321 	.uleb128	19
      000377 00                    1322 	.uleb128	0
      000378 00                    1323 	.uleb128	0
      000379 02                    1324 	.uleb128	2
      00037A 2E                    1325 	.uleb128	46
      00037B 01                    1326 	.db	1
      00037C 01                    1327 	.uleb128	1
      00037D 13                    1328 	.uleb128	19
      00037E 03                    1329 	.uleb128	3
      00037F 08                    1330 	.uleb128	8
      000380 11                    1331 	.uleb128	17
      000381 01                    1332 	.uleb128	1
      000382 12                    1333 	.uleb128	18
      000383 01                    1334 	.uleb128	1
      000384 3F                    1335 	.uleb128	63
      000385 0C                    1336 	.uleb128	12
      000386 40                    1337 	.uleb128	64
      000387 06                    1338 	.uleb128	6
      000388 00                    1339 	.uleb128	0
      000389 00                    1340 	.uleb128	0
      00038A 0F                    1341 	.uleb128	15
      00038B 34                    1342 	.uleb128	52
      00038C 00                    1343 	.db	0
      00038D 02                    1344 	.uleb128	2
      00038E 0A                    1345 	.uleb128	10
      00038F 03                    1346 	.uleb128	3
      000390 08                    1347 	.uleb128	8
      000391 49                    1348 	.uleb128	73
      000392 13                    1349 	.uleb128	19
      000393 00                    1350 	.uleb128	0
      000394 00                    1351 	.uleb128	0
      000395 0B                    1352 	.uleb128	11
      000396 2E                    1353 	.uleb128	46
      000397 01                    1354 	.db	1
      000398 01                    1355 	.uleb128	1
      000399 13                    1356 	.uleb128	19
      00039A 03                    1357 	.uleb128	3
      00039B 08                    1358 	.uleb128	8
      00039C 11                    1359 	.uleb128	17
      00039D 01                    1360 	.uleb128	1
      00039E 12                    1361 	.uleb128	18
      00039F 01                    1362 	.uleb128	1
      0003A0 3F                    1363 	.uleb128	63
      0003A1 0C                    1364 	.uleb128	12
      0003A2 40                    1365 	.uleb128	64
      0003A3 06                    1366 	.uleb128	6
      0003A4 49                    1367 	.uleb128	73
      0003A5 13                    1368 	.uleb128	19
      0003A6 00                    1369 	.uleb128	0
      0003A7 00                    1370 	.uleb128	0
      0003A8 0C                    1371 	.uleb128	12
      0003A9 26                    1372 	.uleb128	38
      0003AA 00                    1373 	.db	0
      0003AB 49                    1374 	.uleb128	73
      0003AC 13                    1375 	.uleb128	19
      0003AD 00                    1376 	.uleb128	0
      0003AE 00                    1377 	.uleb128	0
      0003AF 01                    1378 	.uleb128	1
      0003B0 11                    1379 	.uleb128	17
      0003B1 01                    1380 	.db	1
      0003B2 03                    1381 	.uleb128	3
      0003B3 08                    1382 	.uleb128	8
      0003B4 10                    1383 	.uleb128	16
      0003B5 06                    1384 	.uleb128	6
      0003B6 13                    1385 	.uleb128	19
      0003B7 0B                    1386 	.uleb128	11
      0003B8 25                    1387 	.uleb128	37
      0003B9 08                    1388 	.uleb128	8
      0003BA 00                    1389 	.uleb128	0
      0003BB 00                    1390 	.uleb128	0
      0003BC 05                    1391 	.uleb128	5
      0003BD 0D                    1392 	.uleb128	13
      0003BE 00                    1393 	.db	0
      0003BF 03                    1394 	.uleb128	3
      0003C0 08                    1395 	.uleb128	8
      0003C1 38                    1396 	.uleb128	56
      0003C2 0A                    1397 	.uleb128	10
      0003C3 49                    1398 	.uleb128	73
      0003C4 13                    1399 	.uleb128	19
      0003C5 00                    1400 	.uleb128	0
      0003C6 00                    1401 	.uleb128	0
      0003C7 0A                    1402 	.uleb128	10
      0003C8 0B                    1403 	.uleb128	11
      0003C9 00                    1404 	.db	0
      0003CA 11                    1405 	.uleb128	17
      0003CB 01                    1406 	.uleb128	1
      0003CC 12                    1407 	.uleb128	18
      0003CD 01                    1408 	.uleb128	1
      0003CE 00                    1409 	.uleb128	0
      0003CF 00                    1410 	.uleb128	0
      0003D0 09                    1411 	.uleb128	9
      0003D1 0B                    1412 	.uleb128	11
      0003D2 01                    1413 	.db	1
      0003D3 01                    1414 	.uleb128	1
      0003D4 13                    1415 	.uleb128	19
      0003D5 11                    1416 	.uleb128	17
      0003D6 01                    1417 	.uleb128	1
      0003D7 12                    1418 	.uleb128	18
      0003D8 01                    1419 	.uleb128	1
      0003D9 00                    1420 	.uleb128	0
      0003DA 00                    1421 	.uleb128	0
      0003DB 0E                    1422 	.uleb128	14
      0003DC 21                    1423 	.uleb128	33
      0003DD 00                    1424 	.db	0
      0003DE 2F                    1425 	.uleb128	47
      0003DF 0B                    1426 	.uleb128	11
      0003E0 00                    1427 	.uleb128	0
      0003E1 00                    1428 	.uleb128	0
      0003E2 03                    1429 	.uleb128	3
      0003E3 13                    1430 	.uleb128	19
      0003E4 01                    1431 	.db	1
      0003E5 01                    1432 	.uleb128	1
      0003E6 13                    1433 	.uleb128	19
      0003E7 03                    1434 	.uleb128	3
      0003E8 08                    1435 	.uleb128	8
      0003E9 0B                    1436 	.uleb128	11
      0003EA 0B                    1437 	.uleb128	11
      0003EB 00                    1438 	.uleb128	0
      0003EC 00                    1439 	.uleb128	0
      0003ED 08                    1440 	.uleb128	8
      0003EE 24                    1441 	.uleb128	36
      0003EF 00                    1442 	.db	0
      0003F0 03                    1443 	.uleb128	3
      0003F1 08                    1444 	.uleb128	8
      0003F2 0B                    1445 	.uleb128	11
      0003F3 0B                    1446 	.uleb128	11
      0003F4 3E                    1447 	.uleb128	62
      0003F5 0B                    1448 	.uleb128	11
      0003F6 00                    1449 	.uleb128	0
      0003F7 00                    1450 	.uleb128	0
      0003F8 00                    1451 	.uleb128	0
                                   1452 
                                   1453 	.area .debug_info (NOLOAD)
      0013E8 00 00 03 D5           1454 	.dw	0,Ldebug_info_end-Ldebug_info_start
      0013EC                       1455 Ldebug_info_start:
      0013EC 00 02                 1456 	.dw	2
      0013EE 00 00 03 53           1457 	.dw	0,(Ldebug_abbrev)
      0013F2 04                    1458 	.db	4
      0013F3 01                    1459 	.uleb128	1
      0013F4 64 72 69 76 65 72 73  1460 	.ascii "drivers/src/stm8s_gpio.c"
             2F 73 72 63 2F 73 74
             6D 38 73 5F 67 70 69
             6F 2E 63
      00140C 00                    1461 	.db	0
      00140D 00 00 10 E2           1462 	.dw	0,(Ldebug_line_start+-4)
      001411 01                    1463 	.db	1
      001412 53 44 43 43 20 76 65  1464 	.ascii "SDCC version 4.1.0 #12072"
             72 73 69 6F 6E 20 34
             2E 31 2E 30 20 23 31
             32 30 37 32
      00142B 00                    1465 	.db	0
      00142C 02                    1466 	.uleb128	2
      00142D 00 00 00 CB           1467 	.dw	0,203
      001431 47 50 49 4F 5F 44 65  1468 	.ascii "GPIO_DeInit"
             49 6E 69 74
      00143C 00                    1469 	.db	0
      00143D 00 00 8D A4           1470 	.dw	0,(_GPIO_DeInit)
      001441 00 00 8D B3           1471 	.dw	0,(XG$GPIO_DeInit$0$0+1)
      001445 01                    1472 	.db	1
      001446 00 00 20 60           1473 	.dw	0,(Ldebug_loc_start+804)
      00144A 03                    1474 	.uleb128	3
      00144B 00 00 00 B6           1475 	.dw	0,182
      00144F 47 50 49 4F 5F 73 74  1476 	.ascii "GPIO_struct"
             72 75 63 74
      00145A 00                    1477 	.db	0
      00145B 05                    1478 	.db	5
      00145C 04                    1479 	.uleb128	4
      00145D 00 00 00 CB           1480 	.dw	0,203
      001461 05                    1481 	.uleb128	5
      001462 4F 44 52              1482 	.ascii "ODR"
      001465 00                    1483 	.db	0
      001466 02                    1484 	.db	2
      001467 23                    1485 	.db	35
      001468 00                    1486 	.uleb128	0
      001469 00 00 00 74           1487 	.dw	0,116
      00146D 05                    1488 	.uleb128	5
      00146E 49 44 52              1489 	.ascii "IDR"
      001471 00                    1490 	.db	0
      001472 02                    1491 	.db	2
      001473 23                    1492 	.db	35
      001474 01                    1493 	.uleb128	1
      001475 00 00 00 74           1494 	.dw	0,116
      001479 05                    1495 	.uleb128	5
      00147A 44 44 52              1496 	.ascii "DDR"
      00147D 00                    1497 	.db	0
      00147E 02                    1498 	.db	2
      00147F 23                    1499 	.db	35
      001480 02                    1500 	.uleb128	2
      001481 00 00 00 74           1501 	.dw	0,116
      001485 05                    1502 	.uleb128	5
      001486 43 52 31              1503 	.ascii "CR1"
      001489 00                    1504 	.db	0
      00148A 02                    1505 	.db	2
      00148B 23                    1506 	.db	35
      00148C 03                    1507 	.uleb128	3
      00148D 00 00 00 74           1508 	.dw	0,116
      001491 05                    1509 	.uleb128	5
      001492 43 52 32              1510 	.ascii "CR2"
      001495 00                    1511 	.db	0
      001496 02                    1512 	.db	2
      001497 23                    1513 	.db	35
      001498 04                    1514 	.uleb128	4
      001499 00 00 00 74           1515 	.dw	0,116
      00149D 00                    1516 	.uleb128	0
      00149E 06                    1517 	.uleb128	6
      00149F 02                    1518 	.db	2
      0014A0 00 00 00 62           1519 	.dw	0,98
      0014A4 07                    1520 	.uleb128	7
      0014A5 02                    1521 	.db	2
      0014A6 91                    1522 	.db	145
      0014A7 02                    1523 	.sleb128	2
      0014A8 47 50 49 4F 78        1524 	.ascii "GPIOx"
      0014AD 00                    1525 	.db	0
      0014AE 00 00 00 B6           1526 	.dw	0,182
      0014B2 00                    1527 	.uleb128	0
      0014B3 08                    1528 	.uleb128	8
      0014B4 75 6E 73 69 67 6E 65  1529 	.ascii "unsigned char"
             64 20 63 68 61 72
      0014C1 00                    1530 	.db	0
      0014C2 01                    1531 	.db	1
      0014C3 08                    1532 	.db	8
      0014C4 02                    1533 	.uleb128	2
      0014C5 00 00 01 77           1534 	.dw	0,375
      0014C9 47 50 49 4F 5F 49 6E  1535 	.ascii "GPIO_Init"
             69 74
      0014D2 00                    1536 	.db	0
      0014D3 00 00 8D B3           1537 	.dw	0,(_GPIO_Init)
      0014D7 00 00 8E 9F           1538 	.dw	0,(XG$GPIO_Init$0$0+1)
      0014DB 01                    1539 	.db	1
      0014DC 00 00 1E C0           1540 	.dw	0,(Ldebug_loc_start+388)
      0014E0 07                    1541 	.uleb128	7
      0014E1 02                    1542 	.db	2
      0014E2 91                    1543 	.db	145
      0014E3 02                    1544 	.sleb128	2
      0014E4 47 50 49 4F 78        1545 	.ascii "GPIOx"
      0014E9 00                    1546 	.db	0
      0014EA 00 00 00 B6           1547 	.dw	0,182
      0014EE 07                    1548 	.uleb128	7
      0014EF 02                    1549 	.db	2
      0014F0 91                    1550 	.db	145
      0014F1 04                    1551 	.sleb128	4
      0014F2 47 50 49 4F 5F 50 69  1552 	.ascii "GPIO_Pin"
             6E
      0014FA 00                    1553 	.db	0
      0014FB 00 00 01 77           1554 	.dw	0,375
      0014FF 07                    1555 	.uleb128	7
      001500 02                    1556 	.db	2
      001501 91                    1557 	.db	145
      001502 05                    1558 	.sleb128	5
      001503 47 50 49 4F 5F 4D 6F  1559 	.ascii "GPIO_Mode"
             64 65
      00150C 00                    1560 	.db	0
      00150D 00 00 01 77           1561 	.dw	0,375
      001511 09                    1562 	.uleb128	9
      001512 00 00 01 49           1563 	.dw	0,329
      001516 00 00 8E 4C           1564 	.dw	0,(Sstm8s_gpio$GPIO_Init$43)
      00151A 00 00 8E 66           1565 	.dw	0,(Sstm8s_gpio$GPIO_Init$54)
      00151E 0A                    1566 	.uleb128	10
      00151F 00 00 8E 54           1567 	.dw	0,(Sstm8s_gpio$GPIO_Init$47)
      001523 00 00 8E 58           1568 	.dw	0,(Sstm8s_gpio$GPIO_Init$49)
      001527 0A                    1569 	.uleb128	10
      001528 00 00 8E 5A           1570 	.dw	0,(Sstm8s_gpio$GPIO_Init$50)
      00152C 00 00 8E 5E           1571 	.dw	0,(Sstm8s_gpio$GPIO_Init$52)
      001530 00                    1572 	.uleb128	0
      001531 0A                    1573 	.uleb128	10
      001532 00 00 8E 68           1574 	.dw	0,(Sstm8s_gpio$GPIO_Init$55)
      001536 00 00 8E 70           1575 	.dw	0,(Sstm8s_gpio$GPIO_Init$57)
      00153A 0A                    1576 	.uleb128	10
      00153B 00 00 8E 7D           1577 	.dw	0,(Sstm8s_gpio$GPIO_Init$62)
      00153F 00 00 8E 80           1578 	.dw	0,(Sstm8s_gpio$GPIO_Init$64)
      001543 0A                    1579 	.uleb128	10
      001544 00 00 8E 82           1580 	.dw	0,(Sstm8s_gpio$GPIO_Init$65)
      001548 00 00 8E 85           1581 	.dw	0,(Sstm8s_gpio$GPIO_Init$67)
      00154C 0A                    1582 	.uleb128	10
      00154D 00 00 8E 90           1583 	.dw	0,(Sstm8s_gpio$GPIO_Init$72)
      001551 00 00 8E 95           1584 	.dw	0,(Sstm8s_gpio$GPIO_Init$74)
      001555 0A                    1585 	.uleb128	10
      001556 00 00 8E 97           1586 	.dw	0,(Sstm8s_gpio$GPIO_Init$75)
      00155A 00 00 8E 9C           1587 	.dw	0,(Sstm8s_gpio$GPIO_Init$77)
      00155E 00                    1588 	.uleb128	0
      00155F 08                    1589 	.uleb128	8
      001560 75 6E 73 69 67 6E 65  1590 	.ascii "unsigned char"
             64 20 63 68 61 72
      00156D 00                    1591 	.db	0
      00156E 01                    1592 	.db	1
      00156F 08                    1593 	.db	8
      001570 02                    1594 	.uleb128	2
      001571 00 00 01 C4           1595 	.dw	0,452
      001575 47 50 49 4F 5F 57 72  1596 	.ascii "GPIO_Write"
             69 74 65
      00157F 00                    1597 	.db	0
      001580 00 00 8E 9F           1598 	.dw	0,(_GPIO_Write)
      001584 00 00 8E A5           1599 	.dw	0,(XG$GPIO_Write$0$0+1)
      001588 01                    1600 	.db	1
      001589 00 00 1E AC           1601 	.dw	0,(Ldebug_loc_start+368)
      00158D 07                    1602 	.uleb128	7
      00158E 02                    1603 	.db	2
      00158F 91                    1604 	.db	145
      001590 02                    1605 	.sleb128	2
      001591 47 50 49 4F 78        1606 	.ascii "GPIOx"
      001596 00                    1607 	.db	0
      001597 00 00 00 B6           1608 	.dw	0,182
      00159B 07                    1609 	.uleb128	7
      00159C 02                    1610 	.db	2
      00159D 91                    1611 	.db	145
      00159E 04                    1612 	.sleb128	4
      00159F 50 6F 72 74 56 61 6C  1613 	.ascii "PortVal"
      0015A6 00                    1614 	.db	0
      0015A7 00 00 01 77           1615 	.dw	0,375
      0015AB 00                    1616 	.uleb128	0
      0015AC 02                    1617 	.uleb128	2
      0015AD 00 00 02 05           1618 	.dw	0,517
      0015B1 47 50 49 4F 5F 57 72  1619 	.ascii "GPIO_WriteHigh"
             69 74 65 48 69 67 68
      0015BF 00                    1620 	.db	0
      0015C0 00 00 8E A5           1621 	.dw	0,(_GPIO_WriteHigh)
      0015C4 00 00 8E AC           1622 	.dw	0,(XG$GPIO_WriteHigh$0$0+1)
      0015C8 01                    1623 	.db	1
      0015C9 00 00 1E 98           1624 	.dw	0,(Ldebug_loc_start+348)
      0015CD 07                    1625 	.uleb128	7
      0015CE 02                    1626 	.db	2
      0015CF 91                    1627 	.db	145
      0015D0 02                    1628 	.sleb128	2
      0015D1 47 50 49 4F 78        1629 	.ascii "GPIOx"
      0015D6 00                    1630 	.db	0
      0015D7 00 00 00 B6           1631 	.dw	0,182
      0015DB 07                    1632 	.uleb128	7
      0015DC 02                    1633 	.db	2
      0015DD 91                    1634 	.db	145
      0015DE 04                    1635 	.sleb128	4
      0015DF 50 6F 72 74 50 69 6E  1636 	.ascii "PortPins"
             73
      0015E7 00                    1637 	.db	0
      0015E8 00 00 01 77           1638 	.dw	0,375
      0015EC 00                    1639 	.uleb128	0
      0015ED 02                    1640 	.uleb128	2
      0015EE 00 00 02 45           1641 	.dw	0,581
      0015F2 47 50 49 4F 5F 57 72  1642 	.ascii "GPIO_WriteLow"
             69 74 65 4C 6F 77
      0015FF 00                    1643 	.db	0
      001600 00 00 8E AC           1644 	.dw	0,(_GPIO_WriteLow)
      001604 00 00 8E BA           1645 	.dw	0,(XG$GPIO_WriteLow$0$0+1)
      001608 01                    1646 	.db	1
      001609 00 00 1E 6C           1647 	.dw	0,(Ldebug_loc_start+304)
      00160D 07                    1648 	.uleb128	7
      00160E 02                    1649 	.db	2
      00160F 91                    1650 	.db	145
      001610 02                    1651 	.sleb128	2
      001611 47 50 49 4F 78        1652 	.ascii "GPIOx"
      001616 00                    1653 	.db	0
      001617 00 00 00 B6           1654 	.dw	0,182
      00161B 07                    1655 	.uleb128	7
      00161C 02                    1656 	.db	2
      00161D 91                    1657 	.db	145
      00161E 04                    1658 	.sleb128	4
      00161F 50 6F 72 74 50 69 6E  1659 	.ascii "PortPins"
             73
      001627 00                    1660 	.db	0
      001628 00 00 01 77           1661 	.dw	0,375
      00162C 00                    1662 	.uleb128	0
      00162D 02                    1663 	.uleb128	2
      00162E 00 00 02 89           1664 	.dw	0,649
      001632 47 50 49 4F 5F 57 72  1665 	.ascii "GPIO_WriteReverse"
             69 74 65 52 65 76 65
             72 73 65
      001643 00                    1666 	.db	0
      001644 00 00 8E BA           1667 	.dw	0,(_GPIO_WriteReverse)
      001648 00 00 8E C1           1668 	.dw	0,(XG$GPIO_WriteReverse$0$0+1)
      00164C 01                    1669 	.db	1
      00164D 00 00 1E 58           1670 	.dw	0,(Ldebug_loc_start+284)
      001651 07                    1671 	.uleb128	7
      001652 02                    1672 	.db	2
      001653 91                    1673 	.db	145
      001654 02                    1674 	.sleb128	2
      001655 47 50 49 4F 78        1675 	.ascii "GPIOx"
      00165A 00                    1676 	.db	0
      00165B 00 00 00 B6           1677 	.dw	0,182
      00165F 07                    1678 	.uleb128	7
      001660 02                    1679 	.db	2
      001661 91                    1680 	.db	145
      001662 04                    1681 	.sleb128	4
      001663 50 6F 72 74 50 69 6E  1682 	.ascii "PortPins"
             73
      00166B 00                    1683 	.db	0
      00166C 00 00 01 77           1684 	.dw	0,375
      001670 00                    1685 	.uleb128	0
      001671 0B                    1686 	.uleb128	11
      001672 00 00 02 C2           1687 	.dw	0,706
      001676 47 50 49 4F 5F 52 65  1688 	.ascii "GPIO_ReadOutputData"
             61 64 4F 75 74 70 75
             74 44 61 74 61
      001689 00                    1689 	.db	0
      00168A 00 00 8E C1           1690 	.dw	0,(_GPIO_ReadOutputData)
      00168E 00 00 8E C5           1691 	.dw	0,(XG$GPIO_ReadOutputData$0$0+1)
      001692 01                    1692 	.db	1
      001693 00 00 1E 44           1693 	.dw	0,(Ldebug_loc_start+264)
      001697 00 00 01 77           1694 	.dw	0,375
      00169B 07                    1695 	.uleb128	7
      00169C 02                    1696 	.db	2
      00169D 91                    1697 	.db	145
      00169E 02                    1698 	.sleb128	2
      00169F 47 50 49 4F 78        1699 	.ascii "GPIOx"
      0016A4 00                    1700 	.db	0
      0016A5 00 00 00 B6           1701 	.dw	0,182
      0016A9 00                    1702 	.uleb128	0
      0016AA 0B                    1703 	.uleb128	11
      0016AB 00 00 02 FA           1704 	.dw	0,762
      0016AF 47 50 49 4F 5F 52 65  1705 	.ascii "GPIO_ReadInputData"
             61 64 49 6E 70 75 74
             44 61 74 61
      0016C1 00                    1706 	.db	0
      0016C2 00 00 8E C5           1707 	.dw	0,(_GPIO_ReadInputData)
      0016C6 00 00 8E CA           1708 	.dw	0,(XG$GPIO_ReadInputData$0$0+1)
      0016CA 01                    1709 	.db	1
      0016CB 00 00 1E 30           1710 	.dw	0,(Ldebug_loc_start+244)
      0016CF 00 00 01 77           1711 	.dw	0,375
      0016D3 07                    1712 	.uleb128	7
      0016D4 02                    1713 	.db	2
      0016D5 91                    1714 	.db	145
      0016D6 02                    1715 	.sleb128	2
      0016D7 47 50 49 4F 78        1716 	.ascii "GPIOx"
      0016DC 00                    1717 	.db	0
      0016DD 00 00 00 B6           1718 	.dw	0,182
      0016E1 00                    1719 	.uleb128	0
      0016E2 0B                    1720 	.uleb128	11
      0016E3 00 00 03 42           1721 	.dw	0,834
      0016E7 47 50 49 4F 5F 52 65  1722 	.ascii "GPIO_ReadInputPin"
             61 64 49 6E 70 75 74
             50 69 6E
      0016F8 00                    1723 	.db	0
      0016F9 00 00 8E CA           1724 	.dw	0,(_GPIO_ReadInputPin)
      0016FD 00 00 8E D1           1725 	.dw	0,(XG$GPIO_ReadInputPin$0$0+1)
      001701 01                    1726 	.db	1
      001702 00 00 1E 1C           1727 	.dw	0,(Ldebug_loc_start+224)
      001706 00 00 01 77           1728 	.dw	0,375
      00170A 07                    1729 	.uleb128	7
      00170B 02                    1730 	.db	2
      00170C 91                    1731 	.db	145
      00170D 02                    1732 	.sleb128	2
      00170E 47 50 49 4F 78        1733 	.ascii "GPIOx"
      001713 00                    1734 	.db	0
      001714 00 00 00 B6           1735 	.dw	0,182
      001718 07                    1736 	.uleb128	7
      001719 02                    1737 	.db	2
      00171A 91                    1738 	.db	145
      00171B 04                    1739 	.sleb128	4
      00171C 47 50 49 4F 5F 50 69  1740 	.ascii "GPIO_Pin"
             6E
      001724 00                    1741 	.db	0
      001725 00 00 01 77           1742 	.dw	0,375
      001729 00                    1743 	.uleb128	0
      00172A 02                    1744 	.uleb128	2
      00172B 00 00 03 B1           1745 	.dw	0,945
      00172F 47 50 49 4F 5F 45 78  1746 	.ascii "GPIO_ExternalPullUpConfig"
             74 65 72 6E 61 6C 50
             75 6C 6C 55 70 43 6F
             6E 66 69 67
      001748 00                    1747 	.db	0
      001749 00 00 8E D1           1748 	.dw	0,(_GPIO_ExternalPullUpConfig)
      00174D 00 00 8F 18           1749 	.dw	0,(XG$GPIO_ExternalPullUpConfig$0$0+1)
      001751 01                    1750 	.db	1
      001752 00 00 1D 3C           1751 	.dw	0,(Ldebug_loc_start)
      001756 07                    1752 	.uleb128	7
      001757 02                    1753 	.db	2
      001758 91                    1754 	.db	145
      001759 02                    1755 	.sleb128	2
      00175A 47 50 49 4F 78        1756 	.ascii "GPIOx"
      00175F 00                    1757 	.db	0
      001760 00 00 00 B6           1758 	.dw	0,182
      001764 07                    1759 	.uleb128	7
      001765 02                    1760 	.db	2
      001766 91                    1761 	.db	145
      001767 04                    1762 	.sleb128	4
      001768 47 50 49 4F 5F 50 69  1763 	.ascii "GPIO_Pin"
             6E
      001770 00                    1764 	.db	0
      001771 00 00 01 77           1765 	.dw	0,375
      001775 07                    1766 	.uleb128	7
      001776 02                    1767 	.db	2
      001777 91                    1768 	.db	145
      001778 05                    1769 	.sleb128	5
      001779 4E 65 77 53 74 61 74  1770 	.ascii "NewState"
             65
      001781 00                    1771 	.db	0
      001782 00 00 01 77           1772 	.dw	0,375
      001786 0A                    1773 	.uleb128	10
      001787 00 00 8F 07           1774 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$146)
      00178B 00 00 8F 0A           1775 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$148)
      00178F 0A                    1776 	.uleb128	10
      001790 00 00 8F 0C           1777 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$149)
      001794 00 00 8F 16           1778 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$153)
      001798 00                    1779 	.uleb128	0
      001799 0C                    1780 	.uleb128	12
      00179A 00 00 01 77           1781 	.dw	0,375
      00179E 0D                    1782 	.uleb128	13
      00179F 00 00 03 C3           1783 	.dw	0,963
      0017A3 19                    1784 	.db	25
      0017A4 00 00 03 B1           1785 	.dw	0,945
      0017A8 0E                    1786 	.uleb128	14
      0017A9 18                    1787 	.db	24
      0017AA 00                    1788 	.uleb128	0
      0017AB 0F                    1789 	.uleb128	15
      0017AC 05                    1790 	.db	5
      0017AD 03                    1791 	.db	3
      0017AE 00 00 81 13           1792 	.dw	0,(___str_0)
      0017B2 5F 5F 73 74 72 5F 30  1793 	.ascii "__str_0"
      0017B9 00                    1794 	.db	0
      0017BA 00 00 03 B6           1795 	.dw	0,950
      0017BE 00                    1796 	.uleb128	0
      0017BF 00                    1797 	.uleb128	0
      0017C0 00                    1798 	.uleb128	0
      0017C1                       1799 Ldebug_info_end:
                                   1800 
                                   1801 	.area .debug_pubnames (NOLOAD)
      000668 00 00 00 D9           1802 	.dw	0,Ldebug_pubnames_end-Ldebug_pubnames_start
      00066C                       1803 Ldebug_pubnames_start:
      00066C 00 02                 1804 	.dw	2
      00066E 00 00 13 E8           1805 	.dw	0,(Ldebug_info_start-4)
      000672 00 00 03 D9           1806 	.dw	0,4+Ldebug_info_end-Ldebug_info_start
      000676 00 00 00 44           1807 	.dw	0,68
      00067A 47 50 49 4F 5F 44 65  1808 	.ascii "GPIO_DeInit"
             49 6E 69 74
      000685 00                    1809 	.db	0
      000686 00 00 00 DC           1810 	.dw	0,220
      00068A 47 50 49 4F 5F 49 6E  1811 	.ascii "GPIO_Init"
             69 74
      000693 00                    1812 	.db	0
      000694 00 00 01 88           1813 	.dw	0,392
      000698 47 50 49 4F 5F 57 72  1814 	.ascii "GPIO_Write"
             69 74 65
      0006A2 00                    1815 	.db	0
      0006A3 00 00 01 C4           1816 	.dw	0,452
      0006A7 47 50 49 4F 5F 57 72  1817 	.ascii "GPIO_WriteHigh"
             69 74 65 48 69 67 68
      0006B5 00                    1818 	.db	0
      0006B6 00 00 02 05           1819 	.dw	0,517
      0006BA 47 50 49 4F 5F 57 72  1820 	.ascii "GPIO_WriteLow"
             69 74 65 4C 6F 77
      0006C7 00                    1821 	.db	0
      0006C8 00 00 02 45           1822 	.dw	0,581
      0006CC 47 50 49 4F 5F 57 72  1823 	.ascii "GPIO_WriteReverse"
             69 74 65 52 65 76 65
             72 73 65
      0006DD 00                    1824 	.db	0
      0006DE 00 00 02 89           1825 	.dw	0,649
      0006E2 47 50 49 4F 5F 52 65  1826 	.ascii "GPIO_ReadOutputData"
             61 64 4F 75 74 70 75
             74 44 61 74 61
      0006F5 00                    1827 	.db	0
      0006F6 00 00 02 C2           1828 	.dw	0,706
      0006FA 47 50 49 4F 5F 52 65  1829 	.ascii "GPIO_ReadInputData"
             61 64 49 6E 70 75 74
             44 61 74 61
      00070C 00                    1830 	.db	0
      00070D 00 00 02 FA           1831 	.dw	0,762
      000711 47 50 49 4F 5F 52 65  1832 	.ascii "GPIO_ReadInputPin"
             61 64 49 6E 70 75 74
             50 69 6E
      000722 00                    1833 	.db	0
      000723 00 00 03 42           1834 	.dw	0,834
      000727 47 50 49 4F 5F 45 78  1835 	.ascii "GPIO_ExternalPullUpConfig"
             74 65 72 6E 61 6C 50
             75 6C 6C 55 70 43 6F
             6E 66 69 67
      000740 00                    1836 	.db	0
      000741 00 00 00 00           1837 	.dw	0,0
      000745                       1838 Ldebug_pubnames_end:
                                   1839 
                                   1840 	.area .debug_frame (NOLOAD)
      001863 00 00                 1841 	.dw	0
      001865 00 0E                 1842 	.dw	Ldebug_CIE0_end-Ldebug_CIE0_start
      001867                       1843 Ldebug_CIE0_start:
      001867 FF FF                 1844 	.dw	0xffff
      001869 FF FF                 1845 	.dw	0xffff
      00186B 01                    1846 	.db	1
      00186C 00                    1847 	.db	0
      00186D 01                    1848 	.uleb128	1
      00186E 7F                    1849 	.sleb128	-1
      00186F 09                    1850 	.db	9
      001870 0C                    1851 	.db	12
      001871 08                    1852 	.uleb128	8
      001872 02                    1853 	.uleb128	2
      001873 89                    1854 	.db	137
      001874 01                    1855 	.uleb128	1
      001875                       1856 Ldebug_CIE0_end:
      001875 00 00 00 8A           1857 	.dw	0,138
      001879 00 00 18 63           1858 	.dw	0,(Ldebug_CIE0_start-4)
      00187D 00 00 8E D1           1859 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$127)	;initial loc
      001881 00 00 00 47           1860 	.dw	0,Sstm8s_gpio$GPIO_ExternalPullUpConfig$157-Sstm8s_gpio$GPIO_ExternalPullUpConfig$127
      001885 01                    1861 	.db	1
      001886 00 00 8E D1           1862 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$127)
      00188A 0E                    1863 	.db	14
      00188B 02                    1864 	.uleb128	2
      00188C 01                    1865 	.db	1
      00188D 00 00 8E D2           1866 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$128)
      001891 0E                    1867 	.db	14
      001892 03                    1868 	.uleb128	3
      001893 01                    1869 	.db	1
      001894 00 00 8E D8           1870 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$130)
      001898 0E                    1871 	.db	14
      001899 04                    1872 	.uleb128	4
      00189A 01                    1873 	.db	1
      00189B 00 00 8E DA           1874 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$131)
      00189F 0E                    1875 	.db	14
      0018A0 06                    1876 	.uleb128	6
      0018A1 01                    1877 	.db	1
      0018A2 00 00 8E DC           1878 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$132)
      0018A6 0E                    1879 	.db	14
      0018A7 07                    1880 	.uleb128	7
      0018A8 01                    1881 	.db	1
      0018A9 00 00 8E DE           1882 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$133)
      0018AD 0E                    1883 	.db	14
      0018AE 08                    1884 	.uleb128	8
      0018AF 01                    1885 	.db	1
      0018B0 00 00 8E E0           1886 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$134)
      0018B4 0E                    1887 	.db	14
      0018B5 09                    1888 	.uleb128	9
      0018B6 01                    1889 	.db	1
      0018B7 00 00 8E E5           1890 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$135)
      0018BB 0E                    1891 	.db	14
      0018BC 03                    1892 	.uleb128	3
      0018BD 01                    1893 	.db	1
      0018BE 00 00 8E EE           1894 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$137)
      0018C2 0E                    1895 	.db	14
      0018C3 03                    1896 	.uleb128	3
      0018C4 01                    1897 	.db	1
      0018C5 00 00 8E F0           1898 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$138)
      0018C9 0E                    1899 	.db	14
      0018CA 04                    1900 	.uleb128	4
      0018CB 01                    1901 	.db	1
      0018CC 00 00 8E F2           1902 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$139)
      0018D0 0E                    1903 	.db	14
      0018D1 06                    1904 	.uleb128	6
      0018D2 01                    1905 	.db	1
      0018D3 00 00 8E F4           1906 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$140)
      0018D7 0E                    1907 	.db	14
      0018D8 07                    1908 	.uleb128	7
      0018D9 01                    1909 	.db	1
      0018DA 00 00 8E F6           1910 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$141)
      0018DE 0E                    1911 	.db	14
      0018DF 08                    1912 	.uleb128	8
      0018E0 01                    1913 	.db	1
      0018E1 00 00 8E F8           1914 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$142)
      0018E5 0E                    1915 	.db	14
      0018E6 09                    1916 	.uleb128	9
      0018E7 01                    1917 	.db	1
      0018E8 00 00 8E FD           1918 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$143)
      0018EC 0E                    1919 	.db	14
      0018ED 03                    1920 	.uleb128	3
      0018EE 01                    1921 	.db	1
      0018EF 00 00 8F 0D           1922 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$151)
      0018F3 0E                    1923 	.db	14
      0018F4 04                    1924 	.uleb128	4
      0018F5 01                    1925 	.db	1
      0018F6 00 00 8F 13           1926 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$152)
      0018FA 0E                    1927 	.db	14
      0018FB 03                    1928 	.uleb128	3
      0018FC 01                    1929 	.db	1
      0018FD 00 00 8F 17           1930 	.dw	0,(Sstm8s_gpio$GPIO_ExternalPullUpConfig$155)
      001901 0E                    1931 	.db	14
      001902 02                    1932 	.uleb128	2
                                   1933 
                                   1934 	.area .debug_frame (NOLOAD)
      001903 00 00                 1935 	.dw	0
      001905 00 0E                 1936 	.dw	Ldebug_CIE1_end-Ldebug_CIE1_start
      001907                       1937 Ldebug_CIE1_start:
      001907 FF FF                 1938 	.dw	0xffff
      001909 FF FF                 1939 	.dw	0xffff
      00190B 01                    1940 	.db	1
      00190C 00                    1941 	.db	0
      00190D 01                    1942 	.uleb128	1
      00190E 7F                    1943 	.sleb128	-1
      00190F 09                    1944 	.db	9
      001910 0C                    1945 	.db	12
      001911 08                    1946 	.uleb128	8
      001912 02                    1947 	.uleb128	2
      001913 89                    1948 	.db	137
      001914 01                    1949 	.uleb128	1
      001915                       1950 Ldebug_CIE1_end:
      001915 00 00 00 13           1951 	.dw	0,19
      001919 00 00 19 03           1952 	.dw	0,(Ldebug_CIE1_start-4)
      00191D 00 00 8E CA           1953 	.dw	0,(Sstm8s_gpio$GPIO_ReadInputPin$121)	;initial loc
      001921 00 00 00 07           1954 	.dw	0,Sstm8s_gpio$GPIO_ReadInputPin$125-Sstm8s_gpio$GPIO_ReadInputPin$121
      001925 01                    1955 	.db	1
      001926 00 00 8E CA           1956 	.dw	0,(Sstm8s_gpio$GPIO_ReadInputPin$121)
      00192A 0E                    1957 	.db	14
      00192B 02                    1958 	.uleb128	2
                                   1959 
                                   1960 	.area .debug_frame (NOLOAD)
      00192C 00 00                 1961 	.dw	0
      00192E 00 0E                 1962 	.dw	Ldebug_CIE2_end-Ldebug_CIE2_start
      001930                       1963 Ldebug_CIE2_start:
      001930 FF FF                 1964 	.dw	0xffff
      001932 FF FF                 1965 	.dw	0xffff
      001934 01                    1966 	.db	1
      001935 00                    1967 	.db	0
      001936 01                    1968 	.uleb128	1
      001937 7F                    1969 	.sleb128	-1
      001938 09                    1970 	.db	9
      001939 0C                    1971 	.db	12
      00193A 08                    1972 	.uleb128	8
      00193B 02                    1973 	.uleb128	2
      00193C 89                    1974 	.db	137
      00193D 01                    1975 	.uleb128	1
      00193E                       1976 Ldebug_CIE2_end:
      00193E 00 00 00 13           1977 	.dw	0,19
      001942 00 00 19 2C           1978 	.dw	0,(Ldebug_CIE2_start-4)
      001946 00 00 8E C5           1979 	.dw	0,(Sstm8s_gpio$GPIO_ReadInputData$115)	;initial loc
      00194A 00 00 00 05           1980 	.dw	0,Sstm8s_gpio$GPIO_ReadInputData$119-Sstm8s_gpio$GPIO_ReadInputData$115
      00194E 01                    1981 	.db	1
      00194F 00 00 8E C5           1982 	.dw	0,(Sstm8s_gpio$GPIO_ReadInputData$115)
      001953 0E                    1983 	.db	14
      001954 02                    1984 	.uleb128	2
                                   1985 
                                   1986 	.area .debug_frame (NOLOAD)
      001955 00 00                 1987 	.dw	0
      001957 00 0E                 1988 	.dw	Ldebug_CIE3_end-Ldebug_CIE3_start
      001959                       1989 Ldebug_CIE3_start:
      001959 FF FF                 1990 	.dw	0xffff
      00195B FF FF                 1991 	.dw	0xffff
      00195D 01                    1992 	.db	1
      00195E 00                    1993 	.db	0
      00195F 01                    1994 	.uleb128	1
      001960 7F                    1995 	.sleb128	-1
      001961 09                    1996 	.db	9
      001962 0C                    1997 	.db	12
      001963 08                    1998 	.uleb128	8
      001964 02                    1999 	.uleb128	2
      001965 89                    2000 	.db	137
      001966 01                    2001 	.uleb128	1
      001967                       2002 Ldebug_CIE3_end:
      001967 00 00 00 13           2003 	.dw	0,19
      00196B 00 00 19 55           2004 	.dw	0,(Ldebug_CIE3_start-4)
      00196F 00 00 8E C1           2005 	.dw	0,(Sstm8s_gpio$GPIO_ReadOutputData$109)	;initial loc
      001973 00 00 00 04           2006 	.dw	0,Sstm8s_gpio$GPIO_ReadOutputData$113-Sstm8s_gpio$GPIO_ReadOutputData$109
      001977 01                    2007 	.db	1
      001978 00 00 8E C1           2008 	.dw	0,(Sstm8s_gpio$GPIO_ReadOutputData$109)
      00197C 0E                    2009 	.db	14
      00197D 02                    2010 	.uleb128	2
                                   2011 
                                   2012 	.area .debug_frame (NOLOAD)
      00197E 00 00                 2013 	.dw	0
      001980 00 0E                 2014 	.dw	Ldebug_CIE4_end-Ldebug_CIE4_start
      001982                       2015 Ldebug_CIE4_start:
      001982 FF FF                 2016 	.dw	0xffff
      001984 FF FF                 2017 	.dw	0xffff
      001986 01                    2018 	.db	1
      001987 00                    2019 	.db	0
      001988 01                    2020 	.uleb128	1
      001989 7F                    2021 	.sleb128	-1
      00198A 09                    2022 	.db	9
      00198B 0C                    2023 	.db	12
      00198C 08                    2024 	.uleb128	8
      00198D 02                    2025 	.uleb128	2
      00198E 89                    2026 	.db	137
      00198F 01                    2027 	.uleb128	1
      001990                       2028 Ldebug_CIE4_end:
      001990 00 00 00 13           2029 	.dw	0,19
      001994 00 00 19 7E           2030 	.dw	0,(Ldebug_CIE4_start-4)
      001998 00 00 8E BA           2031 	.dw	0,(Sstm8s_gpio$GPIO_WriteReverse$103)	;initial loc
      00199C 00 00 00 07           2032 	.dw	0,Sstm8s_gpio$GPIO_WriteReverse$107-Sstm8s_gpio$GPIO_WriteReverse$103
      0019A0 01                    2033 	.db	1
      0019A1 00 00 8E BA           2034 	.dw	0,(Sstm8s_gpio$GPIO_WriteReverse$103)
      0019A5 0E                    2035 	.db	14
      0019A6 02                    2036 	.uleb128	2
                                   2037 
                                   2038 	.area .debug_frame (NOLOAD)
      0019A7 00 00                 2039 	.dw	0
      0019A9 00 0E                 2040 	.dw	Ldebug_CIE5_end-Ldebug_CIE5_start
      0019AB                       2041 Ldebug_CIE5_start:
      0019AB FF FF                 2042 	.dw	0xffff
      0019AD FF FF                 2043 	.dw	0xffff
      0019AF 01                    2044 	.db	1
      0019B0 00                    2045 	.db	0
      0019B1 01                    2046 	.uleb128	1
      0019B2 7F                    2047 	.sleb128	-1
      0019B3 09                    2048 	.db	9
      0019B4 0C                    2049 	.db	12
      0019B5 08                    2050 	.uleb128	8
      0019B6 02                    2051 	.uleb128	2
      0019B7 89                    2052 	.db	137
      0019B8 01                    2053 	.uleb128	1
      0019B9                       2054 Ldebug_CIE5_end:
      0019B9 00 00 00 21           2055 	.dw	0,33
      0019BD 00 00 19 A7           2056 	.dw	0,(Ldebug_CIE5_start-4)
      0019C1 00 00 8E AC           2057 	.dw	0,(Sstm8s_gpio$GPIO_WriteLow$95)	;initial loc
      0019C5 00 00 00 0E           2058 	.dw	0,Sstm8s_gpio$GPIO_WriteLow$101-Sstm8s_gpio$GPIO_WriteLow$95
      0019C9 01                    2059 	.db	1
      0019CA 00 00 8E AC           2060 	.dw	0,(Sstm8s_gpio$GPIO_WriteLow$95)
      0019CE 0E                    2061 	.db	14
      0019CF 02                    2062 	.uleb128	2
      0019D0 01                    2063 	.db	1
      0019D1 00 00 8E AD           2064 	.dw	0,(Sstm8s_gpio$GPIO_WriteLow$96)
      0019D5 0E                    2065 	.db	14
      0019D6 03                    2066 	.uleb128	3
      0019D7 01                    2067 	.db	1
      0019D8 00 00 8E B9           2068 	.dw	0,(Sstm8s_gpio$GPIO_WriteLow$99)
      0019DC 0E                    2069 	.db	14
      0019DD 02                    2070 	.uleb128	2
                                   2071 
                                   2072 	.area .debug_frame (NOLOAD)
      0019DE 00 00                 2073 	.dw	0
      0019E0 00 0E                 2074 	.dw	Ldebug_CIE6_end-Ldebug_CIE6_start
      0019E2                       2075 Ldebug_CIE6_start:
      0019E2 FF FF                 2076 	.dw	0xffff
      0019E4 FF FF                 2077 	.dw	0xffff
      0019E6 01                    2078 	.db	1
      0019E7 00                    2079 	.db	0
      0019E8 01                    2080 	.uleb128	1
      0019E9 7F                    2081 	.sleb128	-1
      0019EA 09                    2082 	.db	9
      0019EB 0C                    2083 	.db	12
      0019EC 08                    2084 	.uleb128	8
      0019ED 02                    2085 	.uleb128	2
      0019EE 89                    2086 	.db	137
      0019EF 01                    2087 	.uleb128	1
      0019F0                       2088 Ldebug_CIE6_end:
      0019F0 00 00 00 13           2089 	.dw	0,19
      0019F4 00 00 19 DE           2090 	.dw	0,(Ldebug_CIE6_start-4)
      0019F8 00 00 8E A5           2091 	.dw	0,(Sstm8s_gpio$GPIO_WriteHigh$89)	;initial loc
      0019FC 00 00 00 07           2092 	.dw	0,Sstm8s_gpio$GPIO_WriteHigh$93-Sstm8s_gpio$GPIO_WriteHigh$89
      001A00 01                    2093 	.db	1
      001A01 00 00 8E A5           2094 	.dw	0,(Sstm8s_gpio$GPIO_WriteHigh$89)
      001A05 0E                    2095 	.db	14
      001A06 02                    2096 	.uleb128	2
                                   2097 
                                   2098 	.area .debug_frame (NOLOAD)
      001A07 00 00                 2099 	.dw	0
      001A09 00 0E                 2100 	.dw	Ldebug_CIE7_end-Ldebug_CIE7_start
      001A0B                       2101 Ldebug_CIE7_start:
      001A0B FF FF                 2102 	.dw	0xffff
      001A0D FF FF                 2103 	.dw	0xffff
      001A0F 01                    2104 	.db	1
      001A10 00                    2105 	.db	0
      001A11 01                    2106 	.uleb128	1
      001A12 7F                    2107 	.sleb128	-1
      001A13 09                    2108 	.db	9
      001A14 0C                    2109 	.db	12
      001A15 08                    2110 	.uleb128	8
      001A16 02                    2111 	.uleb128	2
      001A17 89                    2112 	.db	137
      001A18 01                    2113 	.uleb128	1
      001A19                       2114 Ldebug_CIE7_end:
      001A19 00 00 00 13           2115 	.dw	0,19
      001A1D 00 00 1A 07           2116 	.dw	0,(Ldebug_CIE7_start-4)
      001A21 00 00 8E 9F           2117 	.dw	0,(Sstm8s_gpio$GPIO_Write$83)	;initial loc
      001A25 00 00 00 06           2118 	.dw	0,Sstm8s_gpio$GPIO_Write$87-Sstm8s_gpio$GPIO_Write$83
      001A29 01                    2119 	.db	1
      001A2A 00 00 8E 9F           2120 	.dw	0,(Sstm8s_gpio$GPIO_Write$83)
      001A2E 0E                    2121 	.db	14
      001A2F 02                    2122 	.uleb128	2
                                   2123 
                                   2124 	.area .debug_frame (NOLOAD)
      001A30 00 00                 2125 	.dw	0
      001A32 00 0E                 2126 	.dw	Ldebug_CIE8_end-Ldebug_CIE8_start
      001A34                       2127 Ldebug_CIE8_start:
      001A34 FF FF                 2128 	.dw	0xffff
      001A36 FF FF                 2129 	.dw	0xffff
      001A38 01                    2130 	.db	1
      001A39 00                    2131 	.db	0
      001A3A 01                    2132 	.uleb128	1
      001A3B 7F                    2133 	.sleb128	-1
      001A3C 09                    2134 	.db	9
      001A3D 0C                    2135 	.db	12
      001A3E 08                    2136 	.uleb128	8
      001A3F 02                    2137 	.uleb128	2
      001A40 89                    2138 	.db	137
      001A41 01                    2139 	.uleb128	1
      001A42                       2140 Ldebug_CIE8_end:
      001A42 00 00 00 FA           2141 	.dw	0,250
      001A46 00 00 1A 30           2142 	.dw	0,(Ldebug_CIE8_start-4)
      001A4A 00 00 8D B3           2143 	.dw	0,(Sstm8s_gpio$GPIO_Init$10)	;initial loc
      001A4E 00 00 00 EC           2144 	.dw	0,Sstm8s_gpio$GPIO_Init$81-Sstm8s_gpio$GPIO_Init$10
      001A52 01                    2145 	.db	1
      001A53 00 00 8D B3           2146 	.dw	0,(Sstm8s_gpio$GPIO_Init$10)
      001A57 0E                    2147 	.db	14
      001A58 02                    2148 	.uleb128	2
      001A59 01                    2149 	.db	1
      001A5A 00 00 8D B5           2150 	.dw	0,(Sstm8s_gpio$GPIO_Init$11)
      001A5E 0E                    2151 	.db	14
      001A5F 07                    2152 	.uleb128	7
      001A60 01                    2153 	.db	1
      001A61 00 00 8D C5           2154 	.dw	0,(Sstm8s_gpio$GPIO_Init$13)
      001A65 0E                    2155 	.db	14
      001A66 07                    2156 	.uleb128	7
      001A67 01                    2157 	.db	1
      001A68 00 00 8D CE           2158 	.dw	0,(Sstm8s_gpio$GPIO_Init$14)
      001A6C 0E                    2159 	.db	14
      001A6D 07                    2160 	.uleb128	7
      001A6E 01                    2161 	.db	1
      001A6F 00 00 8D D7           2162 	.dw	0,(Sstm8s_gpio$GPIO_Init$15)
      001A73 0E                    2163 	.db	14
      001A74 07                    2164 	.uleb128	7
      001A75 01                    2165 	.db	1
      001A76 00 00 8D E0           2166 	.dw	0,(Sstm8s_gpio$GPIO_Init$16)
      001A7A 0E                    2167 	.db	14
      001A7B 07                    2168 	.uleb128	7
      001A7C 01                    2169 	.db	1
      001A7D 00 00 8D E6           2170 	.dw	0,(Sstm8s_gpio$GPIO_Init$17)
      001A81 0E                    2171 	.db	14
      001A82 07                    2172 	.uleb128	7
      001A83 01                    2173 	.db	1
      001A84 00 00 8D EC           2174 	.dw	0,(Sstm8s_gpio$GPIO_Init$18)
      001A88 0E                    2175 	.db	14
      001A89 07                    2176 	.uleb128	7
      001A8A 01                    2177 	.db	1
      001A8B 00 00 8D F2           2178 	.dw	0,(Sstm8s_gpio$GPIO_Init$19)
      001A8F 0E                    2179 	.db	14
      001A90 07                    2180 	.uleb128	7
      001A91 01                    2181 	.db	1
      001A92 00 00 8D F8           2182 	.dw	0,(Sstm8s_gpio$GPIO_Init$20)
      001A96 0E                    2183 	.db	14
      001A97 07                    2184 	.uleb128	7
      001A98 01                    2185 	.db	1
      001A99 00 00 8D FE           2186 	.dw	0,(Sstm8s_gpio$GPIO_Init$21)
      001A9D 0E                    2187 	.db	14
      001A9E 07                    2188 	.uleb128	7
      001A9F 01                    2189 	.db	1
      001AA0 00 00 8E 04           2190 	.dw	0,(Sstm8s_gpio$GPIO_Init$22)
      001AA4 0E                    2191 	.db	14
      001AA5 07                    2192 	.uleb128	7
      001AA6 01                    2193 	.db	1
      001AA7 00 00 8E 0A           2194 	.dw	0,(Sstm8s_gpio$GPIO_Init$23)
      001AAB 0E                    2195 	.db	14
      001AAC 07                    2196 	.uleb128	7
      001AAD 01                    2197 	.db	1
      001AAE 00 00 8E 0C           2198 	.dw	0,(Sstm8s_gpio$GPIO_Init$24)
      001AB2 0E                    2199 	.db	14
      001AB3 08                    2200 	.uleb128	8
      001AB4 01                    2201 	.db	1
      001AB5 00 00 8E 0E           2202 	.dw	0,(Sstm8s_gpio$GPIO_Init$25)
      001AB9 0E                    2203 	.db	14
      001ABA 0A                    2204 	.uleb128	10
      001ABB 01                    2205 	.db	1
      001ABC 00 00 8E 10           2206 	.dw	0,(Sstm8s_gpio$GPIO_Init$26)
      001AC0 0E                    2207 	.db	14
      001AC1 0B                    2208 	.uleb128	11
      001AC2 01                    2209 	.db	1
      001AC3 00 00 8E 12           2210 	.dw	0,(Sstm8s_gpio$GPIO_Init$27)
      001AC7 0E                    2211 	.db	14
      001AC8 0C                    2212 	.uleb128	12
      001AC9 01                    2213 	.db	1
      001ACA 00 00 8E 14           2214 	.dw	0,(Sstm8s_gpio$GPIO_Init$28)
      001ACE 0E                    2215 	.db	14
      001ACF 0D                    2216 	.uleb128	13
      001AD0 01                    2217 	.db	1
      001AD1 00 00 8E 19           2218 	.dw	0,(Sstm8s_gpio$GPIO_Init$29)
      001AD5 0E                    2219 	.db	14
      001AD6 07                    2220 	.uleb128	7
      001AD7 01                    2221 	.db	1
      001AD8 00 00 8E 1F           2222 	.dw	0,(Sstm8s_gpio$GPIO_Init$31)
      001ADC 0E                    2223 	.db	14
      001ADD 08                    2224 	.uleb128	8
      001ADE 01                    2225 	.db	1
      001ADF 00 00 8E 21           2226 	.dw	0,(Sstm8s_gpio$GPIO_Init$32)
      001AE3 0E                    2227 	.db	14
      001AE4 0A                    2228 	.uleb128	10
      001AE5 01                    2229 	.db	1
      001AE6 00 00 8E 23           2230 	.dw	0,(Sstm8s_gpio$GPIO_Init$33)
      001AEA 0E                    2231 	.db	14
      001AEB 0B                    2232 	.uleb128	11
      001AEC 01                    2233 	.db	1
      001AED 00 00 8E 25           2234 	.dw	0,(Sstm8s_gpio$GPIO_Init$34)
      001AF1 0E                    2235 	.db	14
      001AF2 0C                    2236 	.uleb128	12
      001AF3 01                    2237 	.db	1
      001AF4 00 00 8E 27           2238 	.dw	0,(Sstm8s_gpio$GPIO_Init$35)
      001AF8 0E                    2239 	.db	14
      001AF9 0D                    2240 	.uleb128	13
      001AFA 01                    2241 	.db	1
      001AFB 00 00 8E 2C           2242 	.dw	0,(Sstm8s_gpio$GPIO_Init$36)
      001AFF 0E                    2243 	.db	14
      001B00 07                    2244 	.uleb128	7
      001B01 01                    2245 	.db	1
      001B02 00 00 8E 36           2246 	.dw	0,(Sstm8s_gpio$GPIO_Init$38)
      001B06 0E                    2247 	.db	14
      001B07 08                    2248 	.uleb128	8
      001B08 01                    2249 	.db	1
      001B09 00 00 8E 3C           2250 	.dw	0,(Sstm8s_gpio$GPIO_Init$39)
      001B0D 0E                    2251 	.db	14
      001B0E 07                    2252 	.uleb128	7
      001B0F 01                    2253 	.db	1
      001B10 00 00 8E 4D           2254 	.dw	0,(Sstm8s_gpio$GPIO_Init$45)
      001B14 0E                    2255 	.db	14
      001B15 08                    2256 	.uleb128	8
      001B16 01                    2257 	.db	1
      001B17 00 00 8E 52           2258 	.dw	0,(Sstm8s_gpio$GPIO_Init$46)
      001B1B 0E                    2259 	.db	14
      001B1C 07                    2260 	.uleb128	7
      001B1D 01                    2261 	.db	1
      001B1E 00 00 8E 76           2262 	.dw	0,(Sstm8s_gpio$GPIO_Init$60)
      001B22 0E                    2263 	.db	14
      001B23 08                    2264 	.uleb128	8
      001B24 01                    2265 	.db	1
      001B25 00 00 8E 7B           2266 	.dw	0,(Sstm8s_gpio$GPIO_Init$61)
      001B29 0E                    2267 	.db	14
      001B2A 07                    2268 	.uleb128	7
      001B2B 01                    2269 	.db	1
      001B2C 00 00 8E 89           2270 	.dw	0,(Sstm8s_gpio$GPIO_Init$70)
      001B30 0E                    2271 	.db	14
      001B31 08                    2272 	.uleb128	8
      001B32 01                    2273 	.db	1
      001B33 00 00 8E 8E           2274 	.dw	0,(Sstm8s_gpio$GPIO_Init$71)
      001B37 0E                    2275 	.db	14
      001B38 07                    2276 	.uleb128	7
      001B39 01                    2277 	.db	1
      001B3A 00 00 8E 9E           2278 	.dw	0,(Sstm8s_gpio$GPIO_Init$79)
      001B3E 0E                    2279 	.db	14
      001B3F 02                    2280 	.uleb128	2
                                   2281 
                                   2282 	.area .debug_frame (NOLOAD)
      001B40 00 00                 2283 	.dw	0
      001B42 00 0E                 2284 	.dw	Ldebug_CIE9_end-Ldebug_CIE9_start
      001B44                       2285 Ldebug_CIE9_start:
      001B44 FF FF                 2286 	.dw	0xffff
      001B46 FF FF                 2287 	.dw	0xffff
      001B48 01                    2288 	.db	1
      001B49 00                    2289 	.db	0
      001B4A 01                    2290 	.uleb128	1
      001B4B 7F                    2291 	.sleb128	-1
      001B4C 09                    2292 	.db	9
      001B4D 0C                    2293 	.db	12
      001B4E 08                    2294 	.uleb128	8
      001B4F 02                    2295 	.uleb128	2
      001B50 89                    2296 	.db	137
      001B51 01                    2297 	.uleb128	1
      001B52                       2298 Ldebug_CIE9_end:
      001B52 00 00 00 13           2299 	.dw	0,19
      001B56 00 00 1B 40           2300 	.dw	0,(Ldebug_CIE9_start-4)
      001B5A 00 00 8D A4           2301 	.dw	0,(Sstm8s_gpio$GPIO_DeInit$1)	;initial loc
      001B5E 00 00 00 0F           2302 	.dw	0,Sstm8s_gpio$GPIO_DeInit$8-Sstm8s_gpio$GPIO_DeInit$1
      001B62 01                    2303 	.db	1
      001B63 00 00 8D A4           2304 	.dw	0,(Sstm8s_gpio$GPIO_DeInit$1)
      001B67 0E                    2305 	.db	14
      001B68 02                    2306 	.uleb128	2
