;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.1.0 #12072 (MINGW64)
;--------------------------------------------------------
	.module jas
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _TIM2_SetCompare1
	.globl _TIM2_OC1PreloadConfig
	.globl _TIM2_Cmd
	.globl _TIM2_OC1Init
	.globl _TIM2_TimeBaseInit
	.globl _jas_init
	.globl _jas
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)

; default segment ordering for linker
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area CONST
	.area INITIALIZER
	.area CODE

;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
	Sjas$jas_init$0 ==.
;	app/src/jas.c: 4: void jas_init(void) //azurova #00ffff zluta #ffff00 oranzova  #ff4000
;	-----------------------------------------
;	 function jas_init
;	-----------------------------------------
_jas_init:
	Sjas$jas_init$1 ==.
	Sjas$jas_init$2 ==.
;	app/src/jas.c: 6: TIM2_TimeBaseInit(TIM2_PRESCALER_128,255);
	push	#0xff
	Sjas$jas_init$3 ==.
	push	#0x00
	Sjas$jas_init$4 ==.
	push	#0x07
	Sjas$jas_init$5 ==.
	call	_TIM2_TimeBaseInit
	addw	sp, #3
	Sjas$jas_init$6 ==.
	Sjas$jas_init$7 ==.
;	app/src/jas.c: 8: TIM2_OC1Init(TIM2_OCMODE_PWM1,TIM2_OUTPUTSTATE_ENABLE,4000,TIM2_OCPOLARITY_HIGH);
	push	#0x00
	Sjas$jas_init$8 ==.
	push	#0xa0
	Sjas$jas_init$9 ==.
	push	#0x0f
	Sjas$jas_init$10 ==.
	push	#0x11
	Sjas$jas_init$11 ==.
	push	#0x60
	Sjas$jas_init$12 ==.
	call	_TIM2_OC1Init
	addw	sp, #5
	Sjas$jas_init$13 ==.
	Sjas$jas_init$14 ==.
;	app/src/jas.c: 9: TIM2_OC1PreloadConfig(ENABLE);
	push	#0x01
	Sjas$jas_init$15 ==.
	call	_TIM2_OC1PreloadConfig
	pop	a
	Sjas$jas_init$16 ==.
	Sjas$jas_init$17 ==.
;	app/src/jas.c: 11: TIM2_Cmd(ENABLE);
	push	#0x01
	Sjas$jas_init$18 ==.
	call	_TIM2_Cmd
	pop	a
	Sjas$jas_init$19 ==.
	Sjas$jas_init$20 ==.
;	app/src/jas.c: 12: }
	Sjas$jas_init$21 ==.
	XG$jas_init$0$0 ==.
	ret
	Sjas$jas_init$22 ==.
	Sjas$jas$23 ==.
;	app/src/jas.c: 13: void jas(uint8_t j){
;	-----------------------------------------
;	 function jas
;	-----------------------------------------
_jas:
	Sjas$jas$24 ==.
	Sjas$jas$25 ==.
;	app/src/jas.c: 14: TIM2_SetCompare1(j);
	clrw	x
	ld	a, (0x03, sp)
	ld	xl, a
	pushw	x
	Sjas$jas$26 ==.
	call	_TIM2_SetCompare1
	popw	x
	Sjas$jas$27 ==.
	Sjas$jas$28 ==.
;	app/src/jas.c: 17: }
	Sjas$jas$29 ==.
	XG$jas$0$0 ==.
	ret
	Sjas$jas$30 ==.
	.area CODE
	.area CONST
	.area INITIALIZER
	.area CABS (ABS)

	.area .debug_line (NOLOAD)
	.dw	0,Ldebug_line_end-Ldebug_line_start
Ldebug_line_start:
	.dw	2
	.dw	0,Ldebug_line_stmt-6-Ldebug_line_start
	.db	1
	.db	1
	.db	-5
	.db	15
	.db	10
	.db	0
	.db	1
	.db	1
	.db	1
	.db	1
	.db	0
	.db	0
	.db	0
	.db	1
	.ascii "C:\Program Files\SDCC\bin\..\include\stm8"
	.db	0
	.ascii "C:\Program Files\SDCC\bin\..\include"
	.db	0
	.db	0
	.ascii "app/src/jas.c"
	.db	0
	.uleb128	0
	.uleb128	0
	.uleb128	0
	.db	0
Ldebug_line_stmt:
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sjas$jas_init$0)
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sjas$jas_init$2-Sjas$jas_init$0
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sjas$jas_init$7-Sjas$jas_init$2
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sjas$jas_init$14-Sjas$jas_init$7
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sjas$jas_init$17-Sjas$jas_init$14
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sjas$jas_init$20-Sjas$jas_init$17
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sjas$jas_init$21-Sjas$jas_init$20
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sjas$jas$23)
	.db	3
	.sleb128	12
	.db	1
	.db	9
	.dw	Sjas$jas$25-Sjas$jas$23
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sjas$jas$28-Sjas$jas$25
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	1+Sjas$jas$29-Sjas$jas$28
	.db	0
	.uleb128	1
	.db	1
Ldebug_line_end:

	.area .debug_loc (NOLOAD)
Ldebug_loc_start:
	.dw	0,(Sjas$jas$27)
	.dw	0,(Sjas$jas$30)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sjas$jas$26)
	.dw	0,(Sjas$jas$27)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sjas$jas$24)
	.dw	0,(Sjas$jas$26)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sjas$jas_init$19)
	.dw	0,(Sjas$jas_init$22)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sjas$jas_init$18)
	.dw	0,(Sjas$jas_init$19)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sjas$jas_init$16)
	.dw	0,(Sjas$jas_init$18)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sjas$jas_init$15)
	.dw	0,(Sjas$jas_init$16)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sjas$jas_init$13)
	.dw	0,(Sjas$jas_init$15)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sjas$jas_init$12)
	.dw	0,(Sjas$jas_init$13)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sjas$jas_init$11)
	.dw	0,(Sjas$jas_init$12)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sjas$jas_init$10)
	.dw	0,(Sjas$jas_init$11)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sjas$jas_init$9)
	.dw	0,(Sjas$jas_init$10)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sjas$jas_init$8)
	.dw	0,(Sjas$jas_init$9)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sjas$jas_init$6)
	.dw	0,(Sjas$jas_init$8)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sjas$jas_init$5)
	.dw	0,(Sjas$jas_init$6)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sjas$jas_init$4)
	.dw	0,(Sjas$jas_init$5)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sjas$jas_init$3)
	.dw	0,(Sjas$jas_init$4)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sjas$jas_init$1)
	.dw	0,(Sjas$jas_init$3)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0

	.area .debug_abbrev (NOLOAD)
Ldebug_abbrev:
	.uleb128	4
	.uleb128	5
	.db	0
	.uleb128	2
	.uleb128	10
	.uleb128	3
	.uleb128	8
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	3
	.uleb128	46
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	0
	.uleb128	0
	.uleb128	1
	.uleb128	17
	.db	1
	.uleb128	3
	.uleb128	8
	.uleb128	16
	.uleb128	6
	.uleb128	19
	.uleb128	11
	.uleb128	37
	.uleb128	8
	.uleb128	0
	.uleb128	0
	.uleb128	2
	.uleb128	46
	.db	0
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	0
	.uleb128	0
	.uleb128	5
	.uleb128	36
	.db	0
	.uleb128	3
	.uleb128	8
	.uleb128	11
	.uleb128	11
	.uleb128	62
	.uleb128	11
	.uleb128	0
	.uleb128	0
	.uleb128	0

	.area .debug_info (NOLOAD)
	.dw	0,Ldebug_info_end-Ldebug_info_start
Ldebug_info_start:
	.dw	2
	.dw	0,(Ldebug_abbrev)
	.db	4
	.uleb128	1
	.ascii "app/src/jas.c"
	.db	0
	.dw	0,(Ldebug_line_start+-4)
	.db	1
	.ascii "SDCC version 4.1.0 #12072"
	.db	0
	.uleb128	2
	.ascii "jas_init"
	.db	0
	.dw	0,(_jas_init)
	.dw	0,(XG$jas_init$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+44)
	.uleb128	3
	.dw	0,113
	.ascii "jas"
	.db	0
	.dw	0,(_jas)
	.dw	0,(XG$jas$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "j"
	.db	0
	.dw	0,113
	.uleb128	0
	.uleb128	5
	.ascii "unsigned char"
	.db	0
	.db	1
	.db	8
	.uleb128	0
	.uleb128	0
	.uleb128	0
Ldebug_info_end:

	.area .debug_pubnames (NOLOAD)
	.dw	0,Ldebug_pubnames_end-Ldebug_pubnames_start
Ldebug_pubnames_start:
	.dw	2
	.dw	0,(Ldebug_info_start-4)
	.dw	0,4+Ldebug_info_end-Ldebug_info_start
	.dw	0,57
	.ascii "jas_init"
	.db	0
	.dw	0,80
	.ascii "jas"
	.db	0
	.dw	0,0
Ldebug_pubnames_end:

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE0_end-Ldebug_CIE0_start
Ldebug_CIE0_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE0_end:
	.dw	0,33
	.dw	0,(Ldebug_CIE0_start-4)
	.dw	0,(Sjas$jas$24)	;initial loc
	.dw	0,Sjas$jas$30-Sjas$jas$24
	.db	1
	.dw	0,(Sjas$jas$24)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sjas$jas$26)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sjas$jas$27)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE1_end-Ldebug_CIE1_start
Ldebug_CIE1_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE1_end:
	.dw	0,117
	.dw	0,(Ldebug_CIE1_start-4)
	.dw	0,(Sjas$jas_init$1)	;initial loc
	.dw	0,Sjas$jas_init$22-Sjas$jas_init$1
	.db	1
	.dw	0,(Sjas$jas_init$1)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sjas$jas_init$3)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sjas$jas_init$4)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sjas$jas_init$5)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sjas$jas_init$6)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sjas$jas_init$8)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sjas$jas_init$9)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sjas$jas_init$10)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sjas$jas_init$11)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sjas$jas_init$12)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sjas$jas_init$13)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sjas$jas_init$15)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sjas$jas_init$16)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sjas$jas_init$18)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sjas$jas_init$19)
	.db	14
	.uleb128	2
