;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.1.0 #12072 (MINGW64)
;--------------------------------------------------------
	.module delay
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _TIM3_ClearFlag
	.globl _TIM3_GetFlagStatus
	.globl _TIM3_SetCounter
	.globl _TIM3_Cmd
	.globl _TIM3_TimeBaseInit
	.globl _delay_05s_init
	.globl _delay_05s
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)

; default segment ordering for linker
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area CONST
	.area INITIALIZER
	.area CODE

;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
	Sdelay$delay_05s_init$0 ==.
;	app/src/delay.c: 4: void delay_05s_init(void)
;	-----------------------------------------
;	 function delay_05s_init
;	-----------------------------------------
_delay_05s_init:
	Sdelay$delay_05s_init$1 ==.
	Sdelay$delay_05s_init$2 ==.
;	app/src/delay.c: 6: TIM3_TimeBaseInit(TIM3_PRESCALER_256,31499);
	push	#0x0b
	Sdelay$delay_05s_init$3 ==.
	push	#0x7b
	Sdelay$delay_05s_init$4 ==.
	push	#0x08
	Sdelay$delay_05s_init$5 ==.
	call	_TIM3_TimeBaseInit
	addw	sp, #3
	Sdelay$delay_05s_init$6 ==.
	Sdelay$delay_05s_init$7 ==.
;	app/src/delay.c: 7: TIM3_Cmd(ENABLE);
	push	#0x01
	Sdelay$delay_05s_init$8 ==.
	call	_TIM3_Cmd
	pop	a
	Sdelay$delay_05s_init$9 ==.
	Sdelay$delay_05s_init$10 ==.
;	app/src/delay.c: 9: }
	Sdelay$delay_05s_init$11 ==.
	XG$delay_05s_init$0$0 ==.
	ret
	Sdelay$delay_05s_init$12 ==.
	Sdelay$delay_05s$13 ==.
;	app/src/delay.c: 10: void delay_05s(uint32_t time_s){//kolikrát přeteče
;	-----------------------------------------
;	 function delay_05s
;	-----------------------------------------
_delay_05s:
	Sdelay$delay_05s$14 ==.
	sub	sp, #4
	Sdelay$delay_05s$15 ==.
	Sdelay$delay_05s$16 ==.
;	app/src/delay.c: 11: TIM3_SetCounter(0);
	clrw	x
	pushw	x
	Sdelay$delay_05s$17 ==.
	call	_TIM3_SetCounter
	popw	x
	Sdelay$delay_05s$18 ==.
	Sdelay$delay_05s$19 ==.
;	app/src/delay.c: 12: for(uint32_t i=0;i<time_s;i++){
	clrw	x
	ldw	(0x03, sp), x
	ldw	(0x01, sp), x
	Sdelay$delay_05s$20 ==.
00106$:
	ldw	x, (0x03, sp)
	cpw	x, (0x09, sp)
	ld	a, (0x02, sp)
	sbc	a, (0x08, sp)
	ld	a, (0x01, sp)
	sbc	a, (0x07, sp)
	jrnc	00108$
	Sdelay$delay_05s$21 ==.
	Sdelay$delay_05s$22 ==.
;	app/src/delay.c: 13: while (TIM3_GetFlagStatus(TIM3_FLAG_UPDATE) != SET)
00101$:
	push	#0x01
	Sdelay$delay_05s$23 ==.
	push	#0x00
	Sdelay$delay_05s$24 ==.
	call	_TIM3_GetFlagStatus
	popw	x
	Sdelay$delay_05s$25 ==.
	dec	a
	jrne	00101$
	Sdelay$delay_05s$26 ==.
	Sdelay$delay_05s$27 ==.
;	app/src/delay.c: 15: TIM3_ClearFlag (TIM3_FLAG_UPDATE);
	push	#0x01
	Sdelay$delay_05s$28 ==.
	push	#0x00
	Sdelay$delay_05s$29 ==.
	call	_TIM3_ClearFlag
	popw	x
	Sdelay$delay_05s$30 ==.
	Sdelay$delay_05s$31 ==.
	Sdelay$delay_05s$32 ==.
;	app/src/delay.c: 12: for(uint32_t i=0;i<time_s;i++){
	ldw	x, (0x03, sp)
	incw	x
	ldw	(0x03, sp), x
	jrne	00106$
	ldw	x, (0x01, sp)
	incw	x
	ldw	(0x01, sp), x
	jra	00106$
	Sdelay$delay_05s$33 ==.
00108$:
	Sdelay$delay_05s$34 ==.
;	app/src/delay.c: 17: }
	addw	sp, #4
	Sdelay$delay_05s$35 ==.
	Sdelay$delay_05s$36 ==.
	XG$delay_05s$0$0 ==.
	ret
	Sdelay$delay_05s$37 ==.
	.area CODE
	.area CONST
	.area INITIALIZER
	.area CABS (ABS)

	.area .debug_line (NOLOAD)
	.dw	0,Ldebug_line_end-Ldebug_line_start
Ldebug_line_start:
	.dw	2
	.dw	0,Ldebug_line_stmt-6-Ldebug_line_start
	.db	1
	.db	1
	.db	-5
	.db	15
	.db	10
	.db	0
	.db	1
	.db	1
	.db	1
	.db	1
	.db	0
	.db	0
	.db	0
	.db	1
	.ascii "C:\Program Files\SDCC\bin\..\include\stm8"
	.db	0
	.ascii "C:\Program Files\SDCC\bin\..\include"
	.db	0
	.db	0
	.ascii "app/src/delay.c"
	.db	0
	.uleb128	0
	.uleb128	0
	.uleb128	0
	.db	0
Ldebug_line_stmt:
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sdelay$delay_05s_init$0)
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sdelay$delay_05s_init$2-Sdelay$delay_05s_init$0
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sdelay$delay_05s_init$7-Sdelay$delay_05s_init$2
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sdelay$delay_05s_init$10-Sdelay$delay_05s_init$7
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sdelay$delay_05s_init$11-Sdelay$delay_05s_init$10
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sdelay$delay_05s$13)
	.db	3
	.sleb128	9
	.db	1
	.db	9
	.dw	Sdelay$delay_05s$16-Sdelay$delay_05s$13
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sdelay$delay_05s$19-Sdelay$delay_05s$16
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sdelay$delay_05s$22-Sdelay$delay_05s$19
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sdelay$delay_05s$27-Sdelay$delay_05s$22
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sdelay$delay_05s$32-Sdelay$delay_05s$27
	.db	3
	.sleb128	-3
	.db	1
	.db	9
	.dw	Sdelay$delay_05s$34-Sdelay$delay_05s$32
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	1+Sdelay$delay_05s$36-Sdelay$delay_05s$34
	.db	0
	.uleb128	1
	.db	1
Ldebug_line_end:

	.area .debug_loc (NOLOAD)
Ldebug_loc_start:
	.dw	0,(Sdelay$delay_05s$35)
	.dw	0,(Sdelay$delay_05s$37)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sdelay$delay_05s$30)
	.dw	0,(Sdelay$delay_05s$35)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sdelay$delay_05s$29)
	.dw	0,(Sdelay$delay_05s$30)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sdelay$delay_05s$28)
	.dw	0,(Sdelay$delay_05s$29)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sdelay$delay_05s$26)
	.dw	0,(Sdelay$delay_05s$28)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sdelay$delay_05s$25)
	.dw	0,(Sdelay$delay_05s$26)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sdelay$delay_05s$24)
	.dw	0,(Sdelay$delay_05s$25)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sdelay$delay_05s$23)
	.dw	0,(Sdelay$delay_05s$24)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sdelay$delay_05s$18)
	.dw	0,(Sdelay$delay_05s$23)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sdelay$delay_05s$17)
	.dw	0,(Sdelay$delay_05s$18)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sdelay$delay_05s$15)
	.dw	0,(Sdelay$delay_05s$17)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sdelay$delay_05s$14)
	.dw	0,(Sdelay$delay_05s$15)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sdelay$delay_05s_init$9)
	.dw	0,(Sdelay$delay_05s_init$12)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sdelay$delay_05s_init$8)
	.dw	0,(Sdelay$delay_05s_init$9)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sdelay$delay_05s_init$6)
	.dw	0,(Sdelay$delay_05s_init$8)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sdelay$delay_05s_init$5)
	.dw	0,(Sdelay$delay_05s_init$6)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sdelay$delay_05s_init$4)
	.dw	0,(Sdelay$delay_05s_init$5)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sdelay$delay_05s_init$3)
	.dw	0,(Sdelay$delay_05s_init$4)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sdelay$delay_05s_init$1)
	.dw	0,(Sdelay$delay_05s_init$3)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0

	.area .debug_abbrev (NOLOAD)
Ldebug_abbrev:
	.uleb128	4
	.uleb128	5
	.db	0
	.uleb128	2
	.uleb128	10
	.uleb128	3
	.uleb128	8
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	3
	.uleb128	46
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	0
	.uleb128	0
	.uleb128	7
	.uleb128	52
	.db	0
	.uleb128	2
	.uleb128	10
	.uleb128	3
	.uleb128	8
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	1
	.uleb128	17
	.db	1
	.uleb128	3
	.uleb128	8
	.uleb128	16
	.uleb128	6
	.uleb128	19
	.uleb128	11
	.uleb128	37
	.uleb128	8
	.uleb128	0
	.uleb128	0
	.uleb128	5
	.uleb128	11
	.db	1
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	0
	.uleb128	0
	.uleb128	6
	.uleb128	11
	.db	0
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	0
	.uleb128	0
	.uleb128	2
	.uleb128	46
	.db	0
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	0
	.uleb128	0
	.uleb128	8
	.uleb128	36
	.db	0
	.uleb128	3
	.uleb128	8
	.uleb128	11
	.uleb128	11
	.uleb128	62
	.uleb128	11
	.uleb128	0
	.uleb128	0
	.uleb128	0

	.area .debug_info (NOLOAD)
	.dw	0,Ldebug_info_end-Ldebug_info_start
Ldebug_info_start:
	.dw	2
	.dw	0,(Ldebug_abbrev)
	.db	4
	.uleb128	1
	.ascii "app/src/delay.c"
	.db	0
	.dw	0,(Ldebug_line_start+-4)
	.db	1
	.ascii "SDCC version 4.1.0 #12072"
	.db	0
	.uleb128	2
	.ascii "delay_05s_init"
	.db	0
	.dw	0,(_delay_05s_init)
	.dw	0,(XG$delay_05s_init$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+152)
	.uleb128	3
	.dw	0,161
	.ascii "delay_05s"
	.db	0
	.dw	0,(_delay_05s)
	.dw	0,(XG$delay_05s$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "time_s"
	.db	0
	.dw	0,161
	.uleb128	5
	.dw	0,(Sdelay$delay_05s$20)
	.dw	0,(Sdelay$delay_05s$33)
	.uleb128	6
	.dw	0,(Sdelay$delay_05s$21)
	.dw	0,(Sdelay$delay_05s$31)
	.uleb128	7
	.db	2
	.db	145
	.sleb128	-4
	.ascii "i"
	.db	0
	.dw	0,161
	.uleb128	0
	.uleb128	0
	.uleb128	8
	.ascii "unsigned long"
	.db	0
	.db	4
	.db	7
	.uleb128	0
	.uleb128	0
	.uleb128	0
Ldebug_info_end:

	.area .debug_pubnames (NOLOAD)
	.dw	0,Ldebug_pubnames_end-Ldebug_pubnames_start
Ldebug_pubnames_start:
	.dw	2
	.dw	0,(Ldebug_info_start-4)
	.dw	0,4+Ldebug_info_end-Ldebug_info_start
	.dw	0,59
	.ascii "delay_05s_init"
	.db	0
	.dw	0,88
	.ascii "delay_05s"
	.db	0
	.dw	0,0
Ldebug_pubnames_end:

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE0_end-Ldebug_CIE0_start
Ldebug_CIE0_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE0_end:
	.dw	0,96
	.dw	0,(Ldebug_CIE0_start-4)
	.dw	0,(Sdelay$delay_05s$14)	;initial loc
	.dw	0,Sdelay$delay_05s$37-Sdelay$delay_05s$14
	.db	1
	.dw	0,(Sdelay$delay_05s$14)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sdelay$delay_05s$15)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sdelay$delay_05s$17)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sdelay$delay_05s$18)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sdelay$delay_05s$23)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sdelay$delay_05s$24)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sdelay$delay_05s$25)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sdelay$delay_05s$26)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sdelay$delay_05s$28)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sdelay$delay_05s$29)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sdelay$delay_05s$30)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sdelay$delay_05s$35)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE1_end-Ldebug_CIE1_start
Ldebug_CIE1_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE1_end:
	.dw	0,61
	.dw	0,(Ldebug_CIE1_start-4)
	.dw	0,(Sdelay$delay_05s_init$1)	;initial loc
	.dw	0,Sdelay$delay_05s_init$12-Sdelay$delay_05s_init$1
	.db	1
	.dw	0,(Sdelay$delay_05s_init$1)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sdelay$delay_05s_init$3)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sdelay$delay_05s_init$4)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sdelay$delay_05s_init$5)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sdelay$delay_05s_init$6)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sdelay$delay_05s_init$8)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sdelay$delay_05s_init$9)
	.db	14
	.uleb128	2
