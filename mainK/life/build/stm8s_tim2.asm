;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.1.0 #12072 (MINGW64)
;--------------------------------------------------------
	.module stm8s_tim2
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _assert_failed
	.globl _TIM2_DeInit
	.globl _TIM2_TimeBaseInit
	.globl _TIM2_OC1Init
	.globl _TIM2_OC2Init
	.globl _TIM2_OC3Init
	.globl _TIM2_ICInit
	.globl _TIM2_PWMIConfig
	.globl _TIM2_Cmd
	.globl _TIM2_ITConfig
	.globl _TIM2_UpdateDisableConfig
	.globl _TIM2_UpdateRequestConfig
	.globl _TIM2_SelectOnePulseMode
	.globl _TIM2_PrescalerConfig
	.globl _TIM2_ForcedOC1Config
	.globl _TIM2_ForcedOC2Config
	.globl _TIM2_ForcedOC3Config
	.globl _TIM2_ARRPreloadConfig
	.globl _TIM2_OC1PreloadConfig
	.globl _TIM2_OC2PreloadConfig
	.globl _TIM2_OC3PreloadConfig
	.globl _TIM2_GenerateEvent
	.globl _TIM2_OC1PolarityConfig
	.globl _TIM2_OC2PolarityConfig
	.globl _TIM2_OC3PolarityConfig
	.globl _TIM2_CCxCmd
	.globl _TIM2_SelectOCxM
	.globl _TIM2_SetCounter
	.globl _TIM2_SetAutoreload
	.globl _TIM2_SetCompare1
	.globl _TIM2_SetCompare2
	.globl _TIM2_SetCompare3
	.globl _TIM2_SetIC1Prescaler
	.globl _TIM2_SetIC2Prescaler
	.globl _TIM2_SetIC3Prescaler
	.globl _TIM2_GetCapture1
	.globl _TIM2_GetCapture2
	.globl _TIM2_GetCapture3
	.globl _TIM2_GetCounter
	.globl _TIM2_GetPrescaler
	.globl _TIM2_GetFlagStatus
	.globl _TIM2_ClearFlag
	.globl _TIM2_GetITStatus
	.globl _TIM2_ClearITPendingBit
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)

; default segment ordering for linker
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area CONST
	.area INITIALIZER
	.area CODE

;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
	Sstm8s_tim2$TIM2_DeInit$0 ==.
;	drivers/src/stm8s_tim2.c: 52: void TIM2_DeInit(void)
;	-----------------------------------------
;	 function TIM2_DeInit
;	-----------------------------------------
_TIM2_DeInit:
	Sstm8s_tim2$TIM2_DeInit$1 ==.
	Sstm8s_tim2$TIM2_DeInit$2 ==.
;	drivers/src/stm8s_tim2.c: 54: TIM2->CR1 = (uint8_t)TIM2_CR1_RESET_VALUE;
	mov	0x5300+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$3 ==.
;	drivers/src/stm8s_tim2.c: 55: TIM2->IER = (uint8_t)TIM2_IER_RESET_VALUE;
	mov	0x5301+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$4 ==.
;	drivers/src/stm8s_tim2.c: 56: TIM2->SR2 = (uint8_t)TIM2_SR2_RESET_VALUE;
	mov	0x5303+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$5 ==.
;	drivers/src/stm8s_tim2.c: 59: TIM2->CCER1 = (uint8_t)TIM2_CCER1_RESET_VALUE;
	mov	0x5308+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$6 ==.
;	drivers/src/stm8s_tim2.c: 60: TIM2->CCER2 = (uint8_t)TIM2_CCER2_RESET_VALUE;
	mov	0x5309+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$7 ==.
;	drivers/src/stm8s_tim2.c: 64: TIM2->CCER1 = (uint8_t)TIM2_CCER1_RESET_VALUE;
	mov	0x5308+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$8 ==.
;	drivers/src/stm8s_tim2.c: 65: TIM2->CCER2 = (uint8_t)TIM2_CCER2_RESET_VALUE;
	mov	0x5309+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$9 ==.
;	drivers/src/stm8s_tim2.c: 66: TIM2->CCMR1 = (uint8_t)TIM2_CCMR1_RESET_VALUE;
	mov	0x5305+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$10 ==.
;	drivers/src/stm8s_tim2.c: 67: TIM2->CCMR2 = (uint8_t)TIM2_CCMR2_RESET_VALUE;
	mov	0x5306+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$11 ==.
;	drivers/src/stm8s_tim2.c: 68: TIM2->CCMR3 = (uint8_t)TIM2_CCMR3_RESET_VALUE;
	mov	0x5307+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$12 ==.
;	drivers/src/stm8s_tim2.c: 69: TIM2->CNTRH = (uint8_t)TIM2_CNTRH_RESET_VALUE;
	mov	0x530a+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$13 ==.
;	drivers/src/stm8s_tim2.c: 70: TIM2->CNTRL = (uint8_t)TIM2_CNTRL_RESET_VALUE;
	mov	0x530b+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$14 ==.
;	drivers/src/stm8s_tim2.c: 71: TIM2->PSCR = (uint8_t)TIM2_PSCR_RESET_VALUE;
	mov	0x530c+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$15 ==.
;	drivers/src/stm8s_tim2.c: 72: TIM2->ARRH  = (uint8_t)TIM2_ARRH_RESET_VALUE;
	mov	0x530d+0, #0xff
	Sstm8s_tim2$TIM2_DeInit$16 ==.
;	drivers/src/stm8s_tim2.c: 73: TIM2->ARRL  = (uint8_t)TIM2_ARRL_RESET_VALUE;
	mov	0x530e+0, #0xff
	Sstm8s_tim2$TIM2_DeInit$17 ==.
;	drivers/src/stm8s_tim2.c: 74: TIM2->CCR1H = (uint8_t)TIM2_CCR1H_RESET_VALUE;
	mov	0x530f+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$18 ==.
;	drivers/src/stm8s_tim2.c: 75: TIM2->CCR1L = (uint8_t)TIM2_CCR1L_RESET_VALUE;
	mov	0x5310+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$19 ==.
;	drivers/src/stm8s_tim2.c: 76: TIM2->CCR2H = (uint8_t)TIM2_CCR2H_RESET_VALUE;
	mov	0x5311+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$20 ==.
;	drivers/src/stm8s_tim2.c: 77: TIM2->CCR2L = (uint8_t)TIM2_CCR2L_RESET_VALUE;
	mov	0x5312+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$21 ==.
;	drivers/src/stm8s_tim2.c: 78: TIM2->CCR3H = (uint8_t)TIM2_CCR3H_RESET_VALUE;
	mov	0x5313+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$22 ==.
;	drivers/src/stm8s_tim2.c: 79: TIM2->CCR3L = (uint8_t)TIM2_CCR3L_RESET_VALUE;
	mov	0x5314+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$23 ==.
;	drivers/src/stm8s_tim2.c: 80: TIM2->SR1 = (uint8_t)TIM2_SR1_RESET_VALUE;
	mov	0x5302+0, #0x00
	Sstm8s_tim2$TIM2_DeInit$24 ==.
;	drivers/src/stm8s_tim2.c: 81: }
	Sstm8s_tim2$TIM2_DeInit$25 ==.
	XG$TIM2_DeInit$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_DeInit$26 ==.
	Sstm8s_tim2$TIM2_TimeBaseInit$27 ==.
;	drivers/src/stm8s_tim2.c: 89: void TIM2_TimeBaseInit( TIM2_Prescaler_TypeDef TIM2_Prescaler,
;	-----------------------------------------
;	 function TIM2_TimeBaseInit
;	-----------------------------------------
_TIM2_TimeBaseInit:
	Sstm8s_tim2$TIM2_TimeBaseInit$28 ==.
	Sstm8s_tim2$TIM2_TimeBaseInit$29 ==.
;	drivers/src/stm8s_tim2.c: 93: TIM2->PSCR = (uint8_t)(TIM2_Prescaler);
	ldw	x, #0x530c
	ld	a, (0x03, sp)
	ld	(x), a
	Sstm8s_tim2$TIM2_TimeBaseInit$30 ==.
;	drivers/src/stm8s_tim2.c: 95: TIM2->ARRH = (uint8_t)(TIM2_Period >> 8);
	ld	a, (0x04, sp)
	ld	0x530d, a
	Sstm8s_tim2$TIM2_TimeBaseInit$31 ==.
;	drivers/src/stm8s_tim2.c: 96: TIM2->ARRL = (uint8_t)(TIM2_Period);
	ld	a, (0x05, sp)
	ld	0x530e, a
	Sstm8s_tim2$TIM2_TimeBaseInit$32 ==.
;	drivers/src/stm8s_tim2.c: 97: }
	Sstm8s_tim2$TIM2_TimeBaseInit$33 ==.
	XG$TIM2_TimeBaseInit$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_TimeBaseInit$34 ==.
	Sstm8s_tim2$TIM2_OC1Init$35 ==.
;	drivers/src/stm8s_tim2.c: 108: void TIM2_OC1Init(TIM2_OCMode_TypeDef TIM2_OCMode,
;	-----------------------------------------
;	 function TIM2_OC1Init
;	-----------------------------------------
_TIM2_OC1Init:
	Sstm8s_tim2$TIM2_OC1Init$36 ==.
	pushw	x
	Sstm8s_tim2$TIM2_OC1Init$37 ==.
	Sstm8s_tim2$TIM2_OC1Init$38 ==.
;	drivers/src/stm8s_tim2.c: 114: assert_param(IS_TIM2_OC_MODE_OK(TIM2_OCMode));
	tnz	(0x05, sp)
	jreq	00104$
	ld	a, (0x05, sp)
	cp	a, #0x10
	jreq	00104$
	Sstm8s_tim2$TIM2_OC1Init$39 ==.
	ld	a, (0x05, sp)
	cp	a, #0x20
	jreq	00104$
	Sstm8s_tim2$TIM2_OC1Init$40 ==.
	ld	a, (0x05, sp)
	cp	a, #0x30
	jreq	00104$
	Sstm8s_tim2$TIM2_OC1Init$41 ==.
	ld	a, (0x05, sp)
	cp	a, #0x60
	jreq	00104$
	Sstm8s_tim2$TIM2_OC1Init$42 ==.
	ld	a, (0x05, sp)
	cp	a, #0x70
	jreq	00104$
	Sstm8s_tim2$TIM2_OC1Init$43 ==.
	push	#0x72
	Sstm8s_tim2$TIM2_OC1Init$44 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_OC1Init$45 ==.
	push	#0x00
	Sstm8s_tim2$TIM2_OC1Init$46 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_OC1Init$47 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_OC1Init$48 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_OC1Init$49 ==.
00104$:
	Sstm8s_tim2$TIM2_OC1Init$50 ==.
;	drivers/src/stm8s_tim2.c: 115: assert_param(IS_TIM2_OUTPUT_STATE_OK(TIM2_OutputState));
	tnz	(0x06, sp)
	jreq	00121$
	ld	a, (0x06, sp)
	cp	a, #0x11
	jreq	00121$
	Sstm8s_tim2$TIM2_OC1Init$51 ==.
	push	#0x73
	Sstm8s_tim2$TIM2_OC1Init$52 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_OC1Init$53 ==.
	push	#0x00
	Sstm8s_tim2$TIM2_OC1Init$54 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_OC1Init$55 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_OC1Init$56 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_OC1Init$57 ==.
00121$:
	Sstm8s_tim2$TIM2_OC1Init$58 ==.
;	drivers/src/stm8s_tim2.c: 116: assert_param(IS_TIM2_OC_POLARITY_OK(TIM2_OCPolarity));
	tnz	(0x09, sp)
	jreq	00126$
	ld	a, (0x09, sp)
	cp	a, #0x22
	jreq	00126$
	Sstm8s_tim2$TIM2_OC1Init$59 ==.
	push	#0x74
	Sstm8s_tim2$TIM2_OC1Init$60 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_OC1Init$61 ==.
	push	#0x00
	Sstm8s_tim2$TIM2_OC1Init$62 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_OC1Init$63 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_OC1Init$64 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_OC1Init$65 ==.
00126$:
	Sstm8s_tim2$TIM2_OC1Init$66 ==.
;	drivers/src/stm8s_tim2.c: 119: TIM2->CCER1 &= (uint8_t)(~( TIM2_CCER1_CC1E | TIM2_CCER1_CC1P));
	ld	a, 0x5308
	and	a, #0xfc
	ld	0x5308, a
	Sstm8s_tim2$TIM2_OC1Init$67 ==.
;	drivers/src/stm8s_tim2.c: 121: TIM2->CCER1 |= (uint8_t)((uint8_t)(TIM2_OutputState & TIM2_CCER1_CC1E ) | 
	ld	a, 0x5308
	ld	(0x01, sp), a
	ld	a, (0x06, sp)
	and	a, #0x01
	ld	(0x02, sp), a
	Sstm8s_tim2$TIM2_OC1Init$68 ==.
;	drivers/src/stm8s_tim2.c: 122: (uint8_t)(TIM2_OCPolarity & TIM2_CCER1_CC1P));
	ld	a, (0x09, sp)
	and	a, #0x02
	or	a, (0x02, sp)
	or	a, (0x01, sp)
	ld	0x5308, a
	Sstm8s_tim2$TIM2_OC1Init$69 ==.
;	drivers/src/stm8s_tim2.c: 125: TIM2->CCMR1 = (uint8_t)((uint8_t)(TIM2->CCMR1 & (uint8_t)(~TIM2_CCMR_OCM)) |
	ld	a, 0x5305
	and	a, #0x8f
	Sstm8s_tim2$TIM2_OC1Init$70 ==.
;	drivers/src/stm8s_tim2.c: 126: (uint8_t)TIM2_OCMode);
	or	a, (0x05, sp)
	ld	0x5305, a
	Sstm8s_tim2$TIM2_OC1Init$71 ==.
;	drivers/src/stm8s_tim2.c: 129: TIM2->CCR1H = (uint8_t)(TIM2_Pulse >> 8);
	ld	a, (0x07, sp)
	ld	0x530f, a
	Sstm8s_tim2$TIM2_OC1Init$72 ==.
;	drivers/src/stm8s_tim2.c: 130: TIM2->CCR1L = (uint8_t)(TIM2_Pulse);
	ld	a, (0x08, sp)
	ld	0x5310, a
	Sstm8s_tim2$TIM2_OC1Init$73 ==.
;	drivers/src/stm8s_tim2.c: 131: }
	popw	x
	Sstm8s_tim2$TIM2_OC1Init$74 ==.
	Sstm8s_tim2$TIM2_OC1Init$75 ==.
	XG$TIM2_OC1Init$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_OC1Init$76 ==.
	Sstm8s_tim2$TIM2_OC2Init$77 ==.
;	drivers/src/stm8s_tim2.c: 142: void TIM2_OC2Init(TIM2_OCMode_TypeDef TIM2_OCMode,
;	-----------------------------------------
;	 function TIM2_OC2Init
;	-----------------------------------------
_TIM2_OC2Init:
	Sstm8s_tim2$TIM2_OC2Init$78 ==.
	pushw	x
	Sstm8s_tim2$TIM2_OC2Init$79 ==.
	Sstm8s_tim2$TIM2_OC2Init$80 ==.
;	drivers/src/stm8s_tim2.c: 148: assert_param(IS_TIM2_OC_MODE_OK(TIM2_OCMode));
	tnz	(0x05, sp)
	jreq	00104$
	ld	a, (0x05, sp)
	cp	a, #0x10
	jreq	00104$
	Sstm8s_tim2$TIM2_OC2Init$81 ==.
	ld	a, (0x05, sp)
	cp	a, #0x20
	jreq	00104$
	Sstm8s_tim2$TIM2_OC2Init$82 ==.
	ld	a, (0x05, sp)
	cp	a, #0x30
	jreq	00104$
	Sstm8s_tim2$TIM2_OC2Init$83 ==.
	ld	a, (0x05, sp)
	cp	a, #0x60
	jreq	00104$
	Sstm8s_tim2$TIM2_OC2Init$84 ==.
	ld	a, (0x05, sp)
	cp	a, #0x70
	jreq	00104$
	Sstm8s_tim2$TIM2_OC2Init$85 ==.
	push	#0x94
	Sstm8s_tim2$TIM2_OC2Init$86 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_OC2Init$87 ==.
	push	#0x00
	Sstm8s_tim2$TIM2_OC2Init$88 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_OC2Init$89 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_OC2Init$90 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_OC2Init$91 ==.
00104$:
	Sstm8s_tim2$TIM2_OC2Init$92 ==.
;	drivers/src/stm8s_tim2.c: 149: assert_param(IS_TIM2_OUTPUT_STATE_OK(TIM2_OutputState));
	tnz	(0x06, sp)
	jreq	00121$
	ld	a, (0x06, sp)
	cp	a, #0x11
	jreq	00121$
	Sstm8s_tim2$TIM2_OC2Init$93 ==.
	push	#0x95
	Sstm8s_tim2$TIM2_OC2Init$94 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_OC2Init$95 ==.
	push	#0x00
	Sstm8s_tim2$TIM2_OC2Init$96 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_OC2Init$97 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_OC2Init$98 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_OC2Init$99 ==.
00121$:
	Sstm8s_tim2$TIM2_OC2Init$100 ==.
;	drivers/src/stm8s_tim2.c: 150: assert_param(IS_TIM2_OC_POLARITY_OK(TIM2_OCPolarity));
	tnz	(0x09, sp)
	jreq	00126$
	ld	a, (0x09, sp)
	cp	a, #0x22
	jreq	00126$
	Sstm8s_tim2$TIM2_OC2Init$101 ==.
	push	#0x96
	Sstm8s_tim2$TIM2_OC2Init$102 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_OC2Init$103 ==.
	push	#0x00
	Sstm8s_tim2$TIM2_OC2Init$104 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_OC2Init$105 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_OC2Init$106 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_OC2Init$107 ==.
00126$:
	Sstm8s_tim2$TIM2_OC2Init$108 ==.
;	drivers/src/stm8s_tim2.c: 154: TIM2->CCER1 &= (uint8_t)(~( TIM2_CCER1_CC2E |  TIM2_CCER1_CC2P ));
	ld	a, 0x5308
	and	a, #0xcf
	ld	0x5308, a
	Sstm8s_tim2$TIM2_OC2Init$109 ==.
;	drivers/src/stm8s_tim2.c: 156: TIM2->CCER1 |= (uint8_t)((uint8_t)(TIM2_OutputState  & TIM2_CCER1_CC2E ) |
	ld	a, 0x5308
	ld	(0x01, sp), a
	ld	a, (0x06, sp)
	and	a, #0x10
	ld	(0x02, sp), a
	Sstm8s_tim2$TIM2_OC2Init$110 ==.
;	drivers/src/stm8s_tim2.c: 157: (uint8_t)(TIM2_OCPolarity & TIM2_CCER1_CC2P));
	ld	a, (0x09, sp)
	and	a, #0x20
	or	a, (0x02, sp)
	or	a, (0x01, sp)
	ld	0x5308, a
	Sstm8s_tim2$TIM2_OC2Init$111 ==.
;	drivers/src/stm8s_tim2.c: 161: TIM2->CCMR2 = (uint8_t)((uint8_t)(TIM2->CCMR2 & (uint8_t)(~TIM2_CCMR_OCM)) | 
	ld	a, 0x5306
	and	a, #0x8f
	Sstm8s_tim2$TIM2_OC2Init$112 ==.
;	drivers/src/stm8s_tim2.c: 162: (uint8_t)TIM2_OCMode);
	or	a, (0x05, sp)
	ld	0x5306, a
	Sstm8s_tim2$TIM2_OC2Init$113 ==.
;	drivers/src/stm8s_tim2.c: 166: TIM2->CCR2H = (uint8_t)(TIM2_Pulse >> 8);
	ld	a, (0x07, sp)
	ld	0x5311, a
	Sstm8s_tim2$TIM2_OC2Init$114 ==.
;	drivers/src/stm8s_tim2.c: 167: TIM2->CCR2L = (uint8_t)(TIM2_Pulse);
	ld	a, (0x08, sp)
	ld	0x5312, a
	Sstm8s_tim2$TIM2_OC2Init$115 ==.
;	drivers/src/stm8s_tim2.c: 168: }
	popw	x
	Sstm8s_tim2$TIM2_OC2Init$116 ==.
	Sstm8s_tim2$TIM2_OC2Init$117 ==.
	XG$TIM2_OC2Init$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_OC2Init$118 ==.
	Sstm8s_tim2$TIM2_OC3Init$119 ==.
;	drivers/src/stm8s_tim2.c: 179: void TIM2_OC3Init(TIM2_OCMode_TypeDef TIM2_OCMode,
;	-----------------------------------------
;	 function TIM2_OC3Init
;	-----------------------------------------
_TIM2_OC3Init:
	Sstm8s_tim2$TIM2_OC3Init$120 ==.
	pushw	x
	Sstm8s_tim2$TIM2_OC3Init$121 ==.
	Sstm8s_tim2$TIM2_OC3Init$122 ==.
;	drivers/src/stm8s_tim2.c: 185: assert_param(IS_TIM2_OC_MODE_OK(TIM2_OCMode));
	tnz	(0x05, sp)
	jreq	00104$
	ld	a, (0x05, sp)
	cp	a, #0x10
	jreq	00104$
	Sstm8s_tim2$TIM2_OC3Init$123 ==.
	ld	a, (0x05, sp)
	cp	a, #0x20
	jreq	00104$
	Sstm8s_tim2$TIM2_OC3Init$124 ==.
	ld	a, (0x05, sp)
	cp	a, #0x30
	jreq	00104$
	Sstm8s_tim2$TIM2_OC3Init$125 ==.
	ld	a, (0x05, sp)
	cp	a, #0x60
	jreq	00104$
	Sstm8s_tim2$TIM2_OC3Init$126 ==.
	ld	a, (0x05, sp)
	cp	a, #0x70
	jreq	00104$
	Sstm8s_tim2$TIM2_OC3Init$127 ==.
	push	#0xb9
	Sstm8s_tim2$TIM2_OC3Init$128 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_OC3Init$129 ==.
	push	#0x00
	Sstm8s_tim2$TIM2_OC3Init$130 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_OC3Init$131 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_OC3Init$132 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_OC3Init$133 ==.
00104$:
	Sstm8s_tim2$TIM2_OC3Init$134 ==.
;	drivers/src/stm8s_tim2.c: 186: assert_param(IS_TIM2_OUTPUT_STATE_OK(TIM2_OutputState));
	tnz	(0x06, sp)
	jreq	00121$
	ld	a, (0x06, sp)
	cp	a, #0x11
	jreq	00121$
	Sstm8s_tim2$TIM2_OC3Init$135 ==.
	push	#0xba
	Sstm8s_tim2$TIM2_OC3Init$136 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_OC3Init$137 ==.
	push	#0x00
	Sstm8s_tim2$TIM2_OC3Init$138 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_OC3Init$139 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_OC3Init$140 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_OC3Init$141 ==.
00121$:
	Sstm8s_tim2$TIM2_OC3Init$142 ==.
;	drivers/src/stm8s_tim2.c: 187: assert_param(IS_TIM2_OC_POLARITY_OK(TIM2_OCPolarity));
	tnz	(0x09, sp)
	jreq	00126$
	ld	a, (0x09, sp)
	cp	a, #0x22
	jreq	00126$
	Sstm8s_tim2$TIM2_OC3Init$143 ==.
	push	#0xbb
	Sstm8s_tim2$TIM2_OC3Init$144 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_OC3Init$145 ==.
	push	#0x00
	Sstm8s_tim2$TIM2_OC3Init$146 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_OC3Init$147 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_OC3Init$148 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_OC3Init$149 ==.
00126$:
	Sstm8s_tim2$TIM2_OC3Init$150 ==.
;	drivers/src/stm8s_tim2.c: 189: TIM2->CCER2 &= (uint8_t)(~( TIM2_CCER2_CC3E  | TIM2_CCER2_CC3P));
	ld	a, 0x5309
	and	a, #0xfc
	ld	0x5309, a
	Sstm8s_tim2$TIM2_OC3Init$151 ==.
;	drivers/src/stm8s_tim2.c: 191: TIM2->CCER2 |= (uint8_t)((uint8_t)(TIM2_OutputState & TIM2_CCER2_CC3E) |  
	ld	a, 0x5309
	ld	(0x01, sp), a
	ld	a, (0x06, sp)
	and	a, #0x01
	ld	(0x02, sp), a
	Sstm8s_tim2$TIM2_OC3Init$152 ==.
;	drivers/src/stm8s_tim2.c: 192: (uint8_t)(TIM2_OCPolarity & TIM2_CCER2_CC3P));
	ld	a, (0x09, sp)
	and	a, #0x02
	or	a, (0x02, sp)
	or	a, (0x01, sp)
	ld	0x5309, a
	Sstm8s_tim2$TIM2_OC3Init$153 ==.
;	drivers/src/stm8s_tim2.c: 195: TIM2->CCMR3 = (uint8_t)((uint8_t)(TIM2->CCMR3 & (uint8_t)(~TIM2_CCMR_OCM)) |
	ld	a, 0x5307
	and	a, #0x8f
	Sstm8s_tim2$TIM2_OC3Init$154 ==.
;	drivers/src/stm8s_tim2.c: 196: (uint8_t)TIM2_OCMode);
	or	a, (0x05, sp)
	ld	0x5307, a
	Sstm8s_tim2$TIM2_OC3Init$155 ==.
;	drivers/src/stm8s_tim2.c: 199: TIM2->CCR3H = (uint8_t)(TIM2_Pulse >> 8);
	ld	a, (0x07, sp)
	ld	0x5313, a
	Sstm8s_tim2$TIM2_OC3Init$156 ==.
;	drivers/src/stm8s_tim2.c: 200: TIM2->CCR3L = (uint8_t)(TIM2_Pulse);
	ld	a, (0x08, sp)
	ld	0x5314, a
	Sstm8s_tim2$TIM2_OC3Init$157 ==.
;	drivers/src/stm8s_tim2.c: 201: }
	popw	x
	Sstm8s_tim2$TIM2_OC3Init$158 ==.
	Sstm8s_tim2$TIM2_OC3Init$159 ==.
	XG$TIM2_OC3Init$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_OC3Init$160 ==.
	Sstm8s_tim2$TIM2_ICInit$161 ==.
;	drivers/src/stm8s_tim2.c: 212: void TIM2_ICInit(TIM2_Channel_TypeDef TIM2_Channel,
;	-----------------------------------------
;	 function TIM2_ICInit
;	-----------------------------------------
_TIM2_ICInit:
	Sstm8s_tim2$TIM2_ICInit$162 ==.
	push	a
	Sstm8s_tim2$TIM2_ICInit$163 ==.
	Sstm8s_tim2$TIM2_ICInit$164 ==.
;	drivers/src/stm8s_tim2.c: 219: assert_param(IS_TIM2_CHANNEL_OK(TIM2_Channel));
	ld	a, (0x04, sp)
	dec	a
	jrne	00219$
	ld	a, #0x01
	ld	(0x01, sp), a
	.byte 0xc5
00219$:
	clr	(0x01, sp)
00220$:
	Sstm8s_tim2$TIM2_ICInit$165 ==.
	tnz	(0x04, sp)
	jreq	00110$
	tnz	(0x01, sp)
	jrne	00110$
	ld	a, (0x04, sp)
	cp	a, #0x02
	jreq	00110$
	Sstm8s_tim2$TIM2_ICInit$166 ==.
	push	#0xdb
	Sstm8s_tim2$TIM2_ICInit$167 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_ICInit$168 ==.
	push	#0x00
	Sstm8s_tim2$TIM2_ICInit$169 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_ICInit$170 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_ICInit$171 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_ICInit$172 ==.
00110$:
	Sstm8s_tim2$TIM2_ICInit$173 ==.
;	drivers/src/stm8s_tim2.c: 220: assert_param(IS_TIM2_IC_POLARITY_OK(TIM2_ICPolarity));
	tnz	(0x05, sp)
	jreq	00118$
	ld	a, (0x05, sp)
	cp	a, #0x44
	jreq	00118$
	Sstm8s_tim2$TIM2_ICInit$174 ==.
	push	#0xdc
	Sstm8s_tim2$TIM2_ICInit$175 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_ICInit$176 ==.
	push	#0x00
	Sstm8s_tim2$TIM2_ICInit$177 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_ICInit$178 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_ICInit$179 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_ICInit$180 ==.
00118$:
	Sstm8s_tim2$TIM2_ICInit$181 ==.
;	drivers/src/stm8s_tim2.c: 221: assert_param(IS_TIM2_IC_SELECTION_OK(TIM2_ICSelection));
	ld	a, (0x06, sp)
	dec	a
	jreq	00123$
	Sstm8s_tim2$TIM2_ICInit$182 ==.
	ld	a, (0x06, sp)
	cp	a, #0x02
	jreq	00123$
	Sstm8s_tim2$TIM2_ICInit$183 ==.
	ld	a, (0x06, sp)
	cp	a, #0x03
	jreq	00123$
	Sstm8s_tim2$TIM2_ICInit$184 ==.
	push	#0xdd
	Sstm8s_tim2$TIM2_ICInit$185 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_ICInit$186 ==.
	push	#0x00
	Sstm8s_tim2$TIM2_ICInit$187 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_ICInit$188 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_ICInit$189 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_ICInit$190 ==.
00123$:
	Sstm8s_tim2$TIM2_ICInit$191 ==.
;	drivers/src/stm8s_tim2.c: 222: assert_param(IS_TIM2_IC_PRESCALER_OK(TIM2_ICPrescaler));
	tnz	(0x07, sp)
	jreq	00131$
	ld	a, (0x07, sp)
	cp	a, #0x04
	jreq	00131$
	Sstm8s_tim2$TIM2_ICInit$192 ==.
	ld	a, (0x07, sp)
	cp	a, #0x08
	jreq	00131$
	Sstm8s_tim2$TIM2_ICInit$193 ==.
	ld	a, (0x07, sp)
	cp	a, #0x0c
	jreq	00131$
	Sstm8s_tim2$TIM2_ICInit$194 ==.
	push	#0xde
	Sstm8s_tim2$TIM2_ICInit$195 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_ICInit$196 ==.
	push	#0x00
	Sstm8s_tim2$TIM2_ICInit$197 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_ICInit$198 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_ICInit$199 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_ICInit$200 ==.
00131$:
	Sstm8s_tim2$TIM2_ICInit$201 ==.
;	drivers/src/stm8s_tim2.c: 223: assert_param(IS_TIM2_IC_FILTER_OK(TIM2_ICFilter));
	ld	a, (0x08, sp)
	cp	a, #0x0f
	jrule	00142$
	push	#0xdf
	Sstm8s_tim2$TIM2_ICInit$202 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_ICInit$203 ==.
	push	#0x00
	Sstm8s_tim2$TIM2_ICInit$204 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_ICInit$205 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_ICInit$206 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_ICInit$207 ==.
00142$:
	Sstm8s_tim2$TIM2_ICInit$208 ==.
;	drivers/src/stm8s_tim2.c: 225: if (TIM2_Channel == TIM2_CHANNEL_1)
	tnz	(0x04, sp)
	jrne	00105$
	Sstm8s_tim2$TIM2_ICInit$209 ==.
	Sstm8s_tim2$TIM2_ICInit$210 ==.
;	drivers/src/stm8s_tim2.c: 228: TI1_Config((uint8_t)TIM2_ICPolarity,
	ld	a, (0x08, sp)
	push	a
	Sstm8s_tim2$TIM2_ICInit$211 ==.
	ld	a, (0x07, sp)
	push	a
	Sstm8s_tim2$TIM2_ICInit$212 ==.
	ld	a, (0x07, sp)
	push	a
	Sstm8s_tim2$TIM2_ICInit$213 ==.
	call	_TI1_Config
	addw	sp, #3
	Sstm8s_tim2$TIM2_ICInit$214 ==.
	Sstm8s_tim2$TIM2_ICInit$215 ==.
;	drivers/src/stm8s_tim2.c: 233: TIM2_SetIC1Prescaler(TIM2_ICPrescaler);
	ld	a, (0x07, sp)
	push	a
	Sstm8s_tim2$TIM2_ICInit$216 ==.
	call	_TIM2_SetIC1Prescaler
	pop	a
	Sstm8s_tim2$TIM2_ICInit$217 ==.
	Sstm8s_tim2$TIM2_ICInit$218 ==.
	jp	00107$
00105$:
	Sstm8s_tim2$TIM2_ICInit$219 ==.
;	drivers/src/stm8s_tim2.c: 235: else if (TIM2_Channel == TIM2_CHANNEL_2)
	ld	a, (0x01, sp)
	jreq	00102$
	Sstm8s_tim2$TIM2_ICInit$220 ==.
	Sstm8s_tim2$TIM2_ICInit$221 ==.
;	drivers/src/stm8s_tim2.c: 238: TI2_Config((uint8_t)TIM2_ICPolarity,
	ld	a, (0x08, sp)
	push	a
	Sstm8s_tim2$TIM2_ICInit$222 ==.
	ld	a, (0x07, sp)
	push	a
	Sstm8s_tim2$TIM2_ICInit$223 ==.
	ld	a, (0x07, sp)
	push	a
	Sstm8s_tim2$TIM2_ICInit$224 ==.
	call	_TI2_Config
	addw	sp, #3
	Sstm8s_tim2$TIM2_ICInit$225 ==.
	Sstm8s_tim2$TIM2_ICInit$226 ==.
;	drivers/src/stm8s_tim2.c: 243: TIM2_SetIC2Prescaler(TIM2_ICPrescaler);
	ld	a, (0x07, sp)
	push	a
	Sstm8s_tim2$TIM2_ICInit$227 ==.
	call	_TIM2_SetIC2Prescaler
	pop	a
	Sstm8s_tim2$TIM2_ICInit$228 ==.
	Sstm8s_tim2$TIM2_ICInit$229 ==.
	jra	00107$
00102$:
	Sstm8s_tim2$TIM2_ICInit$230 ==.
	Sstm8s_tim2$TIM2_ICInit$231 ==.
;	drivers/src/stm8s_tim2.c: 248: TI3_Config((uint8_t)TIM2_ICPolarity,
	ld	a, (0x08, sp)
	push	a
	Sstm8s_tim2$TIM2_ICInit$232 ==.
	ld	a, (0x07, sp)
	push	a
	Sstm8s_tim2$TIM2_ICInit$233 ==.
	ld	a, (0x07, sp)
	push	a
	Sstm8s_tim2$TIM2_ICInit$234 ==.
	call	_TI3_Config
	addw	sp, #3
	Sstm8s_tim2$TIM2_ICInit$235 ==.
	Sstm8s_tim2$TIM2_ICInit$236 ==.
;	drivers/src/stm8s_tim2.c: 253: TIM2_SetIC3Prescaler(TIM2_ICPrescaler);
	ld	a, (0x07, sp)
	push	a
	Sstm8s_tim2$TIM2_ICInit$237 ==.
	call	_TIM2_SetIC3Prescaler
	pop	a
	Sstm8s_tim2$TIM2_ICInit$238 ==.
	Sstm8s_tim2$TIM2_ICInit$239 ==.
00107$:
	Sstm8s_tim2$TIM2_ICInit$240 ==.
;	drivers/src/stm8s_tim2.c: 255: }
	pop	a
	Sstm8s_tim2$TIM2_ICInit$241 ==.
	Sstm8s_tim2$TIM2_ICInit$242 ==.
	XG$TIM2_ICInit$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_ICInit$243 ==.
	Sstm8s_tim2$TIM2_PWMIConfig$244 ==.
;	drivers/src/stm8s_tim2.c: 266: void TIM2_PWMIConfig(TIM2_Channel_TypeDef TIM2_Channel,
;	-----------------------------------------
;	 function TIM2_PWMIConfig
;	-----------------------------------------
_TIM2_PWMIConfig:
	Sstm8s_tim2$TIM2_PWMIConfig$245 ==.
	pushw	x
	Sstm8s_tim2$TIM2_PWMIConfig$246 ==.
	Sstm8s_tim2$TIM2_PWMIConfig$247 ==.
;	drivers/src/stm8s_tim2.c: 276: assert_param(IS_TIM2_PWMI_CHANNEL_OK(TIM2_Channel));
	tnz	(0x05, sp)
	jreq	00113$
	ld	a, (0x05, sp)
	dec	a
	jreq	00113$
	Sstm8s_tim2$TIM2_PWMIConfig$248 ==.
	push	#0x14
	Sstm8s_tim2$TIM2_PWMIConfig$249 ==.
	push	#0x01
	Sstm8s_tim2$TIM2_PWMIConfig$250 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_PWMIConfig$251 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_PWMIConfig$252 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_PWMIConfig$253 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_PWMIConfig$254 ==.
00113$:
	Sstm8s_tim2$TIM2_PWMIConfig$255 ==.
;	drivers/src/stm8s_tim2.c: 277: assert_param(IS_TIM2_IC_POLARITY_OK(TIM2_ICPolarity));
	ld	a, (0x06, sp)
	sub	a, #0x44
	jrne	00216$
	inc	a
	ld	(0x01, sp), a
	.byte 0xc5
00216$:
	clr	(0x01, sp)
00217$:
	Sstm8s_tim2$TIM2_PWMIConfig$256 ==.
	tnz	(0x06, sp)
	jreq	00118$
	tnz	(0x01, sp)
	jrne	00118$
	push	#0x15
	Sstm8s_tim2$TIM2_PWMIConfig$257 ==.
	push	#0x01
	Sstm8s_tim2$TIM2_PWMIConfig$258 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_PWMIConfig$259 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_PWMIConfig$260 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_PWMIConfig$261 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_PWMIConfig$262 ==.
00118$:
	Sstm8s_tim2$TIM2_PWMIConfig$263 ==.
;	drivers/src/stm8s_tim2.c: 278: assert_param(IS_TIM2_IC_SELECTION_OK(TIM2_ICSelection));
	ld	a, (0x07, sp)
	dec	a
	jrne	00221$
	ld	a, #0x01
	ld	(0x02, sp), a
	.byte 0xc5
00221$:
	clr	(0x02, sp)
00222$:
	Sstm8s_tim2$TIM2_PWMIConfig$264 ==.
	tnz	(0x02, sp)
	jrne	00123$
	ld	a, (0x07, sp)
	cp	a, #0x02
	jreq	00123$
	Sstm8s_tim2$TIM2_PWMIConfig$265 ==.
	ld	a, (0x07, sp)
	cp	a, #0x03
	jreq	00123$
	Sstm8s_tim2$TIM2_PWMIConfig$266 ==.
	push	#0x16
	Sstm8s_tim2$TIM2_PWMIConfig$267 ==.
	push	#0x01
	Sstm8s_tim2$TIM2_PWMIConfig$268 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_PWMIConfig$269 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_PWMIConfig$270 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_PWMIConfig$271 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_PWMIConfig$272 ==.
00123$:
	Sstm8s_tim2$TIM2_PWMIConfig$273 ==.
;	drivers/src/stm8s_tim2.c: 279: assert_param(IS_TIM2_IC_PRESCALER_OK(TIM2_ICPrescaler));
	tnz	(0x08, sp)
	jreq	00131$
	ld	a, (0x08, sp)
	cp	a, #0x04
	jreq	00131$
	Sstm8s_tim2$TIM2_PWMIConfig$274 ==.
	ld	a, (0x08, sp)
	cp	a, #0x08
	jreq	00131$
	Sstm8s_tim2$TIM2_PWMIConfig$275 ==.
	ld	a, (0x08, sp)
	cp	a, #0x0c
	jreq	00131$
	Sstm8s_tim2$TIM2_PWMIConfig$276 ==.
	push	#0x17
	Sstm8s_tim2$TIM2_PWMIConfig$277 ==.
	push	#0x01
	Sstm8s_tim2$TIM2_PWMIConfig$278 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_PWMIConfig$279 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_PWMIConfig$280 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_PWMIConfig$281 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_PWMIConfig$282 ==.
00131$:
	Sstm8s_tim2$TIM2_PWMIConfig$283 ==.
;	drivers/src/stm8s_tim2.c: 282: if (TIM2_ICPolarity != TIM2_ICPOLARITY_FALLING)
	tnz	(0x01, sp)
	jrne	00102$
	Sstm8s_tim2$TIM2_PWMIConfig$284 ==.
	Sstm8s_tim2$TIM2_PWMIConfig$285 ==.
;	drivers/src/stm8s_tim2.c: 284: icpolarity = (uint8_t)TIM2_ICPOLARITY_FALLING;
	ld	a, #0x44
	ld	(0x01, sp), a
	Sstm8s_tim2$TIM2_PWMIConfig$286 ==.
	jra	00103$
00102$:
	Sstm8s_tim2$TIM2_PWMIConfig$287 ==.
	Sstm8s_tim2$TIM2_PWMIConfig$288 ==.
;	drivers/src/stm8s_tim2.c: 288: icpolarity = (uint8_t)TIM2_ICPOLARITY_RISING;
	clr	(0x01, sp)
	Sstm8s_tim2$TIM2_PWMIConfig$289 ==.
00103$:
	Sstm8s_tim2$TIM2_PWMIConfig$290 ==.
;	drivers/src/stm8s_tim2.c: 292: if (TIM2_ICSelection == TIM2_ICSELECTION_DIRECTTI)
	ld	a, (0x02, sp)
	jreq	00105$
	Sstm8s_tim2$TIM2_PWMIConfig$291 ==.
	Sstm8s_tim2$TIM2_PWMIConfig$292 ==.
;	drivers/src/stm8s_tim2.c: 294: icselection = (uint8_t)TIM2_ICSELECTION_INDIRECTTI;
	ld	a, #0x02
	ld	(0x02, sp), a
	Sstm8s_tim2$TIM2_PWMIConfig$293 ==.
	jra	00106$
00105$:
	Sstm8s_tim2$TIM2_PWMIConfig$294 ==.
	Sstm8s_tim2$TIM2_PWMIConfig$295 ==.
;	drivers/src/stm8s_tim2.c: 298: icselection = (uint8_t)TIM2_ICSELECTION_DIRECTTI;
	ld	a, #0x01
	ld	(0x02, sp), a
	Sstm8s_tim2$TIM2_PWMIConfig$296 ==.
00106$:
	Sstm8s_tim2$TIM2_PWMIConfig$297 ==.
;	drivers/src/stm8s_tim2.c: 301: if (TIM2_Channel == TIM2_CHANNEL_1)
	tnz	(0x05, sp)
	jreq	00242$
	jp	00108$
00242$:
	Sstm8s_tim2$TIM2_PWMIConfig$298 ==.
	Sstm8s_tim2$TIM2_PWMIConfig$299 ==.
;	drivers/src/stm8s_tim2.c: 304: TI1_Config((uint8_t)TIM2_ICPolarity, (uint8_t)TIM2_ICSelection,
	ld	a, (0x09, sp)
	push	a
	Sstm8s_tim2$TIM2_PWMIConfig$300 ==.
	ld	a, (0x08, sp)
	push	a
	Sstm8s_tim2$TIM2_PWMIConfig$301 ==.
	ld	a, (0x08, sp)
	push	a
	Sstm8s_tim2$TIM2_PWMIConfig$302 ==.
	call	_TI1_Config
	addw	sp, #3
	Sstm8s_tim2$TIM2_PWMIConfig$303 ==.
	Sstm8s_tim2$TIM2_PWMIConfig$304 ==.
;	drivers/src/stm8s_tim2.c: 308: TIM2_SetIC1Prescaler(TIM2_ICPrescaler);
	ld	a, (0x08, sp)
	push	a
	Sstm8s_tim2$TIM2_PWMIConfig$305 ==.
	call	_TIM2_SetIC1Prescaler
	pop	a
	Sstm8s_tim2$TIM2_PWMIConfig$306 ==.
	Sstm8s_tim2$TIM2_PWMIConfig$307 ==.
;	drivers/src/stm8s_tim2.c: 311: TI2_Config(icpolarity, icselection, TIM2_ICFilter);
	ld	a, (0x09, sp)
	push	a
	Sstm8s_tim2$TIM2_PWMIConfig$308 ==.
	ld	a, (0x03, sp)
	push	a
	Sstm8s_tim2$TIM2_PWMIConfig$309 ==.
	ld	a, (0x03, sp)
	push	a
	Sstm8s_tim2$TIM2_PWMIConfig$310 ==.
	call	_TI2_Config
	addw	sp, #3
	Sstm8s_tim2$TIM2_PWMIConfig$311 ==.
	Sstm8s_tim2$TIM2_PWMIConfig$312 ==.
;	drivers/src/stm8s_tim2.c: 314: TIM2_SetIC2Prescaler(TIM2_ICPrescaler);
	ld	a, (0x08, sp)
	push	a
	Sstm8s_tim2$TIM2_PWMIConfig$313 ==.
	call	_TIM2_SetIC2Prescaler
	pop	a
	Sstm8s_tim2$TIM2_PWMIConfig$314 ==.
	Sstm8s_tim2$TIM2_PWMIConfig$315 ==.
	jp	00110$
00108$:
	Sstm8s_tim2$TIM2_PWMIConfig$316 ==.
	Sstm8s_tim2$TIM2_PWMIConfig$317 ==.
;	drivers/src/stm8s_tim2.c: 319: TI2_Config((uint8_t)TIM2_ICPolarity, (uint8_t)TIM2_ICSelection,
	ld	a, (0x09, sp)
	push	a
	Sstm8s_tim2$TIM2_PWMIConfig$318 ==.
	ld	a, (0x08, sp)
	push	a
	Sstm8s_tim2$TIM2_PWMIConfig$319 ==.
	ld	a, (0x08, sp)
	push	a
	Sstm8s_tim2$TIM2_PWMIConfig$320 ==.
	call	_TI2_Config
	addw	sp, #3
	Sstm8s_tim2$TIM2_PWMIConfig$321 ==.
	Sstm8s_tim2$TIM2_PWMIConfig$322 ==.
;	drivers/src/stm8s_tim2.c: 323: TIM2_SetIC2Prescaler(TIM2_ICPrescaler);
	ld	a, (0x08, sp)
	push	a
	Sstm8s_tim2$TIM2_PWMIConfig$323 ==.
	call	_TIM2_SetIC2Prescaler
	pop	a
	Sstm8s_tim2$TIM2_PWMIConfig$324 ==.
	Sstm8s_tim2$TIM2_PWMIConfig$325 ==.
;	drivers/src/stm8s_tim2.c: 326: TI1_Config((uint8_t)icpolarity, icselection, (uint8_t)TIM2_ICFilter);
	ld	a, (0x09, sp)
	push	a
	Sstm8s_tim2$TIM2_PWMIConfig$326 ==.
	ld	a, (0x03, sp)
	push	a
	Sstm8s_tim2$TIM2_PWMIConfig$327 ==.
	ld	a, (0x03, sp)
	push	a
	Sstm8s_tim2$TIM2_PWMIConfig$328 ==.
	call	_TI1_Config
	addw	sp, #3
	Sstm8s_tim2$TIM2_PWMIConfig$329 ==.
	Sstm8s_tim2$TIM2_PWMIConfig$330 ==.
;	drivers/src/stm8s_tim2.c: 329: TIM2_SetIC1Prescaler(TIM2_ICPrescaler);
	ld	a, (0x08, sp)
	push	a
	Sstm8s_tim2$TIM2_PWMIConfig$331 ==.
	call	_TIM2_SetIC1Prescaler
	pop	a
	Sstm8s_tim2$TIM2_PWMIConfig$332 ==.
	Sstm8s_tim2$TIM2_PWMIConfig$333 ==.
00110$:
	Sstm8s_tim2$TIM2_PWMIConfig$334 ==.
;	drivers/src/stm8s_tim2.c: 331: }
	popw	x
	Sstm8s_tim2$TIM2_PWMIConfig$335 ==.
	Sstm8s_tim2$TIM2_PWMIConfig$336 ==.
	XG$TIM2_PWMIConfig$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_PWMIConfig$337 ==.
	Sstm8s_tim2$TIM2_Cmd$338 ==.
;	drivers/src/stm8s_tim2.c: 339: void TIM2_Cmd(FunctionalState NewState)
;	-----------------------------------------
;	 function TIM2_Cmd
;	-----------------------------------------
_TIM2_Cmd:
	Sstm8s_tim2$TIM2_Cmd$339 ==.
	Sstm8s_tim2$TIM2_Cmd$340 ==.
;	drivers/src/stm8s_tim2.c: 342: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	dec	a
	jreq	00107$
	Sstm8s_tim2$TIM2_Cmd$341 ==.
	push	#0x56
	Sstm8s_tim2$TIM2_Cmd$342 ==.
	push	#0x01
	Sstm8s_tim2$TIM2_Cmd$343 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_Cmd$344 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_Cmd$345 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_Cmd$346 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_Cmd$347 ==.
00107$:
	Sstm8s_tim2$TIM2_Cmd$348 ==.
;	drivers/src/stm8s_tim2.c: 347: TIM2->CR1 |= (uint8_t)TIM2_CR1_CEN;
	ld	a, 0x5300
	Sstm8s_tim2$TIM2_Cmd$349 ==.
;	drivers/src/stm8s_tim2.c: 345: if (NewState != DISABLE)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim2$TIM2_Cmd$350 ==.
	Sstm8s_tim2$TIM2_Cmd$351 ==.
;	drivers/src/stm8s_tim2.c: 347: TIM2->CR1 |= (uint8_t)TIM2_CR1_CEN;
	or	a, #0x01
	ld	0x5300, a
	Sstm8s_tim2$TIM2_Cmd$352 ==.
	jra	00104$
00102$:
	Sstm8s_tim2$TIM2_Cmd$353 ==.
	Sstm8s_tim2$TIM2_Cmd$354 ==.
;	drivers/src/stm8s_tim2.c: 351: TIM2->CR1 &= (uint8_t)(~TIM2_CR1_CEN);
	and	a, #0xfe
	ld	0x5300, a
	Sstm8s_tim2$TIM2_Cmd$355 ==.
00104$:
	Sstm8s_tim2$TIM2_Cmd$356 ==.
;	drivers/src/stm8s_tim2.c: 353: }
	Sstm8s_tim2$TIM2_Cmd$357 ==.
	XG$TIM2_Cmd$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_Cmd$358 ==.
	Sstm8s_tim2$TIM2_ITConfig$359 ==.
;	drivers/src/stm8s_tim2.c: 368: void TIM2_ITConfig(TIM2_IT_TypeDef TIM2_IT, FunctionalState NewState)
;	-----------------------------------------
;	 function TIM2_ITConfig
;	-----------------------------------------
_TIM2_ITConfig:
	Sstm8s_tim2$TIM2_ITConfig$360 ==.
	push	a
	Sstm8s_tim2$TIM2_ITConfig$361 ==.
	Sstm8s_tim2$TIM2_ITConfig$362 ==.
;	drivers/src/stm8s_tim2.c: 371: assert_param(IS_TIM2_IT_OK(TIM2_IT));
	tnz	(0x04, sp)
	jreq	00106$
	ld	a, (0x04, sp)
	cp	a, #0x0f
	jrule	00107$
00106$:
	push	#0x73
	Sstm8s_tim2$TIM2_ITConfig$363 ==.
	push	#0x01
	Sstm8s_tim2$TIM2_ITConfig$364 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_ITConfig$365 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_ITConfig$366 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_ITConfig$367 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_ITConfig$368 ==.
00107$:
	Sstm8s_tim2$TIM2_ITConfig$369 ==.
;	drivers/src/stm8s_tim2.c: 372: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
	tnz	(0x05, sp)
	jreq	00112$
	ld	a, (0x05, sp)
	dec	a
	jreq	00112$
	Sstm8s_tim2$TIM2_ITConfig$370 ==.
	push	#0x74
	Sstm8s_tim2$TIM2_ITConfig$371 ==.
	push	#0x01
	Sstm8s_tim2$TIM2_ITConfig$372 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_ITConfig$373 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_ITConfig$374 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_ITConfig$375 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_ITConfig$376 ==.
00112$:
	Sstm8s_tim2$TIM2_ITConfig$377 ==.
;	drivers/src/stm8s_tim2.c: 377: TIM2->IER |= (uint8_t)TIM2_IT;
	ld	a, 0x5301
	Sstm8s_tim2$TIM2_ITConfig$378 ==.
;	drivers/src/stm8s_tim2.c: 374: if (NewState != DISABLE)
	tnz	(0x05, sp)
	jreq	00102$
	Sstm8s_tim2$TIM2_ITConfig$379 ==.
	Sstm8s_tim2$TIM2_ITConfig$380 ==.
;	drivers/src/stm8s_tim2.c: 377: TIM2->IER |= (uint8_t)TIM2_IT;
	or	a, (0x04, sp)
	ld	0x5301, a
	Sstm8s_tim2$TIM2_ITConfig$381 ==.
	jra	00104$
00102$:
	Sstm8s_tim2$TIM2_ITConfig$382 ==.
	Sstm8s_tim2$TIM2_ITConfig$383 ==.
;	drivers/src/stm8s_tim2.c: 382: TIM2->IER &= (uint8_t)(~TIM2_IT);
	push	a
	Sstm8s_tim2$TIM2_ITConfig$384 ==.
	ld	a, (0x05, sp)
	cpl	a
	ld	(0x02, sp), a
	pop	a
	Sstm8s_tim2$TIM2_ITConfig$385 ==.
	and	a, (0x01, sp)
	ld	0x5301, a
	Sstm8s_tim2$TIM2_ITConfig$386 ==.
00104$:
	Sstm8s_tim2$TIM2_ITConfig$387 ==.
;	drivers/src/stm8s_tim2.c: 384: }
	pop	a
	Sstm8s_tim2$TIM2_ITConfig$388 ==.
	Sstm8s_tim2$TIM2_ITConfig$389 ==.
	XG$TIM2_ITConfig$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_ITConfig$390 ==.
	Sstm8s_tim2$TIM2_UpdateDisableConfig$391 ==.
;	drivers/src/stm8s_tim2.c: 392: void TIM2_UpdateDisableConfig(FunctionalState NewState)
;	-----------------------------------------
;	 function TIM2_UpdateDisableConfig
;	-----------------------------------------
_TIM2_UpdateDisableConfig:
	Sstm8s_tim2$TIM2_UpdateDisableConfig$392 ==.
	Sstm8s_tim2$TIM2_UpdateDisableConfig$393 ==.
;	drivers/src/stm8s_tim2.c: 395: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	dec	a
	jreq	00107$
	Sstm8s_tim2$TIM2_UpdateDisableConfig$394 ==.
	push	#0x8b
	Sstm8s_tim2$TIM2_UpdateDisableConfig$395 ==.
	push	#0x01
	Sstm8s_tim2$TIM2_UpdateDisableConfig$396 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_UpdateDisableConfig$397 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_UpdateDisableConfig$398 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_UpdateDisableConfig$399 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_UpdateDisableConfig$400 ==.
00107$:
	Sstm8s_tim2$TIM2_UpdateDisableConfig$401 ==.
;	drivers/src/stm8s_tim2.c: 400: TIM2->CR1 |= (uint8_t)TIM2_CR1_UDIS;
	ld	a, 0x5300
	Sstm8s_tim2$TIM2_UpdateDisableConfig$402 ==.
;	drivers/src/stm8s_tim2.c: 398: if (NewState != DISABLE)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim2$TIM2_UpdateDisableConfig$403 ==.
	Sstm8s_tim2$TIM2_UpdateDisableConfig$404 ==.
;	drivers/src/stm8s_tim2.c: 400: TIM2->CR1 |= (uint8_t)TIM2_CR1_UDIS;
	or	a, #0x02
	ld	0x5300, a
	Sstm8s_tim2$TIM2_UpdateDisableConfig$405 ==.
	jra	00104$
00102$:
	Sstm8s_tim2$TIM2_UpdateDisableConfig$406 ==.
	Sstm8s_tim2$TIM2_UpdateDisableConfig$407 ==.
;	drivers/src/stm8s_tim2.c: 404: TIM2->CR1 &= (uint8_t)(~TIM2_CR1_UDIS);
	and	a, #0xfd
	ld	0x5300, a
	Sstm8s_tim2$TIM2_UpdateDisableConfig$408 ==.
00104$:
	Sstm8s_tim2$TIM2_UpdateDisableConfig$409 ==.
;	drivers/src/stm8s_tim2.c: 406: }
	Sstm8s_tim2$TIM2_UpdateDisableConfig$410 ==.
	XG$TIM2_UpdateDisableConfig$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_UpdateDisableConfig$411 ==.
	Sstm8s_tim2$TIM2_UpdateRequestConfig$412 ==.
;	drivers/src/stm8s_tim2.c: 416: void TIM2_UpdateRequestConfig(TIM2_UpdateSource_TypeDef TIM2_UpdateSource)
;	-----------------------------------------
;	 function TIM2_UpdateRequestConfig
;	-----------------------------------------
_TIM2_UpdateRequestConfig:
	Sstm8s_tim2$TIM2_UpdateRequestConfig$413 ==.
	Sstm8s_tim2$TIM2_UpdateRequestConfig$414 ==.
;	drivers/src/stm8s_tim2.c: 419: assert_param(IS_TIM2_UPDATE_SOURCE_OK(TIM2_UpdateSource));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	dec	a
	jreq	00107$
	Sstm8s_tim2$TIM2_UpdateRequestConfig$415 ==.
	push	#0xa3
	Sstm8s_tim2$TIM2_UpdateRequestConfig$416 ==.
	push	#0x01
	Sstm8s_tim2$TIM2_UpdateRequestConfig$417 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_UpdateRequestConfig$418 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_UpdateRequestConfig$419 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_UpdateRequestConfig$420 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_UpdateRequestConfig$421 ==.
00107$:
	Sstm8s_tim2$TIM2_UpdateRequestConfig$422 ==.
;	drivers/src/stm8s_tim2.c: 424: TIM2->CR1 |= (uint8_t)TIM2_CR1_URS;
	ld	a, 0x5300
	Sstm8s_tim2$TIM2_UpdateRequestConfig$423 ==.
;	drivers/src/stm8s_tim2.c: 422: if (TIM2_UpdateSource != TIM2_UPDATESOURCE_GLOBAL)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim2$TIM2_UpdateRequestConfig$424 ==.
	Sstm8s_tim2$TIM2_UpdateRequestConfig$425 ==.
;	drivers/src/stm8s_tim2.c: 424: TIM2->CR1 |= (uint8_t)TIM2_CR1_URS;
	or	a, #0x04
	ld	0x5300, a
	Sstm8s_tim2$TIM2_UpdateRequestConfig$426 ==.
	jra	00104$
00102$:
	Sstm8s_tim2$TIM2_UpdateRequestConfig$427 ==.
	Sstm8s_tim2$TIM2_UpdateRequestConfig$428 ==.
;	drivers/src/stm8s_tim2.c: 428: TIM2->CR1 &= (uint8_t)(~TIM2_CR1_URS);
	and	a, #0xfb
	ld	0x5300, a
	Sstm8s_tim2$TIM2_UpdateRequestConfig$429 ==.
00104$:
	Sstm8s_tim2$TIM2_UpdateRequestConfig$430 ==.
;	drivers/src/stm8s_tim2.c: 430: }
	Sstm8s_tim2$TIM2_UpdateRequestConfig$431 ==.
	XG$TIM2_UpdateRequestConfig$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_UpdateRequestConfig$432 ==.
	Sstm8s_tim2$TIM2_SelectOnePulseMode$433 ==.
;	drivers/src/stm8s_tim2.c: 440: void TIM2_SelectOnePulseMode(TIM2_OPMode_TypeDef TIM2_OPMode)
;	-----------------------------------------
;	 function TIM2_SelectOnePulseMode
;	-----------------------------------------
_TIM2_SelectOnePulseMode:
	Sstm8s_tim2$TIM2_SelectOnePulseMode$434 ==.
	Sstm8s_tim2$TIM2_SelectOnePulseMode$435 ==.
;	drivers/src/stm8s_tim2.c: 443: assert_param(IS_TIM2_OPM_MODE_OK(TIM2_OPMode));
	ld	a, (0x03, sp)
	dec	a
	jreq	00107$
	Sstm8s_tim2$TIM2_SelectOnePulseMode$436 ==.
	tnz	(0x03, sp)
	jreq	00107$
	push	#0xbb
	Sstm8s_tim2$TIM2_SelectOnePulseMode$437 ==.
	push	#0x01
	Sstm8s_tim2$TIM2_SelectOnePulseMode$438 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_SelectOnePulseMode$439 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_SelectOnePulseMode$440 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_SelectOnePulseMode$441 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_SelectOnePulseMode$442 ==.
00107$:
	Sstm8s_tim2$TIM2_SelectOnePulseMode$443 ==.
;	drivers/src/stm8s_tim2.c: 448: TIM2->CR1 |= (uint8_t)TIM2_CR1_OPM;
	ld	a, 0x5300
	Sstm8s_tim2$TIM2_SelectOnePulseMode$444 ==.
;	drivers/src/stm8s_tim2.c: 446: if (TIM2_OPMode != TIM2_OPMODE_REPETITIVE)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim2$TIM2_SelectOnePulseMode$445 ==.
	Sstm8s_tim2$TIM2_SelectOnePulseMode$446 ==.
;	drivers/src/stm8s_tim2.c: 448: TIM2->CR1 |= (uint8_t)TIM2_CR1_OPM;
	or	a, #0x08
	ld	0x5300, a
	Sstm8s_tim2$TIM2_SelectOnePulseMode$447 ==.
	jra	00104$
00102$:
	Sstm8s_tim2$TIM2_SelectOnePulseMode$448 ==.
	Sstm8s_tim2$TIM2_SelectOnePulseMode$449 ==.
;	drivers/src/stm8s_tim2.c: 452: TIM2->CR1 &= (uint8_t)(~TIM2_CR1_OPM);
	and	a, #0xf7
	ld	0x5300, a
	Sstm8s_tim2$TIM2_SelectOnePulseMode$450 ==.
00104$:
	Sstm8s_tim2$TIM2_SelectOnePulseMode$451 ==.
;	drivers/src/stm8s_tim2.c: 454: }
	Sstm8s_tim2$TIM2_SelectOnePulseMode$452 ==.
	XG$TIM2_SelectOnePulseMode$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_SelectOnePulseMode$453 ==.
	Sstm8s_tim2$TIM2_PrescalerConfig$454 ==.
;	drivers/src/stm8s_tim2.c: 484: void TIM2_PrescalerConfig(TIM2_Prescaler_TypeDef Prescaler,
;	-----------------------------------------
;	 function TIM2_PrescalerConfig
;	-----------------------------------------
_TIM2_PrescalerConfig:
	Sstm8s_tim2$TIM2_PrescalerConfig$455 ==.
	Sstm8s_tim2$TIM2_PrescalerConfig$456 ==.
;	drivers/src/stm8s_tim2.c: 488: assert_param(IS_TIM2_PRESCALER_RELOAD_OK(TIM2_PSCReloadMode));
	tnz	(0x04, sp)
	jreq	00104$
	ld	a, (0x04, sp)
	dec	a
	jreq	00104$
	Sstm8s_tim2$TIM2_PrescalerConfig$457 ==.
	push	#0xe8
	Sstm8s_tim2$TIM2_PrescalerConfig$458 ==.
	push	#0x01
	Sstm8s_tim2$TIM2_PrescalerConfig$459 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_PrescalerConfig$460 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_PrescalerConfig$461 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_PrescalerConfig$462 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_PrescalerConfig$463 ==.
00104$:
	Sstm8s_tim2$TIM2_PrescalerConfig$464 ==.
;	drivers/src/stm8s_tim2.c: 489: assert_param(IS_TIM2_PRESCALER_OK(Prescaler));
	tnz	(0x03, sp)
	jrne	00249$
	jp	00109$
00249$:
	ld	a, (0x03, sp)
	dec	a
	jrne	00251$
	jp	00109$
00251$:
	Sstm8s_tim2$TIM2_PrescalerConfig$465 ==.
	ld	a, (0x03, sp)
	cp	a, #0x02
	jrne	00254$
	jp	00109$
00254$:
	Sstm8s_tim2$TIM2_PrescalerConfig$466 ==.
	ld	a, (0x03, sp)
	cp	a, #0x03
	jrne	00257$
	jp	00109$
00257$:
	Sstm8s_tim2$TIM2_PrescalerConfig$467 ==.
	ld	a, (0x03, sp)
	cp	a, #0x04
	jrne	00260$
	jp	00109$
00260$:
	Sstm8s_tim2$TIM2_PrescalerConfig$468 ==.
	ld	a, (0x03, sp)
	cp	a, #0x05
	jrne	00263$
	jp	00109$
00263$:
	Sstm8s_tim2$TIM2_PrescalerConfig$469 ==.
	ld	a, (0x03, sp)
	cp	a, #0x06
	jrne	00266$
	jp	00109$
00266$:
	Sstm8s_tim2$TIM2_PrescalerConfig$470 ==.
	ld	a, (0x03, sp)
	cp	a, #0x07
	jrne	00269$
	jp	00109$
00269$:
	Sstm8s_tim2$TIM2_PrescalerConfig$471 ==.
	ld	a, (0x03, sp)
	cp	a, #0x08
	jrne	00272$
	jp	00109$
00272$:
	Sstm8s_tim2$TIM2_PrescalerConfig$472 ==.
	ld	a, (0x03, sp)
	cp	a, #0x09
	jreq	00109$
	Sstm8s_tim2$TIM2_PrescalerConfig$473 ==.
	ld	a, (0x03, sp)
	cp	a, #0x0a
	jreq	00109$
	Sstm8s_tim2$TIM2_PrescalerConfig$474 ==.
	ld	a, (0x03, sp)
	cp	a, #0x0b
	jreq	00109$
	Sstm8s_tim2$TIM2_PrescalerConfig$475 ==.
	ld	a, (0x03, sp)
	cp	a, #0x0c
	jreq	00109$
	Sstm8s_tim2$TIM2_PrescalerConfig$476 ==.
	ld	a, (0x03, sp)
	cp	a, #0x0d
	jreq	00109$
	Sstm8s_tim2$TIM2_PrescalerConfig$477 ==.
	ld	a, (0x03, sp)
	cp	a, #0x0e
	jreq	00109$
	Sstm8s_tim2$TIM2_PrescalerConfig$478 ==.
	ld	a, (0x03, sp)
	cp	a, #0x0f
	jreq	00109$
	Sstm8s_tim2$TIM2_PrescalerConfig$479 ==.
	push	#0xe9
	Sstm8s_tim2$TIM2_PrescalerConfig$480 ==.
	push	#0x01
	Sstm8s_tim2$TIM2_PrescalerConfig$481 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_PrescalerConfig$482 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_PrescalerConfig$483 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_PrescalerConfig$484 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_PrescalerConfig$485 ==.
00109$:
	Sstm8s_tim2$TIM2_PrescalerConfig$486 ==.
;	drivers/src/stm8s_tim2.c: 492: TIM2->PSCR = (uint8_t)Prescaler;
	ldw	x, #0x530c
	ld	a, (0x03, sp)
	ld	(x), a
	Sstm8s_tim2$TIM2_PrescalerConfig$487 ==.
;	drivers/src/stm8s_tim2.c: 495: TIM2->EGR = (uint8_t)TIM2_PSCReloadMode;
	ldw	x, #0x5304
	ld	a, (0x04, sp)
	ld	(x), a
	Sstm8s_tim2$TIM2_PrescalerConfig$488 ==.
;	drivers/src/stm8s_tim2.c: 496: }
	Sstm8s_tim2$TIM2_PrescalerConfig$489 ==.
	XG$TIM2_PrescalerConfig$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_PrescalerConfig$490 ==.
	Sstm8s_tim2$TIM2_ForcedOC1Config$491 ==.
;	drivers/src/stm8s_tim2.c: 507: void TIM2_ForcedOC1Config(TIM2_ForcedAction_TypeDef TIM2_ForcedAction)
;	-----------------------------------------
;	 function TIM2_ForcedOC1Config
;	-----------------------------------------
_TIM2_ForcedOC1Config:
	Sstm8s_tim2$TIM2_ForcedOC1Config$492 ==.
	Sstm8s_tim2$TIM2_ForcedOC1Config$493 ==.
;	drivers/src/stm8s_tim2.c: 510: assert_param(IS_TIM2_FORCED_ACTION_OK(TIM2_ForcedAction));
	ld	a, (0x03, sp)
	cp	a, #0x50
	jreq	00104$
	Sstm8s_tim2$TIM2_ForcedOC1Config$494 ==.
	ld	a, (0x03, sp)
	cp	a, #0x40
	jreq	00104$
	Sstm8s_tim2$TIM2_ForcedOC1Config$495 ==.
	push	#0xfe
	Sstm8s_tim2$TIM2_ForcedOC1Config$496 ==.
	push	#0x01
	Sstm8s_tim2$TIM2_ForcedOC1Config$497 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_ForcedOC1Config$498 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_ForcedOC1Config$499 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_ForcedOC1Config$500 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_ForcedOC1Config$501 ==.
00104$:
	Sstm8s_tim2$TIM2_ForcedOC1Config$502 ==.
;	drivers/src/stm8s_tim2.c: 513: TIM2->CCMR1  =  (uint8_t)((uint8_t)(TIM2->CCMR1 & (uint8_t)(~TIM2_CCMR_OCM))  
	ld	a, 0x5305
	and	a, #0x8f
	Sstm8s_tim2$TIM2_ForcedOC1Config$503 ==.
;	drivers/src/stm8s_tim2.c: 514: | (uint8_t)TIM2_ForcedAction);
	or	a, (0x03, sp)
	ld	0x5305, a
	Sstm8s_tim2$TIM2_ForcedOC1Config$504 ==.
;	drivers/src/stm8s_tim2.c: 515: }
	Sstm8s_tim2$TIM2_ForcedOC1Config$505 ==.
	XG$TIM2_ForcedOC1Config$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_ForcedOC1Config$506 ==.
	Sstm8s_tim2$TIM2_ForcedOC2Config$507 ==.
;	drivers/src/stm8s_tim2.c: 526: void TIM2_ForcedOC2Config(TIM2_ForcedAction_TypeDef TIM2_ForcedAction)
;	-----------------------------------------
;	 function TIM2_ForcedOC2Config
;	-----------------------------------------
_TIM2_ForcedOC2Config:
	Sstm8s_tim2$TIM2_ForcedOC2Config$508 ==.
	Sstm8s_tim2$TIM2_ForcedOC2Config$509 ==.
;	drivers/src/stm8s_tim2.c: 529: assert_param(IS_TIM2_FORCED_ACTION_OK(TIM2_ForcedAction));
	ld	a, (0x03, sp)
	cp	a, #0x50
	jreq	00104$
	Sstm8s_tim2$TIM2_ForcedOC2Config$510 ==.
	ld	a, (0x03, sp)
	cp	a, #0x40
	jreq	00104$
	Sstm8s_tim2$TIM2_ForcedOC2Config$511 ==.
	push	#0x11
	Sstm8s_tim2$TIM2_ForcedOC2Config$512 ==.
	push	#0x02
	Sstm8s_tim2$TIM2_ForcedOC2Config$513 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_ForcedOC2Config$514 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_ForcedOC2Config$515 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_ForcedOC2Config$516 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_ForcedOC2Config$517 ==.
00104$:
	Sstm8s_tim2$TIM2_ForcedOC2Config$518 ==.
;	drivers/src/stm8s_tim2.c: 532: TIM2->CCMR2 = (uint8_t)((uint8_t)(TIM2->CCMR2 & (uint8_t)(~TIM2_CCMR_OCM))  
	ld	a, 0x5306
	and	a, #0x8f
	Sstm8s_tim2$TIM2_ForcedOC2Config$519 ==.
;	drivers/src/stm8s_tim2.c: 533: | (uint8_t)TIM2_ForcedAction);
	or	a, (0x03, sp)
	ld	0x5306, a
	Sstm8s_tim2$TIM2_ForcedOC2Config$520 ==.
;	drivers/src/stm8s_tim2.c: 534: }
	Sstm8s_tim2$TIM2_ForcedOC2Config$521 ==.
	XG$TIM2_ForcedOC2Config$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_ForcedOC2Config$522 ==.
	Sstm8s_tim2$TIM2_ForcedOC3Config$523 ==.
;	drivers/src/stm8s_tim2.c: 545: void TIM2_ForcedOC3Config(TIM2_ForcedAction_TypeDef TIM2_ForcedAction)
;	-----------------------------------------
;	 function TIM2_ForcedOC3Config
;	-----------------------------------------
_TIM2_ForcedOC3Config:
	Sstm8s_tim2$TIM2_ForcedOC3Config$524 ==.
	Sstm8s_tim2$TIM2_ForcedOC3Config$525 ==.
;	drivers/src/stm8s_tim2.c: 548: assert_param(IS_TIM2_FORCED_ACTION_OK(TIM2_ForcedAction));
	ld	a, (0x03, sp)
	cp	a, #0x50
	jreq	00104$
	Sstm8s_tim2$TIM2_ForcedOC3Config$526 ==.
	ld	a, (0x03, sp)
	cp	a, #0x40
	jreq	00104$
	Sstm8s_tim2$TIM2_ForcedOC3Config$527 ==.
	push	#0x24
	Sstm8s_tim2$TIM2_ForcedOC3Config$528 ==.
	push	#0x02
	Sstm8s_tim2$TIM2_ForcedOC3Config$529 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_ForcedOC3Config$530 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_ForcedOC3Config$531 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_ForcedOC3Config$532 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_ForcedOC3Config$533 ==.
00104$:
	Sstm8s_tim2$TIM2_ForcedOC3Config$534 ==.
;	drivers/src/stm8s_tim2.c: 551: TIM2->CCMR3  =  (uint8_t)((uint8_t)(TIM2->CCMR3 & (uint8_t)(~TIM2_CCMR_OCM))
	ld	a, 0x5307
	and	a, #0x8f
	Sstm8s_tim2$TIM2_ForcedOC3Config$535 ==.
;	drivers/src/stm8s_tim2.c: 552: | (uint8_t)TIM2_ForcedAction);
	or	a, (0x03, sp)
	ld	0x5307, a
	Sstm8s_tim2$TIM2_ForcedOC3Config$536 ==.
;	drivers/src/stm8s_tim2.c: 553: }
	Sstm8s_tim2$TIM2_ForcedOC3Config$537 ==.
	XG$TIM2_ForcedOC3Config$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_ForcedOC3Config$538 ==.
	Sstm8s_tim2$TIM2_ARRPreloadConfig$539 ==.
;	drivers/src/stm8s_tim2.c: 561: void TIM2_ARRPreloadConfig(FunctionalState NewState)
;	-----------------------------------------
;	 function TIM2_ARRPreloadConfig
;	-----------------------------------------
_TIM2_ARRPreloadConfig:
	Sstm8s_tim2$TIM2_ARRPreloadConfig$540 ==.
	Sstm8s_tim2$TIM2_ARRPreloadConfig$541 ==.
;	drivers/src/stm8s_tim2.c: 564: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	dec	a
	jreq	00107$
	Sstm8s_tim2$TIM2_ARRPreloadConfig$542 ==.
	push	#0x34
	Sstm8s_tim2$TIM2_ARRPreloadConfig$543 ==.
	push	#0x02
	Sstm8s_tim2$TIM2_ARRPreloadConfig$544 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_ARRPreloadConfig$545 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_ARRPreloadConfig$546 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_ARRPreloadConfig$547 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_ARRPreloadConfig$548 ==.
00107$:
	Sstm8s_tim2$TIM2_ARRPreloadConfig$549 ==.
;	drivers/src/stm8s_tim2.c: 569: TIM2->CR1 |= (uint8_t)TIM2_CR1_ARPE;
	ld	a, 0x5300
	Sstm8s_tim2$TIM2_ARRPreloadConfig$550 ==.
;	drivers/src/stm8s_tim2.c: 567: if (NewState != DISABLE)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim2$TIM2_ARRPreloadConfig$551 ==.
	Sstm8s_tim2$TIM2_ARRPreloadConfig$552 ==.
;	drivers/src/stm8s_tim2.c: 569: TIM2->CR1 |= (uint8_t)TIM2_CR1_ARPE;
	or	a, #0x80
	ld	0x5300, a
	Sstm8s_tim2$TIM2_ARRPreloadConfig$553 ==.
	jra	00104$
00102$:
	Sstm8s_tim2$TIM2_ARRPreloadConfig$554 ==.
	Sstm8s_tim2$TIM2_ARRPreloadConfig$555 ==.
;	drivers/src/stm8s_tim2.c: 573: TIM2->CR1 &= (uint8_t)(~TIM2_CR1_ARPE);
	and	a, #0x7f
	ld	0x5300, a
	Sstm8s_tim2$TIM2_ARRPreloadConfig$556 ==.
00104$:
	Sstm8s_tim2$TIM2_ARRPreloadConfig$557 ==.
;	drivers/src/stm8s_tim2.c: 575: }
	Sstm8s_tim2$TIM2_ARRPreloadConfig$558 ==.
	XG$TIM2_ARRPreloadConfig$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_ARRPreloadConfig$559 ==.
	Sstm8s_tim2$TIM2_OC1PreloadConfig$560 ==.
;	drivers/src/stm8s_tim2.c: 583: void TIM2_OC1PreloadConfig(FunctionalState NewState)
;	-----------------------------------------
;	 function TIM2_OC1PreloadConfig
;	-----------------------------------------
_TIM2_OC1PreloadConfig:
	Sstm8s_tim2$TIM2_OC1PreloadConfig$561 ==.
	Sstm8s_tim2$TIM2_OC1PreloadConfig$562 ==.
;	drivers/src/stm8s_tim2.c: 586: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	dec	a
	jreq	00107$
	Sstm8s_tim2$TIM2_OC1PreloadConfig$563 ==.
	push	#0x4a
	Sstm8s_tim2$TIM2_OC1PreloadConfig$564 ==.
	push	#0x02
	Sstm8s_tim2$TIM2_OC1PreloadConfig$565 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_OC1PreloadConfig$566 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_OC1PreloadConfig$567 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_OC1PreloadConfig$568 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_OC1PreloadConfig$569 ==.
00107$:
	Sstm8s_tim2$TIM2_OC1PreloadConfig$570 ==.
;	drivers/src/stm8s_tim2.c: 591: TIM2->CCMR1 |= (uint8_t)TIM2_CCMR_OCxPE;
	ld	a, 0x5305
	Sstm8s_tim2$TIM2_OC1PreloadConfig$571 ==.
;	drivers/src/stm8s_tim2.c: 589: if (NewState != DISABLE)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim2$TIM2_OC1PreloadConfig$572 ==.
	Sstm8s_tim2$TIM2_OC1PreloadConfig$573 ==.
;	drivers/src/stm8s_tim2.c: 591: TIM2->CCMR1 |= (uint8_t)TIM2_CCMR_OCxPE;
	or	a, #0x08
	ld	0x5305, a
	Sstm8s_tim2$TIM2_OC1PreloadConfig$574 ==.
	jra	00104$
00102$:
	Sstm8s_tim2$TIM2_OC1PreloadConfig$575 ==.
	Sstm8s_tim2$TIM2_OC1PreloadConfig$576 ==.
;	drivers/src/stm8s_tim2.c: 595: TIM2->CCMR1 &= (uint8_t)(~TIM2_CCMR_OCxPE);
	and	a, #0xf7
	ld	0x5305, a
	Sstm8s_tim2$TIM2_OC1PreloadConfig$577 ==.
00104$:
	Sstm8s_tim2$TIM2_OC1PreloadConfig$578 ==.
;	drivers/src/stm8s_tim2.c: 597: }
	Sstm8s_tim2$TIM2_OC1PreloadConfig$579 ==.
	XG$TIM2_OC1PreloadConfig$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_OC1PreloadConfig$580 ==.
	Sstm8s_tim2$TIM2_OC2PreloadConfig$581 ==.
;	drivers/src/stm8s_tim2.c: 605: void TIM2_OC2PreloadConfig(FunctionalState NewState)
;	-----------------------------------------
;	 function TIM2_OC2PreloadConfig
;	-----------------------------------------
_TIM2_OC2PreloadConfig:
	Sstm8s_tim2$TIM2_OC2PreloadConfig$582 ==.
	Sstm8s_tim2$TIM2_OC2PreloadConfig$583 ==.
;	drivers/src/stm8s_tim2.c: 608: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	dec	a
	jreq	00107$
	Sstm8s_tim2$TIM2_OC2PreloadConfig$584 ==.
	push	#0x60
	Sstm8s_tim2$TIM2_OC2PreloadConfig$585 ==.
	push	#0x02
	Sstm8s_tim2$TIM2_OC2PreloadConfig$586 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_OC2PreloadConfig$587 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_OC2PreloadConfig$588 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_OC2PreloadConfig$589 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_OC2PreloadConfig$590 ==.
00107$:
	Sstm8s_tim2$TIM2_OC2PreloadConfig$591 ==.
;	drivers/src/stm8s_tim2.c: 613: TIM2->CCMR2 |= (uint8_t)TIM2_CCMR_OCxPE;
	ld	a, 0x5306
	Sstm8s_tim2$TIM2_OC2PreloadConfig$592 ==.
;	drivers/src/stm8s_tim2.c: 611: if (NewState != DISABLE)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim2$TIM2_OC2PreloadConfig$593 ==.
	Sstm8s_tim2$TIM2_OC2PreloadConfig$594 ==.
;	drivers/src/stm8s_tim2.c: 613: TIM2->CCMR2 |= (uint8_t)TIM2_CCMR_OCxPE;
	or	a, #0x08
	ld	0x5306, a
	Sstm8s_tim2$TIM2_OC2PreloadConfig$595 ==.
	jra	00104$
00102$:
	Sstm8s_tim2$TIM2_OC2PreloadConfig$596 ==.
	Sstm8s_tim2$TIM2_OC2PreloadConfig$597 ==.
;	drivers/src/stm8s_tim2.c: 617: TIM2->CCMR2 &= (uint8_t)(~TIM2_CCMR_OCxPE);
	and	a, #0xf7
	ld	0x5306, a
	Sstm8s_tim2$TIM2_OC2PreloadConfig$598 ==.
00104$:
	Sstm8s_tim2$TIM2_OC2PreloadConfig$599 ==.
;	drivers/src/stm8s_tim2.c: 619: }
	Sstm8s_tim2$TIM2_OC2PreloadConfig$600 ==.
	XG$TIM2_OC2PreloadConfig$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_OC2PreloadConfig$601 ==.
	Sstm8s_tim2$TIM2_OC3PreloadConfig$602 ==.
;	drivers/src/stm8s_tim2.c: 627: void TIM2_OC3PreloadConfig(FunctionalState NewState)
;	-----------------------------------------
;	 function TIM2_OC3PreloadConfig
;	-----------------------------------------
_TIM2_OC3PreloadConfig:
	Sstm8s_tim2$TIM2_OC3PreloadConfig$603 ==.
	Sstm8s_tim2$TIM2_OC3PreloadConfig$604 ==.
;	drivers/src/stm8s_tim2.c: 630: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	dec	a
	jreq	00107$
	Sstm8s_tim2$TIM2_OC3PreloadConfig$605 ==.
	push	#0x76
	Sstm8s_tim2$TIM2_OC3PreloadConfig$606 ==.
	push	#0x02
	Sstm8s_tim2$TIM2_OC3PreloadConfig$607 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_OC3PreloadConfig$608 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_OC3PreloadConfig$609 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_OC3PreloadConfig$610 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_OC3PreloadConfig$611 ==.
00107$:
	Sstm8s_tim2$TIM2_OC3PreloadConfig$612 ==.
;	drivers/src/stm8s_tim2.c: 635: TIM2->CCMR3 |= (uint8_t)TIM2_CCMR_OCxPE;
	ld	a, 0x5307
	Sstm8s_tim2$TIM2_OC3PreloadConfig$613 ==.
;	drivers/src/stm8s_tim2.c: 633: if (NewState != DISABLE)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim2$TIM2_OC3PreloadConfig$614 ==.
	Sstm8s_tim2$TIM2_OC3PreloadConfig$615 ==.
;	drivers/src/stm8s_tim2.c: 635: TIM2->CCMR3 |= (uint8_t)TIM2_CCMR_OCxPE;
	or	a, #0x08
	ld	0x5307, a
	Sstm8s_tim2$TIM2_OC3PreloadConfig$616 ==.
	jra	00104$
00102$:
	Sstm8s_tim2$TIM2_OC3PreloadConfig$617 ==.
	Sstm8s_tim2$TIM2_OC3PreloadConfig$618 ==.
;	drivers/src/stm8s_tim2.c: 639: TIM2->CCMR3 &= (uint8_t)(~TIM2_CCMR_OCxPE);
	and	a, #0xf7
	ld	0x5307, a
	Sstm8s_tim2$TIM2_OC3PreloadConfig$619 ==.
00104$:
	Sstm8s_tim2$TIM2_OC3PreloadConfig$620 ==.
;	drivers/src/stm8s_tim2.c: 641: }
	Sstm8s_tim2$TIM2_OC3PreloadConfig$621 ==.
	XG$TIM2_OC3PreloadConfig$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_OC3PreloadConfig$622 ==.
	Sstm8s_tim2$TIM2_GenerateEvent$623 ==.
;	drivers/src/stm8s_tim2.c: 653: void TIM2_GenerateEvent(TIM2_EventSource_TypeDef TIM2_EventSource)
;	-----------------------------------------
;	 function TIM2_GenerateEvent
;	-----------------------------------------
_TIM2_GenerateEvent:
	Sstm8s_tim2$TIM2_GenerateEvent$624 ==.
	Sstm8s_tim2$TIM2_GenerateEvent$625 ==.
;	drivers/src/stm8s_tim2.c: 656: assert_param(IS_TIM2_EVENT_SOURCE_OK(TIM2_EventSource));
	tnz	(0x03, sp)
	jrne	00104$
	push	#0x90
	Sstm8s_tim2$TIM2_GenerateEvent$626 ==.
	push	#0x02
	Sstm8s_tim2$TIM2_GenerateEvent$627 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_GenerateEvent$628 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_GenerateEvent$629 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_GenerateEvent$630 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_GenerateEvent$631 ==.
00104$:
	Sstm8s_tim2$TIM2_GenerateEvent$632 ==.
;	drivers/src/stm8s_tim2.c: 659: TIM2->EGR = (uint8_t)TIM2_EventSource;
	ldw	x, #0x5304
	ld	a, (0x03, sp)
	ld	(x), a
	Sstm8s_tim2$TIM2_GenerateEvent$633 ==.
;	drivers/src/stm8s_tim2.c: 660: }
	Sstm8s_tim2$TIM2_GenerateEvent$634 ==.
	XG$TIM2_GenerateEvent$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_GenerateEvent$635 ==.
	Sstm8s_tim2$TIM2_OC1PolarityConfig$636 ==.
;	drivers/src/stm8s_tim2.c: 670: void TIM2_OC1PolarityConfig(TIM2_OCPolarity_TypeDef TIM2_OCPolarity)
;	-----------------------------------------
;	 function TIM2_OC1PolarityConfig
;	-----------------------------------------
_TIM2_OC1PolarityConfig:
	Sstm8s_tim2$TIM2_OC1PolarityConfig$637 ==.
	Sstm8s_tim2$TIM2_OC1PolarityConfig$638 ==.
;	drivers/src/stm8s_tim2.c: 673: assert_param(IS_TIM2_OC_POLARITY_OK(TIM2_OCPolarity));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	cp	a, #0x22
	jreq	00107$
	Sstm8s_tim2$TIM2_OC1PolarityConfig$639 ==.
	push	#0xa1
	Sstm8s_tim2$TIM2_OC1PolarityConfig$640 ==.
	push	#0x02
	Sstm8s_tim2$TIM2_OC1PolarityConfig$641 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_OC1PolarityConfig$642 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_OC1PolarityConfig$643 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_OC1PolarityConfig$644 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_OC1PolarityConfig$645 ==.
00107$:
	Sstm8s_tim2$TIM2_OC1PolarityConfig$646 ==.
;	drivers/src/stm8s_tim2.c: 678: TIM2->CCER1 |= (uint8_t)TIM2_CCER1_CC1P;
	ld	a, 0x5308
	Sstm8s_tim2$TIM2_OC1PolarityConfig$647 ==.
;	drivers/src/stm8s_tim2.c: 676: if (TIM2_OCPolarity != TIM2_OCPOLARITY_HIGH)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim2$TIM2_OC1PolarityConfig$648 ==.
	Sstm8s_tim2$TIM2_OC1PolarityConfig$649 ==.
;	drivers/src/stm8s_tim2.c: 678: TIM2->CCER1 |= (uint8_t)TIM2_CCER1_CC1P;
	or	a, #0x02
	ld	0x5308, a
	Sstm8s_tim2$TIM2_OC1PolarityConfig$650 ==.
	jra	00104$
00102$:
	Sstm8s_tim2$TIM2_OC1PolarityConfig$651 ==.
	Sstm8s_tim2$TIM2_OC1PolarityConfig$652 ==.
;	drivers/src/stm8s_tim2.c: 682: TIM2->CCER1 &= (uint8_t)(~TIM2_CCER1_CC1P);
	and	a, #0xfd
	ld	0x5308, a
	Sstm8s_tim2$TIM2_OC1PolarityConfig$653 ==.
00104$:
	Sstm8s_tim2$TIM2_OC1PolarityConfig$654 ==.
;	drivers/src/stm8s_tim2.c: 684: }
	Sstm8s_tim2$TIM2_OC1PolarityConfig$655 ==.
	XG$TIM2_OC1PolarityConfig$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_OC1PolarityConfig$656 ==.
	Sstm8s_tim2$TIM2_OC2PolarityConfig$657 ==.
;	drivers/src/stm8s_tim2.c: 694: void TIM2_OC2PolarityConfig(TIM2_OCPolarity_TypeDef TIM2_OCPolarity)
;	-----------------------------------------
;	 function TIM2_OC2PolarityConfig
;	-----------------------------------------
_TIM2_OC2PolarityConfig:
	Sstm8s_tim2$TIM2_OC2PolarityConfig$658 ==.
	Sstm8s_tim2$TIM2_OC2PolarityConfig$659 ==.
;	drivers/src/stm8s_tim2.c: 697: assert_param(IS_TIM2_OC_POLARITY_OK(TIM2_OCPolarity));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	cp	a, #0x22
	jreq	00107$
	Sstm8s_tim2$TIM2_OC2PolarityConfig$660 ==.
	push	#0xb9
	Sstm8s_tim2$TIM2_OC2PolarityConfig$661 ==.
	push	#0x02
	Sstm8s_tim2$TIM2_OC2PolarityConfig$662 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_OC2PolarityConfig$663 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_OC2PolarityConfig$664 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_OC2PolarityConfig$665 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_OC2PolarityConfig$666 ==.
00107$:
	Sstm8s_tim2$TIM2_OC2PolarityConfig$667 ==.
;	drivers/src/stm8s_tim2.c: 702: TIM2->CCER1 |= TIM2_CCER1_CC2P;
	ld	a, 0x5308
	Sstm8s_tim2$TIM2_OC2PolarityConfig$668 ==.
;	drivers/src/stm8s_tim2.c: 700: if (TIM2_OCPolarity != TIM2_OCPOLARITY_HIGH)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim2$TIM2_OC2PolarityConfig$669 ==.
	Sstm8s_tim2$TIM2_OC2PolarityConfig$670 ==.
;	drivers/src/stm8s_tim2.c: 702: TIM2->CCER1 |= TIM2_CCER1_CC2P;
	or	a, #0x20
	ld	0x5308, a
	Sstm8s_tim2$TIM2_OC2PolarityConfig$671 ==.
	jra	00104$
00102$:
	Sstm8s_tim2$TIM2_OC2PolarityConfig$672 ==.
	Sstm8s_tim2$TIM2_OC2PolarityConfig$673 ==.
;	drivers/src/stm8s_tim2.c: 706: TIM2->CCER1 &= (uint8_t)(~TIM2_CCER1_CC2P);
	and	a, #0xdf
	ld	0x5308, a
	Sstm8s_tim2$TIM2_OC2PolarityConfig$674 ==.
00104$:
	Sstm8s_tim2$TIM2_OC2PolarityConfig$675 ==.
;	drivers/src/stm8s_tim2.c: 708: }
	Sstm8s_tim2$TIM2_OC2PolarityConfig$676 ==.
	XG$TIM2_OC2PolarityConfig$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_OC2PolarityConfig$677 ==.
	Sstm8s_tim2$TIM2_OC3PolarityConfig$678 ==.
;	drivers/src/stm8s_tim2.c: 718: void TIM2_OC3PolarityConfig(TIM2_OCPolarity_TypeDef TIM2_OCPolarity)
;	-----------------------------------------
;	 function TIM2_OC3PolarityConfig
;	-----------------------------------------
_TIM2_OC3PolarityConfig:
	Sstm8s_tim2$TIM2_OC3PolarityConfig$679 ==.
	Sstm8s_tim2$TIM2_OC3PolarityConfig$680 ==.
;	drivers/src/stm8s_tim2.c: 721: assert_param(IS_TIM2_OC_POLARITY_OK(TIM2_OCPolarity));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	cp	a, #0x22
	jreq	00107$
	Sstm8s_tim2$TIM2_OC3PolarityConfig$681 ==.
	push	#0xd1
	Sstm8s_tim2$TIM2_OC3PolarityConfig$682 ==.
	push	#0x02
	Sstm8s_tim2$TIM2_OC3PolarityConfig$683 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_OC3PolarityConfig$684 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_OC3PolarityConfig$685 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_OC3PolarityConfig$686 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_OC3PolarityConfig$687 ==.
00107$:
	Sstm8s_tim2$TIM2_OC3PolarityConfig$688 ==.
;	drivers/src/stm8s_tim2.c: 726: TIM2->CCER2 |= (uint8_t)TIM2_CCER2_CC3P;
	ld	a, 0x5309
	Sstm8s_tim2$TIM2_OC3PolarityConfig$689 ==.
;	drivers/src/stm8s_tim2.c: 724: if (TIM2_OCPolarity != TIM2_OCPOLARITY_HIGH)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim2$TIM2_OC3PolarityConfig$690 ==.
	Sstm8s_tim2$TIM2_OC3PolarityConfig$691 ==.
;	drivers/src/stm8s_tim2.c: 726: TIM2->CCER2 |= (uint8_t)TIM2_CCER2_CC3P;
	or	a, #0x02
	ld	0x5309, a
	Sstm8s_tim2$TIM2_OC3PolarityConfig$692 ==.
	jra	00104$
00102$:
	Sstm8s_tim2$TIM2_OC3PolarityConfig$693 ==.
	Sstm8s_tim2$TIM2_OC3PolarityConfig$694 ==.
;	drivers/src/stm8s_tim2.c: 730: TIM2->CCER2 &= (uint8_t)(~TIM2_CCER2_CC3P);
	and	a, #0xfd
	ld	0x5309, a
	Sstm8s_tim2$TIM2_OC3PolarityConfig$695 ==.
00104$:
	Sstm8s_tim2$TIM2_OC3PolarityConfig$696 ==.
;	drivers/src/stm8s_tim2.c: 732: }
	Sstm8s_tim2$TIM2_OC3PolarityConfig$697 ==.
	XG$TIM2_OC3PolarityConfig$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_OC3PolarityConfig$698 ==.
	Sstm8s_tim2$TIM2_CCxCmd$699 ==.
;	drivers/src/stm8s_tim2.c: 745: void TIM2_CCxCmd(TIM2_Channel_TypeDef TIM2_Channel, FunctionalState NewState)
;	-----------------------------------------
;	 function TIM2_CCxCmd
;	-----------------------------------------
_TIM2_CCxCmd:
	Sstm8s_tim2$TIM2_CCxCmd$700 ==.
	push	a
	Sstm8s_tim2$TIM2_CCxCmd$701 ==.
	Sstm8s_tim2$TIM2_CCxCmd$702 ==.
;	drivers/src/stm8s_tim2.c: 748: assert_param(IS_TIM2_CHANNEL_OK(TIM2_Channel));
	ld	a, (0x04, sp)
	dec	a
	jrne	00182$
	ld	a, #0x01
	ld	(0x01, sp), a
	.byte 0xc5
00182$:
	clr	(0x01, sp)
00183$:
	Sstm8s_tim2$TIM2_CCxCmd$703 ==.
	tnz	(0x04, sp)
	jreq	00119$
	tnz	(0x01, sp)
	jrne	00119$
	ld	a, (0x04, sp)
	cp	a, #0x02
	jreq	00119$
	Sstm8s_tim2$TIM2_CCxCmd$704 ==.
	push	#0xec
	Sstm8s_tim2$TIM2_CCxCmd$705 ==.
	push	#0x02
	Sstm8s_tim2$TIM2_CCxCmd$706 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_CCxCmd$707 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_CCxCmd$708 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_CCxCmd$709 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_CCxCmd$710 ==.
00119$:
	Sstm8s_tim2$TIM2_CCxCmd$711 ==.
;	drivers/src/stm8s_tim2.c: 749: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
	tnz	(0x05, sp)
	jreq	00127$
	ld	a, (0x05, sp)
	dec	a
	jreq	00127$
	Sstm8s_tim2$TIM2_CCxCmd$712 ==.
	push	#0xed
	Sstm8s_tim2$TIM2_CCxCmd$713 ==.
	push	#0x02
	Sstm8s_tim2$TIM2_CCxCmd$714 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_CCxCmd$715 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_CCxCmd$716 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_CCxCmd$717 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_CCxCmd$718 ==.
00127$:
	Sstm8s_tim2$TIM2_CCxCmd$719 ==.
;	drivers/src/stm8s_tim2.c: 751: if (TIM2_Channel == TIM2_CHANNEL_1)
	tnz	(0x04, sp)
	jrne	00114$
	Sstm8s_tim2$TIM2_CCxCmd$720 ==.
;	drivers/src/stm8s_tim2.c: 756: TIM2->CCER1 |= (uint8_t)TIM2_CCER1_CC1E;
	ld	a, 0x5308
	Sstm8s_tim2$TIM2_CCxCmd$721 ==.
	Sstm8s_tim2$TIM2_CCxCmd$722 ==.
;	drivers/src/stm8s_tim2.c: 754: if (NewState != DISABLE)
	tnz	(0x05, sp)
	jreq	00102$
	Sstm8s_tim2$TIM2_CCxCmd$723 ==.
	Sstm8s_tim2$TIM2_CCxCmd$724 ==.
;	drivers/src/stm8s_tim2.c: 756: TIM2->CCER1 |= (uint8_t)TIM2_CCER1_CC1E;
	or	a, #0x01
	ld	0x5308, a
	Sstm8s_tim2$TIM2_CCxCmd$725 ==.
	jp	00116$
00102$:
	Sstm8s_tim2$TIM2_CCxCmd$726 ==.
	Sstm8s_tim2$TIM2_CCxCmd$727 ==.
;	drivers/src/stm8s_tim2.c: 760: TIM2->CCER1 &= (uint8_t)(~TIM2_CCER1_CC1E);
	and	a, #0xfe
	ld	0x5308, a
	Sstm8s_tim2$TIM2_CCxCmd$728 ==.
	jp	00116$
00114$:
	Sstm8s_tim2$TIM2_CCxCmd$729 ==.
;	drivers/src/stm8s_tim2.c: 764: else if (TIM2_Channel == TIM2_CHANNEL_2)
	ld	a, (0x01, sp)
	jreq	00111$
	Sstm8s_tim2$TIM2_CCxCmd$730 ==.
;	drivers/src/stm8s_tim2.c: 756: TIM2->CCER1 |= (uint8_t)TIM2_CCER1_CC1E;
	ld	a, 0x5308
	Sstm8s_tim2$TIM2_CCxCmd$731 ==.
	Sstm8s_tim2$TIM2_CCxCmd$732 ==.
;	drivers/src/stm8s_tim2.c: 767: if (NewState != DISABLE)
	tnz	(0x05, sp)
	jreq	00105$
	Sstm8s_tim2$TIM2_CCxCmd$733 ==.
	Sstm8s_tim2$TIM2_CCxCmd$734 ==.
;	drivers/src/stm8s_tim2.c: 769: TIM2->CCER1 |= (uint8_t)TIM2_CCER1_CC2E;
	or	a, #0x10
	ld	0x5308, a
	Sstm8s_tim2$TIM2_CCxCmd$735 ==.
	jra	00116$
00105$:
	Sstm8s_tim2$TIM2_CCxCmd$736 ==.
	Sstm8s_tim2$TIM2_CCxCmd$737 ==.
;	drivers/src/stm8s_tim2.c: 773: TIM2->CCER1 &= (uint8_t)(~TIM2_CCER1_CC2E);
	and	a, #0xef
	ld	0x5308, a
	Sstm8s_tim2$TIM2_CCxCmd$738 ==.
	jra	00116$
00111$:
	Sstm8s_tim2$TIM2_CCxCmd$739 ==.
;	drivers/src/stm8s_tim2.c: 781: TIM2->CCER2 |= (uint8_t)TIM2_CCER2_CC3E;
	ld	a, 0x5309
	Sstm8s_tim2$TIM2_CCxCmd$740 ==.
	Sstm8s_tim2$TIM2_CCxCmd$741 ==.
;	drivers/src/stm8s_tim2.c: 779: if (NewState != DISABLE)
	tnz	(0x05, sp)
	jreq	00108$
	Sstm8s_tim2$TIM2_CCxCmd$742 ==.
	Sstm8s_tim2$TIM2_CCxCmd$743 ==.
;	drivers/src/stm8s_tim2.c: 781: TIM2->CCER2 |= (uint8_t)TIM2_CCER2_CC3E;
	or	a, #0x01
	ld	0x5309, a
	Sstm8s_tim2$TIM2_CCxCmd$744 ==.
	jra	00116$
00108$:
	Sstm8s_tim2$TIM2_CCxCmd$745 ==.
	Sstm8s_tim2$TIM2_CCxCmd$746 ==.
;	drivers/src/stm8s_tim2.c: 785: TIM2->CCER2 &= (uint8_t)(~TIM2_CCER2_CC3E);
	and	a, #0xfe
	ld	0x5309, a
	Sstm8s_tim2$TIM2_CCxCmd$747 ==.
00116$:
	Sstm8s_tim2$TIM2_CCxCmd$748 ==.
;	drivers/src/stm8s_tim2.c: 788: }
	pop	a
	Sstm8s_tim2$TIM2_CCxCmd$749 ==.
	Sstm8s_tim2$TIM2_CCxCmd$750 ==.
	XG$TIM2_CCxCmd$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_CCxCmd$751 ==.
	Sstm8s_tim2$TIM2_SelectOCxM$752 ==.
;	drivers/src/stm8s_tim2.c: 810: void TIM2_SelectOCxM(TIM2_Channel_TypeDef TIM2_Channel, TIM2_OCMode_TypeDef TIM2_OCMode)
;	-----------------------------------------
;	 function TIM2_SelectOCxM
;	-----------------------------------------
_TIM2_SelectOCxM:
	Sstm8s_tim2$TIM2_SelectOCxM$753 ==.
	push	a
	Sstm8s_tim2$TIM2_SelectOCxM$754 ==.
	Sstm8s_tim2$TIM2_SelectOCxM$755 ==.
;	drivers/src/stm8s_tim2.c: 813: assert_param(IS_TIM2_CHANNEL_OK(TIM2_Channel));
	ld	a, (0x04, sp)
	dec	a
	jrne	00206$
	ld	a, #0x01
	ld	(0x01, sp), a
	.byte 0xc5
00206$:
	clr	(0x01, sp)
00207$:
	Sstm8s_tim2$TIM2_SelectOCxM$756 ==.
	tnz	(0x04, sp)
	jreq	00110$
	tnz	(0x01, sp)
	jrne	00110$
	ld	a, (0x04, sp)
	cp	a, #0x02
	jreq	00110$
	Sstm8s_tim2$TIM2_SelectOCxM$757 ==.
	push	#0x2d
	Sstm8s_tim2$TIM2_SelectOCxM$758 ==.
	push	#0x03
	Sstm8s_tim2$TIM2_SelectOCxM$759 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_SelectOCxM$760 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_SelectOCxM$761 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_SelectOCxM$762 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_SelectOCxM$763 ==.
00110$:
	Sstm8s_tim2$TIM2_SelectOCxM$764 ==.
;	drivers/src/stm8s_tim2.c: 814: assert_param(IS_TIM2_OCM_OK(TIM2_OCMode));
	tnz	(0x05, sp)
	jreq	00118$
	ld	a, (0x05, sp)
	cp	a, #0x10
	jreq	00118$
	Sstm8s_tim2$TIM2_SelectOCxM$765 ==.
	ld	a, (0x05, sp)
	cp	a, #0x20
	jreq	00118$
	Sstm8s_tim2$TIM2_SelectOCxM$766 ==.
	ld	a, (0x05, sp)
	cp	a, #0x30
	jreq	00118$
	Sstm8s_tim2$TIM2_SelectOCxM$767 ==.
	ld	a, (0x05, sp)
	cp	a, #0x60
	jreq	00118$
	Sstm8s_tim2$TIM2_SelectOCxM$768 ==.
	ld	a, (0x05, sp)
	cp	a, #0x70
	jreq	00118$
	Sstm8s_tim2$TIM2_SelectOCxM$769 ==.
	ld	a, (0x05, sp)
	cp	a, #0x50
	jreq	00118$
	Sstm8s_tim2$TIM2_SelectOCxM$770 ==.
	ld	a, (0x05, sp)
	cp	a, #0x40
	jreq	00118$
	Sstm8s_tim2$TIM2_SelectOCxM$771 ==.
	push	#0x2e
	Sstm8s_tim2$TIM2_SelectOCxM$772 ==.
	push	#0x03
	Sstm8s_tim2$TIM2_SelectOCxM$773 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_SelectOCxM$774 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_SelectOCxM$775 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_SelectOCxM$776 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_SelectOCxM$777 ==.
00118$:
	Sstm8s_tim2$TIM2_SelectOCxM$778 ==.
;	drivers/src/stm8s_tim2.c: 816: if (TIM2_Channel == TIM2_CHANNEL_1)
	tnz	(0x04, sp)
	jrne	00105$
	Sstm8s_tim2$TIM2_SelectOCxM$779 ==.
	Sstm8s_tim2$TIM2_SelectOCxM$780 ==.
;	drivers/src/stm8s_tim2.c: 819: TIM2->CCER1 &= (uint8_t)(~TIM2_CCER1_CC1E);
	bres	21256, #0
	Sstm8s_tim2$TIM2_SelectOCxM$781 ==.
;	drivers/src/stm8s_tim2.c: 822: TIM2->CCMR1 = (uint8_t)((uint8_t)(TIM2->CCMR1 & (uint8_t)(~TIM2_CCMR_OCM))
	ld	a, 0x5305
	and	a, #0x8f
	Sstm8s_tim2$TIM2_SelectOCxM$782 ==.
;	drivers/src/stm8s_tim2.c: 823: | (uint8_t)TIM2_OCMode);
	or	a, (0x05, sp)
	ld	0x5305, a
	Sstm8s_tim2$TIM2_SelectOCxM$783 ==.
	jra	00107$
00105$:
	Sstm8s_tim2$TIM2_SelectOCxM$784 ==.
;	drivers/src/stm8s_tim2.c: 825: else if (TIM2_Channel == TIM2_CHANNEL_2)
	ld	a, (0x01, sp)
	jreq	00102$
	Sstm8s_tim2$TIM2_SelectOCxM$785 ==.
	Sstm8s_tim2$TIM2_SelectOCxM$786 ==.
;	drivers/src/stm8s_tim2.c: 828: TIM2->CCER1 &= (uint8_t)(~TIM2_CCER1_CC2E);
	bres	21256, #4
	Sstm8s_tim2$TIM2_SelectOCxM$787 ==.
;	drivers/src/stm8s_tim2.c: 831: TIM2->CCMR2 = (uint8_t)((uint8_t)(TIM2->CCMR2 & (uint8_t)(~TIM2_CCMR_OCM))
	ld	a, 0x5306
	and	a, #0x8f
	Sstm8s_tim2$TIM2_SelectOCxM$788 ==.
;	drivers/src/stm8s_tim2.c: 832: | (uint8_t)TIM2_OCMode);
	or	a, (0x05, sp)
	ld	0x5306, a
	Sstm8s_tim2$TIM2_SelectOCxM$789 ==.
	jra	00107$
00102$:
	Sstm8s_tim2$TIM2_SelectOCxM$790 ==.
	Sstm8s_tim2$TIM2_SelectOCxM$791 ==.
;	drivers/src/stm8s_tim2.c: 837: TIM2->CCER2 &= (uint8_t)(~TIM2_CCER2_CC3E);
	bres	21257, #0
	Sstm8s_tim2$TIM2_SelectOCxM$792 ==.
;	drivers/src/stm8s_tim2.c: 840: TIM2->CCMR3 = (uint8_t)((uint8_t)(TIM2->CCMR3 & (uint8_t)(~TIM2_CCMR_OCM))
	ld	a, 0x5307
	and	a, #0x8f
	Sstm8s_tim2$TIM2_SelectOCxM$793 ==.
;	drivers/src/stm8s_tim2.c: 841: | (uint8_t)TIM2_OCMode);
	or	a, (0x05, sp)
	ld	0x5307, a
	Sstm8s_tim2$TIM2_SelectOCxM$794 ==.
00107$:
	Sstm8s_tim2$TIM2_SelectOCxM$795 ==.
;	drivers/src/stm8s_tim2.c: 843: }
	pop	a
	Sstm8s_tim2$TIM2_SelectOCxM$796 ==.
	Sstm8s_tim2$TIM2_SelectOCxM$797 ==.
	XG$TIM2_SelectOCxM$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_SelectOCxM$798 ==.
	Sstm8s_tim2$TIM2_SetCounter$799 ==.
;	drivers/src/stm8s_tim2.c: 851: void TIM2_SetCounter(uint16_t Counter)
;	-----------------------------------------
;	 function TIM2_SetCounter
;	-----------------------------------------
_TIM2_SetCounter:
	Sstm8s_tim2$TIM2_SetCounter$800 ==.
	Sstm8s_tim2$TIM2_SetCounter$801 ==.
;	drivers/src/stm8s_tim2.c: 854: TIM2->CNTRH = (uint8_t)(Counter >> 8);
	ld	a, (0x03, sp)
	ld	0x530a, a
	Sstm8s_tim2$TIM2_SetCounter$802 ==.
;	drivers/src/stm8s_tim2.c: 855: TIM2->CNTRL = (uint8_t)(Counter);
	ld	a, (0x04, sp)
	ld	0x530b, a
	Sstm8s_tim2$TIM2_SetCounter$803 ==.
;	drivers/src/stm8s_tim2.c: 856: }
	Sstm8s_tim2$TIM2_SetCounter$804 ==.
	XG$TIM2_SetCounter$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_SetCounter$805 ==.
	Sstm8s_tim2$TIM2_SetAutoreload$806 ==.
;	drivers/src/stm8s_tim2.c: 864: void TIM2_SetAutoreload(uint16_t Autoreload)
;	-----------------------------------------
;	 function TIM2_SetAutoreload
;	-----------------------------------------
_TIM2_SetAutoreload:
	Sstm8s_tim2$TIM2_SetAutoreload$807 ==.
	Sstm8s_tim2$TIM2_SetAutoreload$808 ==.
;	drivers/src/stm8s_tim2.c: 867: TIM2->ARRH = (uint8_t)(Autoreload >> 8);
	ld	a, (0x03, sp)
	ld	0x530d, a
	Sstm8s_tim2$TIM2_SetAutoreload$809 ==.
;	drivers/src/stm8s_tim2.c: 868: TIM2->ARRL = (uint8_t)(Autoreload);
	ld	a, (0x04, sp)
	ld	0x530e, a
	Sstm8s_tim2$TIM2_SetAutoreload$810 ==.
;	drivers/src/stm8s_tim2.c: 869: }
	Sstm8s_tim2$TIM2_SetAutoreload$811 ==.
	XG$TIM2_SetAutoreload$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_SetAutoreload$812 ==.
	Sstm8s_tim2$TIM2_SetCompare1$813 ==.
;	drivers/src/stm8s_tim2.c: 877: void TIM2_SetCompare1(uint16_t Compare1)
;	-----------------------------------------
;	 function TIM2_SetCompare1
;	-----------------------------------------
_TIM2_SetCompare1:
	Sstm8s_tim2$TIM2_SetCompare1$814 ==.
	Sstm8s_tim2$TIM2_SetCompare1$815 ==.
;	drivers/src/stm8s_tim2.c: 880: TIM2->CCR1H = (uint8_t)(Compare1 >> 8);
	ld	a, (0x03, sp)
	ld	0x530f, a
	Sstm8s_tim2$TIM2_SetCompare1$816 ==.
;	drivers/src/stm8s_tim2.c: 881: TIM2->CCR1L = (uint8_t)(Compare1);
	ld	a, (0x04, sp)
	ld	0x5310, a
	Sstm8s_tim2$TIM2_SetCompare1$817 ==.
;	drivers/src/stm8s_tim2.c: 882: }
	Sstm8s_tim2$TIM2_SetCompare1$818 ==.
	XG$TIM2_SetCompare1$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_SetCompare1$819 ==.
	Sstm8s_tim2$TIM2_SetCompare2$820 ==.
;	drivers/src/stm8s_tim2.c: 890: void TIM2_SetCompare2(uint16_t Compare2)
;	-----------------------------------------
;	 function TIM2_SetCompare2
;	-----------------------------------------
_TIM2_SetCompare2:
	Sstm8s_tim2$TIM2_SetCompare2$821 ==.
	Sstm8s_tim2$TIM2_SetCompare2$822 ==.
;	drivers/src/stm8s_tim2.c: 893: TIM2->CCR2H = (uint8_t)(Compare2 >> 8);
	ld	a, (0x03, sp)
	ld	0x5311, a
	Sstm8s_tim2$TIM2_SetCompare2$823 ==.
;	drivers/src/stm8s_tim2.c: 894: TIM2->CCR2L = (uint8_t)(Compare2);
	ld	a, (0x04, sp)
	ld	0x5312, a
	Sstm8s_tim2$TIM2_SetCompare2$824 ==.
;	drivers/src/stm8s_tim2.c: 895: }
	Sstm8s_tim2$TIM2_SetCompare2$825 ==.
	XG$TIM2_SetCompare2$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_SetCompare2$826 ==.
	Sstm8s_tim2$TIM2_SetCompare3$827 ==.
;	drivers/src/stm8s_tim2.c: 903: void TIM2_SetCompare3(uint16_t Compare3)
;	-----------------------------------------
;	 function TIM2_SetCompare3
;	-----------------------------------------
_TIM2_SetCompare3:
	Sstm8s_tim2$TIM2_SetCompare3$828 ==.
	Sstm8s_tim2$TIM2_SetCompare3$829 ==.
;	drivers/src/stm8s_tim2.c: 906: TIM2->CCR3H = (uint8_t)(Compare3 >> 8);
	ld	a, (0x03, sp)
	ld	0x5313, a
	Sstm8s_tim2$TIM2_SetCompare3$830 ==.
;	drivers/src/stm8s_tim2.c: 907: TIM2->CCR3L = (uint8_t)(Compare3);
	ld	a, (0x04, sp)
	ld	0x5314, a
	Sstm8s_tim2$TIM2_SetCompare3$831 ==.
;	drivers/src/stm8s_tim2.c: 908: }
	Sstm8s_tim2$TIM2_SetCompare3$832 ==.
	XG$TIM2_SetCompare3$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_SetCompare3$833 ==.
	Sstm8s_tim2$TIM2_SetIC1Prescaler$834 ==.
;	drivers/src/stm8s_tim2.c: 920: void TIM2_SetIC1Prescaler(TIM2_ICPSC_TypeDef TIM2_IC1Prescaler)
;	-----------------------------------------
;	 function TIM2_SetIC1Prescaler
;	-----------------------------------------
_TIM2_SetIC1Prescaler:
	Sstm8s_tim2$TIM2_SetIC1Prescaler$835 ==.
	Sstm8s_tim2$TIM2_SetIC1Prescaler$836 ==.
;	drivers/src/stm8s_tim2.c: 923: assert_param(IS_TIM2_IC_PRESCALER_OK(TIM2_IC1Prescaler));
	tnz	(0x03, sp)
	jreq	00104$
	ld	a, (0x03, sp)
	cp	a, #0x04
	jreq	00104$
	Sstm8s_tim2$TIM2_SetIC1Prescaler$837 ==.
	ld	a, (0x03, sp)
	cp	a, #0x08
	jreq	00104$
	Sstm8s_tim2$TIM2_SetIC1Prescaler$838 ==.
	ld	a, (0x03, sp)
	cp	a, #0x0c
	jreq	00104$
	Sstm8s_tim2$TIM2_SetIC1Prescaler$839 ==.
	push	#0x9b
	Sstm8s_tim2$TIM2_SetIC1Prescaler$840 ==.
	push	#0x03
	Sstm8s_tim2$TIM2_SetIC1Prescaler$841 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_SetIC1Prescaler$842 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_SetIC1Prescaler$843 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_SetIC1Prescaler$844 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_SetIC1Prescaler$845 ==.
00104$:
	Sstm8s_tim2$TIM2_SetIC1Prescaler$846 ==.
;	drivers/src/stm8s_tim2.c: 926: TIM2->CCMR1 = (uint8_t)((uint8_t)(TIM2->CCMR1 & (uint8_t)(~TIM2_CCMR_ICxPSC))
	ld	a, 0x5305
	and	a, #0xf3
	Sstm8s_tim2$TIM2_SetIC1Prescaler$847 ==.
;	drivers/src/stm8s_tim2.c: 927: | (uint8_t)TIM2_IC1Prescaler);
	or	a, (0x03, sp)
	ld	0x5305, a
	Sstm8s_tim2$TIM2_SetIC1Prescaler$848 ==.
;	drivers/src/stm8s_tim2.c: 928: }
	Sstm8s_tim2$TIM2_SetIC1Prescaler$849 ==.
	XG$TIM2_SetIC1Prescaler$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_SetIC1Prescaler$850 ==.
	Sstm8s_tim2$TIM2_SetIC2Prescaler$851 ==.
;	drivers/src/stm8s_tim2.c: 940: void TIM2_SetIC2Prescaler(TIM2_ICPSC_TypeDef TIM2_IC2Prescaler)
;	-----------------------------------------
;	 function TIM2_SetIC2Prescaler
;	-----------------------------------------
_TIM2_SetIC2Prescaler:
	Sstm8s_tim2$TIM2_SetIC2Prescaler$852 ==.
	Sstm8s_tim2$TIM2_SetIC2Prescaler$853 ==.
;	drivers/src/stm8s_tim2.c: 943: assert_param(IS_TIM2_IC_PRESCALER_OK(TIM2_IC2Prescaler));
	tnz	(0x03, sp)
	jreq	00104$
	ld	a, (0x03, sp)
	cp	a, #0x04
	jreq	00104$
	Sstm8s_tim2$TIM2_SetIC2Prescaler$854 ==.
	ld	a, (0x03, sp)
	cp	a, #0x08
	jreq	00104$
	Sstm8s_tim2$TIM2_SetIC2Prescaler$855 ==.
	ld	a, (0x03, sp)
	cp	a, #0x0c
	jreq	00104$
	Sstm8s_tim2$TIM2_SetIC2Prescaler$856 ==.
	push	#0xaf
	Sstm8s_tim2$TIM2_SetIC2Prescaler$857 ==.
	push	#0x03
	Sstm8s_tim2$TIM2_SetIC2Prescaler$858 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_SetIC2Prescaler$859 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_SetIC2Prescaler$860 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_SetIC2Prescaler$861 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_SetIC2Prescaler$862 ==.
00104$:
	Sstm8s_tim2$TIM2_SetIC2Prescaler$863 ==.
;	drivers/src/stm8s_tim2.c: 946: TIM2->CCMR2 = (uint8_t)((uint8_t)(TIM2->CCMR2 & (uint8_t)(~TIM2_CCMR_ICxPSC))
	ld	a, 0x5306
	and	a, #0xf3
	Sstm8s_tim2$TIM2_SetIC2Prescaler$864 ==.
;	drivers/src/stm8s_tim2.c: 947: | (uint8_t)TIM2_IC2Prescaler);
	or	a, (0x03, sp)
	ld	0x5306, a
	Sstm8s_tim2$TIM2_SetIC2Prescaler$865 ==.
;	drivers/src/stm8s_tim2.c: 948: }
	Sstm8s_tim2$TIM2_SetIC2Prescaler$866 ==.
	XG$TIM2_SetIC2Prescaler$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_SetIC2Prescaler$867 ==.
	Sstm8s_tim2$TIM2_SetIC3Prescaler$868 ==.
;	drivers/src/stm8s_tim2.c: 960: void TIM2_SetIC3Prescaler(TIM2_ICPSC_TypeDef TIM2_IC3Prescaler)
;	-----------------------------------------
;	 function TIM2_SetIC3Prescaler
;	-----------------------------------------
_TIM2_SetIC3Prescaler:
	Sstm8s_tim2$TIM2_SetIC3Prescaler$869 ==.
	Sstm8s_tim2$TIM2_SetIC3Prescaler$870 ==.
;	drivers/src/stm8s_tim2.c: 964: assert_param(IS_TIM2_IC_PRESCALER_OK(TIM2_IC3Prescaler));
	tnz	(0x03, sp)
	jreq	00104$
	ld	a, (0x03, sp)
	cp	a, #0x04
	jreq	00104$
	Sstm8s_tim2$TIM2_SetIC3Prescaler$871 ==.
	ld	a, (0x03, sp)
	cp	a, #0x08
	jreq	00104$
	Sstm8s_tim2$TIM2_SetIC3Prescaler$872 ==.
	ld	a, (0x03, sp)
	cp	a, #0x0c
	jreq	00104$
	Sstm8s_tim2$TIM2_SetIC3Prescaler$873 ==.
	push	#0xc4
	Sstm8s_tim2$TIM2_SetIC3Prescaler$874 ==.
	push	#0x03
	Sstm8s_tim2$TIM2_SetIC3Prescaler$875 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_SetIC3Prescaler$876 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_SetIC3Prescaler$877 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_SetIC3Prescaler$878 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_SetIC3Prescaler$879 ==.
00104$:
	Sstm8s_tim2$TIM2_SetIC3Prescaler$880 ==.
;	drivers/src/stm8s_tim2.c: 966: TIM2->CCMR3 = (uint8_t)((uint8_t)(TIM2->CCMR3 & (uint8_t)(~TIM2_CCMR_ICxPSC))
	ld	a, 0x5307
	and	a, #0xf3
	Sstm8s_tim2$TIM2_SetIC3Prescaler$881 ==.
;	drivers/src/stm8s_tim2.c: 967: | (uint8_t)TIM2_IC3Prescaler);
	or	a, (0x03, sp)
	ld	0x5307, a
	Sstm8s_tim2$TIM2_SetIC3Prescaler$882 ==.
;	drivers/src/stm8s_tim2.c: 968: }
	Sstm8s_tim2$TIM2_SetIC3Prescaler$883 ==.
	XG$TIM2_SetIC3Prescaler$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_SetIC3Prescaler$884 ==.
	Sstm8s_tim2$TIM2_GetCapture1$885 ==.
;	drivers/src/stm8s_tim2.c: 975: uint16_t TIM2_GetCapture1(void)
;	-----------------------------------------
;	 function TIM2_GetCapture1
;	-----------------------------------------
_TIM2_GetCapture1:
	Sstm8s_tim2$TIM2_GetCapture1$886 ==.
	pushw	x
	Sstm8s_tim2$TIM2_GetCapture1$887 ==.
	Sstm8s_tim2$TIM2_GetCapture1$888 ==.
;	drivers/src/stm8s_tim2.c: 981: tmpccr1h = TIM2->CCR1H;
	ld	a, 0x530f
	ld	xh, a
	Sstm8s_tim2$TIM2_GetCapture1$889 ==.
;	drivers/src/stm8s_tim2.c: 982: tmpccr1l = TIM2->CCR1L;
	ld	a, 0x5310
	Sstm8s_tim2$TIM2_GetCapture1$890 ==.
;	drivers/src/stm8s_tim2.c: 984: tmpccr1 = (uint16_t)(tmpccr1l);
	ld	xl, a
	clr	a
	Sstm8s_tim2$TIM2_GetCapture1$891 ==.
;	drivers/src/stm8s_tim2.c: 985: tmpccr1 |= (uint16_t)((uint16_t)tmpccr1h << 8);
	clr	(0x02, sp)
	pushw	x
	Sstm8s_tim2$TIM2_GetCapture1$892 ==.
	or	a, (1, sp)
	popw	x
	Sstm8s_tim2$TIM2_GetCapture1$893 ==.
	rrwa	x
	or	a, (0x02, sp)
	ld	xl, a
	Sstm8s_tim2$TIM2_GetCapture1$894 ==.
;	drivers/src/stm8s_tim2.c: 987: return (uint16_t)tmpccr1;
	Sstm8s_tim2$TIM2_GetCapture1$895 ==.
;	drivers/src/stm8s_tim2.c: 988: }
	addw	sp, #2
	Sstm8s_tim2$TIM2_GetCapture1$896 ==.
	Sstm8s_tim2$TIM2_GetCapture1$897 ==.
	XG$TIM2_GetCapture1$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_GetCapture1$898 ==.
	Sstm8s_tim2$TIM2_GetCapture2$899 ==.
;	drivers/src/stm8s_tim2.c: 995: uint16_t TIM2_GetCapture2(void)
;	-----------------------------------------
;	 function TIM2_GetCapture2
;	-----------------------------------------
_TIM2_GetCapture2:
	Sstm8s_tim2$TIM2_GetCapture2$900 ==.
	pushw	x
	Sstm8s_tim2$TIM2_GetCapture2$901 ==.
	Sstm8s_tim2$TIM2_GetCapture2$902 ==.
;	drivers/src/stm8s_tim2.c: 1001: tmpccr2h = TIM2->CCR2H;
	ld	a, 0x5311
	ld	xh, a
	Sstm8s_tim2$TIM2_GetCapture2$903 ==.
;	drivers/src/stm8s_tim2.c: 1002: tmpccr2l = TIM2->CCR2L;
	ld	a, 0x5312
	Sstm8s_tim2$TIM2_GetCapture2$904 ==.
;	drivers/src/stm8s_tim2.c: 1004: tmpccr2 = (uint16_t)(tmpccr2l);
	ld	xl, a
	clr	a
	Sstm8s_tim2$TIM2_GetCapture2$905 ==.
;	drivers/src/stm8s_tim2.c: 1005: tmpccr2 |= (uint16_t)((uint16_t)tmpccr2h << 8);
	clr	(0x02, sp)
	pushw	x
	Sstm8s_tim2$TIM2_GetCapture2$906 ==.
	or	a, (1, sp)
	popw	x
	Sstm8s_tim2$TIM2_GetCapture2$907 ==.
	rrwa	x
	or	a, (0x02, sp)
	ld	xl, a
	Sstm8s_tim2$TIM2_GetCapture2$908 ==.
;	drivers/src/stm8s_tim2.c: 1007: return (uint16_t)tmpccr2;
	Sstm8s_tim2$TIM2_GetCapture2$909 ==.
;	drivers/src/stm8s_tim2.c: 1008: }
	addw	sp, #2
	Sstm8s_tim2$TIM2_GetCapture2$910 ==.
	Sstm8s_tim2$TIM2_GetCapture2$911 ==.
	XG$TIM2_GetCapture2$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_GetCapture2$912 ==.
	Sstm8s_tim2$TIM2_GetCapture3$913 ==.
;	drivers/src/stm8s_tim2.c: 1015: uint16_t TIM2_GetCapture3(void)
;	-----------------------------------------
;	 function TIM2_GetCapture3
;	-----------------------------------------
_TIM2_GetCapture3:
	Sstm8s_tim2$TIM2_GetCapture3$914 ==.
	pushw	x
	Sstm8s_tim2$TIM2_GetCapture3$915 ==.
	Sstm8s_tim2$TIM2_GetCapture3$916 ==.
;	drivers/src/stm8s_tim2.c: 1021: tmpccr3h = TIM2->CCR3H;
	ld	a, 0x5313
	ld	xh, a
	Sstm8s_tim2$TIM2_GetCapture3$917 ==.
;	drivers/src/stm8s_tim2.c: 1022: tmpccr3l = TIM2->CCR3L;
	ld	a, 0x5314
	Sstm8s_tim2$TIM2_GetCapture3$918 ==.
;	drivers/src/stm8s_tim2.c: 1024: tmpccr3 = (uint16_t)(tmpccr3l);
	ld	xl, a
	clr	a
	Sstm8s_tim2$TIM2_GetCapture3$919 ==.
;	drivers/src/stm8s_tim2.c: 1025: tmpccr3 |= (uint16_t)((uint16_t)tmpccr3h << 8);
	clr	(0x02, sp)
	pushw	x
	Sstm8s_tim2$TIM2_GetCapture3$920 ==.
	or	a, (1, sp)
	popw	x
	Sstm8s_tim2$TIM2_GetCapture3$921 ==.
	rrwa	x
	or	a, (0x02, sp)
	ld	xl, a
	Sstm8s_tim2$TIM2_GetCapture3$922 ==.
;	drivers/src/stm8s_tim2.c: 1027: return (uint16_t)tmpccr3;
	Sstm8s_tim2$TIM2_GetCapture3$923 ==.
;	drivers/src/stm8s_tim2.c: 1028: }
	addw	sp, #2
	Sstm8s_tim2$TIM2_GetCapture3$924 ==.
	Sstm8s_tim2$TIM2_GetCapture3$925 ==.
	XG$TIM2_GetCapture3$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_GetCapture3$926 ==.
	Sstm8s_tim2$TIM2_GetCounter$927 ==.
;	drivers/src/stm8s_tim2.c: 1035: uint16_t TIM2_GetCounter(void)
;	-----------------------------------------
;	 function TIM2_GetCounter
;	-----------------------------------------
_TIM2_GetCounter:
	Sstm8s_tim2$TIM2_GetCounter$928 ==.
	sub	sp, #4
	Sstm8s_tim2$TIM2_GetCounter$929 ==.
	Sstm8s_tim2$TIM2_GetCounter$930 ==.
;	drivers/src/stm8s_tim2.c: 1039: tmpcntr =  ((uint16_t)TIM2->CNTRH << 8);
	ld	a, 0x530a
	clrw	x
	ld	xh, a
	clr	a
	ld	(0x02, sp), a
	Sstm8s_tim2$TIM2_GetCounter$931 ==.
;	drivers/src/stm8s_tim2.c: 1041: return (uint16_t)( tmpcntr| (uint16_t)(TIM2->CNTRL));
	ld	a, 0x530b
	clr	(0x03, sp)
	or	a, (0x02, sp)
	rlwa	x
	or	a, (0x03, sp)
	ld	xh, a
	Sstm8s_tim2$TIM2_GetCounter$932 ==.
;	drivers/src/stm8s_tim2.c: 1042: }
	addw	sp, #4
	Sstm8s_tim2$TIM2_GetCounter$933 ==.
	Sstm8s_tim2$TIM2_GetCounter$934 ==.
	XG$TIM2_GetCounter$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_GetCounter$935 ==.
	Sstm8s_tim2$TIM2_GetPrescaler$936 ==.
;	drivers/src/stm8s_tim2.c: 1049: TIM2_Prescaler_TypeDef TIM2_GetPrescaler(void)
;	-----------------------------------------
;	 function TIM2_GetPrescaler
;	-----------------------------------------
_TIM2_GetPrescaler:
	Sstm8s_tim2$TIM2_GetPrescaler$937 ==.
	Sstm8s_tim2$TIM2_GetPrescaler$938 ==.
;	drivers/src/stm8s_tim2.c: 1052: return (TIM2_Prescaler_TypeDef)(TIM2->PSCR);
	ld	a, 0x530c
	Sstm8s_tim2$TIM2_GetPrescaler$939 ==.
;	drivers/src/stm8s_tim2.c: 1053: }
	Sstm8s_tim2$TIM2_GetPrescaler$940 ==.
	XG$TIM2_GetPrescaler$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_GetPrescaler$941 ==.
	Sstm8s_tim2$TIM2_GetFlagStatus$942 ==.
;	drivers/src/stm8s_tim2.c: 1068: FlagStatus TIM2_GetFlagStatus(TIM2_FLAG_TypeDef TIM2_FLAG)
;	-----------------------------------------
;	 function TIM2_GetFlagStatus
;	-----------------------------------------
_TIM2_GetFlagStatus:
	Sstm8s_tim2$TIM2_GetFlagStatus$943 ==.
	push	a
	Sstm8s_tim2$TIM2_GetFlagStatus$944 ==.
	Sstm8s_tim2$TIM2_GetFlagStatus$945 ==.
;	drivers/src/stm8s_tim2.c: 1074: assert_param(IS_TIM2_GET_FLAG_OK(TIM2_FLAG));
	ldw	x, (0x04, sp)
	cpw	x, #0x0001
	jrne	00167$
	jp	00107$
00167$:
	Sstm8s_tim2$TIM2_GetFlagStatus$946 ==.
	cpw	x, #0x0002
	jreq	00107$
	Sstm8s_tim2$TIM2_GetFlagStatus$947 ==.
	cpw	x, #0x0004
	jreq	00107$
	Sstm8s_tim2$TIM2_GetFlagStatus$948 ==.
	cpw	x, #0x0008
	jreq	00107$
	Sstm8s_tim2$TIM2_GetFlagStatus$949 ==.
	cpw	x, #0x0200
	jreq	00107$
	Sstm8s_tim2$TIM2_GetFlagStatus$950 ==.
	cpw	x, #0x0400
	jreq	00107$
	Sstm8s_tim2$TIM2_GetFlagStatus$951 ==.
	cpw	x, #0x0800
	jreq	00107$
	Sstm8s_tim2$TIM2_GetFlagStatus$952 ==.
	pushw	x
	Sstm8s_tim2$TIM2_GetFlagStatus$953 ==.
	push	#0x32
	Sstm8s_tim2$TIM2_GetFlagStatus$954 ==.
	push	#0x04
	Sstm8s_tim2$TIM2_GetFlagStatus$955 ==.
	push	#0x00
	Sstm8s_tim2$TIM2_GetFlagStatus$956 ==.
	push	#0x00
	Sstm8s_tim2$TIM2_GetFlagStatus$957 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_GetFlagStatus$958 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_GetFlagStatus$959 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_GetFlagStatus$960 ==.
	popw	x
	Sstm8s_tim2$TIM2_GetFlagStatus$961 ==.
00107$:
	Sstm8s_tim2$TIM2_GetFlagStatus$962 ==.
;	drivers/src/stm8s_tim2.c: 1076: tim2_flag_l = (uint8_t)(TIM2->SR1 & (uint8_t)TIM2_FLAG);
	ld	a, 0x5302
	ld	(0x01, sp), a
	ld	a, (0x05, sp)
	and	a, (0x01, sp)
	ld	(0x01, sp), a
	Sstm8s_tim2$TIM2_GetFlagStatus$963 ==.
;	drivers/src/stm8s_tim2.c: 1077: tim2_flag_h = (uint8_t)((uint16_t)TIM2_FLAG >> 8);
	Sstm8s_tim2$TIM2_GetFlagStatus$964 ==.
;	drivers/src/stm8s_tim2.c: 1079: if ((tim2_flag_l | (uint8_t)(TIM2->SR2 & tim2_flag_h)) != (uint8_t)RESET )
	ld	a, 0x5303
	pushw	x
	Sstm8s_tim2$TIM2_GetFlagStatus$965 ==.
	and	a, (1, sp)
	popw	x
	Sstm8s_tim2$TIM2_GetFlagStatus$966 ==.
	or	a, (0x01, sp)
	jreq	00102$
	Sstm8s_tim2$TIM2_GetFlagStatus$967 ==.
	Sstm8s_tim2$TIM2_GetFlagStatus$968 ==.
;	drivers/src/stm8s_tim2.c: 1081: bitstatus = SET;
	ld	a, #0x01
	Sstm8s_tim2$TIM2_GetFlagStatus$969 ==.
	jra	00103$
00102$:
	Sstm8s_tim2$TIM2_GetFlagStatus$970 ==.
	Sstm8s_tim2$TIM2_GetFlagStatus$971 ==.
;	drivers/src/stm8s_tim2.c: 1085: bitstatus = RESET;
	clr	a
	Sstm8s_tim2$TIM2_GetFlagStatus$972 ==.
00103$:
	Sstm8s_tim2$TIM2_GetFlagStatus$973 ==.
;	drivers/src/stm8s_tim2.c: 1087: return (FlagStatus)bitstatus;
	Sstm8s_tim2$TIM2_GetFlagStatus$974 ==.
;	drivers/src/stm8s_tim2.c: 1088: }
	addw	sp, #1
	Sstm8s_tim2$TIM2_GetFlagStatus$975 ==.
	Sstm8s_tim2$TIM2_GetFlagStatus$976 ==.
	XG$TIM2_GetFlagStatus$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_GetFlagStatus$977 ==.
	Sstm8s_tim2$TIM2_ClearFlag$978 ==.
;	drivers/src/stm8s_tim2.c: 1103: void TIM2_ClearFlag(TIM2_FLAG_TypeDef TIM2_FLAG)
;	-----------------------------------------
;	 function TIM2_ClearFlag
;	-----------------------------------------
_TIM2_ClearFlag:
	Sstm8s_tim2$TIM2_ClearFlag$979 ==.
	pushw	x
	Sstm8s_tim2$TIM2_ClearFlag$980 ==.
	Sstm8s_tim2$TIM2_ClearFlag$981 ==.
;	drivers/src/stm8s_tim2.c: 1106: assert_param(IS_TIM2_CLEAR_FLAG_OK(TIM2_FLAG));
	ldw	y, (0x05, sp)
	ldw	(0x01, sp), y
	ld	a, (0x02, sp)
	and	a, #0xf0
	ld	xl, a
	ld	a, (0x01, sp)
	and	a, #0xf1
	ld	xh, a
	tnzw	x
	jrne	00103$
	ldw	x, (0x01, sp)
	jrne	00104$
00103$:
	push	#0x52
	Sstm8s_tim2$TIM2_ClearFlag$982 ==.
	push	#0x04
	Sstm8s_tim2$TIM2_ClearFlag$983 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_ClearFlag$984 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_ClearFlag$985 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_ClearFlag$986 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_ClearFlag$987 ==.
00104$:
	Sstm8s_tim2$TIM2_ClearFlag$988 ==.
;	drivers/src/stm8s_tim2.c: 1109: TIM2->SR1 = (uint8_t)(~((uint8_t)(TIM2_FLAG)));
	ld	a, (0x02, sp)
	cpl	a
	ld	0x5302, a
	Sstm8s_tim2$TIM2_ClearFlag$989 ==.
;	drivers/src/stm8s_tim2.c: 1110: TIM2->SR2 = (uint8_t)(~((uint8_t)((uint8_t)TIM2_FLAG >> 8)));
	mov	0x5303+0, #0xff
	Sstm8s_tim2$TIM2_ClearFlag$990 ==.
;	drivers/src/stm8s_tim2.c: 1111: }
	popw	x
	Sstm8s_tim2$TIM2_ClearFlag$991 ==.
	Sstm8s_tim2$TIM2_ClearFlag$992 ==.
	XG$TIM2_ClearFlag$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_ClearFlag$993 ==.
	Sstm8s_tim2$TIM2_GetITStatus$994 ==.
;	drivers/src/stm8s_tim2.c: 1123: ITStatus TIM2_GetITStatus(TIM2_IT_TypeDef TIM2_IT)
;	-----------------------------------------
;	 function TIM2_GetITStatus
;	-----------------------------------------
_TIM2_GetITStatus:
	Sstm8s_tim2$TIM2_GetITStatus$995 ==.
	push	a
	Sstm8s_tim2$TIM2_GetITStatus$996 ==.
	Sstm8s_tim2$TIM2_GetITStatus$997 ==.
;	drivers/src/stm8s_tim2.c: 1129: assert_param(IS_TIM2_GET_IT_OK(TIM2_IT));
	ld	a, (0x04, sp)
	dec	a
	jreq	00108$
	Sstm8s_tim2$TIM2_GetITStatus$998 ==.
	ld	a, (0x04, sp)
	cp	a, #0x02
	jreq	00108$
	Sstm8s_tim2$TIM2_GetITStatus$999 ==.
	ld	a, (0x04, sp)
	cp	a, #0x04
	jreq	00108$
	Sstm8s_tim2$TIM2_GetITStatus$1000 ==.
	ld	a, (0x04, sp)
	cp	a, #0x08
	jreq	00108$
	Sstm8s_tim2$TIM2_GetITStatus$1001 ==.
	push	#0x69
	Sstm8s_tim2$TIM2_GetITStatus$1002 ==.
	push	#0x04
	Sstm8s_tim2$TIM2_GetITStatus$1003 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_GetITStatus$1004 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_GetITStatus$1005 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_GetITStatus$1006 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_GetITStatus$1007 ==.
00108$:
	Sstm8s_tim2$TIM2_GetITStatus$1008 ==.
;	drivers/src/stm8s_tim2.c: 1131: TIM2_itStatus = (uint8_t)(TIM2->SR1 & TIM2_IT);
	ld	a, 0x5302
	and	a, (0x04, sp)
	ld	(0x01, sp), a
	Sstm8s_tim2$TIM2_GetITStatus$1009 ==.
;	drivers/src/stm8s_tim2.c: 1133: TIM2_itEnable = (uint8_t)(TIM2->IER & TIM2_IT);
	ld	a, 0x5301
	and	a, (0x04, sp)
	Sstm8s_tim2$TIM2_GetITStatus$1010 ==.
;	drivers/src/stm8s_tim2.c: 1135: if ((TIM2_itStatus != (uint8_t)RESET ) && (TIM2_itEnable != (uint8_t)RESET ))
	tnz	(0x01, sp)
	jreq	00102$
	tnz	a
	jreq	00102$
	Sstm8s_tim2$TIM2_GetITStatus$1011 ==.
	Sstm8s_tim2$TIM2_GetITStatus$1012 ==.
;	drivers/src/stm8s_tim2.c: 1137: bitstatus = SET;
	ld	a, #0x01
	Sstm8s_tim2$TIM2_GetITStatus$1013 ==.
	jra	00103$
00102$:
	Sstm8s_tim2$TIM2_GetITStatus$1014 ==.
	Sstm8s_tim2$TIM2_GetITStatus$1015 ==.
;	drivers/src/stm8s_tim2.c: 1141: bitstatus = RESET;
	clr	a
	Sstm8s_tim2$TIM2_GetITStatus$1016 ==.
00103$:
	Sstm8s_tim2$TIM2_GetITStatus$1017 ==.
;	drivers/src/stm8s_tim2.c: 1143: return (ITStatus)(bitstatus);
	Sstm8s_tim2$TIM2_GetITStatus$1018 ==.
;	drivers/src/stm8s_tim2.c: 1144: }
	addw	sp, #1
	Sstm8s_tim2$TIM2_GetITStatus$1019 ==.
	Sstm8s_tim2$TIM2_GetITStatus$1020 ==.
	XG$TIM2_GetITStatus$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_GetITStatus$1021 ==.
	Sstm8s_tim2$TIM2_ClearITPendingBit$1022 ==.
;	drivers/src/stm8s_tim2.c: 1156: void TIM2_ClearITPendingBit(TIM2_IT_TypeDef TIM2_IT)
;	-----------------------------------------
;	 function TIM2_ClearITPendingBit
;	-----------------------------------------
_TIM2_ClearITPendingBit:
	Sstm8s_tim2$TIM2_ClearITPendingBit$1023 ==.
	Sstm8s_tim2$TIM2_ClearITPendingBit$1024 ==.
;	drivers/src/stm8s_tim2.c: 1159: assert_param(IS_TIM2_IT_OK(TIM2_IT));
	tnz	(0x03, sp)
	jreq	00103$
	ld	a, (0x03, sp)
	cp	a, #0x0f
	jrule	00104$
00103$:
	push	#0x87
	Sstm8s_tim2$TIM2_ClearITPendingBit$1025 ==.
	push	#0x04
	Sstm8s_tim2$TIM2_ClearITPendingBit$1026 ==.
	clrw	x
	pushw	x
	Sstm8s_tim2$TIM2_ClearITPendingBit$1027 ==.
	push	#<(___str_0+0)
	Sstm8s_tim2$TIM2_ClearITPendingBit$1028 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim2$TIM2_ClearITPendingBit$1029 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim2$TIM2_ClearITPendingBit$1030 ==.
00104$:
	Sstm8s_tim2$TIM2_ClearITPendingBit$1031 ==.
;	drivers/src/stm8s_tim2.c: 1162: TIM2->SR1 = (uint8_t)(~TIM2_IT);
	ld	a, (0x03, sp)
	cpl	a
	ld	0x5302, a
	Sstm8s_tim2$TIM2_ClearITPendingBit$1032 ==.
;	drivers/src/stm8s_tim2.c: 1163: }
	Sstm8s_tim2$TIM2_ClearITPendingBit$1033 ==.
	XG$TIM2_ClearITPendingBit$0$0 ==.
	ret
	Sstm8s_tim2$TIM2_ClearITPendingBit$1034 ==.
	Sstm8s_tim2$TI1_Config$1035 ==.
;	drivers/src/stm8s_tim2.c: 1181: static void TI1_Config(uint8_t TIM2_ICPolarity,
;	-----------------------------------------
;	 function TI1_Config
;	-----------------------------------------
_TI1_Config:
	Sstm8s_tim2$TI1_Config$1036 ==.
	push	a
	Sstm8s_tim2$TI1_Config$1037 ==.
	Sstm8s_tim2$TI1_Config$1038 ==.
;	drivers/src/stm8s_tim2.c: 1186: TIM2->CCER1 &= (uint8_t)(~TIM2_CCER1_CC1E);
	bres	21256, #0
	Sstm8s_tim2$TI1_Config$1039 ==.
;	drivers/src/stm8s_tim2.c: 1189: TIM2->CCMR1  = (uint8_t)((uint8_t)(TIM2->CCMR1 & (uint8_t)(~(uint8_t)( TIM2_CCMR_CCxS | TIM2_CCMR_ICxF )))
	ld	a, 0x5305
	and	a, #0x0c
	ld	(0x01, sp), a
	Sstm8s_tim2$TI1_Config$1040 ==.
;	drivers/src/stm8s_tim2.c: 1190: | (uint8_t)(((TIM2_ICSelection)) | ((uint8_t)( TIM2_ICFilter << 4))));
	ld	a, (0x06, sp)
	swap	a
	and	a, #0xf0
	or	a, (0x05, sp)
	or	a, (0x01, sp)
	ld	0x5305, a
	Sstm8s_tim2$TI1_Config$1041 ==.
;	drivers/src/stm8s_tim2.c: 1186: TIM2->CCER1 &= (uint8_t)(~TIM2_CCER1_CC1E);
	ld	a, 0x5308
	Sstm8s_tim2$TI1_Config$1042 ==.
;	drivers/src/stm8s_tim2.c: 1193: if (TIM2_ICPolarity != TIM2_ICPOLARITY_RISING)
	tnz	(0x04, sp)
	jreq	00102$
	Sstm8s_tim2$TI1_Config$1043 ==.
	Sstm8s_tim2$TI1_Config$1044 ==.
;	drivers/src/stm8s_tim2.c: 1195: TIM2->CCER1 |= TIM2_CCER1_CC1P;
	or	a, #0x02
	ld	0x5308, a
	Sstm8s_tim2$TI1_Config$1045 ==.
	jra	00103$
00102$:
	Sstm8s_tim2$TI1_Config$1046 ==.
	Sstm8s_tim2$TI1_Config$1047 ==.
;	drivers/src/stm8s_tim2.c: 1199: TIM2->CCER1 &= (uint8_t)(~TIM2_CCER1_CC1P);
	and	a, #0xfd
	ld	0x5308, a
	Sstm8s_tim2$TI1_Config$1048 ==.
00103$:
	Sstm8s_tim2$TI1_Config$1049 ==.
;	drivers/src/stm8s_tim2.c: 1202: TIM2->CCER1 |= TIM2_CCER1_CC1E;
	bset	21256, #0
	Sstm8s_tim2$TI1_Config$1050 ==.
;	drivers/src/stm8s_tim2.c: 1203: }
	pop	a
	Sstm8s_tim2$TI1_Config$1051 ==.
	Sstm8s_tim2$TI1_Config$1052 ==.
	XFstm8s_tim2$TI1_Config$0$0 ==.
	ret
	Sstm8s_tim2$TI1_Config$1053 ==.
	Sstm8s_tim2$TI2_Config$1054 ==.
;	drivers/src/stm8s_tim2.c: 1221: static void TI2_Config(uint8_t TIM2_ICPolarity,
;	-----------------------------------------
;	 function TI2_Config
;	-----------------------------------------
_TI2_Config:
	Sstm8s_tim2$TI2_Config$1055 ==.
	push	a
	Sstm8s_tim2$TI2_Config$1056 ==.
	Sstm8s_tim2$TI2_Config$1057 ==.
;	drivers/src/stm8s_tim2.c: 1226: TIM2->CCER1 &= (uint8_t)(~TIM2_CCER1_CC2E);
	bres	21256, #4
	Sstm8s_tim2$TI2_Config$1058 ==.
;	drivers/src/stm8s_tim2.c: 1229: TIM2->CCMR2 = (uint8_t)((uint8_t)(TIM2->CCMR2 & (uint8_t)(~(uint8_t)( TIM2_CCMR_CCxS | TIM2_CCMR_ICxF ))) 
	ld	a, 0x5306
	and	a, #0x0c
	ld	(0x01, sp), a
	Sstm8s_tim2$TI2_Config$1059 ==.
;	drivers/src/stm8s_tim2.c: 1230: | (uint8_t)(( (TIM2_ICSelection)) | ((uint8_t)( TIM2_ICFilter << 4))));
	ld	a, (0x06, sp)
	swap	a
	and	a, #0xf0
	or	a, (0x05, sp)
	or	a, (0x01, sp)
	ld	0x5306, a
	Sstm8s_tim2$TI2_Config$1060 ==.
;	drivers/src/stm8s_tim2.c: 1226: TIM2->CCER1 &= (uint8_t)(~TIM2_CCER1_CC2E);
	ld	a, 0x5308
	Sstm8s_tim2$TI2_Config$1061 ==.
;	drivers/src/stm8s_tim2.c: 1234: if (TIM2_ICPolarity != TIM2_ICPOLARITY_RISING)
	tnz	(0x04, sp)
	jreq	00102$
	Sstm8s_tim2$TI2_Config$1062 ==.
	Sstm8s_tim2$TI2_Config$1063 ==.
;	drivers/src/stm8s_tim2.c: 1236: TIM2->CCER1 |= TIM2_CCER1_CC2P;
	or	a, #0x20
	ld	0x5308, a
	Sstm8s_tim2$TI2_Config$1064 ==.
	jra	00103$
00102$:
	Sstm8s_tim2$TI2_Config$1065 ==.
	Sstm8s_tim2$TI2_Config$1066 ==.
;	drivers/src/stm8s_tim2.c: 1240: TIM2->CCER1 &= (uint8_t)(~TIM2_CCER1_CC2P);
	and	a, #0xdf
	ld	0x5308, a
	Sstm8s_tim2$TI2_Config$1067 ==.
00103$:
	Sstm8s_tim2$TI2_Config$1068 ==.
;	drivers/src/stm8s_tim2.c: 1244: TIM2->CCER1 |= TIM2_CCER1_CC2E;
	bset	21256, #4
	Sstm8s_tim2$TI2_Config$1069 ==.
;	drivers/src/stm8s_tim2.c: 1245: }
	pop	a
	Sstm8s_tim2$TI2_Config$1070 ==.
	Sstm8s_tim2$TI2_Config$1071 ==.
	XFstm8s_tim2$TI2_Config$0$0 ==.
	ret
	Sstm8s_tim2$TI2_Config$1072 ==.
	Sstm8s_tim2$TI3_Config$1073 ==.
;	drivers/src/stm8s_tim2.c: 1261: static void TI3_Config(uint8_t TIM2_ICPolarity, uint8_t TIM2_ICSelection,
;	-----------------------------------------
;	 function TI3_Config
;	-----------------------------------------
_TI3_Config:
	Sstm8s_tim2$TI3_Config$1074 ==.
	push	a
	Sstm8s_tim2$TI3_Config$1075 ==.
	Sstm8s_tim2$TI3_Config$1076 ==.
;	drivers/src/stm8s_tim2.c: 1265: TIM2->CCER2 &=  (uint8_t)(~TIM2_CCER2_CC3E);
	bres	21257, #0
	Sstm8s_tim2$TI3_Config$1077 ==.
;	drivers/src/stm8s_tim2.c: 1268: TIM2->CCMR3 = (uint8_t)((uint8_t)(TIM2->CCMR3 & (uint8_t)(~( TIM2_CCMR_CCxS | TIM2_CCMR_ICxF))) 
	ld	a, 0x5307
	and	a, #0x0c
	ld	(0x01, sp), a
	Sstm8s_tim2$TI3_Config$1078 ==.
;	drivers/src/stm8s_tim2.c: 1269: | (uint8_t)(( (TIM2_ICSelection)) | ((uint8_t)( TIM2_ICFilter << 4))));
	ld	a, (0x06, sp)
	swap	a
	and	a, #0xf0
	or	a, (0x05, sp)
	or	a, (0x01, sp)
	ld	0x5307, a
	Sstm8s_tim2$TI3_Config$1079 ==.
;	drivers/src/stm8s_tim2.c: 1265: TIM2->CCER2 &=  (uint8_t)(~TIM2_CCER2_CC3E);
	ld	a, 0x5309
	Sstm8s_tim2$TI3_Config$1080 ==.
;	drivers/src/stm8s_tim2.c: 1273: if (TIM2_ICPolarity != TIM2_ICPOLARITY_RISING)
	tnz	(0x04, sp)
	jreq	00102$
	Sstm8s_tim2$TI3_Config$1081 ==.
	Sstm8s_tim2$TI3_Config$1082 ==.
;	drivers/src/stm8s_tim2.c: 1275: TIM2->CCER2 |= TIM2_CCER2_CC3P;
	or	a, #0x02
	ld	0x5309, a
	Sstm8s_tim2$TI3_Config$1083 ==.
	jra	00103$
00102$:
	Sstm8s_tim2$TI3_Config$1084 ==.
	Sstm8s_tim2$TI3_Config$1085 ==.
;	drivers/src/stm8s_tim2.c: 1279: TIM2->CCER2 &= (uint8_t)(~TIM2_CCER2_CC3P);
	and	a, #0xfd
	ld	0x5309, a
	Sstm8s_tim2$TI3_Config$1086 ==.
00103$:
	Sstm8s_tim2$TI3_Config$1087 ==.
;	drivers/src/stm8s_tim2.c: 1282: TIM2->CCER2 |= TIM2_CCER2_CC3E;
	bset	21257, #0
	Sstm8s_tim2$TI3_Config$1088 ==.
;	drivers/src/stm8s_tim2.c: 1283: }
	pop	a
	Sstm8s_tim2$TI3_Config$1089 ==.
	Sstm8s_tim2$TI3_Config$1090 ==.
	XFstm8s_tim2$TI3_Config$0$0 ==.
	ret
	Sstm8s_tim2$TI3_Config$1091 ==.
	.area CODE
	.area CONST
Fstm8s_tim2$__str_0$0_0$0 == .
	.area CONST
___str_0:
	.ascii "drivers/src/stm8s_tim2.c"
	.db 0x00
	.area CODE
	.area INITIALIZER
	.area CABS (ABS)

	.area .debug_line (NOLOAD)
	.dw	0,Ldebug_line_end-Ldebug_line_start
Ldebug_line_start:
	.dw	2
	.dw	0,Ldebug_line_stmt-6-Ldebug_line_start
	.db	1
	.db	1
	.db	-5
	.db	15
	.db	10
	.db	0
	.db	1
	.db	1
	.db	1
	.db	1
	.db	0
	.db	0
	.db	0
	.db	1
	.ascii "C:\Program Files\SDCC\bin\..\include\stm8"
	.db	0
	.ascii "C:\Program Files\SDCC\bin\..\include"
	.db	0
	.db	0
	.ascii "drivers/src/stm8s_tim2.c"
	.db	0
	.uleb128	0
	.uleb128	0
	.uleb128	0
	.db	0
Ldebug_line_stmt:
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_DeInit$0)
	.db	3
	.sleb128	51
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$2-Sstm8s_tim2$TIM2_DeInit$0
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$3-Sstm8s_tim2$TIM2_DeInit$2
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$4-Sstm8s_tim2$TIM2_DeInit$3
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$5-Sstm8s_tim2$TIM2_DeInit$4
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$6-Sstm8s_tim2$TIM2_DeInit$5
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$7-Sstm8s_tim2$TIM2_DeInit$6
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$8-Sstm8s_tim2$TIM2_DeInit$7
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$9-Sstm8s_tim2$TIM2_DeInit$8
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$10-Sstm8s_tim2$TIM2_DeInit$9
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$11-Sstm8s_tim2$TIM2_DeInit$10
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$12-Sstm8s_tim2$TIM2_DeInit$11
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$13-Sstm8s_tim2$TIM2_DeInit$12
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$14-Sstm8s_tim2$TIM2_DeInit$13
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$15-Sstm8s_tim2$TIM2_DeInit$14
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$16-Sstm8s_tim2$TIM2_DeInit$15
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$17-Sstm8s_tim2$TIM2_DeInit$16
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$18-Sstm8s_tim2$TIM2_DeInit$17
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$19-Sstm8s_tim2$TIM2_DeInit$18
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$20-Sstm8s_tim2$TIM2_DeInit$19
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$21-Sstm8s_tim2$TIM2_DeInit$20
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$22-Sstm8s_tim2$TIM2_DeInit$21
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$23-Sstm8s_tim2$TIM2_DeInit$22
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_DeInit$24-Sstm8s_tim2$TIM2_DeInit$23
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_DeInit$25-Sstm8s_tim2$TIM2_DeInit$24
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_TimeBaseInit$27)
	.db	3
	.sleb128	88
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_TimeBaseInit$29-Sstm8s_tim2$TIM2_TimeBaseInit$27
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_TimeBaseInit$30-Sstm8s_tim2$TIM2_TimeBaseInit$29
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_TimeBaseInit$31-Sstm8s_tim2$TIM2_TimeBaseInit$30
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_TimeBaseInit$32-Sstm8s_tim2$TIM2_TimeBaseInit$31
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_TimeBaseInit$33-Sstm8s_tim2$TIM2_TimeBaseInit$32
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$35)
	.db	3
	.sleb128	107
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1Init$38-Sstm8s_tim2$TIM2_OC1Init$35
	.db	3
	.sleb128	6
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1Init$50-Sstm8s_tim2$TIM2_OC1Init$38
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1Init$58-Sstm8s_tim2$TIM2_OC1Init$50
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1Init$66-Sstm8s_tim2$TIM2_OC1Init$58
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1Init$67-Sstm8s_tim2$TIM2_OC1Init$66
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1Init$68-Sstm8s_tim2$TIM2_OC1Init$67
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1Init$69-Sstm8s_tim2$TIM2_OC1Init$68
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1Init$70-Sstm8s_tim2$TIM2_OC1Init$69
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1Init$71-Sstm8s_tim2$TIM2_OC1Init$70
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1Init$72-Sstm8s_tim2$TIM2_OC1Init$71
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1Init$73-Sstm8s_tim2$TIM2_OC1Init$72
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_OC1Init$75-Sstm8s_tim2$TIM2_OC1Init$73
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$77)
	.db	3
	.sleb128	141
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2Init$80-Sstm8s_tim2$TIM2_OC2Init$77
	.db	3
	.sleb128	6
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2Init$92-Sstm8s_tim2$TIM2_OC2Init$80
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2Init$100-Sstm8s_tim2$TIM2_OC2Init$92
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2Init$108-Sstm8s_tim2$TIM2_OC2Init$100
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2Init$109-Sstm8s_tim2$TIM2_OC2Init$108
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2Init$110-Sstm8s_tim2$TIM2_OC2Init$109
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2Init$111-Sstm8s_tim2$TIM2_OC2Init$110
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2Init$112-Sstm8s_tim2$TIM2_OC2Init$111
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2Init$113-Sstm8s_tim2$TIM2_OC2Init$112
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2Init$114-Sstm8s_tim2$TIM2_OC2Init$113
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2Init$115-Sstm8s_tim2$TIM2_OC2Init$114
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_OC2Init$117-Sstm8s_tim2$TIM2_OC2Init$115
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$119)
	.db	3
	.sleb128	178
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3Init$122-Sstm8s_tim2$TIM2_OC3Init$119
	.db	3
	.sleb128	6
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3Init$134-Sstm8s_tim2$TIM2_OC3Init$122
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3Init$142-Sstm8s_tim2$TIM2_OC3Init$134
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3Init$150-Sstm8s_tim2$TIM2_OC3Init$142
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3Init$151-Sstm8s_tim2$TIM2_OC3Init$150
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3Init$152-Sstm8s_tim2$TIM2_OC3Init$151
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3Init$153-Sstm8s_tim2$TIM2_OC3Init$152
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3Init$154-Sstm8s_tim2$TIM2_OC3Init$153
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3Init$155-Sstm8s_tim2$TIM2_OC3Init$154
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3Init$156-Sstm8s_tim2$TIM2_OC3Init$155
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3Init$157-Sstm8s_tim2$TIM2_OC3Init$156
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_OC3Init$159-Sstm8s_tim2$TIM2_OC3Init$157
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$161)
	.db	3
	.sleb128	211
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ICInit$164-Sstm8s_tim2$TIM2_ICInit$161
	.db	3
	.sleb128	7
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ICInit$173-Sstm8s_tim2$TIM2_ICInit$164
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ICInit$181-Sstm8s_tim2$TIM2_ICInit$173
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ICInit$191-Sstm8s_tim2$TIM2_ICInit$181
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ICInit$201-Sstm8s_tim2$TIM2_ICInit$191
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ICInit$208-Sstm8s_tim2$TIM2_ICInit$201
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ICInit$210-Sstm8s_tim2$TIM2_ICInit$208
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ICInit$215-Sstm8s_tim2$TIM2_ICInit$210
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ICInit$219-Sstm8s_tim2$TIM2_ICInit$215
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ICInit$221-Sstm8s_tim2$TIM2_ICInit$219
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ICInit$226-Sstm8s_tim2$TIM2_ICInit$221
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ICInit$231-Sstm8s_tim2$TIM2_ICInit$226
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ICInit$236-Sstm8s_tim2$TIM2_ICInit$231
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ICInit$240-Sstm8s_tim2$TIM2_ICInit$236
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_ICInit$242-Sstm8s_tim2$TIM2_ICInit$240
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$244)
	.db	3
	.sleb128	265
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$247-Sstm8s_tim2$TIM2_PWMIConfig$244
	.db	3
	.sleb128	10
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$255-Sstm8s_tim2$TIM2_PWMIConfig$247
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$263-Sstm8s_tim2$TIM2_PWMIConfig$255
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$273-Sstm8s_tim2$TIM2_PWMIConfig$263
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$283-Sstm8s_tim2$TIM2_PWMIConfig$273
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$285-Sstm8s_tim2$TIM2_PWMIConfig$283
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$288-Sstm8s_tim2$TIM2_PWMIConfig$285
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$290-Sstm8s_tim2$TIM2_PWMIConfig$288
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$292-Sstm8s_tim2$TIM2_PWMIConfig$290
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$295-Sstm8s_tim2$TIM2_PWMIConfig$292
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$297-Sstm8s_tim2$TIM2_PWMIConfig$295
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$299-Sstm8s_tim2$TIM2_PWMIConfig$297
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$304-Sstm8s_tim2$TIM2_PWMIConfig$299
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$307-Sstm8s_tim2$TIM2_PWMIConfig$304
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$312-Sstm8s_tim2$TIM2_PWMIConfig$307
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$317-Sstm8s_tim2$TIM2_PWMIConfig$312
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$322-Sstm8s_tim2$TIM2_PWMIConfig$317
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$325-Sstm8s_tim2$TIM2_PWMIConfig$322
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$330-Sstm8s_tim2$TIM2_PWMIConfig$325
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PWMIConfig$334-Sstm8s_tim2$TIM2_PWMIConfig$330
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_PWMIConfig$336-Sstm8s_tim2$TIM2_PWMIConfig$334
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$338)
	.db	3
	.sleb128	338
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_Cmd$340-Sstm8s_tim2$TIM2_Cmd$338
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_Cmd$348-Sstm8s_tim2$TIM2_Cmd$340
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_Cmd$349-Sstm8s_tim2$TIM2_Cmd$348
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_Cmd$351-Sstm8s_tim2$TIM2_Cmd$349
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_Cmd$354-Sstm8s_tim2$TIM2_Cmd$351
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_Cmd$356-Sstm8s_tim2$TIM2_Cmd$354
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_Cmd$357-Sstm8s_tim2$TIM2_Cmd$356
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$359)
	.db	3
	.sleb128	367
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ITConfig$362-Sstm8s_tim2$TIM2_ITConfig$359
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ITConfig$369-Sstm8s_tim2$TIM2_ITConfig$362
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ITConfig$377-Sstm8s_tim2$TIM2_ITConfig$369
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ITConfig$378-Sstm8s_tim2$TIM2_ITConfig$377
	.db	3
	.sleb128	-3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ITConfig$380-Sstm8s_tim2$TIM2_ITConfig$378
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ITConfig$383-Sstm8s_tim2$TIM2_ITConfig$380
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ITConfig$387-Sstm8s_tim2$TIM2_ITConfig$383
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_ITConfig$389-Sstm8s_tim2$TIM2_ITConfig$387
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$391)
	.db	3
	.sleb128	391
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_UpdateDisableConfig$393-Sstm8s_tim2$TIM2_UpdateDisableConfig$391
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_UpdateDisableConfig$401-Sstm8s_tim2$TIM2_UpdateDisableConfig$393
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_UpdateDisableConfig$402-Sstm8s_tim2$TIM2_UpdateDisableConfig$401
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_UpdateDisableConfig$404-Sstm8s_tim2$TIM2_UpdateDisableConfig$402
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_UpdateDisableConfig$407-Sstm8s_tim2$TIM2_UpdateDisableConfig$404
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_UpdateDisableConfig$409-Sstm8s_tim2$TIM2_UpdateDisableConfig$407
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_UpdateDisableConfig$410-Sstm8s_tim2$TIM2_UpdateDisableConfig$409
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$412)
	.db	3
	.sleb128	415
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_UpdateRequestConfig$414-Sstm8s_tim2$TIM2_UpdateRequestConfig$412
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_UpdateRequestConfig$422-Sstm8s_tim2$TIM2_UpdateRequestConfig$414
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_UpdateRequestConfig$423-Sstm8s_tim2$TIM2_UpdateRequestConfig$422
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_UpdateRequestConfig$425-Sstm8s_tim2$TIM2_UpdateRequestConfig$423
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_UpdateRequestConfig$428-Sstm8s_tim2$TIM2_UpdateRequestConfig$425
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_UpdateRequestConfig$430-Sstm8s_tim2$TIM2_UpdateRequestConfig$428
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_UpdateRequestConfig$431-Sstm8s_tim2$TIM2_UpdateRequestConfig$430
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$433)
	.db	3
	.sleb128	439
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOnePulseMode$435-Sstm8s_tim2$TIM2_SelectOnePulseMode$433
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOnePulseMode$443-Sstm8s_tim2$TIM2_SelectOnePulseMode$435
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOnePulseMode$444-Sstm8s_tim2$TIM2_SelectOnePulseMode$443
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOnePulseMode$446-Sstm8s_tim2$TIM2_SelectOnePulseMode$444
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOnePulseMode$449-Sstm8s_tim2$TIM2_SelectOnePulseMode$446
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOnePulseMode$451-Sstm8s_tim2$TIM2_SelectOnePulseMode$449
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_SelectOnePulseMode$452-Sstm8s_tim2$TIM2_SelectOnePulseMode$451
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$454)
	.db	3
	.sleb128	483
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PrescalerConfig$456-Sstm8s_tim2$TIM2_PrescalerConfig$454
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PrescalerConfig$464-Sstm8s_tim2$TIM2_PrescalerConfig$456
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PrescalerConfig$486-Sstm8s_tim2$TIM2_PrescalerConfig$464
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PrescalerConfig$487-Sstm8s_tim2$TIM2_PrescalerConfig$486
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_PrescalerConfig$488-Sstm8s_tim2$TIM2_PrescalerConfig$487
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_PrescalerConfig$489-Sstm8s_tim2$TIM2_PrescalerConfig$488
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$491)
	.db	3
	.sleb128	506
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ForcedOC1Config$493-Sstm8s_tim2$TIM2_ForcedOC1Config$491
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ForcedOC1Config$502-Sstm8s_tim2$TIM2_ForcedOC1Config$493
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ForcedOC1Config$503-Sstm8s_tim2$TIM2_ForcedOC1Config$502
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ForcedOC1Config$504-Sstm8s_tim2$TIM2_ForcedOC1Config$503
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_ForcedOC1Config$505-Sstm8s_tim2$TIM2_ForcedOC1Config$504
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$507)
	.db	3
	.sleb128	525
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ForcedOC2Config$509-Sstm8s_tim2$TIM2_ForcedOC2Config$507
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ForcedOC2Config$518-Sstm8s_tim2$TIM2_ForcedOC2Config$509
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ForcedOC2Config$519-Sstm8s_tim2$TIM2_ForcedOC2Config$518
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ForcedOC2Config$520-Sstm8s_tim2$TIM2_ForcedOC2Config$519
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_ForcedOC2Config$521-Sstm8s_tim2$TIM2_ForcedOC2Config$520
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$523)
	.db	3
	.sleb128	544
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ForcedOC3Config$525-Sstm8s_tim2$TIM2_ForcedOC3Config$523
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ForcedOC3Config$534-Sstm8s_tim2$TIM2_ForcedOC3Config$525
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ForcedOC3Config$535-Sstm8s_tim2$TIM2_ForcedOC3Config$534
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ForcedOC3Config$536-Sstm8s_tim2$TIM2_ForcedOC3Config$535
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_ForcedOC3Config$537-Sstm8s_tim2$TIM2_ForcedOC3Config$536
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$539)
	.db	3
	.sleb128	560
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ARRPreloadConfig$541-Sstm8s_tim2$TIM2_ARRPreloadConfig$539
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ARRPreloadConfig$549-Sstm8s_tim2$TIM2_ARRPreloadConfig$541
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ARRPreloadConfig$550-Sstm8s_tim2$TIM2_ARRPreloadConfig$549
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ARRPreloadConfig$552-Sstm8s_tim2$TIM2_ARRPreloadConfig$550
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ARRPreloadConfig$555-Sstm8s_tim2$TIM2_ARRPreloadConfig$552
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ARRPreloadConfig$557-Sstm8s_tim2$TIM2_ARRPreloadConfig$555
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_ARRPreloadConfig$558-Sstm8s_tim2$TIM2_ARRPreloadConfig$557
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$560)
	.db	3
	.sleb128	582
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1PreloadConfig$562-Sstm8s_tim2$TIM2_OC1PreloadConfig$560
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1PreloadConfig$570-Sstm8s_tim2$TIM2_OC1PreloadConfig$562
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1PreloadConfig$571-Sstm8s_tim2$TIM2_OC1PreloadConfig$570
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1PreloadConfig$573-Sstm8s_tim2$TIM2_OC1PreloadConfig$571
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1PreloadConfig$576-Sstm8s_tim2$TIM2_OC1PreloadConfig$573
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1PreloadConfig$578-Sstm8s_tim2$TIM2_OC1PreloadConfig$576
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_OC1PreloadConfig$579-Sstm8s_tim2$TIM2_OC1PreloadConfig$578
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$581)
	.db	3
	.sleb128	604
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2PreloadConfig$583-Sstm8s_tim2$TIM2_OC2PreloadConfig$581
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2PreloadConfig$591-Sstm8s_tim2$TIM2_OC2PreloadConfig$583
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2PreloadConfig$592-Sstm8s_tim2$TIM2_OC2PreloadConfig$591
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2PreloadConfig$594-Sstm8s_tim2$TIM2_OC2PreloadConfig$592
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2PreloadConfig$597-Sstm8s_tim2$TIM2_OC2PreloadConfig$594
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2PreloadConfig$599-Sstm8s_tim2$TIM2_OC2PreloadConfig$597
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_OC2PreloadConfig$600-Sstm8s_tim2$TIM2_OC2PreloadConfig$599
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$602)
	.db	3
	.sleb128	626
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3PreloadConfig$604-Sstm8s_tim2$TIM2_OC3PreloadConfig$602
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3PreloadConfig$612-Sstm8s_tim2$TIM2_OC3PreloadConfig$604
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3PreloadConfig$613-Sstm8s_tim2$TIM2_OC3PreloadConfig$612
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3PreloadConfig$615-Sstm8s_tim2$TIM2_OC3PreloadConfig$613
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3PreloadConfig$618-Sstm8s_tim2$TIM2_OC3PreloadConfig$615
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3PreloadConfig$620-Sstm8s_tim2$TIM2_OC3PreloadConfig$618
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_OC3PreloadConfig$621-Sstm8s_tim2$TIM2_OC3PreloadConfig$620
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$623)
	.db	3
	.sleb128	652
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GenerateEvent$625-Sstm8s_tim2$TIM2_GenerateEvent$623
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GenerateEvent$632-Sstm8s_tim2$TIM2_GenerateEvent$625
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GenerateEvent$633-Sstm8s_tim2$TIM2_GenerateEvent$632
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_GenerateEvent$634-Sstm8s_tim2$TIM2_GenerateEvent$633
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$636)
	.db	3
	.sleb128	669
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1PolarityConfig$638-Sstm8s_tim2$TIM2_OC1PolarityConfig$636
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1PolarityConfig$646-Sstm8s_tim2$TIM2_OC1PolarityConfig$638
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1PolarityConfig$647-Sstm8s_tim2$TIM2_OC1PolarityConfig$646
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1PolarityConfig$649-Sstm8s_tim2$TIM2_OC1PolarityConfig$647
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1PolarityConfig$652-Sstm8s_tim2$TIM2_OC1PolarityConfig$649
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC1PolarityConfig$654-Sstm8s_tim2$TIM2_OC1PolarityConfig$652
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_OC1PolarityConfig$655-Sstm8s_tim2$TIM2_OC1PolarityConfig$654
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$657)
	.db	3
	.sleb128	693
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2PolarityConfig$659-Sstm8s_tim2$TIM2_OC2PolarityConfig$657
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2PolarityConfig$667-Sstm8s_tim2$TIM2_OC2PolarityConfig$659
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2PolarityConfig$668-Sstm8s_tim2$TIM2_OC2PolarityConfig$667
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2PolarityConfig$670-Sstm8s_tim2$TIM2_OC2PolarityConfig$668
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2PolarityConfig$673-Sstm8s_tim2$TIM2_OC2PolarityConfig$670
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC2PolarityConfig$675-Sstm8s_tim2$TIM2_OC2PolarityConfig$673
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_OC2PolarityConfig$676-Sstm8s_tim2$TIM2_OC2PolarityConfig$675
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$678)
	.db	3
	.sleb128	717
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3PolarityConfig$680-Sstm8s_tim2$TIM2_OC3PolarityConfig$678
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3PolarityConfig$688-Sstm8s_tim2$TIM2_OC3PolarityConfig$680
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3PolarityConfig$689-Sstm8s_tim2$TIM2_OC3PolarityConfig$688
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3PolarityConfig$691-Sstm8s_tim2$TIM2_OC3PolarityConfig$689
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3PolarityConfig$694-Sstm8s_tim2$TIM2_OC3PolarityConfig$691
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_OC3PolarityConfig$696-Sstm8s_tim2$TIM2_OC3PolarityConfig$694
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_OC3PolarityConfig$697-Sstm8s_tim2$TIM2_OC3PolarityConfig$696
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$699)
	.db	3
	.sleb128	744
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_CCxCmd$702-Sstm8s_tim2$TIM2_CCxCmd$699
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_CCxCmd$711-Sstm8s_tim2$TIM2_CCxCmd$702
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_CCxCmd$719-Sstm8s_tim2$TIM2_CCxCmd$711
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_CCxCmd$720-Sstm8s_tim2$TIM2_CCxCmd$719
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_CCxCmd$722-Sstm8s_tim2$TIM2_CCxCmd$720
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_CCxCmd$724-Sstm8s_tim2$TIM2_CCxCmd$722
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_CCxCmd$727-Sstm8s_tim2$TIM2_CCxCmd$724
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_CCxCmd$729-Sstm8s_tim2$TIM2_CCxCmd$727
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_CCxCmd$730-Sstm8s_tim2$TIM2_CCxCmd$729
	.db	3
	.sleb128	-8
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_CCxCmd$732-Sstm8s_tim2$TIM2_CCxCmd$730
	.db	3
	.sleb128	11
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_CCxCmd$734-Sstm8s_tim2$TIM2_CCxCmd$732
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_CCxCmd$737-Sstm8s_tim2$TIM2_CCxCmd$734
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_CCxCmd$739-Sstm8s_tim2$TIM2_CCxCmd$737
	.db	3
	.sleb128	8
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_CCxCmd$741-Sstm8s_tim2$TIM2_CCxCmd$739
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_CCxCmd$743-Sstm8s_tim2$TIM2_CCxCmd$741
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_CCxCmd$746-Sstm8s_tim2$TIM2_CCxCmd$743
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_CCxCmd$748-Sstm8s_tim2$TIM2_CCxCmd$746
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_CCxCmd$750-Sstm8s_tim2$TIM2_CCxCmd$748
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$752)
	.db	3
	.sleb128	809
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOCxM$755-Sstm8s_tim2$TIM2_SelectOCxM$752
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOCxM$764-Sstm8s_tim2$TIM2_SelectOCxM$755
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOCxM$778-Sstm8s_tim2$TIM2_SelectOCxM$764
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOCxM$780-Sstm8s_tim2$TIM2_SelectOCxM$778
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOCxM$781-Sstm8s_tim2$TIM2_SelectOCxM$780
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOCxM$782-Sstm8s_tim2$TIM2_SelectOCxM$781
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOCxM$784-Sstm8s_tim2$TIM2_SelectOCxM$782
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOCxM$786-Sstm8s_tim2$TIM2_SelectOCxM$784
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOCxM$787-Sstm8s_tim2$TIM2_SelectOCxM$786
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOCxM$788-Sstm8s_tim2$TIM2_SelectOCxM$787
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOCxM$791-Sstm8s_tim2$TIM2_SelectOCxM$788
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOCxM$792-Sstm8s_tim2$TIM2_SelectOCxM$791
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOCxM$793-Sstm8s_tim2$TIM2_SelectOCxM$792
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SelectOCxM$795-Sstm8s_tim2$TIM2_SelectOCxM$793
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_SelectOCxM$797-Sstm8s_tim2$TIM2_SelectOCxM$795
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_SetCounter$799)
	.db	3
	.sleb128	850
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetCounter$801-Sstm8s_tim2$TIM2_SetCounter$799
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetCounter$802-Sstm8s_tim2$TIM2_SetCounter$801
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetCounter$803-Sstm8s_tim2$TIM2_SetCounter$802
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_SetCounter$804-Sstm8s_tim2$TIM2_SetCounter$803
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_SetAutoreload$806)
	.db	3
	.sleb128	863
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetAutoreload$808-Sstm8s_tim2$TIM2_SetAutoreload$806
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetAutoreload$809-Sstm8s_tim2$TIM2_SetAutoreload$808
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetAutoreload$810-Sstm8s_tim2$TIM2_SetAutoreload$809
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_SetAutoreload$811-Sstm8s_tim2$TIM2_SetAutoreload$810
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_SetCompare1$813)
	.db	3
	.sleb128	876
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetCompare1$815-Sstm8s_tim2$TIM2_SetCompare1$813
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetCompare1$816-Sstm8s_tim2$TIM2_SetCompare1$815
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetCompare1$817-Sstm8s_tim2$TIM2_SetCompare1$816
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_SetCompare1$818-Sstm8s_tim2$TIM2_SetCompare1$817
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_SetCompare2$820)
	.db	3
	.sleb128	889
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetCompare2$822-Sstm8s_tim2$TIM2_SetCompare2$820
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetCompare2$823-Sstm8s_tim2$TIM2_SetCompare2$822
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetCompare2$824-Sstm8s_tim2$TIM2_SetCompare2$823
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_SetCompare2$825-Sstm8s_tim2$TIM2_SetCompare2$824
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_SetCompare3$827)
	.db	3
	.sleb128	902
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetCompare3$829-Sstm8s_tim2$TIM2_SetCompare3$827
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetCompare3$830-Sstm8s_tim2$TIM2_SetCompare3$829
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetCompare3$831-Sstm8s_tim2$TIM2_SetCompare3$830
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_SetCompare3$832-Sstm8s_tim2$TIM2_SetCompare3$831
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$834)
	.db	3
	.sleb128	919
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetIC1Prescaler$836-Sstm8s_tim2$TIM2_SetIC1Prescaler$834
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetIC1Prescaler$846-Sstm8s_tim2$TIM2_SetIC1Prescaler$836
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetIC1Prescaler$847-Sstm8s_tim2$TIM2_SetIC1Prescaler$846
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetIC1Prescaler$848-Sstm8s_tim2$TIM2_SetIC1Prescaler$847
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_SetIC1Prescaler$849-Sstm8s_tim2$TIM2_SetIC1Prescaler$848
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$851)
	.db	3
	.sleb128	939
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetIC2Prescaler$853-Sstm8s_tim2$TIM2_SetIC2Prescaler$851
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetIC2Prescaler$863-Sstm8s_tim2$TIM2_SetIC2Prescaler$853
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetIC2Prescaler$864-Sstm8s_tim2$TIM2_SetIC2Prescaler$863
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetIC2Prescaler$865-Sstm8s_tim2$TIM2_SetIC2Prescaler$864
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_SetIC2Prescaler$866-Sstm8s_tim2$TIM2_SetIC2Prescaler$865
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$868)
	.db	3
	.sleb128	959
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetIC3Prescaler$870-Sstm8s_tim2$TIM2_SetIC3Prescaler$868
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetIC3Prescaler$880-Sstm8s_tim2$TIM2_SetIC3Prescaler$870
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetIC3Prescaler$881-Sstm8s_tim2$TIM2_SetIC3Prescaler$880
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_SetIC3Prescaler$882-Sstm8s_tim2$TIM2_SetIC3Prescaler$881
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_SetIC3Prescaler$883-Sstm8s_tim2$TIM2_SetIC3Prescaler$882
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture1$885)
	.db	3
	.sleb128	974
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture1$888-Sstm8s_tim2$TIM2_GetCapture1$885
	.db	3
	.sleb128	6
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture1$889-Sstm8s_tim2$TIM2_GetCapture1$888
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture1$890-Sstm8s_tim2$TIM2_GetCapture1$889
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture1$891-Sstm8s_tim2$TIM2_GetCapture1$890
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture1$894-Sstm8s_tim2$TIM2_GetCapture1$891
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture1$895-Sstm8s_tim2$TIM2_GetCapture1$894
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_GetCapture1$897-Sstm8s_tim2$TIM2_GetCapture1$895
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture2$899)
	.db	3
	.sleb128	994
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture2$902-Sstm8s_tim2$TIM2_GetCapture2$899
	.db	3
	.sleb128	6
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture2$903-Sstm8s_tim2$TIM2_GetCapture2$902
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture2$904-Sstm8s_tim2$TIM2_GetCapture2$903
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture2$905-Sstm8s_tim2$TIM2_GetCapture2$904
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture2$908-Sstm8s_tim2$TIM2_GetCapture2$905
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture2$909-Sstm8s_tim2$TIM2_GetCapture2$908
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_GetCapture2$911-Sstm8s_tim2$TIM2_GetCapture2$909
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture3$913)
	.db	3
	.sleb128	1014
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture3$916-Sstm8s_tim2$TIM2_GetCapture3$913
	.db	3
	.sleb128	6
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture3$917-Sstm8s_tim2$TIM2_GetCapture3$916
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture3$918-Sstm8s_tim2$TIM2_GetCapture3$917
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture3$919-Sstm8s_tim2$TIM2_GetCapture3$918
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture3$922-Sstm8s_tim2$TIM2_GetCapture3$919
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCapture3$923-Sstm8s_tim2$TIM2_GetCapture3$922
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_GetCapture3$925-Sstm8s_tim2$TIM2_GetCapture3$923
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_GetCounter$927)
	.db	3
	.sleb128	1034
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCounter$930-Sstm8s_tim2$TIM2_GetCounter$927
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCounter$931-Sstm8s_tim2$TIM2_GetCounter$930
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetCounter$932-Sstm8s_tim2$TIM2_GetCounter$931
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_GetCounter$934-Sstm8s_tim2$TIM2_GetCounter$932
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_GetPrescaler$936)
	.db	3
	.sleb128	1048
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetPrescaler$938-Sstm8s_tim2$TIM2_GetPrescaler$936
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetPrescaler$939-Sstm8s_tim2$TIM2_GetPrescaler$938
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_GetPrescaler$940-Sstm8s_tim2$TIM2_GetPrescaler$939
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$942)
	.db	3
	.sleb128	1067
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetFlagStatus$945-Sstm8s_tim2$TIM2_GetFlagStatus$942
	.db	3
	.sleb128	6
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetFlagStatus$962-Sstm8s_tim2$TIM2_GetFlagStatus$945
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetFlagStatus$963-Sstm8s_tim2$TIM2_GetFlagStatus$962
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetFlagStatus$964-Sstm8s_tim2$TIM2_GetFlagStatus$963
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetFlagStatus$968-Sstm8s_tim2$TIM2_GetFlagStatus$964
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetFlagStatus$971-Sstm8s_tim2$TIM2_GetFlagStatus$968
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetFlagStatus$973-Sstm8s_tim2$TIM2_GetFlagStatus$971
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetFlagStatus$974-Sstm8s_tim2$TIM2_GetFlagStatus$973
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_GetFlagStatus$976-Sstm8s_tim2$TIM2_GetFlagStatus$974
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$978)
	.db	3
	.sleb128	1102
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ClearFlag$981-Sstm8s_tim2$TIM2_ClearFlag$978
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ClearFlag$988-Sstm8s_tim2$TIM2_ClearFlag$981
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ClearFlag$989-Sstm8s_tim2$TIM2_ClearFlag$988
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ClearFlag$990-Sstm8s_tim2$TIM2_ClearFlag$989
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_ClearFlag$992-Sstm8s_tim2$TIM2_ClearFlag$990
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$994)
	.db	3
	.sleb128	1122
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetITStatus$997-Sstm8s_tim2$TIM2_GetITStatus$994
	.db	3
	.sleb128	6
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetITStatus$1008-Sstm8s_tim2$TIM2_GetITStatus$997
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetITStatus$1009-Sstm8s_tim2$TIM2_GetITStatus$1008
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetITStatus$1010-Sstm8s_tim2$TIM2_GetITStatus$1009
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetITStatus$1012-Sstm8s_tim2$TIM2_GetITStatus$1010
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetITStatus$1015-Sstm8s_tim2$TIM2_GetITStatus$1012
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetITStatus$1017-Sstm8s_tim2$TIM2_GetITStatus$1015
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_GetITStatus$1018-Sstm8s_tim2$TIM2_GetITStatus$1017
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_GetITStatus$1020-Sstm8s_tim2$TIM2_GetITStatus$1018
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1022)
	.db	3
	.sleb128	1155
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ClearITPendingBit$1024-Sstm8s_tim2$TIM2_ClearITPendingBit$1022
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ClearITPendingBit$1031-Sstm8s_tim2$TIM2_ClearITPendingBit$1024
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TIM2_ClearITPendingBit$1032-Sstm8s_tim2$TIM2_ClearITPendingBit$1031
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TIM2_ClearITPendingBit$1033-Sstm8s_tim2$TIM2_ClearITPendingBit$1032
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TI1_Config$1035)
	.db	3
	.sleb128	1180
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI1_Config$1038-Sstm8s_tim2$TI1_Config$1035
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI1_Config$1039-Sstm8s_tim2$TI1_Config$1038
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI1_Config$1040-Sstm8s_tim2$TI1_Config$1039
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI1_Config$1041-Sstm8s_tim2$TI1_Config$1040
	.db	3
	.sleb128	-4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI1_Config$1042-Sstm8s_tim2$TI1_Config$1041
	.db	3
	.sleb128	7
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI1_Config$1044-Sstm8s_tim2$TI1_Config$1042
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI1_Config$1047-Sstm8s_tim2$TI1_Config$1044
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI1_Config$1049-Sstm8s_tim2$TI1_Config$1047
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI1_Config$1050-Sstm8s_tim2$TI1_Config$1049
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TI1_Config$1052-Sstm8s_tim2$TI1_Config$1050
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TI2_Config$1054)
	.db	3
	.sleb128	1220
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI2_Config$1057-Sstm8s_tim2$TI2_Config$1054
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI2_Config$1058-Sstm8s_tim2$TI2_Config$1057
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI2_Config$1059-Sstm8s_tim2$TI2_Config$1058
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI2_Config$1060-Sstm8s_tim2$TI2_Config$1059
	.db	3
	.sleb128	-4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI2_Config$1061-Sstm8s_tim2$TI2_Config$1060
	.db	3
	.sleb128	8
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI2_Config$1063-Sstm8s_tim2$TI2_Config$1061
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI2_Config$1066-Sstm8s_tim2$TI2_Config$1063
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI2_Config$1068-Sstm8s_tim2$TI2_Config$1066
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI2_Config$1069-Sstm8s_tim2$TI2_Config$1068
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TI2_Config$1071-Sstm8s_tim2$TI2_Config$1069
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim2$TI3_Config$1073)
	.db	3
	.sleb128	1260
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI3_Config$1076-Sstm8s_tim2$TI3_Config$1073
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI3_Config$1077-Sstm8s_tim2$TI3_Config$1076
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI3_Config$1078-Sstm8s_tim2$TI3_Config$1077
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI3_Config$1079-Sstm8s_tim2$TI3_Config$1078
	.db	3
	.sleb128	-4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI3_Config$1080-Sstm8s_tim2$TI3_Config$1079
	.db	3
	.sleb128	8
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI3_Config$1082-Sstm8s_tim2$TI3_Config$1080
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI3_Config$1085-Sstm8s_tim2$TI3_Config$1082
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI3_Config$1087-Sstm8s_tim2$TI3_Config$1085
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim2$TI3_Config$1088-Sstm8s_tim2$TI3_Config$1087
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim2$TI3_Config$1090-Sstm8s_tim2$TI3_Config$1088
	.db	0
	.uleb128	1
	.db	1
Ldebug_line_end:

	.area .debug_loc (NOLOAD)
Ldebug_loc_start:
	.dw	0,(Sstm8s_tim2$TI3_Config$1089)
	.dw	0,(Sstm8s_tim2$TI3_Config$1091)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TI3_Config$1075)
	.dw	0,(Sstm8s_tim2$TI3_Config$1089)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TI3_Config$1074)
	.dw	0,(Sstm8s_tim2$TI3_Config$1075)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TI2_Config$1070)
	.dw	0,(Sstm8s_tim2$TI2_Config$1072)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TI2_Config$1056)
	.dw	0,(Sstm8s_tim2$TI2_Config$1070)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TI2_Config$1055)
	.dw	0,(Sstm8s_tim2$TI2_Config$1056)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TI1_Config$1051)
	.dw	0,(Sstm8s_tim2$TI1_Config$1053)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TI1_Config$1037)
	.dw	0,(Sstm8s_tim2$TI1_Config$1051)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TI1_Config$1036)
	.dw	0,(Sstm8s_tim2$TI1_Config$1037)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1030)
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1034)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1029)
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1030)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1028)
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1029)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1027)
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1028)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1026)
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1027)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1025)
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1026)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1023)
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1025)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1019)
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1021)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1007)
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1019)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1006)
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1007)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1005)
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1006)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1004)
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1005)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1003)
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1004)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1002)
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1003)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1001)
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1002)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1000)
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1001)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$999)
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1000)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$998)
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$999)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$996)
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$998)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$995)
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$996)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$991)
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$993)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$987)
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$991)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$986)
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$987)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$985)
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$986)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$984)
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$985)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$983)
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$984)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$982)
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$983)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$980)
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$982)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$979)
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$980)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$975)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$977)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$966)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$975)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$965)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$966)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$961)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$965)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$960)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$961)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$959)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$960)
	.dw	2
	.db	120
	.sleb128	10
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$958)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$959)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$957)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$958)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$956)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$957)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$955)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$956)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$954)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$955)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$953)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$954)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$952)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$953)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$951)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$952)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$950)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$951)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$949)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$950)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$948)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$949)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$947)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$948)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$946)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$947)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$944)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$946)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$943)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$944)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_GetPrescaler$937)
	.dw	0,(Sstm8s_tim2$TIM2_GetPrescaler$941)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_GetCounter$933)
	.dw	0,(Sstm8s_tim2$TIM2_GetCounter$935)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCounter$929)
	.dw	0,(Sstm8s_tim2$TIM2_GetCounter$933)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_GetCounter$928)
	.dw	0,(Sstm8s_tim2$TIM2_GetCounter$929)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture3$924)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture3$926)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture3$921)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture3$924)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture3$920)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture3$921)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture3$915)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture3$920)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture3$914)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture3$915)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture2$910)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture2$912)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture2$907)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture2$910)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture2$906)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture2$907)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture2$901)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture2$906)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture2$900)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture2$901)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture1$896)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture1$898)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture1$893)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture1$896)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture1$892)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture1$893)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture1$887)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture1$892)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture1$886)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture1$887)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$879)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$884)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$878)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$879)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$877)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$878)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$876)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$877)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$875)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$876)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$874)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$875)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$873)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$874)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$872)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$873)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$871)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$872)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$869)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$871)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$862)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$867)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$861)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$862)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$860)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$861)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$859)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$860)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$858)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$859)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$857)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$858)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$856)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$857)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$855)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$856)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$854)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$855)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$852)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$854)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$845)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$850)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$844)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$845)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$843)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$844)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$842)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$843)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$841)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$842)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$840)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$841)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$839)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$840)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$838)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$839)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$837)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$838)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$835)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$837)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_SetCompare3$828)
	.dw	0,(Sstm8s_tim2$TIM2_SetCompare3$833)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_SetCompare2$821)
	.dw	0,(Sstm8s_tim2$TIM2_SetCompare2$826)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_SetCompare1$814)
	.dw	0,(Sstm8s_tim2$TIM2_SetCompare1$819)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_SetAutoreload$807)
	.dw	0,(Sstm8s_tim2$TIM2_SetAutoreload$812)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_SetCounter$800)
	.dw	0,(Sstm8s_tim2$TIM2_SetCounter$805)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$796)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$798)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$777)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$796)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$776)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$777)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$775)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$776)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$774)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$775)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$773)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$774)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$772)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$773)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$771)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$772)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$770)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$771)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$769)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$770)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$768)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$769)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$767)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$768)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$766)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$767)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$765)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$766)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$763)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$765)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$762)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$763)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$761)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$762)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$760)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$761)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$759)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$760)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$758)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$759)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$757)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$758)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$756)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$757)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$754)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$756)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$753)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$754)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$749)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$751)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$718)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$749)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$717)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$718)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$716)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$717)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$715)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$716)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$714)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$715)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$713)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$714)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$712)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$713)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$710)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$712)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$709)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$710)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$708)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$709)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$707)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$708)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$706)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$707)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$705)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$706)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$704)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$705)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$703)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$704)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$701)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$703)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$700)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$701)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$687)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$698)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$686)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$687)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$685)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$686)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$684)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$685)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$683)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$684)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$682)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$683)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$681)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$682)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$679)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$681)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$666)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$677)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$665)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$666)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$664)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$665)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$663)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$664)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$662)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$663)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$661)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$662)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$660)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$661)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$658)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$660)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$645)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$656)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$644)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$645)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$643)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$644)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$642)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$643)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$641)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$642)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$640)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$641)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$639)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$640)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$637)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$639)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$631)
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$635)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$630)
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$631)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$629)
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$630)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$628)
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$629)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$627)
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$628)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$626)
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$627)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$624)
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$626)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$611)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$622)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$610)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$611)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$609)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$610)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$608)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$609)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$607)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$608)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$606)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$607)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$605)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$606)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$603)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$605)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$590)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$601)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$589)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$590)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$588)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$589)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$587)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$588)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$586)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$587)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$585)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$586)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$584)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$585)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$582)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$584)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$569)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$580)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$568)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$569)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$567)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$568)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$566)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$567)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$565)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$566)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$564)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$565)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$563)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$564)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$561)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$563)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$548)
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$559)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$547)
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$548)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$546)
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$547)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$545)
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$546)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$544)
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$545)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$543)
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$544)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$542)
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$543)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$540)
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$542)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$533)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$538)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$532)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$533)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$531)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$532)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$530)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$531)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$529)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$530)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$528)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$529)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$527)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$528)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$526)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$527)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$524)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$526)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$517)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$522)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$516)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$517)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$515)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$516)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$514)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$515)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$513)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$514)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$512)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$513)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$511)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$512)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$510)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$511)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$508)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$510)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$501)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$506)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$500)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$501)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$499)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$500)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$498)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$499)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$497)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$498)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$496)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$497)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$495)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$496)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$494)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$495)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$492)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$494)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$485)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$490)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$484)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$485)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$483)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$484)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$482)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$483)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$481)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$482)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$480)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$481)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$479)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$480)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$478)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$479)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$477)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$478)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$476)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$477)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$475)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$476)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$474)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$475)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$473)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$474)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$472)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$473)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$471)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$472)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$470)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$471)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$469)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$470)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$468)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$469)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$467)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$468)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$466)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$467)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$465)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$466)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$463)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$465)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$462)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$463)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$461)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$462)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$460)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$461)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$459)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$460)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$458)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$459)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$457)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$458)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$455)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$457)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$442)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$453)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$441)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$442)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$440)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$441)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$439)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$440)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$438)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$439)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$437)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$438)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$436)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$437)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$434)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$436)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$421)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$432)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$420)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$421)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$419)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$420)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$418)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$419)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$417)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$418)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$416)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$417)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$415)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$416)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$413)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$415)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$400)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$411)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$399)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$400)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$398)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$399)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$397)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$398)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$396)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$397)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$395)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$396)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$394)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$395)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$392)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$394)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$388)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$390)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$385)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$388)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$384)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$385)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$376)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$384)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$375)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$376)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$374)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$375)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$373)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$374)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$372)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$373)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$371)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$372)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$370)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$371)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$368)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$370)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$367)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$368)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$366)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$367)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$365)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$366)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$364)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$365)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$363)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$364)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$361)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$363)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$360)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$361)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$347)
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$358)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$346)
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$347)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$345)
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$346)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$344)
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$345)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$343)
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$344)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$342)
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$343)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$341)
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$342)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$339)
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$341)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$335)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$337)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$332)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$335)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$331)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$332)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$329)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$331)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$328)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$329)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$327)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$328)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$326)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$327)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$324)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$326)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$323)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$324)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$321)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$323)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$320)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$321)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$319)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$320)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$318)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$319)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$314)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$318)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$313)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$314)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$311)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$313)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$310)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$311)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$309)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$310)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$308)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$309)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$306)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$308)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$305)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$306)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$303)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$305)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$302)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$303)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$301)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$302)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$300)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$301)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$282)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$300)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$281)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$282)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$280)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$281)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$279)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$280)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$278)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$279)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$277)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$278)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$276)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$277)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$275)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$276)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$274)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$275)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$272)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$274)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$271)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$272)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$270)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$271)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$269)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$270)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$268)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$269)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$267)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$268)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$266)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$267)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$265)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$266)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$264)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$265)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$262)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$264)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$261)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$262)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$260)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$261)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$259)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$260)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$258)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$259)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$257)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$258)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$256)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$257)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$254)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$256)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$253)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$254)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$252)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$253)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$251)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$252)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$250)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$251)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$249)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$250)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$248)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$249)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$246)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$248)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$245)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$246)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$241)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$243)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$238)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$241)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$237)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$238)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$235)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$237)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$234)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$235)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$233)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$234)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$232)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$233)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$228)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$232)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$227)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$228)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$225)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$227)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$224)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$225)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$223)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$224)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$222)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$223)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$217)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$222)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$216)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$217)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$214)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$216)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$213)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$214)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$212)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$213)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$211)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$212)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$207)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$211)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$206)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$207)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$205)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$206)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$204)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$205)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$203)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$204)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$202)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$203)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$200)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$202)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$199)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$200)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$198)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$199)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$197)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$198)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$196)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$197)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$195)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$196)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$194)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$195)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$193)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$194)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$192)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$193)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$190)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$192)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$189)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$190)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$188)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$189)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$187)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$188)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$186)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$187)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$185)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$186)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$184)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$185)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$183)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$184)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$182)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$183)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$180)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$182)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$179)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$180)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$178)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$179)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$177)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$178)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$176)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$177)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$175)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$176)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$174)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$175)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$172)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$174)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$171)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$172)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$170)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$171)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$169)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$170)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$168)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$169)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$167)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$168)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$166)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$167)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$165)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$166)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$163)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$165)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$162)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$163)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$158)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$160)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$149)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$158)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$148)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$149)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$147)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$148)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$146)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$147)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$145)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$146)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$144)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$145)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$143)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$144)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$141)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$143)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$140)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$141)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$139)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$140)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$138)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$139)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$137)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$138)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$136)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$137)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$135)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$136)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$133)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$135)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$132)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$133)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$131)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$132)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$130)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$131)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$129)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$130)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$128)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$129)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$127)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$128)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$126)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$127)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$125)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$126)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$124)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$125)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$123)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$124)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$121)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$123)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$120)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$121)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$116)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$118)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$107)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$116)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$106)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$107)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$105)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$106)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$104)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$105)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$103)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$104)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$102)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$103)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$101)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$102)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$99)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$101)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$98)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$99)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$97)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$98)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$96)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$97)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$95)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$96)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$94)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$95)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$93)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$94)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$91)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$93)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$90)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$91)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$89)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$90)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$88)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$89)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$87)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$88)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$86)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$87)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$85)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$86)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$84)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$85)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$83)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$84)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$82)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$83)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$81)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$82)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$79)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$81)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$78)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$79)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$74)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$76)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$65)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$74)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$64)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$65)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$63)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$64)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$62)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$63)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$61)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$62)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$60)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$61)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$59)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$60)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$57)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$59)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$56)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$57)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$55)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$56)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$54)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$55)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$53)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$54)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$52)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$53)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$51)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$52)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$49)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$51)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$48)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$49)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$47)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$48)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$46)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$47)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$45)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$46)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$44)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$45)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$43)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$44)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$42)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$43)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$41)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$42)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$40)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$41)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$39)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$40)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$37)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$39)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$36)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$37)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_TimeBaseInit$28)
	.dw	0,(Sstm8s_tim2$TIM2_TimeBaseInit$34)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim2$TIM2_DeInit$1)
	.dw	0,(Sstm8s_tim2$TIM2_DeInit$26)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0

	.area .debug_abbrev (NOLOAD)
Ldebug_abbrev:
	.uleb128	11
	.uleb128	46
	.db	0
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	4
	.uleb128	5
	.db	0
	.uleb128	2
	.uleb128	10
	.uleb128	3
	.uleb128	8
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	13
	.uleb128	1
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	11
	.uleb128	11
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	3
	.uleb128	46
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	0
	.uleb128	0
	.uleb128	7
	.uleb128	52
	.db	0
	.uleb128	2
	.uleb128	10
	.uleb128	3
	.uleb128	8
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	10
	.uleb128	46
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	12
	.uleb128	38
	.db	0
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	9
	.uleb128	11
	.db	1
	.uleb128	17
	.uleb128	1
	.uleb128	0
	.uleb128	0
	.uleb128	1
	.uleb128	17
	.db	1
	.uleb128	3
	.uleb128	8
	.uleb128	16
	.uleb128	6
	.uleb128	19
	.uleb128	11
	.uleb128	37
	.uleb128	8
	.uleb128	0
	.uleb128	0
	.uleb128	6
	.uleb128	11
	.db	0
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	0
	.uleb128	0
	.uleb128	8
	.uleb128	11
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	17
	.uleb128	1
	.uleb128	0
	.uleb128	0
	.uleb128	2
	.uleb128	46
	.db	0
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	0
	.uleb128	0
	.uleb128	14
	.uleb128	33
	.db	0
	.uleb128	47
	.uleb128	11
	.uleb128	0
	.uleb128	0
	.uleb128	5
	.uleb128	36
	.db	0
	.uleb128	3
	.uleb128	8
	.uleb128	11
	.uleb128	11
	.uleb128	62
	.uleb128	11
	.uleb128	0
	.uleb128	0
	.uleb128	0

	.area .debug_info (NOLOAD)
	.dw	0,Ldebug_info_end-Ldebug_info_start
Ldebug_info_start:
	.dw	2
	.dw	0,(Ldebug_abbrev)
	.db	4
	.uleb128	1
	.ascii "drivers/src/stm8s_tim2.c"
	.db	0
	.dw	0,(Ldebug_line_start+-4)
	.db	1
	.ascii "SDCC version 4.1.0 #12072"
	.db	0
	.uleb128	2
	.ascii "TIM2_DeInit"
	.db	0
	.dw	0,(_TIM2_DeInit)
	.dw	0,(XG$TIM2_DeInit$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+6696)
	.uleb128	3
	.dw	0,174
	.ascii "TIM2_TimeBaseInit"
	.db	0
	.dw	0,(_TIM2_TimeBaseInit)
	.dw	0,(XG$TIM2_TimeBaseInit$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+6676)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_Prescaler"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM2_Period"
	.db	0
	.dw	0,191
	.uleb128	0
	.uleb128	5
	.ascii "unsigned char"
	.db	0
	.db	1
	.db	8
	.uleb128	5
	.ascii "unsigned int"
	.db	0
	.db	2
	.db	7
	.uleb128	3
	.dw	0,327
	.ascii "TIM2_OC1Init"
	.db	0
	.dw	0,(_TIM2_OC1Init)
	.dw	0,(XG$TIM2_OC1Init$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+6332)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_OCMode"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM2_OutputState"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	4
	.ascii "TIM2_Pulse"
	.db	0
	.dw	0,191
	.uleb128	4
	.db	2
	.db	145
	.sleb128	6
	.ascii "TIM2_OCPolarity"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,447
	.ascii "TIM2_OC2Init"
	.db	0
	.dw	0,(_TIM2_OC2Init)
	.dw	0,(XG$TIM2_OC2Init$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+5988)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_OCMode"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM2_OutputState"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	4
	.ascii "TIM2_Pulse"
	.db	0
	.dw	0,191
	.uleb128	4
	.db	2
	.db	145
	.sleb128	6
	.ascii "TIM2_OCPolarity"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,567
	.ascii "TIM2_OC3Init"
	.db	0
	.dw	0,(_TIM2_OC3Init)
	.dw	0,(XG$TIM2_OC3Init$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+5644)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_OCMode"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM2_OutputState"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	4
	.ascii "TIM2_Pulse"
	.db	0
	.dw	0,191
	.uleb128	4
	.db	2
	.db	145
	.sleb128	6
	.ascii "TIM2_OCPolarity"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,742
	.ascii "TIM2_ICInit"
	.db	0
	.dw	0,(_TIM2_ICInit)
	.dw	0,(XG$TIM2_ICInit$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+4916)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_Channel"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM2_ICPolarity"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	4
	.ascii "TIM2_ICSelection"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	5
	.ascii "TIM2_ICPrescaler"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	6
	.ascii "TIM2_ICFilter"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$209)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$218)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$220)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$229)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$230)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$239)
	.uleb128	0
	.uleb128	3
	.dw	0,987
	.ascii "TIM2_PWMIConfig"
	.db	0
	.dw	0,(_TIM2_PWMIConfig)
	.dw	0,(XG$TIM2_PWMIConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+4200)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_Channel"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM2_ICPolarity"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	4
	.ascii "TIM2_ICSelection"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	5
	.ascii "TIM2_ICPrescaler"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	6
	.ascii "TIM2_ICFilter"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$284)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$286)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$287)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$289)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$291)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$293)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$294)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$296)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$298)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$315)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$316)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$333)
	.uleb128	7
	.db	2
	.db	145
	.sleb128	-2
	.ascii "icpolarity"
	.db	0
	.dw	0,174
	.uleb128	7
	.db	2
	.db	145
	.sleb128	-1
	.ascii "icselection"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,1050
	.ascii "TIM2_Cmd"
	.db	0
	.dw	0,(_TIM2_Cmd)
	.dw	0,(XG$TIM2_Cmd$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+4096)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "NewState"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$350)
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$352)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$353)
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$355)
	.uleb128	0
	.uleb128	3
	.dw	0,1134
	.ascii "TIM2_ITConfig"
	.db	0
	.dw	0,(_TIM2_ITConfig)
	.dw	0,(XG$TIM2_ITConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+3872)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_IT"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "NewState"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$379)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$381)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$382)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$386)
	.uleb128	0
	.uleb128	3
	.dw	0,1213
	.ascii "TIM2_UpdateDisableConfig"
	.db	0
	.dw	0,(_TIM2_UpdateDisableConfig)
	.dw	0,(XG$TIM2_UpdateDisableConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+3768)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "NewState"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$403)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$405)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$406)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$408)
	.uleb128	0
	.uleb128	3
	.dw	0,1301
	.ascii "TIM2_UpdateRequestConfig"
	.db	0
	.dw	0,(_TIM2_UpdateRequestConfig)
	.dw	0,(XG$TIM2_UpdateRequestConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+3664)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_UpdateSource"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$424)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$426)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$427)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$429)
	.uleb128	0
	.uleb128	3
	.dw	0,1382
	.ascii "TIM2_SelectOnePulseMode"
	.db	0
	.dw	0,(_TIM2_SelectOnePulseMode)
	.dw	0,(XG$TIM2_SelectOnePulseMode$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+3560)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_OPMode"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$445)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$447)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$448)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$450)
	.uleb128	0
	.uleb128	3
	.dw	0,1467
	.ascii "TIM2_PrescalerConfig"
	.db	0
	.dw	0,(_TIM2_PrescalerConfig)
	.dw	0,(XG$TIM2_PrescalerConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+3204)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "Prescaler"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM2_PSCReloadMode"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,1533
	.ascii "TIM2_ForcedOC1Config"
	.db	0
	.dw	0,(_TIM2_ForcedOC1Config)
	.dw	0,(XG$TIM2_ForcedOC1Config$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+3088)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_ForcedAction"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,1599
	.ascii "TIM2_ForcedOC2Config"
	.db	0
	.dw	0,(_TIM2_ForcedOC2Config)
	.dw	0,(XG$TIM2_ForcedOC2Config$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+2972)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_ForcedAction"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,1665
	.ascii "TIM2_ForcedOC3Config"
	.db	0
	.dw	0,(_TIM2_ForcedOC3Config)
	.dw	0,(XG$TIM2_ForcedOC3Config$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+2856)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_ForcedAction"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,1741
	.ascii "TIM2_ARRPreloadConfig"
	.db	0
	.dw	0,(_TIM2_ARRPreloadConfig)
	.dw	0,(XG$TIM2_ARRPreloadConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+2752)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "NewState"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$551)
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$553)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$554)
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$556)
	.uleb128	0
	.uleb128	3
	.dw	0,1817
	.ascii "TIM2_OC1PreloadConfig"
	.db	0
	.dw	0,(_TIM2_OC1PreloadConfig)
	.dw	0,(XG$TIM2_OC1PreloadConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+2648)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "NewState"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$572)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$574)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$575)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$577)
	.uleb128	0
	.uleb128	3
	.dw	0,1893
	.ascii "TIM2_OC2PreloadConfig"
	.db	0
	.dw	0,(_TIM2_OC2PreloadConfig)
	.dw	0,(XG$TIM2_OC2PreloadConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+2544)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "NewState"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$593)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$595)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$596)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$598)
	.uleb128	0
	.uleb128	3
	.dw	0,1969
	.ascii "TIM2_OC3PreloadConfig"
	.db	0
	.dw	0,(_TIM2_OC3PreloadConfig)
	.dw	0,(XG$TIM2_OC3PreloadConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+2440)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "NewState"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$614)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$616)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$617)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$619)
	.uleb128	0
	.uleb128	3
	.dw	0,2032
	.ascii "TIM2_GenerateEvent"
	.db	0
	.dw	0,(_TIM2_GenerateEvent)
	.dw	0,(XG$TIM2_GenerateEvent$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+2348)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_EventSource"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,2116
	.ascii "TIM2_OC1PolarityConfig"
	.db	0
	.dw	0,(_TIM2_OC1PolarityConfig)
	.dw	0,(XG$TIM2_OC1PolarityConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+2244)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_OCPolarity"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$648)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$650)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$651)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$653)
	.uleb128	0
	.uleb128	3
	.dw	0,2200
	.ascii "TIM2_OC2PolarityConfig"
	.db	0
	.dw	0,(_TIM2_OC2PolarityConfig)
	.dw	0,(XG$TIM2_OC2PolarityConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+2140)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_OCPolarity"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$669)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$671)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$672)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$674)
	.uleb128	0
	.uleb128	3
	.dw	0,2284
	.ascii "TIM2_OC3PolarityConfig"
	.db	0
	.dw	0,(_TIM2_OC3PolarityConfig)
	.dw	0,(XG$TIM2_OC3PolarityConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+2036)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_OCPolarity"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$690)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$692)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$693)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$695)
	.uleb128	0
	.uleb128	3
	.dw	0,2433
	.ascii "TIM2_CCxCmd"
	.db	0
	.dw	0,(_TIM2_CCxCmd)
	.dw	0,(XG$TIM2_CCxCmd$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1812)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_Channel"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "NewState"
	.db	0
	.dw	0,174
	.uleb128	8
	.dw	0,2380
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$721)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$723)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$725)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$726)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$728)
	.uleb128	0
	.uleb128	8
	.dw	0,2408
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$731)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$733)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$735)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$736)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$738)
	.uleb128	0
	.uleb128	9
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$740)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$742)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$744)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$745)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$747)
	.uleb128	0
	.uleb128	0
	.uleb128	3
	.dw	0,2536
	.ascii "TIM2_SelectOCxM"
	.db	0
	.dw	0,(_TIM2_SelectOCxM)
	.dw	0,(XG$TIM2_SelectOCxM$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1516)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_Channel"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM2_OCMode"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$779)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$783)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$785)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$789)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$790)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$794)
	.uleb128	0
	.uleb128	3
	.dw	0,2587
	.ascii "TIM2_SetCounter"
	.db	0
	.dw	0,(_TIM2_SetCounter)
	.dw	0,(XG$TIM2_SetCounter$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1496)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "Counter"
	.db	0
	.dw	0,191
	.uleb128	0
	.uleb128	3
	.dw	0,2644
	.ascii "TIM2_SetAutoreload"
	.db	0
	.dw	0,(_TIM2_SetAutoreload)
	.dw	0,(XG$TIM2_SetAutoreload$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1476)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "Autoreload"
	.db	0
	.dw	0,191
	.uleb128	0
	.uleb128	3
	.dw	0,2697
	.ascii "TIM2_SetCompare1"
	.db	0
	.dw	0,(_TIM2_SetCompare1)
	.dw	0,(XG$TIM2_SetCompare1$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1456)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "Compare1"
	.db	0
	.dw	0,191
	.uleb128	0
	.uleb128	3
	.dw	0,2750
	.ascii "TIM2_SetCompare2"
	.db	0
	.dw	0,(_TIM2_SetCompare2)
	.dw	0,(XG$TIM2_SetCompare2$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1436)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "Compare2"
	.db	0
	.dw	0,191
	.uleb128	0
	.uleb128	3
	.dw	0,2803
	.ascii "TIM2_SetCompare3"
	.db	0
	.dw	0,(_TIM2_SetCompare3)
	.dw	0,(XG$TIM2_SetCompare3$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1416)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "Compare3"
	.db	0
	.dw	0,191
	.uleb128	0
	.uleb128	3
	.dw	0,2869
	.ascii "TIM2_SetIC1Prescaler"
	.db	0
	.dw	0,(_TIM2_SetIC1Prescaler)
	.dw	0,(XG$TIM2_SetIC1Prescaler$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1288)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_IC1Prescaler"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,2935
	.ascii "TIM2_SetIC2Prescaler"
	.db	0
	.dw	0,(_TIM2_SetIC2Prescaler)
	.dw	0,(XG$TIM2_SetIC2Prescaler$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1160)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_IC2Prescaler"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,3001
	.ascii "TIM2_SetIC3Prescaler"
	.db	0
	.dw	0,(_TIM2_SetIC3Prescaler)
	.dw	0,(XG$TIM2_SetIC3Prescaler$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1032)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_IC3Prescaler"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	10
	.dw	0,3093
	.ascii "TIM2_GetCapture1"
	.db	0
	.dw	0,(_TIM2_GetCapture1)
	.dw	0,(XG$TIM2_GetCapture1$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+964)
	.dw	0,191
	.uleb128	7
	.db	6
	.db	82
	.db	147
	.uleb128	1
	.db	81
	.db	147
	.uleb128	1
	.ascii "tmpccr1"
	.db	0
	.dw	0,191
	.uleb128	7
	.db	1
	.db	80
	.ascii "tmpccr1l"
	.db	0
	.dw	0,174
	.uleb128	7
	.db	1
	.db	82
	.ascii "tmpccr1h"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	10
	.dw	0,3185
	.ascii "TIM2_GetCapture2"
	.db	0
	.dw	0,(_TIM2_GetCapture2)
	.dw	0,(XG$TIM2_GetCapture2$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+896)
	.dw	0,191
	.uleb128	7
	.db	6
	.db	82
	.db	147
	.uleb128	1
	.db	81
	.db	147
	.uleb128	1
	.ascii "tmpccr2"
	.db	0
	.dw	0,191
	.uleb128	7
	.db	1
	.db	80
	.ascii "tmpccr2l"
	.db	0
	.dw	0,174
	.uleb128	7
	.db	1
	.db	82
	.ascii "tmpccr2h"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	10
	.dw	0,3277
	.ascii "TIM2_GetCapture3"
	.db	0
	.dw	0,(_TIM2_GetCapture3)
	.dw	0,(XG$TIM2_GetCapture3$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+828)
	.dw	0,191
	.uleb128	7
	.db	6
	.db	82
	.db	147
	.uleb128	1
	.db	81
	.db	147
	.uleb128	1
	.ascii "tmpccr3"
	.db	0
	.dw	0,191
	.uleb128	7
	.db	1
	.db	80
	.ascii "tmpccr3l"
	.db	0
	.dw	0,174
	.uleb128	7
	.db	1
	.db	82
	.ascii "tmpccr3h"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	10
	.dw	0,3337
	.ascii "TIM2_GetCounter"
	.db	0
	.dw	0,(_TIM2_GetCounter)
	.dw	0,(XG$TIM2_GetCounter$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+784)
	.dw	0,191
	.uleb128	7
	.db	7
	.db	82
	.db	147
	.uleb128	1
	.db	145
	.sleb128	-3
	.db	147
	.uleb128	1
	.ascii "tmpcntr"
	.db	0
	.dw	0,191
	.uleb128	0
	.uleb128	11
	.ascii "TIM2_GetPrescaler"
	.db	0
	.dw	0,(_TIM2_GetPrescaler)
	.dw	0,(XG$TIM2_GetPrescaler$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+764)
	.dw	0,174
	.uleb128	10
	.dw	0,3507
	.ascii "TIM2_GetFlagStatus"
	.db	0
	.dw	0,(_TIM2_GetFlagStatus)
	.dw	0,(XG$TIM2_GetFlagStatus$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+504)
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_FLAG"
	.db	0
	.dw	0,3507
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$967)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$969)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$970)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$972)
	.uleb128	7
	.db	1
	.db	80
	.ascii "bitstatus"
	.db	0
	.dw	0,174
	.uleb128	7
	.db	2
	.db	145
	.sleb128	-1
	.ascii "tim2_flag_l"
	.db	0
	.dw	0,174
	.uleb128	7
	.db	1
	.db	82
	.ascii "tim2_flag_h"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	5
	.ascii "unsigned int"
	.db	0
	.db	2
	.db	7
	.uleb128	3
	.dw	0,3575
	.ascii "TIM2_ClearFlag"
	.db	0
	.dw	0,(_TIM2_ClearFlag)
	.dw	0,(XG$TIM2_ClearFlag$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+388)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_FLAG"
	.db	0
	.dw	0,3507
	.uleb128	0
	.uleb128	10
	.dw	0,3709
	.ascii "TIM2_GetITStatus"
	.db	0
	.dw	0,(_TIM2_GetITStatus)
	.dw	0,(XG$TIM2_GetITStatus$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+224)
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_IT"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1011)
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1013)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1014)
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1016)
	.uleb128	7
	.db	1
	.db	80
	.ascii "bitstatus"
	.db	0
	.dw	0,174
	.uleb128	7
	.db	2
	.db	145
	.sleb128	-1
	.ascii "TIM2_itStatus"
	.db	0
	.dw	0,174
	.uleb128	7
	.db	1
	.db	80
	.ascii "TIM2_itEnable"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,3767
	.ascii "TIM2_ClearITPendingBit"
	.db	0
	.dw	0,(_TIM2_ClearITPendingBit)
	.dw	0,(XG$TIM2_ClearITPendingBit$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+132)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_IT"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,3886
	.ascii "TI1_Config"
	.db	0
	.dw	0,(_TI1_Config)
	.dw	0,(XFstm8s_tim2$TI1_Config$0$0+1)
	.db	0
	.dw	0,(Ldebug_loc_start+88)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_ICPolarity"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM2_ICSelection"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	4
	.ascii "TIM2_ICFilter"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TI1_Config$1043)
	.dw	0,(Sstm8s_tim2$TI1_Config$1045)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TI1_Config$1046)
	.dw	0,(Sstm8s_tim2$TI1_Config$1048)
	.uleb128	0
	.uleb128	3
	.dw	0,4005
	.ascii "TI2_Config"
	.db	0
	.dw	0,(_TI2_Config)
	.dw	0,(XFstm8s_tim2$TI2_Config$0$0+1)
	.db	0
	.dw	0,(Ldebug_loc_start+44)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_ICPolarity"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM2_ICSelection"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	4
	.ascii "TIM2_ICFilter"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TI2_Config$1062)
	.dw	0,(Sstm8s_tim2$TI2_Config$1064)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TI2_Config$1065)
	.dw	0,(Sstm8s_tim2$TI2_Config$1067)
	.uleb128	0
	.uleb128	3
	.dw	0,4124
	.ascii "TI3_Config"
	.db	0
	.dw	0,(_TI3_Config)
	.dw	0,(XFstm8s_tim2$TI3_Config$0$0+1)
	.db	0
	.dw	0,(Ldebug_loc_start)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM2_ICPolarity"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM2_ICSelection"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	4
	.ascii "TIM2_ICFilter"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TI3_Config$1081)
	.dw	0,(Sstm8s_tim2$TI3_Config$1083)
	.uleb128	6
	.dw	0,(Sstm8s_tim2$TI3_Config$1084)
	.dw	0,(Sstm8s_tim2$TI3_Config$1086)
	.uleb128	0
	.uleb128	12
	.dw	0,174
	.uleb128	13
	.dw	0,4142
	.db	25
	.dw	0,4124
	.uleb128	14
	.db	24
	.uleb128	0
	.uleb128	7
	.db	5
	.db	3
	.dw	0,(___str_0)
	.ascii "__str_0"
	.db	0
	.dw	0,4129
	.uleb128	0
	.uleb128	0
	.uleb128	0
Ldebug_info_end:

	.area .debug_pubnames (NOLOAD)
	.dw	0,Ldebug_pubnames_end-Ldebug_pubnames_start
Ldebug_pubnames_start:
	.dw	2
	.dw	0,(Ldebug_info_start-4)
	.dw	0,4+Ldebug_info_end-Ldebug_info_start
	.dw	0,68
	.ascii "TIM2_DeInit"
	.db	0
	.dw	0,94
	.ascii "TIM2_TimeBaseInit"
	.db	0
	.dw	0,207
	.ascii "TIM2_OC1Init"
	.db	0
	.dw	0,327
	.ascii "TIM2_OC2Init"
	.db	0
	.dw	0,447
	.ascii "TIM2_OC3Init"
	.db	0
	.dw	0,567
	.ascii "TIM2_ICInit"
	.db	0
	.dw	0,742
	.ascii "TIM2_PWMIConfig"
	.db	0
	.dw	0,987
	.ascii "TIM2_Cmd"
	.db	0
	.dw	0,1050
	.ascii "TIM2_ITConfig"
	.db	0
	.dw	0,1134
	.ascii "TIM2_UpdateDisableConfig"
	.db	0
	.dw	0,1213
	.ascii "TIM2_UpdateRequestConfig"
	.db	0
	.dw	0,1301
	.ascii "TIM2_SelectOnePulseMode"
	.db	0
	.dw	0,1382
	.ascii "TIM2_PrescalerConfig"
	.db	0
	.dw	0,1467
	.ascii "TIM2_ForcedOC1Config"
	.db	0
	.dw	0,1533
	.ascii "TIM2_ForcedOC2Config"
	.db	0
	.dw	0,1599
	.ascii "TIM2_ForcedOC3Config"
	.db	0
	.dw	0,1665
	.ascii "TIM2_ARRPreloadConfig"
	.db	0
	.dw	0,1741
	.ascii "TIM2_OC1PreloadConfig"
	.db	0
	.dw	0,1817
	.ascii "TIM2_OC2PreloadConfig"
	.db	0
	.dw	0,1893
	.ascii "TIM2_OC3PreloadConfig"
	.db	0
	.dw	0,1969
	.ascii "TIM2_GenerateEvent"
	.db	0
	.dw	0,2032
	.ascii "TIM2_OC1PolarityConfig"
	.db	0
	.dw	0,2116
	.ascii "TIM2_OC2PolarityConfig"
	.db	0
	.dw	0,2200
	.ascii "TIM2_OC3PolarityConfig"
	.db	0
	.dw	0,2284
	.ascii "TIM2_CCxCmd"
	.db	0
	.dw	0,2433
	.ascii "TIM2_SelectOCxM"
	.db	0
	.dw	0,2536
	.ascii "TIM2_SetCounter"
	.db	0
	.dw	0,2587
	.ascii "TIM2_SetAutoreload"
	.db	0
	.dw	0,2644
	.ascii "TIM2_SetCompare1"
	.db	0
	.dw	0,2697
	.ascii "TIM2_SetCompare2"
	.db	0
	.dw	0,2750
	.ascii "TIM2_SetCompare3"
	.db	0
	.dw	0,2803
	.ascii "TIM2_SetIC1Prescaler"
	.db	0
	.dw	0,2869
	.ascii "TIM2_SetIC2Prescaler"
	.db	0
	.dw	0,2935
	.ascii "TIM2_SetIC3Prescaler"
	.db	0
	.dw	0,3001
	.ascii "TIM2_GetCapture1"
	.db	0
	.dw	0,3093
	.ascii "TIM2_GetCapture2"
	.db	0
	.dw	0,3185
	.ascii "TIM2_GetCapture3"
	.db	0
	.dw	0,3277
	.ascii "TIM2_GetCounter"
	.db	0
	.dw	0,3337
	.ascii "TIM2_GetPrescaler"
	.db	0
	.dw	0,3373
	.ascii "TIM2_GetFlagStatus"
	.db	0
	.dw	0,3523
	.ascii "TIM2_ClearFlag"
	.db	0
	.dw	0,3575
	.ascii "TIM2_GetITStatus"
	.db	0
	.dw	0,3709
	.ascii "TIM2_ClearITPendingBit"
	.db	0
	.dw	0,0
Ldebug_pubnames_end:

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE0_end-Ldebug_CIE0_start
Ldebug_CIE0_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE0_end:
	.dw	0,33
	.dw	0,(Ldebug_CIE0_start-4)
	.dw	0,(Sstm8s_tim2$TI3_Config$1074)	;initial loc
	.dw	0,Sstm8s_tim2$TI3_Config$1091-Sstm8s_tim2$TI3_Config$1074
	.db	1
	.dw	0,(Sstm8s_tim2$TI3_Config$1074)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TI3_Config$1075)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TI3_Config$1089)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE1_end-Ldebug_CIE1_start
Ldebug_CIE1_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE1_end:
	.dw	0,33
	.dw	0,(Ldebug_CIE1_start-4)
	.dw	0,(Sstm8s_tim2$TI2_Config$1055)	;initial loc
	.dw	0,Sstm8s_tim2$TI2_Config$1072-Sstm8s_tim2$TI2_Config$1055
	.db	1
	.dw	0,(Sstm8s_tim2$TI2_Config$1055)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TI2_Config$1056)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TI2_Config$1070)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE2_end-Ldebug_CIE2_start
Ldebug_CIE2_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE2_end:
	.dw	0,33
	.dw	0,(Ldebug_CIE2_start-4)
	.dw	0,(Sstm8s_tim2$TI1_Config$1036)	;initial loc
	.dw	0,Sstm8s_tim2$TI1_Config$1053-Sstm8s_tim2$TI1_Config$1036
	.db	1
	.dw	0,(Sstm8s_tim2$TI1_Config$1036)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TI1_Config$1037)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TI1_Config$1051)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE3_end-Ldebug_CIE3_start
Ldebug_CIE3_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE3_end:
	.dw	0,61
	.dw	0,(Ldebug_CIE3_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1023)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_ClearITPendingBit$1034-Sstm8s_tim2$TIM2_ClearITPendingBit$1023
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1023)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1025)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1026)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1027)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1028)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1029)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearITPendingBit$1030)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE4_end-Ldebug_CIE4_start
Ldebug_CIE4_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE4_end:
	.dw	0,103
	.dw	0,(Ldebug_CIE4_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$995)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_GetITStatus$1021-Sstm8s_tim2$TIM2_GetITStatus$995
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$995)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$996)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$998)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$999)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1000)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1001)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1002)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1003)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1004)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1005)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1006)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1007)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetITStatus$1019)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE5_end-Ldebug_CIE5_start
Ldebug_CIE5_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE5_end:
	.dw	0,75
	.dw	0,(Ldebug_CIE5_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$979)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_ClearFlag$993-Sstm8s_tim2$TIM2_ClearFlag$979
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$979)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$980)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$982)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$983)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$984)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$985)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$986)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$987)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ClearFlag$991)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE6_end-Ldebug_CIE6_start
Ldebug_CIE6_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE6_end:
	.dw	0,159
	.dw	0,(Ldebug_CIE6_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$943)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_GetFlagStatus$977-Sstm8s_tim2$TIM2_GetFlagStatus$943
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$943)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$944)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$946)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$947)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$948)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$949)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$950)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$951)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$952)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$953)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$954)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$955)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$956)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$957)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$958)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$959)
	.db	14
	.uleb128	11
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$960)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$961)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$965)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$966)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetFlagStatus$975)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE7_end-Ldebug_CIE7_start
Ldebug_CIE7_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE7_end:
	.dw	0,19
	.dw	0,(Ldebug_CIE7_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_GetPrescaler$937)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_GetPrescaler$941-Sstm8s_tim2$TIM2_GetPrescaler$937
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetPrescaler$937)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE8_end-Ldebug_CIE8_start
Ldebug_CIE8_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE8_end:
	.dw	0,33
	.dw	0,(Ldebug_CIE8_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_GetCounter$928)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_GetCounter$935-Sstm8s_tim2$TIM2_GetCounter$928
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCounter$928)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCounter$929)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCounter$933)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE9_end-Ldebug_CIE9_start
Ldebug_CIE9_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE9_end:
	.dw	0,47
	.dw	0,(Ldebug_CIE9_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture3$914)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_GetCapture3$926-Sstm8s_tim2$TIM2_GetCapture3$914
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture3$914)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture3$915)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture3$920)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture3$921)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture3$924)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE10_end-Ldebug_CIE10_start
Ldebug_CIE10_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE10_end:
	.dw	0,47
	.dw	0,(Ldebug_CIE10_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture2$900)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_GetCapture2$912-Sstm8s_tim2$TIM2_GetCapture2$900
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture2$900)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture2$901)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture2$906)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture2$907)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture2$910)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE11_end-Ldebug_CIE11_start
Ldebug_CIE11_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE11_end:
	.dw	0,47
	.dw	0,(Ldebug_CIE11_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture1$886)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_GetCapture1$898-Sstm8s_tim2$TIM2_GetCapture1$886
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture1$886)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture1$887)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture1$892)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture1$893)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GetCapture1$896)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE12_end-Ldebug_CIE12_start
Ldebug_CIE12_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE12_end:
	.dw	0,82
	.dw	0,(Ldebug_CIE12_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$869)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_SetIC3Prescaler$884-Sstm8s_tim2$TIM2_SetIC3Prescaler$869
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$869)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$871)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$872)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$873)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$874)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$875)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$876)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$877)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$878)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC3Prescaler$879)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE13_end-Ldebug_CIE13_start
Ldebug_CIE13_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE13_end:
	.dw	0,82
	.dw	0,(Ldebug_CIE13_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$852)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_SetIC2Prescaler$867-Sstm8s_tim2$TIM2_SetIC2Prescaler$852
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$852)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$854)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$855)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$856)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$857)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$858)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$859)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$860)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$861)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC2Prescaler$862)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE14_end-Ldebug_CIE14_start
Ldebug_CIE14_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE14_end:
	.dw	0,82
	.dw	0,(Ldebug_CIE14_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$835)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_SetIC1Prescaler$850-Sstm8s_tim2$TIM2_SetIC1Prescaler$835
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$835)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$837)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$838)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$839)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$840)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$841)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$842)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$843)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$844)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetIC1Prescaler$845)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE15_end-Ldebug_CIE15_start
Ldebug_CIE15_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE15_end:
	.dw	0,19
	.dw	0,(Ldebug_CIE15_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_SetCompare3$828)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_SetCompare3$833-Sstm8s_tim2$TIM2_SetCompare3$828
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetCompare3$828)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE16_end-Ldebug_CIE16_start
Ldebug_CIE16_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE16_end:
	.dw	0,19
	.dw	0,(Ldebug_CIE16_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_SetCompare2$821)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_SetCompare2$826-Sstm8s_tim2$TIM2_SetCompare2$821
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetCompare2$821)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE17_end-Ldebug_CIE17_start
Ldebug_CIE17_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE17_end:
	.dw	0,19
	.dw	0,(Ldebug_CIE17_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_SetCompare1$814)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_SetCompare1$819-Sstm8s_tim2$TIM2_SetCompare1$814
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetCompare1$814)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE18_end-Ldebug_CIE18_start
Ldebug_CIE18_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE18_end:
	.dw	0,19
	.dw	0,(Ldebug_CIE18_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_SetAutoreload$807)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_SetAutoreload$812-Sstm8s_tim2$TIM2_SetAutoreload$807
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetAutoreload$807)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE19_end-Ldebug_CIE19_start
Ldebug_CIE19_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE19_end:
	.dw	0,19
	.dw	0,(Ldebug_CIE19_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_SetCounter$800)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_SetCounter$805-Sstm8s_tim2$TIM2_SetCounter$800
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SetCounter$800)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE20_end-Ldebug_CIE20_start
Ldebug_CIE20_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE20_end:
	.dw	0,180
	.dw	0,(Ldebug_CIE20_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$753)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_SelectOCxM$798-Sstm8s_tim2$TIM2_SelectOCxM$753
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$753)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$754)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$756)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$757)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$758)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$759)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$760)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$761)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$762)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$763)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$765)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$766)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$767)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$768)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$769)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$770)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$771)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$772)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$773)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$774)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$775)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$776)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$777)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOCxM$796)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE21_end-Ldebug_CIE21_start
Ldebug_CIE21_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE21_end:
	.dw	0,138
	.dw	0,(Ldebug_CIE21_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$700)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_CCxCmd$751-Sstm8s_tim2$TIM2_CCxCmd$700
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$700)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$701)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$703)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$704)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$705)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$706)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$707)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$708)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$709)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$710)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$712)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$713)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$714)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$715)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$716)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$717)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$718)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_CCxCmd$749)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE22_end-Ldebug_CIE22_start
Ldebug_CIE22_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE22_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE22_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$679)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_OC3PolarityConfig$698-Sstm8s_tim2$TIM2_OC3PolarityConfig$679
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$679)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$681)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$682)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$683)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$684)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$685)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$686)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PolarityConfig$687)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE23_end-Ldebug_CIE23_start
Ldebug_CIE23_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE23_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE23_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$658)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_OC2PolarityConfig$677-Sstm8s_tim2$TIM2_OC2PolarityConfig$658
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$658)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$660)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$661)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$662)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$663)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$664)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$665)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PolarityConfig$666)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE24_end-Ldebug_CIE24_start
Ldebug_CIE24_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE24_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE24_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$637)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_OC1PolarityConfig$656-Sstm8s_tim2$TIM2_OC1PolarityConfig$637
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$637)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$639)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$640)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$641)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$642)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$643)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$644)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PolarityConfig$645)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE25_end-Ldebug_CIE25_start
Ldebug_CIE25_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE25_end:
	.dw	0,61
	.dw	0,(Ldebug_CIE25_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$624)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_GenerateEvent$635-Sstm8s_tim2$TIM2_GenerateEvent$624
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$624)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$626)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$627)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$628)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$629)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$630)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_GenerateEvent$631)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE26_end-Ldebug_CIE26_start
Ldebug_CIE26_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE26_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE26_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$603)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_OC3PreloadConfig$622-Sstm8s_tim2$TIM2_OC3PreloadConfig$603
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$603)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$605)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$606)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$607)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$608)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$609)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$610)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3PreloadConfig$611)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE27_end-Ldebug_CIE27_start
Ldebug_CIE27_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE27_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE27_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$582)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_OC2PreloadConfig$601-Sstm8s_tim2$TIM2_OC2PreloadConfig$582
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$582)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$584)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$585)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$586)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$587)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$588)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$589)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2PreloadConfig$590)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE28_end-Ldebug_CIE28_start
Ldebug_CIE28_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE28_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE28_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$561)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_OC1PreloadConfig$580-Sstm8s_tim2$TIM2_OC1PreloadConfig$561
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$561)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$563)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$564)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$565)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$566)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$567)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$568)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1PreloadConfig$569)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE29_end-Ldebug_CIE29_start
Ldebug_CIE29_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE29_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE29_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$540)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_ARRPreloadConfig$559-Sstm8s_tim2$TIM2_ARRPreloadConfig$540
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$540)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$542)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$543)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$544)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$545)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$546)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$547)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ARRPreloadConfig$548)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE30_end-Ldebug_CIE30_start
Ldebug_CIE30_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE30_end:
	.dw	0,75
	.dw	0,(Ldebug_CIE30_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$524)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_ForcedOC3Config$538-Sstm8s_tim2$TIM2_ForcedOC3Config$524
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$524)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$526)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$527)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$528)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$529)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$530)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$531)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$532)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC3Config$533)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE31_end-Ldebug_CIE31_start
Ldebug_CIE31_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE31_end:
	.dw	0,75
	.dw	0,(Ldebug_CIE31_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$508)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_ForcedOC2Config$522-Sstm8s_tim2$TIM2_ForcedOC2Config$508
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$508)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$510)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$511)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$512)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$513)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$514)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$515)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$516)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC2Config$517)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE32_end-Ldebug_CIE32_start
Ldebug_CIE32_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE32_end:
	.dw	0,75
	.dw	0,(Ldebug_CIE32_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$492)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_ForcedOC1Config$506-Sstm8s_tim2$TIM2_ForcedOC1Config$492
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$492)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$494)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$495)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$496)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$497)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$498)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$499)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$500)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ForcedOC1Config$501)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE33_end-Ldebug_CIE33_start
Ldebug_CIE33_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE33_end:
	.dw	0,215
	.dw	0,(Ldebug_CIE33_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$455)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_PrescalerConfig$490-Sstm8s_tim2$TIM2_PrescalerConfig$455
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$455)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$457)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$458)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$459)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$460)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$461)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$462)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$463)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$465)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$466)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$467)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$468)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$469)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$470)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$471)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$472)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$473)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$474)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$475)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$476)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$477)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$478)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$479)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$480)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$481)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$482)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$483)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$484)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PrescalerConfig$485)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE34_end-Ldebug_CIE34_start
Ldebug_CIE34_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE34_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE34_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$434)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_SelectOnePulseMode$453-Sstm8s_tim2$TIM2_SelectOnePulseMode$434
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$434)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$436)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$437)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$438)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$439)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$440)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$441)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_SelectOnePulseMode$442)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE35_end-Ldebug_CIE35_start
Ldebug_CIE35_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE35_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE35_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$413)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_UpdateRequestConfig$432-Sstm8s_tim2$TIM2_UpdateRequestConfig$413
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$413)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$415)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$416)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$417)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$418)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$419)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$420)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateRequestConfig$421)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE36_end-Ldebug_CIE36_start
Ldebug_CIE36_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE36_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE36_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$392)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_UpdateDisableConfig$411-Sstm8s_tim2$TIM2_UpdateDisableConfig$392
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$392)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$394)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$395)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$396)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$397)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$398)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$399)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_UpdateDisableConfig$400)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE37_end-Ldebug_CIE37_start
Ldebug_CIE37_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE37_end:
	.dw	0,138
	.dw	0,(Ldebug_CIE37_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$360)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_ITConfig$390-Sstm8s_tim2$TIM2_ITConfig$360
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$360)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$361)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$363)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$364)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$365)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$366)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$367)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$368)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$370)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$371)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$372)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$373)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$374)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$375)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$376)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$384)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$385)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ITConfig$388)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE38_end-Ldebug_CIE38_start
Ldebug_CIE38_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE38_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE38_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$339)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_Cmd$358-Sstm8s_tim2$TIM2_Cmd$339
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$339)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$341)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$342)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$343)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$344)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$345)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$346)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_Cmd$347)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE39_end-Ldebug_CIE39_start
Ldebug_CIE39_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE39_end:
	.dw	0,425
	.dw	0,(Ldebug_CIE39_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$245)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_PWMIConfig$337-Sstm8s_tim2$TIM2_PWMIConfig$245
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$245)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$246)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$248)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$249)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$250)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$251)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$252)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$253)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$254)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$256)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$257)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$258)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$259)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$260)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$261)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$262)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$264)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$265)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$266)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$267)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$268)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$269)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$270)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$271)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$272)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$274)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$275)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$276)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$277)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$278)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$279)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$280)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$281)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$282)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$300)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$301)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$302)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$303)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$305)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$306)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$308)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$309)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$310)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$311)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$313)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$314)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$318)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$319)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$320)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$321)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$323)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$324)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$326)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$327)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$328)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$329)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$331)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$332)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_PWMIConfig$335)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE40_end-Ldebug_CIE40_start
Ldebug_CIE40_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE40_end:
	.dw	0,432
	.dw	0,(Ldebug_CIE40_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$162)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_ICInit$243-Sstm8s_tim2$TIM2_ICInit$162
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$162)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$163)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$165)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$166)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$167)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$168)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$169)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$170)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$171)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$172)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$174)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$175)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$176)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$177)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$178)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$179)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$180)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$182)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$183)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$184)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$185)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$186)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$187)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$188)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$189)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$190)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$192)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$193)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$194)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$195)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$196)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$197)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$198)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$199)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$200)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$202)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$203)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$204)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$205)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$206)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$207)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$211)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$212)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$213)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$214)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$216)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$217)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$222)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$223)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$224)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$225)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$227)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$228)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$232)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$233)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$234)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$235)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$237)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$238)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_ICInit$241)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE41_end-Ldebug_CIE41_start
Ldebug_CIE41_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE41_end:
	.dw	0,208
	.dw	0,(Ldebug_CIE41_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$120)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_OC3Init$160-Sstm8s_tim2$TIM2_OC3Init$120
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$120)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$121)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$123)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$124)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$125)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$126)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$127)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$128)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$129)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$130)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$131)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$132)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$133)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$135)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$136)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$137)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$138)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$139)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$140)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$141)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$143)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$144)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$145)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$146)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$147)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$148)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$149)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC3Init$158)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE42_end-Ldebug_CIE42_start
Ldebug_CIE42_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE42_end:
	.dw	0,208
	.dw	0,(Ldebug_CIE42_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$78)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_OC2Init$118-Sstm8s_tim2$TIM2_OC2Init$78
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$78)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$79)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$81)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$82)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$83)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$84)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$85)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$86)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$87)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$88)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$89)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$90)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$91)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$93)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$94)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$95)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$96)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$97)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$98)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$99)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$101)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$102)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$103)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$104)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$105)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$106)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$107)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC2Init$116)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE43_end-Ldebug_CIE43_start
Ldebug_CIE43_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE43_end:
	.dw	0,208
	.dw	0,(Ldebug_CIE43_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$36)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_OC1Init$76-Sstm8s_tim2$TIM2_OC1Init$36
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$36)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$37)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$39)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$40)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$41)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$42)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$43)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$44)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$45)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$46)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$47)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$48)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$49)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$51)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$52)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$53)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$54)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$55)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$56)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$57)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$59)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$60)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$61)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$62)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$63)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$64)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$65)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_OC1Init$74)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE44_end-Ldebug_CIE44_start
Ldebug_CIE44_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE44_end:
	.dw	0,19
	.dw	0,(Ldebug_CIE44_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_TimeBaseInit$28)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_TimeBaseInit$34-Sstm8s_tim2$TIM2_TimeBaseInit$28
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_TimeBaseInit$28)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE45_end-Ldebug_CIE45_start
Ldebug_CIE45_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE45_end:
	.dw	0,19
	.dw	0,(Ldebug_CIE45_start-4)
	.dw	0,(Sstm8s_tim2$TIM2_DeInit$1)	;initial loc
	.dw	0,Sstm8s_tim2$TIM2_DeInit$26-Sstm8s_tim2$TIM2_DeInit$1
	.db	1
	.dw	0,(Sstm8s_tim2$TIM2_DeInit$1)
	.db	14
	.uleb128	2
