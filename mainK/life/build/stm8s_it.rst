                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.1.0 #12072 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module stm8s_it
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _TRAP_IRQHandler
                                     12 	.globl _TLI_IRQHandler
                                     13 	.globl _AWU_IRQHandler
                                     14 	.globl _CLK_IRQHandler
                                     15 	.globl _EXTI_PORTA_IRQHandler
                                     16 	.globl _EXTI_PORTB_IRQHandler
                                     17 	.globl _EXTI_PORTC_IRQHandler
                                     18 	.globl _EXTI_PORTD_IRQHandler
                                     19 	.globl _CAN_RX_IRQHandler
                                     20 	.globl _CAN_TX_IRQHandler
                                     21 	.globl _SPI_IRQHandler
                                     22 	.globl _TIM1_UPD_OVF_TRG_BRK_IRQHandler
                                     23 	.globl _TIM1_CAP_COM_IRQHandler
                                     24 	.globl _TIM2_UPD_OVF_BRK_IRQHandler
                                     25 	.globl _TIM2_CAP_COM_IRQHandler
                                     26 	.globl _TIM3_UPD_OVF_BRK_IRQHandler
                                     27 	.globl _TIM3_CAP_COM_IRQHandler
                                     28 	.globl _UART1_TX_IRQHandler
                                     29 	.globl _UART1_RX_IRQHandler
                                     30 	.globl _I2C_IRQHandler
                                     31 	.globl _UART3_TX_IRQHandler
                                     32 	.globl _UART3_RX_IRQHandler
                                     33 	.globl _ADC2_IRQHandler
                                     34 	.globl _TIM4_UPD_OVF_IRQHandler
                                     35 	.globl _EEPROM_EEC_IRQHandler
                                     36 ;--------------------------------------------------------
                                     37 ; ram data
                                     38 ;--------------------------------------------------------
                                     39 	.area DATA
                                     40 ;--------------------------------------------------------
                                     41 ; ram data
                                     42 ;--------------------------------------------------------
                                     43 	.area INITIALIZED
                                     44 ;--------------------------------------------------------
                                     45 ; absolute external ram data
                                     46 ;--------------------------------------------------------
                                     47 	.area DABS (ABS)
                                     48 
                                     49 ; default segment ordering for linker
                                     50 	.area HOME
                                     51 	.area GSINIT
                                     52 	.area GSFINAL
                                     53 	.area CONST
                                     54 	.area INITIALIZER
                                     55 	.area CODE
                                     56 
                                     57 ;--------------------------------------------------------
                                     58 ; global & static initialisations
                                     59 ;--------------------------------------------------------
                                     60 	.area HOME
                                     61 	.area GSINIT
                                     62 	.area GSFINAL
                                     63 	.area GSINIT
                                     64 ;--------------------------------------------------------
                                     65 ; Home
                                     66 ;--------------------------------------------------------
                                     67 	.area HOME
                                     68 	.area HOME
                                     69 ;--------------------------------------------------------
                                     70 ; code
                                     71 ;--------------------------------------------------------
                                     72 	.area CODE
                           000000    73 	Sstm8s_it$TRAP_IRQHandler$0 ==.
                                     74 ;	app/src/stm8s_it.c: 62: INTERRUPT_HANDLER_TRAP(TRAP_IRQHandler)
                                     75 ;	-----------------------------------------
                                     76 ;	 function TRAP_IRQHandler
                                     77 ;	-----------------------------------------
      0084DA                         78 _TRAP_IRQHandler:
                           000000    79 	Sstm8s_it$TRAP_IRQHandler$1 ==.
                           000000    80 	Sstm8s_it$TRAP_IRQHandler$2 ==.
                                     81 ;	app/src/stm8s_it.c: 67: }
                           000000    82 	Sstm8s_it$TRAP_IRQHandler$3 ==.
                           000000    83 	XG$TRAP_IRQHandler$0$0 ==.
      0084DA 80               [11]   84 	iret
                           000001    85 	Sstm8s_it$TRAP_IRQHandler$4 ==.
                           000001    86 	Sstm8s_it$TLI_IRQHandler$5 ==.
                                     87 ;	app/src/stm8s_it.c: 74: INTERRUPT_HANDLER(TLI_IRQHandler, 0)
                                     88 ;	-----------------------------------------
                                     89 ;	 function TLI_IRQHandler
                                     90 ;	-----------------------------------------
      0084DB                         91 _TLI_IRQHandler:
                           000001    92 	Sstm8s_it$TLI_IRQHandler$6 ==.
                           000001    93 	Sstm8s_it$TLI_IRQHandler$7 ==.
                                     94 ;	app/src/stm8s_it.c: 79: }
                           000001    95 	Sstm8s_it$TLI_IRQHandler$8 ==.
                           000001    96 	XG$TLI_IRQHandler$0$0 ==.
      0084DB 80               [11]   97 	iret
                           000002    98 	Sstm8s_it$TLI_IRQHandler$9 ==.
                           000002    99 	Sstm8s_it$AWU_IRQHandler$10 ==.
                                    100 ;	app/src/stm8s_it.c: 86: INTERRUPT_HANDLER(AWU_IRQHandler, 1)
                                    101 ;	-----------------------------------------
                                    102 ;	 function AWU_IRQHandler
                                    103 ;	-----------------------------------------
      0084DC                        104 _AWU_IRQHandler:
                           000002   105 	Sstm8s_it$AWU_IRQHandler$11 ==.
                           000002   106 	Sstm8s_it$AWU_IRQHandler$12 ==.
                                    107 ;	app/src/stm8s_it.c: 91: }
                           000002   108 	Sstm8s_it$AWU_IRQHandler$13 ==.
                           000002   109 	XG$AWU_IRQHandler$0$0 ==.
      0084DC 80               [11]  110 	iret
                           000003   111 	Sstm8s_it$AWU_IRQHandler$14 ==.
                           000003   112 	Sstm8s_it$CLK_IRQHandler$15 ==.
                                    113 ;	app/src/stm8s_it.c: 98: INTERRUPT_HANDLER(CLK_IRQHandler, 2)
                                    114 ;	-----------------------------------------
                                    115 ;	 function CLK_IRQHandler
                                    116 ;	-----------------------------------------
      0084DD                        117 _CLK_IRQHandler:
                           000003   118 	Sstm8s_it$CLK_IRQHandler$16 ==.
                           000003   119 	Sstm8s_it$CLK_IRQHandler$17 ==.
                                    120 ;	app/src/stm8s_it.c: 103: }
                           000003   121 	Sstm8s_it$CLK_IRQHandler$18 ==.
                           000003   122 	XG$CLK_IRQHandler$0$0 ==.
      0084DD 80               [11]  123 	iret
                           000004   124 	Sstm8s_it$CLK_IRQHandler$19 ==.
                           000004   125 	Sstm8s_it$EXTI_PORTA_IRQHandler$20 ==.
                                    126 ;	app/src/stm8s_it.c: 110: INTERRUPT_HANDLER(EXTI_PORTA_IRQHandler, 3)
                                    127 ;	-----------------------------------------
                                    128 ;	 function EXTI_PORTA_IRQHandler
                                    129 ;	-----------------------------------------
      0084DE                        130 _EXTI_PORTA_IRQHandler:
                           000004   131 	Sstm8s_it$EXTI_PORTA_IRQHandler$21 ==.
                           000004   132 	Sstm8s_it$EXTI_PORTA_IRQHandler$22 ==.
                                    133 ;	app/src/stm8s_it.c: 115: }
                           000004   134 	Sstm8s_it$EXTI_PORTA_IRQHandler$23 ==.
                           000004   135 	XG$EXTI_PORTA_IRQHandler$0$0 ==.
      0084DE 80               [11]  136 	iret
                           000005   137 	Sstm8s_it$EXTI_PORTA_IRQHandler$24 ==.
                           000005   138 	Sstm8s_it$EXTI_PORTB_IRQHandler$25 ==.
                                    139 ;	app/src/stm8s_it.c: 122: INTERRUPT_HANDLER(EXTI_PORTB_IRQHandler, 4)
                                    140 ;	-----------------------------------------
                                    141 ;	 function EXTI_PORTB_IRQHandler
                                    142 ;	-----------------------------------------
      0084DF                        143 _EXTI_PORTB_IRQHandler:
                           000005   144 	Sstm8s_it$EXTI_PORTB_IRQHandler$26 ==.
                           000005   145 	Sstm8s_it$EXTI_PORTB_IRQHandler$27 ==.
                                    146 ;	app/src/stm8s_it.c: 127: }
                           000005   147 	Sstm8s_it$EXTI_PORTB_IRQHandler$28 ==.
                           000005   148 	XG$EXTI_PORTB_IRQHandler$0$0 ==.
      0084DF 80               [11]  149 	iret
                           000006   150 	Sstm8s_it$EXTI_PORTB_IRQHandler$29 ==.
                           000006   151 	Sstm8s_it$EXTI_PORTC_IRQHandler$30 ==.
                                    152 ;	app/src/stm8s_it.c: 134: INTERRUPT_HANDLER(EXTI_PORTC_IRQHandler, 5)
                                    153 ;	-----------------------------------------
                                    154 ;	 function EXTI_PORTC_IRQHandler
                                    155 ;	-----------------------------------------
      0084E0                        156 _EXTI_PORTC_IRQHandler:
                           000006   157 	Sstm8s_it$EXTI_PORTC_IRQHandler$31 ==.
                           000006   158 	Sstm8s_it$EXTI_PORTC_IRQHandler$32 ==.
                                    159 ;	app/src/stm8s_it.c: 139: }
                           000006   160 	Sstm8s_it$EXTI_PORTC_IRQHandler$33 ==.
                           000006   161 	XG$EXTI_PORTC_IRQHandler$0$0 ==.
      0084E0 80               [11]  162 	iret
                           000007   163 	Sstm8s_it$EXTI_PORTC_IRQHandler$34 ==.
                           000007   164 	Sstm8s_it$EXTI_PORTD_IRQHandler$35 ==.
                                    165 ;	app/src/stm8s_it.c: 146: INTERRUPT_HANDLER(EXTI_PORTD_IRQHandler, 6)
                                    166 ;	-----------------------------------------
                                    167 ;	 function EXTI_PORTD_IRQHandler
                                    168 ;	-----------------------------------------
      0084E1                        169 _EXTI_PORTD_IRQHandler:
                           000007   170 	Sstm8s_it$EXTI_PORTD_IRQHandler$36 ==.
                           000007   171 	Sstm8s_it$EXTI_PORTD_IRQHandler$37 ==.
                                    172 ;	app/src/stm8s_it.c: 151: }
                           000007   173 	Sstm8s_it$EXTI_PORTD_IRQHandler$38 ==.
                           000007   174 	XG$EXTI_PORTD_IRQHandler$0$0 ==.
      0084E1 80               [11]  175 	iret
                           000008   176 	Sstm8s_it$EXTI_PORTD_IRQHandler$39 ==.
                           000008   177 	Sstm8s_it$CAN_RX_IRQHandler$40 ==.
                                    178 ;	app/src/stm8s_it.c: 181: INTERRUPT_HANDLER(CAN_RX_IRQHandler, 8)
                                    179 ;	-----------------------------------------
                                    180 ;	 function CAN_RX_IRQHandler
                                    181 ;	-----------------------------------------
      0084E2                        182 _CAN_RX_IRQHandler:
                           000008   183 	Sstm8s_it$CAN_RX_IRQHandler$41 ==.
                           000008   184 	Sstm8s_it$CAN_RX_IRQHandler$42 ==.
                                    185 ;	app/src/stm8s_it.c: 186: }
                           000008   186 	Sstm8s_it$CAN_RX_IRQHandler$43 ==.
                           000008   187 	XG$CAN_RX_IRQHandler$0$0 ==.
      0084E2 80               [11]  188 	iret
                           000009   189 	Sstm8s_it$CAN_RX_IRQHandler$44 ==.
                           000009   190 	Sstm8s_it$CAN_TX_IRQHandler$45 ==.
                                    191 ;	app/src/stm8s_it.c: 193: INTERRUPT_HANDLER(CAN_TX_IRQHandler, 9)
                                    192 ;	-----------------------------------------
                                    193 ;	 function CAN_TX_IRQHandler
                                    194 ;	-----------------------------------------
      0084E3                        195 _CAN_TX_IRQHandler:
                           000009   196 	Sstm8s_it$CAN_TX_IRQHandler$46 ==.
                           000009   197 	Sstm8s_it$CAN_TX_IRQHandler$47 ==.
                                    198 ;	app/src/stm8s_it.c: 198: }
                           000009   199 	Sstm8s_it$CAN_TX_IRQHandler$48 ==.
                           000009   200 	XG$CAN_TX_IRQHandler$0$0 ==.
      0084E3 80               [11]  201 	iret
                           00000A   202 	Sstm8s_it$CAN_TX_IRQHandler$49 ==.
                           00000A   203 	Sstm8s_it$SPI_IRQHandler$50 ==.
                                    204 ;	app/src/stm8s_it.c: 206: INTERRUPT_HANDLER(SPI_IRQHandler, 10)
                                    205 ;	-----------------------------------------
                                    206 ;	 function SPI_IRQHandler
                                    207 ;	-----------------------------------------
      0084E4                        208 _SPI_IRQHandler:
                           00000A   209 	Sstm8s_it$SPI_IRQHandler$51 ==.
                           00000A   210 	Sstm8s_it$SPI_IRQHandler$52 ==.
                                    211 ;	app/src/stm8s_it.c: 211: }
                           00000A   212 	Sstm8s_it$SPI_IRQHandler$53 ==.
                           00000A   213 	XG$SPI_IRQHandler$0$0 ==.
      0084E4 80               [11]  214 	iret
                           00000B   215 	Sstm8s_it$SPI_IRQHandler$54 ==.
                           00000B   216 	Sstm8s_it$TIM1_UPD_OVF_TRG_BRK_IRQHandler$55 ==.
                                    217 ;	app/src/stm8s_it.c: 218: INTERRUPT_HANDLER(TIM1_UPD_OVF_TRG_BRK_IRQHandler, 11)
                                    218 ;	-----------------------------------------
                                    219 ;	 function TIM1_UPD_OVF_TRG_BRK_IRQHandler
                                    220 ;	-----------------------------------------
      0084E5                        221 _TIM1_UPD_OVF_TRG_BRK_IRQHandler:
                           00000B   222 	Sstm8s_it$TIM1_UPD_OVF_TRG_BRK_IRQHandler$56 ==.
                           00000B   223 	Sstm8s_it$TIM1_UPD_OVF_TRG_BRK_IRQHandler$57 ==.
                                    224 ;	app/src/stm8s_it.c: 223: }
                           00000B   225 	Sstm8s_it$TIM1_UPD_OVF_TRG_BRK_IRQHandler$58 ==.
                           00000B   226 	XG$TIM1_UPD_OVF_TRG_BRK_IRQHandler$0$0 ==.
      0084E5 80               [11]  227 	iret
                           00000C   228 	Sstm8s_it$TIM1_UPD_OVF_TRG_BRK_IRQHandler$59 ==.
                           00000C   229 	Sstm8s_it$TIM1_CAP_COM_IRQHandler$60 ==.
                                    230 ;	app/src/stm8s_it.c: 230: INTERRUPT_HANDLER(TIM1_CAP_COM_IRQHandler, 12)
                                    231 ;	-----------------------------------------
                                    232 ;	 function TIM1_CAP_COM_IRQHandler
                                    233 ;	-----------------------------------------
      0084E6                        234 _TIM1_CAP_COM_IRQHandler:
                           00000C   235 	Sstm8s_it$TIM1_CAP_COM_IRQHandler$61 ==.
                           00000C   236 	Sstm8s_it$TIM1_CAP_COM_IRQHandler$62 ==.
                                    237 ;	app/src/stm8s_it.c: 235: }
                           00000C   238 	Sstm8s_it$TIM1_CAP_COM_IRQHandler$63 ==.
                           00000C   239 	XG$TIM1_CAP_COM_IRQHandler$0$0 ==.
      0084E6 80               [11]  240 	iret
                           00000D   241 	Sstm8s_it$TIM1_CAP_COM_IRQHandler$64 ==.
                           00000D   242 	Sstm8s_it$TIM2_UPD_OVF_BRK_IRQHandler$65 ==.
                                    243 ;	app/src/stm8s_it.c: 270: INTERRUPT_HANDLER(TIM2_UPD_OVF_BRK_IRQHandler, 13)
                                    244 ;	-----------------------------------------
                                    245 ;	 function TIM2_UPD_OVF_BRK_IRQHandler
                                    246 ;	-----------------------------------------
      0084E7                        247 _TIM2_UPD_OVF_BRK_IRQHandler:
                           00000D   248 	Sstm8s_it$TIM2_UPD_OVF_BRK_IRQHandler$66 ==.
                           00000D   249 	Sstm8s_it$TIM2_UPD_OVF_BRK_IRQHandler$67 ==.
                                    250 ;	app/src/stm8s_it.c: 275: }
                           00000D   251 	Sstm8s_it$TIM2_UPD_OVF_BRK_IRQHandler$68 ==.
                           00000D   252 	XG$TIM2_UPD_OVF_BRK_IRQHandler$0$0 ==.
      0084E7 80               [11]  253 	iret
                           00000E   254 	Sstm8s_it$TIM2_UPD_OVF_BRK_IRQHandler$69 ==.
                           00000E   255 	Sstm8s_it$TIM2_CAP_COM_IRQHandler$70 ==.
                                    256 ;	app/src/stm8s_it.c: 282: INTERRUPT_HANDLER(TIM2_CAP_COM_IRQHandler, 14)
                                    257 ;	-----------------------------------------
                                    258 ;	 function TIM2_CAP_COM_IRQHandler
                                    259 ;	-----------------------------------------
      0084E8                        260 _TIM2_CAP_COM_IRQHandler:
                           00000E   261 	Sstm8s_it$TIM2_CAP_COM_IRQHandler$71 ==.
                           00000E   262 	Sstm8s_it$TIM2_CAP_COM_IRQHandler$72 ==.
                                    263 ;	app/src/stm8s_it.c: 287: }
                           00000E   264 	Sstm8s_it$TIM2_CAP_COM_IRQHandler$73 ==.
                           00000E   265 	XG$TIM2_CAP_COM_IRQHandler$0$0 ==.
      0084E8 80               [11]  266 	iret
                           00000F   267 	Sstm8s_it$TIM2_CAP_COM_IRQHandler$74 ==.
                           00000F   268 	Sstm8s_it$TIM3_UPD_OVF_BRK_IRQHandler$75 ==.
                                    269 ;	app/src/stm8s_it.c: 298: INTERRUPT_HANDLER(TIM3_UPD_OVF_BRK_IRQHandler, 15)
                                    270 ;	-----------------------------------------
                                    271 ;	 function TIM3_UPD_OVF_BRK_IRQHandler
                                    272 ;	-----------------------------------------
      0084E9                        273 _TIM3_UPD_OVF_BRK_IRQHandler:
                           00000F   274 	Sstm8s_it$TIM3_UPD_OVF_BRK_IRQHandler$76 ==.
                           00000F   275 	Sstm8s_it$TIM3_UPD_OVF_BRK_IRQHandler$77 ==.
                                    276 ;	app/src/stm8s_it.c: 303: }
                           00000F   277 	Sstm8s_it$TIM3_UPD_OVF_BRK_IRQHandler$78 ==.
                           00000F   278 	XG$TIM3_UPD_OVF_BRK_IRQHandler$0$0 ==.
      0084E9 80               [11]  279 	iret
                           000010   280 	Sstm8s_it$TIM3_UPD_OVF_BRK_IRQHandler$79 ==.
                           000010   281 	Sstm8s_it$TIM3_CAP_COM_IRQHandler$80 ==.
                                    282 ;	app/src/stm8s_it.c: 310: INTERRUPT_HANDLER(TIM3_CAP_COM_IRQHandler, 16)
                                    283 ;	-----------------------------------------
                                    284 ;	 function TIM3_CAP_COM_IRQHandler
                                    285 ;	-----------------------------------------
      0084EA                        286 _TIM3_CAP_COM_IRQHandler:
                           000010   287 	Sstm8s_it$TIM3_CAP_COM_IRQHandler$81 ==.
                           000010   288 	Sstm8s_it$TIM3_CAP_COM_IRQHandler$82 ==.
                                    289 ;	app/src/stm8s_it.c: 315: }
                           000010   290 	Sstm8s_it$TIM3_CAP_COM_IRQHandler$83 ==.
                           000010   291 	XG$TIM3_CAP_COM_IRQHandler$0$0 ==.
      0084EA 80               [11]  292 	iret
                           000011   293 	Sstm8s_it$TIM3_CAP_COM_IRQHandler$84 ==.
                           000011   294 	Sstm8s_it$UART1_TX_IRQHandler$85 ==.
                                    295 ;	app/src/stm8s_it.c: 326: INTERRUPT_HANDLER(UART1_TX_IRQHandler, 17)
                                    296 ;	-----------------------------------------
                                    297 ;	 function UART1_TX_IRQHandler
                                    298 ;	-----------------------------------------
      0084EB                        299 _UART1_TX_IRQHandler:
                           000011   300 	Sstm8s_it$UART1_TX_IRQHandler$86 ==.
                           000011   301 	Sstm8s_it$UART1_TX_IRQHandler$87 ==.
                                    302 ;	app/src/stm8s_it.c: 331: }
                           000011   303 	Sstm8s_it$UART1_TX_IRQHandler$88 ==.
                           000011   304 	XG$UART1_TX_IRQHandler$0$0 ==.
      0084EB 80               [11]  305 	iret
                           000012   306 	Sstm8s_it$UART1_TX_IRQHandler$89 ==.
                           000012   307 	Sstm8s_it$UART1_RX_IRQHandler$90 ==.
                                    308 ;	app/src/stm8s_it.c: 338: INTERRUPT_HANDLER(UART1_RX_IRQHandler, 18)
                                    309 ;	-----------------------------------------
                                    310 ;	 function UART1_RX_IRQHandler
                                    311 ;	-----------------------------------------
      0084EC                        312 _UART1_RX_IRQHandler:
                           000012   313 	Sstm8s_it$UART1_RX_IRQHandler$91 ==.
                           000012   314 	Sstm8s_it$UART1_RX_IRQHandler$92 ==.
                                    315 ;	app/src/stm8s_it.c: 343: }
                           000012   316 	Sstm8s_it$UART1_RX_IRQHandler$93 ==.
                           000012   317 	XG$UART1_RX_IRQHandler$0$0 ==.
      0084EC 80               [11]  318 	iret
                           000013   319 	Sstm8s_it$UART1_RX_IRQHandler$94 ==.
                           000013   320 	Sstm8s_it$I2C_IRQHandler$95 ==.
                                    321 ;	app/src/stm8s_it.c: 351: INTERRUPT_HANDLER(I2C_IRQHandler, 19)
                                    322 ;	-----------------------------------------
                                    323 ;	 function I2C_IRQHandler
                                    324 ;	-----------------------------------------
      0084ED                        325 _I2C_IRQHandler:
                           000013   326 	Sstm8s_it$I2C_IRQHandler$96 ==.
                           000013   327 	Sstm8s_it$I2C_IRQHandler$97 ==.
                                    328 ;	app/src/stm8s_it.c: 356: }
                           000013   329 	Sstm8s_it$I2C_IRQHandler$98 ==.
                           000013   330 	XG$I2C_IRQHandler$0$0 ==.
      0084ED 80               [11]  331 	iret
                           000014   332 	Sstm8s_it$I2C_IRQHandler$99 ==.
                           000014   333 	Sstm8s_it$UART3_TX_IRQHandler$100 ==.
                                    334 ;	app/src/stm8s_it.c: 392: INTERRUPT_HANDLER(UART3_TX_IRQHandler, 20)
                                    335 ;	-----------------------------------------
                                    336 ;	 function UART3_TX_IRQHandler
                                    337 ;	-----------------------------------------
      0084EE                        338 _UART3_TX_IRQHandler:
                           000014   339 	Sstm8s_it$UART3_TX_IRQHandler$101 ==.
                           000014   340 	Sstm8s_it$UART3_TX_IRQHandler$102 ==.
                                    341 ;	app/src/stm8s_it.c: 397: }
                           000014   342 	Sstm8s_it$UART3_TX_IRQHandler$103 ==.
                           000014   343 	XG$UART3_TX_IRQHandler$0$0 ==.
      0084EE 80               [11]  344 	iret
                           000015   345 	Sstm8s_it$UART3_TX_IRQHandler$104 ==.
                           000015   346 	Sstm8s_it$UART3_RX_IRQHandler$105 ==.
                                    347 ;	app/src/stm8s_it.c: 404: INTERRUPT_HANDLER(UART3_RX_IRQHandler, 21)
                                    348 ;	-----------------------------------------
                                    349 ;	 function UART3_RX_IRQHandler
                                    350 ;	-----------------------------------------
      0084EF                        351 _UART3_RX_IRQHandler:
                           000015   352 	Sstm8s_it$UART3_RX_IRQHandler$106 ==.
                           000015   353 	Sstm8s_it$UART3_RX_IRQHandler$107 ==.
                                    354 ;	app/src/stm8s_it.c: 409: }
                           000015   355 	Sstm8s_it$UART3_RX_IRQHandler$108 ==.
                           000015   356 	XG$UART3_RX_IRQHandler$0$0 ==.
      0084EF 80               [11]  357 	iret
                           000016   358 	Sstm8s_it$UART3_RX_IRQHandler$109 ==.
                           000016   359 	Sstm8s_it$ADC2_IRQHandler$110 ==.
                                    360 ;	app/src/stm8s_it.c: 419: INTERRUPT_HANDLER(ADC2_IRQHandler, 22)
                                    361 ;	-----------------------------------------
                                    362 ;	 function ADC2_IRQHandler
                                    363 ;	-----------------------------------------
      0084F0                        364 _ADC2_IRQHandler:
                           000016   365 	Sstm8s_it$ADC2_IRQHandler$111 ==.
                           000016   366 	Sstm8s_it$ADC2_IRQHandler$112 ==.
                                    367 ;	app/src/stm8s_it.c: 424: return;
                           000016   368 	Sstm8s_it$ADC2_IRQHandler$113 ==.
                                    369 ;	app/src/stm8s_it.c: 425: }
                           000016   370 	Sstm8s_it$ADC2_IRQHandler$114 ==.
                           000016   371 	XG$ADC2_IRQHandler$0$0 ==.
      0084F0 80               [11]  372 	iret
                           000017   373 	Sstm8s_it$ADC2_IRQHandler$115 ==.
                           000017   374 	Sstm8s_it$TIM4_UPD_OVF_IRQHandler$116 ==.
                                    375 ;	app/src/stm8s_it.c: 462: INTERRUPT_HANDLER(TIM4_UPD_OVF_IRQHandler, 23)
                                    376 ;	-----------------------------------------
                                    377 ;	 function TIM4_UPD_OVF_IRQHandler
                                    378 ;	-----------------------------------------
      0084F1                        379 _TIM4_UPD_OVF_IRQHandler:
                           000017   380 	Sstm8s_it$TIM4_UPD_OVF_IRQHandler$117 ==.
                           000017   381 	Sstm8s_it$TIM4_UPD_OVF_IRQHandler$118 ==.
                                    382 ;	app/src/stm8s_it.c: 467: }
                           000017   383 	Sstm8s_it$TIM4_UPD_OVF_IRQHandler$119 ==.
                           000017   384 	XG$TIM4_UPD_OVF_IRQHandler$0$0 ==.
      0084F1 80               [11]  385 	iret
                           000018   386 	Sstm8s_it$TIM4_UPD_OVF_IRQHandler$120 ==.
                           000018   387 	Sstm8s_it$EEPROM_EEC_IRQHandler$121 ==.
                                    388 ;	app/src/stm8s_it.c: 475: INTERRUPT_HANDLER(EEPROM_EEC_IRQHandler, 24)
                                    389 ;	-----------------------------------------
                                    390 ;	 function EEPROM_EEC_IRQHandler
                                    391 ;	-----------------------------------------
      0084F2                        392 _EEPROM_EEC_IRQHandler:
                           000018   393 	Sstm8s_it$EEPROM_EEC_IRQHandler$122 ==.
                           000018   394 	Sstm8s_it$EEPROM_EEC_IRQHandler$123 ==.
                                    395 ;	app/src/stm8s_it.c: 480: }
                           000018   396 	Sstm8s_it$EEPROM_EEC_IRQHandler$124 ==.
                           000018   397 	XG$EEPROM_EEC_IRQHandler$0$0 ==.
      0084F2 80               [11]  398 	iret
                           000019   399 	Sstm8s_it$EEPROM_EEC_IRQHandler$125 ==.
                                    400 	.area CODE
                                    401 	.area CONST
                                    402 	.area INITIALIZER
                                    403 	.area CABS (ABS)
                                    404 
                                    405 	.area .debug_line (NOLOAD)
      0004B4 00 00 02 BD            406 	.dw	0,Ldebug_line_end-Ldebug_line_start
      0004B8                        407 Ldebug_line_start:
      0004B8 00 02                  408 	.dw	2
      0004BA 00 00 00 73            409 	.dw	0,Ldebug_line_stmt-6-Ldebug_line_start
      0004BE 01                     410 	.db	1
      0004BF 01                     411 	.db	1
      0004C0 FB                     412 	.db	-5
      0004C1 0F                     413 	.db	15
      0004C2 0A                     414 	.db	10
      0004C3 00                     415 	.db	0
      0004C4 01                     416 	.db	1
      0004C5 01                     417 	.db	1
      0004C6 01                     418 	.db	1
      0004C7 01                     419 	.db	1
      0004C8 00                     420 	.db	0
      0004C9 00                     421 	.db	0
      0004CA 00                     422 	.db	0
      0004CB 01                     423 	.db	1
      0004CC 43 3A 5C 50 72 6F 67   424 	.ascii "C:\Program Files\SDCC\bin\..\include\stm8"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
             5C 73 74 6D 38
      0004F4 00                     425 	.db	0
      0004F5 43 3A 5C 50 72 6F 67   426 	.ascii "C:\Program Files\SDCC\bin\..\include"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
      000518 00                     427 	.db	0
      000519 00                     428 	.db	0
      00051A 61 70 70 2F 73 72 63   429 	.ascii "app/src/stm8s_it.c"
             2F 73 74 6D 38 73 5F
             69 74 2E 63
      00052C 00                     430 	.db	0
      00052D 00                     431 	.uleb128	0
      00052E 00                     432 	.uleb128	0
      00052F 00                     433 	.uleb128	0
      000530 00                     434 	.db	0
      000531                        435 Ldebug_line_stmt:
      000531 00                     436 	.db	0
      000532 05                     437 	.uleb128	5
      000533 02                     438 	.db	2
      000534 00 00 84 DA            439 	.dw	0,(Sstm8s_it$TRAP_IRQHandler$0)
      000538 03                     440 	.db	3
      000539 3D                     441 	.sleb128	61
      00053A 01                     442 	.db	1
      00053B 09                     443 	.db	9
      00053C 00 00                  444 	.dw	Sstm8s_it$TRAP_IRQHandler$2-Sstm8s_it$TRAP_IRQHandler$0
      00053E 03                     445 	.db	3
      00053F 05                     446 	.sleb128	5
      000540 01                     447 	.db	1
      000541 09                     448 	.db	9
      000542 00 01                  449 	.dw	1+Sstm8s_it$TRAP_IRQHandler$3-Sstm8s_it$TRAP_IRQHandler$2
      000544 00                     450 	.db	0
      000545 01                     451 	.uleb128	1
      000546 01                     452 	.db	1
      000547 00                     453 	.db	0
      000548 05                     454 	.uleb128	5
      000549 02                     455 	.db	2
      00054A 00 00 84 DB            456 	.dw	0,(Sstm8s_it$TLI_IRQHandler$5)
      00054E 03                     457 	.db	3
      00054F C9 00                  458 	.sleb128	73
      000551 01                     459 	.db	1
      000552 09                     460 	.db	9
      000553 00 00                  461 	.dw	Sstm8s_it$TLI_IRQHandler$7-Sstm8s_it$TLI_IRQHandler$5
      000555 03                     462 	.db	3
      000556 05                     463 	.sleb128	5
      000557 01                     464 	.db	1
      000558 09                     465 	.db	9
      000559 00 01                  466 	.dw	1+Sstm8s_it$TLI_IRQHandler$8-Sstm8s_it$TLI_IRQHandler$7
      00055B 00                     467 	.db	0
      00055C 01                     468 	.uleb128	1
      00055D 01                     469 	.db	1
      00055E 00                     470 	.db	0
      00055F 05                     471 	.uleb128	5
      000560 02                     472 	.db	2
      000561 00 00 84 DC            473 	.dw	0,(Sstm8s_it$AWU_IRQHandler$10)
      000565 03                     474 	.db	3
      000566 D5 00                  475 	.sleb128	85
      000568 01                     476 	.db	1
      000569 09                     477 	.db	9
      00056A 00 00                  478 	.dw	Sstm8s_it$AWU_IRQHandler$12-Sstm8s_it$AWU_IRQHandler$10
      00056C 03                     479 	.db	3
      00056D 05                     480 	.sleb128	5
      00056E 01                     481 	.db	1
      00056F 09                     482 	.db	9
      000570 00 01                  483 	.dw	1+Sstm8s_it$AWU_IRQHandler$13-Sstm8s_it$AWU_IRQHandler$12
      000572 00                     484 	.db	0
      000573 01                     485 	.uleb128	1
      000574 01                     486 	.db	1
      000575 00                     487 	.db	0
      000576 05                     488 	.uleb128	5
      000577 02                     489 	.db	2
      000578 00 00 84 DD            490 	.dw	0,(Sstm8s_it$CLK_IRQHandler$15)
      00057C 03                     491 	.db	3
      00057D E1 00                  492 	.sleb128	97
      00057F 01                     493 	.db	1
      000580 09                     494 	.db	9
      000581 00 00                  495 	.dw	Sstm8s_it$CLK_IRQHandler$17-Sstm8s_it$CLK_IRQHandler$15
      000583 03                     496 	.db	3
      000584 05                     497 	.sleb128	5
      000585 01                     498 	.db	1
      000586 09                     499 	.db	9
      000587 00 01                  500 	.dw	1+Sstm8s_it$CLK_IRQHandler$18-Sstm8s_it$CLK_IRQHandler$17
      000589 00                     501 	.db	0
      00058A 01                     502 	.uleb128	1
      00058B 01                     503 	.db	1
      00058C 00                     504 	.db	0
      00058D 05                     505 	.uleb128	5
      00058E 02                     506 	.db	2
      00058F 00 00 84 DE            507 	.dw	0,(Sstm8s_it$EXTI_PORTA_IRQHandler$20)
      000593 03                     508 	.db	3
      000594 ED 00                  509 	.sleb128	109
      000596 01                     510 	.db	1
      000597 09                     511 	.db	9
      000598 00 00                  512 	.dw	Sstm8s_it$EXTI_PORTA_IRQHandler$22-Sstm8s_it$EXTI_PORTA_IRQHandler$20
      00059A 03                     513 	.db	3
      00059B 05                     514 	.sleb128	5
      00059C 01                     515 	.db	1
      00059D 09                     516 	.db	9
      00059E 00 01                  517 	.dw	1+Sstm8s_it$EXTI_PORTA_IRQHandler$23-Sstm8s_it$EXTI_PORTA_IRQHandler$22
      0005A0 00                     518 	.db	0
      0005A1 01                     519 	.uleb128	1
      0005A2 01                     520 	.db	1
      0005A3 00                     521 	.db	0
      0005A4 05                     522 	.uleb128	5
      0005A5 02                     523 	.db	2
      0005A6 00 00 84 DF            524 	.dw	0,(Sstm8s_it$EXTI_PORTB_IRQHandler$25)
      0005AA 03                     525 	.db	3
      0005AB F9 00                  526 	.sleb128	121
      0005AD 01                     527 	.db	1
      0005AE 09                     528 	.db	9
      0005AF 00 00                  529 	.dw	Sstm8s_it$EXTI_PORTB_IRQHandler$27-Sstm8s_it$EXTI_PORTB_IRQHandler$25
      0005B1 03                     530 	.db	3
      0005B2 05                     531 	.sleb128	5
      0005B3 01                     532 	.db	1
      0005B4 09                     533 	.db	9
      0005B5 00 01                  534 	.dw	1+Sstm8s_it$EXTI_PORTB_IRQHandler$28-Sstm8s_it$EXTI_PORTB_IRQHandler$27
      0005B7 00                     535 	.db	0
      0005B8 01                     536 	.uleb128	1
      0005B9 01                     537 	.db	1
      0005BA 00                     538 	.db	0
      0005BB 05                     539 	.uleb128	5
      0005BC 02                     540 	.db	2
      0005BD 00 00 84 E0            541 	.dw	0,(Sstm8s_it$EXTI_PORTC_IRQHandler$30)
      0005C1 03                     542 	.db	3
      0005C2 85 01                  543 	.sleb128	133
      0005C4 01                     544 	.db	1
      0005C5 09                     545 	.db	9
      0005C6 00 00                  546 	.dw	Sstm8s_it$EXTI_PORTC_IRQHandler$32-Sstm8s_it$EXTI_PORTC_IRQHandler$30
      0005C8 03                     547 	.db	3
      0005C9 05                     548 	.sleb128	5
      0005CA 01                     549 	.db	1
      0005CB 09                     550 	.db	9
      0005CC 00 01                  551 	.dw	1+Sstm8s_it$EXTI_PORTC_IRQHandler$33-Sstm8s_it$EXTI_PORTC_IRQHandler$32
      0005CE 00                     552 	.db	0
      0005CF 01                     553 	.uleb128	1
      0005D0 01                     554 	.db	1
      0005D1 00                     555 	.db	0
      0005D2 05                     556 	.uleb128	5
      0005D3 02                     557 	.db	2
      0005D4 00 00 84 E1            558 	.dw	0,(Sstm8s_it$EXTI_PORTD_IRQHandler$35)
      0005D8 03                     559 	.db	3
      0005D9 91 01                  560 	.sleb128	145
      0005DB 01                     561 	.db	1
      0005DC 09                     562 	.db	9
      0005DD 00 00                  563 	.dw	Sstm8s_it$EXTI_PORTD_IRQHandler$37-Sstm8s_it$EXTI_PORTD_IRQHandler$35
      0005DF 03                     564 	.db	3
      0005E0 05                     565 	.sleb128	5
      0005E1 01                     566 	.db	1
      0005E2 09                     567 	.db	9
      0005E3 00 01                  568 	.dw	1+Sstm8s_it$EXTI_PORTD_IRQHandler$38-Sstm8s_it$EXTI_PORTD_IRQHandler$37
      0005E5 00                     569 	.db	0
      0005E6 01                     570 	.uleb128	1
      0005E7 01                     571 	.db	1
      0005E8 00                     572 	.db	0
      0005E9 05                     573 	.uleb128	5
      0005EA 02                     574 	.db	2
      0005EB 00 00 84 E2            575 	.dw	0,(Sstm8s_it$CAN_RX_IRQHandler$40)
      0005EF 03                     576 	.db	3
      0005F0 B4 01                  577 	.sleb128	180
      0005F2 01                     578 	.db	1
      0005F3 09                     579 	.db	9
      0005F4 00 00                  580 	.dw	Sstm8s_it$CAN_RX_IRQHandler$42-Sstm8s_it$CAN_RX_IRQHandler$40
      0005F6 03                     581 	.db	3
      0005F7 05                     582 	.sleb128	5
      0005F8 01                     583 	.db	1
      0005F9 09                     584 	.db	9
      0005FA 00 01                  585 	.dw	1+Sstm8s_it$CAN_RX_IRQHandler$43-Sstm8s_it$CAN_RX_IRQHandler$42
      0005FC 00                     586 	.db	0
      0005FD 01                     587 	.uleb128	1
      0005FE 01                     588 	.db	1
      0005FF 00                     589 	.db	0
      000600 05                     590 	.uleb128	5
      000601 02                     591 	.db	2
      000602 00 00 84 E3            592 	.dw	0,(Sstm8s_it$CAN_TX_IRQHandler$45)
      000606 03                     593 	.db	3
      000607 C0 01                  594 	.sleb128	192
      000609 01                     595 	.db	1
      00060A 09                     596 	.db	9
      00060B 00 00                  597 	.dw	Sstm8s_it$CAN_TX_IRQHandler$47-Sstm8s_it$CAN_TX_IRQHandler$45
      00060D 03                     598 	.db	3
      00060E 05                     599 	.sleb128	5
      00060F 01                     600 	.db	1
      000610 09                     601 	.db	9
      000611 00 01                  602 	.dw	1+Sstm8s_it$CAN_TX_IRQHandler$48-Sstm8s_it$CAN_TX_IRQHandler$47
      000613 00                     603 	.db	0
      000614 01                     604 	.uleb128	1
      000615 01                     605 	.db	1
      000616 00                     606 	.db	0
      000617 05                     607 	.uleb128	5
      000618 02                     608 	.db	2
      000619 00 00 84 E4            609 	.dw	0,(Sstm8s_it$SPI_IRQHandler$50)
      00061D 03                     610 	.db	3
      00061E CD 01                  611 	.sleb128	205
      000620 01                     612 	.db	1
      000621 09                     613 	.db	9
      000622 00 00                  614 	.dw	Sstm8s_it$SPI_IRQHandler$52-Sstm8s_it$SPI_IRQHandler$50
      000624 03                     615 	.db	3
      000625 05                     616 	.sleb128	5
      000626 01                     617 	.db	1
      000627 09                     618 	.db	9
      000628 00 01                  619 	.dw	1+Sstm8s_it$SPI_IRQHandler$53-Sstm8s_it$SPI_IRQHandler$52
      00062A 00                     620 	.db	0
      00062B 01                     621 	.uleb128	1
      00062C 01                     622 	.db	1
      00062D 00                     623 	.db	0
      00062E 05                     624 	.uleb128	5
      00062F 02                     625 	.db	2
      000630 00 00 84 E5            626 	.dw	0,(Sstm8s_it$TIM1_UPD_OVF_TRG_BRK_IRQHandler$55)
      000634 03                     627 	.db	3
      000635 D9 01                  628 	.sleb128	217
      000637 01                     629 	.db	1
      000638 09                     630 	.db	9
      000639 00 00                  631 	.dw	Sstm8s_it$TIM1_UPD_OVF_TRG_BRK_IRQHandler$57-Sstm8s_it$TIM1_UPD_OVF_TRG_BRK_IRQHandler$55
      00063B 03                     632 	.db	3
      00063C 05                     633 	.sleb128	5
      00063D 01                     634 	.db	1
      00063E 09                     635 	.db	9
      00063F 00 01                  636 	.dw	1+Sstm8s_it$TIM1_UPD_OVF_TRG_BRK_IRQHandler$58-Sstm8s_it$TIM1_UPD_OVF_TRG_BRK_IRQHandler$57
      000641 00                     637 	.db	0
      000642 01                     638 	.uleb128	1
      000643 01                     639 	.db	1
      000644 00                     640 	.db	0
      000645 05                     641 	.uleb128	5
      000646 02                     642 	.db	2
      000647 00 00 84 E6            643 	.dw	0,(Sstm8s_it$TIM1_CAP_COM_IRQHandler$60)
      00064B 03                     644 	.db	3
      00064C E5 01                  645 	.sleb128	229
      00064E 01                     646 	.db	1
      00064F 09                     647 	.db	9
      000650 00 00                  648 	.dw	Sstm8s_it$TIM1_CAP_COM_IRQHandler$62-Sstm8s_it$TIM1_CAP_COM_IRQHandler$60
      000652 03                     649 	.db	3
      000653 05                     650 	.sleb128	5
      000654 01                     651 	.db	1
      000655 09                     652 	.db	9
      000656 00 01                  653 	.dw	1+Sstm8s_it$TIM1_CAP_COM_IRQHandler$63-Sstm8s_it$TIM1_CAP_COM_IRQHandler$62
      000658 00                     654 	.db	0
      000659 01                     655 	.uleb128	1
      00065A 01                     656 	.db	1
      00065B 00                     657 	.db	0
      00065C 05                     658 	.uleb128	5
      00065D 02                     659 	.db	2
      00065E 00 00 84 E7            660 	.dw	0,(Sstm8s_it$TIM2_UPD_OVF_BRK_IRQHandler$65)
      000662 03                     661 	.db	3
      000663 8D 02                  662 	.sleb128	269
      000665 01                     663 	.db	1
      000666 09                     664 	.db	9
      000667 00 00                  665 	.dw	Sstm8s_it$TIM2_UPD_OVF_BRK_IRQHandler$67-Sstm8s_it$TIM2_UPD_OVF_BRK_IRQHandler$65
      000669 03                     666 	.db	3
      00066A 05                     667 	.sleb128	5
      00066B 01                     668 	.db	1
      00066C 09                     669 	.db	9
      00066D 00 01                  670 	.dw	1+Sstm8s_it$TIM2_UPD_OVF_BRK_IRQHandler$68-Sstm8s_it$TIM2_UPD_OVF_BRK_IRQHandler$67
      00066F 00                     671 	.db	0
      000670 01                     672 	.uleb128	1
      000671 01                     673 	.db	1
      000672 00                     674 	.db	0
      000673 05                     675 	.uleb128	5
      000674 02                     676 	.db	2
      000675 00 00 84 E8            677 	.dw	0,(Sstm8s_it$TIM2_CAP_COM_IRQHandler$70)
      000679 03                     678 	.db	3
      00067A 99 02                  679 	.sleb128	281
      00067C 01                     680 	.db	1
      00067D 09                     681 	.db	9
      00067E 00 00                  682 	.dw	Sstm8s_it$TIM2_CAP_COM_IRQHandler$72-Sstm8s_it$TIM2_CAP_COM_IRQHandler$70
      000680 03                     683 	.db	3
      000681 05                     684 	.sleb128	5
      000682 01                     685 	.db	1
      000683 09                     686 	.db	9
      000684 00 01                  687 	.dw	1+Sstm8s_it$TIM2_CAP_COM_IRQHandler$73-Sstm8s_it$TIM2_CAP_COM_IRQHandler$72
      000686 00                     688 	.db	0
      000687 01                     689 	.uleb128	1
      000688 01                     690 	.db	1
      000689 00                     691 	.db	0
      00068A 05                     692 	.uleb128	5
      00068B 02                     693 	.db	2
      00068C 00 00 84 E9            694 	.dw	0,(Sstm8s_it$TIM3_UPD_OVF_BRK_IRQHandler$75)
      000690 03                     695 	.db	3
      000691 A9 02                  696 	.sleb128	297
      000693 01                     697 	.db	1
      000694 09                     698 	.db	9
      000695 00 00                  699 	.dw	Sstm8s_it$TIM3_UPD_OVF_BRK_IRQHandler$77-Sstm8s_it$TIM3_UPD_OVF_BRK_IRQHandler$75
      000697 03                     700 	.db	3
      000698 05                     701 	.sleb128	5
      000699 01                     702 	.db	1
      00069A 09                     703 	.db	9
      00069B 00 01                  704 	.dw	1+Sstm8s_it$TIM3_UPD_OVF_BRK_IRQHandler$78-Sstm8s_it$TIM3_UPD_OVF_BRK_IRQHandler$77
      00069D 00                     705 	.db	0
      00069E 01                     706 	.uleb128	1
      00069F 01                     707 	.db	1
      0006A0 00                     708 	.db	0
      0006A1 05                     709 	.uleb128	5
      0006A2 02                     710 	.db	2
      0006A3 00 00 84 EA            711 	.dw	0,(Sstm8s_it$TIM3_CAP_COM_IRQHandler$80)
      0006A7 03                     712 	.db	3
      0006A8 B5 02                  713 	.sleb128	309
      0006AA 01                     714 	.db	1
      0006AB 09                     715 	.db	9
      0006AC 00 00                  716 	.dw	Sstm8s_it$TIM3_CAP_COM_IRQHandler$82-Sstm8s_it$TIM3_CAP_COM_IRQHandler$80
      0006AE 03                     717 	.db	3
      0006AF 05                     718 	.sleb128	5
      0006B0 01                     719 	.db	1
      0006B1 09                     720 	.db	9
      0006B2 00 01                  721 	.dw	1+Sstm8s_it$TIM3_CAP_COM_IRQHandler$83-Sstm8s_it$TIM3_CAP_COM_IRQHandler$82
      0006B4 00                     722 	.db	0
      0006B5 01                     723 	.uleb128	1
      0006B6 01                     724 	.db	1
      0006B7 00                     725 	.db	0
      0006B8 05                     726 	.uleb128	5
      0006B9 02                     727 	.db	2
      0006BA 00 00 84 EB            728 	.dw	0,(Sstm8s_it$UART1_TX_IRQHandler$85)
      0006BE 03                     729 	.db	3
      0006BF C5 02                  730 	.sleb128	325
      0006C1 01                     731 	.db	1
      0006C2 09                     732 	.db	9
      0006C3 00 00                  733 	.dw	Sstm8s_it$UART1_TX_IRQHandler$87-Sstm8s_it$UART1_TX_IRQHandler$85
      0006C5 03                     734 	.db	3
      0006C6 05                     735 	.sleb128	5
      0006C7 01                     736 	.db	1
      0006C8 09                     737 	.db	9
      0006C9 00 01                  738 	.dw	1+Sstm8s_it$UART1_TX_IRQHandler$88-Sstm8s_it$UART1_TX_IRQHandler$87
      0006CB 00                     739 	.db	0
      0006CC 01                     740 	.uleb128	1
      0006CD 01                     741 	.db	1
      0006CE 00                     742 	.db	0
      0006CF 05                     743 	.uleb128	5
      0006D0 02                     744 	.db	2
      0006D1 00 00 84 EC            745 	.dw	0,(Sstm8s_it$UART1_RX_IRQHandler$90)
      0006D5 03                     746 	.db	3
      0006D6 D1 02                  747 	.sleb128	337
      0006D8 01                     748 	.db	1
      0006D9 09                     749 	.db	9
      0006DA 00 00                  750 	.dw	Sstm8s_it$UART1_RX_IRQHandler$92-Sstm8s_it$UART1_RX_IRQHandler$90
      0006DC 03                     751 	.db	3
      0006DD 05                     752 	.sleb128	5
      0006DE 01                     753 	.db	1
      0006DF 09                     754 	.db	9
      0006E0 00 01                  755 	.dw	1+Sstm8s_it$UART1_RX_IRQHandler$93-Sstm8s_it$UART1_RX_IRQHandler$92
      0006E2 00                     756 	.db	0
      0006E3 01                     757 	.uleb128	1
      0006E4 01                     758 	.db	1
      0006E5 00                     759 	.db	0
      0006E6 05                     760 	.uleb128	5
      0006E7 02                     761 	.db	2
      0006E8 00 00 84 ED            762 	.dw	0,(Sstm8s_it$I2C_IRQHandler$95)
      0006EC 03                     763 	.db	3
      0006ED DE 02                  764 	.sleb128	350
      0006EF 01                     765 	.db	1
      0006F0 09                     766 	.db	9
      0006F1 00 00                  767 	.dw	Sstm8s_it$I2C_IRQHandler$97-Sstm8s_it$I2C_IRQHandler$95
      0006F3 03                     768 	.db	3
      0006F4 05                     769 	.sleb128	5
      0006F5 01                     770 	.db	1
      0006F6 09                     771 	.db	9
      0006F7 00 01                  772 	.dw	1+Sstm8s_it$I2C_IRQHandler$98-Sstm8s_it$I2C_IRQHandler$97
      0006F9 00                     773 	.db	0
      0006FA 01                     774 	.uleb128	1
      0006FB 01                     775 	.db	1
      0006FC 00                     776 	.db	0
      0006FD 05                     777 	.uleb128	5
      0006FE 02                     778 	.db	2
      0006FF 00 00 84 EE            779 	.dw	0,(Sstm8s_it$UART3_TX_IRQHandler$100)
      000703 03                     780 	.db	3
      000704 87 03                  781 	.sleb128	391
      000706 01                     782 	.db	1
      000707 09                     783 	.db	9
      000708 00 00                  784 	.dw	Sstm8s_it$UART3_TX_IRQHandler$102-Sstm8s_it$UART3_TX_IRQHandler$100
      00070A 03                     785 	.db	3
      00070B 05                     786 	.sleb128	5
      00070C 01                     787 	.db	1
      00070D 09                     788 	.db	9
      00070E 00 01                  789 	.dw	1+Sstm8s_it$UART3_TX_IRQHandler$103-Sstm8s_it$UART3_TX_IRQHandler$102
      000710 00                     790 	.db	0
      000711 01                     791 	.uleb128	1
      000712 01                     792 	.db	1
      000713 00                     793 	.db	0
      000714 05                     794 	.uleb128	5
      000715 02                     795 	.db	2
      000716 00 00 84 EF            796 	.dw	0,(Sstm8s_it$UART3_RX_IRQHandler$105)
      00071A 03                     797 	.db	3
      00071B 93 03                  798 	.sleb128	403
      00071D 01                     799 	.db	1
      00071E 09                     800 	.db	9
      00071F 00 00                  801 	.dw	Sstm8s_it$UART3_RX_IRQHandler$107-Sstm8s_it$UART3_RX_IRQHandler$105
      000721 03                     802 	.db	3
      000722 05                     803 	.sleb128	5
      000723 01                     804 	.db	1
      000724 09                     805 	.db	9
      000725 00 01                  806 	.dw	1+Sstm8s_it$UART3_RX_IRQHandler$108-Sstm8s_it$UART3_RX_IRQHandler$107
      000727 00                     807 	.db	0
      000728 01                     808 	.uleb128	1
      000729 01                     809 	.db	1
      00072A 00                     810 	.db	0
      00072B 05                     811 	.uleb128	5
      00072C 02                     812 	.db	2
      00072D 00 00 84 F0            813 	.dw	0,(Sstm8s_it$ADC2_IRQHandler$110)
      000731 03                     814 	.db	3
      000732 A2 03                  815 	.sleb128	418
      000734 01                     816 	.db	1
      000735 09                     817 	.db	9
      000736 00 00                  818 	.dw	Sstm8s_it$ADC2_IRQHandler$112-Sstm8s_it$ADC2_IRQHandler$110
      000738 03                     819 	.db	3
      000739 05                     820 	.sleb128	5
      00073A 01                     821 	.db	1
      00073B 09                     822 	.db	9
      00073C 00 00                  823 	.dw	Sstm8s_it$ADC2_IRQHandler$113-Sstm8s_it$ADC2_IRQHandler$112
      00073E 03                     824 	.db	3
      00073F 01                     825 	.sleb128	1
      000740 01                     826 	.db	1
      000741 09                     827 	.db	9
      000742 00 01                  828 	.dw	1+Sstm8s_it$ADC2_IRQHandler$114-Sstm8s_it$ADC2_IRQHandler$113
      000744 00                     829 	.db	0
      000745 01                     830 	.uleb128	1
      000746 01                     831 	.db	1
      000747 00                     832 	.db	0
      000748 05                     833 	.uleb128	5
      000749 02                     834 	.db	2
      00074A 00 00 84 F1            835 	.dw	0,(Sstm8s_it$TIM4_UPD_OVF_IRQHandler$116)
      00074E 03                     836 	.db	3
      00074F CD 03                  837 	.sleb128	461
      000751 01                     838 	.db	1
      000752 09                     839 	.db	9
      000753 00 00                  840 	.dw	Sstm8s_it$TIM4_UPD_OVF_IRQHandler$118-Sstm8s_it$TIM4_UPD_OVF_IRQHandler$116
      000755 03                     841 	.db	3
      000756 05                     842 	.sleb128	5
      000757 01                     843 	.db	1
      000758 09                     844 	.db	9
      000759 00 01                  845 	.dw	1+Sstm8s_it$TIM4_UPD_OVF_IRQHandler$119-Sstm8s_it$TIM4_UPD_OVF_IRQHandler$118
      00075B 00                     846 	.db	0
      00075C 01                     847 	.uleb128	1
      00075D 01                     848 	.db	1
      00075E 00                     849 	.db	0
      00075F 05                     850 	.uleb128	5
      000760 02                     851 	.db	2
      000761 00 00 84 F2            852 	.dw	0,(Sstm8s_it$EEPROM_EEC_IRQHandler$121)
      000765 03                     853 	.db	3
      000766 DA 03                  854 	.sleb128	474
      000768 01                     855 	.db	1
      000769 09                     856 	.db	9
      00076A 00 00                  857 	.dw	Sstm8s_it$EEPROM_EEC_IRQHandler$123-Sstm8s_it$EEPROM_EEC_IRQHandler$121
      00076C 03                     858 	.db	3
      00076D 05                     859 	.sleb128	5
      00076E 01                     860 	.db	1
      00076F 09                     861 	.db	9
      000770 00 01                  862 	.dw	1+Sstm8s_it$EEPROM_EEC_IRQHandler$124-Sstm8s_it$EEPROM_EEC_IRQHandler$123
      000772 00                     863 	.db	0
      000773 01                     864 	.uleb128	1
      000774 01                     865 	.db	1
      000775                        866 Ldebug_line_end:
                                    867 
                                    868 	.area .debug_loc (NOLOAD)
      000A40                        869 Ldebug_loc_start:
      000A40 00 00 84 F2            870 	.dw	0,(Sstm8s_it$EEPROM_EEC_IRQHandler$122)
      000A44 00 00 84 F3            871 	.dw	0,(Sstm8s_it$EEPROM_EEC_IRQHandler$125)
      000A48 00 02                  872 	.dw	2
      000A4A 78                     873 	.db	120
      000A4B 01                     874 	.sleb128	1
      000A4C 00 00 00 00            875 	.dw	0,0
      000A50 00 00 00 00            876 	.dw	0,0
      000A54 00 00 84 F1            877 	.dw	0,(Sstm8s_it$TIM4_UPD_OVF_IRQHandler$117)
      000A58 00 00 84 F2            878 	.dw	0,(Sstm8s_it$TIM4_UPD_OVF_IRQHandler$120)
      000A5C 00 02                  879 	.dw	2
      000A5E 78                     880 	.db	120
      000A5F 01                     881 	.sleb128	1
      000A60 00 00 00 00            882 	.dw	0,0
      000A64 00 00 00 00            883 	.dw	0,0
      000A68 00 00 84 F0            884 	.dw	0,(Sstm8s_it$ADC2_IRQHandler$111)
      000A6C 00 00 84 F1            885 	.dw	0,(Sstm8s_it$ADC2_IRQHandler$115)
      000A70 00 02                  886 	.dw	2
      000A72 78                     887 	.db	120
      000A73 01                     888 	.sleb128	1
      000A74 00 00 00 00            889 	.dw	0,0
      000A78 00 00 00 00            890 	.dw	0,0
      000A7C 00 00 84 EF            891 	.dw	0,(Sstm8s_it$UART3_RX_IRQHandler$106)
      000A80 00 00 84 F0            892 	.dw	0,(Sstm8s_it$UART3_RX_IRQHandler$109)
      000A84 00 02                  893 	.dw	2
      000A86 78                     894 	.db	120
      000A87 01                     895 	.sleb128	1
      000A88 00 00 00 00            896 	.dw	0,0
      000A8C 00 00 00 00            897 	.dw	0,0
      000A90 00 00 84 EE            898 	.dw	0,(Sstm8s_it$UART3_TX_IRQHandler$101)
      000A94 00 00 84 EF            899 	.dw	0,(Sstm8s_it$UART3_TX_IRQHandler$104)
      000A98 00 02                  900 	.dw	2
      000A9A 78                     901 	.db	120
      000A9B 01                     902 	.sleb128	1
      000A9C 00 00 00 00            903 	.dw	0,0
      000AA0 00 00 00 00            904 	.dw	0,0
      000AA4 00 00 84 ED            905 	.dw	0,(Sstm8s_it$I2C_IRQHandler$96)
      000AA8 00 00 84 EE            906 	.dw	0,(Sstm8s_it$I2C_IRQHandler$99)
      000AAC 00 02                  907 	.dw	2
      000AAE 78                     908 	.db	120
      000AAF 01                     909 	.sleb128	1
      000AB0 00 00 00 00            910 	.dw	0,0
      000AB4 00 00 00 00            911 	.dw	0,0
      000AB8 00 00 84 EC            912 	.dw	0,(Sstm8s_it$UART1_RX_IRQHandler$91)
      000ABC 00 00 84 ED            913 	.dw	0,(Sstm8s_it$UART1_RX_IRQHandler$94)
      000AC0 00 02                  914 	.dw	2
      000AC2 78                     915 	.db	120
      000AC3 01                     916 	.sleb128	1
      000AC4 00 00 00 00            917 	.dw	0,0
      000AC8 00 00 00 00            918 	.dw	0,0
      000ACC 00 00 84 EB            919 	.dw	0,(Sstm8s_it$UART1_TX_IRQHandler$86)
      000AD0 00 00 84 EC            920 	.dw	0,(Sstm8s_it$UART1_TX_IRQHandler$89)
      000AD4 00 02                  921 	.dw	2
      000AD6 78                     922 	.db	120
      000AD7 01                     923 	.sleb128	1
      000AD8 00 00 00 00            924 	.dw	0,0
      000ADC 00 00 00 00            925 	.dw	0,0
      000AE0 00 00 84 EA            926 	.dw	0,(Sstm8s_it$TIM3_CAP_COM_IRQHandler$81)
      000AE4 00 00 84 EB            927 	.dw	0,(Sstm8s_it$TIM3_CAP_COM_IRQHandler$84)
      000AE8 00 02                  928 	.dw	2
      000AEA 78                     929 	.db	120
      000AEB 01                     930 	.sleb128	1
      000AEC 00 00 00 00            931 	.dw	0,0
      000AF0 00 00 00 00            932 	.dw	0,0
      000AF4 00 00 84 E9            933 	.dw	0,(Sstm8s_it$TIM3_UPD_OVF_BRK_IRQHandler$76)
      000AF8 00 00 84 EA            934 	.dw	0,(Sstm8s_it$TIM3_UPD_OVF_BRK_IRQHandler$79)
      000AFC 00 02                  935 	.dw	2
      000AFE 78                     936 	.db	120
      000AFF 01                     937 	.sleb128	1
      000B00 00 00 00 00            938 	.dw	0,0
      000B04 00 00 00 00            939 	.dw	0,0
      000B08 00 00 84 E8            940 	.dw	0,(Sstm8s_it$TIM2_CAP_COM_IRQHandler$71)
      000B0C 00 00 84 E9            941 	.dw	0,(Sstm8s_it$TIM2_CAP_COM_IRQHandler$74)
      000B10 00 02                  942 	.dw	2
      000B12 78                     943 	.db	120
      000B13 01                     944 	.sleb128	1
      000B14 00 00 00 00            945 	.dw	0,0
      000B18 00 00 00 00            946 	.dw	0,0
      000B1C 00 00 84 E7            947 	.dw	0,(Sstm8s_it$TIM2_UPD_OVF_BRK_IRQHandler$66)
      000B20 00 00 84 E8            948 	.dw	0,(Sstm8s_it$TIM2_UPD_OVF_BRK_IRQHandler$69)
      000B24 00 02                  949 	.dw	2
      000B26 78                     950 	.db	120
      000B27 01                     951 	.sleb128	1
      000B28 00 00 00 00            952 	.dw	0,0
      000B2C 00 00 00 00            953 	.dw	0,0
      000B30 00 00 84 E6            954 	.dw	0,(Sstm8s_it$TIM1_CAP_COM_IRQHandler$61)
      000B34 00 00 84 E7            955 	.dw	0,(Sstm8s_it$TIM1_CAP_COM_IRQHandler$64)
      000B38 00 02                  956 	.dw	2
      000B3A 78                     957 	.db	120
      000B3B 01                     958 	.sleb128	1
      000B3C 00 00 00 00            959 	.dw	0,0
      000B40 00 00 00 00            960 	.dw	0,0
      000B44 00 00 84 E5            961 	.dw	0,(Sstm8s_it$TIM1_UPD_OVF_TRG_BRK_IRQHandler$56)
      000B48 00 00 84 E6            962 	.dw	0,(Sstm8s_it$TIM1_UPD_OVF_TRG_BRK_IRQHandler$59)
      000B4C 00 02                  963 	.dw	2
      000B4E 78                     964 	.db	120
      000B4F 01                     965 	.sleb128	1
      000B50 00 00 00 00            966 	.dw	0,0
      000B54 00 00 00 00            967 	.dw	0,0
      000B58 00 00 84 E4            968 	.dw	0,(Sstm8s_it$SPI_IRQHandler$51)
      000B5C 00 00 84 E5            969 	.dw	0,(Sstm8s_it$SPI_IRQHandler$54)
      000B60 00 02                  970 	.dw	2
      000B62 78                     971 	.db	120
      000B63 01                     972 	.sleb128	1
      000B64 00 00 00 00            973 	.dw	0,0
      000B68 00 00 00 00            974 	.dw	0,0
      000B6C 00 00 84 E3            975 	.dw	0,(Sstm8s_it$CAN_TX_IRQHandler$46)
      000B70 00 00 84 E4            976 	.dw	0,(Sstm8s_it$CAN_TX_IRQHandler$49)
      000B74 00 02                  977 	.dw	2
      000B76 78                     978 	.db	120
      000B77 01                     979 	.sleb128	1
      000B78 00 00 00 00            980 	.dw	0,0
      000B7C 00 00 00 00            981 	.dw	0,0
      000B80 00 00 84 E2            982 	.dw	0,(Sstm8s_it$CAN_RX_IRQHandler$41)
      000B84 00 00 84 E3            983 	.dw	0,(Sstm8s_it$CAN_RX_IRQHandler$44)
      000B88 00 02                  984 	.dw	2
      000B8A 78                     985 	.db	120
      000B8B 01                     986 	.sleb128	1
      000B8C 00 00 00 00            987 	.dw	0,0
      000B90 00 00 00 00            988 	.dw	0,0
      000B94 00 00 84 E1            989 	.dw	0,(Sstm8s_it$EXTI_PORTD_IRQHandler$36)
      000B98 00 00 84 E2            990 	.dw	0,(Sstm8s_it$EXTI_PORTD_IRQHandler$39)
      000B9C 00 02                  991 	.dw	2
      000B9E 78                     992 	.db	120
      000B9F 01                     993 	.sleb128	1
      000BA0 00 00 00 00            994 	.dw	0,0
      000BA4 00 00 00 00            995 	.dw	0,0
      000BA8 00 00 84 E0            996 	.dw	0,(Sstm8s_it$EXTI_PORTC_IRQHandler$31)
      000BAC 00 00 84 E1            997 	.dw	0,(Sstm8s_it$EXTI_PORTC_IRQHandler$34)
      000BB0 00 02                  998 	.dw	2
      000BB2 78                     999 	.db	120
      000BB3 01                    1000 	.sleb128	1
      000BB4 00 00 00 00           1001 	.dw	0,0
      000BB8 00 00 00 00           1002 	.dw	0,0
      000BBC 00 00 84 DF           1003 	.dw	0,(Sstm8s_it$EXTI_PORTB_IRQHandler$26)
      000BC0 00 00 84 E0           1004 	.dw	0,(Sstm8s_it$EXTI_PORTB_IRQHandler$29)
      000BC4 00 02                 1005 	.dw	2
      000BC6 78                    1006 	.db	120
      000BC7 01                    1007 	.sleb128	1
      000BC8 00 00 00 00           1008 	.dw	0,0
      000BCC 00 00 00 00           1009 	.dw	0,0
      000BD0 00 00 84 DE           1010 	.dw	0,(Sstm8s_it$EXTI_PORTA_IRQHandler$21)
      000BD4 00 00 84 DF           1011 	.dw	0,(Sstm8s_it$EXTI_PORTA_IRQHandler$24)
      000BD8 00 02                 1012 	.dw	2
      000BDA 78                    1013 	.db	120
      000BDB 01                    1014 	.sleb128	1
      000BDC 00 00 00 00           1015 	.dw	0,0
      000BE0 00 00 00 00           1016 	.dw	0,0
      000BE4 00 00 84 DD           1017 	.dw	0,(Sstm8s_it$CLK_IRQHandler$16)
      000BE8 00 00 84 DE           1018 	.dw	0,(Sstm8s_it$CLK_IRQHandler$19)
      000BEC 00 02                 1019 	.dw	2
      000BEE 78                    1020 	.db	120
      000BEF 01                    1021 	.sleb128	1
      000BF0 00 00 00 00           1022 	.dw	0,0
      000BF4 00 00 00 00           1023 	.dw	0,0
      000BF8 00 00 84 DC           1024 	.dw	0,(Sstm8s_it$AWU_IRQHandler$11)
      000BFC 00 00 84 DD           1025 	.dw	0,(Sstm8s_it$AWU_IRQHandler$14)
      000C00 00 02                 1026 	.dw	2
      000C02 78                    1027 	.db	120
      000C03 01                    1028 	.sleb128	1
      000C04 00 00 00 00           1029 	.dw	0,0
      000C08 00 00 00 00           1030 	.dw	0,0
      000C0C 00 00 84 DB           1031 	.dw	0,(Sstm8s_it$TLI_IRQHandler$6)
      000C10 00 00 84 DC           1032 	.dw	0,(Sstm8s_it$TLI_IRQHandler$9)
      000C14 00 02                 1033 	.dw	2
      000C16 78                    1034 	.db	120
      000C17 01                    1035 	.sleb128	1
      000C18 00 00 00 00           1036 	.dw	0,0
      000C1C 00 00 00 00           1037 	.dw	0,0
      000C20 00 00 84 DA           1038 	.dw	0,(Sstm8s_it$TRAP_IRQHandler$1)
      000C24 00 00 84 DB           1039 	.dw	0,(Sstm8s_it$TRAP_IRQHandler$4)
      000C28 00 02                 1040 	.dw	2
      000C2A 78                    1041 	.db	120
      000C2B 01                    1042 	.sleb128	1
      000C2C 00 00 00 00           1043 	.dw	0,0
      000C30 00 00 00 00           1044 	.dw	0,0
                                   1045 
                                   1046 	.area .debug_abbrev (NOLOAD)
      0001B5                       1047 Ldebug_abbrev:
      0001B5 01                    1048 	.uleb128	1
      0001B6 11                    1049 	.uleb128	17
      0001B7 01                    1050 	.db	1
      0001B8 03                    1051 	.uleb128	3
      0001B9 08                    1052 	.uleb128	8
      0001BA 10                    1053 	.uleb128	16
      0001BB 06                    1054 	.uleb128	6
      0001BC 13                    1055 	.uleb128	19
      0001BD 0B                    1056 	.uleb128	11
      0001BE 25                    1057 	.uleb128	37
      0001BF 08                    1058 	.uleb128	8
      0001C0 00                    1059 	.uleb128	0
      0001C1 00                    1060 	.uleb128	0
      0001C2 02                    1061 	.uleb128	2
      0001C3 2E                    1062 	.uleb128	46
      0001C4 00                    1063 	.db	0
      0001C5 03                    1064 	.uleb128	3
      0001C6 08                    1065 	.uleb128	8
      0001C7 11                    1066 	.uleb128	17
      0001C8 01                    1067 	.uleb128	1
      0001C9 12                    1068 	.uleb128	18
      0001CA 01                    1069 	.uleb128	1
      0001CB 36                    1070 	.uleb128	54
      0001CC 0B                    1071 	.uleb128	11
      0001CD 3F                    1072 	.uleb128	63
      0001CE 0C                    1073 	.uleb128	12
      0001CF 40                    1074 	.uleb128	64
      0001D0 06                    1075 	.uleb128	6
      0001D1 00                    1076 	.uleb128	0
      0001D2 00                    1077 	.uleb128	0
      0001D3 00                    1078 	.uleb128	0
                                   1079 
                                   1080 	.area .debug_info (NOLOAD)
      00049E 00 00 03 B9           1081 	.dw	0,Ldebug_info_end-Ldebug_info_start
      0004A2                       1082 Ldebug_info_start:
      0004A2 00 02                 1083 	.dw	2
      0004A4 00 00 01 B5           1084 	.dw	0,(Ldebug_abbrev)
      0004A8 04                    1085 	.db	4
      0004A9 01                    1086 	.uleb128	1
      0004AA 61 70 70 2F 73 72 63  1087 	.ascii "app/src/stm8s_it.c"
             2F 73 74 6D 38 73 5F
             69 74 2E 63
      0004BC 00                    1088 	.db	0
      0004BD 00 00 04 B4           1089 	.dw	0,(Ldebug_line_start+-4)
      0004C1 01                    1090 	.db	1
      0004C2 53 44 43 43 20 76 65  1091 	.ascii "SDCC version 4.1.0 #12072"
             72 73 69 6F 6E 20 34
             2E 31 2E 30 20 23 31
             32 30 37 32
      0004DB 00                    1092 	.db	0
      0004DC 02                    1093 	.uleb128	2
      0004DD 54 52 41 50 5F 49 52  1094 	.ascii "TRAP_IRQHandler"
             51 48 61 6E 64 6C 65
             72
      0004EC 00                    1095 	.db	0
      0004ED 00 00 84 DA           1096 	.dw	0,(_TRAP_IRQHandler)
      0004F1 00 00 84 DB           1097 	.dw	0,(XG$TRAP_IRQHandler$0$0+1)
      0004F5 03                    1098 	.db	3
      0004F6 01                    1099 	.db	1
      0004F7 00 00 0C 20           1100 	.dw	0,(Ldebug_loc_start+480)
      0004FB 02                    1101 	.uleb128	2
      0004FC 54 4C 49 5F 49 52 51  1102 	.ascii "TLI_IRQHandler"
             48 61 6E 64 6C 65 72
      00050A 00                    1103 	.db	0
      00050B 00 00 84 DB           1104 	.dw	0,(_TLI_IRQHandler)
      00050F 00 00 84 DC           1105 	.dw	0,(XG$TLI_IRQHandler$0$0+1)
      000513 03                    1106 	.db	3
      000514 01                    1107 	.db	1
      000515 00 00 0C 0C           1108 	.dw	0,(Ldebug_loc_start+460)
      000519 02                    1109 	.uleb128	2
      00051A 41 57 55 5F 49 52 51  1110 	.ascii "AWU_IRQHandler"
             48 61 6E 64 6C 65 72
      000528 00                    1111 	.db	0
      000529 00 00 84 DC           1112 	.dw	0,(_AWU_IRQHandler)
      00052D 00 00 84 DD           1113 	.dw	0,(XG$AWU_IRQHandler$0$0+1)
      000531 03                    1114 	.db	3
      000532 01                    1115 	.db	1
      000533 00 00 0B F8           1116 	.dw	0,(Ldebug_loc_start+440)
      000537 02                    1117 	.uleb128	2
      000538 43 4C 4B 5F 49 52 51  1118 	.ascii "CLK_IRQHandler"
             48 61 6E 64 6C 65 72
      000546 00                    1119 	.db	0
      000547 00 00 84 DD           1120 	.dw	0,(_CLK_IRQHandler)
      00054B 00 00 84 DE           1121 	.dw	0,(XG$CLK_IRQHandler$0$0+1)
      00054F 03                    1122 	.db	3
      000550 01                    1123 	.db	1
      000551 00 00 0B E4           1124 	.dw	0,(Ldebug_loc_start+420)
      000555 02                    1125 	.uleb128	2
      000556 45 58 54 49 5F 50 4F  1126 	.ascii "EXTI_PORTA_IRQHandler"
             52 54 41 5F 49 52 51
             48 61 6E 64 6C 65 72
      00056B 00                    1127 	.db	0
      00056C 00 00 84 DE           1128 	.dw	0,(_EXTI_PORTA_IRQHandler)
      000570 00 00 84 DF           1129 	.dw	0,(XG$EXTI_PORTA_IRQHandler$0$0+1)
      000574 03                    1130 	.db	3
      000575 01                    1131 	.db	1
      000576 00 00 0B D0           1132 	.dw	0,(Ldebug_loc_start+400)
      00057A 02                    1133 	.uleb128	2
      00057B 45 58 54 49 5F 50 4F  1134 	.ascii "EXTI_PORTB_IRQHandler"
             52 54 42 5F 49 52 51
             48 61 6E 64 6C 65 72
      000590 00                    1135 	.db	0
      000591 00 00 84 DF           1136 	.dw	0,(_EXTI_PORTB_IRQHandler)
      000595 00 00 84 E0           1137 	.dw	0,(XG$EXTI_PORTB_IRQHandler$0$0+1)
      000599 03                    1138 	.db	3
      00059A 01                    1139 	.db	1
      00059B 00 00 0B BC           1140 	.dw	0,(Ldebug_loc_start+380)
      00059F 02                    1141 	.uleb128	2
      0005A0 45 58 54 49 5F 50 4F  1142 	.ascii "EXTI_PORTC_IRQHandler"
             52 54 43 5F 49 52 51
             48 61 6E 64 6C 65 72
      0005B5 00                    1143 	.db	0
      0005B6 00 00 84 E0           1144 	.dw	0,(_EXTI_PORTC_IRQHandler)
      0005BA 00 00 84 E1           1145 	.dw	0,(XG$EXTI_PORTC_IRQHandler$0$0+1)
      0005BE 03                    1146 	.db	3
      0005BF 01                    1147 	.db	1
      0005C0 00 00 0B A8           1148 	.dw	0,(Ldebug_loc_start+360)
      0005C4 02                    1149 	.uleb128	2
      0005C5 45 58 54 49 5F 50 4F  1150 	.ascii "EXTI_PORTD_IRQHandler"
             52 54 44 5F 49 52 51
             48 61 6E 64 6C 65 72
      0005DA 00                    1151 	.db	0
      0005DB 00 00 84 E1           1152 	.dw	0,(_EXTI_PORTD_IRQHandler)
      0005DF 00 00 84 E2           1153 	.dw	0,(XG$EXTI_PORTD_IRQHandler$0$0+1)
      0005E3 03                    1154 	.db	3
      0005E4 01                    1155 	.db	1
      0005E5 00 00 0B 94           1156 	.dw	0,(Ldebug_loc_start+340)
      0005E9 02                    1157 	.uleb128	2
      0005EA 43 41 4E 5F 52 58 5F  1158 	.ascii "CAN_RX_IRQHandler"
             49 52 51 48 61 6E 64
             6C 65 72
      0005FB 00                    1159 	.db	0
      0005FC 00 00 84 E2           1160 	.dw	0,(_CAN_RX_IRQHandler)
      000600 00 00 84 E3           1161 	.dw	0,(XG$CAN_RX_IRQHandler$0$0+1)
      000604 03                    1162 	.db	3
      000605 01                    1163 	.db	1
      000606 00 00 0B 80           1164 	.dw	0,(Ldebug_loc_start+320)
      00060A 02                    1165 	.uleb128	2
      00060B 43 41 4E 5F 54 58 5F  1166 	.ascii "CAN_TX_IRQHandler"
             49 52 51 48 61 6E 64
             6C 65 72
      00061C 00                    1167 	.db	0
      00061D 00 00 84 E3           1168 	.dw	0,(_CAN_TX_IRQHandler)
      000621 00 00 84 E4           1169 	.dw	0,(XG$CAN_TX_IRQHandler$0$0+1)
      000625 03                    1170 	.db	3
      000626 01                    1171 	.db	1
      000627 00 00 0B 6C           1172 	.dw	0,(Ldebug_loc_start+300)
      00062B 02                    1173 	.uleb128	2
      00062C 53 50 49 5F 49 52 51  1174 	.ascii "SPI_IRQHandler"
             48 61 6E 64 6C 65 72
      00063A 00                    1175 	.db	0
      00063B 00 00 84 E4           1176 	.dw	0,(_SPI_IRQHandler)
      00063F 00 00 84 E5           1177 	.dw	0,(XG$SPI_IRQHandler$0$0+1)
      000643 03                    1178 	.db	3
      000644 01                    1179 	.db	1
      000645 00 00 0B 58           1180 	.dw	0,(Ldebug_loc_start+280)
      000649 02                    1181 	.uleb128	2
      00064A 54 49 4D 31 5F 55 50  1182 	.ascii "TIM1_UPD_OVF_TRG_BRK_IRQHandler"
             44 5F 4F 56 46 5F 54
             52 47 5F 42 52 4B 5F
             49 52 51 48 61 6E 64
             6C 65 72
      000669 00                    1183 	.db	0
      00066A 00 00 84 E5           1184 	.dw	0,(_TIM1_UPD_OVF_TRG_BRK_IRQHandler)
      00066E 00 00 84 E6           1185 	.dw	0,(XG$TIM1_UPD_OVF_TRG_BRK_IRQHandler$0$0+1)
      000672 03                    1186 	.db	3
      000673 01                    1187 	.db	1
      000674 00 00 0B 44           1188 	.dw	0,(Ldebug_loc_start+260)
      000678 02                    1189 	.uleb128	2
      000679 54 49 4D 31 5F 43 41  1190 	.ascii "TIM1_CAP_COM_IRQHandler"
             50 5F 43 4F 4D 5F 49
             52 51 48 61 6E 64 6C
             65 72
      000690 00                    1191 	.db	0
      000691 00 00 84 E6           1192 	.dw	0,(_TIM1_CAP_COM_IRQHandler)
      000695 00 00 84 E7           1193 	.dw	0,(XG$TIM1_CAP_COM_IRQHandler$0$0+1)
      000699 03                    1194 	.db	3
      00069A 01                    1195 	.db	1
      00069B 00 00 0B 30           1196 	.dw	0,(Ldebug_loc_start+240)
      00069F 02                    1197 	.uleb128	2
      0006A0 54 49 4D 32 5F 55 50  1198 	.ascii "TIM2_UPD_OVF_BRK_IRQHandler"
             44 5F 4F 56 46 5F 42
             52 4B 5F 49 52 51 48
             61 6E 64 6C 65 72
      0006BB 00                    1199 	.db	0
      0006BC 00 00 84 E7           1200 	.dw	0,(_TIM2_UPD_OVF_BRK_IRQHandler)
      0006C0 00 00 84 E8           1201 	.dw	0,(XG$TIM2_UPD_OVF_BRK_IRQHandler$0$0+1)
      0006C4 03                    1202 	.db	3
      0006C5 01                    1203 	.db	1
      0006C6 00 00 0B 1C           1204 	.dw	0,(Ldebug_loc_start+220)
      0006CA 02                    1205 	.uleb128	2
      0006CB 54 49 4D 32 5F 43 41  1206 	.ascii "TIM2_CAP_COM_IRQHandler"
             50 5F 43 4F 4D 5F 49
             52 51 48 61 6E 64 6C
             65 72
      0006E2 00                    1207 	.db	0
      0006E3 00 00 84 E8           1208 	.dw	0,(_TIM2_CAP_COM_IRQHandler)
      0006E7 00 00 84 E9           1209 	.dw	0,(XG$TIM2_CAP_COM_IRQHandler$0$0+1)
      0006EB 03                    1210 	.db	3
      0006EC 01                    1211 	.db	1
      0006ED 00 00 0B 08           1212 	.dw	0,(Ldebug_loc_start+200)
      0006F1 02                    1213 	.uleb128	2
      0006F2 54 49 4D 33 5F 55 50  1214 	.ascii "TIM3_UPD_OVF_BRK_IRQHandler"
             44 5F 4F 56 46 5F 42
             52 4B 5F 49 52 51 48
             61 6E 64 6C 65 72
      00070D 00                    1215 	.db	0
      00070E 00 00 84 E9           1216 	.dw	0,(_TIM3_UPD_OVF_BRK_IRQHandler)
      000712 00 00 84 EA           1217 	.dw	0,(XG$TIM3_UPD_OVF_BRK_IRQHandler$0$0+1)
      000716 03                    1218 	.db	3
      000717 01                    1219 	.db	1
      000718 00 00 0A F4           1220 	.dw	0,(Ldebug_loc_start+180)
      00071C 02                    1221 	.uleb128	2
      00071D 54 49 4D 33 5F 43 41  1222 	.ascii "TIM3_CAP_COM_IRQHandler"
             50 5F 43 4F 4D 5F 49
             52 51 48 61 6E 64 6C
             65 72
      000734 00                    1223 	.db	0
      000735 00 00 84 EA           1224 	.dw	0,(_TIM3_CAP_COM_IRQHandler)
      000739 00 00 84 EB           1225 	.dw	0,(XG$TIM3_CAP_COM_IRQHandler$0$0+1)
      00073D 03                    1226 	.db	3
      00073E 01                    1227 	.db	1
      00073F 00 00 0A E0           1228 	.dw	0,(Ldebug_loc_start+160)
      000743 02                    1229 	.uleb128	2
      000744 55 41 52 54 31 5F 54  1230 	.ascii "UART1_TX_IRQHandler"
             58 5F 49 52 51 48 61
             6E 64 6C 65 72
      000757 00                    1231 	.db	0
      000758 00 00 84 EB           1232 	.dw	0,(_UART1_TX_IRQHandler)
      00075C 00 00 84 EC           1233 	.dw	0,(XG$UART1_TX_IRQHandler$0$0+1)
      000760 03                    1234 	.db	3
      000761 01                    1235 	.db	1
      000762 00 00 0A CC           1236 	.dw	0,(Ldebug_loc_start+140)
      000766 02                    1237 	.uleb128	2
      000767 55 41 52 54 31 5F 52  1238 	.ascii "UART1_RX_IRQHandler"
             58 5F 49 52 51 48 61
             6E 64 6C 65 72
      00077A 00                    1239 	.db	0
      00077B 00 00 84 EC           1240 	.dw	0,(_UART1_RX_IRQHandler)
      00077F 00 00 84 ED           1241 	.dw	0,(XG$UART1_RX_IRQHandler$0$0+1)
      000783 03                    1242 	.db	3
      000784 01                    1243 	.db	1
      000785 00 00 0A B8           1244 	.dw	0,(Ldebug_loc_start+120)
      000789 02                    1245 	.uleb128	2
      00078A 49 32 43 5F 49 52 51  1246 	.ascii "I2C_IRQHandler"
             48 61 6E 64 6C 65 72
      000798 00                    1247 	.db	0
      000799 00 00 84 ED           1248 	.dw	0,(_I2C_IRQHandler)
      00079D 00 00 84 EE           1249 	.dw	0,(XG$I2C_IRQHandler$0$0+1)
      0007A1 03                    1250 	.db	3
      0007A2 01                    1251 	.db	1
      0007A3 00 00 0A A4           1252 	.dw	0,(Ldebug_loc_start+100)
      0007A7 02                    1253 	.uleb128	2
      0007A8 55 41 52 54 33 5F 54  1254 	.ascii "UART3_TX_IRQHandler"
             58 5F 49 52 51 48 61
             6E 64 6C 65 72
      0007BB 00                    1255 	.db	0
      0007BC 00 00 84 EE           1256 	.dw	0,(_UART3_TX_IRQHandler)
      0007C0 00 00 84 EF           1257 	.dw	0,(XG$UART3_TX_IRQHandler$0$0+1)
      0007C4 03                    1258 	.db	3
      0007C5 01                    1259 	.db	1
      0007C6 00 00 0A 90           1260 	.dw	0,(Ldebug_loc_start+80)
      0007CA 02                    1261 	.uleb128	2
      0007CB 55 41 52 54 33 5F 52  1262 	.ascii "UART3_RX_IRQHandler"
             58 5F 49 52 51 48 61
             6E 64 6C 65 72
      0007DE 00                    1263 	.db	0
      0007DF 00 00 84 EF           1264 	.dw	0,(_UART3_RX_IRQHandler)
      0007E3 00 00 84 F0           1265 	.dw	0,(XG$UART3_RX_IRQHandler$0$0+1)
      0007E7 03                    1266 	.db	3
      0007E8 01                    1267 	.db	1
      0007E9 00 00 0A 7C           1268 	.dw	0,(Ldebug_loc_start+60)
      0007ED 02                    1269 	.uleb128	2
      0007EE 41 44 43 32 5F 49 52  1270 	.ascii "ADC2_IRQHandler"
             51 48 61 6E 64 6C 65
             72
      0007FD 00                    1271 	.db	0
      0007FE 00 00 84 F0           1272 	.dw	0,(_ADC2_IRQHandler)
      000802 00 00 84 F1           1273 	.dw	0,(XG$ADC2_IRQHandler$0$0+1)
      000806 03                    1274 	.db	3
      000807 01                    1275 	.db	1
      000808 00 00 0A 68           1276 	.dw	0,(Ldebug_loc_start+40)
      00080C 02                    1277 	.uleb128	2
      00080D 54 49 4D 34 5F 55 50  1278 	.ascii "TIM4_UPD_OVF_IRQHandler"
             44 5F 4F 56 46 5F 49
             52 51 48 61 6E 64 6C
             65 72
      000824 00                    1279 	.db	0
      000825 00 00 84 F1           1280 	.dw	0,(_TIM4_UPD_OVF_IRQHandler)
      000829 00 00 84 F2           1281 	.dw	0,(XG$TIM4_UPD_OVF_IRQHandler$0$0+1)
      00082D 03                    1282 	.db	3
      00082E 01                    1283 	.db	1
      00082F 00 00 0A 54           1284 	.dw	0,(Ldebug_loc_start+20)
      000833 02                    1285 	.uleb128	2
      000834 45 45 50 52 4F 4D 5F  1286 	.ascii "EEPROM_EEC_IRQHandler"
             45 45 43 5F 49 52 51
             48 61 6E 64 6C 65 72
      000849 00                    1287 	.db	0
      00084A 00 00 84 F2           1288 	.dw	0,(_EEPROM_EEC_IRQHandler)
      00084E 00 00 84 F3           1289 	.dw	0,(XG$EEPROM_EEC_IRQHandler$0$0+1)
      000852 03                    1290 	.db	3
      000853 01                    1291 	.db	1
      000854 00 00 0A 40           1292 	.dw	0,(Ldebug_loc_start)
      000858 00                    1293 	.uleb128	0
      000859 00                    1294 	.uleb128	0
      00085A 00                    1295 	.uleb128	0
      00085B                       1296 Ldebug_info_end:
                                   1297 
                                   1298 	.area .debug_pubnames (NOLOAD)
      0000F4 00 00 02 77           1299 	.dw	0,Ldebug_pubnames_end-Ldebug_pubnames_start
      0000F8                       1300 Ldebug_pubnames_start:
      0000F8 00 02                 1301 	.dw	2
      0000FA 00 00 04 9E           1302 	.dw	0,(Ldebug_info_start-4)
      0000FE 00 00 03 BD           1303 	.dw	0,4+Ldebug_info_end-Ldebug_info_start
      000102 00 00 00 3E           1304 	.dw	0,62
      000106 54 52 41 50 5F 49 52  1305 	.ascii "TRAP_IRQHandler"
             51 48 61 6E 64 6C 65
             72
      000115 00                    1306 	.db	0
      000116 00 00 00 5D           1307 	.dw	0,93
      00011A 54 4C 49 5F 49 52 51  1308 	.ascii "TLI_IRQHandler"
             48 61 6E 64 6C 65 72
      000128 00                    1309 	.db	0
      000129 00 00 00 7B           1310 	.dw	0,123
      00012D 41 57 55 5F 49 52 51  1311 	.ascii "AWU_IRQHandler"
             48 61 6E 64 6C 65 72
      00013B 00                    1312 	.db	0
      00013C 00 00 00 99           1313 	.dw	0,153
      000140 43 4C 4B 5F 49 52 51  1314 	.ascii "CLK_IRQHandler"
             48 61 6E 64 6C 65 72
      00014E 00                    1315 	.db	0
      00014F 00 00 00 B7           1316 	.dw	0,183
      000153 45 58 54 49 5F 50 4F  1317 	.ascii "EXTI_PORTA_IRQHandler"
             52 54 41 5F 49 52 51
             48 61 6E 64 6C 65 72
      000168 00                    1318 	.db	0
      000169 00 00 00 DC           1319 	.dw	0,220
      00016D 45 58 54 49 5F 50 4F  1320 	.ascii "EXTI_PORTB_IRQHandler"
             52 54 42 5F 49 52 51
             48 61 6E 64 6C 65 72
      000182 00                    1321 	.db	0
      000183 00 00 01 01           1322 	.dw	0,257
      000187 45 58 54 49 5F 50 4F  1323 	.ascii "EXTI_PORTC_IRQHandler"
             52 54 43 5F 49 52 51
             48 61 6E 64 6C 65 72
      00019C 00                    1324 	.db	0
      00019D 00 00 01 26           1325 	.dw	0,294
      0001A1 45 58 54 49 5F 50 4F  1326 	.ascii "EXTI_PORTD_IRQHandler"
             52 54 44 5F 49 52 51
             48 61 6E 64 6C 65 72
      0001B6 00                    1327 	.db	0
      0001B7 00 00 01 4B           1328 	.dw	0,331
      0001BB 43 41 4E 5F 52 58 5F  1329 	.ascii "CAN_RX_IRQHandler"
             49 52 51 48 61 6E 64
             6C 65 72
      0001CC 00                    1330 	.db	0
      0001CD 00 00 01 6C           1331 	.dw	0,364
      0001D1 43 41 4E 5F 54 58 5F  1332 	.ascii "CAN_TX_IRQHandler"
             49 52 51 48 61 6E 64
             6C 65 72
      0001E2 00                    1333 	.db	0
      0001E3 00 00 01 8D           1334 	.dw	0,397
      0001E7 53 50 49 5F 49 52 51  1335 	.ascii "SPI_IRQHandler"
             48 61 6E 64 6C 65 72
      0001F5 00                    1336 	.db	0
      0001F6 00 00 01 AB           1337 	.dw	0,427
      0001FA 54 49 4D 31 5F 55 50  1338 	.ascii "TIM1_UPD_OVF_TRG_BRK_IRQHandler"
             44 5F 4F 56 46 5F 54
             52 47 5F 42 52 4B 5F
             49 52 51 48 61 6E 64
             6C 65 72
      000219 00                    1339 	.db	0
      00021A 00 00 01 DA           1340 	.dw	0,474
      00021E 54 49 4D 31 5F 43 41  1341 	.ascii "TIM1_CAP_COM_IRQHandler"
             50 5F 43 4F 4D 5F 49
             52 51 48 61 6E 64 6C
             65 72
      000235 00                    1342 	.db	0
      000236 00 00 02 01           1343 	.dw	0,513
      00023A 54 49 4D 32 5F 55 50  1344 	.ascii "TIM2_UPD_OVF_BRK_IRQHandler"
             44 5F 4F 56 46 5F 42
             52 4B 5F 49 52 51 48
             61 6E 64 6C 65 72
      000255 00                    1345 	.db	0
      000256 00 00 02 2C           1346 	.dw	0,556
      00025A 54 49 4D 32 5F 43 41  1347 	.ascii "TIM2_CAP_COM_IRQHandler"
             50 5F 43 4F 4D 5F 49
             52 51 48 61 6E 64 6C
             65 72
      000271 00                    1348 	.db	0
      000272 00 00 02 53           1349 	.dw	0,595
      000276 54 49 4D 33 5F 55 50  1350 	.ascii "TIM3_UPD_OVF_BRK_IRQHandler"
             44 5F 4F 56 46 5F 42
             52 4B 5F 49 52 51 48
             61 6E 64 6C 65 72
      000291 00                    1351 	.db	0
      000292 00 00 02 7E           1352 	.dw	0,638
      000296 54 49 4D 33 5F 43 41  1353 	.ascii "TIM3_CAP_COM_IRQHandler"
             50 5F 43 4F 4D 5F 49
             52 51 48 61 6E 64 6C
             65 72
      0002AD 00                    1354 	.db	0
      0002AE 00 00 02 A5           1355 	.dw	0,677
      0002B2 55 41 52 54 31 5F 54  1356 	.ascii "UART1_TX_IRQHandler"
             58 5F 49 52 51 48 61
             6E 64 6C 65 72
      0002C5 00                    1357 	.db	0
      0002C6 00 00 02 C8           1358 	.dw	0,712
      0002CA 55 41 52 54 31 5F 52  1359 	.ascii "UART1_RX_IRQHandler"
             58 5F 49 52 51 48 61
             6E 64 6C 65 72
      0002DD 00                    1360 	.db	0
      0002DE 00 00 02 EB           1361 	.dw	0,747
      0002E2 49 32 43 5F 49 52 51  1362 	.ascii "I2C_IRQHandler"
             48 61 6E 64 6C 65 72
      0002F0 00                    1363 	.db	0
      0002F1 00 00 03 09           1364 	.dw	0,777
      0002F5 55 41 52 54 33 5F 54  1365 	.ascii "UART3_TX_IRQHandler"
             58 5F 49 52 51 48 61
             6E 64 6C 65 72
      000308 00                    1366 	.db	0
      000309 00 00 03 2C           1367 	.dw	0,812
      00030D 55 41 52 54 33 5F 52  1368 	.ascii "UART3_RX_IRQHandler"
             58 5F 49 52 51 48 61
             6E 64 6C 65 72
      000320 00                    1369 	.db	0
      000321 00 00 03 4F           1370 	.dw	0,847
      000325 41 44 43 32 5F 49 52  1371 	.ascii "ADC2_IRQHandler"
             51 48 61 6E 64 6C 65
             72
      000334 00                    1372 	.db	0
      000335 00 00 03 6E           1373 	.dw	0,878
      000339 54 49 4D 34 5F 55 50  1374 	.ascii "TIM4_UPD_OVF_IRQHandler"
             44 5F 4F 56 46 5F 49
             52 51 48 61 6E 64 6C
             65 72
      000350 00                    1375 	.db	0
      000351 00 00 03 95           1376 	.dw	0,917
      000355 45 45 50 52 4F 4D 5F  1377 	.ascii "EEPROM_EEC_IRQHandler"
             45 45 43 5F 49 52 51
             48 61 6E 64 6C 65 72
      00036A 00                    1378 	.db	0
      00036B 00 00 00 00           1379 	.dw	0,0
      00036F                       1380 Ldebug_pubnames_end:
                                   1381 
                                   1382 	.area .debug_frame (NOLOAD)
      000720 00 00                 1383 	.dw	0
      000722 00 0E                 1384 	.dw	Ldebug_CIE0_end-Ldebug_CIE0_start
      000724                       1385 Ldebug_CIE0_start:
      000724 FF FF                 1386 	.dw	0xffff
      000726 FF FF                 1387 	.dw	0xffff
      000728 01                    1388 	.db	1
      000729 00                    1389 	.db	0
      00072A 01                    1390 	.uleb128	1
      00072B 7F                    1391 	.sleb128	-1
      00072C 09                    1392 	.db	9
      00072D 0C                    1393 	.db	12
      00072E 08                    1394 	.uleb128	8
      00072F 09                    1395 	.uleb128	9
      000730 89                    1396 	.db	137
      000731 01                    1397 	.uleb128	1
      000732                       1398 Ldebug_CIE0_end:
      000732 00 00 00 13           1399 	.dw	0,19
      000736 00 00 07 20           1400 	.dw	0,(Ldebug_CIE0_start-4)
      00073A 00 00 84 F2           1401 	.dw	0,(Sstm8s_it$EEPROM_EEC_IRQHandler$122)	;initial loc
      00073E 00 00 00 01           1402 	.dw	0,Sstm8s_it$EEPROM_EEC_IRQHandler$125-Sstm8s_it$EEPROM_EEC_IRQHandler$122
      000742 01                    1403 	.db	1
      000743 00 00 84 F2           1404 	.dw	0,(Sstm8s_it$EEPROM_EEC_IRQHandler$122)
      000747 0E                    1405 	.db	14
      000748 09                    1406 	.uleb128	9
                                   1407 
                                   1408 	.area .debug_frame (NOLOAD)
      000749 00 00                 1409 	.dw	0
      00074B 00 0E                 1410 	.dw	Ldebug_CIE1_end-Ldebug_CIE1_start
      00074D                       1411 Ldebug_CIE1_start:
      00074D FF FF                 1412 	.dw	0xffff
      00074F FF FF                 1413 	.dw	0xffff
      000751 01                    1414 	.db	1
      000752 00                    1415 	.db	0
      000753 01                    1416 	.uleb128	1
      000754 7F                    1417 	.sleb128	-1
      000755 09                    1418 	.db	9
      000756 0C                    1419 	.db	12
      000757 08                    1420 	.uleb128	8
      000758 09                    1421 	.uleb128	9
      000759 89                    1422 	.db	137
      00075A 01                    1423 	.uleb128	1
      00075B                       1424 Ldebug_CIE1_end:
      00075B 00 00 00 13           1425 	.dw	0,19
      00075F 00 00 07 49           1426 	.dw	0,(Ldebug_CIE1_start-4)
      000763 00 00 84 F1           1427 	.dw	0,(Sstm8s_it$TIM4_UPD_OVF_IRQHandler$117)	;initial loc
      000767 00 00 00 01           1428 	.dw	0,Sstm8s_it$TIM4_UPD_OVF_IRQHandler$120-Sstm8s_it$TIM4_UPD_OVF_IRQHandler$117
      00076B 01                    1429 	.db	1
      00076C 00 00 84 F1           1430 	.dw	0,(Sstm8s_it$TIM4_UPD_OVF_IRQHandler$117)
      000770 0E                    1431 	.db	14
      000771 09                    1432 	.uleb128	9
                                   1433 
                                   1434 	.area .debug_frame (NOLOAD)
      000772 00 00                 1435 	.dw	0
      000774 00 0E                 1436 	.dw	Ldebug_CIE2_end-Ldebug_CIE2_start
      000776                       1437 Ldebug_CIE2_start:
      000776 FF FF                 1438 	.dw	0xffff
      000778 FF FF                 1439 	.dw	0xffff
      00077A 01                    1440 	.db	1
      00077B 00                    1441 	.db	0
      00077C 01                    1442 	.uleb128	1
      00077D 7F                    1443 	.sleb128	-1
      00077E 09                    1444 	.db	9
      00077F 0C                    1445 	.db	12
      000780 08                    1446 	.uleb128	8
      000781 09                    1447 	.uleb128	9
      000782 89                    1448 	.db	137
      000783 01                    1449 	.uleb128	1
      000784                       1450 Ldebug_CIE2_end:
      000784 00 00 00 13           1451 	.dw	0,19
      000788 00 00 07 72           1452 	.dw	0,(Ldebug_CIE2_start-4)
      00078C 00 00 84 F0           1453 	.dw	0,(Sstm8s_it$ADC2_IRQHandler$111)	;initial loc
      000790 00 00 00 01           1454 	.dw	0,Sstm8s_it$ADC2_IRQHandler$115-Sstm8s_it$ADC2_IRQHandler$111
      000794 01                    1455 	.db	1
      000795 00 00 84 F0           1456 	.dw	0,(Sstm8s_it$ADC2_IRQHandler$111)
      000799 0E                    1457 	.db	14
      00079A 09                    1458 	.uleb128	9
                                   1459 
                                   1460 	.area .debug_frame (NOLOAD)
      00079B 00 00                 1461 	.dw	0
      00079D 00 0E                 1462 	.dw	Ldebug_CIE3_end-Ldebug_CIE3_start
      00079F                       1463 Ldebug_CIE3_start:
      00079F FF FF                 1464 	.dw	0xffff
      0007A1 FF FF                 1465 	.dw	0xffff
      0007A3 01                    1466 	.db	1
      0007A4 00                    1467 	.db	0
      0007A5 01                    1468 	.uleb128	1
      0007A6 7F                    1469 	.sleb128	-1
      0007A7 09                    1470 	.db	9
      0007A8 0C                    1471 	.db	12
      0007A9 08                    1472 	.uleb128	8
      0007AA 09                    1473 	.uleb128	9
      0007AB 89                    1474 	.db	137
      0007AC 01                    1475 	.uleb128	1
      0007AD                       1476 Ldebug_CIE3_end:
      0007AD 00 00 00 13           1477 	.dw	0,19
      0007B1 00 00 07 9B           1478 	.dw	0,(Ldebug_CIE3_start-4)
      0007B5 00 00 84 EF           1479 	.dw	0,(Sstm8s_it$UART3_RX_IRQHandler$106)	;initial loc
      0007B9 00 00 00 01           1480 	.dw	0,Sstm8s_it$UART3_RX_IRQHandler$109-Sstm8s_it$UART3_RX_IRQHandler$106
      0007BD 01                    1481 	.db	1
      0007BE 00 00 84 EF           1482 	.dw	0,(Sstm8s_it$UART3_RX_IRQHandler$106)
      0007C2 0E                    1483 	.db	14
      0007C3 09                    1484 	.uleb128	9
                                   1485 
                                   1486 	.area .debug_frame (NOLOAD)
      0007C4 00 00                 1487 	.dw	0
      0007C6 00 0E                 1488 	.dw	Ldebug_CIE4_end-Ldebug_CIE4_start
      0007C8                       1489 Ldebug_CIE4_start:
      0007C8 FF FF                 1490 	.dw	0xffff
      0007CA FF FF                 1491 	.dw	0xffff
      0007CC 01                    1492 	.db	1
      0007CD 00                    1493 	.db	0
      0007CE 01                    1494 	.uleb128	1
      0007CF 7F                    1495 	.sleb128	-1
      0007D0 09                    1496 	.db	9
      0007D1 0C                    1497 	.db	12
      0007D2 08                    1498 	.uleb128	8
      0007D3 09                    1499 	.uleb128	9
      0007D4 89                    1500 	.db	137
      0007D5 01                    1501 	.uleb128	1
      0007D6                       1502 Ldebug_CIE4_end:
      0007D6 00 00 00 13           1503 	.dw	0,19
      0007DA 00 00 07 C4           1504 	.dw	0,(Ldebug_CIE4_start-4)
      0007DE 00 00 84 EE           1505 	.dw	0,(Sstm8s_it$UART3_TX_IRQHandler$101)	;initial loc
      0007E2 00 00 00 01           1506 	.dw	0,Sstm8s_it$UART3_TX_IRQHandler$104-Sstm8s_it$UART3_TX_IRQHandler$101
      0007E6 01                    1507 	.db	1
      0007E7 00 00 84 EE           1508 	.dw	0,(Sstm8s_it$UART3_TX_IRQHandler$101)
      0007EB 0E                    1509 	.db	14
      0007EC 09                    1510 	.uleb128	9
                                   1511 
                                   1512 	.area .debug_frame (NOLOAD)
      0007ED 00 00                 1513 	.dw	0
      0007EF 00 0E                 1514 	.dw	Ldebug_CIE5_end-Ldebug_CIE5_start
      0007F1                       1515 Ldebug_CIE5_start:
      0007F1 FF FF                 1516 	.dw	0xffff
      0007F3 FF FF                 1517 	.dw	0xffff
      0007F5 01                    1518 	.db	1
      0007F6 00                    1519 	.db	0
      0007F7 01                    1520 	.uleb128	1
      0007F8 7F                    1521 	.sleb128	-1
      0007F9 09                    1522 	.db	9
      0007FA 0C                    1523 	.db	12
      0007FB 08                    1524 	.uleb128	8
      0007FC 09                    1525 	.uleb128	9
      0007FD 89                    1526 	.db	137
      0007FE 01                    1527 	.uleb128	1
      0007FF                       1528 Ldebug_CIE5_end:
      0007FF 00 00 00 13           1529 	.dw	0,19
      000803 00 00 07 ED           1530 	.dw	0,(Ldebug_CIE5_start-4)
      000807 00 00 84 ED           1531 	.dw	0,(Sstm8s_it$I2C_IRQHandler$96)	;initial loc
      00080B 00 00 00 01           1532 	.dw	0,Sstm8s_it$I2C_IRQHandler$99-Sstm8s_it$I2C_IRQHandler$96
      00080F 01                    1533 	.db	1
      000810 00 00 84 ED           1534 	.dw	0,(Sstm8s_it$I2C_IRQHandler$96)
      000814 0E                    1535 	.db	14
      000815 09                    1536 	.uleb128	9
                                   1537 
                                   1538 	.area .debug_frame (NOLOAD)
      000816 00 00                 1539 	.dw	0
      000818 00 0E                 1540 	.dw	Ldebug_CIE6_end-Ldebug_CIE6_start
      00081A                       1541 Ldebug_CIE6_start:
      00081A FF FF                 1542 	.dw	0xffff
      00081C FF FF                 1543 	.dw	0xffff
      00081E 01                    1544 	.db	1
      00081F 00                    1545 	.db	0
      000820 01                    1546 	.uleb128	1
      000821 7F                    1547 	.sleb128	-1
      000822 09                    1548 	.db	9
      000823 0C                    1549 	.db	12
      000824 08                    1550 	.uleb128	8
      000825 09                    1551 	.uleb128	9
      000826 89                    1552 	.db	137
      000827 01                    1553 	.uleb128	1
      000828                       1554 Ldebug_CIE6_end:
      000828 00 00 00 13           1555 	.dw	0,19
      00082C 00 00 08 16           1556 	.dw	0,(Ldebug_CIE6_start-4)
      000830 00 00 84 EC           1557 	.dw	0,(Sstm8s_it$UART1_RX_IRQHandler$91)	;initial loc
      000834 00 00 00 01           1558 	.dw	0,Sstm8s_it$UART1_RX_IRQHandler$94-Sstm8s_it$UART1_RX_IRQHandler$91
      000838 01                    1559 	.db	1
      000839 00 00 84 EC           1560 	.dw	0,(Sstm8s_it$UART1_RX_IRQHandler$91)
      00083D 0E                    1561 	.db	14
      00083E 09                    1562 	.uleb128	9
                                   1563 
                                   1564 	.area .debug_frame (NOLOAD)
      00083F 00 00                 1565 	.dw	0
      000841 00 0E                 1566 	.dw	Ldebug_CIE7_end-Ldebug_CIE7_start
      000843                       1567 Ldebug_CIE7_start:
      000843 FF FF                 1568 	.dw	0xffff
      000845 FF FF                 1569 	.dw	0xffff
      000847 01                    1570 	.db	1
      000848 00                    1571 	.db	0
      000849 01                    1572 	.uleb128	1
      00084A 7F                    1573 	.sleb128	-1
      00084B 09                    1574 	.db	9
      00084C 0C                    1575 	.db	12
      00084D 08                    1576 	.uleb128	8
      00084E 09                    1577 	.uleb128	9
      00084F 89                    1578 	.db	137
      000850 01                    1579 	.uleb128	1
      000851                       1580 Ldebug_CIE7_end:
      000851 00 00 00 13           1581 	.dw	0,19
      000855 00 00 08 3F           1582 	.dw	0,(Ldebug_CIE7_start-4)
      000859 00 00 84 EB           1583 	.dw	0,(Sstm8s_it$UART1_TX_IRQHandler$86)	;initial loc
      00085D 00 00 00 01           1584 	.dw	0,Sstm8s_it$UART1_TX_IRQHandler$89-Sstm8s_it$UART1_TX_IRQHandler$86
      000861 01                    1585 	.db	1
      000862 00 00 84 EB           1586 	.dw	0,(Sstm8s_it$UART1_TX_IRQHandler$86)
      000866 0E                    1587 	.db	14
      000867 09                    1588 	.uleb128	9
                                   1589 
                                   1590 	.area .debug_frame (NOLOAD)
      000868 00 00                 1591 	.dw	0
      00086A 00 0E                 1592 	.dw	Ldebug_CIE8_end-Ldebug_CIE8_start
      00086C                       1593 Ldebug_CIE8_start:
      00086C FF FF                 1594 	.dw	0xffff
      00086E FF FF                 1595 	.dw	0xffff
      000870 01                    1596 	.db	1
      000871 00                    1597 	.db	0
      000872 01                    1598 	.uleb128	1
      000873 7F                    1599 	.sleb128	-1
      000874 09                    1600 	.db	9
      000875 0C                    1601 	.db	12
      000876 08                    1602 	.uleb128	8
      000877 09                    1603 	.uleb128	9
      000878 89                    1604 	.db	137
      000879 01                    1605 	.uleb128	1
      00087A                       1606 Ldebug_CIE8_end:
      00087A 00 00 00 13           1607 	.dw	0,19
      00087E 00 00 08 68           1608 	.dw	0,(Ldebug_CIE8_start-4)
      000882 00 00 84 EA           1609 	.dw	0,(Sstm8s_it$TIM3_CAP_COM_IRQHandler$81)	;initial loc
      000886 00 00 00 01           1610 	.dw	0,Sstm8s_it$TIM3_CAP_COM_IRQHandler$84-Sstm8s_it$TIM3_CAP_COM_IRQHandler$81
      00088A 01                    1611 	.db	1
      00088B 00 00 84 EA           1612 	.dw	0,(Sstm8s_it$TIM3_CAP_COM_IRQHandler$81)
      00088F 0E                    1613 	.db	14
      000890 09                    1614 	.uleb128	9
                                   1615 
                                   1616 	.area .debug_frame (NOLOAD)
      000891 00 00                 1617 	.dw	0
      000893 00 0E                 1618 	.dw	Ldebug_CIE9_end-Ldebug_CIE9_start
      000895                       1619 Ldebug_CIE9_start:
      000895 FF FF                 1620 	.dw	0xffff
      000897 FF FF                 1621 	.dw	0xffff
      000899 01                    1622 	.db	1
      00089A 00                    1623 	.db	0
      00089B 01                    1624 	.uleb128	1
      00089C 7F                    1625 	.sleb128	-1
      00089D 09                    1626 	.db	9
      00089E 0C                    1627 	.db	12
      00089F 08                    1628 	.uleb128	8
      0008A0 09                    1629 	.uleb128	9
      0008A1 89                    1630 	.db	137
      0008A2 01                    1631 	.uleb128	1
      0008A3                       1632 Ldebug_CIE9_end:
      0008A3 00 00 00 13           1633 	.dw	0,19
      0008A7 00 00 08 91           1634 	.dw	0,(Ldebug_CIE9_start-4)
      0008AB 00 00 84 E9           1635 	.dw	0,(Sstm8s_it$TIM3_UPD_OVF_BRK_IRQHandler$76)	;initial loc
      0008AF 00 00 00 01           1636 	.dw	0,Sstm8s_it$TIM3_UPD_OVF_BRK_IRQHandler$79-Sstm8s_it$TIM3_UPD_OVF_BRK_IRQHandler$76
      0008B3 01                    1637 	.db	1
      0008B4 00 00 84 E9           1638 	.dw	0,(Sstm8s_it$TIM3_UPD_OVF_BRK_IRQHandler$76)
      0008B8 0E                    1639 	.db	14
      0008B9 09                    1640 	.uleb128	9
                                   1641 
                                   1642 	.area .debug_frame (NOLOAD)
      0008BA 00 00                 1643 	.dw	0
      0008BC 00 0E                 1644 	.dw	Ldebug_CIE10_end-Ldebug_CIE10_start
      0008BE                       1645 Ldebug_CIE10_start:
      0008BE FF FF                 1646 	.dw	0xffff
      0008C0 FF FF                 1647 	.dw	0xffff
      0008C2 01                    1648 	.db	1
      0008C3 00                    1649 	.db	0
      0008C4 01                    1650 	.uleb128	1
      0008C5 7F                    1651 	.sleb128	-1
      0008C6 09                    1652 	.db	9
      0008C7 0C                    1653 	.db	12
      0008C8 08                    1654 	.uleb128	8
      0008C9 09                    1655 	.uleb128	9
      0008CA 89                    1656 	.db	137
      0008CB 01                    1657 	.uleb128	1
      0008CC                       1658 Ldebug_CIE10_end:
      0008CC 00 00 00 13           1659 	.dw	0,19
      0008D0 00 00 08 BA           1660 	.dw	0,(Ldebug_CIE10_start-4)
      0008D4 00 00 84 E8           1661 	.dw	0,(Sstm8s_it$TIM2_CAP_COM_IRQHandler$71)	;initial loc
      0008D8 00 00 00 01           1662 	.dw	0,Sstm8s_it$TIM2_CAP_COM_IRQHandler$74-Sstm8s_it$TIM2_CAP_COM_IRQHandler$71
      0008DC 01                    1663 	.db	1
      0008DD 00 00 84 E8           1664 	.dw	0,(Sstm8s_it$TIM2_CAP_COM_IRQHandler$71)
      0008E1 0E                    1665 	.db	14
      0008E2 09                    1666 	.uleb128	9
                                   1667 
                                   1668 	.area .debug_frame (NOLOAD)
      0008E3 00 00                 1669 	.dw	0
      0008E5 00 0E                 1670 	.dw	Ldebug_CIE11_end-Ldebug_CIE11_start
      0008E7                       1671 Ldebug_CIE11_start:
      0008E7 FF FF                 1672 	.dw	0xffff
      0008E9 FF FF                 1673 	.dw	0xffff
      0008EB 01                    1674 	.db	1
      0008EC 00                    1675 	.db	0
      0008ED 01                    1676 	.uleb128	1
      0008EE 7F                    1677 	.sleb128	-1
      0008EF 09                    1678 	.db	9
      0008F0 0C                    1679 	.db	12
      0008F1 08                    1680 	.uleb128	8
      0008F2 09                    1681 	.uleb128	9
      0008F3 89                    1682 	.db	137
      0008F4 01                    1683 	.uleb128	1
      0008F5                       1684 Ldebug_CIE11_end:
      0008F5 00 00 00 13           1685 	.dw	0,19
      0008F9 00 00 08 E3           1686 	.dw	0,(Ldebug_CIE11_start-4)
      0008FD 00 00 84 E7           1687 	.dw	0,(Sstm8s_it$TIM2_UPD_OVF_BRK_IRQHandler$66)	;initial loc
      000901 00 00 00 01           1688 	.dw	0,Sstm8s_it$TIM2_UPD_OVF_BRK_IRQHandler$69-Sstm8s_it$TIM2_UPD_OVF_BRK_IRQHandler$66
      000905 01                    1689 	.db	1
      000906 00 00 84 E7           1690 	.dw	0,(Sstm8s_it$TIM2_UPD_OVF_BRK_IRQHandler$66)
      00090A 0E                    1691 	.db	14
      00090B 09                    1692 	.uleb128	9
                                   1693 
                                   1694 	.area .debug_frame (NOLOAD)
      00090C 00 00                 1695 	.dw	0
      00090E 00 0E                 1696 	.dw	Ldebug_CIE12_end-Ldebug_CIE12_start
      000910                       1697 Ldebug_CIE12_start:
      000910 FF FF                 1698 	.dw	0xffff
      000912 FF FF                 1699 	.dw	0xffff
      000914 01                    1700 	.db	1
      000915 00                    1701 	.db	0
      000916 01                    1702 	.uleb128	1
      000917 7F                    1703 	.sleb128	-1
      000918 09                    1704 	.db	9
      000919 0C                    1705 	.db	12
      00091A 08                    1706 	.uleb128	8
      00091B 09                    1707 	.uleb128	9
      00091C 89                    1708 	.db	137
      00091D 01                    1709 	.uleb128	1
      00091E                       1710 Ldebug_CIE12_end:
      00091E 00 00 00 13           1711 	.dw	0,19
      000922 00 00 09 0C           1712 	.dw	0,(Ldebug_CIE12_start-4)
      000926 00 00 84 E6           1713 	.dw	0,(Sstm8s_it$TIM1_CAP_COM_IRQHandler$61)	;initial loc
      00092A 00 00 00 01           1714 	.dw	0,Sstm8s_it$TIM1_CAP_COM_IRQHandler$64-Sstm8s_it$TIM1_CAP_COM_IRQHandler$61
      00092E 01                    1715 	.db	1
      00092F 00 00 84 E6           1716 	.dw	0,(Sstm8s_it$TIM1_CAP_COM_IRQHandler$61)
      000933 0E                    1717 	.db	14
      000934 09                    1718 	.uleb128	9
                                   1719 
                                   1720 	.area .debug_frame (NOLOAD)
      000935 00 00                 1721 	.dw	0
      000937 00 0E                 1722 	.dw	Ldebug_CIE13_end-Ldebug_CIE13_start
      000939                       1723 Ldebug_CIE13_start:
      000939 FF FF                 1724 	.dw	0xffff
      00093B FF FF                 1725 	.dw	0xffff
      00093D 01                    1726 	.db	1
      00093E 00                    1727 	.db	0
      00093F 01                    1728 	.uleb128	1
      000940 7F                    1729 	.sleb128	-1
      000941 09                    1730 	.db	9
      000942 0C                    1731 	.db	12
      000943 08                    1732 	.uleb128	8
      000944 09                    1733 	.uleb128	9
      000945 89                    1734 	.db	137
      000946 01                    1735 	.uleb128	1
      000947                       1736 Ldebug_CIE13_end:
      000947 00 00 00 13           1737 	.dw	0,19
      00094B 00 00 09 35           1738 	.dw	0,(Ldebug_CIE13_start-4)
      00094F 00 00 84 E5           1739 	.dw	0,(Sstm8s_it$TIM1_UPD_OVF_TRG_BRK_IRQHandler$56)	;initial loc
      000953 00 00 00 01           1740 	.dw	0,Sstm8s_it$TIM1_UPD_OVF_TRG_BRK_IRQHandler$59-Sstm8s_it$TIM1_UPD_OVF_TRG_BRK_IRQHandler$56
      000957 01                    1741 	.db	1
      000958 00 00 84 E5           1742 	.dw	0,(Sstm8s_it$TIM1_UPD_OVF_TRG_BRK_IRQHandler$56)
      00095C 0E                    1743 	.db	14
      00095D 09                    1744 	.uleb128	9
                                   1745 
                                   1746 	.area .debug_frame (NOLOAD)
      00095E 00 00                 1747 	.dw	0
      000960 00 0E                 1748 	.dw	Ldebug_CIE14_end-Ldebug_CIE14_start
      000962                       1749 Ldebug_CIE14_start:
      000962 FF FF                 1750 	.dw	0xffff
      000964 FF FF                 1751 	.dw	0xffff
      000966 01                    1752 	.db	1
      000967 00                    1753 	.db	0
      000968 01                    1754 	.uleb128	1
      000969 7F                    1755 	.sleb128	-1
      00096A 09                    1756 	.db	9
      00096B 0C                    1757 	.db	12
      00096C 08                    1758 	.uleb128	8
      00096D 09                    1759 	.uleb128	9
      00096E 89                    1760 	.db	137
      00096F 01                    1761 	.uleb128	1
      000970                       1762 Ldebug_CIE14_end:
      000970 00 00 00 13           1763 	.dw	0,19
      000974 00 00 09 5E           1764 	.dw	0,(Ldebug_CIE14_start-4)
      000978 00 00 84 E4           1765 	.dw	0,(Sstm8s_it$SPI_IRQHandler$51)	;initial loc
      00097C 00 00 00 01           1766 	.dw	0,Sstm8s_it$SPI_IRQHandler$54-Sstm8s_it$SPI_IRQHandler$51
      000980 01                    1767 	.db	1
      000981 00 00 84 E4           1768 	.dw	0,(Sstm8s_it$SPI_IRQHandler$51)
      000985 0E                    1769 	.db	14
      000986 09                    1770 	.uleb128	9
                                   1771 
                                   1772 	.area .debug_frame (NOLOAD)
      000987 00 00                 1773 	.dw	0
      000989 00 0E                 1774 	.dw	Ldebug_CIE15_end-Ldebug_CIE15_start
      00098B                       1775 Ldebug_CIE15_start:
      00098B FF FF                 1776 	.dw	0xffff
      00098D FF FF                 1777 	.dw	0xffff
      00098F 01                    1778 	.db	1
      000990 00                    1779 	.db	0
      000991 01                    1780 	.uleb128	1
      000992 7F                    1781 	.sleb128	-1
      000993 09                    1782 	.db	9
      000994 0C                    1783 	.db	12
      000995 08                    1784 	.uleb128	8
      000996 09                    1785 	.uleb128	9
      000997 89                    1786 	.db	137
      000998 01                    1787 	.uleb128	1
      000999                       1788 Ldebug_CIE15_end:
      000999 00 00 00 13           1789 	.dw	0,19
      00099D 00 00 09 87           1790 	.dw	0,(Ldebug_CIE15_start-4)
      0009A1 00 00 84 E3           1791 	.dw	0,(Sstm8s_it$CAN_TX_IRQHandler$46)	;initial loc
      0009A5 00 00 00 01           1792 	.dw	0,Sstm8s_it$CAN_TX_IRQHandler$49-Sstm8s_it$CAN_TX_IRQHandler$46
      0009A9 01                    1793 	.db	1
      0009AA 00 00 84 E3           1794 	.dw	0,(Sstm8s_it$CAN_TX_IRQHandler$46)
      0009AE 0E                    1795 	.db	14
      0009AF 09                    1796 	.uleb128	9
                                   1797 
                                   1798 	.area .debug_frame (NOLOAD)
      0009B0 00 00                 1799 	.dw	0
      0009B2 00 0E                 1800 	.dw	Ldebug_CIE16_end-Ldebug_CIE16_start
      0009B4                       1801 Ldebug_CIE16_start:
      0009B4 FF FF                 1802 	.dw	0xffff
      0009B6 FF FF                 1803 	.dw	0xffff
      0009B8 01                    1804 	.db	1
      0009B9 00                    1805 	.db	0
      0009BA 01                    1806 	.uleb128	1
      0009BB 7F                    1807 	.sleb128	-1
      0009BC 09                    1808 	.db	9
      0009BD 0C                    1809 	.db	12
      0009BE 08                    1810 	.uleb128	8
      0009BF 09                    1811 	.uleb128	9
      0009C0 89                    1812 	.db	137
      0009C1 01                    1813 	.uleb128	1
      0009C2                       1814 Ldebug_CIE16_end:
      0009C2 00 00 00 13           1815 	.dw	0,19
      0009C6 00 00 09 B0           1816 	.dw	0,(Ldebug_CIE16_start-4)
      0009CA 00 00 84 E2           1817 	.dw	0,(Sstm8s_it$CAN_RX_IRQHandler$41)	;initial loc
      0009CE 00 00 00 01           1818 	.dw	0,Sstm8s_it$CAN_RX_IRQHandler$44-Sstm8s_it$CAN_RX_IRQHandler$41
      0009D2 01                    1819 	.db	1
      0009D3 00 00 84 E2           1820 	.dw	0,(Sstm8s_it$CAN_RX_IRQHandler$41)
      0009D7 0E                    1821 	.db	14
      0009D8 09                    1822 	.uleb128	9
                                   1823 
                                   1824 	.area .debug_frame (NOLOAD)
      0009D9 00 00                 1825 	.dw	0
      0009DB 00 0E                 1826 	.dw	Ldebug_CIE17_end-Ldebug_CIE17_start
      0009DD                       1827 Ldebug_CIE17_start:
      0009DD FF FF                 1828 	.dw	0xffff
      0009DF FF FF                 1829 	.dw	0xffff
      0009E1 01                    1830 	.db	1
      0009E2 00                    1831 	.db	0
      0009E3 01                    1832 	.uleb128	1
      0009E4 7F                    1833 	.sleb128	-1
      0009E5 09                    1834 	.db	9
      0009E6 0C                    1835 	.db	12
      0009E7 08                    1836 	.uleb128	8
      0009E8 09                    1837 	.uleb128	9
      0009E9 89                    1838 	.db	137
      0009EA 01                    1839 	.uleb128	1
      0009EB                       1840 Ldebug_CIE17_end:
      0009EB 00 00 00 13           1841 	.dw	0,19
      0009EF 00 00 09 D9           1842 	.dw	0,(Ldebug_CIE17_start-4)
      0009F3 00 00 84 E1           1843 	.dw	0,(Sstm8s_it$EXTI_PORTD_IRQHandler$36)	;initial loc
      0009F7 00 00 00 01           1844 	.dw	0,Sstm8s_it$EXTI_PORTD_IRQHandler$39-Sstm8s_it$EXTI_PORTD_IRQHandler$36
      0009FB 01                    1845 	.db	1
      0009FC 00 00 84 E1           1846 	.dw	0,(Sstm8s_it$EXTI_PORTD_IRQHandler$36)
      000A00 0E                    1847 	.db	14
      000A01 09                    1848 	.uleb128	9
                                   1849 
                                   1850 	.area .debug_frame (NOLOAD)
      000A02 00 00                 1851 	.dw	0
      000A04 00 0E                 1852 	.dw	Ldebug_CIE18_end-Ldebug_CIE18_start
      000A06                       1853 Ldebug_CIE18_start:
      000A06 FF FF                 1854 	.dw	0xffff
      000A08 FF FF                 1855 	.dw	0xffff
      000A0A 01                    1856 	.db	1
      000A0B 00                    1857 	.db	0
      000A0C 01                    1858 	.uleb128	1
      000A0D 7F                    1859 	.sleb128	-1
      000A0E 09                    1860 	.db	9
      000A0F 0C                    1861 	.db	12
      000A10 08                    1862 	.uleb128	8
      000A11 09                    1863 	.uleb128	9
      000A12 89                    1864 	.db	137
      000A13 01                    1865 	.uleb128	1
      000A14                       1866 Ldebug_CIE18_end:
      000A14 00 00 00 13           1867 	.dw	0,19
      000A18 00 00 0A 02           1868 	.dw	0,(Ldebug_CIE18_start-4)
      000A1C 00 00 84 E0           1869 	.dw	0,(Sstm8s_it$EXTI_PORTC_IRQHandler$31)	;initial loc
      000A20 00 00 00 01           1870 	.dw	0,Sstm8s_it$EXTI_PORTC_IRQHandler$34-Sstm8s_it$EXTI_PORTC_IRQHandler$31
      000A24 01                    1871 	.db	1
      000A25 00 00 84 E0           1872 	.dw	0,(Sstm8s_it$EXTI_PORTC_IRQHandler$31)
      000A29 0E                    1873 	.db	14
      000A2A 09                    1874 	.uleb128	9
                                   1875 
                                   1876 	.area .debug_frame (NOLOAD)
      000A2B 00 00                 1877 	.dw	0
      000A2D 00 0E                 1878 	.dw	Ldebug_CIE19_end-Ldebug_CIE19_start
      000A2F                       1879 Ldebug_CIE19_start:
      000A2F FF FF                 1880 	.dw	0xffff
      000A31 FF FF                 1881 	.dw	0xffff
      000A33 01                    1882 	.db	1
      000A34 00                    1883 	.db	0
      000A35 01                    1884 	.uleb128	1
      000A36 7F                    1885 	.sleb128	-1
      000A37 09                    1886 	.db	9
      000A38 0C                    1887 	.db	12
      000A39 08                    1888 	.uleb128	8
      000A3A 09                    1889 	.uleb128	9
      000A3B 89                    1890 	.db	137
      000A3C 01                    1891 	.uleb128	1
      000A3D                       1892 Ldebug_CIE19_end:
      000A3D 00 00 00 13           1893 	.dw	0,19
      000A41 00 00 0A 2B           1894 	.dw	0,(Ldebug_CIE19_start-4)
      000A45 00 00 84 DF           1895 	.dw	0,(Sstm8s_it$EXTI_PORTB_IRQHandler$26)	;initial loc
      000A49 00 00 00 01           1896 	.dw	0,Sstm8s_it$EXTI_PORTB_IRQHandler$29-Sstm8s_it$EXTI_PORTB_IRQHandler$26
      000A4D 01                    1897 	.db	1
      000A4E 00 00 84 DF           1898 	.dw	0,(Sstm8s_it$EXTI_PORTB_IRQHandler$26)
      000A52 0E                    1899 	.db	14
      000A53 09                    1900 	.uleb128	9
                                   1901 
                                   1902 	.area .debug_frame (NOLOAD)
      000A54 00 00                 1903 	.dw	0
      000A56 00 0E                 1904 	.dw	Ldebug_CIE20_end-Ldebug_CIE20_start
      000A58                       1905 Ldebug_CIE20_start:
      000A58 FF FF                 1906 	.dw	0xffff
      000A5A FF FF                 1907 	.dw	0xffff
      000A5C 01                    1908 	.db	1
      000A5D 00                    1909 	.db	0
      000A5E 01                    1910 	.uleb128	1
      000A5F 7F                    1911 	.sleb128	-1
      000A60 09                    1912 	.db	9
      000A61 0C                    1913 	.db	12
      000A62 08                    1914 	.uleb128	8
      000A63 09                    1915 	.uleb128	9
      000A64 89                    1916 	.db	137
      000A65 01                    1917 	.uleb128	1
      000A66                       1918 Ldebug_CIE20_end:
      000A66 00 00 00 13           1919 	.dw	0,19
      000A6A 00 00 0A 54           1920 	.dw	0,(Ldebug_CIE20_start-4)
      000A6E 00 00 84 DE           1921 	.dw	0,(Sstm8s_it$EXTI_PORTA_IRQHandler$21)	;initial loc
      000A72 00 00 00 01           1922 	.dw	0,Sstm8s_it$EXTI_PORTA_IRQHandler$24-Sstm8s_it$EXTI_PORTA_IRQHandler$21
      000A76 01                    1923 	.db	1
      000A77 00 00 84 DE           1924 	.dw	0,(Sstm8s_it$EXTI_PORTA_IRQHandler$21)
      000A7B 0E                    1925 	.db	14
      000A7C 09                    1926 	.uleb128	9
                                   1927 
                                   1928 	.area .debug_frame (NOLOAD)
      000A7D 00 00                 1929 	.dw	0
      000A7F 00 0E                 1930 	.dw	Ldebug_CIE21_end-Ldebug_CIE21_start
      000A81                       1931 Ldebug_CIE21_start:
      000A81 FF FF                 1932 	.dw	0xffff
      000A83 FF FF                 1933 	.dw	0xffff
      000A85 01                    1934 	.db	1
      000A86 00                    1935 	.db	0
      000A87 01                    1936 	.uleb128	1
      000A88 7F                    1937 	.sleb128	-1
      000A89 09                    1938 	.db	9
      000A8A 0C                    1939 	.db	12
      000A8B 08                    1940 	.uleb128	8
      000A8C 09                    1941 	.uleb128	9
      000A8D 89                    1942 	.db	137
      000A8E 01                    1943 	.uleb128	1
      000A8F                       1944 Ldebug_CIE21_end:
      000A8F 00 00 00 13           1945 	.dw	0,19
      000A93 00 00 0A 7D           1946 	.dw	0,(Ldebug_CIE21_start-4)
      000A97 00 00 84 DD           1947 	.dw	0,(Sstm8s_it$CLK_IRQHandler$16)	;initial loc
      000A9B 00 00 00 01           1948 	.dw	0,Sstm8s_it$CLK_IRQHandler$19-Sstm8s_it$CLK_IRQHandler$16
      000A9F 01                    1949 	.db	1
      000AA0 00 00 84 DD           1950 	.dw	0,(Sstm8s_it$CLK_IRQHandler$16)
      000AA4 0E                    1951 	.db	14
      000AA5 09                    1952 	.uleb128	9
                                   1953 
                                   1954 	.area .debug_frame (NOLOAD)
      000AA6 00 00                 1955 	.dw	0
      000AA8 00 0E                 1956 	.dw	Ldebug_CIE22_end-Ldebug_CIE22_start
      000AAA                       1957 Ldebug_CIE22_start:
      000AAA FF FF                 1958 	.dw	0xffff
      000AAC FF FF                 1959 	.dw	0xffff
      000AAE 01                    1960 	.db	1
      000AAF 00                    1961 	.db	0
      000AB0 01                    1962 	.uleb128	1
      000AB1 7F                    1963 	.sleb128	-1
      000AB2 09                    1964 	.db	9
      000AB3 0C                    1965 	.db	12
      000AB4 08                    1966 	.uleb128	8
      000AB5 09                    1967 	.uleb128	9
      000AB6 89                    1968 	.db	137
      000AB7 01                    1969 	.uleb128	1
      000AB8                       1970 Ldebug_CIE22_end:
      000AB8 00 00 00 13           1971 	.dw	0,19
      000ABC 00 00 0A A6           1972 	.dw	0,(Ldebug_CIE22_start-4)
      000AC0 00 00 84 DC           1973 	.dw	0,(Sstm8s_it$AWU_IRQHandler$11)	;initial loc
      000AC4 00 00 00 01           1974 	.dw	0,Sstm8s_it$AWU_IRQHandler$14-Sstm8s_it$AWU_IRQHandler$11
      000AC8 01                    1975 	.db	1
      000AC9 00 00 84 DC           1976 	.dw	0,(Sstm8s_it$AWU_IRQHandler$11)
      000ACD 0E                    1977 	.db	14
      000ACE 09                    1978 	.uleb128	9
                                   1979 
                                   1980 	.area .debug_frame (NOLOAD)
      000ACF 00 00                 1981 	.dw	0
      000AD1 00 0E                 1982 	.dw	Ldebug_CIE23_end-Ldebug_CIE23_start
      000AD3                       1983 Ldebug_CIE23_start:
      000AD3 FF FF                 1984 	.dw	0xffff
      000AD5 FF FF                 1985 	.dw	0xffff
      000AD7 01                    1986 	.db	1
      000AD8 00                    1987 	.db	0
      000AD9 01                    1988 	.uleb128	1
      000ADA 7F                    1989 	.sleb128	-1
      000ADB 09                    1990 	.db	9
      000ADC 0C                    1991 	.db	12
      000ADD 08                    1992 	.uleb128	8
      000ADE 09                    1993 	.uleb128	9
      000ADF 89                    1994 	.db	137
      000AE0 01                    1995 	.uleb128	1
      000AE1                       1996 Ldebug_CIE23_end:
      000AE1 00 00 00 13           1997 	.dw	0,19
      000AE5 00 00 0A CF           1998 	.dw	0,(Ldebug_CIE23_start-4)
      000AE9 00 00 84 DB           1999 	.dw	0,(Sstm8s_it$TLI_IRQHandler$6)	;initial loc
      000AED 00 00 00 01           2000 	.dw	0,Sstm8s_it$TLI_IRQHandler$9-Sstm8s_it$TLI_IRQHandler$6
      000AF1 01                    2001 	.db	1
      000AF2 00 00 84 DB           2002 	.dw	0,(Sstm8s_it$TLI_IRQHandler$6)
      000AF6 0E                    2003 	.db	14
      000AF7 09                    2004 	.uleb128	9
                                   2005 
                                   2006 	.area .debug_frame (NOLOAD)
      000AF8 00 00                 2007 	.dw	0
      000AFA 00 0E                 2008 	.dw	Ldebug_CIE24_end-Ldebug_CIE24_start
      000AFC                       2009 Ldebug_CIE24_start:
      000AFC FF FF                 2010 	.dw	0xffff
      000AFE FF FF                 2011 	.dw	0xffff
      000B00 01                    2012 	.db	1
      000B01 00                    2013 	.db	0
      000B02 01                    2014 	.uleb128	1
      000B03 7F                    2015 	.sleb128	-1
      000B04 09                    2016 	.db	9
      000B05 0C                    2017 	.db	12
      000B06 08                    2018 	.uleb128	8
      000B07 09                    2019 	.uleb128	9
      000B08 89                    2020 	.db	137
      000B09 01                    2021 	.uleb128	1
      000B0A                       2022 Ldebug_CIE24_end:
      000B0A 00 00 00 13           2023 	.dw	0,19
      000B0E 00 00 0A F8           2024 	.dw	0,(Ldebug_CIE24_start-4)
      000B12 00 00 84 DA           2025 	.dw	0,(Sstm8s_it$TRAP_IRQHandler$1)	;initial loc
      000B16 00 00 00 01           2026 	.dw	0,Sstm8s_it$TRAP_IRQHandler$4-Sstm8s_it$TRAP_IRQHandler$1
      000B1A 01                    2027 	.db	1
      000B1B 00 00 84 DA           2028 	.dw	0,(Sstm8s_it$TRAP_IRQHandler$1)
      000B1F 0E                    2029 	.db	14
      000B20 09                    2030 	.uleb128	9
