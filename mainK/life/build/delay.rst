                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.1.0 #12072 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module delay
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _TIM3_ClearFlag
                                     12 	.globl _TIM3_GetFlagStatus
                                     13 	.globl _TIM3_SetCounter
                                     14 	.globl _TIM3_Cmd
                                     15 	.globl _TIM3_TimeBaseInit
                                     16 	.globl _delay_05s_init
                                     17 	.globl _delay_05s
                                     18 ;--------------------------------------------------------
                                     19 ; ram data
                                     20 ;--------------------------------------------------------
                                     21 	.area DATA
                                     22 ;--------------------------------------------------------
                                     23 ; ram data
                                     24 ;--------------------------------------------------------
                                     25 	.area INITIALIZED
                                     26 ;--------------------------------------------------------
                                     27 ; absolute external ram data
                                     28 ;--------------------------------------------------------
                                     29 	.area DABS (ABS)
                                     30 
                                     31 ; default segment ordering for linker
                                     32 	.area HOME
                                     33 	.area GSINIT
                                     34 	.area GSFINAL
                                     35 	.area CONST
                                     36 	.area INITIALIZER
                                     37 	.area CODE
                                     38 
                                     39 ;--------------------------------------------------------
                                     40 ; global & static initialisations
                                     41 ;--------------------------------------------------------
                                     42 	.area HOME
                                     43 	.area GSINIT
                                     44 	.area GSFINAL
                                     45 	.area GSINIT
                                     46 ;--------------------------------------------------------
                                     47 ; Home
                                     48 ;--------------------------------------------------------
                                     49 	.area HOME
                                     50 	.area HOME
                                     51 ;--------------------------------------------------------
                                     52 ; code
                                     53 ;--------------------------------------------------------
                                     54 	.area CODE
                           000000    55 	Sdelay$delay_05s_init$0 ==.
                                     56 ;	app/src/delay.c: 4: void delay_05s_init(void)
                                     57 ;	-----------------------------------------
                                     58 ;	 function delay_05s_init
                                     59 ;	-----------------------------------------
      0081DD                         60 _delay_05s_init:
                           000000    61 	Sdelay$delay_05s_init$1 ==.
                           000000    62 	Sdelay$delay_05s_init$2 ==.
                                     63 ;	app/src/delay.c: 6: TIM3_TimeBaseInit(TIM3_PRESCALER_256,31499);
      0081DD 4B 0B            [ 1]   64 	push	#0x0b
                           000002    65 	Sdelay$delay_05s_init$3 ==.
      0081DF 4B 7B            [ 1]   66 	push	#0x7b
                           000004    67 	Sdelay$delay_05s_init$4 ==.
      0081E1 4B 08            [ 1]   68 	push	#0x08
                           000006    69 	Sdelay$delay_05s_init$5 ==.
      0081E3 CD 9A BF         [ 4]   70 	call	_TIM3_TimeBaseInit
      0081E6 5B 03            [ 2]   71 	addw	sp, #3
                           00000B    72 	Sdelay$delay_05s_init$6 ==.
                           00000B    73 	Sdelay$delay_05s_init$7 ==.
                                     74 ;	app/src/delay.c: 7: TIM3_Cmd(ENABLE);
      0081E8 4B 01            [ 1]   75 	push	#0x01
                           00000D    76 	Sdelay$delay_05s_init$8 ==.
      0081EA CD 9D DC         [ 4]   77 	call	_TIM3_Cmd
      0081ED 84               [ 1]   78 	pop	a
                           000011    79 	Sdelay$delay_05s_init$9 ==.
                           000011    80 	Sdelay$delay_05s_init$10 ==.
                                     81 ;	app/src/delay.c: 9: }
                           000011    82 	Sdelay$delay_05s_init$11 ==.
                           000011    83 	XG$delay_05s_init$0$0 ==.
      0081EE 81               [ 4]   84 	ret
                           000012    85 	Sdelay$delay_05s_init$12 ==.
                           000012    86 	Sdelay$delay_05s$13 ==.
                                     87 ;	app/src/delay.c: 10: void delay_05s(uint32_t time_s){//kolikrát přeteče
                                     88 ;	-----------------------------------------
                                     89 ;	 function delay_05s
                                     90 ;	-----------------------------------------
      0081EF                         91 _delay_05s:
                           000012    92 	Sdelay$delay_05s$14 ==.
      0081EF 52 04            [ 2]   93 	sub	sp, #4
                           000014    94 	Sdelay$delay_05s$15 ==.
                           000014    95 	Sdelay$delay_05s$16 ==.
                                     96 ;	app/src/delay.c: 11: TIM3_SetCounter(0);
      0081F1 5F               [ 1]   97 	clrw	x
      0081F2 89               [ 2]   98 	pushw	x
                           000016    99 	Sdelay$delay_05s$17 ==.
      0081F3 CD A1 A1         [ 4]  100 	call	_TIM3_SetCounter
      0081F6 85               [ 2]  101 	popw	x
                           00001A   102 	Sdelay$delay_05s$18 ==.
                           00001A   103 	Sdelay$delay_05s$19 ==.
                                    104 ;	app/src/delay.c: 12: for(uint32_t i=0;i<time_s;i++){
      0081F7 5F               [ 1]  105 	clrw	x
      0081F8 1F 03            [ 2]  106 	ldw	(0x03, sp), x
      0081FA 1F 01            [ 2]  107 	ldw	(0x01, sp), x
                           00001F   108 	Sdelay$delay_05s$20 ==.
      0081FC                        109 00106$:
      0081FC 1E 03            [ 2]  110 	ldw	x, (0x03, sp)
      0081FE 13 09            [ 2]  111 	cpw	x, (0x09, sp)
      008200 7B 02            [ 1]  112 	ld	a, (0x02, sp)
      008202 12 08            [ 1]  113 	sbc	a, (0x08, sp)
      008204 7B 01            [ 1]  114 	ld	a, (0x01, sp)
      008206 12 07            [ 1]  115 	sbc	a, (0x07, sp)
      008208 24 21            [ 1]  116 	jrnc	00108$
                           00002D   117 	Sdelay$delay_05s$21 ==.
                           00002D   118 	Sdelay$delay_05s$22 ==.
                                    119 ;	app/src/delay.c: 13: while (TIM3_GetFlagStatus(TIM3_FLAG_UPDATE) != SET)
      00820A                        120 00101$:
      00820A 4B 01            [ 1]  121 	push	#0x01
                           00002F   122 	Sdelay$delay_05s$23 ==.
      00820C 4B 00            [ 1]  123 	push	#0x00
                           000031   124 	Sdelay$delay_05s$24 ==.
      00820E CD A2 77         [ 4]  125 	call	_TIM3_GetFlagStatus
      008211 85               [ 2]  126 	popw	x
                           000035   127 	Sdelay$delay_05s$25 ==.
      008212 4A               [ 1]  128 	dec	a
      008213 26 F5            [ 1]  129 	jrne	00101$
                           000038   130 	Sdelay$delay_05s$26 ==.
                           000038   131 	Sdelay$delay_05s$27 ==.
                                    132 ;	app/src/delay.c: 15: TIM3_ClearFlag (TIM3_FLAG_UPDATE);
      008215 4B 01            [ 1]  133 	push	#0x01
                           00003A   134 	Sdelay$delay_05s$28 ==.
      008217 4B 00            [ 1]  135 	push	#0x00
                           00003C   136 	Sdelay$delay_05s$29 ==.
      008219 CD A2 C4         [ 4]  137 	call	_TIM3_ClearFlag
      00821C 85               [ 2]  138 	popw	x
                           000040   139 	Sdelay$delay_05s$30 ==.
                           000040   140 	Sdelay$delay_05s$31 ==.
                           000040   141 	Sdelay$delay_05s$32 ==.
                                    142 ;	app/src/delay.c: 12: for(uint32_t i=0;i<time_s;i++){
      00821D 1E 03            [ 2]  143 	ldw	x, (0x03, sp)
      00821F 5C               [ 1]  144 	incw	x
      008220 1F 03            [ 2]  145 	ldw	(0x03, sp), x
      008222 26 D8            [ 1]  146 	jrne	00106$
      008224 1E 01            [ 2]  147 	ldw	x, (0x01, sp)
      008226 5C               [ 1]  148 	incw	x
      008227 1F 01            [ 2]  149 	ldw	(0x01, sp), x
      008229 20 D1            [ 2]  150 	jra	00106$
                           00004E   151 	Sdelay$delay_05s$33 ==.
      00822B                        152 00108$:
                           00004E   153 	Sdelay$delay_05s$34 ==.
                                    154 ;	app/src/delay.c: 17: }
      00822B 5B 04            [ 2]  155 	addw	sp, #4
                           000050   156 	Sdelay$delay_05s$35 ==.
                           000050   157 	Sdelay$delay_05s$36 ==.
                           000050   158 	XG$delay_05s$0$0 ==.
      00822D 81               [ 4]  159 	ret
                           000051   160 	Sdelay$delay_05s$37 ==.
                                    161 	.area CODE
                                    162 	.area CONST
                                    163 	.area INITIALIZER
                                    164 	.area CABS (ABS)
                                    165 
                                    166 	.area .debug_line (NOLOAD)
      000103 00 00 00 CC            167 	.dw	0,Ldebug_line_end-Ldebug_line_start
      000107                        168 Ldebug_line_start:
      000107 00 02                  169 	.dw	2
      000109 00 00 00 70            170 	.dw	0,Ldebug_line_stmt-6-Ldebug_line_start
      00010D 01                     171 	.db	1
      00010E 01                     172 	.db	1
      00010F FB                     173 	.db	-5
      000110 0F                     174 	.db	15
      000111 0A                     175 	.db	10
      000112 00                     176 	.db	0
      000113 01                     177 	.db	1
      000114 01                     178 	.db	1
      000115 01                     179 	.db	1
      000116 01                     180 	.db	1
      000117 00                     181 	.db	0
      000118 00                     182 	.db	0
      000119 00                     183 	.db	0
      00011A 01                     184 	.db	1
      00011B 43 3A 5C 50 72 6F 67   185 	.ascii "C:\Program Files\SDCC\bin\..\include\stm8"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
             5C 73 74 6D 38
      000143 00                     186 	.db	0
      000144 43 3A 5C 50 72 6F 67   187 	.ascii "C:\Program Files\SDCC\bin\..\include"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
      000167 00                     188 	.db	0
      000168 00                     189 	.db	0
      000169 61 70 70 2F 73 72 63   190 	.ascii "app/src/delay.c"
             2F 64 65 6C 61 79 2E
             63
      000178 00                     191 	.db	0
      000179 00                     192 	.uleb128	0
      00017A 00                     193 	.uleb128	0
      00017B 00                     194 	.uleb128	0
      00017C 00                     195 	.db	0
      00017D                        196 Ldebug_line_stmt:
      00017D 00                     197 	.db	0
      00017E 05                     198 	.uleb128	5
      00017F 02                     199 	.db	2
      000180 00 00 81 DD            200 	.dw	0,(Sdelay$delay_05s_init$0)
      000184 03                     201 	.db	3
      000185 03                     202 	.sleb128	3
      000186 01                     203 	.db	1
      000187 09                     204 	.db	9
      000188 00 00                  205 	.dw	Sdelay$delay_05s_init$2-Sdelay$delay_05s_init$0
      00018A 03                     206 	.db	3
      00018B 02                     207 	.sleb128	2
      00018C 01                     208 	.db	1
      00018D 09                     209 	.db	9
      00018E 00 0B                  210 	.dw	Sdelay$delay_05s_init$7-Sdelay$delay_05s_init$2
      000190 03                     211 	.db	3
      000191 01                     212 	.sleb128	1
      000192 01                     213 	.db	1
      000193 09                     214 	.db	9
      000194 00 06                  215 	.dw	Sdelay$delay_05s_init$10-Sdelay$delay_05s_init$7
      000196 03                     216 	.db	3
      000197 02                     217 	.sleb128	2
      000198 01                     218 	.db	1
      000199 09                     219 	.db	9
      00019A 00 01                  220 	.dw	1+Sdelay$delay_05s_init$11-Sdelay$delay_05s_init$10
      00019C 00                     221 	.db	0
      00019D 01                     222 	.uleb128	1
      00019E 01                     223 	.db	1
      00019F 00                     224 	.db	0
      0001A0 05                     225 	.uleb128	5
      0001A1 02                     226 	.db	2
      0001A2 00 00 81 EF            227 	.dw	0,(Sdelay$delay_05s$13)
      0001A6 03                     228 	.db	3
      0001A7 09                     229 	.sleb128	9
      0001A8 01                     230 	.db	1
      0001A9 09                     231 	.db	9
      0001AA 00 02                  232 	.dw	Sdelay$delay_05s$16-Sdelay$delay_05s$13
      0001AC 03                     233 	.db	3
      0001AD 01                     234 	.sleb128	1
      0001AE 01                     235 	.db	1
      0001AF 09                     236 	.db	9
      0001B0 00 06                  237 	.dw	Sdelay$delay_05s$19-Sdelay$delay_05s$16
      0001B2 03                     238 	.db	3
      0001B3 01                     239 	.sleb128	1
      0001B4 01                     240 	.db	1
      0001B5 09                     241 	.db	9
      0001B6 00 13                  242 	.dw	Sdelay$delay_05s$22-Sdelay$delay_05s$19
      0001B8 03                     243 	.db	3
      0001B9 01                     244 	.sleb128	1
      0001BA 01                     245 	.db	1
      0001BB 09                     246 	.db	9
      0001BC 00 0B                  247 	.dw	Sdelay$delay_05s$27-Sdelay$delay_05s$22
      0001BE 03                     248 	.db	3
      0001BF 02                     249 	.sleb128	2
      0001C0 01                     250 	.db	1
      0001C1 09                     251 	.db	9
      0001C2 00 08                  252 	.dw	Sdelay$delay_05s$32-Sdelay$delay_05s$27
      0001C4 03                     253 	.db	3
      0001C5 7D                     254 	.sleb128	-3
      0001C6 01                     255 	.db	1
      0001C7 09                     256 	.db	9
      0001C8 00 0E                  257 	.dw	Sdelay$delay_05s$34-Sdelay$delay_05s$32
      0001CA 03                     258 	.db	3
      0001CB 05                     259 	.sleb128	5
      0001CC 01                     260 	.db	1
      0001CD 09                     261 	.db	9
      0001CE 00 03                  262 	.dw	1+Sdelay$delay_05s$36-Sdelay$delay_05s$34
      0001D0 00                     263 	.db	0
      0001D1 01                     264 	.uleb128	1
      0001D2 01                     265 	.db	1
      0001D3                        266 Ldebug_line_end:
                                    267 
                                    268 	.area .debug_loc (NOLOAD)
      000164                        269 Ldebug_loc_start:
      000164 00 00 82 2D            270 	.dw	0,(Sdelay$delay_05s$35)
      000168 00 00 82 2E            271 	.dw	0,(Sdelay$delay_05s$37)
      00016C 00 02                  272 	.dw	2
      00016E 78                     273 	.db	120
      00016F 01                     274 	.sleb128	1
      000170 00 00 82 1D            275 	.dw	0,(Sdelay$delay_05s$30)
      000174 00 00 82 2D            276 	.dw	0,(Sdelay$delay_05s$35)
      000178 00 02                  277 	.dw	2
      00017A 78                     278 	.db	120
      00017B 05                     279 	.sleb128	5
      00017C 00 00 82 19            280 	.dw	0,(Sdelay$delay_05s$29)
      000180 00 00 82 1D            281 	.dw	0,(Sdelay$delay_05s$30)
      000184 00 02                  282 	.dw	2
      000186 78                     283 	.db	120
      000187 07                     284 	.sleb128	7
      000188 00 00 82 17            285 	.dw	0,(Sdelay$delay_05s$28)
      00018C 00 00 82 19            286 	.dw	0,(Sdelay$delay_05s$29)
      000190 00 02                  287 	.dw	2
      000192 78                     288 	.db	120
      000193 06                     289 	.sleb128	6
      000194 00 00 82 15            290 	.dw	0,(Sdelay$delay_05s$26)
      000198 00 00 82 17            291 	.dw	0,(Sdelay$delay_05s$28)
      00019C 00 02                  292 	.dw	2
      00019E 78                     293 	.db	120
      00019F 05                     294 	.sleb128	5
      0001A0 00 00 82 12            295 	.dw	0,(Sdelay$delay_05s$25)
      0001A4 00 00 82 15            296 	.dw	0,(Sdelay$delay_05s$26)
      0001A8 00 02                  297 	.dw	2
      0001AA 78                     298 	.db	120
      0001AB 05                     299 	.sleb128	5
      0001AC 00 00 82 0E            300 	.dw	0,(Sdelay$delay_05s$24)
      0001B0 00 00 82 12            301 	.dw	0,(Sdelay$delay_05s$25)
      0001B4 00 02                  302 	.dw	2
      0001B6 78                     303 	.db	120
      0001B7 07                     304 	.sleb128	7
      0001B8 00 00 82 0C            305 	.dw	0,(Sdelay$delay_05s$23)
      0001BC 00 00 82 0E            306 	.dw	0,(Sdelay$delay_05s$24)
      0001C0 00 02                  307 	.dw	2
      0001C2 78                     308 	.db	120
      0001C3 06                     309 	.sleb128	6
      0001C4 00 00 81 F7            310 	.dw	0,(Sdelay$delay_05s$18)
      0001C8 00 00 82 0C            311 	.dw	0,(Sdelay$delay_05s$23)
      0001CC 00 02                  312 	.dw	2
      0001CE 78                     313 	.db	120
      0001CF 05                     314 	.sleb128	5
      0001D0 00 00 81 F3            315 	.dw	0,(Sdelay$delay_05s$17)
      0001D4 00 00 81 F7            316 	.dw	0,(Sdelay$delay_05s$18)
      0001D8 00 02                  317 	.dw	2
      0001DA 78                     318 	.db	120
      0001DB 07                     319 	.sleb128	7
      0001DC 00 00 81 F1            320 	.dw	0,(Sdelay$delay_05s$15)
      0001E0 00 00 81 F3            321 	.dw	0,(Sdelay$delay_05s$17)
      0001E4 00 02                  322 	.dw	2
      0001E6 78                     323 	.db	120
      0001E7 05                     324 	.sleb128	5
      0001E8 00 00 81 EF            325 	.dw	0,(Sdelay$delay_05s$14)
      0001EC 00 00 81 F1            326 	.dw	0,(Sdelay$delay_05s$15)
      0001F0 00 02                  327 	.dw	2
      0001F2 78                     328 	.db	120
      0001F3 01                     329 	.sleb128	1
      0001F4 00 00 00 00            330 	.dw	0,0
      0001F8 00 00 00 00            331 	.dw	0,0
      0001FC 00 00 81 EE            332 	.dw	0,(Sdelay$delay_05s_init$9)
      000200 00 00 81 EF            333 	.dw	0,(Sdelay$delay_05s_init$12)
      000204 00 02                  334 	.dw	2
      000206 78                     335 	.db	120
      000207 01                     336 	.sleb128	1
      000208 00 00 81 EA            337 	.dw	0,(Sdelay$delay_05s_init$8)
      00020C 00 00 81 EE            338 	.dw	0,(Sdelay$delay_05s_init$9)
      000210 00 02                  339 	.dw	2
      000212 78                     340 	.db	120
      000213 02                     341 	.sleb128	2
      000214 00 00 81 E8            342 	.dw	0,(Sdelay$delay_05s_init$6)
      000218 00 00 81 EA            343 	.dw	0,(Sdelay$delay_05s_init$8)
      00021C 00 02                  344 	.dw	2
      00021E 78                     345 	.db	120
      00021F 01                     346 	.sleb128	1
      000220 00 00 81 E3            347 	.dw	0,(Sdelay$delay_05s_init$5)
      000224 00 00 81 E8            348 	.dw	0,(Sdelay$delay_05s_init$6)
      000228 00 02                  349 	.dw	2
      00022A 78                     350 	.db	120
      00022B 04                     351 	.sleb128	4
      00022C 00 00 81 E1            352 	.dw	0,(Sdelay$delay_05s_init$4)
      000230 00 00 81 E3            353 	.dw	0,(Sdelay$delay_05s_init$5)
      000234 00 02                  354 	.dw	2
      000236 78                     355 	.db	120
      000237 03                     356 	.sleb128	3
      000238 00 00 81 DF            357 	.dw	0,(Sdelay$delay_05s_init$3)
      00023C 00 00 81 E1            358 	.dw	0,(Sdelay$delay_05s_init$4)
      000240 00 02                  359 	.dw	2
      000242 78                     360 	.db	120
      000243 02                     361 	.sleb128	2
      000244 00 00 81 DD            362 	.dw	0,(Sdelay$delay_05s_init$1)
      000248 00 00 81 DF            363 	.dw	0,(Sdelay$delay_05s_init$3)
      00024C 00 02                  364 	.dw	2
      00024E 78                     365 	.db	120
      00024F 01                     366 	.sleb128	1
      000250 00 00 00 00            367 	.dw	0,0
      000254 00 00 00 00            368 	.dw	0,0
                                    369 
                                    370 	.area .debug_abbrev (NOLOAD)
      000079                        371 Ldebug_abbrev:
      000079 04                     372 	.uleb128	4
      00007A 05                     373 	.uleb128	5
      00007B 00                     374 	.db	0
      00007C 02                     375 	.uleb128	2
      00007D 0A                     376 	.uleb128	10
      00007E 03                     377 	.uleb128	3
      00007F 08                     378 	.uleb128	8
      000080 49                     379 	.uleb128	73
      000081 13                     380 	.uleb128	19
      000082 00                     381 	.uleb128	0
      000083 00                     382 	.uleb128	0
      000084 03                     383 	.uleb128	3
      000085 2E                     384 	.uleb128	46
      000086 01                     385 	.db	1
      000087 01                     386 	.uleb128	1
      000088 13                     387 	.uleb128	19
      000089 03                     388 	.uleb128	3
      00008A 08                     389 	.uleb128	8
      00008B 11                     390 	.uleb128	17
      00008C 01                     391 	.uleb128	1
      00008D 12                     392 	.uleb128	18
      00008E 01                     393 	.uleb128	1
      00008F 3F                     394 	.uleb128	63
      000090 0C                     395 	.uleb128	12
      000091 40                     396 	.uleb128	64
      000092 06                     397 	.uleb128	6
      000093 00                     398 	.uleb128	0
      000094 00                     399 	.uleb128	0
      000095 07                     400 	.uleb128	7
      000096 34                     401 	.uleb128	52
      000097 00                     402 	.db	0
      000098 02                     403 	.uleb128	2
      000099 0A                     404 	.uleb128	10
      00009A 03                     405 	.uleb128	3
      00009B 08                     406 	.uleb128	8
      00009C 49                     407 	.uleb128	73
      00009D 13                     408 	.uleb128	19
      00009E 00                     409 	.uleb128	0
      00009F 00                     410 	.uleb128	0
      0000A0 01                     411 	.uleb128	1
      0000A1 11                     412 	.uleb128	17
      0000A2 01                     413 	.db	1
      0000A3 03                     414 	.uleb128	3
      0000A4 08                     415 	.uleb128	8
      0000A5 10                     416 	.uleb128	16
      0000A6 06                     417 	.uleb128	6
      0000A7 13                     418 	.uleb128	19
      0000A8 0B                     419 	.uleb128	11
      0000A9 25                     420 	.uleb128	37
      0000AA 08                     421 	.uleb128	8
      0000AB 00                     422 	.uleb128	0
      0000AC 00                     423 	.uleb128	0
      0000AD 05                     424 	.uleb128	5
      0000AE 0B                     425 	.uleb128	11
      0000AF 01                     426 	.db	1
      0000B0 11                     427 	.uleb128	17
      0000B1 01                     428 	.uleb128	1
      0000B2 12                     429 	.uleb128	18
      0000B3 01                     430 	.uleb128	1
      0000B4 00                     431 	.uleb128	0
      0000B5 00                     432 	.uleb128	0
      0000B6 06                     433 	.uleb128	6
      0000B7 0B                     434 	.uleb128	11
      0000B8 00                     435 	.db	0
      0000B9 11                     436 	.uleb128	17
      0000BA 01                     437 	.uleb128	1
      0000BB 12                     438 	.uleb128	18
      0000BC 01                     439 	.uleb128	1
      0000BD 00                     440 	.uleb128	0
      0000BE 00                     441 	.uleb128	0
      0000BF 02                     442 	.uleb128	2
      0000C0 2E                     443 	.uleb128	46
      0000C1 00                     444 	.db	0
      0000C2 03                     445 	.uleb128	3
      0000C3 08                     446 	.uleb128	8
      0000C4 11                     447 	.uleb128	17
      0000C5 01                     448 	.uleb128	1
      0000C6 12                     449 	.uleb128	18
      0000C7 01                     450 	.uleb128	1
      0000C8 3F                     451 	.uleb128	63
      0000C9 0C                     452 	.uleb128	12
      0000CA 40                     453 	.uleb128	64
      0000CB 06                     454 	.uleb128	6
      0000CC 00                     455 	.uleb128	0
      0000CD 00                     456 	.uleb128	0
      0000CE 08                     457 	.uleb128	8
      0000CF 24                     458 	.uleb128	36
      0000D0 00                     459 	.db	0
      0000D1 03                     460 	.uleb128	3
      0000D2 08                     461 	.uleb128	8
      0000D3 0B                     462 	.uleb128	11
      0000D4 0B                     463 	.uleb128	11
      0000D5 3E                     464 	.uleb128	62
      0000D6 0B                     465 	.uleb128	11
      0000D7 00                     466 	.uleb128	0
      0000D8 00                     467 	.uleb128	0
      0000D9 00                     468 	.uleb128	0
                                    469 
                                    470 	.area .debug_info (NOLOAD)
      00011B 00 00 00 B1            471 	.dw	0,Ldebug_info_end-Ldebug_info_start
      00011F                        472 Ldebug_info_start:
      00011F 00 02                  473 	.dw	2
      000121 00 00 00 79            474 	.dw	0,(Ldebug_abbrev)
      000125 04                     475 	.db	4
      000126 01                     476 	.uleb128	1
      000127 61 70 70 2F 73 72 63   477 	.ascii "app/src/delay.c"
             2F 64 65 6C 61 79 2E
             63
      000136 00                     478 	.db	0
      000137 00 00 01 03            479 	.dw	0,(Ldebug_line_start+-4)
      00013B 01                     480 	.db	1
      00013C 53 44 43 43 20 76 65   481 	.ascii "SDCC version 4.1.0 #12072"
             72 73 69 6F 6E 20 34
             2E 31 2E 30 20 23 31
             32 30 37 32
      000155 00                     482 	.db	0
      000156 02                     483 	.uleb128	2
      000157 64 65 6C 61 79 5F 30   484 	.ascii "delay_05s_init"
             35 73 5F 69 6E 69 74
      000165 00                     485 	.db	0
      000166 00 00 81 DD            486 	.dw	0,(_delay_05s_init)
      00016A 00 00 81 EF            487 	.dw	0,(XG$delay_05s_init$0$0+1)
      00016E 01                     488 	.db	1
      00016F 00 00 01 FC            489 	.dw	0,(Ldebug_loc_start+152)
      000173 03                     490 	.uleb128	3
      000174 00 00 00 A1            491 	.dw	0,161
      000178 64 65 6C 61 79 5F 30   492 	.ascii "delay_05s"
             35 73
      000181 00                     493 	.db	0
      000182 00 00 81 EF            494 	.dw	0,(_delay_05s)
      000186 00 00 82 2E            495 	.dw	0,(XG$delay_05s$0$0+1)
      00018A 01                     496 	.db	1
      00018B 00 00 01 64            497 	.dw	0,(Ldebug_loc_start)
      00018F 04                     498 	.uleb128	4
      000190 02                     499 	.db	2
      000191 91                     500 	.db	145
      000192 02                     501 	.sleb128	2
      000193 74 69 6D 65 5F 73      502 	.ascii "time_s"
      000199 00                     503 	.db	0
      00019A 00 00 00 A1            504 	.dw	0,161
      00019E 05                     505 	.uleb128	5
      00019F 00 00 81 FC            506 	.dw	0,(Sdelay$delay_05s$20)
      0001A3 00 00 82 2B            507 	.dw	0,(Sdelay$delay_05s$33)
      0001A7 06                     508 	.uleb128	6
      0001A8 00 00 82 0A            509 	.dw	0,(Sdelay$delay_05s$21)
      0001AC 00 00 82 1D            510 	.dw	0,(Sdelay$delay_05s$31)
      0001B0 07                     511 	.uleb128	7
      0001B1 02                     512 	.db	2
      0001B2 91                     513 	.db	145
      0001B3 7C                     514 	.sleb128	-4
      0001B4 69                     515 	.ascii "i"
      0001B5 00                     516 	.db	0
      0001B6 00 00 00 A1            517 	.dw	0,161
      0001BA 00                     518 	.uleb128	0
      0001BB 00                     519 	.uleb128	0
      0001BC 08                     520 	.uleb128	8
      0001BD 75 6E 73 69 67 6E 65   521 	.ascii "unsigned long"
             64 20 6C 6F 6E 67
      0001CA 00                     522 	.db	0
      0001CB 04                     523 	.db	4
      0001CC 07                     524 	.db	7
      0001CD 00                     525 	.uleb128	0
      0001CE 00                     526 	.uleb128	0
      0001CF 00                     527 	.uleb128	0
      0001D0                        528 Ldebug_info_end:
                                    529 
                                    530 	.area .debug_pubnames (NOLOAD)
      000055 00 00 00 2F            531 	.dw	0,Ldebug_pubnames_end-Ldebug_pubnames_start
      000059                        532 Ldebug_pubnames_start:
      000059 00 02                  533 	.dw	2
      00005B 00 00 01 1B            534 	.dw	0,(Ldebug_info_start-4)
      00005F 00 00 00 B5            535 	.dw	0,4+Ldebug_info_end-Ldebug_info_start
      000063 00 00 00 3B            536 	.dw	0,59
      000067 64 65 6C 61 79 5F 30   537 	.ascii "delay_05s_init"
             35 73 5F 69 6E 69 74
      000075 00                     538 	.db	0
      000076 00 00 00 58            539 	.dw	0,88
      00007A 64 65 6C 61 79 5F 30   540 	.ascii "delay_05s"
             35 73
      000083 00                     541 	.db	0
      000084 00 00 00 00            542 	.dw	0,0
      000088                        543 Ldebug_pubnames_end:
                                    544 
                                    545 	.area .debug_frame (NOLOAD)
      000145 00 00                  546 	.dw	0
      000147 00 0E                  547 	.dw	Ldebug_CIE0_end-Ldebug_CIE0_start
      000149                        548 Ldebug_CIE0_start:
      000149 FF FF                  549 	.dw	0xffff
      00014B FF FF                  550 	.dw	0xffff
      00014D 01                     551 	.db	1
      00014E 00                     552 	.db	0
      00014F 01                     553 	.uleb128	1
      000150 7F                     554 	.sleb128	-1
      000151 09                     555 	.db	9
      000152 0C                     556 	.db	12
      000153 08                     557 	.uleb128	8
      000154 02                     558 	.uleb128	2
      000155 89                     559 	.db	137
      000156 01                     560 	.uleb128	1
      000157                        561 Ldebug_CIE0_end:
      000157 00 00 00 60            562 	.dw	0,96
      00015B 00 00 01 45            563 	.dw	0,(Ldebug_CIE0_start-4)
      00015F 00 00 81 EF            564 	.dw	0,(Sdelay$delay_05s$14)	;initial loc
      000163 00 00 00 3F            565 	.dw	0,Sdelay$delay_05s$37-Sdelay$delay_05s$14
      000167 01                     566 	.db	1
      000168 00 00 81 EF            567 	.dw	0,(Sdelay$delay_05s$14)
      00016C 0E                     568 	.db	14
      00016D 02                     569 	.uleb128	2
      00016E 01                     570 	.db	1
      00016F 00 00 81 F1            571 	.dw	0,(Sdelay$delay_05s$15)
      000173 0E                     572 	.db	14
      000174 06                     573 	.uleb128	6
      000175 01                     574 	.db	1
      000176 00 00 81 F3            575 	.dw	0,(Sdelay$delay_05s$17)
      00017A 0E                     576 	.db	14
      00017B 08                     577 	.uleb128	8
      00017C 01                     578 	.db	1
      00017D 00 00 81 F7            579 	.dw	0,(Sdelay$delay_05s$18)
      000181 0E                     580 	.db	14
      000182 06                     581 	.uleb128	6
      000183 01                     582 	.db	1
      000184 00 00 82 0C            583 	.dw	0,(Sdelay$delay_05s$23)
      000188 0E                     584 	.db	14
      000189 07                     585 	.uleb128	7
      00018A 01                     586 	.db	1
      00018B 00 00 82 0E            587 	.dw	0,(Sdelay$delay_05s$24)
      00018F 0E                     588 	.db	14
      000190 08                     589 	.uleb128	8
      000191 01                     590 	.db	1
      000192 00 00 82 12            591 	.dw	0,(Sdelay$delay_05s$25)
      000196 0E                     592 	.db	14
      000197 06                     593 	.uleb128	6
      000198 01                     594 	.db	1
      000199 00 00 82 15            595 	.dw	0,(Sdelay$delay_05s$26)
      00019D 0E                     596 	.db	14
      00019E 06                     597 	.uleb128	6
      00019F 01                     598 	.db	1
      0001A0 00 00 82 17            599 	.dw	0,(Sdelay$delay_05s$28)
      0001A4 0E                     600 	.db	14
      0001A5 07                     601 	.uleb128	7
      0001A6 01                     602 	.db	1
      0001A7 00 00 82 19            603 	.dw	0,(Sdelay$delay_05s$29)
      0001AB 0E                     604 	.db	14
      0001AC 08                     605 	.uleb128	8
      0001AD 01                     606 	.db	1
      0001AE 00 00 82 1D            607 	.dw	0,(Sdelay$delay_05s$30)
      0001B2 0E                     608 	.db	14
      0001B3 06                     609 	.uleb128	6
      0001B4 01                     610 	.db	1
      0001B5 00 00 82 2D            611 	.dw	0,(Sdelay$delay_05s$35)
      0001B9 0E                     612 	.db	14
      0001BA 02                     613 	.uleb128	2
                                    614 
                                    615 	.area .debug_frame (NOLOAD)
      0001BB 00 00                  616 	.dw	0
      0001BD 00 0E                  617 	.dw	Ldebug_CIE1_end-Ldebug_CIE1_start
      0001BF                        618 Ldebug_CIE1_start:
      0001BF FF FF                  619 	.dw	0xffff
      0001C1 FF FF                  620 	.dw	0xffff
      0001C3 01                     621 	.db	1
      0001C4 00                     622 	.db	0
      0001C5 01                     623 	.uleb128	1
      0001C6 7F                     624 	.sleb128	-1
      0001C7 09                     625 	.db	9
      0001C8 0C                     626 	.db	12
      0001C9 08                     627 	.uleb128	8
      0001CA 02                     628 	.uleb128	2
      0001CB 89                     629 	.db	137
      0001CC 01                     630 	.uleb128	1
      0001CD                        631 Ldebug_CIE1_end:
      0001CD 00 00 00 3D            632 	.dw	0,61
      0001D1 00 00 01 BB            633 	.dw	0,(Ldebug_CIE1_start-4)
      0001D5 00 00 81 DD            634 	.dw	0,(Sdelay$delay_05s_init$1)	;initial loc
      0001D9 00 00 00 12            635 	.dw	0,Sdelay$delay_05s_init$12-Sdelay$delay_05s_init$1
      0001DD 01                     636 	.db	1
      0001DE 00 00 81 DD            637 	.dw	0,(Sdelay$delay_05s_init$1)
      0001E2 0E                     638 	.db	14
      0001E3 02                     639 	.uleb128	2
      0001E4 01                     640 	.db	1
      0001E5 00 00 81 DF            641 	.dw	0,(Sdelay$delay_05s_init$3)
      0001E9 0E                     642 	.db	14
      0001EA 03                     643 	.uleb128	3
      0001EB 01                     644 	.db	1
      0001EC 00 00 81 E1            645 	.dw	0,(Sdelay$delay_05s_init$4)
      0001F0 0E                     646 	.db	14
      0001F1 04                     647 	.uleb128	4
      0001F2 01                     648 	.db	1
      0001F3 00 00 81 E3            649 	.dw	0,(Sdelay$delay_05s_init$5)
      0001F7 0E                     650 	.db	14
      0001F8 05                     651 	.uleb128	5
      0001F9 01                     652 	.db	1
      0001FA 00 00 81 E8            653 	.dw	0,(Sdelay$delay_05s_init$6)
      0001FE 0E                     654 	.db	14
      0001FF 02                     655 	.uleb128	2
      000200 01                     656 	.db	1
      000201 00 00 81 EA            657 	.dw	0,(Sdelay$delay_05s_init$8)
      000205 0E                     658 	.db	14
      000206 03                     659 	.uleb128	3
      000207 01                     660 	.db	1
      000208 00 00 81 EE            661 	.dw	0,(Sdelay$delay_05s_init$9)
      00020C 0E                     662 	.db	14
      00020D 02                     663 	.uleb128	2
