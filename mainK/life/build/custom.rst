                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.1.0 #12072 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module custom
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _UART1_GetFlagStatus
                                     12 	.globl _UART1_SendData8
                                     13 	.globl _UART1_ReceiveData8
                                     14 	.globl _UART1_Init
                                     15 	.globl _pc_init
                                     16 	.globl _pc_write_char
                                     17 	.globl _pc_write_string
                                     18 	.globl _pc_read_char
                                     19 ;--------------------------------------------------------
                                     20 ; ram data
                                     21 ;--------------------------------------------------------
                                     22 	.area DATA
                                     23 ;--------------------------------------------------------
                                     24 ; ram data
                                     25 ;--------------------------------------------------------
                                     26 	.area INITIALIZED
                                     27 ;--------------------------------------------------------
                                     28 ; absolute external ram data
                                     29 ;--------------------------------------------------------
                                     30 	.area DABS (ABS)
                                     31 
                                     32 ; default segment ordering for linker
                                     33 	.area HOME
                                     34 	.area GSINIT
                                     35 	.area GSFINAL
                                     36 	.area CONST
                                     37 	.area INITIALIZER
                                     38 	.area CODE
                                     39 
                                     40 ;--------------------------------------------------------
                                     41 ; global & static initialisations
                                     42 ;--------------------------------------------------------
                                     43 	.area HOME
                                     44 	.area GSINIT
                                     45 	.area GSFINAL
                                     46 	.area GSINIT
                                     47 ;--------------------------------------------------------
                                     48 ; Home
                                     49 ;--------------------------------------------------------
                                     50 	.area HOME
                                     51 	.area HOME
                                     52 ;--------------------------------------------------------
                                     53 ; code
                                     54 ;--------------------------------------------------------
                                     55 	.area CODE
                           000000    56 	Scustom$pc_init$0 ==.
                                     57 ;	app/src/custom.c: 4: void pc_init(void)
                                     58 ;	-----------------------------------------
                                     59 ;	 function pc_init
                                     60 ;	-----------------------------------------
      008180                         61 _pc_init:
                           000000    62 	Scustom$pc_init$1 ==.
                           000000    63 	Scustom$pc_init$2 ==.
                                     64 ;	app/src/custom.c: 6: UART1_Init(9600,UART1_WORDLENGTH_8D,UART1_STOPBITS_1,UART1_PARITY_NO,UART1_SYNCMODE_CLOCK_DISABLE,UART1_MODE_TXRX_ENABLE);
      008180 4B 0C            [ 1]   65 	push	#0x0c
                           000002    66 	Scustom$pc_init$3 ==.
      008182 4B 80            [ 1]   67 	push	#0x80
                           000004    68 	Scustom$pc_init$4 ==.
      008184 4B 00            [ 1]   69 	push	#0x00
                           000006    70 	Scustom$pc_init$5 ==.
      008186 4B 00            [ 1]   71 	push	#0x00
                           000008    72 	Scustom$pc_init$6 ==.
      008188 4B 00            [ 1]   73 	push	#0x00
                           00000A    74 	Scustom$pc_init$7 ==.
      00818A 4B 80            [ 1]   75 	push	#0x80
                           00000C    76 	Scustom$pc_init$8 ==.
      00818C 4B 25            [ 1]   77 	push	#0x25
                           00000E    78 	Scustom$pc_init$9 ==.
      00818E 5F               [ 1]   79 	clrw	x
      00818F 89               [ 2]   80 	pushw	x
                           000010    81 	Scustom$pc_init$10 ==.
      008190 CD A3 E6         [ 4]   82 	call	_UART1_Init
      008193 5B 09            [ 2]   83 	addw	sp, #9
                           000015    84 	Scustom$pc_init$11 ==.
                           000015    85 	Scustom$pc_init$12 ==.
                                     86 ;	app/src/custom.c: 7: }
                           000015    87 	Scustom$pc_init$13 ==.
                           000015    88 	XG$pc_init$0$0 ==.
      008195 81               [ 4]   89 	ret
                           000016    90 	Scustom$pc_init$14 ==.
                           000016    91 	Scustom$pc_write_char$15 ==.
                                     92 ;	app/src/custom.c: 9: void pc_write_char(char c)
                                     93 ;	-----------------------------------------
                                     94 ;	 function pc_write_char
                                     95 ;	-----------------------------------------
      008196                         96 _pc_write_char:
                           000016    97 	Scustom$pc_write_char$16 ==.
                           000016    98 	Scustom$pc_write_char$17 ==.
                                     99 ;	app/src/custom.c: 11: while (!(UART1_GetFlagStatus(UART1_FLAG_TXE))){;}
      008196                        100 00101$:
      008196 4B 80            [ 1]  101 	push	#0x80
                           000018   102 	Scustom$pc_write_char$18 ==.
      008198 4B 00            [ 1]  103 	push	#0x00
                           00001A   104 	Scustom$pc_write_char$19 ==.
      00819A CD A9 29         [ 4]  105 	call	_UART1_GetFlagStatus
      00819D 85               [ 2]  106 	popw	x
                           00001E   107 	Scustom$pc_write_char$20 ==.
      00819E 4D               [ 1]  108 	tnz	a
      00819F 27 F5            [ 1]  109 	jreq	00101$
                           000021   110 	Scustom$pc_write_char$21 ==.
                                    111 ;	app/src/custom.c: 12: UART1_SendData8(c);
      0081A1 7B 03            [ 1]  112 	ld	a, (0x03, sp)
      0081A3 88               [ 1]  113 	push	a
                           000024   114 	Scustom$pc_write_char$22 ==.
      0081A4 CD A8 CC         [ 4]  115 	call	_UART1_SendData8
      0081A7 84               [ 1]  116 	pop	a
                           000028   117 	Scustom$pc_write_char$23 ==.
                           000028   118 	Scustom$pc_write_char$24 ==.
                                    119 ;	app/src/custom.c: 13: }
                           000028   120 	Scustom$pc_write_char$25 ==.
                           000028   121 	XG$pc_write_char$0$0 ==.
      0081A8 81               [ 4]  122 	ret
                           000029   123 	Scustom$pc_write_char$26 ==.
                           000029   124 	Scustom$pc_write_string$27 ==.
                           000029   125 	Scustom$pc_write_string$28 ==.
                                    126 ;	app/src/custom.c: 14: void pc_write_string(char str[])
                                    127 ;	-----------------------------------------
                                    128 ;	 function pc_write_string
                                    129 ;	-----------------------------------------
      0081A9                        130 _pc_write_string:
                           000029   131 	Scustom$pc_write_string$29 ==.
      0081A9 52 04            [ 2]  132 	sub	sp, #4
                           00002B   133 	Scustom$pc_write_string$30 ==.
                           00002B   134 	Scustom$pc_write_string$31 ==.
                                    135 ;	app/src/custom.c: 16: for(uint32_t i=0;str[i];i++)
      0081AB 90 5F            [ 1]  136 	clrw	y
      0081AD 5F               [ 1]  137 	clrw	x
      0081AE 1F 01            [ 2]  138 	ldw	(0x01, sp), x
      0081B0                        139 00103$:
      0081B0 93               [ 1]  140 	ldw	x, y
      0081B1 72 FB 07         [ 2]  141 	addw	x, (0x07, sp)
      0081B4 F6               [ 1]  142 	ld	a, (x)
      0081B5 27 14            [ 1]  143 	jreq	00105$
                           000037   144 	Scustom$pc_write_string$32 ==.
                           000037   145 	Scustom$pc_write_string$33 ==.
                                    146 ;	app/src/custom.c: 17: {pc_write_char(str[i]);}
      0081B7 90 89            [ 2]  147 	pushw	y
                           000039   148 	Scustom$pc_write_string$34 ==.
      0081B9 88               [ 1]  149 	push	a
                           00003A   150 	Scustom$pc_write_string$35 ==.
      0081BA CD 81 96         [ 4]  151 	call	_pc_write_char
      0081BD 84               [ 1]  152 	pop	a
                           00003E   153 	Scustom$pc_write_string$36 ==.
      0081BE 90 85            [ 2]  154 	popw	y
                           000040   155 	Scustom$pc_write_string$37 ==.
                           000040   156 	Scustom$pc_write_string$38 ==.
                           000040   157 	Scustom$pc_write_string$39 ==.
                                    158 ;	app/src/custom.c: 16: for(uint32_t i=0;str[i];i++)
      0081C0 90 5C            [ 1]  159 	incw	y
      0081C2 26 EC            [ 1]  160 	jrne	00103$
      0081C4 1E 01            [ 2]  161 	ldw	x, (0x01, sp)
      0081C6 5C               [ 1]  162 	incw	x
      0081C7 1F 01            [ 2]  163 	ldw	(0x01, sp), x
      0081C9 20 E5            [ 2]  164 	jra	00103$
      0081CB                        165 00105$:
                           00004B   166 	Scustom$pc_write_string$40 ==.
                                    167 ;	app/src/custom.c: 18: }
      0081CB 5B 04            [ 2]  168 	addw	sp, #4
                           00004D   169 	Scustom$pc_write_string$41 ==.
                           00004D   170 	Scustom$pc_write_string$42 ==.
                           00004D   171 	XG$pc_write_string$0$0 ==.
      0081CD 81               [ 4]  172 	ret
                           00004E   173 	Scustom$pc_write_string$43 ==.
                           00004E   174 	Scustom$pc_read_char$44 ==.
                                    175 ;	app/src/custom.c: 19: char pc_read_char(void)
                                    176 ;	-----------------------------------------
                                    177 ;	 function pc_read_char
                                    178 ;	-----------------------------------------
      0081CE                        179 _pc_read_char:
                           00004E   180 	Scustom$pc_read_char$45 ==.
                           00004E   181 	Scustom$pc_read_char$46 ==.
                                    182 ;	app/src/custom.c: 21: while (!(UART1_GetFlagStatus(UART1_FLAG_RXNE))){;}
      0081CE                        183 00101$:
      0081CE 4B 20            [ 1]  184 	push	#0x20
                           000050   185 	Scustom$pc_read_char$47 ==.
      0081D0 4B 00            [ 1]  186 	push	#0x00
                           000052   187 	Scustom$pc_read_char$48 ==.
      0081D2 CD A9 29         [ 4]  188 	call	_UART1_GetFlagStatus
      0081D5 85               [ 2]  189 	popw	x
                           000056   190 	Scustom$pc_read_char$49 ==.
      0081D6 4D               [ 1]  191 	tnz	a
      0081D7 27 F5            [ 1]  192 	jreq	00101$
                           000059   193 	Scustom$pc_read_char$50 ==.
                                    194 ;	app/src/custom.c: 22: return UART1_ReceiveData8();
      0081D9 CC A8 AD         [ 2]  195 	jp	_UART1_ReceiveData8
                           00005C   196 	Scustom$pc_read_char$51 ==.
                                    197 ;	app/src/custom.c: 23: }
                           00005C   198 	Scustom$pc_read_char$52 ==.
                           00005C   199 	XG$pc_read_char$0$0 ==.
      0081DC 81               [ 4]  200 	ret
                           00005D   201 	Scustom$pc_read_char$53 ==.
                                    202 	.area CODE
                                    203 	.area CONST
                                    204 	.area INITIALIZER
                                    205 	.area CABS (ABS)
                                    206 
                                    207 	.area .debug_line (NOLOAD)
      000000 00 00 00 FF            208 	.dw	0,Ldebug_line_end-Ldebug_line_start
      000004                        209 Ldebug_line_start:
      000004 00 02                  210 	.dw	2
      000006 00 00 00 71            211 	.dw	0,Ldebug_line_stmt-6-Ldebug_line_start
      00000A 01                     212 	.db	1
      00000B 01                     213 	.db	1
      00000C FB                     214 	.db	-5
      00000D 0F                     215 	.db	15
      00000E 0A                     216 	.db	10
      00000F 00                     217 	.db	0
      000010 01                     218 	.db	1
      000011 01                     219 	.db	1
      000012 01                     220 	.db	1
      000013 01                     221 	.db	1
      000014 00                     222 	.db	0
      000015 00                     223 	.db	0
      000016 00                     224 	.db	0
      000017 01                     225 	.db	1
      000018 43 3A 5C 50 72 6F 67   226 	.ascii "C:\Program Files\SDCC\bin\..\include\stm8"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
             5C 73 74 6D 38
      000040 00                     227 	.db	0
      000041 43 3A 5C 50 72 6F 67   228 	.ascii "C:\Program Files\SDCC\bin\..\include"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
      000064 00                     229 	.db	0
      000065 00                     230 	.db	0
      000066 61 70 70 2F 73 72 63   231 	.ascii "app/src/custom.c"
             2F 63 75 73 74 6F 6D
             2E 63
      000076 00                     232 	.db	0
      000077 00                     233 	.uleb128	0
      000078 00                     234 	.uleb128	0
      000079 00                     235 	.uleb128	0
      00007A 00                     236 	.db	0
      00007B                        237 Ldebug_line_stmt:
      00007B 00                     238 	.db	0
      00007C 05                     239 	.uleb128	5
      00007D 02                     240 	.db	2
      00007E 00 00 81 80            241 	.dw	0,(Scustom$pc_init$0)
      000082 03                     242 	.db	3
      000083 03                     243 	.sleb128	3
      000084 01                     244 	.db	1
      000085 09                     245 	.db	9
      000086 00 00                  246 	.dw	Scustom$pc_init$2-Scustom$pc_init$0
      000088 03                     247 	.db	3
      000089 02                     248 	.sleb128	2
      00008A 01                     249 	.db	1
      00008B 09                     250 	.db	9
      00008C 00 15                  251 	.dw	Scustom$pc_init$12-Scustom$pc_init$2
      00008E 03                     252 	.db	3
      00008F 01                     253 	.sleb128	1
      000090 01                     254 	.db	1
      000091 09                     255 	.db	9
      000092 00 01                  256 	.dw	1+Scustom$pc_init$13-Scustom$pc_init$12
      000094 00                     257 	.db	0
      000095 01                     258 	.uleb128	1
      000096 01                     259 	.db	1
      000097 00                     260 	.db	0
      000098 05                     261 	.uleb128	5
      000099 02                     262 	.db	2
      00009A 00 00 81 96            263 	.dw	0,(Scustom$pc_write_char$15)
      00009E 03                     264 	.db	3
      00009F 08                     265 	.sleb128	8
      0000A0 01                     266 	.db	1
      0000A1 09                     267 	.db	9
      0000A2 00 00                  268 	.dw	Scustom$pc_write_char$17-Scustom$pc_write_char$15
      0000A4 03                     269 	.db	3
      0000A5 02                     270 	.sleb128	2
      0000A6 01                     271 	.db	1
      0000A7 09                     272 	.db	9
      0000A8 00 0B                  273 	.dw	Scustom$pc_write_char$21-Scustom$pc_write_char$17
      0000AA 03                     274 	.db	3
      0000AB 01                     275 	.sleb128	1
      0000AC 01                     276 	.db	1
      0000AD 09                     277 	.db	9
      0000AE 00 07                  278 	.dw	Scustom$pc_write_char$24-Scustom$pc_write_char$21
      0000B0 03                     279 	.db	3
      0000B1 01                     280 	.sleb128	1
      0000B2 01                     281 	.db	1
      0000B3 09                     282 	.db	9
      0000B4 00 01                  283 	.dw	1+Scustom$pc_write_char$25-Scustom$pc_write_char$24
      0000B6 00                     284 	.db	0
      0000B7 01                     285 	.uleb128	1
      0000B8 01                     286 	.db	1
      0000B9 00                     287 	.db	0
      0000BA 05                     288 	.uleb128	5
      0000BB 02                     289 	.db	2
      0000BC 00 00 81 A9            290 	.dw	0,(Scustom$pc_write_string$28)
      0000C0 03                     291 	.db	3
      0000C1 0D                     292 	.sleb128	13
      0000C2 01                     293 	.db	1
      0000C3 09                     294 	.db	9
      0000C4 00 02                  295 	.dw	Scustom$pc_write_string$31-Scustom$pc_write_string$28
      0000C6 03                     296 	.db	3
      0000C7 02                     297 	.sleb128	2
      0000C8 01                     298 	.db	1
      0000C9 09                     299 	.db	9
      0000CA 00 0C                  300 	.dw	Scustom$pc_write_string$33-Scustom$pc_write_string$31
      0000CC 03                     301 	.db	3
      0000CD 01                     302 	.sleb128	1
      0000CE 01                     303 	.db	1
      0000CF 09                     304 	.db	9
      0000D0 00 09                  305 	.dw	Scustom$pc_write_string$39-Scustom$pc_write_string$33
      0000D2 03                     306 	.db	3
      0000D3 7F                     307 	.sleb128	-1
      0000D4 01                     308 	.db	1
      0000D5 09                     309 	.db	9
      0000D6 00 0B                  310 	.dw	Scustom$pc_write_string$40-Scustom$pc_write_string$39
      0000D8 03                     311 	.db	3
      0000D9 02                     312 	.sleb128	2
      0000DA 01                     313 	.db	1
      0000DB 09                     314 	.db	9
      0000DC 00 03                  315 	.dw	1+Scustom$pc_write_string$42-Scustom$pc_write_string$40
      0000DE 00                     316 	.db	0
      0000DF 01                     317 	.uleb128	1
      0000E0 01                     318 	.db	1
      0000E1 00                     319 	.db	0
      0000E2 05                     320 	.uleb128	5
      0000E3 02                     321 	.db	2
      0000E4 00 00 81 CE            322 	.dw	0,(Scustom$pc_read_char$44)
      0000E8 03                     323 	.db	3
      0000E9 12                     324 	.sleb128	18
      0000EA 01                     325 	.db	1
      0000EB 09                     326 	.db	9
      0000EC 00 00                  327 	.dw	Scustom$pc_read_char$46-Scustom$pc_read_char$44
      0000EE 03                     328 	.db	3
      0000EF 02                     329 	.sleb128	2
      0000F0 01                     330 	.db	1
      0000F1 09                     331 	.db	9
      0000F2 00 0B                  332 	.dw	Scustom$pc_read_char$50-Scustom$pc_read_char$46
      0000F4 03                     333 	.db	3
      0000F5 01                     334 	.sleb128	1
      0000F6 01                     335 	.db	1
      0000F7 09                     336 	.db	9
      0000F8 00 03                  337 	.dw	Scustom$pc_read_char$51-Scustom$pc_read_char$50
      0000FA 03                     338 	.db	3
      0000FB 01                     339 	.sleb128	1
      0000FC 01                     340 	.db	1
      0000FD 09                     341 	.db	9
      0000FE 00 01                  342 	.dw	1+Scustom$pc_read_char$52-Scustom$pc_read_char$51
      000100 00                     343 	.db	0
      000101 01                     344 	.uleb128	1
      000102 01                     345 	.db	1
      000103                        346 Ldebug_line_end:
                                    347 
                                    348 	.area .debug_loc (NOLOAD)
      000000                        349 Ldebug_loc_start:
      000000 00 00 81 D6            350 	.dw	0,(Scustom$pc_read_char$49)
      000004 00 00 81 DD            351 	.dw	0,(Scustom$pc_read_char$53)
      000008 00 02                  352 	.dw	2
      00000A 78                     353 	.db	120
      00000B 01                     354 	.sleb128	1
      00000C 00 00 81 D2            355 	.dw	0,(Scustom$pc_read_char$48)
      000010 00 00 81 D6            356 	.dw	0,(Scustom$pc_read_char$49)
      000014 00 02                  357 	.dw	2
      000016 78                     358 	.db	120
      000017 03                     359 	.sleb128	3
      000018 00 00 81 D0            360 	.dw	0,(Scustom$pc_read_char$47)
      00001C 00 00 81 D2            361 	.dw	0,(Scustom$pc_read_char$48)
      000020 00 02                  362 	.dw	2
      000022 78                     363 	.db	120
      000023 02                     364 	.sleb128	2
      000024 00 00 81 CE            365 	.dw	0,(Scustom$pc_read_char$45)
      000028 00 00 81 D0            366 	.dw	0,(Scustom$pc_read_char$47)
      00002C 00 02                  367 	.dw	2
      00002E 78                     368 	.db	120
      00002F 01                     369 	.sleb128	1
      000030 00 00 00 00            370 	.dw	0,0
      000034 00 00 00 00            371 	.dw	0,0
      000038 00 00 81 CD            372 	.dw	0,(Scustom$pc_write_string$41)
      00003C 00 00 81 CE            373 	.dw	0,(Scustom$pc_write_string$43)
      000040 00 02                  374 	.dw	2
      000042 78                     375 	.db	120
      000043 01                     376 	.sleb128	1
      000044 00 00 81 C0            377 	.dw	0,(Scustom$pc_write_string$37)
      000048 00 00 81 CD            378 	.dw	0,(Scustom$pc_write_string$41)
      00004C 00 02                  379 	.dw	2
      00004E 78                     380 	.db	120
      00004F 05                     381 	.sleb128	5
      000050 00 00 81 BE            382 	.dw	0,(Scustom$pc_write_string$36)
      000054 00 00 81 C0            383 	.dw	0,(Scustom$pc_write_string$37)
      000058 00 02                  384 	.dw	2
      00005A 78                     385 	.db	120
      00005B 07                     386 	.sleb128	7
      00005C 00 00 81 BA            387 	.dw	0,(Scustom$pc_write_string$35)
      000060 00 00 81 BE            388 	.dw	0,(Scustom$pc_write_string$36)
      000064 00 02                  389 	.dw	2
      000066 78                     390 	.db	120
      000067 08                     391 	.sleb128	8
      000068 00 00 81 B9            392 	.dw	0,(Scustom$pc_write_string$34)
      00006C 00 00 81 BA            393 	.dw	0,(Scustom$pc_write_string$35)
      000070 00 02                  394 	.dw	2
      000072 78                     395 	.db	120
      000073 07                     396 	.sleb128	7
      000074 00 00 81 AB            397 	.dw	0,(Scustom$pc_write_string$30)
      000078 00 00 81 B9            398 	.dw	0,(Scustom$pc_write_string$34)
      00007C 00 02                  399 	.dw	2
      00007E 78                     400 	.db	120
      00007F 05                     401 	.sleb128	5
      000080 00 00 81 A9            402 	.dw	0,(Scustom$pc_write_string$29)
      000084 00 00 81 AB            403 	.dw	0,(Scustom$pc_write_string$30)
      000088 00 02                  404 	.dw	2
      00008A 78                     405 	.db	120
      00008B 01                     406 	.sleb128	1
      00008C 00 00 00 00            407 	.dw	0,0
      000090 00 00 00 00            408 	.dw	0,0
      000094 00 00 81 A8            409 	.dw	0,(Scustom$pc_write_char$23)
      000098 00 00 81 A9            410 	.dw	0,(Scustom$pc_write_char$26)
      00009C 00 02                  411 	.dw	2
      00009E 78                     412 	.db	120
      00009F 01                     413 	.sleb128	1
      0000A0 00 00 81 A4            414 	.dw	0,(Scustom$pc_write_char$22)
      0000A4 00 00 81 A8            415 	.dw	0,(Scustom$pc_write_char$23)
      0000A8 00 02                  416 	.dw	2
      0000AA 78                     417 	.db	120
      0000AB 02                     418 	.sleb128	2
      0000AC 00 00 81 9E            419 	.dw	0,(Scustom$pc_write_char$20)
      0000B0 00 00 81 A4            420 	.dw	0,(Scustom$pc_write_char$22)
      0000B4 00 02                  421 	.dw	2
      0000B6 78                     422 	.db	120
      0000B7 01                     423 	.sleb128	1
      0000B8 00 00 81 9A            424 	.dw	0,(Scustom$pc_write_char$19)
      0000BC 00 00 81 9E            425 	.dw	0,(Scustom$pc_write_char$20)
      0000C0 00 02                  426 	.dw	2
      0000C2 78                     427 	.db	120
      0000C3 03                     428 	.sleb128	3
      0000C4 00 00 81 98            429 	.dw	0,(Scustom$pc_write_char$18)
      0000C8 00 00 81 9A            430 	.dw	0,(Scustom$pc_write_char$19)
      0000CC 00 02                  431 	.dw	2
      0000CE 78                     432 	.db	120
      0000CF 02                     433 	.sleb128	2
      0000D0 00 00 81 96            434 	.dw	0,(Scustom$pc_write_char$16)
      0000D4 00 00 81 98            435 	.dw	0,(Scustom$pc_write_char$18)
      0000D8 00 02                  436 	.dw	2
      0000DA 78                     437 	.db	120
      0000DB 01                     438 	.sleb128	1
      0000DC 00 00 00 00            439 	.dw	0,0
      0000E0 00 00 00 00            440 	.dw	0,0
      0000E4 00 00 81 95            441 	.dw	0,(Scustom$pc_init$11)
      0000E8 00 00 81 96            442 	.dw	0,(Scustom$pc_init$14)
      0000EC 00 02                  443 	.dw	2
      0000EE 78                     444 	.db	120
      0000EF 01                     445 	.sleb128	1
      0000F0 00 00 81 90            446 	.dw	0,(Scustom$pc_init$10)
      0000F4 00 00 81 95            447 	.dw	0,(Scustom$pc_init$11)
      0000F8 00 02                  448 	.dw	2
      0000FA 78                     449 	.db	120
      0000FB 0A                     450 	.sleb128	10
      0000FC 00 00 81 8E            451 	.dw	0,(Scustom$pc_init$9)
      000100 00 00 81 90            452 	.dw	0,(Scustom$pc_init$10)
      000104 00 02                  453 	.dw	2
      000106 78                     454 	.db	120
      000107 08                     455 	.sleb128	8
      000108 00 00 81 8C            456 	.dw	0,(Scustom$pc_init$8)
      00010C 00 00 81 8E            457 	.dw	0,(Scustom$pc_init$9)
      000110 00 02                  458 	.dw	2
      000112 78                     459 	.db	120
      000113 07                     460 	.sleb128	7
      000114 00 00 81 8A            461 	.dw	0,(Scustom$pc_init$7)
      000118 00 00 81 8C            462 	.dw	0,(Scustom$pc_init$8)
      00011C 00 02                  463 	.dw	2
      00011E 78                     464 	.db	120
      00011F 06                     465 	.sleb128	6
      000120 00 00 81 88            466 	.dw	0,(Scustom$pc_init$6)
      000124 00 00 81 8A            467 	.dw	0,(Scustom$pc_init$7)
      000128 00 02                  468 	.dw	2
      00012A 78                     469 	.db	120
      00012B 05                     470 	.sleb128	5
      00012C 00 00 81 86            471 	.dw	0,(Scustom$pc_init$5)
      000130 00 00 81 88            472 	.dw	0,(Scustom$pc_init$6)
      000134 00 02                  473 	.dw	2
      000136 78                     474 	.db	120
      000137 04                     475 	.sleb128	4
      000138 00 00 81 84            476 	.dw	0,(Scustom$pc_init$4)
      00013C 00 00 81 86            477 	.dw	0,(Scustom$pc_init$5)
      000140 00 02                  478 	.dw	2
      000142 78                     479 	.db	120
      000143 03                     480 	.sleb128	3
      000144 00 00 81 82            481 	.dw	0,(Scustom$pc_init$3)
      000148 00 00 81 84            482 	.dw	0,(Scustom$pc_init$4)
      00014C 00 02                  483 	.dw	2
      00014E 78                     484 	.db	120
      00014F 02                     485 	.sleb128	2
      000150 00 00 81 80            486 	.dw	0,(Scustom$pc_init$1)
      000154 00 00 81 82            487 	.dw	0,(Scustom$pc_init$3)
      000158 00 02                  488 	.dw	2
      00015A 78                     489 	.db	120
      00015B 01                     490 	.sleb128	1
      00015C 00 00 00 00            491 	.dw	0,0
      000160 00 00 00 00            492 	.dw	0,0
                                    493 
                                    494 	.area .debug_abbrev (NOLOAD)
      000000                        495 Ldebug_abbrev:
      000000 06                     496 	.uleb128	6
      000001 0F                     497 	.uleb128	15
      000002 00                     498 	.db	0
      000003 0B                     499 	.uleb128	11
      000004 0B                     500 	.uleb128	11
      000005 49                     501 	.uleb128	73
      000006 13                     502 	.uleb128	19
      000007 00                     503 	.uleb128	0
      000008 00                     504 	.uleb128	0
      000009 0A                     505 	.uleb128	10
      00000A 2E                     506 	.uleb128	46
      00000B 00                     507 	.db	0
      00000C 03                     508 	.uleb128	3
      00000D 08                     509 	.uleb128	8
      00000E 11                     510 	.uleb128	17
      00000F 01                     511 	.uleb128	1
      000010 12                     512 	.uleb128	18
      000011 01                     513 	.uleb128	1
      000012 3F                     514 	.uleb128	63
      000013 0C                     515 	.uleb128	12
      000014 40                     516 	.uleb128	64
      000015 06                     517 	.uleb128	6
      000016 49                     518 	.uleb128	73
      000017 13                     519 	.uleb128	19
      000018 00                     520 	.uleb128	0
      000019 00                     521 	.uleb128	0
      00001A 04                     522 	.uleb128	4
      00001B 05                     523 	.uleb128	5
      00001C 00                     524 	.db	0
      00001D 02                     525 	.uleb128	2
      00001E 0A                     526 	.uleb128	10
      00001F 03                     527 	.uleb128	3
      000020 08                     528 	.uleb128	8
      000021 49                     529 	.uleb128	73
      000022 13                     530 	.uleb128	19
      000023 00                     531 	.uleb128	0
      000024 00                     532 	.uleb128	0
      000025 03                     533 	.uleb128	3
      000026 2E                     534 	.uleb128	46
      000027 01                     535 	.db	1
      000028 01                     536 	.uleb128	1
      000029 13                     537 	.uleb128	19
      00002A 03                     538 	.uleb128	3
      00002B 08                     539 	.uleb128	8
      00002C 11                     540 	.uleb128	17
      00002D 01                     541 	.uleb128	1
      00002E 12                     542 	.uleb128	18
      00002F 01                     543 	.uleb128	1
      000030 3F                     544 	.uleb128	63
      000031 0C                     545 	.uleb128	12
      000032 40                     546 	.uleb128	64
      000033 06                     547 	.uleb128	6
      000034 00                     548 	.uleb128	0
      000035 00                     549 	.uleb128	0
      000036 09                     550 	.uleb128	9
      000037 34                     551 	.uleb128	52
      000038 00                     552 	.db	0
      000039 02                     553 	.uleb128	2
      00003A 0A                     554 	.uleb128	10
      00003B 03                     555 	.uleb128	3
      00003C 08                     556 	.uleb128	8
      00003D 49                     557 	.uleb128	73
      00003E 13                     558 	.uleb128	19
      00003F 00                     559 	.uleb128	0
      000040 00                     560 	.uleb128	0
      000041 07                     561 	.uleb128	7
      000042 0B                     562 	.uleb128	11
      000043 01                     563 	.db	1
      000044 11                     564 	.uleb128	17
      000045 01                     565 	.uleb128	1
      000046 00                     566 	.uleb128	0
      000047 00                     567 	.uleb128	0
      000048 01                     568 	.uleb128	1
      000049 11                     569 	.uleb128	17
      00004A 01                     570 	.db	1
      00004B 03                     571 	.uleb128	3
      00004C 08                     572 	.uleb128	8
      00004D 10                     573 	.uleb128	16
      00004E 06                     574 	.uleb128	6
      00004F 13                     575 	.uleb128	19
      000050 0B                     576 	.uleb128	11
      000051 25                     577 	.uleb128	37
      000052 08                     578 	.uleb128	8
      000053 00                     579 	.uleb128	0
      000054 00                     580 	.uleb128	0
      000055 08                     581 	.uleb128	8
      000056 0B                     582 	.uleb128	11
      000057 00                     583 	.db	0
      000058 11                     584 	.uleb128	17
      000059 01                     585 	.uleb128	1
      00005A 12                     586 	.uleb128	18
      00005B 01                     587 	.uleb128	1
      00005C 00                     588 	.uleb128	0
      00005D 00                     589 	.uleb128	0
      00005E 02                     590 	.uleb128	2
      00005F 2E                     591 	.uleb128	46
      000060 00                     592 	.db	0
      000061 03                     593 	.uleb128	3
      000062 08                     594 	.uleb128	8
      000063 11                     595 	.uleb128	17
      000064 01                     596 	.uleb128	1
      000065 12                     597 	.uleb128	18
      000066 01                     598 	.uleb128	1
      000067 3F                     599 	.uleb128	63
      000068 0C                     600 	.uleb128	12
      000069 40                     601 	.uleb128	64
      00006A 06                     602 	.uleb128	6
      00006B 00                     603 	.uleb128	0
      00006C 00                     604 	.uleb128	0
      00006D 05                     605 	.uleb128	5
      00006E 24                     606 	.uleb128	36
      00006F 00                     607 	.db	0
      000070 03                     608 	.uleb128	3
      000071 08                     609 	.uleb128	8
      000072 0B                     610 	.uleb128	11
      000073 0B                     611 	.uleb128	11
      000074 3E                     612 	.uleb128	62
      000075 0B                     613 	.uleb128	11
      000076 00                     614 	.uleb128	0
      000077 00                     615 	.uleb128	0
      000078 00                     616 	.uleb128	0
                                    617 
                                    618 	.area .debug_info (NOLOAD)
      000000 00 00 01 17            619 	.dw	0,Ldebug_info_end-Ldebug_info_start
      000004                        620 Ldebug_info_start:
      000004 00 02                  621 	.dw	2
      000006 00 00 00 00            622 	.dw	0,(Ldebug_abbrev)
      00000A 04                     623 	.db	4
      00000B 01                     624 	.uleb128	1
      00000C 61 70 70 2F 73 72 63   625 	.ascii "app/src/custom.c"
             2F 63 75 73 74 6F 6D
             2E 63
      00001C 00                     626 	.db	0
      00001D 00 00 00 00            627 	.dw	0,(Ldebug_line_start+-4)
      000021 01                     628 	.db	1
      000022 53 44 43 43 20 76 65   629 	.ascii "SDCC version 4.1.0 #12072"
             72 73 69 6F 6E 20 34
             2E 31 2E 30 20 23 31
             32 30 37 32
      00003B 00                     630 	.db	0
      00003C 02                     631 	.uleb128	2
      00003D 70 63 5F 69 6E 69 74   632 	.ascii "pc_init"
      000044 00                     633 	.db	0
      000045 00 00 81 80            634 	.dw	0,(_pc_init)
      000049 00 00 81 96            635 	.dw	0,(XG$pc_init$0$0+1)
      00004D 01                     636 	.db	1
      00004E 00 00 00 E4            637 	.dw	0,(Ldebug_loc_start+228)
      000052 03                     638 	.uleb128	3
      000053 00 00 00 7D            639 	.dw	0,125
      000057 70 63 5F 77 72 69 74   640 	.ascii "pc_write_char"
             65 5F 63 68 61 72
      000064 00                     641 	.db	0
      000065 00 00 81 96            642 	.dw	0,(_pc_write_char)
      000069 00 00 81 A9            643 	.dw	0,(XG$pc_write_char$0$0+1)
      00006D 01                     644 	.db	1
      00006E 00 00 00 94            645 	.dw	0,(Ldebug_loc_start+148)
      000072 04                     646 	.uleb128	4
      000073 02                     647 	.db	2
      000074 91                     648 	.db	145
      000075 02                     649 	.sleb128	2
      000076 63                     650 	.ascii "c"
      000077 00                     651 	.db	0
      000078 00 00 00 7D            652 	.dw	0,125
      00007C 00                     653 	.uleb128	0
      00007D 05                     654 	.uleb128	5
      00007E 75 6E 73 69 67 6E 65   655 	.ascii "unsigned char"
             64 20 63 68 61 72
      00008B 00                     656 	.db	0
      00008C 01                     657 	.db	1
      00008D 08                     658 	.db	8
      00008E 03                     659 	.uleb128	3
      00008F 00 00 00 E8            660 	.dw	0,232
      000093 70 63 5F 77 72 69 74   661 	.ascii "pc_write_string"
             65 5F 73 74 72 69 6E
             67
      0000A2 00                     662 	.db	0
      0000A3 00 00 81 A9            663 	.dw	0,(_pc_write_string)
      0000A7 00 00 81 CE            664 	.dw	0,(XG$pc_write_string$0$0+1)
      0000AB 01                     665 	.db	1
      0000AC 00 00 00 38            666 	.dw	0,(Ldebug_loc_start+56)
      0000B0 06                     667 	.uleb128	6
      0000B1 02                     668 	.db	2
      0000B2 00 00 00 7D            669 	.dw	0,125
      0000B6 04                     670 	.uleb128	4
      0000B7 02                     671 	.db	2
      0000B8 91                     672 	.db	145
      0000B9 02                     673 	.sleb128	2
      0000BA 73 74 72               674 	.ascii "str"
      0000BD 00                     675 	.db	0
      0000BE 00 00 00 B0            676 	.dw	0,176
      0000C2 07                     677 	.uleb128	7
      0000C3 00 00 81 A9            678 	.dw	0,(Scustom$pc_write_string$27)
      0000C7 08                     679 	.uleb128	8
      0000C8 00 00 81 B7            680 	.dw	0,(Scustom$pc_write_string$32)
      0000CC 00 00 81 C0            681 	.dw	0,(Scustom$pc_write_string$38)
      0000D0 09                     682 	.uleb128	9
      0000D1 0E                     683 	.db	14
      0000D2 91                     684 	.db	145
      0000D3 7C                     685 	.sleb128	-4
      0000D4 93                     686 	.db	147
      0000D5 01                     687 	.uleb128	1
      0000D6 91                     688 	.db	145
      0000D7 7D                     689 	.sleb128	-3
      0000D8 93                     690 	.db	147
      0000D9 01                     691 	.uleb128	1
      0000DA 54                     692 	.db	84
      0000DB 93                     693 	.db	147
      0000DC 01                     694 	.uleb128	1
      0000DD 53                     695 	.db	83
      0000DE 93                     696 	.db	147
      0000DF 01                     697 	.uleb128	1
      0000E0 69                     698 	.ascii "i"
      0000E1 00                     699 	.db	0
      0000E2 00 00 00 E8            700 	.dw	0,232
      0000E6 00                     701 	.uleb128	0
      0000E7 00                     702 	.uleb128	0
      0000E8 05                     703 	.uleb128	5
      0000E9 75 6E 73 69 67 6E 65   704 	.ascii "unsigned long"
             64 20 6C 6F 6E 67
      0000F6 00                     705 	.db	0
      0000F7 04                     706 	.db	4
      0000F8 07                     707 	.db	7
      0000F9 0A                     708 	.uleb128	10
      0000FA 70 63 5F 72 65 61 64   709 	.ascii "pc_read_char"
             5F 63 68 61 72
      000106 00                     710 	.db	0
      000107 00 00 81 CE            711 	.dw	0,(_pc_read_char)
      00010B 00 00 81 DD            712 	.dw	0,(XG$pc_read_char$0$0+1)
      00010F 01                     713 	.db	1
      000110 00 00 00 00            714 	.dw	0,(Ldebug_loc_start)
      000114 00 00 00 7D            715 	.dw	0,125
      000118 00                     716 	.uleb128	0
      000119 00                     717 	.uleb128	0
      00011A 00                     718 	.uleb128	0
      00011B                        719 Ldebug_info_end:
                                    720 
                                    721 	.area .debug_pubnames (NOLOAD)
      000000 00 00 00 51            722 	.dw	0,Ldebug_pubnames_end-Ldebug_pubnames_start
      000004                        723 Ldebug_pubnames_start:
      000004 00 02                  724 	.dw	2
      000006 00 00 00 00            725 	.dw	0,(Ldebug_info_start-4)
      00000A 00 00 01 1B            726 	.dw	0,4+Ldebug_info_end-Ldebug_info_start
      00000E 00 00 00 3C            727 	.dw	0,60
      000012 70 63 5F 69 6E 69 74   728 	.ascii "pc_init"
      000019 00                     729 	.db	0
      00001A 00 00 00 52            730 	.dw	0,82
      00001E 70 63 5F 77 72 69 74   731 	.ascii "pc_write_char"
             65 5F 63 68 61 72
      00002B 00                     732 	.db	0
      00002C 00 00 00 8E            733 	.dw	0,142
      000030 70 63 5F 77 72 69 74   734 	.ascii "pc_write_string"
             65 5F 73 74 72 69 6E
             67
      00003F 00                     735 	.db	0
      000040 00 00 00 F9            736 	.dw	0,249
      000044 70 63 5F 72 65 61 64   737 	.ascii "pc_read_char"
             5F 63 68 61 72
      000050 00                     738 	.db	0
      000051 00 00 00 00            739 	.dw	0,0
      000055                        740 Ldebug_pubnames_end:
                                    741 
                                    742 	.area .debug_frame (NOLOAD)
      000000 00 00                  743 	.dw	0
      000002 00 0E                  744 	.dw	Ldebug_CIE0_end-Ldebug_CIE0_start
      000004                        745 Ldebug_CIE0_start:
      000004 FF FF                  746 	.dw	0xffff
      000006 FF FF                  747 	.dw	0xffff
      000008 01                     748 	.db	1
      000009 00                     749 	.db	0
      00000A 01                     750 	.uleb128	1
      00000B 7F                     751 	.sleb128	-1
      00000C 09                     752 	.db	9
      00000D 0C                     753 	.db	12
      00000E 08                     754 	.uleb128	8
      00000F 02                     755 	.uleb128	2
      000010 89                     756 	.db	137
      000011 01                     757 	.uleb128	1
      000012                        758 Ldebug_CIE0_end:
      000012 00 00 00 28            759 	.dw	0,40
      000016 00 00 00 00            760 	.dw	0,(Ldebug_CIE0_start-4)
      00001A 00 00 81 CE            761 	.dw	0,(Scustom$pc_read_char$45)	;initial loc
      00001E 00 00 00 0F            762 	.dw	0,Scustom$pc_read_char$53-Scustom$pc_read_char$45
      000022 01                     763 	.db	1
      000023 00 00 81 CE            764 	.dw	0,(Scustom$pc_read_char$45)
      000027 0E                     765 	.db	14
      000028 02                     766 	.uleb128	2
      000029 01                     767 	.db	1
      00002A 00 00 81 D0            768 	.dw	0,(Scustom$pc_read_char$47)
      00002E 0E                     769 	.db	14
      00002F 03                     770 	.uleb128	3
      000030 01                     771 	.db	1
      000031 00 00 81 D2            772 	.dw	0,(Scustom$pc_read_char$48)
      000035 0E                     773 	.db	14
      000036 04                     774 	.uleb128	4
      000037 01                     775 	.db	1
      000038 00 00 81 D6            776 	.dw	0,(Scustom$pc_read_char$49)
      00003C 0E                     777 	.db	14
      00003D 02                     778 	.uleb128	2
                                    779 
                                    780 	.area .debug_frame (NOLOAD)
      00003E 00 00                  781 	.dw	0
      000040 00 0E                  782 	.dw	Ldebug_CIE1_end-Ldebug_CIE1_start
      000042                        783 Ldebug_CIE1_start:
      000042 FF FF                  784 	.dw	0xffff
      000044 FF FF                  785 	.dw	0xffff
      000046 01                     786 	.db	1
      000047 00                     787 	.db	0
      000048 01                     788 	.uleb128	1
      000049 7F                     789 	.sleb128	-1
      00004A 09                     790 	.db	9
      00004B 0C                     791 	.db	12
      00004C 08                     792 	.uleb128	8
      00004D 02                     793 	.uleb128	2
      00004E 89                     794 	.db	137
      00004F 01                     795 	.uleb128	1
      000050                        796 Ldebug_CIE1_end:
      000050 00 00 00 3D            797 	.dw	0,61
      000054 00 00 00 3E            798 	.dw	0,(Ldebug_CIE1_start-4)
      000058 00 00 81 A9            799 	.dw	0,(Scustom$pc_write_string$29)	;initial loc
      00005C 00 00 00 25            800 	.dw	0,Scustom$pc_write_string$43-Scustom$pc_write_string$29
      000060 01                     801 	.db	1
      000061 00 00 81 A9            802 	.dw	0,(Scustom$pc_write_string$29)
      000065 0E                     803 	.db	14
      000066 02                     804 	.uleb128	2
      000067 01                     805 	.db	1
      000068 00 00 81 AB            806 	.dw	0,(Scustom$pc_write_string$30)
      00006C 0E                     807 	.db	14
      00006D 06                     808 	.uleb128	6
      00006E 01                     809 	.db	1
      00006F 00 00 81 B9            810 	.dw	0,(Scustom$pc_write_string$34)
      000073 0E                     811 	.db	14
      000074 08                     812 	.uleb128	8
      000075 01                     813 	.db	1
      000076 00 00 81 BA            814 	.dw	0,(Scustom$pc_write_string$35)
      00007A 0E                     815 	.db	14
      00007B 09                     816 	.uleb128	9
      00007C 01                     817 	.db	1
      00007D 00 00 81 BE            818 	.dw	0,(Scustom$pc_write_string$36)
      000081 0E                     819 	.db	14
      000082 08                     820 	.uleb128	8
      000083 01                     821 	.db	1
      000084 00 00 81 C0            822 	.dw	0,(Scustom$pc_write_string$37)
      000088 0E                     823 	.db	14
      000089 06                     824 	.uleb128	6
      00008A 01                     825 	.db	1
      00008B 00 00 81 CD            826 	.dw	0,(Scustom$pc_write_string$41)
      00008F 0E                     827 	.db	14
      000090 02                     828 	.uleb128	2
                                    829 
                                    830 	.area .debug_frame (NOLOAD)
      000091 00 00                  831 	.dw	0
      000093 00 0E                  832 	.dw	Ldebug_CIE2_end-Ldebug_CIE2_start
      000095                        833 Ldebug_CIE2_start:
      000095 FF FF                  834 	.dw	0xffff
      000097 FF FF                  835 	.dw	0xffff
      000099 01                     836 	.db	1
      00009A 00                     837 	.db	0
      00009B 01                     838 	.uleb128	1
      00009C 7F                     839 	.sleb128	-1
      00009D 09                     840 	.db	9
      00009E 0C                     841 	.db	12
      00009F 08                     842 	.uleb128	8
      0000A0 02                     843 	.uleb128	2
      0000A1 89                     844 	.db	137
      0000A2 01                     845 	.uleb128	1
      0000A3                        846 Ldebug_CIE2_end:
      0000A3 00 00 00 36            847 	.dw	0,54
      0000A7 00 00 00 91            848 	.dw	0,(Ldebug_CIE2_start-4)
      0000AB 00 00 81 96            849 	.dw	0,(Scustom$pc_write_char$16)	;initial loc
      0000AF 00 00 00 13            850 	.dw	0,Scustom$pc_write_char$26-Scustom$pc_write_char$16
      0000B3 01                     851 	.db	1
      0000B4 00 00 81 96            852 	.dw	0,(Scustom$pc_write_char$16)
      0000B8 0E                     853 	.db	14
      0000B9 02                     854 	.uleb128	2
      0000BA 01                     855 	.db	1
      0000BB 00 00 81 98            856 	.dw	0,(Scustom$pc_write_char$18)
      0000BF 0E                     857 	.db	14
      0000C0 03                     858 	.uleb128	3
      0000C1 01                     859 	.db	1
      0000C2 00 00 81 9A            860 	.dw	0,(Scustom$pc_write_char$19)
      0000C6 0E                     861 	.db	14
      0000C7 04                     862 	.uleb128	4
      0000C8 01                     863 	.db	1
      0000C9 00 00 81 9E            864 	.dw	0,(Scustom$pc_write_char$20)
      0000CD 0E                     865 	.db	14
      0000CE 02                     866 	.uleb128	2
      0000CF 01                     867 	.db	1
      0000D0 00 00 81 A4            868 	.dw	0,(Scustom$pc_write_char$22)
      0000D4 0E                     869 	.db	14
      0000D5 03                     870 	.uleb128	3
      0000D6 01                     871 	.db	1
      0000D7 00 00 81 A8            872 	.dw	0,(Scustom$pc_write_char$23)
      0000DB 0E                     873 	.db	14
      0000DC 02                     874 	.uleb128	2
                                    875 
                                    876 	.area .debug_frame (NOLOAD)
      0000DD 00 00                  877 	.dw	0
      0000DF 00 0E                  878 	.dw	Ldebug_CIE3_end-Ldebug_CIE3_start
      0000E1                        879 Ldebug_CIE3_start:
      0000E1 FF FF                  880 	.dw	0xffff
      0000E3 FF FF                  881 	.dw	0xffff
      0000E5 01                     882 	.db	1
      0000E6 00                     883 	.db	0
      0000E7 01                     884 	.uleb128	1
      0000E8 7F                     885 	.sleb128	-1
      0000E9 09                     886 	.db	9
      0000EA 0C                     887 	.db	12
      0000EB 08                     888 	.uleb128	8
      0000EC 02                     889 	.uleb128	2
      0000ED 89                     890 	.db	137
      0000EE 01                     891 	.uleb128	1
      0000EF                        892 Ldebug_CIE3_end:
      0000EF 00 00 00 52            893 	.dw	0,82
      0000F3 00 00 00 DD            894 	.dw	0,(Ldebug_CIE3_start-4)
      0000F7 00 00 81 80            895 	.dw	0,(Scustom$pc_init$1)	;initial loc
      0000FB 00 00 00 16            896 	.dw	0,Scustom$pc_init$14-Scustom$pc_init$1
      0000FF 01                     897 	.db	1
      000100 00 00 81 80            898 	.dw	0,(Scustom$pc_init$1)
      000104 0E                     899 	.db	14
      000105 02                     900 	.uleb128	2
      000106 01                     901 	.db	1
      000107 00 00 81 82            902 	.dw	0,(Scustom$pc_init$3)
      00010B 0E                     903 	.db	14
      00010C 03                     904 	.uleb128	3
      00010D 01                     905 	.db	1
      00010E 00 00 81 84            906 	.dw	0,(Scustom$pc_init$4)
      000112 0E                     907 	.db	14
      000113 04                     908 	.uleb128	4
      000114 01                     909 	.db	1
      000115 00 00 81 86            910 	.dw	0,(Scustom$pc_init$5)
      000119 0E                     911 	.db	14
      00011A 05                     912 	.uleb128	5
      00011B 01                     913 	.db	1
      00011C 00 00 81 88            914 	.dw	0,(Scustom$pc_init$6)
      000120 0E                     915 	.db	14
      000121 06                     916 	.uleb128	6
      000122 01                     917 	.db	1
      000123 00 00 81 8A            918 	.dw	0,(Scustom$pc_init$7)
      000127 0E                     919 	.db	14
      000128 07                     920 	.uleb128	7
      000129 01                     921 	.db	1
      00012A 00 00 81 8C            922 	.dw	0,(Scustom$pc_init$8)
      00012E 0E                     923 	.db	14
      00012F 08                     924 	.uleb128	8
      000130 01                     925 	.db	1
      000131 00 00 81 8E            926 	.dw	0,(Scustom$pc_init$9)
      000135 0E                     927 	.db	14
      000136 09                     928 	.uleb128	9
      000137 01                     929 	.db	1
      000138 00 00 81 90            930 	.dw	0,(Scustom$pc_init$10)
      00013C 0E                     931 	.db	14
      00013D 0B                     932 	.uleb128	11
      00013E 01                     933 	.db	1
      00013F 00 00 81 95            934 	.dw	0,(Scustom$pc_init$11)
      000143 0E                     935 	.db	14
      000144 02                     936 	.uleb128	2
