                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.1.0 #12072 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module stm8s_uart1
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _assert_failed
                                     12 	.globl _CLK_GetClockFreq
                                     13 	.globl _UART1_DeInit
                                     14 	.globl _UART1_Init
                                     15 	.globl _UART1_Cmd
                                     16 	.globl _UART1_ITConfig
                                     17 	.globl _UART1_HalfDuplexCmd
                                     18 	.globl _UART1_IrDAConfig
                                     19 	.globl _UART1_IrDACmd
                                     20 	.globl _UART1_LINBreakDetectionConfig
                                     21 	.globl _UART1_LINCmd
                                     22 	.globl _UART1_SmartCardCmd
                                     23 	.globl _UART1_SmartCardNACKCmd
                                     24 	.globl _UART1_WakeUpConfig
                                     25 	.globl _UART1_ReceiverWakeUpCmd
                                     26 	.globl _UART1_ReceiveData8
                                     27 	.globl _UART1_ReceiveData9
                                     28 	.globl _UART1_SendData8
                                     29 	.globl _UART1_SendData9
                                     30 	.globl _UART1_SendBreak
                                     31 	.globl _UART1_SetAddress
                                     32 	.globl _UART1_SetGuardTime
                                     33 	.globl _UART1_SetPrescaler
                                     34 	.globl _UART1_GetFlagStatus
                                     35 	.globl _UART1_ClearFlag
                                     36 	.globl _UART1_GetITStatus
                                     37 	.globl _UART1_ClearITPendingBit
                                     38 ;--------------------------------------------------------
                                     39 ; ram data
                                     40 ;--------------------------------------------------------
                                     41 	.area DATA
                                     42 ;--------------------------------------------------------
                                     43 ; ram data
                                     44 ;--------------------------------------------------------
                                     45 	.area INITIALIZED
                                     46 ;--------------------------------------------------------
                                     47 ; absolute external ram data
                                     48 ;--------------------------------------------------------
                                     49 	.area DABS (ABS)
                                     50 
                                     51 ; default segment ordering for linker
                                     52 	.area HOME
                                     53 	.area GSINIT
                                     54 	.area GSFINAL
                                     55 	.area CONST
                                     56 	.area INITIALIZER
                                     57 	.area CODE
                                     58 
                                     59 ;--------------------------------------------------------
                                     60 ; global & static initialisations
                                     61 ;--------------------------------------------------------
                                     62 	.area HOME
                                     63 	.area GSINIT
                                     64 	.area GSFINAL
                                     65 	.area GSINIT
                                     66 ;--------------------------------------------------------
                                     67 ; Home
                                     68 ;--------------------------------------------------------
                                     69 	.area HOME
                                     70 	.area HOME
                                     71 ;--------------------------------------------------------
                                     72 ; code
                                     73 ;--------------------------------------------------------
                                     74 	.area CODE
                           000000    75 	Sstm8s_uart1$UART1_DeInit$0 ==.
                                     76 ;	drivers/src/stm8s_uart1.c: 53: void UART1_DeInit(void)
                                     77 ;	-----------------------------------------
                                     78 ;	 function UART1_DeInit
                                     79 ;	-----------------------------------------
      00A3BB                         80 _UART1_DeInit:
                           000000    81 	Sstm8s_uart1$UART1_DeInit$1 ==.
                           000000    82 	Sstm8s_uart1$UART1_DeInit$2 ==.
                                     83 ;	drivers/src/stm8s_uart1.c: 57: (void)UART1->SR;
      00A3BB C6 52 30         [ 1]   84 	ld	a, 0x5230
                           000003    85 	Sstm8s_uart1$UART1_DeInit$3 ==.
                                     86 ;	drivers/src/stm8s_uart1.c: 58: (void)UART1->DR;
      00A3BE C6 52 31         [ 1]   87 	ld	a, 0x5231
                           000006    88 	Sstm8s_uart1$UART1_DeInit$4 ==.
                                     89 ;	drivers/src/stm8s_uart1.c: 60: UART1->BRR2 = UART1_BRR2_RESET_VALUE;  /* Set UART1_BRR2 to reset value 0x00 */
      00A3C1 35 00 52 33      [ 1]   90 	mov	0x5233+0, #0x00
                           00000A    91 	Sstm8s_uart1$UART1_DeInit$5 ==.
                                     92 ;	drivers/src/stm8s_uart1.c: 61: UART1->BRR1 = UART1_BRR1_RESET_VALUE;  /* Set UART1_BRR1 to reset value 0x00 */
      00A3C5 35 00 52 32      [ 1]   93 	mov	0x5232+0, #0x00
                           00000E    94 	Sstm8s_uart1$UART1_DeInit$6 ==.
                                     95 ;	drivers/src/stm8s_uart1.c: 63: UART1->CR1 = UART1_CR1_RESET_VALUE;  /* Set UART1_CR1 to reset value 0x00 */
      00A3C9 35 00 52 34      [ 1]   96 	mov	0x5234+0, #0x00
                           000012    97 	Sstm8s_uart1$UART1_DeInit$7 ==.
                                     98 ;	drivers/src/stm8s_uart1.c: 64: UART1->CR2 = UART1_CR2_RESET_VALUE;  /* Set UART1_CR2 to reset value 0x00 */
      00A3CD 35 00 52 35      [ 1]   99 	mov	0x5235+0, #0x00
                           000016   100 	Sstm8s_uart1$UART1_DeInit$8 ==.
                                    101 ;	drivers/src/stm8s_uart1.c: 65: UART1->CR3 = UART1_CR3_RESET_VALUE;  /* Set UART1_CR3 to reset value 0x00 */
      00A3D1 35 00 52 36      [ 1]  102 	mov	0x5236+0, #0x00
                           00001A   103 	Sstm8s_uart1$UART1_DeInit$9 ==.
                                    104 ;	drivers/src/stm8s_uart1.c: 66: UART1->CR4 = UART1_CR4_RESET_VALUE;  /* Set UART1_CR4 to reset value 0x00 */
      00A3D5 35 00 52 37      [ 1]  105 	mov	0x5237+0, #0x00
                           00001E   106 	Sstm8s_uart1$UART1_DeInit$10 ==.
                                    107 ;	drivers/src/stm8s_uart1.c: 67: UART1->CR5 = UART1_CR5_RESET_VALUE;  /* Set UART1_CR5 to reset value 0x00 */
      00A3D9 35 00 52 38      [ 1]  108 	mov	0x5238+0, #0x00
                           000022   109 	Sstm8s_uart1$UART1_DeInit$11 ==.
                                    110 ;	drivers/src/stm8s_uart1.c: 69: UART1->GTR = UART1_GTR_RESET_VALUE;
      00A3DD 35 00 52 39      [ 1]  111 	mov	0x5239+0, #0x00
                           000026   112 	Sstm8s_uart1$UART1_DeInit$12 ==.
                                    113 ;	drivers/src/stm8s_uart1.c: 70: UART1->PSCR = UART1_PSCR_RESET_VALUE;
      00A3E1 35 00 52 3A      [ 1]  114 	mov	0x523a+0, #0x00
                           00002A   115 	Sstm8s_uart1$UART1_DeInit$13 ==.
                                    116 ;	drivers/src/stm8s_uart1.c: 71: }
                           00002A   117 	Sstm8s_uart1$UART1_DeInit$14 ==.
                           00002A   118 	XG$UART1_DeInit$0$0 ==.
      00A3E5 81               [ 4]  119 	ret
                           00002B   120 	Sstm8s_uart1$UART1_DeInit$15 ==.
                           00002B   121 	Sstm8s_uart1$UART1_Init$16 ==.
                                    122 ;	drivers/src/stm8s_uart1.c: 90: void UART1_Init(uint32_t BaudRate, UART1_WordLength_TypeDef WordLength, 
                                    123 ;	-----------------------------------------
                                    124 ;	 function UART1_Init
                                    125 ;	-----------------------------------------
      00A3E6                        126 _UART1_Init:
                           00002B   127 	Sstm8s_uart1$UART1_Init$17 ==.
      00A3E6 52 0D            [ 2]  128 	sub	sp, #13
                           00002D   129 	Sstm8s_uart1$UART1_Init$18 ==.
                           00002D   130 	Sstm8s_uart1$UART1_Init$19 ==.
                                    131 ;	drivers/src/stm8s_uart1.c: 97: assert_param(IS_UART1_BAUDRATE_OK(BaudRate));
      00A3E8 AE 89 68         [ 2]  132 	ldw	x, #0x8968
      00A3EB 13 12            [ 2]  133 	cpw	x, (0x12, sp)
      00A3ED A6 09            [ 1]  134 	ld	a, #0x09
      00A3EF 12 11            [ 1]  135 	sbc	a, (0x11, sp)
      00A3F1 4F               [ 1]  136 	clr	a
      00A3F2 12 10            [ 1]  137 	sbc	a, (0x10, sp)
      00A3F4 24 0F            [ 1]  138 	jrnc	00113$
      00A3F6 4B 61            [ 1]  139 	push	#0x61
                           00003D   140 	Sstm8s_uart1$UART1_Init$20 ==.
      00A3F8 5F               [ 1]  141 	clrw	x
      00A3F9 89               [ 2]  142 	pushw	x
                           00003F   143 	Sstm8s_uart1$UART1_Init$21 ==.
      00A3FA 4B 00            [ 1]  144 	push	#0x00
                           000041   145 	Sstm8s_uart1$UART1_Init$22 ==.
      00A3FC 4B 5E            [ 1]  146 	push	#<(___str_0+0)
                           000043   147 	Sstm8s_uart1$UART1_Init$23 ==.
      00A3FE 4B 81            [ 1]  148 	push	#((___str_0+0) >> 8)
                           000045   149 	Sstm8s_uart1$UART1_Init$24 ==.
      00A400 CD 84 F3         [ 4]  150 	call	_assert_failed
      00A403 5B 06            [ 2]  151 	addw	sp, #6
                           00004A   152 	Sstm8s_uart1$UART1_Init$25 ==.
      00A405                        153 00113$:
                           00004A   154 	Sstm8s_uart1$UART1_Init$26 ==.
                                    155 ;	drivers/src/stm8s_uart1.c: 98: assert_param(IS_UART1_WORDLENGTH_OK(WordLength));
      00A405 0D 14            [ 1]  156 	tnz	(0x14, sp)
      00A407 27 15            [ 1]  157 	jreq	00115$
      00A409 7B 14            [ 1]  158 	ld	a, (0x14, sp)
      00A40B A1 10            [ 1]  159 	cp	a, #0x10
      00A40D 27 0F            [ 1]  160 	jreq	00115$
                           000054   161 	Sstm8s_uart1$UART1_Init$27 ==.
      00A40F 4B 62            [ 1]  162 	push	#0x62
                           000056   163 	Sstm8s_uart1$UART1_Init$28 ==.
      00A411 5F               [ 1]  164 	clrw	x
      00A412 89               [ 2]  165 	pushw	x
                           000058   166 	Sstm8s_uart1$UART1_Init$29 ==.
      00A413 4B 00            [ 1]  167 	push	#0x00
                           00005A   168 	Sstm8s_uart1$UART1_Init$30 ==.
      00A415 4B 5E            [ 1]  169 	push	#<(___str_0+0)
                           00005C   170 	Sstm8s_uart1$UART1_Init$31 ==.
      00A417 4B 81            [ 1]  171 	push	#((___str_0+0) >> 8)
                           00005E   172 	Sstm8s_uart1$UART1_Init$32 ==.
      00A419 CD 84 F3         [ 4]  173 	call	_assert_failed
      00A41C 5B 06            [ 2]  174 	addw	sp, #6
                           000063   175 	Sstm8s_uart1$UART1_Init$33 ==.
      00A41E                        176 00115$:
                           000063   177 	Sstm8s_uart1$UART1_Init$34 ==.
                                    178 ;	drivers/src/stm8s_uart1.c: 99: assert_param(IS_UART1_STOPBITS_OK(StopBits));
      00A41E 0D 15            [ 1]  179 	tnz	(0x15, sp)
      00A420 27 21            [ 1]  180 	jreq	00120$
      00A422 7B 15            [ 1]  181 	ld	a, (0x15, sp)
      00A424 A1 10            [ 1]  182 	cp	a, #0x10
      00A426 27 1B            [ 1]  183 	jreq	00120$
                           00006D   184 	Sstm8s_uart1$UART1_Init$35 ==.
      00A428 7B 15            [ 1]  185 	ld	a, (0x15, sp)
      00A42A A1 20            [ 1]  186 	cp	a, #0x20
      00A42C 27 15            [ 1]  187 	jreq	00120$
                           000073   188 	Sstm8s_uart1$UART1_Init$36 ==.
      00A42E 7B 15            [ 1]  189 	ld	a, (0x15, sp)
      00A430 A1 30            [ 1]  190 	cp	a, #0x30
      00A432 27 0F            [ 1]  191 	jreq	00120$
                           000079   192 	Sstm8s_uart1$UART1_Init$37 ==.
      00A434 4B 63            [ 1]  193 	push	#0x63
                           00007B   194 	Sstm8s_uart1$UART1_Init$38 ==.
      00A436 5F               [ 1]  195 	clrw	x
      00A437 89               [ 2]  196 	pushw	x
                           00007D   197 	Sstm8s_uart1$UART1_Init$39 ==.
      00A438 4B 00            [ 1]  198 	push	#0x00
                           00007F   199 	Sstm8s_uart1$UART1_Init$40 ==.
      00A43A 4B 5E            [ 1]  200 	push	#<(___str_0+0)
                           000081   201 	Sstm8s_uart1$UART1_Init$41 ==.
      00A43C 4B 81            [ 1]  202 	push	#((___str_0+0) >> 8)
                           000083   203 	Sstm8s_uart1$UART1_Init$42 ==.
      00A43E CD 84 F3         [ 4]  204 	call	_assert_failed
      00A441 5B 06            [ 2]  205 	addw	sp, #6
                           000088   206 	Sstm8s_uart1$UART1_Init$43 ==.
      00A443                        207 00120$:
                           000088   208 	Sstm8s_uart1$UART1_Init$44 ==.
                                    209 ;	drivers/src/stm8s_uart1.c: 100: assert_param(IS_UART1_PARITY_OK(Parity));
      00A443 0D 16            [ 1]  210 	tnz	(0x16, sp)
      00A445 27 1B            [ 1]  211 	jreq	00131$
      00A447 7B 16            [ 1]  212 	ld	a, (0x16, sp)
      00A449 A1 04            [ 1]  213 	cp	a, #0x04
      00A44B 27 15            [ 1]  214 	jreq	00131$
                           000092   215 	Sstm8s_uart1$UART1_Init$45 ==.
      00A44D 7B 16            [ 1]  216 	ld	a, (0x16, sp)
      00A44F A1 06            [ 1]  217 	cp	a, #0x06
      00A451 27 0F            [ 1]  218 	jreq	00131$
                           000098   219 	Sstm8s_uart1$UART1_Init$46 ==.
      00A453 4B 64            [ 1]  220 	push	#0x64
                           00009A   221 	Sstm8s_uart1$UART1_Init$47 ==.
      00A455 5F               [ 1]  222 	clrw	x
      00A456 89               [ 2]  223 	pushw	x
                           00009C   224 	Sstm8s_uart1$UART1_Init$48 ==.
      00A457 4B 00            [ 1]  225 	push	#0x00
                           00009E   226 	Sstm8s_uart1$UART1_Init$49 ==.
      00A459 4B 5E            [ 1]  227 	push	#<(___str_0+0)
                           0000A0   228 	Sstm8s_uart1$UART1_Init$50 ==.
      00A45B 4B 81            [ 1]  229 	push	#((___str_0+0) >> 8)
                           0000A2   230 	Sstm8s_uart1$UART1_Init$51 ==.
      00A45D CD 84 F3         [ 4]  231 	call	_assert_failed
      00A460 5B 06            [ 2]  232 	addw	sp, #6
                           0000A7   233 	Sstm8s_uart1$UART1_Init$52 ==.
      00A462                        234 00131$:
                           0000A7   235 	Sstm8s_uart1$UART1_Init$53 ==.
                                    236 ;	drivers/src/stm8s_uart1.c: 101: assert_param(IS_UART1_MODE_OK((uint8_t)Mode));
      00A462 7B 18            [ 1]  237 	ld	a, (0x18, sp)
      00A464 A1 08            [ 1]  238 	cp	a, #0x08
      00A466 26 03            [ 1]  239 	jrne	00327$
      00A468 CC A4 B0         [ 2]  240 	jp	00139$
      00A46B                        241 00327$:
                           0000B0   242 	Sstm8s_uart1$UART1_Init$54 ==.
      00A46B 7B 18            [ 1]  243 	ld	a, (0x18, sp)
      00A46D A1 40            [ 1]  244 	cp	a, #0x40
      00A46F 26 03            [ 1]  245 	jrne	00330$
      00A471 CC A4 B0         [ 2]  246 	jp	00139$
      00A474                        247 00330$:
                           0000B9   248 	Sstm8s_uart1$UART1_Init$55 ==.
      00A474 7B 18            [ 1]  249 	ld	a, (0x18, sp)
      00A476 A1 04            [ 1]  250 	cp	a, #0x04
      00A478 27 36            [ 1]  251 	jreq	00139$
                           0000BF   252 	Sstm8s_uart1$UART1_Init$56 ==.
      00A47A 7B 18            [ 1]  253 	ld	a, (0x18, sp)
      00A47C A1 80            [ 1]  254 	cp	a, #0x80
      00A47E 27 30            [ 1]  255 	jreq	00139$
                           0000C5   256 	Sstm8s_uart1$UART1_Init$57 ==.
      00A480 7B 18            [ 1]  257 	ld	a, (0x18, sp)
      00A482 A0 0C            [ 1]  258 	sub	a, #0x0c
      00A484 26 02            [ 1]  259 	jrne	00339$
      00A486 4C               [ 1]  260 	inc	a
      00A487 21                     261 	.byte 0x21
      00A488                        262 00339$:
      00A488 4F               [ 1]  263 	clr	a
      00A489                        264 00340$:
                           0000CE   265 	Sstm8s_uart1$UART1_Init$58 ==.
      00A489 4D               [ 1]  266 	tnz	a
      00A48A 26 24            [ 1]  267 	jrne	00139$
      00A48C 4D               [ 1]  268 	tnz	a
      00A48D 26 21            [ 1]  269 	jrne	00139$
      00A48F 7B 18            [ 1]  270 	ld	a, (0x18, sp)
      00A491 A1 44            [ 1]  271 	cp	a, #0x44
      00A493 27 1B            [ 1]  272 	jreq	00139$
                           0000DA   273 	Sstm8s_uart1$UART1_Init$59 ==.
      00A495 7B 18            [ 1]  274 	ld	a, (0x18, sp)
      00A497 A1 C0            [ 1]  275 	cp	a, #0xc0
      00A499 27 15            [ 1]  276 	jreq	00139$
                           0000E0   277 	Sstm8s_uart1$UART1_Init$60 ==.
      00A49B 7B 18            [ 1]  278 	ld	a, (0x18, sp)
      00A49D A1 88            [ 1]  279 	cp	a, #0x88
      00A49F 27 0F            [ 1]  280 	jreq	00139$
                           0000E6   281 	Sstm8s_uart1$UART1_Init$61 ==.
      00A4A1 4B 65            [ 1]  282 	push	#0x65
                           0000E8   283 	Sstm8s_uart1$UART1_Init$62 ==.
      00A4A3 5F               [ 1]  284 	clrw	x
      00A4A4 89               [ 2]  285 	pushw	x
                           0000EA   286 	Sstm8s_uart1$UART1_Init$63 ==.
      00A4A5 4B 00            [ 1]  287 	push	#0x00
                           0000EC   288 	Sstm8s_uart1$UART1_Init$64 ==.
      00A4A7 4B 5E            [ 1]  289 	push	#<(___str_0+0)
                           0000EE   290 	Sstm8s_uart1$UART1_Init$65 ==.
      00A4A9 4B 81            [ 1]  291 	push	#((___str_0+0) >> 8)
                           0000F0   292 	Sstm8s_uart1$UART1_Init$66 ==.
      00A4AB CD 84 F3         [ 4]  293 	call	_assert_failed
      00A4AE 5B 06            [ 2]  294 	addw	sp, #6
                           0000F5   295 	Sstm8s_uart1$UART1_Init$67 ==.
      00A4B0                        296 00139$:
                           0000F5   297 	Sstm8s_uart1$UART1_Init$68 ==.
                                    298 ;	drivers/src/stm8s_uart1.c: 102: assert_param(IS_UART1_SYNCMODE_OK((uint8_t)SyncMode));
      00A4B0 7B 17            [ 1]  299 	ld	a, (0x17, sp)
      00A4B2 A4 88            [ 1]  300 	and	a, #0x88
      00A4B4 A1 88            [ 1]  301 	cp	a, #0x88
      00A4B6 27 18            [ 1]  302 	jreq	00167$
                           0000FD   303 	Sstm8s_uart1$UART1_Init$69 ==.
      00A4B8 7B 17            [ 1]  304 	ld	a, (0x17, sp)
      00A4BA A4 44            [ 1]  305 	and	a, #0x44
      00A4BC A1 44            [ 1]  306 	cp	a, #0x44
      00A4BE 27 10            [ 1]  307 	jreq	00167$
                           000105   308 	Sstm8s_uart1$UART1_Init$70 ==.
      00A4C0 7B 17            [ 1]  309 	ld	a, (0x17, sp)
      00A4C2 A4 22            [ 1]  310 	and	a, #0x22
      00A4C4 A1 22            [ 1]  311 	cp	a, #0x22
      00A4C6 27 08            [ 1]  312 	jreq	00167$
                           00010D   313 	Sstm8s_uart1$UART1_Init$71 ==.
      00A4C8 7B 17            [ 1]  314 	ld	a, (0x17, sp)
      00A4CA A4 11            [ 1]  315 	and	a, #0x11
      00A4CC A1 11            [ 1]  316 	cp	a, #0x11
      00A4CE 26 0F            [ 1]  317 	jrne	00165$
                           000115   318 	Sstm8s_uart1$UART1_Init$72 ==.
      00A4D0                        319 00167$:
      00A4D0 4B 66            [ 1]  320 	push	#0x66
                           000117   321 	Sstm8s_uart1$UART1_Init$73 ==.
      00A4D2 5F               [ 1]  322 	clrw	x
      00A4D3 89               [ 2]  323 	pushw	x
                           000119   324 	Sstm8s_uart1$UART1_Init$74 ==.
      00A4D4 4B 00            [ 1]  325 	push	#0x00
                           00011B   326 	Sstm8s_uart1$UART1_Init$75 ==.
      00A4D6 4B 5E            [ 1]  327 	push	#<(___str_0+0)
                           00011D   328 	Sstm8s_uart1$UART1_Init$76 ==.
      00A4D8 4B 81            [ 1]  329 	push	#((___str_0+0) >> 8)
                           00011F   330 	Sstm8s_uart1$UART1_Init$77 ==.
      00A4DA CD 84 F3         [ 4]  331 	call	_assert_failed
      00A4DD 5B 06            [ 2]  332 	addw	sp, #6
                           000124   333 	Sstm8s_uart1$UART1_Init$78 ==.
      00A4DF                        334 00165$:
                           000124   335 	Sstm8s_uart1$UART1_Init$79 ==.
                                    336 ;	drivers/src/stm8s_uart1.c: 105: UART1->CR1 &= (uint8_t)(~UART1_CR1_M);  
      00A4DF 72 19 52 34      [ 1]  337 	bres	21044, #4
                           000128   338 	Sstm8s_uart1$UART1_Init$80 ==.
                                    339 ;	drivers/src/stm8s_uart1.c: 108: UART1->CR1 |= (uint8_t)WordLength;
      00A4E3 C6 52 34         [ 1]  340 	ld	a, 0x5234
      00A4E6 1A 14            [ 1]  341 	or	a, (0x14, sp)
      00A4E8 C7 52 34         [ 1]  342 	ld	0x5234, a
                           000130   343 	Sstm8s_uart1$UART1_Init$81 ==.
                                    344 ;	drivers/src/stm8s_uart1.c: 111: UART1->CR3 &= (uint8_t)(~UART1_CR3_STOP);  
      00A4EB C6 52 36         [ 1]  345 	ld	a, 0x5236
      00A4EE A4 CF            [ 1]  346 	and	a, #0xcf
      00A4F0 C7 52 36         [ 1]  347 	ld	0x5236, a
                           000138   348 	Sstm8s_uart1$UART1_Init$82 ==.
                                    349 ;	drivers/src/stm8s_uart1.c: 113: UART1->CR3 |= (uint8_t)StopBits;  
      00A4F3 C6 52 36         [ 1]  350 	ld	a, 0x5236
      00A4F6 1A 15            [ 1]  351 	or	a, (0x15, sp)
      00A4F8 C7 52 36         [ 1]  352 	ld	0x5236, a
                           000140   353 	Sstm8s_uart1$UART1_Init$83 ==.
                                    354 ;	drivers/src/stm8s_uart1.c: 116: UART1->CR1 &= (uint8_t)(~(UART1_CR1_PCEN | UART1_CR1_PS  ));  
      00A4FB C6 52 34         [ 1]  355 	ld	a, 0x5234
      00A4FE A4 F9            [ 1]  356 	and	a, #0xf9
      00A500 C7 52 34         [ 1]  357 	ld	0x5234, a
                           000148   358 	Sstm8s_uart1$UART1_Init$84 ==.
                                    359 ;	drivers/src/stm8s_uart1.c: 118: UART1->CR1 |= (uint8_t)Parity;  
      00A503 C6 52 34         [ 1]  360 	ld	a, 0x5234
      00A506 1A 16            [ 1]  361 	or	a, (0x16, sp)
      00A508 C7 52 34         [ 1]  362 	ld	0x5234, a
                           000150   363 	Sstm8s_uart1$UART1_Init$85 ==.
                                    364 ;	drivers/src/stm8s_uart1.c: 121: UART1->BRR1 &= (uint8_t)(~UART1_BRR1_DIVM);  
      00A50B C6 52 32         [ 1]  365 	ld	a, 0x5232
      00A50E 35 00 52 32      [ 1]  366 	mov	0x5232+0, #0x00
                           000157   367 	Sstm8s_uart1$UART1_Init$86 ==.
                                    368 ;	drivers/src/stm8s_uart1.c: 123: UART1->BRR2 &= (uint8_t)(~UART1_BRR2_DIVM);  
      00A512 C6 52 33         [ 1]  369 	ld	a, 0x5233
      00A515 A4 0F            [ 1]  370 	and	a, #0x0f
      00A517 C7 52 33         [ 1]  371 	ld	0x5233, a
                           00015F   372 	Sstm8s_uart1$UART1_Init$87 ==.
                                    373 ;	drivers/src/stm8s_uart1.c: 125: UART1->BRR2 &= (uint8_t)(~UART1_BRR2_DIVF);  
      00A51A C6 52 33         [ 1]  374 	ld	a, 0x5233
      00A51D A4 F0            [ 1]  375 	and	a, #0xf0
      00A51F C7 52 33         [ 1]  376 	ld	0x5233, a
                           000167   377 	Sstm8s_uart1$UART1_Init$88 ==.
                                    378 ;	drivers/src/stm8s_uart1.c: 128: BaudRate_Mantissa    = ((uint32_t)CLK_GetClockFreq() / (BaudRate << 4));
      00A522 CD 8A 65         [ 4]  379 	call	_CLK_GetClockFreq
      00A525 1F 0C            [ 2]  380 	ldw	(0x0c, sp), x
      00A527 1E 10            [ 2]  381 	ldw	x, (0x10, sp)
      00A529 1F 06            [ 2]  382 	ldw	(0x06, sp), x
      00A52B 1E 12            [ 2]  383 	ldw	x, (0x12, sp)
      00A52D A6 04            [ 1]  384 	ld	a, #0x04
      00A52F                        385 00364$:
      00A52F 58               [ 2]  386 	sllw	x
      00A530 09 07            [ 1]  387 	rlc	(0x07, sp)
      00A532 09 06            [ 1]  388 	rlc	(0x06, sp)
      00A534 4A               [ 1]  389 	dec	a
      00A535 26 F8            [ 1]  390 	jrne	00364$
      00A537 1F 08            [ 2]  391 	ldw	(0x08, sp), x
      00A539 89               [ 2]  392 	pushw	x
                           00017F   393 	Sstm8s_uart1$UART1_Init$89 ==.
      00A53A 1E 08            [ 2]  394 	ldw	x, (0x08, sp)
      00A53C 89               [ 2]  395 	pushw	x
                           000182   396 	Sstm8s_uart1$UART1_Init$90 ==.
      00A53D 1E 10            [ 2]  397 	ldw	x, (0x10, sp)
      00A53F 89               [ 2]  398 	pushw	x
                           000185   399 	Sstm8s_uart1$UART1_Init$91 ==.
      00A540 90 89            [ 2]  400 	pushw	y
                           000187   401 	Sstm8s_uart1$UART1_Init$92 ==.
      00A542 CD AB 55         [ 4]  402 	call	__divulong
      00A545 5B 08            [ 2]  403 	addw	sp, #8
                           00018C   404 	Sstm8s_uart1$UART1_Init$93 ==.
      00A547 1F 03            [ 2]  405 	ldw	(0x03, sp), x
      00A549 17 01            [ 2]  406 	ldw	(0x01, sp), y
                           000190   407 	Sstm8s_uart1$UART1_Init$94 ==.
                                    408 ;	drivers/src/stm8s_uart1.c: 129: BaudRate_Mantissa100 = (((uint32_t)CLK_GetClockFreq() * 100) / (BaudRate << 4));
      00A54B CD 8A 65         [ 4]  409 	call	_CLK_GetClockFreq
      00A54E 89               [ 2]  410 	pushw	x
                           000194   411 	Sstm8s_uart1$UART1_Init$95 ==.
      00A54F 90 89            [ 2]  412 	pushw	y
                           000196   413 	Sstm8s_uart1$UART1_Init$96 ==.
      00A551 4B 64            [ 1]  414 	push	#0x64
                           000198   415 	Sstm8s_uart1$UART1_Init$97 ==.
      00A553 5F               [ 1]  416 	clrw	x
      00A554 89               [ 2]  417 	pushw	x
                           00019A   418 	Sstm8s_uart1$UART1_Init$98 ==.
      00A555 4B 00            [ 1]  419 	push	#0x00
                           00019C   420 	Sstm8s_uart1$UART1_Init$99 ==.
      00A557 CD AB AF         [ 4]  421 	call	__mullong
      00A55A 5B 08            [ 2]  422 	addw	sp, #8
                           0001A1   423 	Sstm8s_uart1$UART1_Init$100 ==.
      00A55C 1F 0C            [ 2]  424 	ldw	(0x0c, sp), x
      00A55E 1E 08            [ 2]  425 	ldw	x, (0x08, sp)
      00A560 89               [ 2]  426 	pushw	x
                           0001A6   427 	Sstm8s_uart1$UART1_Init$101 ==.
      00A561 1E 08            [ 2]  428 	ldw	x, (0x08, sp)
      00A563 89               [ 2]  429 	pushw	x
                           0001A9   430 	Sstm8s_uart1$UART1_Init$102 ==.
      00A564 1E 10            [ 2]  431 	ldw	x, (0x10, sp)
      00A566 89               [ 2]  432 	pushw	x
                           0001AC   433 	Sstm8s_uart1$UART1_Init$103 ==.
      00A567 90 89            [ 2]  434 	pushw	y
                           0001AE   435 	Sstm8s_uart1$UART1_Init$104 ==.
      00A569 CD AB 55         [ 4]  436 	call	__divulong
      00A56C 5B 08            [ 2]  437 	addw	sp, #8
                           0001B3   438 	Sstm8s_uart1$UART1_Init$105 ==.
      00A56E 90 9E            [ 1]  439 	ld	a, yh
      00A570 1F 07            [ 2]  440 	ldw	(0x07, sp), x
      00A572 6B 05            [ 1]  441 	ld	(0x05, sp), a
      00A574 90 9F            [ 1]  442 	ld	a, yl
                           0001BB   443 	Sstm8s_uart1$UART1_Init$106 ==.
                                    444 ;	drivers/src/stm8s_uart1.c: 131: UART1->BRR2 |= (uint8_t)((uint8_t)(((BaudRate_Mantissa100 - (BaudRate_Mantissa * 100)) << 4) / 100) & (uint8_t)0x0F); 
      00A576 AE 52 33         [ 2]  445 	ldw	x, #0x5233
      00A579 88               [ 1]  446 	push	a
                           0001BF   447 	Sstm8s_uart1$UART1_Init$107 ==.
      00A57A F6               [ 1]  448 	ld	a, (x)
      00A57B 6B 0A            [ 1]  449 	ld	(0x0a, sp), a
      00A57D 84               [ 1]  450 	pop	a
                           0001C3   451 	Sstm8s_uart1$UART1_Init$108 ==.
      00A57E 88               [ 1]  452 	push	a
                           0001C4   453 	Sstm8s_uart1$UART1_Init$109 ==.
      00A57F 1E 04            [ 2]  454 	ldw	x, (0x04, sp)
      00A581 89               [ 2]  455 	pushw	x
                           0001C7   456 	Sstm8s_uart1$UART1_Init$110 ==.
      00A582 1E 04            [ 2]  457 	ldw	x, (0x04, sp)
      00A584 89               [ 2]  458 	pushw	x
                           0001CA   459 	Sstm8s_uart1$UART1_Init$111 ==.
      00A585 4B 64            [ 1]  460 	push	#0x64
                           0001CC   461 	Sstm8s_uart1$UART1_Init$112 ==.
      00A587 5F               [ 1]  462 	clrw	x
      00A588 89               [ 2]  463 	pushw	x
                           0001CE   464 	Sstm8s_uart1$UART1_Init$113 ==.
      00A589 4B 00            [ 1]  465 	push	#0x00
                           0001D0   466 	Sstm8s_uart1$UART1_Init$114 ==.
      00A58B CD AB AF         [ 4]  467 	call	__mullong
      00A58E 5B 08            [ 2]  468 	addw	sp, #8
                           0001D5   469 	Sstm8s_uart1$UART1_Init$115 ==.
      00A590 1F 0D            [ 2]  470 	ldw	(0x0d, sp), x
      00A592 17 0B            [ 2]  471 	ldw	(0x0b, sp), y
      00A594 84               [ 1]  472 	pop	a
                           0001DA   473 	Sstm8s_uart1$UART1_Init$116 ==.
      00A595 16 07            [ 2]  474 	ldw	y, (0x07, sp)
      00A597 72 F2 0C         [ 2]  475 	subw	y, (0x0c, sp)
      00A59A 12 0B            [ 1]  476 	sbc	a, (0x0b, sp)
      00A59C 97               [ 1]  477 	ld	xl, a
      00A59D 7B 05            [ 1]  478 	ld	a, (0x05, sp)
      00A59F 12 0A            [ 1]  479 	sbc	a, (0x0a, sp)
      00A5A1 95               [ 1]  480 	ld	xh, a
      00A5A2 A6 04            [ 1]  481 	ld	a, #0x04
      00A5A4                        482 00366$:
      00A5A4 90 58            [ 2]  483 	sllw	y
      00A5A6 59               [ 2]  484 	rlcw	x
      00A5A7 4A               [ 1]  485 	dec	a
      00A5A8 26 FA            [ 1]  486 	jrne	00366$
      00A5AA 4B 64            [ 1]  487 	push	#0x64
                           0001F1   488 	Sstm8s_uart1$UART1_Init$117 ==.
      00A5AC 4B 00            [ 1]  489 	push	#0x00
                           0001F3   490 	Sstm8s_uart1$UART1_Init$118 ==.
      00A5AE 4B 00            [ 1]  491 	push	#0x00
                           0001F5   492 	Sstm8s_uart1$UART1_Init$119 ==.
      00A5B0 4B 00            [ 1]  493 	push	#0x00
                           0001F7   494 	Sstm8s_uart1$UART1_Init$120 ==.
      00A5B2 90 89            [ 2]  495 	pushw	y
                           0001F9   496 	Sstm8s_uart1$UART1_Init$121 ==.
      00A5B4 89               [ 2]  497 	pushw	x
                           0001FA   498 	Sstm8s_uart1$UART1_Init$122 ==.
      00A5B5 CD AB 55         [ 4]  499 	call	__divulong
      00A5B8 5B 08            [ 2]  500 	addw	sp, #8
                           0001FF   501 	Sstm8s_uart1$UART1_Init$123 ==.
      00A5BA 9F               [ 1]  502 	ld	a, xl
      00A5BB A4 0F            [ 1]  503 	and	a, #0x0f
      00A5BD 1A 09            [ 1]  504 	or	a, (0x09, sp)
      00A5BF C7 52 33         [ 1]  505 	ld	0x5233, a
                           000207   506 	Sstm8s_uart1$UART1_Init$124 ==.
                                    507 ;	drivers/src/stm8s_uart1.c: 133: UART1->BRR2 |= (uint8_t)((BaudRate_Mantissa >> 4) & (uint8_t)0xF0); 
      00A5C2 C6 52 33         [ 1]  508 	ld	a, 0x5233
      00A5C5 6B 0D            [ 1]  509 	ld	(0x0d, sp), a
      00A5C7 1E 03            [ 2]  510 	ldw	x, (0x03, sp)
      00A5C9 A6 10            [ 1]  511 	ld	a, #0x10
      00A5CB 62               [ 2]  512 	div	x, a
      00A5CC 9F               [ 1]  513 	ld	a, xl
      00A5CD A4 F0            [ 1]  514 	and	a, #0xf0
      00A5CF 1A 0D            [ 1]  515 	or	a, (0x0d, sp)
      00A5D1 C7 52 33         [ 1]  516 	ld	0x5233, a
                           000219   517 	Sstm8s_uart1$UART1_Init$125 ==.
                                    518 ;	drivers/src/stm8s_uart1.c: 135: UART1->BRR1 |= (uint8_t)BaudRate_Mantissa;           
      00A5D4 C6 52 32         [ 1]  519 	ld	a, 0x5232
      00A5D7 6B 0D            [ 1]  520 	ld	(0x0d, sp), a
      00A5D9 7B 04            [ 1]  521 	ld	a, (0x04, sp)
      00A5DB 1A 0D            [ 1]  522 	or	a, (0x0d, sp)
      00A5DD C7 52 32         [ 1]  523 	ld	0x5232, a
                           000225   524 	Sstm8s_uart1$UART1_Init$126 ==.
                                    525 ;	drivers/src/stm8s_uart1.c: 138: UART1->CR2 &= (uint8_t)~(UART1_CR2_TEN | UART1_CR2_REN); 
      00A5E0 C6 52 35         [ 1]  526 	ld	a, 0x5235
      00A5E3 A4 F3            [ 1]  527 	and	a, #0xf3
      00A5E5 C7 52 35         [ 1]  528 	ld	0x5235, a
                           00022D   529 	Sstm8s_uart1$UART1_Init$127 ==.
                                    530 ;	drivers/src/stm8s_uart1.c: 140: UART1->CR3 &= (uint8_t)~(UART1_CR3_CPOL | UART1_CR3_CPHA | UART1_CR3_LBCL); 
      00A5E8 C6 52 36         [ 1]  531 	ld	a, 0x5236
      00A5EB A4 F8            [ 1]  532 	and	a, #0xf8
      00A5ED C7 52 36         [ 1]  533 	ld	0x5236, a
                           000235   534 	Sstm8s_uart1$UART1_Init$128 ==.
                                    535 ;	drivers/src/stm8s_uart1.c: 142: UART1->CR3 |= (uint8_t)((uint8_t)SyncMode & (uint8_t)(UART1_CR3_CPOL | 
      00A5F0 C6 52 36         [ 1]  536 	ld	a, 0x5236
      00A5F3 6B 0D            [ 1]  537 	ld	(0x0d, sp), a
      00A5F5 7B 17            [ 1]  538 	ld	a, (0x17, sp)
      00A5F7 A4 07            [ 1]  539 	and	a, #0x07
      00A5F9 1A 0D            [ 1]  540 	or	a, (0x0d, sp)
      00A5FB C7 52 36         [ 1]  541 	ld	0x5236, a
                           000243   542 	Sstm8s_uart1$UART1_Init$129 ==.
                                    543 ;	drivers/src/stm8s_uart1.c: 138: UART1->CR2 &= (uint8_t)~(UART1_CR2_TEN | UART1_CR2_REN); 
      00A5FE C6 52 35         [ 1]  544 	ld	a, 0x5235
                           000246   545 	Sstm8s_uart1$UART1_Init$130 ==.
                                    546 ;	drivers/src/stm8s_uart1.c: 145: if ((uint8_t)(Mode & UART1_MODE_TX_ENABLE))
      00A601 88               [ 1]  547 	push	a
                           000247   548 	Sstm8s_uart1$UART1_Init$131 ==.
      00A602 7B 19            [ 1]  549 	ld	a, (0x19, sp)
      00A604 A5 04            [ 1]  550 	bcp	a, #0x04
      00A606 84               [ 1]  551 	pop	a
                           00024C   552 	Sstm8s_uart1$UART1_Init$132 ==.
      00A607 27 07            [ 1]  553 	jreq	00102$
                           00024E   554 	Sstm8s_uart1$UART1_Init$133 ==.
                           00024E   555 	Sstm8s_uart1$UART1_Init$134 ==.
                                    556 ;	drivers/src/stm8s_uart1.c: 148: UART1->CR2 |= (uint8_t)UART1_CR2_TEN;  
      00A609 AA 08            [ 1]  557 	or	a, #0x08
      00A60B C7 52 35         [ 1]  558 	ld	0x5235, a
                           000253   559 	Sstm8s_uart1$UART1_Init$135 ==.
      00A60E 20 05            [ 2]  560 	jra	00103$
      00A610                        561 00102$:
                           000255   562 	Sstm8s_uart1$UART1_Init$136 ==.
                           000255   563 	Sstm8s_uart1$UART1_Init$137 ==.
                                    564 ;	drivers/src/stm8s_uart1.c: 153: UART1->CR2 &= (uint8_t)(~UART1_CR2_TEN);  
      00A610 A4 F7            [ 1]  565 	and	a, #0xf7
      00A612 C7 52 35         [ 1]  566 	ld	0x5235, a
                           00025A   567 	Sstm8s_uart1$UART1_Init$138 ==.
      00A615                        568 00103$:
                           00025A   569 	Sstm8s_uart1$UART1_Init$139 ==.
                                    570 ;	drivers/src/stm8s_uart1.c: 138: UART1->CR2 &= (uint8_t)~(UART1_CR2_TEN | UART1_CR2_REN); 
      00A615 C6 52 35         [ 1]  571 	ld	a, 0x5235
                           00025D   572 	Sstm8s_uart1$UART1_Init$140 ==.
                                    573 ;	drivers/src/stm8s_uart1.c: 155: if ((uint8_t)(Mode & UART1_MODE_RX_ENABLE))
      00A618 88               [ 1]  574 	push	a
                           00025E   575 	Sstm8s_uart1$UART1_Init$141 ==.
      00A619 7B 19            [ 1]  576 	ld	a, (0x19, sp)
      00A61B A5 08            [ 1]  577 	bcp	a, #0x08
      00A61D 84               [ 1]  578 	pop	a
                           000263   579 	Sstm8s_uart1$UART1_Init$142 ==.
      00A61E 27 07            [ 1]  580 	jreq	00105$
                           000265   581 	Sstm8s_uart1$UART1_Init$143 ==.
                           000265   582 	Sstm8s_uart1$UART1_Init$144 ==.
                                    583 ;	drivers/src/stm8s_uart1.c: 158: UART1->CR2 |= (uint8_t)UART1_CR2_REN;  
      00A620 AA 04            [ 1]  584 	or	a, #0x04
      00A622 C7 52 35         [ 1]  585 	ld	0x5235, a
                           00026A   586 	Sstm8s_uart1$UART1_Init$145 ==.
      00A625 20 05            [ 2]  587 	jra	00106$
      00A627                        588 00105$:
                           00026C   589 	Sstm8s_uart1$UART1_Init$146 ==.
                           00026C   590 	Sstm8s_uart1$UART1_Init$147 ==.
                                    591 ;	drivers/src/stm8s_uart1.c: 163: UART1->CR2 &= (uint8_t)(~UART1_CR2_REN);  
      00A627 A4 FB            [ 1]  592 	and	a, #0xfb
      00A629 C7 52 35         [ 1]  593 	ld	0x5235, a
                           000271   594 	Sstm8s_uart1$UART1_Init$148 ==.
      00A62C                        595 00106$:
                           000271   596 	Sstm8s_uart1$UART1_Init$149 ==.
                                    597 ;	drivers/src/stm8s_uart1.c: 111: UART1->CR3 &= (uint8_t)(~UART1_CR3_STOP);  
      00A62C C6 52 36         [ 1]  598 	ld	a, 0x5236
                           000274   599 	Sstm8s_uart1$UART1_Init$150 ==.
                                    600 ;	drivers/src/stm8s_uart1.c: 167: if ((uint8_t)(SyncMode & UART1_SYNCMODE_CLOCK_DISABLE))
      00A62F 0D 17            [ 1]  601 	tnz	(0x17, sp)
      00A631 2A 07            [ 1]  602 	jrpl	00108$
                           000278   603 	Sstm8s_uart1$UART1_Init$151 ==.
                           000278   604 	Sstm8s_uart1$UART1_Init$152 ==.
                                    605 ;	drivers/src/stm8s_uart1.c: 170: UART1->CR3 &= (uint8_t)(~UART1_CR3_CKEN); 
      00A633 A4 F7            [ 1]  606 	and	a, #0xf7
      00A635 C7 52 36         [ 1]  607 	ld	0x5236, a
                           00027D   608 	Sstm8s_uart1$UART1_Init$153 ==.
      00A638 20 0D            [ 2]  609 	jra	00110$
      00A63A                        610 00108$:
                           00027F   611 	Sstm8s_uart1$UART1_Init$154 ==.
                           00027F   612 	Sstm8s_uart1$UART1_Init$155 ==.
                                    613 ;	drivers/src/stm8s_uart1.c: 174: UART1->CR3 |= (uint8_t)((uint8_t)SyncMode & UART1_CR3_CKEN);
      00A63A 88               [ 1]  614 	push	a
                           000280   615 	Sstm8s_uart1$UART1_Init$156 ==.
      00A63B 7B 18            [ 1]  616 	ld	a, (0x18, sp)
      00A63D A4 08            [ 1]  617 	and	a, #0x08
      00A63F 6B 0E            [ 1]  618 	ld	(0x0e, sp), a
      00A641 84               [ 1]  619 	pop	a
                           000287   620 	Sstm8s_uart1$UART1_Init$157 ==.
      00A642 1A 0D            [ 1]  621 	or	a, (0x0d, sp)
      00A644 C7 52 36         [ 1]  622 	ld	0x5236, a
                           00028C   623 	Sstm8s_uart1$UART1_Init$158 ==.
      00A647                        624 00110$:
                           00028C   625 	Sstm8s_uart1$UART1_Init$159 ==.
                                    626 ;	drivers/src/stm8s_uart1.c: 176: }
      00A647 5B 0D            [ 2]  627 	addw	sp, #13
                           00028E   628 	Sstm8s_uart1$UART1_Init$160 ==.
                           00028E   629 	Sstm8s_uart1$UART1_Init$161 ==.
                           00028E   630 	XG$UART1_Init$0$0 ==.
      00A649 81               [ 4]  631 	ret
                           00028F   632 	Sstm8s_uart1$UART1_Init$162 ==.
                           00028F   633 	Sstm8s_uart1$UART1_Cmd$163 ==.
                                    634 ;	drivers/src/stm8s_uart1.c: 184: void UART1_Cmd(FunctionalState NewState)
                                    635 ;	-----------------------------------------
                                    636 ;	 function UART1_Cmd
                                    637 ;	-----------------------------------------
      00A64A                        638 _UART1_Cmd:
                           00028F   639 	Sstm8s_uart1$UART1_Cmd$164 ==.
                           00028F   640 	Sstm8s_uart1$UART1_Cmd$165 ==.
                                    641 ;	drivers/src/stm8s_uart1.c: 189: UART1->CR1 &= (uint8_t)(~UART1_CR1_UARTD); 
      00A64A C6 52 34         [ 1]  642 	ld	a, 0x5234
                           000292   643 	Sstm8s_uart1$UART1_Cmd$166 ==.
                                    644 ;	drivers/src/stm8s_uart1.c: 186: if (NewState != DISABLE)
      00A64D 0D 03            [ 1]  645 	tnz	(0x03, sp)
      00A64F 27 07            [ 1]  646 	jreq	00102$
                           000296   647 	Sstm8s_uart1$UART1_Cmd$167 ==.
                           000296   648 	Sstm8s_uart1$UART1_Cmd$168 ==.
                                    649 ;	drivers/src/stm8s_uart1.c: 189: UART1->CR1 &= (uint8_t)(~UART1_CR1_UARTD); 
      00A651 A4 DF            [ 1]  650 	and	a, #0xdf
      00A653 C7 52 34         [ 1]  651 	ld	0x5234, a
                           00029B   652 	Sstm8s_uart1$UART1_Cmd$169 ==.
      00A656 20 05            [ 2]  653 	jra	00104$
      00A658                        654 00102$:
                           00029D   655 	Sstm8s_uart1$UART1_Cmd$170 ==.
                           00029D   656 	Sstm8s_uart1$UART1_Cmd$171 ==.
                                    657 ;	drivers/src/stm8s_uart1.c: 194: UART1->CR1 |= UART1_CR1_UARTD;  
      00A658 AA 20            [ 1]  658 	or	a, #0x20
      00A65A C7 52 34         [ 1]  659 	ld	0x5234, a
                           0002A2   660 	Sstm8s_uart1$UART1_Cmd$172 ==.
      00A65D                        661 00104$:
                           0002A2   662 	Sstm8s_uart1$UART1_Cmd$173 ==.
                                    663 ;	drivers/src/stm8s_uart1.c: 196: }
                           0002A2   664 	Sstm8s_uart1$UART1_Cmd$174 ==.
                           0002A2   665 	XG$UART1_Cmd$0$0 ==.
      00A65D 81               [ 4]  666 	ret
                           0002A3   667 	Sstm8s_uart1$UART1_Cmd$175 ==.
                           0002A3   668 	Sstm8s_uart1$UART1_ITConfig$176 ==.
                                    669 ;	drivers/src/stm8s_uart1.c: 211: void UART1_ITConfig(UART1_IT_TypeDef UART1_IT, FunctionalState NewState)
                                    670 ;	-----------------------------------------
                                    671 ;	 function UART1_ITConfig
                                    672 ;	-----------------------------------------
      00A65E                        673 _UART1_ITConfig:
                           0002A3   674 	Sstm8s_uart1$UART1_ITConfig$177 ==.
      00A65E 89               [ 2]  675 	pushw	x
                           0002A4   676 	Sstm8s_uart1$UART1_ITConfig$178 ==.
                           0002A4   677 	Sstm8s_uart1$UART1_ITConfig$179 ==.
                                    678 ;	drivers/src/stm8s_uart1.c: 216: assert_param(IS_UART1_CONFIG_IT_OK(UART1_IT));
      00A65F 1E 05            [ 2]  679 	ldw	x, (0x05, sp)
      00A661 A3 01 00         [ 2]  680 	cpw	x, #0x0100
      00A664 27 2C            [ 1]  681 	jreq	00119$
                           0002AB   682 	Sstm8s_uart1$UART1_ITConfig$180 ==.
      00A666 A3 02 77         [ 2]  683 	cpw	x, #0x0277
      00A669 27 27            [ 1]  684 	jreq	00119$
                           0002B0   685 	Sstm8s_uart1$UART1_ITConfig$181 ==.
      00A66B A3 02 66         [ 2]  686 	cpw	x, #0x0266
      00A66E 27 22            [ 1]  687 	jreq	00119$
                           0002B5   688 	Sstm8s_uart1$UART1_ITConfig$182 ==.
      00A670 A3 02 05         [ 2]  689 	cpw	x, #0x0205
      00A673 27 1D            [ 1]  690 	jreq	00119$
                           0002BA   691 	Sstm8s_uart1$UART1_ITConfig$183 ==.
      00A675 A3 02 44         [ 2]  692 	cpw	x, #0x0244
      00A678 27 18            [ 1]  693 	jreq	00119$
                           0002BF   694 	Sstm8s_uart1$UART1_ITConfig$184 ==.
      00A67A A3 03 46         [ 2]  695 	cpw	x, #0x0346
      00A67D 27 13            [ 1]  696 	jreq	00119$
                           0002C4   697 	Sstm8s_uart1$UART1_ITConfig$185 ==.
      00A67F 89               [ 2]  698 	pushw	x
                           0002C5   699 	Sstm8s_uart1$UART1_ITConfig$186 ==.
      00A680 4B D8            [ 1]  700 	push	#0xd8
                           0002C7   701 	Sstm8s_uart1$UART1_ITConfig$187 ==.
      00A682 4B 00            [ 1]  702 	push	#0x00
                           0002C9   703 	Sstm8s_uart1$UART1_ITConfig$188 ==.
      00A684 4B 00            [ 1]  704 	push	#0x00
                           0002CB   705 	Sstm8s_uart1$UART1_ITConfig$189 ==.
      00A686 4B 00            [ 1]  706 	push	#0x00
                           0002CD   707 	Sstm8s_uart1$UART1_ITConfig$190 ==.
      00A688 4B 5E            [ 1]  708 	push	#<(___str_0+0)
                           0002CF   709 	Sstm8s_uart1$UART1_ITConfig$191 ==.
      00A68A 4B 81            [ 1]  710 	push	#((___str_0+0) >> 8)
                           0002D1   711 	Sstm8s_uart1$UART1_ITConfig$192 ==.
      00A68C CD 84 F3         [ 4]  712 	call	_assert_failed
      00A68F 5B 06            [ 2]  713 	addw	sp, #6
                           0002D6   714 	Sstm8s_uart1$UART1_ITConfig$193 ==.
      00A691 85               [ 2]  715 	popw	x
                           0002D7   716 	Sstm8s_uart1$UART1_ITConfig$194 ==.
      00A692                        717 00119$:
                           0002D7   718 	Sstm8s_uart1$UART1_ITConfig$195 ==.
                                    719 ;	drivers/src/stm8s_uart1.c: 217: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
      00A692 0D 07            [ 1]  720 	tnz	(0x07, sp)
      00A694 27 18            [ 1]  721 	jreq	00136$
      00A696 7B 07            [ 1]  722 	ld	a, (0x07, sp)
      00A698 4A               [ 1]  723 	dec	a
      00A699 27 13            [ 1]  724 	jreq	00136$
                           0002E0   725 	Sstm8s_uart1$UART1_ITConfig$196 ==.
      00A69B 89               [ 2]  726 	pushw	x
                           0002E1   727 	Sstm8s_uart1$UART1_ITConfig$197 ==.
      00A69C 4B D9            [ 1]  728 	push	#0xd9
                           0002E3   729 	Sstm8s_uart1$UART1_ITConfig$198 ==.
      00A69E 4B 00            [ 1]  730 	push	#0x00
                           0002E5   731 	Sstm8s_uart1$UART1_ITConfig$199 ==.
      00A6A0 4B 00            [ 1]  732 	push	#0x00
                           0002E7   733 	Sstm8s_uart1$UART1_ITConfig$200 ==.
      00A6A2 4B 00            [ 1]  734 	push	#0x00
                           0002E9   735 	Sstm8s_uart1$UART1_ITConfig$201 ==.
      00A6A4 4B 5E            [ 1]  736 	push	#<(___str_0+0)
                           0002EB   737 	Sstm8s_uart1$UART1_ITConfig$202 ==.
      00A6A6 4B 81            [ 1]  738 	push	#((___str_0+0) >> 8)
                           0002ED   739 	Sstm8s_uart1$UART1_ITConfig$203 ==.
      00A6A8 CD 84 F3         [ 4]  740 	call	_assert_failed
      00A6AB 5B 06            [ 2]  741 	addw	sp, #6
                           0002F2   742 	Sstm8s_uart1$UART1_ITConfig$204 ==.
      00A6AD 85               [ 2]  743 	popw	x
                           0002F3   744 	Sstm8s_uart1$UART1_ITConfig$205 ==.
      00A6AE                        745 00136$:
                           0002F3   746 	Sstm8s_uart1$UART1_ITConfig$206 ==.
                                    747 ;	drivers/src/stm8s_uart1.c: 220: uartreg = (uint8_t)((uint16_t)UART1_IT >> 0x08);
                           0002F3   748 	Sstm8s_uart1$UART1_ITConfig$207 ==.
                                    749 ;	drivers/src/stm8s_uart1.c: 222: itpos = (uint8_t)((uint8_t)1 << (uint8_t)((uint8_t)UART1_IT & (uint8_t)0x0F));
      00A6AE 7B 06            [ 1]  750 	ld	a, (0x06, sp)
      00A6B0 A4 0F            [ 1]  751 	and	a, #0x0f
      00A6B2 88               [ 1]  752 	push	a
                           0002F8   753 	Sstm8s_uart1$UART1_ITConfig$208 ==.
      00A6B3 A6 01            [ 1]  754 	ld	a, #0x01
      00A6B5 6B 03            [ 1]  755 	ld	(0x03, sp), a
      00A6B7 84               [ 1]  756 	pop	a
                           0002FD   757 	Sstm8s_uart1$UART1_ITConfig$209 ==.
      00A6B8 4D               [ 1]  758 	tnz	a
      00A6B9 27 05            [ 1]  759 	jreq	00228$
      00A6BB                        760 00227$:
      00A6BB 08 02            [ 1]  761 	sll	(0x02, sp)
      00A6BD 4A               [ 1]  762 	dec	a
      00A6BE 26 FB            [ 1]  763 	jrne	00227$
      00A6C0                        764 00228$:
                           000305   765 	Sstm8s_uart1$UART1_ITConfig$210 ==.
                                    766 ;	drivers/src/stm8s_uart1.c: 227: if (uartreg == 0x01)
      00A6C0 9E               [ 1]  767 	ld	a, xh
      00A6C1 4A               [ 1]  768 	dec	a
      00A6C2 26 05            [ 1]  769 	jrne	00230$
      00A6C4 A6 01            [ 1]  770 	ld	a, #0x01
      00A6C6 6B 01            [ 1]  771 	ld	(0x01, sp), a
      00A6C8 C5                     772 	.byte 0xc5
      00A6C9                        773 00230$:
      00A6C9 0F 01            [ 1]  774 	clr	(0x01, sp)
      00A6CB                        775 00231$:
                           000310   776 	Sstm8s_uart1$UART1_ITConfig$211 ==.
                           000310   777 	Sstm8s_uart1$UART1_ITConfig$212 ==.
                                    778 ;	drivers/src/stm8s_uart1.c: 231: else if (uartreg == 0x02)
      00A6CB 9E               [ 1]  779 	ld	a, xh
      00A6CC A0 02            [ 1]  780 	sub	a, #0x02
      00A6CE 26 02            [ 1]  781 	jrne	00233$
      00A6D0 4C               [ 1]  782 	inc	a
      00A6D1 21                     783 	.byte 0x21
      00A6D2                        784 00233$:
      00A6D2 4F               [ 1]  785 	clr	a
      00A6D3                        786 00234$:
                           000318   787 	Sstm8s_uart1$UART1_ITConfig$213 ==.
                           000318   788 	Sstm8s_uart1$UART1_ITConfig$214 ==.
                                    789 ;	drivers/src/stm8s_uart1.c: 224: if (NewState != DISABLE)
      00A6D3 0D 07            [ 1]  790 	tnz	(0x07, sp)
      00A6D5 27 27            [ 1]  791 	jreq	00114$
                           00031C   792 	Sstm8s_uart1$UART1_ITConfig$215 ==.
                           00031C   793 	Sstm8s_uart1$UART1_ITConfig$216 ==.
                                    794 ;	drivers/src/stm8s_uart1.c: 227: if (uartreg == 0x01)
      00A6D7 0D 01            [ 1]  795 	tnz	(0x01, sp)
      00A6D9 27 0B            [ 1]  796 	jreq	00105$
                           000320   797 	Sstm8s_uart1$UART1_ITConfig$217 ==.
                           000320   798 	Sstm8s_uart1$UART1_ITConfig$218 ==.
                                    799 ;	drivers/src/stm8s_uart1.c: 229: UART1->CR1 |= itpos;
      00A6DB C6 52 34         [ 1]  800 	ld	a, 0x5234
      00A6DE 1A 02            [ 1]  801 	or	a, (0x02, sp)
      00A6E0 C7 52 34         [ 1]  802 	ld	0x5234, a
                           000328   803 	Sstm8s_uart1$UART1_ITConfig$219 ==.
      00A6E3 CC A7 25         [ 2]  804 	jp	00116$
      00A6E6                        805 00105$:
                           00032B   806 	Sstm8s_uart1$UART1_ITConfig$220 ==.
                                    807 ;	drivers/src/stm8s_uart1.c: 231: else if (uartreg == 0x02)
      00A6E6 4D               [ 1]  808 	tnz	a
      00A6E7 27 0B            [ 1]  809 	jreq	00102$
                           00032E   810 	Sstm8s_uart1$UART1_ITConfig$221 ==.
                           00032E   811 	Sstm8s_uart1$UART1_ITConfig$222 ==.
                                    812 ;	drivers/src/stm8s_uart1.c: 233: UART1->CR2 |= itpos;
      00A6E9 C6 52 35         [ 1]  813 	ld	a, 0x5235
      00A6EC 1A 02            [ 1]  814 	or	a, (0x02, sp)
      00A6EE C7 52 35         [ 1]  815 	ld	0x5235, a
                           000336   816 	Sstm8s_uart1$UART1_ITConfig$223 ==.
      00A6F1 CC A7 25         [ 2]  817 	jp	00116$
      00A6F4                        818 00102$:
                           000339   819 	Sstm8s_uart1$UART1_ITConfig$224 ==.
                           000339   820 	Sstm8s_uart1$UART1_ITConfig$225 ==.
                                    821 ;	drivers/src/stm8s_uart1.c: 237: UART1->CR4 |= itpos;
      00A6F4 C6 52 37         [ 1]  822 	ld	a, 0x5237
      00A6F7 1A 02            [ 1]  823 	or	a, (0x02, sp)
      00A6F9 C7 52 37         [ 1]  824 	ld	0x5237, a
                           000341   825 	Sstm8s_uart1$UART1_ITConfig$226 ==.
      00A6FC 20 27            [ 2]  826 	jra	00116$
      00A6FE                        827 00114$:
                           000343   828 	Sstm8s_uart1$UART1_ITConfig$227 ==.
                                    829 ;	drivers/src/stm8s_uart1.c: 245: UART1->CR1 &= (uint8_t)(~itpos);
      00A6FE 88               [ 1]  830 	push	a
                           000344   831 	Sstm8s_uart1$UART1_ITConfig$228 ==.
      00A6FF 03 03            [ 1]  832 	cpl	(0x03, sp)
      00A701 84               [ 1]  833 	pop	a
                           000347   834 	Sstm8s_uart1$UART1_ITConfig$229 ==.
                           000347   835 	Sstm8s_uart1$UART1_ITConfig$230 ==.
                           000347   836 	Sstm8s_uart1$UART1_ITConfig$231 ==.
                                    837 ;	drivers/src/stm8s_uart1.c: 243: if (uartreg == 0x01)
      00A702 0D 01            [ 1]  838 	tnz	(0x01, sp)
      00A704 27 0A            [ 1]  839 	jreq	00111$
                           00034B   840 	Sstm8s_uart1$UART1_ITConfig$232 ==.
                           00034B   841 	Sstm8s_uart1$UART1_ITConfig$233 ==.
                                    842 ;	drivers/src/stm8s_uart1.c: 245: UART1->CR1 &= (uint8_t)(~itpos);
      00A706 C6 52 34         [ 1]  843 	ld	a, 0x5234
      00A709 14 02            [ 1]  844 	and	a, (0x02, sp)
      00A70B C7 52 34         [ 1]  845 	ld	0x5234, a
                           000353   846 	Sstm8s_uart1$UART1_ITConfig$234 ==.
      00A70E 20 15            [ 2]  847 	jra	00116$
      00A710                        848 00111$:
                           000355   849 	Sstm8s_uart1$UART1_ITConfig$235 ==.
                                    850 ;	drivers/src/stm8s_uart1.c: 247: else if (uartreg == 0x02)
      00A710 4D               [ 1]  851 	tnz	a
      00A711 27 0A            [ 1]  852 	jreq	00108$
                           000358   853 	Sstm8s_uart1$UART1_ITConfig$236 ==.
                           000358   854 	Sstm8s_uart1$UART1_ITConfig$237 ==.
                                    855 ;	drivers/src/stm8s_uart1.c: 249: UART1->CR2 &= (uint8_t)(~itpos);
      00A713 C6 52 35         [ 1]  856 	ld	a, 0x5235
      00A716 14 02            [ 1]  857 	and	a, (0x02, sp)
      00A718 C7 52 35         [ 1]  858 	ld	0x5235, a
                           000360   859 	Sstm8s_uart1$UART1_ITConfig$238 ==.
      00A71B 20 08            [ 2]  860 	jra	00116$
      00A71D                        861 00108$:
                           000362   862 	Sstm8s_uart1$UART1_ITConfig$239 ==.
                           000362   863 	Sstm8s_uart1$UART1_ITConfig$240 ==.
                                    864 ;	drivers/src/stm8s_uart1.c: 253: UART1->CR4 &= (uint8_t)(~itpos);
      00A71D C6 52 37         [ 1]  865 	ld	a, 0x5237
      00A720 14 02            [ 1]  866 	and	a, (0x02, sp)
      00A722 C7 52 37         [ 1]  867 	ld	0x5237, a
                           00036A   868 	Sstm8s_uart1$UART1_ITConfig$241 ==.
      00A725                        869 00116$:
                           00036A   870 	Sstm8s_uart1$UART1_ITConfig$242 ==.
                                    871 ;	drivers/src/stm8s_uart1.c: 257: }
      00A725 85               [ 2]  872 	popw	x
                           00036B   873 	Sstm8s_uart1$UART1_ITConfig$243 ==.
                           00036B   874 	Sstm8s_uart1$UART1_ITConfig$244 ==.
                           00036B   875 	XG$UART1_ITConfig$0$0 ==.
      00A726 81               [ 4]  876 	ret
                           00036C   877 	Sstm8s_uart1$UART1_ITConfig$245 ==.
                           00036C   878 	Sstm8s_uart1$UART1_HalfDuplexCmd$246 ==.
                                    879 ;	drivers/src/stm8s_uart1.c: 265: void UART1_HalfDuplexCmd(FunctionalState NewState)
                                    880 ;	-----------------------------------------
                                    881 ;	 function UART1_HalfDuplexCmd
                                    882 ;	-----------------------------------------
      00A727                        883 _UART1_HalfDuplexCmd:
                           00036C   884 	Sstm8s_uart1$UART1_HalfDuplexCmd$247 ==.
                           00036C   885 	Sstm8s_uart1$UART1_HalfDuplexCmd$248 ==.
                                    886 ;	drivers/src/stm8s_uart1.c: 267: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
      00A727 0D 03            [ 1]  887 	tnz	(0x03, sp)
      00A729 27 14            [ 1]  888 	jreq	00107$
      00A72B 7B 03            [ 1]  889 	ld	a, (0x03, sp)
      00A72D 4A               [ 1]  890 	dec	a
      00A72E 27 0F            [ 1]  891 	jreq	00107$
                           000375   892 	Sstm8s_uart1$UART1_HalfDuplexCmd$249 ==.
      00A730 4B 0B            [ 1]  893 	push	#0x0b
                           000377   894 	Sstm8s_uart1$UART1_HalfDuplexCmd$250 ==.
      00A732 4B 01            [ 1]  895 	push	#0x01
                           000379   896 	Sstm8s_uart1$UART1_HalfDuplexCmd$251 ==.
      00A734 5F               [ 1]  897 	clrw	x
      00A735 89               [ 2]  898 	pushw	x
                           00037B   899 	Sstm8s_uart1$UART1_HalfDuplexCmd$252 ==.
      00A736 4B 5E            [ 1]  900 	push	#<(___str_0+0)
                           00037D   901 	Sstm8s_uart1$UART1_HalfDuplexCmd$253 ==.
      00A738 4B 81            [ 1]  902 	push	#((___str_0+0) >> 8)
                           00037F   903 	Sstm8s_uart1$UART1_HalfDuplexCmd$254 ==.
      00A73A CD 84 F3         [ 4]  904 	call	_assert_failed
      00A73D 5B 06            [ 2]  905 	addw	sp, #6
                           000384   906 	Sstm8s_uart1$UART1_HalfDuplexCmd$255 ==.
      00A73F                        907 00107$:
                           000384   908 	Sstm8s_uart1$UART1_HalfDuplexCmd$256 ==.
                                    909 ;	drivers/src/stm8s_uart1.c: 271: UART1->CR5 |= UART1_CR5_HDSEL;  /**< UART1 Half Duplex Enable  */
      00A73F C6 52 38         [ 1]  910 	ld	a, 0x5238
                           000387   911 	Sstm8s_uart1$UART1_HalfDuplexCmd$257 ==.
                                    912 ;	drivers/src/stm8s_uart1.c: 269: if (NewState != DISABLE)
      00A742 0D 03            [ 1]  913 	tnz	(0x03, sp)
      00A744 27 07            [ 1]  914 	jreq	00102$
                           00038B   915 	Sstm8s_uart1$UART1_HalfDuplexCmd$258 ==.
                           00038B   916 	Sstm8s_uart1$UART1_HalfDuplexCmd$259 ==.
                                    917 ;	drivers/src/stm8s_uart1.c: 271: UART1->CR5 |= UART1_CR5_HDSEL;  /**< UART1 Half Duplex Enable  */
      00A746 AA 08            [ 1]  918 	or	a, #0x08
      00A748 C7 52 38         [ 1]  919 	ld	0x5238, a
                           000390   920 	Sstm8s_uart1$UART1_HalfDuplexCmd$260 ==.
      00A74B 20 05            [ 2]  921 	jra	00104$
      00A74D                        922 00102$:
                           000392   923 	Sstm8s_uart1$UART1_HalfDuplexCmd$261 ==.
                           000392   924 	Sstm8s_uart1$UART1_HalfDuplexCmd$262 ==.
                                    925 ;	drivers/src/stm8s_uart1.c: 275: UART1->CR5 &= (uint8_t)~UART1_CR5_HDSEL; /**< UART1 Half Duplex Disable */
      00A74D A4 F7            [ 1]  926 	and	a, #0xf7
      00A74F C7 52 38         [ 1]  927 	ld	0x5238, a
                           000397   928 	Sstm8s_uart1$UART1_HalfDuplexCmd$263 ==.
      00A752                        929 00104$:
                           000397   930 	Sstm8s_uart1$UART1_HalfDuplexCmd$264 ==.
                                    931 ;	drivers/src/stm8s_uart1.c: 277: }
                           000397   932 	Sstm8s_uart1$UART1_HalfDuplexCmd$265 ==.
                           000397   933 	XG$UART1_HalfDuplexCmd$0$0 ==.
      00A752 81               [ 4]  934 	ret
                           000398   935 	Sstm8s_uart1$UART1_HalfDuplexCmd$266 ==.
                           000398   936 	Sstm8s_uart1$UART1_IrDAConfig$267 ==.
                                    937 ;	drivers/src/stm8s_uart1.c: 285: void UART1_IrDAConfig(UART1_IrDAMode_TypeDef UART1_IrDAMode)
                                    938 ;	-----------------------------------------
                                    939 ;	 function UART1_IrDAConfig
                                    940 ;	-----------------------------------------
      00A753                        941 _UART1_IrDAConfig:
                           000398   942 	Sstm8s_uart1$UART1_IrDAConfig$268 ==.
                           000398   943 	Sstm8s_uart1$UART1_IrDAConfig$269 ==.
                                    944 ;	drivers/src/stm8s_uart1.c: 287: assert_param(IS_UART1_IRDAMODE_OK(UART1_IrDAMode));
      00A753 7B 03            [ 1]  945 	ld	a, (0x03, sp)
      00A755 4A               [ 1]  946 	dec	a
      00A756 27 13            [ 1]  947 	jreq	00107$
                           00039D   948 	Sstm8s_uart1$UART1_IrDAConfig$270 ==.
      00A758 0D 03            [ 1]  949 	tnz	(0x03, sp)
      00A75A 27 0F            [ 1]  950 	jreq	00107$
      00A75C 4B 1F            [ 1]  951 	push	#0x1f
                           0003A3   952 	Sstm8s_uart1$UART1_IrDAConfig$271 ==.
      00A75E 4B 01            [ 1]  953 	push	#0x01
                           0003A5   954 	Sstm8s_uart1$UART1_IrDAConfig$272 ==.
      00A760 5F               [ 1]  955 	clrw	x
      00A761 89               [ 2]  956 	pushw	x
                           0003A7   957 	Sstm8s_uart1$UART1_IrDAConfig$273 ==.
      00A762 4B 5E            [ 1]  958 	push	#<(___str_0+0)
                           0003A9   959 	Sstm8s_uart1$UART1_IrDAConfig$274 ==.
      00A764 4B 81            [ 1]  960 	push	#((___str_0+0) >> 8)
                           0003AB   961 	Sstm8s_uart1$UART1_IrDAConfig$275 ==.
      00A766 CD 84 F3         [ 4]  962 	call	_assert_failed
      00A769 5B 06            [ 2]  963 	addw	sp, #6
                           0003B0   964 	Sstm8s_uart1$UART1_IrDAConfig$276 ==.
      00A76B                        965 00107$:
                           0003B0   966 	Sstm8s_uart1$UART1_IrDAConfig$277 ==.
                                    967 ;	drivers/src/stm8s_uart1.c: 291: UART1->CR5 |= UART1_CR5_IRLP;
      00A76B C6 52 38         [ 1]  968 	ld	a, 0x5238
                           0003B3   969 	Sstm8s_uart1$UART1_IrDAConfig$278 ==.
                                    970 ;	drivers/src/stm8s_uart1.c: 289: if (UART1_IrDAMode != UART1_IRDAMODE_NORMAL)
      00A76E 0D 03            [ 1]  971 	tnz	(0x03, sp)
      00A770 27 07            [ 1]  972 	jreq	00102$
                           0003B7   973 	Sstm8s_uart1$UART1_IrDAConfig$279 ==.
                           0003B7   974 	Sstm8s_uart1$UART1_IrDAConfig$280 ==.
                                    975 ;	drivers/src/stm8s_uart1.c: 291: UART1->CR5 |= UART1_CR5_IRLP;
      00A772 AA 04            [ 1]  976 	or	a, #0x04
      00A774 C7 52 38         [ 1]  977 	ld	0x5238, a
                           0003BC   978 	Sstm8s_uart1$UART1_IrDAConfig$281 ==.
      00A777 20 05            [ 2]  979 	jra	00104$
      00A779                        980 00102$:
                           0003BE   981 	Sstm8s_uart1$UART1_IrDAConfig$282 ==.
                           0003BE   982 	Sstm8s_uart1$UART1_IrDAConfig$283 ==.
                                    983 ;	drivers/src/stm8s_uart1.c: 295: UART1->CR5 &= ((uint8_t)~UART1_CR5_IRLP);
      00A779 A4 FB            [ 1]  984 	and	a, #0xfb
      00A77B C7 52 38         [ 1]  985 	ld	0x5238, a
                           0003C3   986 	Sstm8s_uart1$UART1_IrDAConfig$284 ==.
      00A77E                        987 00104$:
                           0003C3   988 	Sstm8s_uart1$UART1_IrDAConfig$285 ==.
                                    989 ;	drivers/src/stm8s_uart1.c: 297: }
                           0003C3   990 	Sstm8s_uart1$UART1_IrDAConfig$286 ==.
                           0003C3   991 	XG$UART1_IrDAConfig$0$0 ==.
      00A77E 81               [ 4]  992 	ret
                           0003C4   993 	Sstm8s_uart1$UART1_IrDAConfig$287 ==.
                           0003C4   994 	Sstm8s_uart1$UART1_IrDACmd$288 ==.
                                    995 ;	drivers/src/stm8s_uart1.c: 305: void UART1_IrDACmd(FunctionalState NewState)
                                    996 ;	-----------------------------------------
                                    997 ;	 function UART1_IrDACmd
                                    998 ;	-----------------------------------------
      00A77F                        999 _UART1_IrDACmd:
                           0003C4  1000 	Sstm8s_uart1$UART1_IrDACmd$289 ==.
                           0003C4  1001 	Sstm8s_uart1$UART1_IrDACmd$290 ==.
                                   1002 ;	drivers/src/stm8s_uart1.c: 308: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
      00A77F 0D 03            [ 1] 1003 	tnz	(0x03, sp)
      00A781 27 14            [ 1] 1004 	jreq	00107$
      00A783 7B 03            [ 1] 1005 	ld	a, (0x03, sp)
      00A785 4A               [ 1] 1006 	dec	a
      00A786 27 0F            [ 1] 1007 	jreq	00107$
                           0003CD  1008 	Sstm8s_uart1$UART1_IrDACmd$291 ==.
      00A788 4B 34            [ 1] 1009 	push	#0x34
                           0003CF  1010 	Sstm8s_uart1$UART1_IrDACmd$292 ==.
      00A78A 4B 01            [ 1] 1011 	push	#0x01
                           0003D1  1012 	Sstm8s_uart1$UART1_IrDACmd$293 ==.
      00A78C 5F               [ 1] 1013 	clrw	x
      00A78D 89               [ 2] 1014 	pushw	x
                           0003D3  1015 	Sstm8s_uart1$UART1_IrDACmd$294 ==.
      00A78E 4B 5E            [ 1] 1016 	push	#<(___str_0+0)
                           0003D5  1017 	Sstm8s_uart1$UART1_IrDACmd$295 ==.
      00A790 4B 81            [ 1] 1018 	push	#((___str_0+0) >> 8)
                           0003D7  1019 	Sstm8s_uart1$UART1_IrDACmd$296 ==.
      00A792 CD 84 F3         [ 4] 1020 	call	_assert_failed
      00A795 5B 06            [ 2] 1021 	addw	sp, #6
                           0003DC  1022 	Sstm8s_uart1$UART1_IrDACmd$297 ==.
      00A797                       1023 00107$:
                           0003DC  1024 	Sstm8s_uart1$UART1_IrDACmd$298 ==.
                                   1025 ;	drivers/src/stm8s_uart1.c: 313: UART1->CR5 |= UART1_CR5_IREN;
      00A797 C6 52 38         [ 1] 1026 	ld	a, 0x5238
                           0003DF  1027 	Sstm8s_uart1$UART1_IrDACmd$299 ==.
                                   1028 ;	drivers/src/stm8s_uart1.c: 310: if (NewState != DISABLE)
      00A79A 0D 03            [ 1] 1029 	tnz	(0x03, sp)
      00A79C 27 07            [ 1] 1030 	jreq	00102$
                           0003E3  1031 	Sstm8s_uart1$UART1_IrDACmd$300 ==.
                           0003E3  1032 	Sstm8s_uart1$UART1_IrDACmd$301 ==.
                                   1033 ;	drivers/src/stm8s_uart1.c: 313: UART1->CR5 |= UART1_CR5_IREN;
      00A79E AA 02            [ 1] 1034 	or	a, #0x02
      00A7A0 C7 52 38         [ 1] 1035 	ld	0x5238, a
                           0003E8  1036 	Sstm8s_uart1$UART1_IrDACmd$302 ==.
      00A7A3 20 05            [ 2] 1037 	jra	00104$
      00A7A5                       1038 00102$:
                           0003EA  1039 	Sstm8s_uart1$UART1_IrDACmd$303 ==.
                           0003EA  1040 	Sstm8s_uart1$UART1_IrDACmd$304 ==.
                                   1041 ;	drivers/src/stm8s_uart1.c: 318: UART1->CR5 &= ((uint8_t)~UART1_CR5_IREN);
      00A7A5 A4 FD            [ 1] 1042 	and	a, #0xfd
      00A7A7 C7 52 38         [ 1] 1043 	ld	0x5238, a
                           0003EF  1044 	Sstm8s_uart1$UART1_IrDACmd$305 ==.
      00A7AA                       1045 00104$:
                           0003EF  1046 	Sstm8s_uart1$UART1_IrDACmd$306 ==.
                                   1047 ;	drivers/src/stm8s_uart1.c: 320: }
                           0003EF  1048 	Sstm8s_uart1$UART1_IrDACmd$307 ==.
                           0003EF  1049 	XG$UART1_IrDACmd$0$0 ==.
      00A7AA 81               [ 4] 1050 	ret
                           0003F0  1051 	Sstm8s_uart1$UART1_IrDACmd$308 ==.
                           0003F0  1052 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$309 ==.
                                   1053 ;	drivers/src/stm8s_uart1.c: 329: void UART1_LINBreakDetectionConfig(UART1_LINBreakDetectionLength_TypeDef UART1_LINBreakDetectionLength)
                                   1054 ;	-----------------------------------------
                                   1055 ;	 function UART1_LINBreakDetectionConfig
                                   1056 ;	-----------------------------------------
      00A7AB                       1057 _UART1_LINBreakDetectionConfig:
                           0003F0  1058 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$310 ==.
                           0003F0  1059 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$311 ==.
                                   1060 ;	drivers/src/stm8s_uart1.c: 331: assert_param(IS_UART1_LINBREAKDETECTIONLENGTH_OK(UART1_LINBreakDetectionLength));
      00A7AB 0D 03            [ 1] 1061 	tnz	(0x03, sp)
      00A7AD 27 14            [ 1] 1062 	jreq	00107$
      00A7AF 7B 03            [ 1] 1063 	ld	a, (0x03, sp)
      00A7B1 4A               [ 1] 1064 	dec	a
      00A7B2 27 0F            [ 1] 1065 	jreq	00107$
                           0003F9  1066 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$312 ==.
      00A7B4 4B 4B            [ 1] 1067 	push	#0x4b
                           0003FB  1068 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$313 ==.
      00A7B6 4B 01            [ 1] 1069 	push	#0x01
                           0003FD  1070 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$314 ==.
      00A7B8 5F               [ 1] 1071 	clrw	x
      00A7B9 89               [ 2] 1072 	pushw	x
                           0003FF  1073 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$315 ==.
      00A7BA 4B 5E            [ 1] 1074 	push	#<(___str_0+0)
                           000401  1075 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$316 ==.
      00A7BC 4B 81            [ 1] 1076 	push	#((___str_0+0) >> 8)
                           000403  1077 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$317 ==.
      00A7BE CD 84 F3         [ 4] 1078 	call	_assert_failed
      00A7C1 5B 06            [ 2] 1079 	addw	sp, #6
                           000408  1080 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$318 ==.
      00A7C3                       1081 00107$:
                           000408  1082 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$319 ==.
                                   1083 ;	drivers/src/stm8s_uart1.c: 335: UART1->CR4 |= UART1_CR4_LBDL;
      00A7C3 C6 52 37         [ 1] 1084 	ld	a, 0x5237
                           00040B  1085 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$320 ==.
                                   1086 ;	drivers/src/stm8s_uart1.c: 333: if (UART1_LINBreakDetectionLength != UART1_LINBREAKDETECTIONLENGTH_10BITS)
      00A7C6 0D 03            [ 1] 1087 	tnz	(0x03, sp)
      00A7C8 27 07            [ 1] 1088 	jreq	00102$
                           00040F  1089 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$321 ==.
                           00040F  1090 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$322 ==.
                                   1091 ;	drivers/src/stm8s_uart1.c: 335: UART1->CR4 |= UART1_CR4_LBDL;
      00A7CA AA 20            [ 1] 1092 	or	a, #0x20
      00A7CC C7 52 37         [ 1] 1093 	ld	0x5237, a
                           000414  1094 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$323 ==.
      00A7CF 20 05            [ 2] 1095 	jra	00104$
      00A7D1                       1096 00102$:
                           000416  1097 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$324 ==.
                           000416  1098 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$325 ==.
                                   1099 ;	drivers/src/stm8s_uart1.c: 339: UART1->CR4 &= ((uint8_t)~UART1_CR4_LBDL);
      00A7D1 A4 DF            [ 1] 1100 	and	a, #0xdf
      00A7D3 C7 52 37         [ 1] 1101 	ld	0x5237, a
                           00041B  1102 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$326 ==.
      00A7D6                       1103 00104$:
                           00041B  1104 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$327 ==.
                                   1105 ;	drivers/src/stm8s_uart1.c: 341: }
                           00041B  1106 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$328 ==.
                           00041B  1107 	XG$UART1_LINBreakDetectionConfig$0$0 ==.
      00A7D6 81               [ 4] 1108 	ret
                           00041C  1109 	Sstm8s_uart1$UART1_LINBreakDetectionConfig$329 ==.
                           00041C  1110 	Sstm8s_uart1$UART1_LINCmd$330 ==.
                                   1111 ;	drivers/src/stm8s_uart1.c: 349: void UART1_LINCmd(FunctionalState NewState)
                                   1112 ;	-----------------------------------------
                                   1113 ;	 function UART1_LINCmd
                                   1114 ;	-----------------------------------------
      00A7D7                       1115 _UART1_LINCmd:
                           00041C  1116 	Sstm8s_uart1$UART1_LINCmd$331 ==.
                           00041C  1117 	Sstm8s_uart1$UART1_LINCmd$332 ==.
                                   1118 ;	drivers/src/stm8s_uart1.c: 351: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
      00A7D7 0D 03            [ 1] 1119 	tnz	(0x03, sp)
      00A7D9 27 14            [ 1] 1120 	jreq	00107$
      00A7DB 7B 03            [ 1] 1121 	ld	a, (0x03, sp)
      00A7DD 4A               [ 1] 1122 	dec	a
      00A7DE 27 0F            [ 1] 1123 	jreq	00107$
                           000425  1124 	Sstm8s_uart1$UART1_LINCmd$333 ==.
      00A7E0 4B 5F            [ 1] 1125 	push	#0x5f
                           000427  1126 	Sstm8s_uart1$UART1_LINCmd$334 ==.
      00A7E2 4B 01            [ 1] 1127 	push	#0x01
                           000429  1128 	Sstm8s_uart1$UART1_LINCmd$335 ==.
      00A7E4 5F               [ 1] 1129 	clrw	x
      00A7E5 89               [ 2] 1130 	pushw	x
                           00042B  1131 	Sstm8s_uart1$UART1_LINCmd$336 ==.
      00A7E6 4B 5E            [ 1] 1132 	push	#<(___str_0+0)
                           00042D  1133 	Sstm8s_uart1$UART1_LINCmd$337 ==.
      00A7E8 4B 81            [ 1] 1134 	push	#((___str_0+0) >> 8)
                           00042F  1135 	Sstm8s_uart1$UART1_LINCmd$338 ==.
      00A7EA CD 84 F3         [ 4] 1136 	call	_assert_failed
      00A7ED 5B 06            [ 2] 1137 	addw	sp, #6
                           000434  1138 	Sstm8s_uart1$UART1_LINCmd$339 ==.
      00A7EF                       1139 00107$:
                           000434  1140 	Sstm8s_uart1$UART1_LINCmd$340 ==.
                                   1141 ;	drivers/src/stm8s_uart1.c: 356: UART1->CR3 |= UART1_CR3_LINEN;
      00A7EF C6 52 36         [ 1] 1142 	ld	a, 0x5236
                           000437  1143 	Sstm8s_uart1$UART1_LINCmd$341 ==.
                                   1144 ;	drivers/src/stm8s_uart1.c: 353: if (NewState != DISABLE)
      00A7F2 0D 03            [ 1] 1145 	tnz	(0x03, sp)
      00A7F4 27 07            [ 1] 1146 	jreq	00102$
                           00043B  1147 	Sstm8s_uart1$UART1_LINCmd$342 ==.
                           00043B  1148 	Sstm8s_uart1$UART1_LINCmd$343 ==.
                                   1149 ;	drivers/src/stm8s_uart1.c: 356: UART1->CR3 |= UART1_CR3_LINEN;
      00A7F6 AA 40            [ 1] 1150 	or	a, #0x40
      00A7F8 C7 52 36         [ 1] 1151 	ld	0x5236, a
                           000440  1152 	Sstm8s_uart1$UART1_LINCmd$344 ==.
      00A7FB 20 05            [ 2] 1153 	jra	00104$
      00A7FD                       1154 00102$:
                           000442  1155 	Sstm8s_uart1$UART1_LINCmd$345 ==.
                           000442  1156 	Sstm8s_uart1$UART1_LINCmd$346 ==.
                                   1157 ;	drivers/src/stm8s_uart1.c: 361: UART1->CR3 &= ((uint8_t)~UART1_CR3_LINEN);
      00A7FD A4 BF            [ 1] 1158 	and	a, #0xbf
      00A7FF C7 52 36         [ 1] 1159 	ld	0x5236, a
                           000447  1160 	Sstm8s_uart1$UART1_LINCmd$347 ==.
      00A802                       1161 00104$:
                           000447  1162 	Sstm8s_uart1$UART1_LINCmd$348 ==.
                                   1163 ;	drivers/src/stm8s_uart1.c: 363: }
                           000447  1164 	Sstm8s_uart1$UART1_LINCmd$349 ==.
                           000447  1165 	XG$UART1_LINCmd$0$0 ==.
      00A802 81               [ 4] 1166 	ret
                           000448  1167 	Sstm8s_uart1$UART1_LINCmd$350 ==.
                           000448  1168 	Sstm8s_uart1$UART1_SmartCardCmd$351 ==.
                                   1169 ;	drivers/src/stm8s_uart1.c: 371: void UART1_SmartCardCmd(FunctionalState NewState)
                                   1170 ;	-----------------------------------------
                                   1171 ;	 function UART1_SmartCardCmd
                                   1172 ;	-----------------------------------------
      00A803                       1173 _UART1_SmartCardCmd:
                           000448  1174 	Sstm8s_uart1$UART1_SmartCardCmd$352 ==.
                           000448  1175 	Sstm8s_uart1$UART1_SmartCardCmd$353 ==.
                                   1176 ;	drivers/src/stm8s_uart1.c: 373: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
      00A803 0D 03            [ 1] 1177 	tnz	(0x03, sp)
      00A805 27 14            [ 1] 1178 	jreq	00107$
      00A807 7B 03            [ 1] 1179 	ld	a, (0x03, sp)
      00A809 4A               [ 1] 1180 	dec	a
      00A80A 27 0F            [ 1] 1181 	jreq	00107$
                           000451  1182 	Sstm8s_uart1$UART1_SmartCardCmd$354 ==.
      00A80C 4B 75            [ 1] 1183 	push	#0x75
                           000453  1184 	Sstm8s_uart1$UART1_SmartCardCmd$355 ==.
      00A80E 4B 01            [ 1] 1185 	push	#0x01
                           000455  1186 	Sstm8s_uart1$UART1_SmartCardCmd$356 ==.
      00A810 5F               [ 1] 1187 	clrw	x
      00A811 89               [ 2] 1188 	pushw	x
                           000457  1189 	Sstm8s_uart1$UART1_SmartCardCmd$357 ==.
      00A812 4B 5E            [ 1] 1190 	push	#<(___str_0+0)
                           000459  1191 	Sstm8s_uart1$UART1_SmartCardCmd$358 ==.
      00A814 4B 81            [ 1] 1192 	push	#((___str_0+0) >> 8)
                           00045B  1193 	Sstm8s_uart1$UART1_SmartCardCmd$359 ==.
      00A816 CD 84 F3         [ 4] 1194 	call	_assert_failed
      00A819 5B 06            [ 2] 1195 	addw	sp, #6
                           000460  1196 	Sstm8s_uart1$UART1_SmartCardCmd$360 ==.
      00A81B                       1197 00107$:
                           000460  1198 	Sstm8s_uart1$UART1_SmartCardCmd$361 ==.
                                   1199 ;	drivers/src/stm8s_uart1.c: 378: UART1->CR5 |= UART1_CR5_SCEN;
      00A81B C6 52 38         [ 1] 1200 	ld	a, 0x5238
                           000463  1201 	Sstm8s_uart1$UART1_SmartCardCmd$362 ==.
                                   1202 ;	drivers/src/stm8s_uart1.c: 375: if (NewState != DISABLE)
      00A81E 0D 03            [ 1] 1203 	tnz	(0x03, sp)
      00A820 27 07            [ 1] 1204 	jreq	00102$
                           000467  1205 	Sstm8s_uart1$UART1_SmartCardCmd$363 ==.
                           000467  1206 	Sstm8s_uart1$UART1_SmartCardCmd$364 ==.
                                   1207 ;	drivers/src/stm8s_uart1.c: 378: UART1->CR5 |= UART1_CR5_SCEN;
      00A822 AA 20            [ 1] 1208 	or	a, #0x20
      00A824 C7 52 38         [ 1] 1209 	ld	0x5238, a
                           00046C  1210 	Sstm8s_uart1$UART1_SmartCardCmd$365 ==.
      00A827 20 05            [ 2] 1211 	jra	00104$
      00A829                       1212 00102$:
                           00046E  1213 	Sstm8s_uart1$UART1_SmartCardCmd$366 ==.
                           00046E  1214 	Sstm8s_uart1$UART1_SmartCardCmd$367 ==.
                                   1215 ;	drivers/src/stm8s_uart1.c: 383: UART1->CR5 &= ((uint8_t)(~UART1_CR5_SCEN));
      00A829 A4 DF            [ 1] 1216 	and	a, #0xdf
      00A82B C7 52 38         [ 1] 1217 	ld	0x5238, a
                           000473  1218 	Sstm8s_uart1$UART1_SmartCardCmd$368 ==.
      00A82E                       1219 00104$:
                           000473  1220 	Sstm8s_uart1$UART1_SmartCardCmd$369 ==.
                                   1221 ;	drivers/src/stm8s_uart1.c: 385: }
                           000473  1222 	Sstm8s_uart1$UART1_SmartCardCmd$370 ==.
                           000473  1223 	XG$UART1_SmartCardCmd$0$0 ==.
      00A82E 81               [ 4] 1224 	ret
                           000474  1225 	Sstm8s_uart1$UART1_SmartCardCmd$371 ==.
                           000474  1226 	Sstm8s_uart1$UART1_SmartCardNACKCmd$372 ==.
                                   1227 ;	drivers/src/stm8s_uart1.c: 394: void UART1_SmartCardNACKCmd(FunctionalState NewState)
                                   1228 ;	-----------------------------------------
                                   1229 ;	 function UART1_SmartCardNACKCmd
                                   1230 ;	-----------------------------------------
      00A82F                       1231 _UART1_SmartCardNACKCmd:
                           000474  1232 	Sstm8s_uart1$UART1_SmartCardNACKCmd$373 ==.
                           000474  1233 	Sstm8s_uart1$UART1_SmartCardNACKCmd$374 ==.
                                   1234 ;	drivers/src/stm8s_uart1.c: 396: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
      00A82F 0D 03            [ 1] 1235 	tnz	(0x03, sp)
      00A831 27 14            [ 1] 1236 	jreq	00107$
      00A833 7B 03            [ 1] 1237 	ld	a, (0x03, sp)
      00A835 4A               [ 1] 1238 	dec	a
      00A836 27 0F            [ 1] 1239 	jreq	00107$
                           00047D  1240 	Sstm8s_uart1$UART1_SmartCardNACKCmd$375 ==.
      00A838 4B 8C            [ 1] 1241 	push	#0x8c
                           00047F  1242 	Sstm8s_uart1$UART1_SmartCardNACKCmd$376 ==.
      00A83A 4B 01            [ 1] 1243 	push	#0x01
                           000481  1244 	Sstm8s_uart1$UART1_SmartCardNACKCmd$377 ==.
      00A83C 5F               [ 1] 1245 	clrw	x
      00A83D 89               [ 2] 1246 	pushw	x
                           000483  1247 	Sstm8s_uart1$UART1_SmartCardNACKCmd$378 ==.
      00A83E 4B 5E            [ 1] 1248 	push	#<(___str_0+0)
                           000485  1249 	Sstm8s_uart1$UART1_SmartCardNACKCmd$379 ==.
      00A840 4B 81            [ 1] 1250 	push	#((___str_0+0) >> 8)
                           000487  1251 	Sstm8s_uart1$UART1_SmartCardNACKCmd$380 ==.
      00A842 CD 84 F3         [ 4] 1252 	call	_assert_failed
      00A845 5B 06            [ 2] 1253 	addw	sp, #6
                           00048C  1254 	Sstm8s_uart1$UART1_SmartCardNACKCmd$381 ==.
      00A847                       1255 00107$:
                           00048C  1256 	Sstm8s_uart1$UART1_SmartCardNACKCmd$382 ==.
                                   1257 ;	drivers/src/stm8s_uart1.c: 401: UART1->CR5 |= UART1_CR5_NACK;
      00A847 C6 52 38         [ 1] 1258 	ld	a, 0x5238
                           00048F  1259 	Sstm8s_uart1$UART1_SmartCardNACKCmd$383 ==.
                                   1260 ;	drivers/src/stm8s_uart1.c: 398: if (NewState != DISABLE)
      00A84A 0D 03            [ 1] 1261 	tnz	(0x03, sp)
      00A84C 27 07            [ 1] 1262 	jreq	00102$
                           000493  1263 	Sstm8s_uart1$UART1_SmartCardNACKCmd$384 ==.
                           000493  1264 	Sstm8s_uart1$UART1_SmartCardNACKCmd$385 ==.
                                   1265 ;	drivers/src/stm8s_uart1.c: 401: UART1->CR5 |= UART1_CR5_NACK;
      00A84E AA 10            [ 1] 1266 	or	a, #0x10
      00A850 C7 52 38         [ 1] 1267 	ld	0x5238, a
                           000498  1268 	Sstm8s_uart1$UART1_SmartCardNACKCmd$386 ==.
      00A853 20 05            [ 2] 1269 	jra	00104$
      00A855                       1270 00102$:
                           00049A  1271 	Sstm8s_uart1$UART1_SmartCardNACKCmd$387 ==.
                           00049A  1272 	Sstm8s_uart1$UART1_SmartCardNACKCmd$388 ==.
                                   1273 ;	drivers/src/stm8s_uart1.c: 406: UART1->CR5 &= ((uint8_t)~(UART1_CR5_NACK));
      00A855 A4 EF            [ 1] 1274 	and	a, #0xef
      00A857 C7 52 38         [ 1] 1275 	ld	0x5238, a
                           00049F  1276 	Sstm8s_uart1$UART1_SmartCardNACKCmd$389 ==.
      00A85A                       1277 00104$:
                           00049F  1278 	Sstm8s_uart1$UART1_SmartCardNACKCmd$390 ==.
                                   1279 ;	drivers/src/stm8s_uart1.c: 408: }
                           00049F  1280 	Sstm8s_uart1$UART1_SmartCardNACKCmd$391 ==.
                           00049F  1281 	XG$UART1_SmartCardNACKCmd$0$0 ==.
      00A85A 81               [ 4] 1282 	ret
                           0004A0  1283 	Sstm8s_uart1$UART1_SmartCardNACKCmd$392 ==.
                           0004A0  1284 	Sstm8s_uart1$UART1_WakeUpConfig$393 ==.
                                   1285 ;	drivers/src/stm8s_uart1.c: 416: void UART1_WakeUpConfig(UART1_WakeUp_TypeDef UART1_WakeUp)
                                   1286 ;	-----------------------------------------
                                   1287 ;	 function UART1_WakeUpConfig
                                   1288 ;	-----------------------------------------
      00A85B                       1289 _UART1_WakeUpConfig:
                           0004A0  1290 	Sstm8s_uart1$UART1_WakeUpConfig$394 ==.
                           0004A0  1291 	Sstm8s_uart1$UART1_WakeUpConfig$395 ==.
                                   1292 ;	drivers/src/stm8s_uart1.c: 418: assert_param(IS_UART1_WAKEUP_OK(UART1_WakeUp));
      00A85B 0D 03            [ 1] 1293 	tnz	(0x03, sp)
      00A85D 27 15            [ 1] 1294 	jreq	00104$
      00A85F 7B 03            [ 1] 1295 	ld	a, (0x03, sp)
      00A861 A1 08            [ 1] 1296 	cp	a, #0x08
      00A863 27 0F            [ 1] 1297 	jreq	00104$
                           0004AA  1298 	Sstm8s_uart1$UART1_WakeUpConfig$396 ==.
      00A865 4B A2            [ 1] 1299 	push	#0xa2
                           0004AC  1300 	Sstm8s_uart1$UART1_WakeUpConfig$397 ==.
      00A867 4B 01            [ 1] 1301 	push	#0x01
                           0004AE  1302 	Sstm8s_uart1$UART1_WakeUpConfig$398 ==.
      00A869 5F               [ 1] 1303 	clrw	x
      00A86A 89               [ 2] 1304 	pushw	x
                           0004B0  1305 	Sstm8s_uart1$UART1_WakeUpConfig$399 ==.
      00A86B 4B 5E            [ 1] 1306 	push	#<(___str_0+0)
                           0004B2  1307 	Sstm8s_uart1$UART1_WakeUpConfig$400 ==.
      00A86D 4B 81            [ 1] 1308 	push	#((___str_0+0) >> 8)
                           0004B4  1309 	Sstm8s_uart1$UART1_WakeUpConfig$401 ==.
      00A86F CD 84 F3         [ 4] 1310 	call	_assert_failed
      00A872 5B 06            [ 2] 1311 	addw	sp, #6
                           0004B9  1312 	Sstm8s_uart1$UART1_WakeUpConfig$402 ==.
      00A874                       1313 00104$:
                           0004B9  1314 	Sstm8s_uart1$UART1_WakeUpConfig$403 ==.
                                   1315 ;	drivers/src/stm8s_uart1.c: 420: UART1->CR1 &= ((uint8_t)~UART1_CR1_WAKE);
      00A874 72 17 52 34      [ 1] 1316 	bres	21044, #3
                           0004BD  1317 	Sstm8s_uart1$UART1_WakeUpConfig$404 ==.
                                   1318 ;	drivers/src/stm8s_uart1.c: 421: UART1->CR1 |= (uint8_t)UART1_WakeUp;
      00A878 C6 52 34         [ 1] 1319 	ld	a, 0x5234
      00A87B 1A 03            [ 1] 1320 	or	a, (0x03, sp)
      00A87D C7 52 34         [ 1] 1321 	ld	0x5234, a
                           0004C5  1322 	Sstm8s_uart1$UART1_WakeUpConfig$405 ==.
                                   1323 ;	drivers/src/stm8s_uart1.c: 422: }
                           0004C5  1324 	Sstm8s_uart1$UART1_WakeUpConfig$406 ==.
                           0004C5  1325 	XG$UART1_WakeUpConfig$0$0 ==.
      00A880 81               [ 4] 1326 	ret
                           0004C6  1327 	Sstm8s_uart1$UART1_WakeUpConfig$407 ==.
                           0004C6  1328 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$408 ==.
                                   1329 ;	drivers/src/stm8s_uart1.c: 430: void UART1_ReceiverWakeUpCmd(FunctionalState NewState)
                                   1330 ;	-----------------------------------------
                                   1331 ;	 function UART1_ReceiverWakeUpCmd
                                   1332 ;	-----------------------------------------
      00A881                       1333 _UART1_ReceiverWakeUpCmd:
                           0004C6  1334 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$409 ==.
                           0004C6  1335 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$410 ==.
                                   1336 ;	drivers/src/stm8s_uart1.c: 432: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
      00A881 0D 03            [ 1] 1337 	tnz	(0x03, sp)
      00A883 27 14            [ 1] 1338 	jreq	00107$
      00A885 7B 03            [ 1] 1339 	ld	a, (0x03, sp)
      00A887 4A               [ 1] 1340 	dec	a
      00A888 27 0F            [ 1] 1341 	jreq	00107$
                           0004CF  1342 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$411 ==.
      00A88A 4B B0            [ 1] 1343 	push	#0xb0
                           0004D1  1344 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$412 ==.
      00A88C 4B 01            [ 1] 1345 	push	#0x01
                           0004D3  1346 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$413 ==.
      00A88E 5F               [ 1] 1347 	clrw	x
      00A88F 89               [ 2] 1348 	pushw	x
                           0004D5  1349 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$414 ==.
      00A890 4B 5E            [ 1] 1350 	push	#<(___str_0+0)
                           0004D7  1351 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$415 ==.
      00A892 4B 81            [ 1] 1352 	push	#((___str_0+0) >> 8)
                           0004D9  1353 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$416 ==.
      00A894 CD 84 F3         [ 4] 1354 	call	_assert_failed
      00A897 5B 06            [ 2] 1355 	addw	sp, #6
                           0004DE  1356 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$417 ==.
      00A899                       1357 00107$:
                           0004DE  1358 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$418 ==.
                                   1359 ;	drivers/src/stm8s_uart1.c: 437: UART1->CR2 |= UART1_CR2_RWU;
      00A899 C6 52 35         [ 1] 1360 	ld	a, 0x5235
                           0004E1  1361 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$419 ==.
                                   1362 ;	drivers/src/stm8s_uart1.c: 434: if (NewState != DISABLE)
      00A89C 0D 03            [ 1] 1363 	tnz	(0x03, sp)
      00A89E 27 07            [ 1] 1364 	jreq	00102$
                           0004E5  1365 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$420 ==.
                           0004E5  1366 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$421 ==.
                                   1367 ;	drivers/src/stm8s_uart1.c: 437: UART1->CR2 |= UART1_CR2_RWU;
      00A8A0 AA 02            [ 1] 1368 	or	a, #0x02
      00A8A2 C7 52 35         [ 1] 1369 	ld	0x5235, a
                           0004EA  1370 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$422 ==.
      00A8A5 20 05            [ 2] 1371 	jra	00104$
      00A8A7                       1372 00102$:
                           0004EC  1373 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$423 ==.
                           0004EC  1374 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$424 ==.
                                   1375 ;	drivers/src/stm8s_uart1.c: 442: UART1->CR2 &= ((uint8_t)~UART1_CR2_RWU);
      00A8A7 A4 FD            [ 1] 1376 	and	a, #0xfd
      00A8A9 C7 52 35         [ 1] 1377 	ld	0x5235, a
                           0004F1  1378 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$425 ==.
      00A8AC                       1379 00104$:
                           0004F1  1380 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$426 ==.
                                   1381 ;	drivers/src/stm8s_uart1.c: 444: }
                           0004F1  1382 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$427 ==.
                           0004F1  1383 	XG$UART1_ReceiverWakeUpCmd$0$0 ==.
      00A8AC 81               [ 4] 1384 	ret
                           0004F2  1385 	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$428 ==.
                           0004F2  1386 	Sstm8s_uart1$UART1_ReceiveData8$429 ==.
                                   1387 ;	drivers/src/stm8s_uart1.c: 451: uint8_t UART1_ReceiveData8(void)
                                   1388 ;	-----------------------------------------
                                   1389 ;	 function UART1_ReceiveData8
                                   1390 ;	-----------------------------------------
      00A8AD                       1391 _UART1_ReceiveData8:
                           0004F2  1392 	Sstm8s_uart1$UART1_ReceiveData8$430 ==.
                           0004F2  1393 	Sstm8s_uart1$UART1_ReceiveData8$431 ==.
                                   1394 ;	drivers/src/stm8s_uart1.c: 453: return ((uint8_t)UART1->DR);
      00A8AD C6 52 31         [ 1] 1395 	ld	a, 0x5231
                           0004F5  1396 	Sstm8s_uart1$UART1_ReceiveData8$432 ==.
                                   1397 ;	drivers/src/stm8s_uart1.c: 454: }
                           0004F5  1398 	Sstm8s_uart1$UART1_ReceiveData8$433 ==.
                           0004F5  1399 	XG$UART1_ReceiveData8$0$0 ==.
      00A8B0 81               [ 4] 1400 	ret
                           0004F6  1401 	Sstm8s_uart1$UART1_ReceiveData8$434 ==.
                           0004F6  1402 	Sstm8s_uart1$UART1_ReceiveData9$435 ==.
                                   1403 ;	drivers/src/stm8s_uart1.c: 461: uint16_t UART1_ReceiveData9(void)
                                   1404 ;	-----------------------------------------
                                   1405 ;	 function UART1_ReceiveData9
                                   1406 ;	-----------------------------------------
      00A8B1                       1407 _UART1_ReceiveData9:
                           0004F6  1408 	Sstm8s_uart1$UART1_ReceiveData9$436 ==.
      00A8B1 89               [ 2] 1409 	pushw	x
                           0004F7  1410 	Sstm8s_uart1$UART1_ReceiveData9$437 ==.
                           0004F7  1411 	Sstm8s_uart1$UART1_ReceiveData9$438 ==.
                                   1412 ;	drivers/src/stm8s_uart1.c: 465: temp = (uint16_t)(((uint16_t)( (uint16_t)UART1->CR1 & (uint16_t)UART1_CR1_R8)) << 1);
      00A8B2 C6 52 34         [ 1] 1413 	ld	a, 0x5234
      00A8B5 A4 80            [ 1] 1414 	and	a, #0x80
      00A8B7 97               [ 1] 1415 	ld	xl, a
      00A8B8 4F               [ 1] 1416 	clr	a
      00A8B9 95               [ 1] 1417 	ld	xh, a
      00A8BA 58               [ 2] 1418 	sllw	x
      00A8BB 1F 01            [ 2] 1419 	ldw	(0x01, sp), x
                           000502  1420 	Sstm8s_uart1$UART1_ReceiveData9$439 ==.
                                   1421 ;	drivers/src/stm8s_uart1.c: 466: return (uint16_t)( (((uint16_t) UART1->DR) | temp ) & ((uint16_t)0x01FF));
      00A8BD C6 52 31         [ 1] 1422 	ld	a, 0x5231
      00A8C0 5F               [ 1] 1423 	clrw	x
      00A8C1 1A 02            [ 1] 1424 	or	a, (0x02, sp)
      00A8C3 02               [ 1] 1425 	rlwa	x
      00A8C4 1A 01            [ 1] 1426 	or	a, (0x01, sp)
      00A8C6 A4 01            [ 1] 1427 	and	a, #0x01
      00A8C8 95               [ 1] 1428 	ld	xh, a
                           00050E  1429 	Sstm8s_uart1$UART1_ReceiveData9$440 ==.
                                   1430 ;	drivers/src/stm8s_uart1.c: 467: }
      00A8C9 5B 02            [ 2] 1431 	addw	sp, #2
                           000510  1432 	Sstm8s_uart1$UART1_ReceiveData9$441 ==.
                           000510  1433 	Sstm8s_uart1$UART1_ReceiveData9$442 ==.
                           000510  1434 	XG$UART1_ReceiveData9$0$0 ==.
      00A8CB 81               [ 4] 1435 	ret
                           000511  1436 	Sstm8s_uart1$UART1_ReceiveData9$443 ==.
                           000511  1437 	Sstm8s_uart1$UART1_SendData8$444 ==.
                                   1438 ;	drivers/src/stm8s_uart1.c: 474: void UART1_SendData8(uint8_t Data)
                                   1439 ;	-----------------------------------------
                                   1440 ;	 function UART1_SendData8
                                   1441 ;	-----------------------------------------
      00A8CC                       1442 _UART1_SendData8:
                           000511  1443 	Sstm8s_uart1$UART1_SendData8$445 ==.
                           000511  1444 	Sstm8s_uart1$UART1_SendData8$446 ==.
                                   1445 ;	drivers/src/stm8s_uart1.c: 477: UART1->DR = Data;
      00A8CC AE 52 31         [ 2] 1446 	ldw	x, #0x5231
      00A8CF 7B 03            [ 1] 1447 	ld	a, (0x03, sp)
      00A8D1 F7               [ 1] 1448 	ld	(x), a
                           000517  1449 	Sstm8s_uart1$UART1_SendData8$447 ==.
                                   1450 ;	drivers/src/stm8s_uart1.c: 478: }
                           000517  1451 	Sstm8s_uart1$UART1_SendData8$448 ==.
                           000517  1452 	XG$UART1_SendData8$0$0 ==.
      00A8D2 81               [ 4] 1453 	ret
                           000518  1454 	Sstm8s_uart1$UART1_SendData8$449 ==.
                           000518  1455 	Sstm8s_uart1$UART1_SendData9$450 ==.
                                   1456 ;	drivers/src/stm8s_uart1.c: 486: void UART1_SendData9(uint16_t Data)
                                   1457 ;	-----------------------------------------
                                   1458 ;	 function UART1_SendData9
                                   1459 ;	-----------------------------------------
      00A8D3                       1460 _UART1_SendData9:
                           000518  1461 	Sstm8s_uart1$UART1_SendData9$451 ==.
      00A8D3 88               [ 1] 1462 	push	a
                           000519  1463 	Sstm8s_uart1$UART1_SendData9$452 ==.
                           000519  1464 	Sstm8s_uart1$UART1_SendData9$453 ==.
                                   1465 ;	drivers/src/stm8s_uart1.c: 489: UART1->CR1 &= ((uint8_t)~UART1_CR1_T8);
      00A8D4 72 1D 52 34      [ 1] 1466 	bres	21044, #6
                           00051D  1467 	Sstm8s_uart1$UART1_SendData9$454 ==.
                                   1468 ;	drivers/src/stm8s_uart1.c: 491: UART1->CR1 |= (uint8_t)(((uint8_t)(Data >> 2)) & UART1_CR1_T8);
      00A8D8 C6 52 34         [ 1] 1469 	ld	a, 0x5234
      00A8DB 6B 01            [ 1] 1470 	ld	(0x01, sp), a
      00A8DD 1E 04            [ 2] 1471 	ldw	x, (0x04, sp)
      00A8DF 54               [ 2] 1472 	srlw	x
      00A8E0 54               [ 2] 1473 	srlw	x
      00A8E1 9F               [ 1] 1474 	ld	a, xl
      00A8E2 A4 40            [ 1] 1475 	and	a, #0x40
      00A8E4 1A 01            [ 1] 1476 	or	a, (0x01, sp)
      00A8E6 C7 52 34         [ 1] 1477 	ld	0x5234, a
                           00052E  1478 	Sstm8s_uart1$UART1_SendData9$455 ==.
                                   1479 ;	drivers/src/stm8s_uart1.c: 493: UART1->DR   = (uint8_t)(Data);
      00A8E9 7B 05            [ 1] 1480 	ld	a, (0x05, sp)
      00A8EB C7 52 31         [ 1] 1481 	ld	0x5231, a
                           000533  1482 	Sstm8s_uart1$UART1_SendData9$456 ==.
                                   1483 ;	drivers/src/stm8s_uart1.c: 494: }
      00A8EE 84               [ 1] 1484 	pop	a
                           000534  1485 	Sstm8s_uart1$UART1_SendData9$457 ==.
                           000534  1486 	Sstm8s_uart1$UART1_SendData9$458 ==.
                           000534  1487 	XG$UART1_SendData9$0$0 ==.
      00A8EF 81               [ 4] 1488 	ret
                           000535  1489 	Sstm8s_uart1$UART1_SendData9$459 ==.
                           000535  1490 	Sstm8s_uart1$UART1_SendBreak$460 ==.
                                   1491 ;	drivers/src/stm8s_uart1.c: 501: void UART1_SendBreak(void)
                                   1492 ;	-----------------------------------------
                                   1493 ;	 function UART1_SendBreak
                                   1494 ;	-----------------------------------------
      00A8F0                       1495 _UART1_SendBreak:
                           000535  1496 	Sstm8s_uart1$UART1_SendBreak$461 ==.
                           000535  1497 	Sstm8s_uart1$UART1_SendBreak$462 ==.
                                   1498 ;	drivers/src/stm8s_uart1.c: 503: UART1->CR2 |= UART1_CR2_SBK;
      00A8F0 72 10 52 35      [ 1] 1499 	bset	21045, #0
                           000539  1500 	Sstm8s_uart1$UART1_SendBreak$463 ==.
                                   1501 ;	drivers/src/stm8s_uart1.c: 504: }
                           000539  1502 	Sstm8s_uart1$UART1_SendBreak$464 ==.
                           000539  1503 	XG$UART1_SendBreak$0$0 ==.
      00A8F4 81               [ 4] 1504 	ret
                           00053A  1505 	Sstm8s_uart1$UART1_SendBreak$465 ==.
                           00053A  1506 	Sstm8s_uart1$UART1_SetAddress$466 ==.
                                   1507 ;	drivers/src/stm8s_uart1.c: 511: void UART1_SetAddress(uint8_t UART1_Address)
                                   1508 ;	-----------------------------------------
                                   1509 ;	 function UART1_SetAddress
                                   1510 ;	-----------------------------------------
      00A8F5                       1511 _UART1_SetAddress:
                           00053A  1512 	Sstm8s_uart1$UART1_SetAddress$467 ==.
                           00053A  1513 	Sstm8s_uart1$UART1_SetAddress$468 ==.
                                   1514 ;	drivers/src/stm8s_uart1.c: 514: assert_param(IS_UART1_ADDRESS_OK(UART1_Address));
      00A8F5 7B 03            [ 1] 1515 	ld	a, (0x03, sp)
      00A8F7 A1 10            [ 1] 1516 	cp	a, #0x10
      00A8F9 25 0F            [ 1] 1517 	jrc	00104$
      00A8FB 4B 02            [ 1] 1518 	push	#0x02
                           000542  1519 	Sstm8s_uart1$UART1_SetAddress$469 ==.
      00A8FD 4B 02            [ 1] 1520 	push	#0x02
                           000544  1521 	Sstm8s_uart1$UART1_SetAddress$470 ==.
      00A8FF 5F               [ 1] 1522 	clrw	x
      00A900 89               [ 2] 1523 	pushw	x
                           000546  1524 	Sstm8s_uart1$UART1_SetAddress$471 ==.
      00A901 4B 5E            [ 1] 1525 	push	#<(___str_0+0)
                           000548  1526 	Sstm8s_uart1$UART1_SetAddress$472 ==.
      00A903 4B 81            [ 1] 1527 	push	#((___str_0+0) >> 8)
                           00054A  1528 	Sstm8s_uart1$UART1_SetAddress$473 ==.
      00A905 CD 84 F3         [ 4] 1529 	call	_assert_failed
      00A908 5B 06            [ 2] 1530 	addw	sp, #6
                           00054F  1531 	Sstm8s_uart1$UART1_SetAddress$474 ==.
      00A90A                       1532 00104$:
                           00054F  1533 	Sstm8s_uart1$UART1_SetAddress$475 ==.
                                   1534 ;	drivers/src/stm8s_uart1.c: 517: UART1->CR4 &= ((uint8_t)~UART1_CR4_ADD);
      00A90A C6 52 37         [ 1] 1535 	ld	a, 0x5237
      00A90D A4 F0            [ 1] 1536 	and	a, #0xf0
      00A90F C7 52 37         [ 1] 1537 	ld	0x5237, a
                           000557  1538 	Sstm8s_uart1$UART1_SetAddress$476 ==.
                                   1539 ;	drivers/src/stm8s_uart1.c: 519: UART1->CR4 |= UART1_Address;
      00A912 C6 52 37         [ 1] 1540 	ld	a, 0x5237
      00A915 1A 03            [ 1] 1541 	or	a, (0x03, sp)
      00A917 C7 52 37         [ 1] 1542 	ld	0x5237, a
                           00055F  1543 	Sstm8s_uart1$UART1_SetAddress$477 ==.
                                   1544 ;	drivers/src/stm8s_uart1.c: 520: }
                           00055F  1545 	Sstm8s_uart1$UART1_SetAddress$478 ==.
                           00055F  1546 	XG$UART1_SetAddress$0$0 ==.
      00A91A 81               [ 4] 1547 	ret
                           000560  1548 	Sstm8s_uart1$UART1_SetAddress$479 ==.
                           000560  1549 	Sstm8s_uart1$UART1_SetGuardTime$480 ==.
                                   1550 ;	drivers/src/stm8s_uart1.c: 528: void UART1_SetGuardTime(uint8_t UART1_GuardTime)
                                   1551 ;	-----------------------------------------
                                   1552 ;	 function UART1_SetGuardTime
                                   1553 ;	-----------------------------------------
      00A91B                       1554 _UART1_SetGuardTime:
                           000560  1555 	Sstm8s_uart1$UART1_SetGuardTime$481 ==.
                           000560  1556 	Sstm8s_uart1$UART1_SetGuardTime$482 ==.
                                   1557 ;	drivers/src/stm8s_uart1.c: 531: UART1->GTR = UART1_GuardTime;
      00A91B AE 52 39         [ 2] 1558 	ldw	x, #0x5239
      00A91E 7B 03            [ 1] 1559 	ld	a, (0x03, sp)
      00A920 F7               [ 1] 1560 	ld	(x), a
                           000566  1561 	Sstm8s_uart1$UART1_SetGuardTime$483 ==.
                                   1562 ;	drivers/src/stm8s_uart1.c: 532: }
                           000566  1563 	Sstm8s_uart1$UART1_SetGuardTime$484 ==.
                           000566  1564 	XG$UART1_SetGuardTime$0$0 ==.
      00A921 81               [ 4] 1565 	ret
                           000567  1566 	Sstm8s_uart1$UART1_SetGuardTime$485 ==.
                           000567  1567 	Sstm8s_uart1$UART1_SetPrescaler$486 ==.
                                   1568 ;	drivers/src/stm8s_uart1.c: 556: void UART1_SetPrescaler(uint8_t UART1_Prescaler)
                                   1569 ;	-----------------------------------------
                                   1570 ;	 function UART1_SetPrescaler
                                   1571 ;	-----------------------------------------
      00A922                       1572 _UART1_SetPrescaler:
                           000567  1573 	Sstm8s_uart1$UART1_SetPrescaler$487 ==.
                           000567  1574 	Sstm8s_uart1$UART1_SetPrescaler$488 ==.
                                   1575 ;	drivers/src/stm8s_uart1.c: 559: UART1->PSCR = UART1_Prescaler;
      00A922 AE 52 3A         [ 2] 1576 	ldw	x, #0x523a
      00A925 7B 03            [ 1] 1577 	ld	a, (0x03, sp)
      00A927 F7               [ 1] 1578 	ld	(x), a
                           00056D  1579 	Sstm8s_uart1$UART1_SetPrescaler$489 ==.
                                   1580 ;	drivers/src/stm8s_uart1.c: 560: }
                           00056D  1581 	Sstm8s_uart1$UART1_SetPrescaler$490 ==.
                           00056D  1582 	XG$UART1_SetPrescaler$0$0 ==.
      00A928 81               [ 4] 1583 	ret
                           00056E  1584 	Sstm8s_uart1$UART1_SetPrescaler$491 ==.
                           00056E  1585 	Sstm8s_uart1$UART1_GetFlagStatus$492 ==.
                                   1586 ;	drivers/src/stm8s_uart1.c: 568: FlagStatus UART1_GetFlagStatus(UART1_Flag_TypeDef UART1_FLAG)
                                   1587 ;	-----------------------------------------
                                   1588 ;	 function UART1_GetFlagStatus
                                   1589 ;	-----------------------------------------
      00A929                       1590 _UART1_GetFlagStatus:
                           00056E  1591 	Sstm8s_uart1$UART1_GetFlagStatus$493 ==.
      00A929 89               [ 2] 1592 	pushw	x
                           00056F  1593 	Sstm8s_uart1$UART1_GetFlagStatus$494 ==.
                           00056F  1594 	Sstm8s_uart1$UART1_GetFlagStatus$495 ==.
                                   1595 ;	drivers/src/stm8s_uart1.c: 573: assert_param(IS_UART1_FLAG_OK(UART1_FLAG));
      00A92A 1E 05            [ 2] 1596 	ldw	x, (0x05, sp)
      00A92C A3 01 01         [ 2] 1597 	cpw	x, #0x0101
      00A92F 26 05            [ 1] 1598 	jrne	00223$
      00A931 A6 01            [ 1] 1599 	ld	a, #0x01
      00A933 6B 01            [ 1] 1600 	ld	(0x01, sp), a
      00A935 C5                    1601 	.byte 0xc5
      00A936                       1602 00223$:
      00A936 0F 01            [ 1] 1603 	clr	(0x01, sp)
      00A938                       1604 00224$:
                           00057D  1605 	Sstm8s_uart1$UART1_GetFlagStatus$496 ==.
      00A938 A3 02 10         [ 2] 1606 	cpw	x, #0x0210
      00A93B 26 03            [ 1] 1607 	jrne	00226$
      00A93D A6 01            [ 1] 1608 	ld	a, #0x01
      00A93F 21                    1609 	.byte 0x21
      00A940                       1610 00226$:
      00A940 4F               [ 1] 1611 	clr	a
      00A941                       1612 00227$:
                           000586  1613 	Sstm8s_uart1$UART1_GetFlagStatus$497 ==.
      00A941 A3 00 80         [ 2] 1614 	cpw	x, #0x0080
      00A944 26 03            [ 1] 1615 	jrne	00229$
      00A946 CC A9 85         [ 2] 1616 	jp	00119$
      00A949                       1617 00229$:
                           00058E  1618 	Sstm8s_uart1$UART1_GetFlagStatus$498 ==.
      00A949 A3 00 40         [ 2] 1619 	cpw	x, #0x0040
      00A94C 26 03            [ 1] 1620 	jrne	00232$
      00A94E CC A9 85         [ 2] 1621 	jp	00119$
      00A951                       1622 00232$:
                           000596  1623 	Sstm8s_uart1$UART1_GetFlagStatus$499 ==.
      00A951 A3 00 20         [ 2] 1624 	cpw	x, #0x0020
      00A954 27 2F            [ 1] 1625 	jreq	00119$
                           00059B  1626 	Sstm8s_uart1$UART1_GetFlagStatus$500 ==.
      00A956 A3 00 10         [ 2] 1627 	cpw	x, #0x0010
      00A959 27 2A            [ 1] 1628 	jreq	00119$
                           0005A0  1629 	Sstm8s_uart1$UART1_GetFlagStatus$501 ==.
      00A95B A3 00 08         [ 2] 1630 	cpw	x, #0x0008
      00A95E 27 25            [ 1] 1631 	jreq	00119$
                           0005A5  1632 	Sstm8s_uart1$UART1_GetFlagStatus$502 ==.
      00A960 A3 00 04         [ 2] 1633 	cpw	x, #0x0004
      00A963 27 20            [ 1] 1634 	jreq	00119$
                           0005AA  1635 	Sstm8s_uart1$UART1_GetFlagStatus$503 ==.
      00A965 A3 00 02         [ 2] 1636 	cpw	x, #0x0002
      00A968 27 1B            [ 1] 1637 	jreq	00119$
                           0005AF  1638 	Sstm8s_uart1$UART1_GetFlagStatus$504 ==.
      00A96A 5A               [ 2] 1639 	decw	x
      00A96B 27 18            [ 1] 1640 	jreq	00119$
                           0005B2  1641 	Sstm8s_uart1$UART1_GetFlagStatus$505 ==.
      00A96D 0D 01            [ 1] 1642 	tnz	(0x01, sp)
      00A96F 26 14            [ 1] 1643 	jrne	00119$
      00A971 4D               [ 1] 1644 	tnz	a
      00A972 26 11            [ 1] 1645 	jrne	00119$
      00A974 88               [ 1] 1646 	push	a
                           0005BA  1647 	Sstm8s_uart1$UART1_GetFlagStatus$506 ==.
      00A975 4B 3D            [ 1] 1648 	push	#0x3d
                           0005BC  1649 	Sstm8s_uart1$UART1_GetFlagStatus$507 ==.
      00A977 4B 02            [ 1] 1650 	push	#0x02
                           0005BE  1651 	Sstm8s_uart1$UART1_GetFlagStatus$508 ==.
      00A979 5F               [ 1] 1652 	clrw	x
      00A97A 89               [ 2] 1653 	pushw	x
                           0005C0  1654 	Sstm8s_uart1$UART1_GetFlagStatus$509 ==.
      00A97B 4B 5E            [ 1] 1655 	push	#<(___str_0+0)
                           0005C2  1656 	Sstm8s_uart1$UART1_GetFlagStatus$510 ==.
      00A97D 4B 81            [ 1] 1657 	push	#((___str_0+0) >> 8)
                           0005C4  1658 	Sstm8s_uart1$UART1_GetFlagStatus$511 ==.
      00A97F CD 84 F3         [ 4] 1659 	call	_assert_failed
      00A982 5B 06            [ 2] 1660 	addw	sp, #6
                           0005C9  1661 	Sstm8s_uart1$UART1_GetFlagStatus$512 ==.
      00A984 84               [ 1] 1662 	pop	a
                           0005CA  1663 	Sstm8s_uart1$UART1_GetFlagStatus$513 ==.
      00A985                       1664 00119$:
                           0005CA  1665 	Sstm8s_uart1$UART1_GetFlagStatus$514 ==.
                                   1666 ;	drivers/src/stm8s_uart1.c: 579: if ((UART1->CR4 & (uint8_t)UART1_FLAG) != (uint8_t)0x00)
      00A985 88               [ 1] 1667 	push	a
                           0005CB  1668 	Sstm8s_uart1$UART1_GetFlagStatus$515 ==.
      00A986 7B 07            [ 1] 1669 	ld	a, (0x07, sp)
      00A988 6B 03            [ 1] 1670 	ld	(0x03, sp), a
      00A98A 84               [ 1] 1671 	pop	a
                           0005D0  1672 	Sstm8s_uart1$UART1_GetFlagStatus$516 ==.
                           0005D0  1673 	Sstm8s_uart1$UART1_GetFlagStatus$517 ==.
                                   1674 ;	drivers/src/stm8s_uart1.c: 577: if (UART1_FLAG == UART1_FLAG_LBDF)
      00A98B 4D               [ 1] 1675 	tnz	a
      00A98C 27 0F            [ 1] 1676 	jreq	00114$
                           0005D3  1677 	Sstm8s_uart1$UART1_GetFlagStatus$518 ==.
                           0005D3  1678 	Sstm8s_uart1$UART1_GetFlagStatus$519 ==.
                                   1679 ;	drivers/src/stm8s_uart1.c: 579: if ((UART1->CR4 & (uint8_t)UART1_FLAG) != (uint8_t)0x00)
      00A98E C6 52 37         [ 1] 1680 	ld	a, 0x5237
      00A991 14 02            [ 1] 1681 	and	a, (0x02, sp)
      00A993 27 05            [ 1] 1682 	jreq	00102$
                           0005DA  1683 	Sstm8s_uart1$UART1_GetFlagStatus$520 ==.
                           0005DA  1684 	Sstm8s_uart1$UART1_GetFlagStatus$521 ==.
                                   1685 ;	drivers/src/stm8s_uart1.c: 582: status = SET;
      00A995 A6 01            [ 1] 1686 	ld	a, #0x01
                           0005DC  1687 	Sstm8s_uart1$UART1_GetFlagStatus$522 ==.
      00A997 CC A9 BB         [ 2] 1688 	jp	00115$
      00A99A                       1689 00102$:
                           0005DF  1690 	Sstm8s_uart1$UART1_GetFlagStatus$523 ==.
                           0005DF  1691 	Sstm8s_uart1$UART1_GetFlagStatus$524 ==.
                                   1692 ;	drivers/src/stm8s_uart1.c: 587: status = RESET;
      00A99A 4F               [ 1] 1693 	clr	a
                           0005E0  1694 	Sstm8s_uart1$UART1_GetFlagStatus$525 ==.
      00A99B 20 1E            [ 2] 1695 	jra	00115$
      00A99D                       1696 00114$:
                           0005E2  1697 	Sstm8s_uart1$UART1_GetFlagStatus$526 ==.
                                   1698 ;	drivers/src/stm8s_uart1.c: 590: else if (UART1_FLAG == UART1_FLAG_SBK)
      00A99D 7B 01            [ 1] 1699 	ld	a, (0x01, sp)
      00A99F 27 0E            [ 1] 1700 	jreq	00111$
                           0005E6  1701 	Sstm8s_uart1$UART1_GetFlagStatus$527 ==.
                           0005E6  1702 	Sstm8s_uart1$UART1_GetFlagStatus$528 ==.
                                   1703 ;	drivers/src/stm8s_uart1.c: 592: if ((UART1->CR2 & (uint8_t)UART1_FLAG) != (uint8_t)0x00)
      00A9A1 C6 52 35         [ 1] 1704 	ld	a, 0x5235
      00A9A4 14 02            [ 1] 1705 	and	a, (0x02, sp)
      00A9A6 27 04            [ 1] 1706 	jreq	00105$
                           0005ED  1707 	Sstm8s_uart1$UART1_GetFlagStatus$529 ==.
                           0005ED  1708 	Sstm8s_uart1$UART1_GetFlagStatus$530 ==.
                                   1709 ;	drivers/src/stm8s_uart1.c: 595: status = SET;
      00A9A8 A6 01            [ 1] 1710 	ld	a, #0x01
                           0005EF  1711 	Sstm8s_uart1$UART1_GetFlagStatus$531 ==.
      00A9AA 20 0F            [ 2] 1712 	jra	00115$
      00A9AC                       1713 00105$:
                           0005F1  1714 	Sstm8s_uart1$UART1_GetFlagStatus$532 ==.
                           0005F1  1715 	Sstm8s_uart1$UART1_GetFlagStatus$533 ==.
                                   1716 ;	drivers/src/stm8s_uart1.c: 600: status = RESET;
      00A9AC 4F               [ 1] 1717 	clr	a
                           0005F2  1718 	Sstm8s_uart1$UART1_GetFlagStatus$534 ==.
      00A9AD 20 0C            [ 2] 1719 	jra	00115$
      00A9AF                       1720 00111$:
                           0005F4  1721 	Sstm8s_uart1$UART1_GetFlagStatus$535 ==.
                           0005F4  1722 	Sstm8s_uart1$UART1_GetFlagStatus$536 ==.
                                   1723 ;	drivers/src/stm8s_uart1.c: 605: if ((UART1->SR & (uint8_t)UART1_FLAG) != (uint8_t)0x00)
      00A9AF C6 52 30         [ 1] 1724 	ld	a, 0x5230
      00A9B2 14 02            [ 1] 1725 	and	a, (0x02, sp)
      00A9B4 27 04            [ 1] 1726 	jreq	00108$
                           0005FB  1727 	Sstm8s_uart1$UART1_GetFlagStatus$537 ==.
                           0005FB  1728 	Sstm8s_uart1$UART1_GetFlagStatus$538 ==.
                                   1729 ;	drivers/src/stm8s_uart1.c: 608: status = SET;
      00A9B6 A6 01            [ 1] 1730 	ld	a, #0x01
                           0005FD  1731 	Sstm8s_uart1$UART1_GetFlagStatus$539 ==.
      00A9B8 20 01            [ 2] 1732 	jra	00115$
      00A9BA                       1733 00108$:
                           0005FF  1734 	Sstm8s_uart1$UART1_GetFlagStatus$540 ==.
                           0005FF  1735 	Sstm8s_uart1$UART1_GetFlagStatus$541 ==.
                                   1736 ;	drivers/src/stm8s_uart1.c: 613: status = RESET;
      00A9BA 4F               [ 1] 1737 	clr	a
                           000600  1738 	Sstm8s_uart1$UART1_GetFlagStatus$542 ==.
      00A9BB                       1739 00115$:
                           000600  1740 	Sstm8s_uart1$UART1_GetFlagStatus$543 ==.
                                   1741 ;	drivers/src/stm8s_uart1.c: 617: return status;
                           000600  1742 	Sstm8s_uart1$UART1_GetFlagStatus$544 ==.
                                   1743 ;	drivers/src/stm8s_uart1.c: 618: }
      00A9BB 85               [ 2] 1744 	popw	x
                           000601  1745 	Sstm8s_uart1$UART1_GetFlagStatus$545 ==.
                           000601  1746 	Sstm8s_uart1$UART1_GetFlagStatus$546 ==.
                           000601  1747 	XG$UART1_GetFlagStatus$0$0 ==.
      00A9BC 81               [ 4] 1748 	ret
                           000602  1749 	Sstm8s_uart1$UART1_GetFlagStatus$547 ==.
                           000602  1750 	Sstm8s_uart1$UART1_ClearFlag$548 ==.
                                   1751 ;	drivers/src/stm8s_uart1.c: 646: void UART1_ClearFlag(UART1_Flag_TypeDef UART1_FLAG)
                                   1752 ;	-----------------------------------------
                                   1753 ;	 function UART1_ClearFlag
                                   1754 ;	-----------------------------------------
      00A9BD                       1755 _UART1_ClearFlag:
                           000602  1756 	Sstm8s_uart1$UART1_ClearFlag$549 ==.
                           000602  1757 	Sstm8s_uart1$UART1_ClearFlag$550 ==.
                                   1758 ;	drivers/src/stm8s_uart1.c: 648: assert_param(IS_UART1_CLEAR_FLAG_OK(UART1_FLAG));
      00A9BD 1E 03            [ 2] 1759 	ldw	x, (0x03, sp)
      00A9BF A3 00 20         [ 2] 1760 	cpw	x, #0x0020
      00A9C2 26 03            [ 1] 1761 	jrne	00127$
      00A9C4 A6 01            [ 1] 1762 	ld	a, #0x01
      00A9C6 21                    1763 	.byte 0x21
      00A9C7                       1764 00127$:
      00A9C7 4F               [ 1] 1765 	clr	a
      00A9C8                       1766 00128$:
                           00060D  1767 	Sstm8s_uart1$UART1_ClearFlag$551 ==.
      00A9C8 4D               [ 1] 1768 	tnz	a
      00A9C9 26 16            [ 1] 1769 	jrne	00107$
      00A9CB A3 02 10         [ 2] 1770 	cpw	x, #0x0210
      00A9CE 27 11            [ 1] 1771 	jreq	00107$
                           000615  1772 	Sstm8s_uart1$UART1_ClearFlag$552 ==.
      00A9D0 88               [ 1] 1773 	push	a
                           000616  1774 	Sstm8s_uart1$UART1_ClearFlag$553 ==.
      00A9D1 4B 88            [ 1] 1775 	push	#0x88
                           000618  1776 	Sstm8s_uart1$UART1_ClearFlag$554 ==.
      00A9D3 4B 02            [ 1] 1777 	push	#0x02
                           00061A  1778 	Sstm8s_uart1$UART1_ClearFlag$555 ==.
      00A9D5 5F               [ 1] 1779 	clrw	x
      00A9D6 89               [ 2] 1780 	pushw	x
                           00061C  1781 	Sstm8s_uart1$UART1_ClearFlag$556 ==.
      00A9D7 4B 5E            [ 1] 1782 	push	#<(___str_0+0)
                           00061E  1783 	Sstm8s_uart1$UART1_ClearFlag$557 ==.
      00A9D9 4B 81            [ 1] 1784 	push	#((___str_0+0) >> 8)
                           000620  1785 	Sstm8s_uart1$UART1_ClearFlag$558 ==.
      00A9DB CD 84 F3         [ 4] 1786 	call	_assert_failed
      00A9DE 5B 06            [ 2] 1787 	addw	sp, #6
                           000625  1788 	Sstm8s_uart1$UART1_ClearFlag$559 ==.
      00A9E0 84               [ 1] 1789 	pop	a
                           000626  1790 	Sstm8s_uart1$UART1_ClearFlag$560 ==.
      00A9E1                       1791 00107$:
                           000626  1792 	Sstm8s_uart1$UART1_ClearFlag$561 ==.
                                   1793 ;	drivers/src/stm8s_uart1.c: 651: if (UART1_FLAG == UART1_FLAG_RXNE)
      00A9E1 4D               [ 1] 1794 	tnz	a
      00A9E2 27 06            [ 1] 1795 	jreq	00102$
                           000629  1796 	Sstm8s_uart1$UART1_ClearFlag$562 ==.
                           000629  1797 	Sstm8s_uart1$UART1_ClearFlag$563 ==.
                                   1798 ;	drivers/src/stm8s_uart1.c: 653: UART1->SR = (uint8_t)~(UART1_SR_RXNE);
      00A9E4 35 DF 52 30      [ 1] 1799 	mov	0x5230+0, #0xdf
                           00062D  1800 	Sstm8s_uart1$UART1_ClearFlag$564 ==.
      00A9E8 20 04            [ 2] 1801 	jra	00104$
      00A9EA                       1802 00102$:
                           00062F  1803 	Sstm8s_uart1$UART1_ClearFlag$565 ==.
                           00062F  1804 	Sstm8s_uart1$UART1_ClearFlag$566 ==.
                                   1805 ;	drivers/src/stm8s_uart1.c: 658: UART1->CR4 &= (uint8_t)~(UART1_CR4_LBDF);
      00A9EA 72 19 52 37      [ 1] 1806 	bres	21047, #4
                           000633  1807 	Sstm8s_uart1$UART1_ClearFlag$567 ==.
      00A9EE                       1808 00104$:
                           000633  1809 	Sstm8s_uart1$UART1_ClearFlag$568 ==.
                                   1810 ;	drivers/src/stm8s_uart1.c: 660: }
                           000633  1811 	Sstm8s_uart1$UART1_ClearFlag$569 ==.
                           000633  1812 	XG$UART1_ClearFlag$0$0 ==.
      00A9EE 81               [ 4] 1813 	ret
                           000634  1814 	Sstm8s_uart1$UART1_ClearFlag$570 ==.
                           000634  1815 	Sstm8s_uart1$UART1_GetITStatus$571 ==.
                                   1816 ;	drivers/src/stm8s_uart1.c: 675: ITStatus UART1_GetITStatus(UART1_IT_TypeDef UART1_IT)
                                   1817 ;	-----------------------------------------
                                   1818 ;	 function UART1_GetITStatus
                                   1819 ;	-----------------------------------------
      00A9EF                       1820 _UART1_GetITStatus:
                           000634  1821 	Sstm8s_uart1$UART1_GetITStatus$572 ==.
      00A9EF 52 04            [ 2] 1822 	sub	sp, #4
                           000636  1823 	Sstm8s_uart1$UART1_GetITStatus$573 ==.
                           000636  1824 	Sstm8s_uart1$UART1_GetITStatus$574 ==.
                                   1825 ;	drivers/src/stm8s_uart1.c: 684: assert_param(IS_UART1_GET_IT_OK(UART1_IT));
      00A9F1 1E 07            [ 2] 1826 	ldw	x, (0x07, sp)
      00A9F3 A3 03 46         [ 2] 1827 	cpw	x, #0x0346
      00A9F6 26 05            [ 1] 1828 	jrne	00217$
      00A9F8 A6 01            [ 1] 1829 	ld	a, #0x01
      00A9FA 6B 01            [ 1] 1830 	ld	(0x01, sp), a
      00A9FC C5                    1831 	.byte 0xc5
      00A9FD                       1832 00217$:
      00A9FD 0F 01            [ 1] 1833 	clr	(0x01, sp)
      00A9FF                       1834 00218$:
                           000644  1835 	Sstm8s_uart1$UART1_GetITStatus$575 ==.
      00A9FF A3 01 00         [ 2] 1836 	cpw	x, #0x0100
      00AA02 26 05            [ 1] 1837 	jrne	00220$
      00AA04 A6 01            [ 1] 1838 	ld	a, #0x01
      00AA06 6B 02            [ 1] 1839 	ld	(0x02, sp), a
      00AA08 C5                    1840 	.byte 0xc5
      00AA09                       1841 00220$:
      00AA09 0F 02            [ 1] 1842 	clr	(0x02, sp)
      00AA0B                       1843 00221$:
                           000650  1844 	Sstm8s_uart1$UART1_GetITStatus$576 ==.
      00AA0B A3 02 77         [ 2] 1845 	cpw	x, #0x0277
      00AA0E 27 2B            [ 1] 1846 	jreq	00122$
                           000655  1847 	Sstm8s_uart1$UART1_GetITStatus$577 ==.
      00AA10 A3 02 66         [ 2] 1848 	cpw	x, #0x0266
      00AA13 27 26            [ 1] 1849 	jreq	00122$
                           00065A  1850 	Sstm8s_uart1$UART1_GetITStatus$578 ==.
      00AA15 A3 02 55         [ 2] 1851 	cpw	x, #0x0255
      00AA18 27 21            [ 1] 1852 	jreq	00122$
                           00065F  1853 	Sstm8s_uart1$UART1_GetITStatus$579 ==.
      00AA1A A3 02 44         [ 2] 1854 	cpw	x, #0x0244
      00AA1D 27 1C            [ 1] 1855 	jreq	00122$
                           000664  1856 	Sstm8s_uart1$UART1_GetITStatus$580 ==.
      00AA1F A3 02 35         [ 2] 1857 	cpw	x, #0x0235
      00AA22 27 17            [ 1] 1858 	jreq	00122$
                           000669  1859 	Sstm8s_uart1$UART1_GetITStatus$581 ==.
      00AA24 0D 01            [ 1] 1860 	tnz	(0x01, sp)
      00AA26 26 13            [ 1] 1861 	jrne	00122$
      00AA28 0D 02            [ 1] 1862 	tnz	(0x02, sp)
      00AA2A 26 0F            [ 1] 1863 	jrne	00122$
      00AA2C 4B AC            [ 1] 1864 	push	#0xac
                           000673  1865 	Sstm8s_uart1$UART1_GetITStatus$582 ==.
      00AA2E 4B 02            [ 1] 1866 	push	#0x02
                           000675  1867 	Sstm8s_uart1$UART1_GetITStatus$583 ==.
      00AA30 5F               [ 1] 1868 	clrw	x
      00AA31 89               [ 2] 1869 	pushw	x
                           000677  1870 	Sstm8s_uart1$UART1_GetITStatus$584 ==.
      00AA32 4B 5E            [ 1] 1871 	push	#<(___str_0+0)
                           000679  1872 	Sstm8s_uart1$UART1_GetITStatus$585 ==.
      00AA34 4B 81            [ 1] 1873 	push	#((___str_0+0) >> 8)
                           00067B  1874 	Sstm8s_uart1$UART1_GetITStatus$586 ==.
      00AA36 CD 84 F3         [ 4] 1875 	call	_assert_failed
      00AA39 5B 06            [ 2] 1876 	addw	sp, #6
                           000680  1877 	Sstm8s_uart1$UART1_GetITStatus$587 ==.
      00AA3B                       1878 00122$:
                           000680  1879 	Sstm8s_uart1$UART1_GetITStatus$588 ==.
                                   1880 ;	drivers/src/stm8s_uart1.c: 687: itpos = (uint8_t)((uint8_t)1 << (uint8_t)((uint8_t)UART1_IT & (uint8_t)0x0F));
      00AA3B 7B 08            [ 1] 1881 	ld	a, (0x08, sp)
      00AA3D 97               [ 1] 1882 	ld	xl, a
      00AA3E A4 0F            [ 1] 1883 	and	a, #0x0f
      00AA40 88               [ 1] 1884 	push	a
                           000686  1885 	Sstm8s_uart1$UART1_GetITStatus$589 ==.
      00AA41 A6 01            [ 1] 1886 	ld	a, #0x01
      00AA43 6B 04            [ 1] 1887 	ld	(0x04, sp), a
      00AA45 84               [ 1] 1888 	pop	a
                           00068B  1889 	Sstm8s_uart1$UART1_GetITStatus$590 ==.
      00AA46 4D               [ 1] 1890 	tnz	a
      00AA47 27 05            [ 1] 1891 	jreq	00240$
      00AA49                       1892 00239$:
      00AA49 08 03            [ 1] 1893 	sll	(0x03, sp)
      00AA4B 4A               [ 1] 1894 	dec	a
      00AA4C 26 FB            [ 1] 1895 	jrne	00239$
      00AA4E                       1896 00240$:
                           000693  1897 	Sstm8s_uart1$UART1_GetITStatus$591 ==.
                                   1898 ;	drivers/src/stm8s_uart1.c: 689: itmask1 = (uint8_t)((uint8_t)UART1_IT >> (uint8_t)4);
      00AA4E 9F               [ 1] 1899 	ld	a, xl
      00AA4F 4E               [ 1] 1900 	swap	a
      00AA50 A4 0F            [ 1] 1901 	and	a, #0x0f
                           000697  1902 	Sstm8s_uart1$UART1_GetITStatus$592 ==.
                                   1903 ;	drivers/src/stm8s_uart1.c: 691: itmask2 = (uint8_t)((uint8_t)1 << itmask1);
      00AA52 88               [ 1] 1904 	push	a
                           000698  1905 	Sstm8s_uart1$UART1_GetITStatus$593 ==.
      00AA53 A6 01            [ 1] 1906 	ld	a, #0x01
      00AA55 6B 05            [ 1] 1907 	ld	(0x05, sp), a
      00AA57 84               [ 1] 1908 	pop	a
                           00069D  1909 	Sstm8s_uart1$UART1_GetITStatus$594 ==.
      00AA58 4D               [ 1] 1910 	tnz	a
      00AA59 27 05            [ 1] 1911 	jreq	00242$
      00AA5B                       1912 00241$:
      00AA5B 08 04            [ 1] 1913 	sll	(0x04, sp)
      00AA5D 4A               [ 1] 1914 	dec	a
      00AA5E 26 FB            [ 1] 1915 	jrne	00241$
      00AA60                       1916 00242$:
                           0006A5  1917 	Sstm8s_uart1$UART1_GetITStatus$595 ==.
                                   1918 ;	drivers/src/stm8s_uart1.c: 695: if (UART1_IT == UART1_IT_PE)
      00AA60 7B 02            [ 1] 1919 	ld	a, (0x02, sp)
      00AA62 27 1A            [ 1] 1920 	jreq	00117$
                           0006A9  1921 	Sstm8s_uart1$UART1_GetITStatus$596 ==.
                           0006A9  1922 	Sstm8s_uart1$UART1_GetITStatus$597 ==.
                                   1923 ;	drivers/src/stm8s_uart1.c: 698: enablestatus = (uint8_t)((uint8_t)UART1->CR1 & itmask2);
      00AA64 C6 52 34         [ 1] 1924 	ld	a, 0x5234
      00AA67 14 04            [ 1] 1925 	and	a, (0x04, sp)
      00AA69 97               [ 1] 1926 	ld	xl, a
                           0006AF  1927 	Sstm8s_uart1$UART1_GetITStatus$598 ==.
                                   1928 ;	drivers/src/stm8s_uart1.c: 701: if (((UART1->SR & itpos) != (uint8_t)0x00) && enablestatus)
      00AA6A C6 52 30         [ 1] 1929 	ld	a, 0x5230
      00AA6D 14 03            [ 1] 1930 	and	a, (0x03, sp)
      00AA6F 27 09            [ 1] 1931 	jreq	00102$
      00AA71 9F               [ 1] 1932 	ld	a, xl
      00AA72 4D               [ 1] 1933 	tnz	a
      00AA73 27 05            [ 1] 1934 	jreq	00102$
                           0006BA  1935 	Sstm8s_uart1$UART1_GetITStatus$599 ==.
                           0006BA  1936 	Sstm8s_uart1$UART1_GetITStatus$600 ==.
                                   1937 ;	drivers/src/stm8s_uart1.c: 704: pendingbitstatus = SET;
      00AA75 A6 01            [ 1] 1938 	ld	a, #0x01
                           0006BC  1939 	Sstm8s_uart1$UART1_GetITStatus$601 ==.
      00AA77 CC AA B0         [ 2] 1940 	jp	00118$
      00AA7A                       1941 00102$:
                           0006BF  1942 	Sstm8s_uart1$UART1_GetITStatus$602 ==.
                           0006BF  1943 	Sstm8s_uart1$UART1_GetITStatus$603 ==.
                                   1944 ;	drivers/src/stm8s_uart1.c: 709: pendingbitstatus = RESET;
      00AA7A 4F               [ 1] 1945 	clr	a
                           0006C0  1946 	Sstm8s_uart1$UART1_GetITStatus$604 ==.
      00AA7B CC AA B0         [ 2] 1947 	jp	00118$
      00AA7E                       1948 00117$:
                           0006C3  1949 	Sstm8s_uart1$UART1_GetITStatus$605 ==.
                                   1950 ;	drivers/src/stm8s_uart1.c: 713: else if (UART1_IT == UART1_IT_LBDF)
      00AA7E 7B 01            [ 1] 1951 	ld	a, (0x01, sp)
      00AA80 27 18            [ 1] 1952 	jreq	00114$
                           0006C7  1953 	Sstm8s_uart1$UART1_GetITStatus$606 ==.
                           0006C7  1954 	Sstm8s_uart1$UART1_GetITStatus$607 ==.
                                   1955 ;	drivers/src/stm8s_uart1.c: 716: enablestatus = (uint8_t)((uint8_t)UART1->CR4 & itmask2);
      00AA82 C6 52 37         [ 1] 1956 	ld	a, 0x5237
      00AA85 14 04            [ 1] 1957 	and	a, (0x04, sp)
      00AA87 97               [ 1] 1958 	ld	xl, a
                           0006CD  1959 	Sstm8s_uart1$UART1_GetITStatus$608 ==.
                                   1960 ;	drivers/src/stm8s_uart1.c: 718: if (((UART1->CR4 & itpos) != (uint8_t)0x00) && enablestatus)
      00AA88 C6 52 37         [ 1] 1961 	ld	a, 0x5237
      00AA8B 14 03            [ 1] 1962 	and	a, (0x03, sp)
      00AA8D 27 08            [ 1] 1963 	jreq	00106$
      00AA8F 9F               [ 1] 1964 	ld	a, xl
      00AA90 4D               [ 1] 1965 	tnz	a
      00AA91 27 04            [ 1] 1966 	jreq	00106$
                           0006D8  1967 	Sstm8s_uart1$UART1_GetITStatus$609 ==.
                           0006D8  1968 	Sstm8s_uart1$UART1_GetITStatus$610 ==.
                                   1969 ;	drivers/src/stm8s_uart1.c: 721: pendingbitstatus = SET;
      00AA93 A6 01            [ 1] 1970 	ld	a, #0x01
                           0006DA  1971 	Sstm8s_uart1$UART1_GetITStatus$611 ==.
      00AA95 20 19            [ 2] 1972 	jra	00118$
      00AA97                       1973 00106$:
                           0006DC  1974 	Sstm8s_uart1$UART1_GetITStatus$612 ==.
                           0006DC  1975 	Sstm8s_uart1$UART1_GetITStatus$613 ==.
                                   1976 ;	drivers/src/stm8s_uart1.c: 726: pendingbitstatus = RESET;
      00AA97 4F               [ 1] 1977 	clr	a
                           0006DD  1978 	Sstm8s_uart1$UART1_GetITStatus$614 ==.
      00AA98 20 16            [ 2] 1979 	jra	00118$
      00AA9A                       1980 00114$:
                           0006DF  1981 	Sstm8s_uart1$UART1_GetITStatus$615 ==.
                           0006DF  1982 	Sstm8s_uart1$UART1_GetITStatus$616 ==.
                                   1983 ;	drivers/src/stm8s_uart1.c: 732: enablestatus = (uint8_t)((uint8_t)UART1->CR2 & itmask2);
      00AA9A C6 52 35         [ 1] 1984 	ld	a, 0x5235
      00AA9D 14 04            [ 1] 1985 	and	a, (0x04, sp)
      00AA9F 97               [ 1] 1986 	ld	xl, a
                           0006E5  1987 	Sstm8s_uart1$UART1_GetITStatus$617 ==.
                                   1988 ;	drivers/src/stm8s_uart1.c: 734: if (((UART1->SR & itpos) != (uint8_t)0x00) && enablestatus)
      00AAA0 C6 52 30         [ 1] 1989 	ld	a, 0x5230
      00AAA3 14 03            [ 1] 1990 	and	a, (0x03, sp)
      00AAA5 27 08            [ 1] 1991 	jreq	00110$
      00AAA7 9F               [ 1] 1992 	ld	a, xl
      00AAA8 4D               [ 1] 1993 	tnz	a
      00AAA9 27 04            [ 1] 1994 	jreq	00110$
                           0006F0  1995 	Sstm8s_uart1$UART1_GetITStatus$618 ==.
                           0006F0  1996 	Sstm8s_uart1$UART1_GetITStatus$619 ==.
                                   1997 ;	drivers/src/stm8s_uart1.c: 737: pendingbitstatus = SET;
      00AAAB A6 01            [ 1] 1998 	ld	a, #0x01
                           0006F2  1999 	Sstm8s_uart1$UART1_GetITStatus$620 ==.
      00AAAD 20 01            [ 2] 2000 	jra	00118$
      00AAAF                       2001 00110$:
                           0006F4  2002 	Sstm8s_uart1$UART1_GetITStatus$621 ==.
                           0006F4  2003 	Sstm8s_uart1$UART1_GetITStatus$622 ==.
                                   2004 ;	drivers/src/stm8s_uart1.c: 742: pendingbitstatus = RESET;
      00AAAF 4F               [ 1] 2005 	clr	a
                           0006F5  2006 	Sstm8s_uart1$UART1_GetITStatus$623 ==.
      00AAB0                       2007 00118$:
                           0006F5  2008 	Sstm8s_uart1$UART1_GetITStatus$624 ==.
                                   2009 ;	drivers/src/stm8s_uart1.c: 747: return  pendingbitstatus;
                           0006F5  2010 	Sstm8s_uart1$UART1_GetITStatus$625 ==.
                                   2011 ;	drivers/src/stm8s_uart1.c: 748: }
      00AAB0 5B 04            [ 2] 2012 	addw	sp, #4
                           0006F7  2013 	Sstm8s_uart1$UART1_GetITStatus$626 ==.
                           0006F7  2014 	Sstm8s_uart1$UART1_GetITStatus$627 ==.
                           0006F7  2015 	XG$UART1_GetITStatus$0$0 ==.
      00AAB2 81               [ 4] 2016 	ret
                           0006F8  2017 	Sstm8s_uart1$UART1_GetITStatus$628 ==.
                           0006F8  2018 	Sstm8s_uart1$UART1_ClearITPendingBit$629 ==.
                                   2019 ;	drivers/src/stm8s_uart1.c: 775: void UART1_ClearITPendingBit(UART1_IT_TypeDef UART1_IT)
                                   2020 ;	-----------------------------------------
                                   2021 ;	 function UART1_ClearITPendingBit
                                   2022 ;	-----------------------------------------
      00AAB3                       2023 _UART1_ClearITPendingBit:
                           0006F8  2024 	Sstm8s_uart1$UART1_ClearITPendingBit$630 ==.
                           0006F8  2025 	Sstm8s_uart1$UART1_ClearITPendingBit$631 ==.
                                   2026 ;	drivers/src/stm8s_uart1.c: 777: assert_param(IS_UART1_CLEAR_IT_OK(UART1_IT));
      00AAB3 1E 03            [ 2] 2027 	ldw	x, (0x03, sp)
      00AAB5 A3 02 55         [ 2] 2028 	cpw	x, #0x0255
      00AAB8 26 03            [ 1] 2029 	jrne	00127$
      00AABA A6 01            [ 1] 2030 	ld	a, #0x01
      00AABC 21                    2031 	.byte 0x21
      00AABD                       2032 00127$:
      00AABD 4F               [ 1] 2033 	clr	a
      00AABE                       2034 00128$:
                           000703  2035 	Sstm8s_uart1$UART1_ClearITPendingBit$632 ==.
      00AABE 4D               [ 1] 2036 	tnz	a
      00AABF 26 16            [ 1] 2037 	jrne	00107$
      00AAC1 A3 03 46         [ 2] 2038 	cpw	x, #0x0346
      00AAC4 27 11            [ 1] 2039 	jreq	00107$
                           00070B  2040 	Sstm8s_uart1$UART1_ClearITPendingBit$633 ==.
      00AAC6 88               [ 1] 2041 	push	a
                           00070C  2042 	Sstm8s_uart1$UART1_ClearITPendingBit$634 ==.
      00AAC7 4B 09            [ 1] 2043 	push	#0x09
                           00070E  2044 	Sstm8s_uart1$UART1_ClearITPendingBit$635 ==.
      00AAC9 4B 03            [ 1] 2045 	push	#0x03
                           000710  2046 	Sstm8s_uart1$UART1_ClearITPendingBit$636 ==.
      00AACB 5F               [ 1] 2047 	clrw	x
      00AACC 89               [ 2] 2048 	pushw	x
                           000712  2049 	Sstm8s_uart1$UART1_ClearITPendingBit$637 ==.
      00AACD 4B 5E            [ 1] 2050 	push	#<(___str_0+0)
                           000714  2051 	Sstm8s_uart1$UART1_ClearITPendingBit$638 ==.
      00AACF 4B 81            [ 1] 2052 	push	#((___str_0+0) >> 8)
                           000716  2053 	Sstm8s_uart1$UART1_ClearITPendingBit$639 ==.
      00AAD1 CD 84 F3         [ 4] 2054 	call	_assert_failed
      00AAD4 5B 06            [ 2] 2055 	addw	sp, #6
                           00071B  2056 	Sstm8s_uart1$UART1_ClearITPendingBit$640 ==.
      00AAD6 84               [ 1] 2057 	pop	a
                           00071C  2058 	Sstm8s_uart1$UART1_ClearITPendingBit$641 ==.
      00AAD7                       2059 00107$:
                           00071C  2060 	Sstm8s_uart1$UART1_ClearITPendingBit$642 ==.
                                   2061 ;	drivers/src/stm8s_uart1.c: 780: if (UART1_IT == UART1_IT_RXNE)
      00AAD7 4D               [ 1] 2062 	tnz	a
      00AAD8 27 06            [ 1] 2063 	jreq	00102$
                           00071F  2064 	Sstm8s_uart1$UART1_ClearITPendingBit$643 ==.
                           00071F  2065 	Sstm8s_uart1$UART1_ClearITPendingBit$644 ==.
                                   2066 ;	drivers/src/stm8s_uart1.c: 782: UART1->SR = (uint8_t)~(UART1_SR_RXNE);
      00AADA 35 DF 52 30      [ 1] 2067 	mov	0x5230+0, #0xdf
                           000723  2068 	Sstm8s_uart1$UART1_ClearITPendingBit$645 ==.
      00AADE 20 04            [ 2] 2069 	jra	00104$
      00AAE0                       2070 00102$:
                           000725  2071 	Sstm8s_uart1$UART1_ClearITPendingBit$646 ==.
                           000725  2072 	Sstm8s_uart1$UART1_ClearITPendingBit$647 ==.
                                   2073 ;	drivers/src/stm8s_uart1.c: 787: UART1->CR4 &= (uint8_t)~(UART1_CR4_LBDF);
      00AAE0 72 19 52 37      [ 1] 2074 	bres	21047, #4
                           000729  2075 	Sstm8s_uart1$UART1_ClearITPendingBit$648 ==.
      00AAE4                       2076 00104$:
                           000729  2077 	Sstm8s_uart1$UART1_ClearITPendingBit$649 ==.
                                   2078 ;	drivers/src/stm8s_uart1.c: 789: }
                           000729  2079 	Sstm8s_uart1$UART1_ClearITPendingBit$650 ==.
                           000729  2080 	XG$UART1_ClearITPendingBit$0$0 ==.
      00AAE4 81               [ 4] 2081 	ret
                           00072A  2082 	Sstm8s_uart1$UART1_ClearITPendingBit$651 ==.
                                   2083 	.area CODE
                                   2084 	.area CONST
                           000000  2085 Fstm8s_uart1$__str_0$0_0$0 == .
                                   2086 	.area CONST
      00815E                       2087 ___str_0:
      00815E 64 72 69 76 65 72 73  2088 	.ascii "drivers/src/stm8s_uart1.c"
             2F 73 72 63 2F 73 74
             6D 38 73 5F 75 61 72
             74 31 2E 63
      008177 00                    2089 	.db 0x00
                                   2090 	.area CODE
                                   2091 	.area INITIALIZER
                                   2092 	.area CABS (ABS)
                                   2093 
                                   2094 	.area .debug_line (NOLOAD)
      0026D4 00 00 06 9C           2095 	.dw	0,Ldebug_line_end-Ldebug_line_start
      0026D8                       2096 Ldebug_line_start:
      0026D8 00 02                 2097 	.dw	2
      0026DA 00 00 00 7A           2098 	.dw	0,Ldebug_line_stmt-6-Ldebug_line_start
      0026DE 01                    2099 	.db	1
      0026DF 01                    2100 	.db	1
      0026E0 FB                    2101 	.db	-5
      0026E1 0F                    2102 	.db	15
      0026E2 0A                    2103 	.db	10
      0026E3 00                    2104 	.db	0
      0026E4 01                    2105 	.db	1
      0026E5 01                    2106 	.db	1
      0026E6 01                    2107 	.db	1
      0026E7 01                    2108 	.db	1
      0026E8 00                    2109 	.db	0
      0026E9 00                    2110 	.db	0
      0026EA 00                    2111 	.db	0
      0026EB 01                    2112 	.db	1
      0026EC 43 3A 5C 50 72 6F 67  2113 	.ascii "C:\Program Files\SDCC\bin\..\include\stm8"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
             5C 73 74 6D 38
      002714 00                    2114 	.db	0
      002715 43 3A 5C 50 72 6F 67  2115 	.ascii "C:\Program Files\SDCC\bin\..\include"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
      002738 00                    2116 	.db	0
      002739 00                    2117 	.db	0
      00273A 64 72 69 76 65 72 73  2118 	.ascii "drivers/src/stm8s_uart1.c"
             2F 73 72 63 2F 73 74
             6D 38 73 5F 75 61 72
             74 31 2E 63
      002753 00                    2119 	.db	0
      002754 00                    2120 	.uleb128	0
      002755 00                    2121 	.uleb128	0
      002756 00                    2122 	.uleb128	0
      002757 00                    2123 	.db	0
      002758                       2124 Ldebug_line_stmt:
      002758 00                    2125 	.db	0
      002759 05                    2126 	.uleb128	5
      00275A 02                    2127 	.db	2
      00275B 00 00 A3 BB           2128 	.dw	0,(Sstm8s_uart1$UART1_DeInit$0)
      00275F 03                    2129 	.db	3
      002760 34                    2130 	.sleb128	52
      002761 01                    2131 	.db	1
      002762 09                    2132 	.db	9
      002763 00 00                 2133 	.dw	Sstm8s_uart1$UART1_DeInit$2-Sstm8s_uart1$UART1_DeInit$0
      002765 03                    2134 	.db	3
      002766 04                    2135 	.sleb128	4
      002767 01                    2136 	.db	1
      002768 09                    2137 	.db	9
      002769 00 03                 2138 	.dw	Sstm8s_uart1$UART1_DeInit$3-Sstm8s_uart1$UART1_DeInit$2
      00276B 03                    2139 	.db	3
      00276C 01                    2140 	.sleb128	1
      00276D 01                    2141 	.db	1
      00276E 09                    2142 	.db	9
      00276F 00 03                 2143 	.dw	Sstm8s_uart1$UART1_DeInit$4-Sstm8s_uart1$UART1_DeInit$3
      002771 03                    2144 	.db	3
      002772 02                    2145 	.sleb128	2
      002773 01                    2146 	.db	1
      002774 09                    2147 	.db	9
      002775 00 04                 2148 	.dw	Sstm8s_uart1$UART1_DeInit$5-Sstm8s_uart1$UART1_DeInit$4
      002777 03                    2149 	.db	3
      002778 01                    2150 	.sleb128	1
      002779 01                    2151 	.db	1
      00277A 09                    2152 	.db	9
      00277B 00 04                 2153 	.dw	Sstm8s_uart1$UART1_DeInit$6-Sstm8s_uart1$UART1_DeInit$5
      00277D 03                    2154 	.db	3
      00277E 02                    2155 	.sleb128	2
      00277F 01                    2156 	.db	1
      002780 09                    2157 	.db	9
      002781 00 04                 2158 	.dw	Sstm8s_uart1$UART1_DeInit$7-Sstm8s_uart1$UART1_DeInit$6
      002783 03                    2159 	.db	3
      002784 01                    2160 	.sleb128	1
      002785 01                    2161 	.db	1
      002786 09                    2162 	.db	9
      002787 00 04                 2163 	.dw	Sstm8s_uart1$UART1_DeInit$8-Sstm8s_uart1$UART1_DeInit$7
      002789 03                    2164 	.db	3
      00278A 01                    2165 	.sleb128	1
      00278B 01                    2166 	.db	1
      00278C 09                    2167 	.db	9
      00278D 00 04                 2168 	.dw	Sstm8s_uart1$UART1_DeInit$9-Sstm8s_uart1$UART1_DeInit$8
      00278F 03                    2169 	.db	3
      002790 01                    2170 	.sleb128	1
      002791 01                    2171 	.db	1
      002792 09                    2172 	.db	9
      002793 00 04                 2173 	.dw	Sstm8s_uart1$UART1_DeInit$10-Sstm8s_uart1$UART1_DeInit$9
      002795 03                    2174 	.db	3
      002796 01                    2175 	.sleb128	1
      002797 01                    2176 	.db	1
      002798 09                    2177 	.db	9
      002799 00 04                 2178 	.dw	Sstm8s_uart1$UART1_DeInit$11-Sstm8s_uart1$UART1_DeInit$10
      00279B 03                    2179 	.db	3
      00279C 02                    2180 	.sleb128	2
      00279D 01                    2181 	.db	1
      00279E 09                    2182 	.db	9
      00279F 00 04                 2183 	.dw	Sstm8s_uart1$UART1_DeInit$12-Sstm8s_uart1$UART1_DeInit$11
      0027A1 03                    2184 	.db	3
      0027A2 01                    2185 	.sleb128	1
      0027A3 01                    2186 	.db	1
      0027A4 09                    2187 	.db	9
      0027A5 00 04                 2188 	.dw	Sstm8s_uart1$UART1_DeInit$13-Sstm8s_uart1$UART1_DeInit$12
      0027A7 03                    2189 	.db	3
      0027A8 01                    2190 	.sleb128	1
      0027A9 01                    2191 	.db	1
      0027AA 09                    2192 	.db	9
      0027AB 00 01                 2193 	.dw	1+Sstm8s_uart1$UART1_DeInit$14-Sstm8s_uart1$UART1_DeInit$13
      0027AD 00                    2194 	.db	0
      0027AE 01                    2195 	.uleb128	1
      0027AF 01                    2196 	.db	1
      0027B0 00                    2197 	.db	0
      0027B1 05                    2198 	.uleb128	5
      0027B2 02                    2199 	.db	2
      0027B3 00 00 A3 E6           2200 	.dw	0,(Sstm8s_uart1$UART1_Init$16)
      0027B7 03                    2201 	.db	3
      0027B8 D9 00                 2202 	.sleb128	89
      0027BA 01                    2203 	.db	1
      0027BB 09                    2204 	.db	9
      0027BC 00 02                 2205 	.dw	Sstm8s_uart1$UART1_Init$19-Sstm8s_uart1$UART1_Init$16
      0027BE 03                    2206 	.db	3
      0027BF 07                    2207 	.sleb128	7
      0027C0 01                    2208 	.db	1
      0027C1 09                    2209 	.db	9
      0027C2 00 1D                 2210 	.dw	Sstm8s_uart1$UART1_Init$26-Sstm8s_uart1$UART1_Init$19
      0027C4 03                    2211 	.db	3
      0027C5 01                    2212 	.sleb128	1
      0027C6 01                    2213 	.db	1
      0027C7 09                    2214 	.db	9
      0027C8 00 19                 2215 	.dw	Sstm8s_uart1$UART1_Init$34-Sstm8s_uart1$UART1_Init$26
      0027CA 03                    2216 	.db	3
      0027CB 01                    2217 	.sleb128	1
      0027CC 01                    2218 	.db	1
      0027CD 09                    2219 	.db	9
      0027CE 00 25                 2220 	.dw	Sstm8s_uart1$UART1_Init$44-Sstm8s_uart1$UART1_Init$34
      0027D0 03                    2221 	.db	3
      0027D1 01                    2222 	.sleb128	1
      0027D2 01                    2223 	.db	1
      0027D3 09                    2224 	.db	9
      0027D4 00 1F                 2225 	.dw	Sstm8s_uart1$UART1_Init$53-Sstm8s_uart1$UART1_Init$44
      0027D6 03                    2226 	.db	3
      0027D7 01                    2227 	.sleb128	1
      0027D8 01                    2228 	.db	1
      0027D9 09                    2229 	.db	9
      0027DA 00 4E                 2230 	.dw	Sstm8s_uart1$UART1_Init$68-Sstm8s_uart1$UART1_Init$53
      0027DC 03                    2231 	.db	3
      0027DD 01                    2232 	.sleb128	1
      0027DE 01                    2233 	.db	1
      0027DF 09                    2234 	.db	9
      0027E0 00 2F                 2235 	.dw	Sstm8s_uart1$UART1_Init$79-Sstm8s_uart1$UART1_Init$68
      0027E2 03                    2236 	.db	3
      0027E3 03                    2237 	.sleb128	3
      0027E4 01                    2238 	.db	1
      0027E5 09                    2239 	.db	9
      0027E6 00 04                 2240 	.dw	Sstm8s_uart1$UART1_Init$80-Sstm8s_uart1$UART1_Init$79
      0027E8 03                    2241 	.db	3
      0027E9 03                    2242 	.sleb128	3
      0027EA 01                    2243 	.db	1
      0027EB 09                    2244 	.db	9
      0027EC 00 08                 2245 	.dw	Sstm8s_uart1$UART1_Init$81-Sstm8s_uart1$UART1_Init$80
      0027EE 03                    2246 	.db	3
      0027EF 03                    2247 	.sleb128	3
      0027F0 01                    2248 	.db	1
      0027F1 09                    2249 	.db	9
      0027F2 00 08                 2250 	.dw	Sstm8s_uart1$UART1_Init$82-Sstm8s_uart1$UART1_Init$81
      0027F4 03                    2251 	.db	3
      0027F5 02                    2252 	.sleb128	2
      0027F6 01                    2253 	.db	1
      0027F7 09                    2254 	.db	9
      0027F8 00 08                 2255 	.dw	Sstm8s_uart1$UART1_Init$83-Sstm8s_uart1$UART1_Init$82
      0027FA 03                    2256 	.db	3
      0027FB 03                    2257 	.sleb128	3
      0027FC 01                    2258 	.db	1
      0027FD 09                    2259 	.db	9
      0027FE 00 08                 2260 	.dw	Sstm8s_uart1$UART1_Init$84-Sstm8s_uart1$UART1_Init$83
      002800 03                    2261 	.db	3
      002801 02                    2262 	.sleb128	2
      002802 01                    2263 	.db	1
      002803 09                    2264 	.db	9
      002804 00 08                 2265 	.dw	Sstm8s_uart1$UART1_Init$85-Sstm8s_uart1$UART1_Init$84
      002806 03                    2266 	.db	3
      002807 03                    2267 	.sleb128	3
      002808 01                    2268 	.db	1
      002809 09                    2269 	.db	9
      00280A 00 07                 2270 	.dw	Sstm8s_uart1$UART1_Init$86-Sstm8s_uart1$UART1_Init$85
      00280C 03                    2271 	.db	3
      00280D 02                    2272 	.sleb128	2
      00280E 01                    2273 	.db	1
      00280F 09                    2274 	.db	9
      002810 00 08                 2275 	.dw	Sstm8s_uart1$UART1_Init$87-Sstm8s_uart1$UART1_Init$86
      002812 03                    2276 	.db	3
      002813 02                    2277 	.sleb128	2
      002814 01                    2278 	.db	1
      002815 09                    2279 	.db	9
      002816 00 08                 2280 	.dw	Sstm8s_uart1$UART1_Init$88-Sstm8s_uart1$UART1_Init$87
      002818 03                    2281 	.db	3
      002819 03                    2282 	.sleb128	3
      00281A 01                    2283 	.db	1
      00281B 09                    2284 	.db	9
      00281C 00 29                 2285 	.dw	Sstm8s_uart1$UART1_Init$94-Sstm8s_uart1$UART1_Init$88
      00281E 03                    2286 	.db	3
      00281F 01                    2287 	.sleb128	1
      002820 01                    2288 	.db	1
      002821 09                    2289 	.db	9
      002822 00 2B                 2290 	.dw	Sstm8s_uart1$UART1_Init$106-Sstm8s_uart1$UART1_Init$94
      002824 03                    2291 	.db	3
      002825 02                    2292 	.sleb128	2
      002826 01                    2293 	.db	1
      002827 09                    2294 	.db	9
      002828 00 4C                 2295 	.dw	Sstm8s_uart1$UART1_Init$124-Sstm8s_uart1$UART1_Init$106
      00282A 03                    2296 	.db	3
      00282B 02                    2297 	.sleb128	2
      00282C 01                    2298 	.db	1
      00282D 09                    2299 	.db	9
      00282E 00 12                 2300 	.dw	Sstm8s_uart1$UART1_Init$125-Sstm8s_uart1$UART1_Init$124
      002830 03                    2301 	.db	3
      002831 02                    2302 	.sleb128	2
      002832 01                    2303 	.db	1
      002833 09                    2304 	.db	9
      002834 00 0C                 2305 	.dw	Sstm8s_uart1$UART1_Init$126-Sstm8s_uart1$UART1_Init$125
      002836 03                    2306 	.db	3
      002837 03                    2307 	.sleb128	3
      002838 01                    2308 	.db	1
      002839 09                    2309 	.db	9
      00283A 00 08                 2310 	.dw	Sstm8s_uart1$UART1_Init$127-Sstm8s_uart1$UART1_Init$126
      00283C 03                    2311 	.db	3
      00283D 02                    2312 	.sleb128	2
      00283E 01                    2313 	.db	1
      00283F 09                    2314 	.db	9
      002840 00 08                 2315 	.dw	Sstm8s_uart1$UART1_Init$128-Sstm8s_uart1$UART1_Init$127
      002842 03                    2316 	.db	3
      002843 02                    2317 	.sleb128	2
      002844 01                    2318 	.db	1
      002845 09                    2319 	.db	9
      002846 00 0E                 2320 	.dw	Sstm8s_uart1$UART1_Init$129-Sstm8s_uart1$UART1_Init$128
      002848 03                    2321 	.db	3
      002849 7C                    2322 	.sleb128	-4
      00284A 01                    2323 	.db	1
      00284B 09                    2324 	.db	9
      00284C 00 03                 2325 	.dw	Sstm8s_uart1$UART1_Init$130-Sstm8s_uart1$UART1_Init$129
      00284E 03                    2326 	.db	3
      00284F 07                    2327 	.sleb128	7
      002850 01                    2328 	.db	1
      002851 09                    2329 	.db	9
      002852 00 08                 2330 	.dw	Sstm8s_uart1$UART1_Init$134-Sstm8s_uart1$UART1_Init$130
      002854 03                    2331 	.db	3
      002855 03                    2332 	.sleb128	3
      002856 01                    2333 	.db	1
      002857 09                    2334 	.db	9
      002858 00 07                 2335 	.dw	Sstm8s_uart1$UART1_Init$137-Sstm8s_uart1$UART1_Init$134
      00285A 03                    2336 	.db	3
      00285B 05                    2337 	.sleb128	5
      00285C 01                    2338 	.db	1
      00285D 09                    2339 	.db	9
      00285E 00 05                 2340 	.dw	Sstm8s_uart1$UART1_Init$139-Sstm8s_uart1$UART1_Init$137
      002860 03                    2341 	.db	3
      002861 71                    2342 	.sleb128	-15
      002862 01                    2343 	.db	1
      002863 09                    2344 	.db	9
      002864 00 03                 2345 	.dw	Sstm8s_uart1$UART1_Init$140-Sstm8s_uart1$UART1_Init$139
      002866 03                    2346 	.db	3
      002867 11                    2347 	.sleb128	17
      002868 01                    2348 	.db	1
      002869 09                    2349 	.db	9
      00286A 00 08                 2350 	.dw	Sstm8s_uart1$UART1_Init$144-Sstm8s_uart1$UART1_Init$140
      00286C 03                    2351 	.db	3
      00286D 03                    2352 	.sleb128	3
      00286E 01                    2353 	.db	1
      00286F 09                    2354 	.db	9
      002870 00 07                 2355 	.dw	Sstm8s_uart1$UART1_Init$147-Sstm8s_uart1$UART1_Init$144
      002872 03                    2356 	.db	3
      002873 05                    2357 	.sleb128	5
      002874 01                    2358 	.db	1
      002875 09                    2359 	.db	9
      002876 00 05                 2360 	.dw	Sstm8s_uart1$UART1_Init$149-Sstm8s_uart1$UART1_Init$147
      002878 03                    2361 	.db	3
      002879 4C                    2362 	.sleb128	-52
      00287A 01                    2363 	.db	1
      00287B 09                    2364 	.db	9
      00287C 00 03                 2365 	.dw	Sstm8s_uart1$UART1_Init$150-Sstm8s_uart1$UART1_Init$149
      00287E 03                    2366 	.db	3
      00287F 38                    2367 	.sleb128	56
      002880 01                    2368 	.db	1
      002881 09                    2369 	.db	9
      002882 00 04                 2370 	.dw	Sstm8s_uart1$UART1_Init$152-Sstm8s_uart1$UART1_Init$150
      002884 03                    2371 	.db	3
      002885 03                    2372 	.sleb128	3
      002886 01                    2373 	.db	1
      002887 09                    2374 	.db	9
      002888 00 07                 2375 	.dw	Sstm8s_uart1$UART1_Init$155-Sstm8s_uart1$UART1_Init$152
      00288A 03                    2376 	.db	3
      00288B 04                    2377 	.sleb128	4
      00288C 01                    2378 	.db	1
      00288D 09                    2379 	.db	9
      00288E 00 0D                 2380 	.dw	Sstm8s_uart1$UART1_Init$159-Sstm8s_uart1$UART1_Init$155
      002890 03                    2381 	.db	3
      002891 02                    2382 	.sleb128	2
      002892 01                    2383 	.db	1
      002893 09                    2384 	.db	9
      002894 00 03                 2385 	.dw	1+Sstm8s_uart1$UART1_Init$161-Sstm8s_uart1$UART1_Init$159
      002896 00                    2386 	.db	0
      002897 01                    2387 	.uleb128	1
      002898 01                    2388 	.db	1
      002899 00                    2389 	.db	0
      00289A 05                    2390 	.uleb128	5
      00289B 02                    2391 	.db	2
      00289C 00 00 A6 4A           2392 	.dw	0,(Sstm8s_uart1$UART1_Cmd$163)
      0028A0 03                    2393 	.db	3
      0028A1 B7 01                 2394 	.sleb128	183
      0028A3 01                    2395 	.db	1
      0028A4 09                    2396 	.db	9
      0028A5 00 00                 2397 	.dw	Sstm8s_uart1$UART1_Cmd$165-Sstm8s_uart1$UART1_Cmd$163
      0028A7 03                    2398 	.db	3
      0028A8 05                    2399 	.sleb128	5
      0028A9 01                    2400 	.db	1
      0028AA 09                    2401 	.db	9
      0028AB 00 03                 2402 	.dw	Sstm8s_uart1$UART1_Cmd$166-Sstm8s_uart1$UART1_Cmd$165
      0028AD 03                    2403 	.db	3
      0028AE 7D                    2404 	.sleb128	-3
      0028AF 01                    2405 	.db	1
      0028B0 09                    2406 	.db	9
      0028B1 00 04                 2407 	.dw	Sstm8s_uart1$UART1_Cmd$168-Sstm8s_uart1$UART1_Cmd$166
      0028B3 03                    2408 	.db	3
      0028B4 03                    2409 	.sleb128	3
      0028B5 01                    2410 	.db	1
      0028B6 09                    2411 	.db	9
      0028B7 00 07                 2412 	.dw	Sstm8s_uart1$UART1_Cmd$171-Sstm8s_uart1$UART1_Cmd$168
      0028B9 03                    2413 	.db	3
      0028BA 05                    2414 	.sleb128	5
      0028BB 01                    2415 	.db	1
      0028BC 09                    2416 	.db	9
      0028BD 00 05                 2417 	.dw	Sstm8s_uart1$UART1_Cmd$173-Sstm8s_uart1$UART1_Cmd$171
      0028BF 03                    2418 	.db	3
      0028C0 02                    2419 	.sleb128	2
      0028C1 01                    2420 	.db	1
      0028C2 09                    2421 	.db	9
      0028C3 00 01                 2422 	.dw	1+Sstm8s_uart1$UART1_Cmd$174-Sstm8s_uart1$UART1_Cmd$173
      0028C5 00                    2423 	.db	0
      0028C6 01                    2424 	.uleb128	1
      0028C7 01                    2425 	.db	1
      0028C8 00                    2426 	.db	0
      0028C9 05                    2427 	.uleb128	5
      0028CA 02                    2428 	.db	2
      0028CB 00 00 A6 5E           2429 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$176)
      0028CF 03                    2430 	.db	3
      0028D0 D2 01                 2431 	.sleb128	210
      0028D2 01                    2432 	.db	1
      0028D3 09                    2433 	.db	9
      0028D4 00 01                 2434 	.dw	Sstm8s_uart1$UART1_ITConfig$179-Sstm8s_uart1$UART1_ITConfig$176
      0028D6 03                    2435 	.db	3
      0028D7 05                    2436 	.sleb128	5
      0028D8 01                    2437 	.db	1
      0028D9 09                    2438 	.db	9
      0028DA 00 33                 2439 	.dw	Sstm8s_uart1$UART1_ITConfig$195-Sstm8s_uart1$UART1_ITConfig$179
      0028DC 03                    2440 	.db	3
      0028DD 01                    2441 	.sleb128	1
      0028DE 01                    2442 	.db	1
      0028DF 09                    2443 	.db	9
      0028E0 00 1C                 2444 	.dw	Sstm8s_uart1$UART1_ITConfig$206-Sstm8s_uart1$UART1_ITConfig$195
      0028E2 03                    2445 	.db	3
      0028E3 03                    2446 	.sleb128	3
      0028E4 01                    2447 	.db	1
      0028E5 09                    2448 	.db	9
      0028E6 00 00                 2449 	.dw	Sstm8s_uart1$UART1_ITConfig$207-Sstm8s_uart1$UART1_ITConfig$206
      0028E8 03                    2450 	.db	3
      0028E9 02                    2451 	.sleb128	2
      0028EA 01                    2452 	.db	1
      0028EB 09                    2453 	.db	9
      0028EC 00 12                 2454 	.dw	Sstm8s_uart1$UART1_ITConfig$210-Sstm8s_uart1$UART1_ITConfig$207
      0028EE 03                    2455 	.db	3
      0028EF 05                    2456 	.sleb128	5
      0028F0 01                    2457 	.db	1
      0028F1 09                    2458 	.db	9
      0028F2 00 0B                 2459 	.dw	Sstm8s_uart1$UART1_ITConfig$212-Sstm8s_uart1$UART1_ITConfig$210
      0028F4 03                    2460 	.db	3
      0028F5 04                    2461 	.sleb128	4
      0028F6 01                    2462 	.db	1
      0028F7 09                    2463 	.db	9
      0028F8 00 08                 2464 	.dw	Sstm8s_uart1$UART1_ITConfig$214-Sstm8s_uart1$UART1_ITConfig$212
      0028FA 03                    2465 	.db	3
      0028FB 79                    2466 	.sleb128	-7
      0028FC 01                    2467 	.db	1
      0028FD 09                    2468 	.db	9
      0028FE 00 04                 2469 	.dw	Sstm8s_uart1$UART1_ITConfig$216-Sstm8s_uart1$UART1_ITConfig$214
      002900 03                    2470 	.db	3
      002901 03                    2471 	.sleb128	3
      002902 01                    2472 	.db	1
      002903 09                    2473 	.db	9
      002904 00 04                 2474 	.dw	Sstm8s_uart1$UART1_ITConfig$218-Sstm8s_uart1$UART1_ITConfig$216
      002906 03                    2475 	.db	3
      002907 02                    2476 	.sleb128	2
      002908 01                    2477 	.db	1
      002909 09                    2478 	.db	9
      00290A 00 0B                 2479 	.dw	Sstm8s_uart1$UART1_ITConfig$220-Sstm8s_uart1$UART1_ITConfig$218
      00290C 03                    2480 	.db	3
      00290D 02                    2481 	.sleb128	2
      00290E 01                    2482 	.db	1
      00290F 09                    2483 	.db	9
      002910 00 03                 2484 	.dw	Sstm8s_uart1$UART1_ITConfig$222-Sstm8s_uart1$UART1_ITConfig$220
      002912 03                    2485 	.db	3
      002913 02                    2486 	.sleb128	2
      002914 01                    2487 	.db	1
      002915 09                    2488 	.db	9
      002916 00 0B                 2489 	.dw	Sstm8s_uart1$UART1_ITConfig$225-Sstm8s_uart1$UART1_ITConfig$222
      002918 03                    2490 	.db	3
      002919 04                    2491 	.sleb128	4
      00291A 01                    2492 	.db	1
      00291B 09                    2493 	.db	9
      00291C 00 0A                 2494 	.dw	Sstm8s_uart1$UART1_ITConfig$227-Sstm8s_uart1$UART1_ITConfig$225
      00291E 03                    2495 	.db	3
      00291F 08                    2496 	.sleb128	8
      002920 01                    2497 	.db	1
      002921 09                    2498 	.db	9
      002922 00 04                 2499 	.dw	Sstm8s_uart1$UART1_ITConfig$231-Sstm8s_uart1$UART1_ITConfig$227
      002924 03                    2500 	.db	3
      002925 7E                    2501 	.sleb128	-2
      002926 01                    2502 	.db	1
      002927 09                    2503 	.db	9
      002928 00 04                 2504 	.dw	Sstm8s_uart1$UART1_ITConfig$233-Sstm8s_uart1$UART1_ITConfig$231
      00292A 03                    2505 	.db	3
      00292B 02                    2506 	.sleb128	2
      00292C 01                    2507 	.db	1
      00292D 09                    2508 	.db	9
      00292E 00 0A                 2509 	.dw	Sstm8s_uart1$UART1_ITConfig$235-Sstm8s_uart1$UART1_ITConfig$233
      002930 03                    2510 	.db	3
      002931 02                    2511 	.sleb128	2
      002932 01                    2512 	.db	1
      002933 09                    2513 	.db	9
      002934 00 03                 2514 	.dw	Sstm8s_uart1$UART1_ITConfig$237-Sstm8s_uart1$UART1_ITConfig$235
      002936 03                    2515 	.db	3
      002937 02                    2516 	.sleb128	2
      002938 01                    2517 	.db	1
      002939 09                    2518 	.db	9
      00293A 00 0A                 2519 	.dw	Sstm8s_uart1$UART1_ITConfig$240-Sstm8s_uart1$UART1_ITConfig$237
      00293C 03                    2520 	.db	3
      00293D 04                    2521 	.sleb128	4
      00293E 01                    2522 	.db	1
      00293F 09                    2523 	.db	9
      002940 00 08                 2524 	.dw	Sstm8s_uart1$UART1_ITConfig$242-Sstm8s_uart1$UART1_ITConfig$240
      002942 03                    2525 	.db	3
      002943 04                    2526 	.sleb128	4
      002944 01                    2527 	.db	1
      002945 09                    2528 	.db	9
      002946 00 02                 2529 	.dw	1+Sstm8s_uart1$UART1_ITConfig$244-Sstm8s_uart1$UART1_ITConfig$242
      002948 00                    2530 	.db	0
      002949 01                    2531 	.uleb128	1
      00294A 01                    2532 	.db	1
      00294B 00                    2533 	.db	0
      00294C 05                    2534 	.uleb128	5
      00294D 02                    2535 	.db	2
      00294E 00 00 A7 27           2536 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$246)
      002952 03                    2537 	.db	3
      002953 88 02                 2538 	.sleb128	264
      002955 01                    2539 	.db	1
      002956 09                    2540 	.db	9
      002957 00 00                 2541 	.dw	Sstm8s_uart1$UART1_HalfDuplexCmd$248-Sstm8s_uart1$UART1_HalfDuplexCmd$246
      002959 03                    2542 	.db	3
      00295A 02                    2543 	.sleb128	2
      00295B 01                    2544 	.db	1
      00295C 09                    2545 	.db	9
      00295D 00 18                 2546 	.dw	Sstm8s_uart1$UART1_HalfDuplexCmd$256-Sstm8s_uart1$UART1_HalfDuplexCmd$248
      00295F 03                    2547 	.db	3
      002960 04                    2548 	.sleb128	4
      002961 01                    2549 	.db	1
      002962 09                    2550 	.db	9
      002963 00 03                 2551 	.dw	Sstm8s_uart1$UART1_HalfDuplexCmd$257-Sstm8s_uart1$UART1_HalfDuplexCmd$256
      002965 03                    2552 	.db	3
      002966 7E                    2553 	.sleb128	-2
      002967 01                    2554 	.db	1
      002968 09                    2555 	.db	9
      002969 00 04                 2556 	.dw	Sstm8s_uart1$UART1_HalfDuplexCmd$259-Sstm8s_uart1$UART1_HalfDuplexCmd$257
      00296B 03                    2557 	.db	3
      00296C 02                    2558 	.sleb128	2
      00296D 01                    2559 	.db	1
      00296E 09                    2560 	.db	9
      00296F 00 07                 2561 	.dw	Sstm8s_uart1$UART1_HalfDuplexCmd$262-Sstm8s_uart1$UART1_HalfDuplexCmd$259
      002971 03                    2562 	.db	3
      002972 04                    2563 	.sleb128	4
      002973 01                    2564 	.db	1
      002974 09                    2565 	.db	9
      002975 00 05                 2566 	.dw	Sstm8s_uart1$UART1_HalfDuplexCmd$264-Sstm8s_uart1$UART1_HalfDuplexCmd$262
      002977 03                    2567 	.db	3
      002978 02                    2568 	.sleb128	2
      002979 01                    2569 	.db	1
      00297A 09                    2570 	.db	9
      00297B 00 01                 2571 	.dw	1+Sstm8s_uart1$UART1_HalfDuplexCmd$265-Sstm8s_uart1$UART1_HalfDuplexCmd$264
      00297D 00                    2572 	.db	0
      00297E 01                    2573 	.uleb128	1
      00297F 01                    2574 	.db	1
      002980 00                    2575 	.db	0
      002981 05                    2576 	.uleb128	5
      002982 02                    2577 	.db	2
      002983 00 00 A7 53           2578 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$267)
      002987 03                    2579 	.db	3
      002988 9C 02                 2580 	.sleb128	284
      00298A 01                    2581 	.db	1
      00298B 09                    2582 	.db	9
      00298C 00 00                 2583 	.dw	Sstm8s_uart1$UART1_IrDAConfig$269-Sstm8s_uart1$UART1_IrDAConfig$267
      00298E 03                    2584 	.db	3
      00298F 02                    2585 	.sleb128	2
      002990 01                    2586 	.db	1
      002991 09                    2587 	.db	9
      002992 00 18                 2588 	.dw	Sstm8s_uart1$UART1_IrDAConfig$277-Sstm8s_uart1$UART1_IrDAConfig$269
      002994 03                    2589 	.db	3
      002995 04                    2590 	.sleb128	4
      002996 01                    2591 	.db	1
      002997 09                    2592 	.db	9
      002998 00 03                 2593 	.dw	Sstm8s_uart1$UART1_IrDAConfig$278-Sstm8s_uart1$UART1_IrDAConfig$277
      00299A 03                    2594 	.db	3
      00299B 7E                    2595 	.sleb128	-2
      00299C 01                    2596 	.db	1
      00299D 09                    2597 	.db	9
      00299E 00 04                 2598 	.dw	Sstm8s_uart1$UART1_IrDAConfig$280-Sstm8s_uart1$UART1_IrDAConfig$278
      0029A0 03                    2599 	.db	3
      0029A1 02                    2600 	.sleb128	2
      0029A2 01                    2601 	.db	1
      0029A3 09                    2602 	.db	9
      0029A4 00 07                 2603 	.dw	Sstm8s_uart1$UART1_IrDAConfig$283-Sstm8s_uart1$UART1_IrDAConfig$280
      0029A6 03                    2604 	.db	3
      0029A7 04                    2605 	.sleb128	4
      0029A8 01                    2606 	.db	1
      0029A9 09                    2607 	.db	9
      0029AA 00 05                 2608 	.dw	Sstm8s_uart1$UART1_IrDAConfig$285-Sstm8s_uart1$UART1_IrDAConfig$283
      0029AC 03                    2609 	.db	3
      0029AD 02                    2610 	.sleb128	2
      0029AE 01                    2611 	.db	1
      0029AF 09                    2612 	.db	9
      0029B0 00 01                 2613 	.dw	1+Sstm8s_uart1$UART1_IrDAConfig$286-Sstm8s_uart1$UART1_IrDAConfig$285
      0029B2 00                    2614 	.db	0
      0029B3 01                    2615 	.uleb128	1
      0029B4 01                    2616 	.db	1
      0029B5 00                    2617 	.db	0
      0029B6 05                    2618 	.uleb128	5
      0029B7 02                    2619 	.db	2
      0029B8 00 00 A7 7F           2620 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$288)
      0029BC 03                    2621 	.db	3
      0029BD B0 02                 2622 	.sleb128	304
      0029BF 01                    2623 	.db	1
      0029C0 09                    2624 	.db	9
      0029C1 00 00                 2625 	.dw	Sstm8s_uart1$UART1_IrDACmd$290-Sstm8s_uart1$UART1_IrDACmd$288
      0029C3 03                    2626 	.db	3
      0029C4 03                    2627 	.sleb128	3
      0029C5 01                    2628 	.db	1
      0029C6 09                    2629 	.db	9
      0029C7 00 18                 2630 	.dw	Sstm8s_uart1$UART1_IrDACmd$298-Sstm8s_uart1$UART1_IrDACmd$290
      0029C9 03                    2631 	.db	3
      0029CA 05                    2632 	.sleb128	5
      0029CB 01                    2633 	.db	1
      0029CC 09                    2634 	.db	9
      0029CD 00 03                 2635 	.dw	Sstm8s_uart1$UART1_IrDACmd$299-Sstm8s_uart1$UART1_IrDACmd$298
      0029CF 03                    2636 	.db	3
      0029D0 7D                    2637 	.sleb128	-3
      0029D1 01                    2638 	.db	1
      0029D2 09                    2639 	.db	9
      0029D3 00 04                 2640 	.dw	Sstm8s_uart1$UART1_IrDACmd$301-Sstm8s_uart1$UART1_IrDACmd$299
      0029D5 03                    2641 	.db	3
      0029D6 03                    2642 	.sleb128	3
      0029D7 01                    2643 	.db	1
      0029D8 09                    2644 	.db	9
      0029D9 00 07                 2645 	.dw	Sstm8s_uart1$UART1_IrDACmd$304-Sstm8s_uart1$UART1_IrDACmd$301
      0029DB 03                    2646 	.db	3
      0029DC 05                    2647 	.sleb128	5
      0029DD 01                    2648 	.db	1
      0029DE 09                    2649 	.db	9
      0029DF 00 05                 2650 	.dw	Sstm8s_uart1$UART1_IrDACmd$306-Sstm8s_uart1$UART1_IrDACmd$304
      0029E1 03                    2651 	.db	3
      0029E2 02                    2652 	.sleb128	2
      0029E3 01                    2653 	.db	1
      0029E4 09                    2654 	.db	9
      0029E5 00 01                 2655 	.dw	1+Sstm8s_uart1$UART1_IrDACmd$307-Sstm8s_uart1$UART1_IrDACmd$306
      0029E7 00                    2656 	.db	0
      0029E8 01                    2657 	.uleb128	1
      0029E9 01                    2658 	.db	1
      0029EA 00                    2659 	.db	0
      0029EB 05                    2660 	.uleb128	5
      0029EC 02                    2661 	.db	2
      0029ED 00 00 A7 AB           2662 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$309)
      0029F1 03                    2663 	.db	3
      0029F2 C8 02                 2664 	.sleb128	328
      0029F4 01                    2665 	.db	1
      0029F5 09                    2666 	.db	9
      0029F6 00 00                 2667 	.dw	Sstm8s_uart1$UART1_LINBreakDetectionConfig$311-Sstm8s_uart1$UART1_LINBreakDetectionConfig$309
      0029F8 03                    2668 	.db	3
      0029F9 02                    2669 	.sleb128	2
      0029FA 01                    2670 	.db	1
      0029FB 09                    2671 	.db	9
      0029FC 00 18                 2672 	.dw	Sstm8s_uart1$UART1_LINBreakDetectionConfig$319-Sstm8s_uart1$UART1_LINBreakDetectionConfig$311
      0029FE 03                    2673 	.db	3
      0029FF 04                    2674 	.sleb128	4
      002A00 01                    2675 	.db	1
      002A01 09                    2676 	.db	9
      002A02 00 03                 2677 	.dw	Sstm8s_uart1$UART1_LINBreakDetectionConfig$320-Sstm8s_uart1$UART1_LINBreakDetectionConfig$319
      002A04 03                    2678 	.db	3
      002A05 7E                    2679 	.sleb128	-2
      002A06 01                    2680 	.db	1
      002A07 09                    2681 	.db	9
      002A08 00 04                 2682 	.dw	Sstm8s_uart1$UART1_LINBreakDetectionConfig$322-Sstm8s_uart1$UART1_LINBreakDetectionConfig$320
      002A0A 03                    2683 	.db	3
      002A0B 02                    2684 	.sleb128	2
      002A0C 01                    2685 	.db	1
      002A0D 09                    2686 	.db	9
      002A0E 00 07                 2687 	.dw	Sstm8s_uart1$UART1_LINBreakDetectionConfig$325-Sstm8s_uart1$UART1_LINBreakDetectionConfig$322
      002A10 03                    2688 	.db	3
      002A11 04                    2689 	.sleb128	4
      002A12 01                    2690 	.db	1
      002A13 09                    2691 	.db	9
      002A14 00 05                 2692 	.dw	Sstm8s_uart1$UART1_LINBreakDetectionConfig$327-Sstm8s_uart1$UART1_LINBreakDetectionConfig$325
      002A16 03                    2693 	.db	3
      002A17 02                    2694 	.sleb128	2
      002A18 01                    2695 	.db	1
      002A19 09                    2696 	.db	9
      002A1A 00 01                 2697 	.dw	1+Sstm8s_uart1$UART1_LINBreakDetectionConfig$328-Sstm8s_uart1$UART1_LINBreakDetectionConfig$327
      002A1C 00                    2698 	.db	0
      002A1D 01                    2699 	.uleb128	1
      002A1E 01                    2700 	.db	1
      002A1F 00                    2701 	.db	0
      002A20 05                    2702 	.uleb128	5
      002A21 02                    2703 	.db	2
      002A22 00 00 A7 D7           2704 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$330)
      002A26 03                    2705 	.db	3
      002A27 DC 02                 2706 	.sleb128	348
      002A29 01                    2707 	.db	1
      002A2A 09                    2708 	.db	9
      002A2B 00 00                 2709 	.dw	Sstm8s_uart1$UART1_LINCmd$332-Sstm8s_uart1$UART1_LINCmd$330
      002A2D 03                    2710 	.db	3
      002A2E 02                    2711 	.sleb128	2
      002A2F 01                    2712 	.db	1
      002A30 09                    2713 	.db	9
      002A31 00 18                 2714 	.dw	Sstm8s_uart1$UART1_LINCmd$340-Sstm8s_uart1$UART1_LINCmd$332
      002A33 03                    2715 	.db	3
      002A34 05                    2716 	.sleb128	5
      002A35 01                    2717 	.db	1
      002A36 09                    2718 	.db	9
      002A37 00 03                 2719 	.dw	Sstm8s_uart1$UART1_LINCmd$341-Sstm8s_uart1$UART1_LINCmd$340
      002A39 03                    2720 	.db	3
      002A3A 7D                    2721 	.sleb128	-3
      002A3B 01                    2722 	.db	1
      002A3C 09                    2723 	.db	9
      002A3D 00 04                 2724 	.dw	Sstm8s_uart1$UART1_LINCmd$343-Sstm8s_uart1$UART1_LINCmd$341
      002A3F 03                    2725 	.db	3
      002A40 03                    2726 	.sleb128	3
      002A41 01                    2727 	.db	1
      002A42 09                    2728 	.db	9
      002A43 00 07                 2729 	.dw	Sstm8s_uart1$UART1_LINCmd$346-Sstm8s_uart1$UART1_LINCmd$343
      002A45 03                    2730 	.db	3
      002A46 05                    2731 	.sleb128	5
      002A47 01                    2732 	.db	1
      002A48 09                    2733 	.db	9
      002A49 00 05                 2734 	.dw	Sstm8s_uart1$UART1_LINCmd$348-Sstm8s_uart1$UART1_LINCmd$346
      002A4B 03                    2735 	.db	3
      002A4C 02                    2736 	.sleb128	2
      002A4D 01                    2737 	.db	1
      002A4E 09                    2738 	.db	9
      002A4F 00 01                 2739 	.dw	1+Sstm8s_uart1$UART1_LINCmd$349-Sstm8s_uart1$UART1_LINCmd$348
      002A51 00                    2740 	.db	0
      002A52 01                    2741 	.uleb128	1
      002A53 01                    2742 	.db	1
      002A54 00                    2743 	.db	0
      002A55 05                    2744 	.uleb128	5
      002A56 02                    2745 	.db	2
      002A57 00 00 A8 03           2746 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$351)
      002A5B 03                    2747 	.db	3
      002A5C F2 02                 2748 	.sleb128	370
      002A5E 01                    2749 	.db	1
      002A5F 09                    2750 	.db	9
      002A60 00 00                 2751 	.dw	Sstm8s_uart1$UART1_SmartCardCmd$353-Sstm8s_uart1$UART1_SmartCardCmd$351
      002A62 03                    2752 	.db	3
      002A63 02                    2753 	.sleb128	2
      002A64 01                    2754 	.db	1
      002A65 09                    2755 	.db	9
      002A66 00 18                 2756 	.dw	Sstm8s_uart1$UART1_SmartCardCmd$361-Sstm8s_uart1$UART1_SmartCardCmd$353
      002A68 03                    2757 	.db	3
      002A69 05                    2758 	.sleb128	5
      002A6A 01                    2759 	.db	1
      002A6B 09                    2760 	.db	9
      002A6C 00 03                 2761 	.dw	Sstm8s_uart1$UART1_SmartCardCmd$362-Sstm8s_uart1$UART1_SmartCardCmd$361
      002A6E 03                    2762 	.db	3
      002A6F 7D                    2763 	.sleb128	-3
      002A70 01                    2764 	.db	1
      002A71 09                    2765 	.db	9
      002A72 00 04                 2766 	.dw	Sstm8s_uart1$UART1_SmartCardCmd$364-Sstm8s_uart1$UART1_SmartCardCmd$362
      002A74 03                    2767 	.db	3
      002A75 03                    2768 	.sleb128	3
      002A76 01                    2769 	.db	1
      002A77 09                    2770 	.db	9
      002A78 00 07                 2771 	.dw	Sstm8s_uart1$UART1_SmartCardCmd$367-Sstm8s_uart1$UART1_SmartCardCmd$364
      002A7A 03                    2772 	.db	3
      002A7B 05                    2773 	.sleb128	5
      002A7C 01                    2774 	.db	1
      002A7D 09                    2775 	.db	9
      002A7E 00 05                 2776 	.dw	Sstm8s_uart1$UART1_SmartCardCmd$369-Sstm8s_uart1$UART1_SmartCardCmd$367
      002A80 03                    2777 	.db	3
      002A81 02                    2778 	.sleb128	2
      002A82 01                    2779 	.db	1
      002A83 09                    2780 	.db	9
      002A84 00 01                 2781 	.dw	1+Sstm8s_uart1$UART1_SmartCardCmd$370-Sstm8s_uart1$UART1_SmartCardCmd$369
      002A86 00                    2782 	.db	0
      002A87 01                    2783 	.uleb128	1
      002A88 01                    2784 	.db	1
      002A89 00                    2785 	.db	0
      002A8A 05                    2786 	.uleb128	5
      002A8B 02                    2787 	.db	2
      002A8C 00 00 A8 2F           2788 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$372)
      002A90 03                    2789 	.db	3
      002A91 89 03                 2790 	.sleb128	393
      002A93 01                    2791 	.db	1
      002A94 09                    2792 	.db	9
      002A95 00 00                 2793 	.dw	Sstm8s_uart1$UART1_SmartCardNACKCmd$374-Sstm8s_uart1$UART1_SmartCardNACKCmd$372
      002A97 03                    2794 	.db	3
      002A98 02                    2795 	.sleb128	2
      002A99 01                    2796 	.db	1
      002A9A 09                    2797 	.db	9
      002A9B 00 18                 2798 	.dw	Sstm8s_uart1$UART1_SmartCardNACKCmd$382-Sstm8s_uart1$UART1_SmartCardNACKCmd$374
      002A9D 03                    2799 	.db	3
      002A9E 05                    2800 	.sleb128	5
      002A9F 01                    2801 	.db	1
      002AA0 09                    2802 	.db	9
      002AA1 00 03                 2803 	.dw	Sstm8s_uart1$UART1_SmartCardNACKCmd$383-Sstm8s_uart1$UART1_SmartCardNACKCmd$382
      002AA3 03                    2804 	.db	3
      002AA4 7D                    2805 	.sleb128	-3
      002AA5 01                    2806 	.db	1
      002AA6 09                    2807 	.db	9
      002AA7 00 04                 2808 	.dw	Sstm8s_uart1$UART1_SmartCardNACKCmd$385-Sstm8s_uart1$UART1_SmartCardNACKCmd$383
      002AA9 03                    2809 	.db	3
      002AAA 03                    2810 	.sleb128	3
      002AAB 01                    2811 	.db	1
      002AAC 09                    2812 	.db	9
      002AAD 00 07                 2813 	.dw	Sstm8s_uart1$UART1_SmartCardNACKCmd$388-Sstm8s_uart1$UART1_SmartCardNACKCmd$385
      002AAF 03                    2814 	.db	3
      002AB0 05                    2815 	.sleb128	5
      002AB1 01                    2816 	.db	1
      002AB2 09                    2817 	.db	9
      002AB3 00 05                 2818 	.dw	Sstm8s_uart1$UART1_SmartCardNACKCmd$390-Sstm8s_uart1$UART1_SmartCardNACKCmd$388
      002AB5 03                    2819 	.db	3
      002AB6 02                    2820 	.sleb128	2
      002AB7 01                    2821 	.db	1
      002AB8 09                    2822 	.db	9
      002AB9 00 01                 2823 	.dw	1+Sstm8s_uart1$UART1_SmartCardNACKCmd$391-Sstm8s_uart1$UART1_SmartCardNACKCmd$390
      002ABB 00                    2824 	.db	0
      002ABC 01                    2825 	.uleb128	1
      002ABD 01                    2826 	.db	1
      002ABE 00                    2827 	.db	0
      002ABF 05                    2828 	.uleb128	5
      002AC0 02                    2829 	.db	2
      002AC1 00 00 A8 5B           2830 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$393)
      002AC5 03                    2831 	.db	3
      002AC6 9F 03                 2832 	.sleb128	415
      002AC8 01                    2833 	.db	1
      002AC9 09                    2834 	.db	9
      002ACA 00 00                 2835 	.dw	Sstm8s_uart1$UART1_WakeUpConfig$395-Sstm8s_uart1$UART1_WakeUpConfig$393
      002ACC 03                    2836 	.db	3
      002ACD 02                    2837 	.sleb128	2
      002ACE 01                    2838 	.db	1
      002ACF 09                    2839 	.db	9
      002AD0 00 19                 2840 	.dw	Sstm8s_uart1$UART1_WakeUpConfig$403-Sstm8s_uart1$UART1_WakeUpConfig$395
      002AD2 03                    2841 	.db	3
      002AD3 02                    2842 	.sleb128	2
      002AD4 01                    2843 	.db	1
      002AD5 09                    2844 	.db	9
      002AD6 00 04                 2845 	.dw	Sstm8s_uart1$UART1_WakeUpConfig$404-Sstm8s_uart1$UART1_WakeUpConfig$403
      002AD8 03                    2846 	.db	3
      002AD9 01                    2847 	.sleb128	1
      002ADA 01                    2848 	.db	1
      002ADB 09                    2849 	.db	9
      002ADC 00 08                 2850 	.dw	Sstm8s_uart1$UART1_WakeUpConfig$405-Sstm8s_uart1$UART1_WakeUpConfig$404
      002ADE 03                    2851 	.db	3
      002ADF 01                    2852 	.sleb128	1
      002AE0 01                    2853 	.db	1
      002AE1 09                    2854 	.db	9
      002AE2 00 01                 2855 	.dw	1+Sstm8s_uart1$UART1_WakeUpConfig$406-Sstm8s_uart1$UART1_WakeUpConfig$405
      002AE4 00                    2856 	.db	0
      002AE5 01                    2857 	.uleb128	1
      002AE6 01                    2858 	.db	1
      002AE7 00                    2859 	.db	0
      002AE8 05                    2860 	.uleb128	5
      002AE9 02                    2861 	.db	2
      002AEA 00 00 A8 81           2862 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$408)
      002AEE 03                    2863 	.db	3
      002AEF AD 03                 2864 	.sleb128	429
      002AF1 01                    2865 	.db	1
      002AF2 09                    2866 	.db	9
      002AF3 00 00                 2867 	.dw	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$410-Sstm8s_uart1$UART1_ReceiverWakeUpCmd$408
      002AF5 03                    2868 	.db	3
      002AF6 02                    2869 	.sleb128	2
      002AF7 01                    2870 	.db	1
      002AF8 09                    2871 	.db	9
      002AF9 00 18                 2872 	.dw	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$418-Sstm8s_uart1$UART1_ReceiverWakeUpCmd$410
      002AFB 03                    2873 	.db	3
      002AFC 05                    2874 	.sleb128	5
      002AFD 01                    2875 	.db	1
      002AFE 09                    2876 	.db	9
      002AFF 00 03                 2877 	.dw	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$419-Sstm8s_uart1$UART1_ReceiverWakeUpCmd$418
      002B01 03                    2878 	.db	3
      002B02 7D                    2879 	.sleb128	-3
      002B03 01                    2880 	.db	1
      002B04 09                    2881 	.db	9
      002B05 00 04                 2882 	.dw	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$421-Sstm8s_uart1$UART1_ReceiverWakeUpCmd$419
      002B07 03                    2883 	.db	3
      002B08 03                    2884 	.sleb128	3
      002B09 01                    2885 	.db	1
      002B0A 09                    2886 	.db	9
      002B0B 00 07                 2887 	.dw	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$424-Sstm8s_uart1$UART1_ReceiverWakeUpCmd$421
      002B0D 03                    2888 	.db	3
      002B0E 05                    2889 	.sleb128	5
      002B0F 01                    2890 	.db	1
      002B10 09                    2891 	.db	9
      002B11 00 05                 2892 	.dw	Sstm8s_uart1$UART1_ReceiverWakeUpCmd$426-Sstm8s_uart1$UART1_ReceiverWakeUpCmd$424
      002B13 03                    2893 	.db	3
      002B14 02                    2894 	.sleb128	2
      002B15 01                    2895 	.db	1
      002B16 09                    2896 	.db	9
      002B17 00 01                 2897 	.dw	1+Sstm8s_uart1$UART1_ReceiverWakeUpCmd$427-Sstm8s_uart1$UART1_ReceiverWakeUpCmd$426
      002B19 00                    2898 	.db	0
      002B1A 01                    2899 	.uleb128	1
      002B1B 01                    2900 	.db	1
      002B1C 00                    2901 	.db	0
      002B1D 05                    2902 	.uleb128	5
      002B1E 02                    2903 	.db	2
      002B1F 00 00 A8 AD           2904 	.dw	0,(Sstm8s_uart1$UART1_ReceiveData8$429)
      002B23 03                    2905 	.db	3
      002B24 C2 03                 2906 	.sleb128	450
      002B26 01                    2907 	.db	1
      002B27 09                    2908 	.db	9
      002B28 00 00                 2909 	.dw	Sstm8s_uart1$UART1_ReceiveData8$431-Sstm8s_uart1$UART1_ReceiveData8$429
      002B2A 03                    2910 	.db	3
      002B2B 02                    2911 	.sleb128	2
      002B2C 01                    2912 	.db	1
      002B2D 09                    2913 	.db	9
      002B2E 00 03                 2914 	.dw	Sstm8s_uart1$UART1_ReceiveData8$432-Sstm8s_uart1$UART1_ReceiveData8$431
      002B30 03                    2915 	.db	3
      002B31 01                    2916 	.sleb128	1
      002B32 01                    2917 	.db	1
      002B33 09                    2918 	.db	9
      002B34 00 01                 2919 	.dw	1+Sstm8s_uart1$UART1_ReceiveData8$433-Sstm8s_uart1$UART1_ReceiveData8$432
      002B36 00                    2920 	.db	0
      002B37 01                    2921 	.uleb128	1
      002B38 01                    2922 	.db	1
      002B39 00                    2923 	.db	0
      002B3A 05                    2924 	.uleb128	5
      002B3B 02                    2925 	.db	2
      002B3C 00 00 A8 B1           2926 	.dw	0,(Sstm8s_uart1$UART1_ReceiveData9$435)
      002B40 03                    2927 	.db	3
      002B41 CC 03                 2928 	.sleb128	460
      002B43 01                    2929 	.db	1
      002B44 09                    2930 	.db	9
      002B45 00 01                 2931 	.dw	Sstm8s_uart1$UART1_ReceiveData9$438-Sstm8s_uart1$UART1_ReceiveData9$435
      002B47 03                    2932 	.db	3
      002B48 04                    2933 	.sleb128	4
      002B49 01                    2934 	.db	1
      002B4A 09                    2935 	.db	9
      002B4B 00 0B                 2936 	.dw	Sstm8s_uart1$UART1_ReceiveData9$439-Sstm8s_uart1$UART1_ReceiveData9$438
      002B4D 03                    2937 	.db	3
      002B4E 01                    2938 	.sleb128	1
      002B4F 01                    2939 	.db	1
      002B50 09                    2940 	.db	9
      002B51 00 0C                 2941 	.dw	Sstm8s_uart1$UART1_ReceiveData9$440-Sstm8s_uart1$UART1_ReceiveData9$439
      002B53 03                    2942 	.db	3
      002B54 01                    2943 	.sleb128	1
      002B55 01                    2944 	.db	1
      002B56 09                    2945 	.db	9
      002B57 00 03                 2946 	.dw	1+Sstm8s_uart1$UART1_ReceiveData9$442-Sstm8s_uart1$UART1_ReceiveData9$440
      002B59 00                    2947 	.db	0
      002B5A 01                    2948 	.uleb128	1
      002B5B 01                    2949 	.db	1
      002B5C 00                    2950 	.db	0
      002B5D 05                    2951 	.uleb128	5
      002B5E 02                    2952 	.db	2
      002B5F 00 00 A8 CC           2953 	.dw	0,(Sstm8s_uart1$UART1_SendData8$444)
      002B63 03                    2954 	.db	3
      002B64 D9 03                 2955 	.sleb128	473
      002B66 01                    2956 	.db	1
      002B67 09                    2957 	.db	9
      002B68 00 00                 2958 	.dw	Sstm8s_uart1$UART1_SendData8$446-Sstm8s_uart1$UART1_SendData8$444
      002B6A 03                    2959 	.db	3
      002B6B 03                    2960 	.sleb128	3
      002B6C 01                    2961 	.db	1
      002B6D 09                    2962 	.db	9
      002B6E 00 06                 2963 	.dw	Sstm8s_uart1$UART1_SendData8$447-Sstm8s_uart1$UART1_SendData8$446
      002B70 03                    2964 	.db	3
      002B71 01                    2965 	.sleb128	1
      002B72 01                    2966 	.db	1
      002B73 09                    2967 	.db	9
      002B74 00 01                 2968 	.dw	1+Sstm8s_uart1$UART1_SendData8$448-Sstm8s_uart1$UART1_SendData8$447
      002B76 00                    2969 	.db	0
      002B77 01                    2970 	.uleb128	1
      002B78 01                    2971 	.db	1
      002B79 00                    2972 	.db	0
      002B7A 05                    2973 	.uleb128	5
      002B7B 02                    2974 	.db	2
      002B7C 00 00 A8 D3           2975 	.dw	0,(Sstm8s_uart1$UART1_SendData9$450)
      002B80 03                    2976 	.db	3
      002B81 E5 03                 2977 	.sleb128	485
      002B83 01                    2978 	.db	1
      002B84 09                    2979 	.db	9
      002B85 00 01                 2980 	.dw	Sstm8s_uart1$UART1_SendData9$453-Sstm8s_uart1$UART1_SendData9$450
      002B87 03                    2981 	.db	3
      002B88 03                    2982 	.sleb128	3
      002B89 01                    2983 	.db	1
      002B8A 09                    2984 	.db	9
      002B8B 00 04                 2985 	.dw	Sstm8s_uart1$UART1_SendData9$454-Sstm8s_uart1$UART1_SendData9$453
      002B8D 03                    2986 	.db	3
      002B8E 02                    2987 	.sleb128	2
      002B8F 01                    2988 	.db	1
      002B90 09                    2989 	.db	9
      002B91 00 11                 2990 	.dw	Sstm8s_uart1$UART1_SendData9$455-Sstm8s_uart1$UART1_SendData9$454
      002B93 03                    2991 	.db	3
      002B94 02                    2992 	.sleb128	2
      002B95 01                    2993 	.db	1
      002B96 09                    2994 	.db	9
      002B97 00 05                 2995 	.dw	Sstm8s_uart1$UART1_SendData9$456-Sstm8s_uart1$UART1_SendData9$455
      002B99 03                    2996 	.db	3
      002B9A 01                    2997 	.sleb128	1
      002B9B 01                    2998 	.db	1
      002B9C 09                    2999 	.db	9
      002B9D 00 02                 3000 	.dw	1+Sstm8s_uart1$UART1_SendData9$458-Sstm8s_uart1$UART1_SendData9$456
      002B9F 00                    3001 	.db	0
      002BA0 01                    3002 	.uleb128	1
      002BA1 01                    3003 	.db	1
      002BA2 00                    3004 	.db	0
      002BA3 05                    3005 	.uleb128	5
      002BA4 02                    3006 	.db	2
      002BA5 00 00 A8 F0           3007 	.dw	0,(Sstm8s_uart1$UART1_SendBreak$460)
      002BA9 03                    3008 	.db	3
      002BAA F4 03                 3009 	.sleb128	500
      002BAC 01                    3010 	.db	1
      002BAD 09                    3011 	.db	9
      002BAE 00 00                 3012 	.dw	Sstm8s_uart1$UART1_SendBreak$462-Sstm8s_uart1$UART1_SendBreak$460
      002BB0 03                    3013 	.db	3
      002BB1 02                    3014 	.sleb128	2
      002BB2 01                    3015 	.db	1
      002BB3 09                    3016 	.db	9
      002BB4 00 04                 3017 	.dw	Sstm8s_uart1$UART1_SendBreak$463-Sstm8s_uart1$UART1_SendBreak$462
      002BB6 03                    3018 	.db	3
      002BB7 01                    3019 	.sleb128	1
      002BB8 01                    3020 	.db	1
      002BB9 09                    3021 	.db	9
      002BBA 00 01                 3022 	.dw	1+Sstm8s_uart1$UART1_SendBreak$464-Sstm8s_uart1$UART1_SendBreak$463
      002BBC 00                    3023 	.db	0
      002BBD 01                    3024 	.uleb128	1
      002BBE 01                    3025 	.db	1
      002BBF 00                    3026 	.db	0
      002BC0 05                    3027 	.uleb128	5
      002BC1 02                    3028 	.db	2
      002BC2 00 00 A8 F5           3029 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$466)
      002BC6 03                    3030 	.db	3
      002BC7 FE 03                 3031 	.sleb128	510
      002BC9 01                    3032 	.db	1
      002BCA 09                    3033 	.db	9
      002BCB 00 00                 3034 	.dw	Sstm8s_uart1$UART1_SetAddress$468-Sstm8s_uart1$UART1_SetAddress$466
      002BCD 03                    3035 	.db	3
      002BCE 03                    3036 	.sleb128	3
      002BCF 01                    3037 	.db	1
      002BD0 09                    3038 	.db	9
      002BD1 00 15                 3039 	.dw	Sstm8s_uart1$UART1_SetAddress$475-Sstm8s_uart1$UART1_SetAddress$468
      002BD3 03                    3040 	.db	3
      002BD4 03                    3041 	.sleb128	3
      002BD5 01                    3042 	.db	1
      002BD6 09                    3043 	.db	9
      002BD7 00 08                 3044 	.dw	Sstm8s_uart1$UART1_SetAddress$476-Sstm8s_uart1$UART1_SetAddress$475
      002BD9 03                    3045 	.db	3
      002BDA 02                    3046 	.sleb128	2
      002BDB 01                    3047 	.db	1
      002BDC 09                    3048 	.db	9
      002BDD 00 08                 3049 	.dw	Sstm8s_uart1$UART1_SetAddress$477-Sstm8s_uart1$UART1_SetAddress$476
      002BDF 03                    3050 	.db	3
      002BE0 01                    3051 	.sleb128	1
      002BE1 01                    3052 	.db	1
      002BE2 09                    3053 	.db	9
      002BE3 00 01                 3054 	.dw	1+Sstm8s_uart1$UART1_SetAddress$478-Sstm8s_uart1$UART1_SetAddress$477
      002BE5 00                    3055 	.db	0
      002BE6 01                    3056 	.uleb128	1
      002BE7 01                    3057 	.db	1
      002BE8 00                    3058 	.db	0
      002BE9 05                    3059 	.uleb128	5
      002BEA 02                    3060 	.db	2
      002BEB 00 00 A9 1B           3061 	.dw	0,(Sstm8s_uart1$UART1_SetGuardTime$480)
      002BEF 03                    3062 	.db	3
      002BF0 8F 04                 3063 	.sleb128	527
      002BF2 01                    3064 	.db	1
      002BF3 09                    3065 	.db	9
      002BF4 00 00                 3066 	.dw	Sstm8s_uart1$UART1_SetGuardTime$482-Sstm8s_uart1$UART1_SetGuardTime$480
      002BF6 03                    3067 	.db	3
      002BF7 03                    3068 	.sleb128	3
      002BF8 01                    3069 	.db	1
      002BF9 09                    3070 	.db	9
      002BFA 00 06                 3071 	.dw	Sstm8s_uart1$UART1_SetGuardTime$483-Sstm8s_uart1$UART1_SetGuardTime$482
      002BFC 03                    3072 	.db	3
      002BFD 01                    3073 	.sleb128	1
      002BFE 01                    3074 	.db	1
      002BFF 09                    3075 	.db	9
      002C00 00 01                 3076 	.dw	1+Sstm8s_uart1$UART1_SetGuardTime$484-Sstm8s_uart1$UART1_SetGuardTime$483
      002C02 00                    3077 	.db	0
      002C03 01                    3078 	.uleb128	1
      002C04 01                    3079 	.db	1
      002C05 00                    3080 	.db	0
      002C06 05                    3081 	.uleb128	5
      002C07 02                    3082 	.db	2
      002C08 00 00 A9 22           3083 	.dw	0,(Sstm8s_uart1$UART1_SetPrescaler$486)
      002C0C 03                    3084 	.db	3
      002C0D AB 04                 3085 	.sleb128	555
      002C0F 01                    3086 	.db	1
      002C10 09                    3087 	.db	9
      002C11 00 00                 3088 	.dw	Sstm8s_uart1$UART1_SetPrescaler$488-Sstm8s_uart1$UART1_SetPrescaler$486
      002C13 03                    3089 	.db	3
      002C14 03                    3090 	.sleb128	3
      002C15 01                    3091 	.db	1
      002C16 09                    3092 	.db	9
      002C17 00 06                 3093 	.dw	Sstm8s_uart1$UART1_SetPrescaler$489-Sstm8s_uart1$UART1_SetPrescaler$488
      002C19 03                    3094 	.db	3
      002C1A 01                    3095 	.sleb128	1
      002C1B 01                    3096 	.db	1
      002C1C 09                    3097 	.db	9
      002C1D 00 01                 3098 	.dw	1+Sstm8s_uart1$UART1_SetPrescaler$490-Sstm8s_uart1$UART1_SetPrescaler$489
      002C1F 00                    3099 	.db	0
      002C20 01                    3100 	.uleb128	1
      002C21 01                    3101 	.db	1
      002C22 00                    3102 	.db	0
      002C23 05                    3103 	.uleb128	5
      002C24 02                    3104 	.db	2
      002C25 00 00 A9 29           3105 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$492)
      002C29 03                    3106 	.db	3
      002C2A B7 04                 3107 	.sleb128	567
      002C2C 01                    3108 	.db	1
      002C2D 09                    3109 	.db	9
      002C2E 00 01                 3110 	.dw	Sstm8s_uart1$UART1_GetFlagStatus$495-Sstm8s_uart1$UART1_GetFlagStatus$492
      002C30 03                    3111 	.db	3
      002C31 05                    3112 	.sleb128	5
      002C32 01                    3113 	.db	1
      002C33 09                    3114 	.db	9
      002C34 00 5B                 3115 	.dw	Sstm8s_uart1$UART1_GetFlagStatus$514-Sstm8s_uart1$UART1_GetFlagStatus$495
      002C36 03                    3116 	.db	3
      002C37 06                    3117 	.sleb128	6
      002C38 01                    3118 	.db	1
      002C39 09                    3119 	.db	9
      002C3A 00 06                 3120 	.dw	Sstm8s_uart1$UART1_GetFlagStatus$517-Sstm8s_uart1$UART1_GetFlagStatus$514
      002C3C 03                    3121 	.db	3
      002C3D 7E                    3122 	.sleb128	-2
      002C3E 01                    3123 	.db	1
      002C3F 09                    3124 	.db	9
      002C40 00 03                 3125 	.dw	Sstm8s_uart1$UART1_GetFlagStatus$519-Sstm8s_uart1$UART1_GetFlagStatus$517
      002C42 03                    3126 	.db	3
      002C43 02                    3127 	.sleb128	2
      002C44 01                    3128 	.db	1
      002C45 09                    3129 	.db	9
      002C46 00 07                 3130 	.dw	Sstm8s_uart1$UART1_GetFlagStatus$521-Sstm8s_uart1$UART1_GetFlagStatus$519
      002C48 03                    3131 	.db	3
      002C49 03                    3132 	.sleb128	3
      002C4A 01                    3133 	.db	1
      002C4B 09                    3134 	.db	9
      002C4C 00 05                 3135 	.dw	Sstm8s_uart1$UART1_GetFlagStatus$524-Sstm8s_uart1$UART1_GetFlagStatus$521
      002C4E 03                    3136 	.db	3
      002C4F 05                    3137 	.sleb128	5
      002C50 01                    3138 	.db	1
      002C51 09                    3139 	.db	9
      002C52 00 03                 3140 	.dw	Sstm8s_uart1$UART1_GetFlagStatus$526-Sstm8s_uart1$UART1_GetFlagStatus$524
      002C54 03                    3141 	.db	3
      002C55 03                    3142 	.sleb128	3
      002C56 01                    3143 	.db	1
      002C57 09                    3144 	.db	9
      002C58 00 04                 3145 	.dw	Sstm8s_uart1$UART1_GetFlagStatus$528-Sstm8s_uart1$UART1_GetFlagStatus$526
      002C5A 03                    3146 	.db	3
      002C5B 02                    3147 	.sleb128	2
      002C5C 01                    3148 	.db	1
      002C5D 09                    3149 	.db	9
      002C5E 00 07                 3150 	.dw	Sstm8s_uart1$UART1_GetFlagStatus$530-Sstm8s_uart1$UART1_GetFlagStatus$528
      002C60 03                    3151 	.db	3
      002C61 03                    3152 	.sleb128	3
      002C62 01                    3153 	.db	1
      002C63 09                    3154 	.db	9
      002C64 00 04                 3155 	.dw	Sstm8s_uart1$UART1_GetFlagStatus$533-Sstm8s_uart1$UART1_GetFlagStatus$530
      002C66 03                    3156 	.db	3
      002C67 05                    3157 	.sleb128	5
      002C68 01                    3158 	.db	1
      002C69 09                    3159 	.db	9
      002C6A 00 03                 3160 	.dw	Sstm8s_uart1$UART1_GetFlagStatus$536-Sstm8s_uart1$UART1_GetFlagStatus$533
      002C6C 03                    3161 	.db	3
      002C6D 05                    3162 	.sleb128	5
      002C6E 01                    3163 	.db	1
      002C6F 09                    3164 	.db	9
      002C70 00 07                 3165 	.dw	Sstm8s_uart1$UART1_GetFlagStatus$538-Sstm8s_uart1$UART1_GetFlagStatus$536
      002C72 03                    3166 	.db	3
      002C73 03                    3167 	.sleb128	3
      002C74 01                    3168 	.db	1
      002C75 09                    3169 	.db	9
      002C76 00 04                 3170 	.dw	Sstm8s_uart1$UART1_GetFlagStatus$541-Sstm8s_uart1$UART1_GetFlagStatus$538
      002C78 03                    3171 	.db	3
      002C79 05                    3172 	.sleb128	5
      002C7A 01                    3173 	.db	1
      002C7B 09                    3174 	.db	9
      002C7C 00 01                 3175 	.dw	Sstm8s_uart1$UART1_GetFlagStatus$543-Sstm8s_uart1$UART1_GetFlagStatus$541
      002C7E 03                    3176 	.db	3
      002C7F 04                    3177 	.sleb128	4
      002C80 01                    3178 	.db	1
      002C81 09                    3179 	.db	9
      002C82 00 00                 3180 	.dw	Sstm8s_uart1$UART1_GetFlagStatus$544-Sstm8s_uart1$UART1_GetFlagStatus$543
      002C84 03                    3181 	.db	3
      002C85 01                    3182 	.sleb128	1
      002C86 01                    3183 	.db	1
      002C87 09                    3184 	.db	9
      002C88 00 02                 3185 	.dw	1+Sstm8s_uart1$UART1_GetFlagStatus$546-Sstm8s_uart1$UART1_GetFlagStatus$544
      002C8A 00                    3186 	.db	0
      002C8B 01                    3187 	.uleb128	1
      002C8C 01                    3188 	.db	1
      002C8D 00                    3189 	.db	0
      002C8E 05                    3190 	.uleb128	5
      002C8F 02                    3191 	.db	2
      002C90 00 00 A9 BD           3192 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$548)
      002C94 03                    3193 	.db	3
      002C95 85 05                 3194 	.sleb128	645
      002C97 01                    3195 	.db	1
      002C98 09                    3196 	.db	9
      002C99 00 00                 3197 	.dw	Sstm8s_uart1$UART1_ClearFlag$550-Sstm8s_uart1$UART1_ClearFlag$548
      002C9B 03                    3198 	.db	3
      002C9C 02                    3199 	.sleb128	2
      002C9D 01                    3200 	.db	1
      002C9E 09                    3201 	.db	9
      002C9F 00 24                 3202 	.dw	Sstm8s_uart1$UART1_ClearFlag$561-Sstm8s_uart1$UART1_ClearFlag$550
      002CA1 03                    3203 	.db	3
      002CA2 03                    3204 	.sleb128	3
      002CA3 01                    3205 	.db	1
      002CA4 09                    3206 	.db	9
      002CA5 00 03                 3207 	.dw	Sstm8s_uart1$UART1_ClearFlag$563-Sstm8s_uart1$UART1_ClearFlag$561
      002CA7 03                    3208 	.db	3
      002CA8 02                    3209 	.sleb128	2
      002CA9 01                    3210 	.db	1
      002CAA 09                    3211 	.db	9
      002CAB 00 06                 3212 	.dw	Sstm8s_uart1$UART1_ClearFlag$566-Sstm8s_uart1$UART1_ClearFlag$563
      002CAD 03                    3213 	.db	3
      002CAE 05                    3214 	.sleb128	5
      002CAF 01                    3215 	.db	1
      002CB0 09                    3216 	.db	9
      002CB1 00 04                 3217 	.dw	Sstm8s_uart1$UART1_ClearFlag$568-Sstm8s_uart1$UART1_ClearFlag$566
      002CB3 03                    3218 	.db	3
      002CB4 02                    3219 	.sleb128	2
      002CB5 01                    3220 	.db	1
      002CB6 09                    3221 	.db	9
      002CB7 00 01                 3222 	.dw	1+Sstm8s_uart1$UART1_ClearFlag$569-Sstm8s_uart1$UART1_ClearFlag$568
      002CB9 00                    3223 	.db	0
      002CBA 01                    3224 	.uleb128	1
      002CBB 01                    3225 	.db	1
      002CBC 00                    3226 	.db	0
      002CBD 05                    3227 	.uleb128	5
      002CBE 02                    3228 	.db	2
      002CBF 00 00 A9 EF           3229 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$571)
      002CC3 03                    3230 	.db	3
      002CC4 A2 05                 3231 	.sleb128	674
      002CC6 01                    3232 	.db	1
      002CC7 09                    3233 	.db	9
      002CC8 00 02                 3234 	.dw	Sstm8s_uart1$UART1_GetITStatus$574-Sstm8s_uart1$UART1_GetITStatus$571
      002CCA 03                    3235 	.db	3
      002CCB 09                    3236 	.sleb128	9
      002CCC 01                    3237 	.db	1
      002CCD 09                    3238 	.db	9
      002CCE 00 4A                 3239 	.dw	Sstm8s_uart1$UART1_GetITStatus$588-Sstm8s_uart1$UART1_GetITStatus$574
      002CD0 03                    3240 	.db	3
      002CD1 03                    3241 	.sleb128	3
      002CD2 01                    3242 	.db	1
      002CD3 09                    3243 	.db	9
      002CD4 00 13                 3244 	.dw	Sstm8s_uart1$UART1_GetITStatus$591-Sstm8s_uart1$UART1_GetITStatus$588
      002CD6 03                    3245 	.db	3
      002CD7 02                    3246 	.sleb128	2
      002CD8 01                    3247 	.db	1
      002CD9 09                    3248 	.db	9
      002CDA 00 04                 3249 	.dw	Sstm8s_uart1$UART1_GetITStatus$592-Sstm8s_uart1$UART1_GetITStatus$591
      002CDC 03                    3250 	.db	3
      002CDD 02                    3251 	.sleb128	2
      002CDE 01                    3252 	.db	1
      002CDF 09                    3253 	.db	9
      002CE0 00 0E                 3254 	.dw	Sstm8s_uart1$UART1_GetITStatus$595-Sstm8s_uart1$UART1_GetITStatus$592
      002CE2 03                    3255 	.db	3
      002CE3 04                    3256 	.sleb128	4
      002CE4 01                    3257 	.db	1
      002CE5 09                    3258 	.db	9
      002CE6 00 04                 3259 	.dw	Sstm8s_uart1$UART1_GetITStatus$597-Sstm8s_uart1$UART1_GetITStatus$595
      002CE8 03                    3260 	.db	3
      002CE9 03                    3261 	.sleb128	3
      002CEA 01                    3262 	.db	1
      002CEB 09                    3263 	.db	9
      002CEC 00 06                 3264 	.dw	Sstm8s_uart1$UART1_GetITStatus$598-Sstm8s_uart1$UART1_GetITStatus$597
      002CEE 03                    3265 	.db	3
      002CEF 03                    3266 	.sleb128	3
      002CF0 01                    3267 	.db	1
      002CF1 09                    3268 	.db	9
      002CF2 00 0B                 3269 	.dw	Sstm8s_uart1$UART1_GetITStatus$600-Sstm8s_uart1$UART1_GetITStatus$598
      002CF4 03                    3270 	.db	3
      002CF5 03                    3271 	.sleb128	3
      002CF6 01                    3272 	.db	1
      002CF7 09                    3273 	.db	9
      002CF8 00 05                 3274 	.dw	Sstm8s_uart1$UART1_GetITStatus$603-Sstm8s_uart1$UART1_GetITStatus$600
      002CFA 03                    3275 	.db	3
      002CFB 05                    3276 	.sleb128	5
      002CFC 01                    3277 	.db	1
      002CFD 09                    3278 	.db	9
      002CFE 00 04                 3279 	.dw	Sstm8s_uart1$UART1_GetITStatus$605-Sstm8s_uart1$UART1_GetITStatus$603
      002D00 03                    3280 	.db	3
      002D01 04                    3281 	.sleb128	4
      002D02 01                    3282 	.db	1
      002D03 09                    3283 	.db	9
      002D04 00 04                 3284 	.dw	Sstm8s_uart1$UART1_GetITStatus$607-Sstm8s_uart1$UART1_GetITStatus$605
      002D06 03                    3285 	.db	3
      002D07 03                    3286 	.sleb128	3
      002D08 01                    3287 	.db	1
      002D09 09                    3288 	.db	9
      002D0A 00 06                 3289 	.dw	Sstm8s_uart1$UART1_GetITStatus$608-Sstm8s_uart1$UART1_GetITStatus$607
      002D0C 03                    3290 	.db	3
      002D0D 02                    3291 	.sleb128	2
      002D0E 01                    3292 	.db	1
      002D0F 09                    3293 	.db	9
      002D10 00 0B                 3294 	.dw	Sstm8s_uart1$UART1_GetITStatus$610-Sstm8s_uart1$UART1_GetITStatus$608
      002D12 03                    3295 	.db	3
      002D13 03                    3296 	.sleb128	3
      002D14 01                    3297 	.db	1
      002D15 09                    3298 	.db	9
      002D16 00 04                 3299 	.dw	Sstm8s_uart1$UART1_GetITStatus$613-Sstm8s_uart1$UART1_GetITStatus$610
      002D18 03                    3300 	.db	3
      002D19 05                    3301 	.sleb128	5
      002D1A 01                    3302 	.db	1
      002D1B 09                    3303 	.db	9
      002D1C 00 03                 3304 	.dw	Sstm8s_uart1$UART1_GetITStatus$616-Sstm8s_uart1$UART1_GetITStatus$613
      002D1E 03                    3305 	.db	3
      002D1F 06                    3306 	.sleb128	6
      002D20 01                    3307 	.db	1
      002D21 09                    3308 	.db	9
      002D22 00 06                 3309 	.dw	Sstm8s_uart1$UART1_GetITStatus$617-Sstm8s_uart1$UART1_GetITStatus$616
      002D24 03                    3310 	.db	3
      002D25 02                    3311 	.sleb128	2
      002D26 01                    3312 	.db	1
      002D27 09                    3313 	.db	9
      002D28 00 0B                 3314 	.dw	Sstm8s_uart1$UART1_GetITStatus$619-Sstm8s_uart1$UART1_GetITStatus$617
      002D2A 03                    3315 	.db	3
      002D2B 03                    3316 	.sleb128	3
      002D2C 01                    3317 	.db	1
      002D2D 09                    3318 	.db	9
      002D2E 00 04                 3319 	.dw	Sstm8s_uart1$UART1_GetITStatus$622-Sstm8s_uart1$UART1_GetITStatus$619
      002D30 03                    3320 	.db	3
      002D31 05                    3321 	.sleb128	5
      002D32 01                    3322 	.db	1
      002D33 09                    3323 	.db	9
      002D34 00 01                 3324 	.dw	Sstm8s_uart1$UART1_GetITStatus$624-Sstm8s_uart1$UART1_GetITStatus$622
      002D36 03                    3325 	.db	3
      002D37 05                    3326 	.sleb128	5
      002D38 01                    3327 	.db	1
      002D39 09                    3328 	.db	9
      002D3A 00 00                 3329 	.dw	Sstm8s_uart1$UART1_GetITStatus$625-Sstm8s_uart1$UART1_GetITStatus$624
      002D3C 03                    3330 	.db	3
      002D3D 01                    3331 	.sleb128	1
      002D3E 01                    3332 	.db	1
      002D3F 09                    3333 	.db	9
      002D40 00 03                 3334 	.dw	1+Sstm8s_uart1$UART1_GetITStatus$627-Sstm8s_uart1$UART1_GetITStatus$625
      002D42 00                    3335 	.db	0
      002D43 01                    3336 	.uleb128	1
      002D44 01                    3337 	.db	1
      002D45 00                    3338 	.db	0
      002D46 05                    3339 	.uleb128	5
      002D47 02                    3340 	.db	2
      002D48 00 00 AA B3           3341 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$629)
      002D4C 03                    3342 	.db	3
      002D4D 86 06                 3343 	.sleb128	774
      002D4F 01                    3344 	.db	1
      002D50 09                    3345 	.db	9
      002D51 00 00                 3346 	.dw	Sstm8s_uart1$UART1_ClearITPendingBit$631-Sstm8s_uart1$UART1_ClearITPendingBit$629
      002D53 03                    3347 	.db	3
      002D54 02                    3348 	.sleb128	2
      002D55 01                    3349 	.db	1
      002D56 09                    3350 	.db	9
      002D57 00 24                 3351 	.dw	Sstm8s_uart1$UART1_ClearITPendingBit$642-Sstm8s_uart1$UART1_ClearITPendingBit$631
      002D59 03                    3352 	.db	3
      002D5A 03                    3353 	.sleb128	3
      002D5B 01                    3354 	.db	1
      002D5C 09                    3355 	.db	9
      002D5D 00 03                 3356 	.dw	Sstm8s_uart1$UART1_ClearITPendingBit$644-Sstm8s_uart1$UART1_ClearITPendingBit$642
      002D5F 03                    3357 	.db	3
      002D60 02                    3358 	.sleb128	2
      002D61 01                    3359 	.db	1
      002D62 09                    3360 	.db	9
      002D63 00 06                 3361 	.dw	Sstm8s_uart1$UART1_ClearITPendingBit$647-Sstm8s_uart1$UART1_ClearITPendingBit$644
      002D65 03                    3362 	.db	3
      002D66 05                    3363 	.sleb128	5
      002D67 01                    3364 	.db	1
      002D68 09                    3365 	.db	9
      002D69 00 04                 3366 	.dw	Sstm8s_uart1$UART1_ClearITPendingBit$649-Sstm8s_uart1$UART1_ClearITPendingBit$647
      002D6B 03                    3367 	.db	3
      002D6C 02                    3368 	.sleb128	2
      002D6D 01                    3369 	.db	1
      002D6E 09                    3370 	.db	9
      002D6F 00 01                 3371 	.dw	1+Sstm8s_uart1$UART1_ClearITPendingBit$650-Sstm8s_uart1$UART1_ClearITPendingBit$649
      002D71 00                    3372 	.db	0
      002D72 01                    3373 	.uleb128	1
      002D73 01                    3374 	.db	1
      002D74                       3375 Ldebug_line_end:
                                   3376 
                                   3377 	.area .debug_loc (NOLOAD)
      0050B0                       3378 Ldebug_loc_start:
      0050B0 00 00 AA D7           3379 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$641)
      0050B4 00 00 AA E5           3380 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$651)
      0050B8 00 02                 3381 	.dw	2
      0050BA 78                    3382 	.db	120
      0050BB 01                    3383 	.sleb128	1
      0050BC 00 00 AA D6           3384 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$640)
      0050C0 00 00 AA D7           3385 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$641)
      0050C4 00 02                 3386 	.dw	2
      0050C6 78                    3387 	.db	120
      0050C7 02                    3388 	.sleb128	2
      0050C8 00 00 AA D1           3389 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$639)
      0050CC 00 00 AA D6           3390 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$640)
      0050D0 00 02                 3391 	.dw	2
      0050D2 78                    3392 	.db	120
      0050D3 08                    3393 	.sleb128	8
      0050D4 00 00 AA CF           3394 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$638)
      0050D8 00 00 AA D1           3395 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$639)
      0050DC 00 02                 3396 	.dw	2
      0050DE 78                    3397 	.db	120
      0050DF 07                    3398 	.sleb128	7
      0050E0 00 00 AA CD           3399 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$637)
      0050E4 00 00 AA CF           3400 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$638)
      0050E8 00 02                 3401 	.dw	2
      0050EA 78                    3402 	.db	120
      0050EB 06                    3403 	.sleb128	6
      0050EC 00 00 AA CB           3404 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$636)
      0050F0 00 00 AA CD           3405 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$637)
      0050F4 00 02                 3406 	.dw	2
      0050F6 78                    3407 	.db	120
      0050F7 04                    3408 	.sleb128	4
      0050F8 00 00 AA C9           3409 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$635)
      0050FC 00 00 AA CB           3410 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$636)
      005100 00 02                 3411 	.dw	2
      005102 78                    3412 	.db	120
      005103 03                    3413 	.sleb128	3
      005104 00 00 AA C7           3414 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$634)
      005108 00 00 AA C9           3415 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$635)
      00510C 00 02                 3416 	.dw	2
      00510E 78                    3417 	.db	120
      00510F 02                    3418 	.sleb128	2
      005110 00 00 AA C6           3419 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$633)
      005114 00 00 AA C7           3420 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$634)
      005118 00 02                 3421 	.dw	2
      00511A 78                    3422 	.db	120
      00511B 01                    3423 	.sleb128	1
      00511C 00 00 AA BE           3424 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$632)
      005120 00 00 AA C6           3425 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$633)
      005124 00 02                 3426 	.dw	2
      005126 78                    3427 	.db	120
      005127 01                    3428 	.sleb128	1
      005128 00 00 AA B3           3429 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$630)
      00512C 00 00 AA BE           3430 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$632)
      005130 00 02                 3431 	.dw	2
      005132 78                    3432 	.db	120
      005133 01                    3433 	.sleb128	1
      005134 00 00 00 00           3434 	.dw	0,0
      005138 00 00 00 00           3435 	.dw	0,0
      00513C 00 00 AA B2           3436 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$626)
      005140 00 00 AA B3           3437 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$628)
      005144 00 02                 3438 	.dw	2
      005146 78                    3439 	.db	120
      005147 01                    3440 	.sleb128	1
      005148 00 00 AA 58           3441 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$594)
      00514C 00 00 AA B2           3442 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$626)
      005150 00 02                 3443 	.dw	2
      005152 78                    3444 	.db	120
      005153 05                    3445 	.sleb128	5
      005154 00 00 AA 53           3446 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$593)
      005158 00 00 AA 58           3447 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$594)
      00515C 00 02                 3448 	.dw	2
      00515E 78                    3449 	.db	120
      00515F 06                    3450 	.sleb128	6
      005160 00 00 AA 46           3451 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$590)
      005164 00 00 AA 53           3452 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$593)
      005168 00 02                 3453 	.dw	2
      00516A 78                    3454 	.db	120
      00516B 05                    3455 	.sleb128	5
      00516C 00 00 AA 41           3456 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$589)
      005170 00 00 AA 46           3457 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$590)
      005174 00 02                 3458 	.dw	2
      005176 78                    3459 	.db	120
      005177 06                    3460 	.sleb128	6
      005178 00 00 AA 3B           3461 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$587)
      00517C 00 00 AA 41           3462 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$589)
      005180 00 02                 3463 	.dw	2
      005182 78                    3464 	.db	120
      005183 05                    3465 	.sleb128	5
      005184 00 00 AA 36           3466 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$586)
      005188 00 00 AA 3B           3467 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$587)
      00518C 00 02                 3468 	.dw	2
      00518E 78                    3469 	.db	120
      00518F 0B                    3470 	.sleb128	11
      005190 00 00 AA 34           3471 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$585)
      005194 00 00 AA 36           3472 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$586)
      005198 00 02                 3473 	.dw	2
      00519A 78                    3474 	.db	120
      00519B 0A                    3475 	.sleb128	10
      00519C 00 00 AA 32           3476 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$584)
      0051A0 00 00 AA 34           3477 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$585)
      0051A4 00 02                 3478 	.dw	2
      0051A6 78                    3479 	.db	120
      0051A7 09                    3480 	.sleb128	9
      0051A8 00 00 AA 30           3481 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$583)
      0051AC 00 00 AA 32           3482 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$584)
      0051B0 00 02                 3483 	.dw	2
      0051B2 78                    3484 	.db	120
      0051B3 07                    3485 	.sleb128	7
      0051B4 00 00 AA 2E           3486 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$582)
      0051B8 00 00 AA 30           3487 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$583)
      0051BC 00 02                 3488 	.dw	2
      0051BE 78                    3489 	.db	120
      0051BF 06                    3490 	.sleb128	6
      0051C0 00 00 AA 24           3491 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$581)
      0051C4 00 00 AA 2E           3492 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$582)
      0051C8 00 02                 3493 	.dw	2
      0051CA 78                    3494 	.db	120
      0051CB 05                    3495 	.sleb128	5
      0051CC 00 00 AA 1F           3496 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$580)
      0051D0 00 00 AA 24           3497 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$581)
      0051D4 00 02                 3498 	.dw	2
      0051D6 78                    3499 	.db	120
      0051D7 05                    3500 	.sleb128	5
      0051D8 00 00 AA 1A           3501 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$579)
      0051DC 00 00 AA 1F           3502 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$580)
      0051E0 00 02                 3503 	.dw	2
      0051E2 78                    3504 	.db	120
      0051E3 05                    3505 	.sleb128	5
      0051E4 00 00 AA 15           3506 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$578)
      0051E8 00 00 AA 1A           3507 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$579)
      0051EC 00 02                 3508 	.dw	2
      0051EE 78                    3509 	.db	120
      0051EF 05                    3510 	.sleb128	5
      0051F0 00 00 AA 10           3511 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$577)
      0051F4 00 00 AA 15           3512 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$578)
      0051F8 00 02                 3513 	.dw	2
      0051FA 78                    3514 	.db	120
      0051FB 05                    3515 	.sleb128	5
      0051FC 00 00 AA 0B           3516 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$576)
      005200 00 00 AA 10           3517 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$577)
      005204 00 02                 3518 	.dw	2
      005206 78                    3519 	.db	120
      005207 05                    3520 	.sleb128	5
      005208 00 00 A9 FF           3521 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$575)
      00520C 00 00 AA 0B           3522 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$576)
      005210 00 02                 3523 	.dw	2
      005212 78                    3524 	.db	120
      005213 05                    3525 	.sleb128	5
      005214 00 00 A9 F1           3526 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$573)
      005218 00 00 A9 FF           3527 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$575)
      00521C 00 02                 3528 	.dw	2
      00521E 78                    3529 	.db	120
      00521F 05                    3530 	.sleb128	5
      005220 00 00 A9 EF           3531 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$572)
      005224 00 00 A9 F1           3532 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$573)
      005228 00 02                 3533 	.dw	2
      00522A 78                    3534 	.db	120
      00522B 01                    3535 	.sleb128	1
      00522C 00 00 00 00           3536 	.dw	0,0
      005230 00 00 00 00           3537 	.dw	0,0
      005234 00 00 A9 E1           3538 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$560)
      005238 00 00 A9 EF           3539 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$570)
      00523C 00 02                 3540 	.dw	2
      00523E 78                    3541 	.db	120
      00523F 01                    3542 	.sleb128	1
      005240 00 00 A9 E0           3543 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$559)
      005244 00 00 A9 E1           3544 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$560)
      005248 00 02                 3545 	.dw	2
      00524A 78                    3546 	.db	120
      00524B 02                    3547 	.sleb128	2
      00524C 00 00 A9 DB           3548 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$558)
      005250 00 00 A9 E0           3549 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$559)
      005254 00 02                 3550 	.dw	2
      005256 78                    3551 	.db	120
      005257 08                    3552 	.sleb128	8
      005258 00 00 A9 D9           3553 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$557)
      00525C 00 00 A9 DB           3554 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$558)
      005260 00 02                 3555 	.dw	2
      005262 78                    3556 	.db	120
      005263 07                    3557 	.sleb128	7
      005264 00 00 A9 D7           3558 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$556)
      005268 00 00 A9 D9           3559 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$557)
      00526C 00 02                 3560 	.dw	2
      00526E 78                    3561 	.db	120
      00526F 06                    3562 	.sleb128	6
      005270 00 00 A9 D5           3563 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$555)
      005274 00 00 A9 D7           3564 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$556)
      005278 00 02                 3565 	.dw	2
      00527A 78                    3566 	.db	120
      00527B 04                    3567 	.sleb128	4
      00527C 00 00 A9 D3           3568 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$554)
      005280 00 00 A9 D5           3569 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$555)
      005284 00 02                 3570 	.dw	2
      005286 78                    3571 	.db	120
      005287 03                    3572 	.sleb128	3
      005288 00 00 A9 D1           3573 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$553)
      00528C 00 00 A9 D3           3574 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$554)
      005290 00 02                 3575 	.dw	2
      005292 78                    3576 	.db	120
      005293 02                    3577 	.sleb128	2
      005294 00 00 A9 D0           3578 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$552)
      005298 00 00 A9 D1           3579 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$553)
      00529C 00 02                 3580 	.dw	2
      00529E 78                    3581 	.db	120
      00529F 01                    3582 	.sleb128	1
      0052A0 00 00 A9 C8           3583 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$551)
      0052A4 00 00 A9 D0           3584 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$552)
      0052A8 00 02                 3585 	.dw	2
      0052AA 78                    3586 	.db	120
      0052AB 01                    3587 	.sleb128	1
      0052AC 00 00 A9 BD           3588 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$549)
      0052B0 00 00 A9 C8           3589 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$551)
      0052B4 00 02                 3590 	.dw	2
      0052B6 78                    3591 	.db	120
      0052B7 01                    3592 	.sleb128	1
      0052B8 00 00 00 00           3593 	.dw	0,0
      0052BC 00 00 00 00           3594 	.dw	0,0
      0052C0 00 00 A9 BC           3595 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$545)
      0052C4 00 00 A9 BD           3596 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$547)
      0052C8 00 02                 3597 	.dw	2
      0052CA 78                    3598 	.db	120
      0052CB 01                    3599 	.sleb128	1
      0052CC 00 00 A9 8B           3600 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$516)
      0052D0 00 00 A9 BC           3601 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$545)
      0052D4 00 02                 3602 	.dw	2
      0052D6 78                    3603 	.db	120
      0052D7 03                    3604 	.sleb128	3
      0052D8 00 00 A9 86           3605 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$515)
      0052DC 00 00 A9 8B           3606 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$516)
      0052E0 00 02                 3607 	.dw	2
      0052E2 78                    3608 	.db	120
      0052E3 04                    3609 	.sleb128	4
      0052E4 00 00 A9 85           3610 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$513)
      0052E8 00 00 A9 86           3611 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$515)
      0052EC 00 02                 3612 	.dw	2
      0052EE 78                    3613 	.db	120
      0052EF 03                    3614 	.sleb128	3
      0052F0 00 00 A9 84           3615 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$512)
      0052F4 00 00 A9 85           3616 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$513)
      0052F8 00 02                 3617 	.dw	2
      0052FA 78                    3618 	.db	120
      0052FB 04                    3619 	.sleb128	4
      0052FC 00 00 A9 7F           3620 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$511)
      005300 00 00 A9 84           3621 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$512)
      005304 00 02                 3622 	.dw	2
      005306 78                    3623 	.db	120
      005307 0A                    3624 	.sleb128	10
      005308 00 00 A9 7D           3625 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$510)
      00530C 00 00 A9 7F           3626 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$511)
      005310 00 02                 3627 	.dw	2
      005312 78                    3628 	.db	120
      005313 09                    3629 	.sleb128	9
      005314 00 00 A9 7B           3630 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$509)
      005318 00 00 A9 7D           3631 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$510)
      00531C 00 02                 3632 	.dw	2
      00531E 78                    3633 	.db	120
      00531F 08                    3634 	.sleb128	8
      005320 00 00 A9 79           3635 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$508)
      005324 00 00 A9 7B           3636 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$509)
      005328 00 02                 3637 	.dw	2
      00532A 78                    3638 	.db	120
      00532B 06                    3639 	.sleb128	6
      00532C 00 00 A9 77           3640 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$507)
      005330 00 00 A9 79           3641 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$508)
      005334 00 02                 3642 	.dw	2
      005336 78                    3643 	.db	120
      005337 05                    3644 	.sleb128	5
      005338 00 00 A9 75           3645 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$506)
      00533C 00 00 A9 77           3646 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$507)
      005340 00 02                 3647 	.dw	2
      005342 78                    3648 	.db	120
      005343 04                    3649 	.sleb128	4
      005344 00 00 A9 6D           3650 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$505)
      005348 00 00 A9 75           3651 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$506)
      00534C 00 02                 3652 	.dw	2
      00534E 78                    3653 	.db	120
      00534F 03                    3654 	.sleb128	3
      005350 00 00 A9 6A           3655 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$504)
      005354 00 00 A9 6D           3656 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$505)
      005358 00 02                 3657 	.dw	2
      00535A 78                    3658 	.db	120
      00535B 03                    3659 	.sleb128	3
      00535C 00 00 A9 65           3660 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$503)
      005360 00 00 A9 6A           3661 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$504)
      005364 00 02                 3662 	.dw	2
      005366 78                    3663 	.db	120
      005367 03                    3664 	.sleb128	3
      005368 00 00 A9 60           3665 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$502)
      00536C 00 00 A9 65           3666 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$503)
      005370 00 02                 3667 	.dw	2
      005372 78                    3668 	.db	120
      005373 03                    3669 	.sleb128	3
      005374 00 00 A9 5B           3670 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$501)
      005378 00 00 A9 60           3671 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$502)
      00537C 00 02                 3672 	.dw	2
      00537E 78                    3673 	.db	120
      00537F 03                    3674 	.sleb128	3
      005380 00 00 A9 56           3675 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$500)
      005384 00 00 A9 5B           3676 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$501)
      005388 00 02                 3677 	.dw	2
      00538A 78                    3678 	.db	120
      00538B 03                    3679 	.sleb128	3
      00538C 00 00 A9 51           3680 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$499)
      005390 00 00 A9 56           3681 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$500)
      005394 00 02                 3682 	.dw	2
      005396 78                    3683 	.db	120
      005397 03                    3684 	.sleb128	3
      005398 00 00 A9 49           3685 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$498)
      00539C 00 00 A9 51           3686 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$499)
      0053A0 00 02                 3687 	.dw	2
      0053A2 78                    3688 	.db	120
      0053A3 03                    3689 	.sleb128	3
      0053A4 00 00 A9 41           3690 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$497)
      0053A8 00 00 A9 49           3691 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$498)
      0053AC 00 02                 3692 	.dw	2
      0053AE 78                    3693 	.db	120
      0053AF 03                    3694 	.sleb128	3
      0053B0 00 00 A9 38           3695 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$496)
      0053B4 00 00 A9 41           3696 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$497)
      0053B8 00 02                 3697 	.dw	2
      0053BA 78                    3698 	.db	120
      0053BB 03                    3699 	.sleb128	3
      0053BC 00 00 A9 2A           3700 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$494)
      0053C0 00 00 A9 38           3701 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$496)
      0053C4 00 02                 3702 	.dw	2
      0053C6 78                    3703 	.db	120
      0053C7 03                    3704 	.sleb128	3
      0053C8 00 00 A9 29           3705 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$493)
      0053CC 00 00 A9 2A           3706 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$494)
      0053D0 00 02                 3707 	.dw	2
      0053D2 78                    3708 	.db	120
      0053D3 01                    3709 	.sleb128	1
      0053D4 00 00 00 00           3710 	.dw	0,0
      0053D8 00 00 00 00           3711 	.dw	0,0
      0053DC 00 00 A9 22           3712 	.dw	0,(Sstm8s_uart1$UART1_SetPrescaler$487)
      0053E0 00 00 A9 29           3713 	.dw	0,(Sstm8s_uart1$UART1_SetPrescaler$491)
      0053E4 00 02                 3714 	.dw	2
      0053E6 78                    3715 	.db	120
      0053E7 01                    3716 	.sleb128	1
      0053E8 00 00 00 00           3717 	.dw	0,0
      0053EC 00 00 00 00           3718 	.dw	0,0
      0053F0 00 00 A9 1B           3719 	.dw	0,(Sstm8s_uart1$UART1_SetGuardTime$481)
      0053F4 00 00 A9 22           3720 	.dw	0,(Sstm8s_uart1$UART1_SetGuardTime$485)
      0053F8 00 02                 3721 	.dw	2
      0053FA 78                    3722 	.db	120
      0053FB 01                    3723 	.sleb128	1
      0053FC 00 00 00 00           3724 	.dw	0,0
      005400 00 00 00 00           3725 	.dw	0,0
      005404 00 00 A9 0A           3726 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$474)
      005408 00 00 A9 1B           3727 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$479)
      00540C 00 02                 3728 	.dw	2
      00540E 78                    3729 	.db	120
      00540F 01                    3730 	.sleb128	1
      005410 00 00 A9 05           3731 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$473)
      005414 00 00 A9 0A           3732 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$474)
      005418 00 02                 3733 	.dw	2
      00541A 78                    3734 	.db	120
      00541B 07                    3735 	.sleb128	7
      00541C 00 00 A9 03           3736 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$472)
      005420 00 00 A9 05           3737 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$473)
      005424 00 02                 3738 	.dw	2
      005426 78                    3739 	.db	120
      005427 06                    3740 	.sleb128	6
      005428 00 00 A9 01           3741 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$471)
      00542C 00 00 A9 03           3742 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$472)
      005430 00 02                 3743 	.dw	2
      005432 78                    3744 	.db	120
      005433 05                    3745 	.sleb128	5
      005434 00 00 A8 FF           3746 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$470)
      005438 00 00 A9 01           3747 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$471)
      00543C 00 02                 3748 	.dw	2
      00543E 78                    3749 	.db	120
      00543F 03                    3750 	.sleb128	3
      005440 00 00 A8 FD           3751 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$469)
      005444 00 00 A8 FF           3752 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$470)
      005448 00 02                 3753 	.dw	2
      00544A 78                    3754 	.db	120
      00544B 02                    3755 	.sleb128	2
      00544C 00 00 A8 F5           3756 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$467)
      005450 00 00 A8 FD           3757 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$469)
      005454 00 02                 3758 	.dw	2
      005456 78                    3759 	.db	120
      005457 01                    3760 	.sleb128	1
      005458 00 00 00 00           3761 	.dw	0,0
      00545C 00 00 00 00           3762 	.dw	0,0
      005460 00 00 A8 F0           3763 	.dw	0,(Sstm8s_uart1$UART1_SendBreak$461)
      005464 00 00 A8 F5           3764 	.dw	0,(Sstm8s_uart1$UART1_SendBreak$465)
      005468 00 02                 3765 	.dw	2
      00546A 78                    3766 	.db	120
      00546B 01                    3767 	.sleb128	1
      00546C 00 00 00 00           3768 	.dw	0,0
      005470 00 00 00 00           3769 	.dw	0,0
      005474 00 00 A8 EF           3770 	.dw	0,(Sstm8s_uart1$UART1_SendData9$457)
      005478 00 00 A8 F0           3771 	.dw	0,(Sstm8s_uart1$UART1_SendData9$459)
      00547C 00 02                 3772 	.dw	2
      00547E 78                    3773 	.db	120
      00547F 01                    3774 	.sleb128	1
      005480 00 00 A8 D4           3775 	.dw	0,(Sstm8s_uart1$UART1_SendData9$452)
      005484 00 00 A8 EF           3776 	.dw	0,(Sstm8s_uart1$UART1_SendData9$457)
      005488 00 02                 3777 	.dw	2
      00548A 78                    3778 	.db	120
      00548B 02                    3779 	.sleb128	2
      00548C 00 00 A8 D3           3780 	.dw	0,(Sstm8s_uart1$UART1_SendData9$451)
      005490 00 00 A8 D4           3781 	.dw	0,(Sstm8s_uart1$UART1_SendData9$452)
      005494 00 02                 3782 	.dw	2
      005496 78                    3783 	.db	120
      005497 01                    3784 	.sleb128	1
      005498 00 00 00 00           3785 	.dw	0,0
      00549C 00 00 00 00           3786 	.dw	0,0
      0054A0 00 00 A8 CC           3787 	.dw	0,(Sstm8s_uart1$UART1_SendData8$445)
      0054A4 00 00 A8 D3           3788 	.dw	0,(Sstm8s_uart1$UART1_SendData8$449)
      0054A8 00 02                 3789 	.dw	2
      0054AA 78                    3790 	.db	120
      0054AB 01                    3791 	.sleb128	1
      0054AC 00 00 00 00           3792 	.dw	0,0
      0054B0 00 00 00 00           3793 	.dw	0,0
      0054B4 00 00 A8 CB           3794 	.dw	0,(Sstm8s_uart1$UART1_ReceiveData9$441)
      0054B8 00 00 A8 CC           3795 	.dw	0,(Sstm8s_uart1$UART1_ReceiveData9$443)
      0054BC 00 02                 3796 	.dw	2
      0054BE 78                    3797 	.db	120
      0054BF 01                    3798 	.sleb128	1
      0054C0 00 00 A8 B2           3799 	.dw	0,(Sstm8s_uart1$UART1_ReceiveData9$437)
      0054C4 00 00 A8 CB           3800 	.dw	0,(Sstm8s_uart1$UART1_ReceiveData9$441)
      0054C8 00 02                 3801 	.dw	2
      0054CA 78                    3802 	.db	120
      0054CB 03                    3803 	.sleb128	3
      0054CC 00 00 A8 B1           3804 	.dw	0,(Sstm8s_uart1$UART1_ReceiveData9$436)
      0054D0 00 00 A8 B2           3805 	.dw	0,(Sstm8s_uart1$UART1_ReceiveData9$437)
      0054D4 00 02                 3806 	.dw	2
      0054D6 78                    3807 	.db	120
      0054D7 01                    3808 	.sleb128	1
      0054D8 00 00 00 00           3809 	.dw	0,0
      0054DC 00 00 00 00           3810 	.dw	0,0
      0054E0 00 00 A8 AD           3811 	.dw	0,(Sstm8s_uart1$UART1_ReceiveData8$430)
      0054E4 00 00 A8 B1           3812 	.dw	0,(Sstm8s_uart1$UART1_ReceiveData8$434)
      0054E8 00 02                 3813 	.dw	2
      0054EA 78                    3814 	.db	120
      0054EB 01                    3815 	.sleb128	1
      0054EC 00 00 00 00           3816 	.dw	0,0
      0054F0 00 00 00 00           3817 	.dw	0,0
      0054F4 00 00 A8 99           3818 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$417)
      0054F8 00 00 A8 AD           3819 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$428)
      0054FC 00 02                 3820 	.dw	2
      0054FE 78                    3821 	.db	120
      0054FF 01                    3822 	.sleb128	1
      005500 00 00 A8 94           3823 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$416)
      005504 00 00 A8 99           3824 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$417)
      005508 00 02                 3825 	.dw	2
      00550A 78                    3826 	.db	120
      00550B 07                    3827 	.sleb128	7
      00550C 00 00 A8 92           3828 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$415)
      005510 00 00 A8 94           3829 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$416)
      005514 00 02                 3830 	.dw	2
      005516 78                    3831 	.db	120
      005517 06                    3832 	.sleb128	6
      005518 00 00 A8 90           3833 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$414)
      00551C 00 00 A8 92           3834 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$415)
      005520 00 02                 3835 	.dw	2
      005522 78                    3836 	.db	120
      005523 05                    3837 	.sleb128	5
      005524 00 00 A8 8E           3838 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$413)
      005528 00 00 A8 90           3839 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$414)
      00552C 00 02                 3840 	.dw	2
      00552E 78                    3841 	.db	120
      00552F 03                    3842 	.sleb128	3
      005530 00 00 A8 8C           3843 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$412)
      005534 00 00 A8 8E           3844 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$413)
      005538 00 02                 3845 	.dw	2
      00553A 78                    3846 	.db	120
      00553B 02                    3847 	.sleb128	2
      00553C 00 00 A8 8A           3848 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$411)
      005540 00 00 A8 8C           3849 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$412)
      005544 00 02                 3850 	.dw	2
      005546 78                    3851 	.db	120
      005547 01                    3852 	.sleb128	1
      005548 00 00 A8 81           3853 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$409)
      00554C 00 00 A8 8A           3854 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$411)
      005550 00 02                 3855 	.dw	2
      005552 78                    3856 	.db	120
      005553 01                    3857 	.sleb128	1
      005554 00 00 00 00           3858 	.dw	0,0
      005558 00 00 00 00           3859 	.dw	0,0
      00555C 00 00 A8 74           3860 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$402)
      005560 00 00 A8 81           3861 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$407)
      005564 00 02                 3862 	.dw	2
      005566 78                    3863 	.db	120
      005567 01                    3864 	.sleb128	1
      005568 00 00 A8 6F           3865 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$401)
      00556C 00 00 A8 74           3866 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$402)
      005570 00 02                 3867 	.dw	2
      005572 78                    3868 	.db	120
      005573 07                    3869 	.sleb128	7
      005574 00 00 A8 6D           3870 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$400)
      005578 00 00 A8 6F           3871 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$401)
      00557C 00 02                 3872 	.dw	2
      00557E 78                    3873 	.db	120
      00557F 06                    3874 	.sleb128	6
      005580 00 00 A8 6B           3875 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$399)
      005584 00 00 A8 6D           3876 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$400)
      005588 00 02                 3877 	.dw	2
      00558A 78                    3878 	.db	120
      00558B 05                    3879 	.sleb128	5
      00558C 00 00 A8 69           3880 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$398)
      005590 00 00 A8 6B           3881 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$399)
      005594 00 02                 3882 	.dw	2
      005596 78                    3883 	.db	120
      005597 03                    3884 	.sleb128	3
      005598 00 00 A8 67           3885 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$397)
      00559C 00 00 A8 69           3886 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$398)
      0055A0 00 02                 3887 	.dw	2
      0055A2 78                    3888 	.db	120
      0055A3 02                    3889 	.sleb128	2
      0055A4 00 00 A8 65           3890 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$396)
      0055A8 00 00 A8 67           3891 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$397)
      0055AC 00 02                 3892 	.dw	2
      0055AE 78                    3893 	.db	120
      0055AF 01                    3894 	.sleb128	1
      0055B0 00 00 A8 5B           3895 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$394)
      0055B4 00 00 A8 65           3896 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$396)
      0055B8 00 02                 3897 	.dw	2
      0055BA 78                    3898 	.db	120
      0055BB 01                    3899 	.sleb128	1
      0055BC 00 00 00 00           3900 	.dw	0,0
      0055C0 00 00 00 00           3901 	.dw	0,0
      0055C4 00 00 A8 47           3902 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$381)
      0055C8 00 00 A8 5B           3903 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$392)
      0055CC 00 02                 3904 	.dw	2
      0055CE 78                    3905 	.db	120
      0055CF 01                    3906 	.sleb128	1
      0055D0 00 00 A8 42           3907 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$380)
      0055D4 00 00 A8 47           3908 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$381)
      0055D8 00 02                 3909 	.dw	2
      0055DA 78                    3910 	.db	120
      0055DB 07                    3911 	.sleb128	7
      0055DC 00 00 A8 40           3912 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$379)
      0055E0 00 00 A8 42           3913 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$380)
      0055E4 00 02                 3914 	.dw	2
      0055E6 78                    3915 	.db	120
      0055E7 06                    3916 	.sleb128	6
      0055E8 00 00 A8 3E           3917 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$378)
      0055EC 00 00 A8 40           3918 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$379)
      0055F0 00 02                 3919 	.dw	2
      0055F2 78                    3920 	.db	120
      0055F3 05                    3921 	.sleb128	5
      0055F4 00 00 A8 3C           3922 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$377)
      0055F8 00 00 A8 3E           3923 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$378)
      0055FC 00 02                 3924 	.dw	2
      0055FE 78                    3925 	.db	120
      0055FF 03                    3926 	.sleb128	3
      005600 00 00 A8 3A           3927 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$376)
      005604 00 00 A8 3C           3928 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$377)
      005608 00 02                 3929 	.dw	2
      00560A 78                    3930 	.db	120
      00560B 02                    3931 	.sleb128	2
      00560C 00 00 A8 38           3932 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$375)
      005610 00 00 A8 3A           3933 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$376)
      005614 00 02                 3934 	.dw	2
      005616 78                    3935 	.db	120
      005617 01                    3936 	.sleb128	1
      005618 00 00 A8 2F           3937 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$373)
      00561C 00 00 A8 38           3938 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$375)
      005620 00 02                 3939 	.dw	2
      005622 78                    3940 	.db	120
      005623 01                    3941 	.sleb128	1
      005624 00 00 00 00           3942 	.dw	0,0
      005628 00 00 00 00           3943 	.dw	0,0
      00562C 00 00 A8 1B           3944 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$360)
      005630 00 00 A8 2F           3945 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$371)
      005634 00 02                 3946 	.dw	2
      005636 78                    3947 	.db	120
      005637 01                    3948 	.sleb128	1
      005638 00 00 A8 16           3949 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$359)
      00563C 00 00 A8 1B           3950 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$360)
      005640 00 02                 3951 	.dw	2
      005642 78                    3952 	.db	120
      005643 07                    3953 	.sleb128	7
      005644 00 00 A8 14           3954 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$358)
      005648 00 00 A8 16           3955 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$359)
      00564C 00 02                 3956 	.dw	2
      00564E 78                    3957 	.db	120
      00564F 06                    3958 	.sleb128	6
      005650 00 00 A8 12           3959 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$357)
      005654 00 00 A8 14           3960 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$358)
      005658 00 02                 3961 	.dw	2
      00565A 78                    3962 	.db	120
      00565B 05                    3963 	.sleb128	5
      00565C 00 00 A8 10           3964 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$356)
      005660 00 00 A8 12           3965 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$357)
      005664 00 02                 3966 	.dw	2
      005666 78                    3967 	.db	120
      005667 03                    3968 	.sleb128	3
      005668 00 00 A8 0E           3969 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$355)
      00566C 00 00 A8 10           3970 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$356)
      005670 00 02                 3971 	.dw	2
      005672 78                    3972 	.db	120
      005673 02                    3973 	.sleb128	2
      005674 00 00 A8 0C           3974 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$354)
      005678 00 00 A8 0E           3975 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$355)
      00567C 00 02                 3976 	.dw	2
      00567E 78                    3977 	.db	120
      00567F 01                    3978 	.sleb128	1
      005680 00 00 A8 03           3979 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$352)
      005684 00 00 A8 0C           3980 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$354)
      005688 00 02                 3981 	.dw	2
      00568A 78                    3982 	.db	120
      00568B 01                    3983 	.sleb128	1
      00568C 00 00 00 00           3984 	.dw	0,0
      005690 00 00 00 00           3985 	.dw	0,0
      005694 00 00 A7 EF           3986 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$339)
      005698 00 00 A8 03           3987 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$350)
      00569C 00 02                 3988 	.dw	2
      00569E 78                    3989 	.db	120
      00569F 01                    3990 	.sleb128	1
      0056A0 00 00 A7 EA           3991 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$338)
      0056A4 00 00 A7 EF           3992 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$339)
      0056A8 00 02                 3993 	.dw	2
      0056AA 78                    3994 	.db	120
      0056AB 07                    3995 	.sleb128	7
      0056AC 00 00 A7 E8           3996 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$337)
      0056B0 00 00 A7 EA           3997 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$338)
      0056B4 00 02                 3998 	.dw	2
      0056B6 78                    3999 	.db	120
      0056B7 06                    4000 	.sleb128	6
      0056B8 00 00 A7 E6           4001 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$336)
      0056BC 00 00 A7 E8           4002 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$337)
      0056C0 00 02                 4003 	.dw	2
      0056C2 78                    4004 	.db	120
      0056C3 05                    4005 	.sleb128	5
      0056C4 00 00 A7 E4           4006 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$335)
      0056C8 00 00 A7 E6           4007 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$336)
      0056CC 00 02                 4008 	.dw	2
      0056CE 78                    4009 	.db	120
      0056CF 03                    4010 	.sleb128	3
      0056D0 00 00 A7 E2           4011 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$334)
      0056D4 00 00 A7 E4           4012 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$335)
      0056D8 00 02                 4013 	.dw	2
      0056DA 78                    4014 	.db	120
      0056DB 02                    4015 	.sleb128	2
      0056DC 00 00 A7 E0           4016 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$333)
      0056E0 00 00 A7 E2           4017 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$334)
      0056E4 00 02                 4018 	.dw	2
      0056E6 78                    4019 	.db	120
      0056E7 01                    4020 	.sleb128	1
      0056E8 00 00 A7 D7           4021 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$331)
      0056EC 00 00 A7 E0           4022 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$333)
      0056F0 00 02                 4023 	.dw	2
      0056F2 78                    4024 	.db	120
      0056F3 01                    4025 	.sleb128	1
      0056F4 00 00 00 00           4026 	.dw	0,0
      0056F8 00 00 00 00           4027 	.dw	0,0
      0056FC 00 00 A7 C3           4028 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$318)
      005700 00 00 A7 D7           4029 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$329)
      005704 00 02                 4030 	.dw	2
      005706 78                    4031 	.db	120
      005707 01                    4032 	.sleb128	1
      005708 00 00 A7 BE           4033 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$317)
      00570C 00 00 A7 C3           4034 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$318)
      005710 00 02                 4035 	.dw	2
      005712 78                    4036 	.db	120
      005713 07                    4037 	.sleb128	7
      005714 00 00 A7 BC           4038 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$316)
      005718 00 00 A7 BE           4039 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$317)
      00571C 00 02                 4040 	.dw	2
      00571E 78                    4041 	.db	120
      00571F 06                    4042 	.sleb128	6
      005720 00 00 A7 BA           4043 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$315)
      005724 00 00 A7 BC           4044 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$316)
      005728 00 02                 4045 	.dw	2
      00572A 78                    4046 	.db	120
      00572B 05                    4047 	.sleb128	5
      00572C 00 00 A7 B8           4048 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$314)
      005730 00 00 A7 BA           4049 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$315)
      005734 00 02                 4050 	.dw	2
      005736 78                    4051 	.db	120
      005737 03                    4052 	.sleb128	3
      005738 00 00 A7 B6           4053 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$313)
      00573C 00 00 A7 B8           4054 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$314)
      005740 00 02                 4055 	.dw	2
      005742 78                    4056 	.db	120
      005743 02                    4057 	.sleb128	2
      005744 00 00 A7 B4           4058 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$312)
      005748 00 00 A7 B6           4059 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$313)
      00574C 00 02                 4060 	.dw	2
      00574E 78                    4061 	.db	120
      00574F 01                    4062 	.sleb128	1
      005750 00 00 A7 AB           4063 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$310)
      005754 00 00 A7 B4           4064 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$312)
      005758 00 02                 4065 	.dw	2
      00575A 78                    4066 	.db	120
      00575B 01                    4067 	.sleb128	1
      00575C 00 00 00 00           4068 	.dw	0,0
      005760 00 00 00 00           4069 	.dw	0,0
      005764 00 00 A7 97           4070 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$297)
      005768 00 00 A7 AB           4071 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$308)
      00576C 00 02                 4072 	.dw	2
      00576E 78                    4073 	.db	120
      00576F 01                    4074 	.sleb128	1
      005770 00 00 A7 92           4075 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$296)
      005774 00 00 A7 97           4076 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$297)
      005778 00 02                 4077 	.dw	2
      00577A 78                    4078 	.db	120
      00577B 07                    4079 	.sleb128	7
      00577C 00 00 A7 90           4080 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$295)
      005780 00 00 A7 92           4081 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$296)
      005784 00 02                 4082 	.dw	2
      005786 78                    4083 	.db	120
      005787 06                    4084 	.sleb128	6
      005788 00 00 A7 8E           4085 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$294)
      00578C 00 00 A7 90           4086 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$295)
      005790 00 02                 4087 	.dw	2
      005792 78                    4088 	.db	120
      005793 05                    4089 	.sleb128	5
      005794 00 00 A7 8C           4090 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$293)
      005798 00 00 A7 8E           4091 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$294)
      00579C 00 02                 4092 	.dw	2
      00579E 78                    4093 	.db	120
      00579F 03                    4094 	.sleb128	3
      0057A0 00 00 A7 8A           4095 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$292)
      0057A4 00 00 A7 8C           4096 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$293)
      0057A8 00 02                 4097 	.dw	2
      0057AA 78                    4098 	.db	120
      0057AB 02                    4099 	.sleb128	2
      0057AC 00 00 A7 88           4100 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$291)
      0057B0 00 00 A7 8A           4101 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$292)
      0057B4 00 02                 4102 	.dw	2
      0057B6 78                    4103 	.db	120
      0057B7 01                    4104 	.sleb128	1
      0057B8 00 00 A7 7F           4105 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$289)
      0057BC 00 00 A7 88           4106 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$291)
      0057C0 00 02                 4107 	.dw	2
      0057C2 78                    4108 	.db	120
      0057C3 01                    4109 	.sleb128	1
      0057C4 00 00 00 00           4110 	.dw	0,0
      0057C8 00 00 00 00           4111 	.dw	0,0
      0057CC 00 00 A7 6B           4112 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$276)
      0057D0 00 00 A7 7F           4113 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$287)
      0057D4 00 02                 4114 	.dw	2
      0057D6 78                    4115 	.db	120
      0057D7 01                    4116 	.sleb128	1
      0057D8 00 00 A7 66           4117 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$275)
      0057DC 00 00 A7 6B           4118 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$276)
      0057E0 00 02                 4119 	.dw	2
      0057E2 78                    4120 	.db	120
      0057E3 07                    4121 	.sleb128	7
      0057E4 00 00 A7 64           4122 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$274)
      0057E8 00 00 A7 66           4123 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$275)
      0057EC 00 02                 4124 	.dw	2
      0057EE 78                    4125 	.db	120
      0057EF 06                    4126 	.sleb128	6
      0057F0 00 00 A7 62           4127 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$273)
      0057F4 00 00 A7 64           4128 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$274)
      0057F8 00 02                 4129 	.dw	2
      0057FA 78                    4130 	.db	120
      0057FB 05                    4131 	.sleb128	5
      0057FC 00 00 A7 60           4132 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$272)
      005800 00 00 A7 62           4133 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$273)
      005804 00 02                 4134 	.dw	2
      005806 78                    4135 	.db	120
      005807 03                    4136 	.sleb128	3
      005808 00 00 A7 5E           4137 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$271)
      00580C 00 00 A7 60           4138 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$272)
      005810 00 02                 4139 	.dw	2
      005812 78                    4140 	.db	120
      005813 02                    4141 	.sleb128	2
      005814 00 00 A7 58           4142 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$270)
      005818 00 00 A7 5E           4143 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$271)
      00581C 00 02                 4144 	.dw	2
      00581E 78                    4145 	.db	120
      00581F 01                    4146 	.sleb128	1
      005820 00 00 A7 53           4147 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$268)
      005824 00 00 A7 58           4148 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$270)
      005828 00 02                 4149 	.dw	2
      00582A 78                    4150 	.db	120
      00582B 01                    4151 	.sleb128	1
      00582C 00 00 00 00           4152 	.dw	0,0
      005830 00 00 00 00           4153 	.dw	0,0
      005834 00 00 A7 3F           4154 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$255)
      005838 00 00 A7 53           4155 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$266)
      00583C 00 02                 4156 	.dw	2
      00583E 78                    4157 	.db	120
      00583F 01                    4158 	.sleb128	1
      005840 00 00 A7 3A           4159 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$254)
      005844 00 00 A7 3F           4160 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$255)
      005848 00 02                 4161 	.dw	2
      00584A 78                    4162 	.db	120
      00584B 07                    4163 	.sleb128	7
      00584C 00 00 A7 38           4164 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$253)
      005850 00 00 A7 3A           4165 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$254)
      005854 00 02                 4166 	.dw	2
      005856 78                    4167 	.db	120
      005857 06                    4168 	.sleb128	6
      005858 00 00 A7 36           4169 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$252)
      00585C 00 00 A7 38           4170 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$253)
      005860 00 02                 4171 	.dw	2
      005862 78                    4172 	.db	120
      005863 05                    4173 	.sleb128	5
      005864 00 00 A7 34           4174 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$251)
      005868 00 00 A7 36           4175 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$252)
      00586C 00 02                 4176 	.dw	2
      00586E 78                    4177 	.db	120
      00586F 03                    4178 	.sleb128	3
      005870 00 00 A7 32           4179 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$250)
      005874 00 00 A7 34           4180 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$251)
      005878 00 02                 4181 	.dw	2
      00587A 78                    4182 	.db	120
      00587B 02                    4183 	.sleb128	2
      00587C 00 00 A7 30           4184 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$249)
      005880 00 00 A7 32           4185 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$250)
      005884 00 02                 4186 	.dw	2
      005886 78                    4187 	.db	120
      005887 01                    4188 	.sleb128	1
      005888 00 00 A7 27           4189 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$247)
      00588C 00 00 A7 30           4190 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$249)
      005890 00 02                 4191 	.dw	2
      005892 78                    4192 	.db	120
      005893 01                    4193 	.sleb128	1
      005894 00 00 00 00           4194 	.dw	0,0
      005898 00 00 00 00           4195 	.dw	0,0
      00589C 00 00 A7 26           4196 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$243)
      0058A0 00 00 A7 27           4197 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$245)
      0058A4 00 02                 4198 	.dw	2
      0058A6 78                    4199 	.db	120
      0058A7 01                    4200 	.sleb128	1
      0058A8 00 00 A7 02           4201 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$229)
      0058AC 00 00 A7 26           4202 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$243)
      0058B0 00 02                 4203 	.dw	2
      0058B2 78                    4204 	.db	120
      0058B3 03                    4205 	.sleb128	3
      0058B4 00 00 A6 FF           4206 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$228)
      0058B8 00 00 A7 02           4207 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$229)
      0058BC 00 02                 4208 	.dw	2
      0058BE 78                    4209 	.db	120
      0058BF 04                    4210 	.sleb128	4
      0058C0 00 00 A6 D3           4211 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$213)
      0058C4 00 00 A6 FF           4212 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$228)
      0058C8 00 02                 4213 	.dw	2
      0058CA 78                    4214 	.db	120
      0058CB 03                    4215 	.sleb128	3
      0058CC 00 00 A6 CB           4216 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$211)
      0058D0 00 00 A6 D3           4217 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$213)
      0058D4 00 02                 4218 	.dw	2
      0058D6 78                    4219 	.db	120
      0058D7 03                    4220 	.sleb128	3
      0058D8 00 00 A6 B8           4221 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$209)
      0058DC 00 00 A6 CB           4222 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$211)
      0058E0 00 02                 4223 	.dw	2
      0058E2 78                    4224 	.db	120
      0058E3 03                    4225 	.sleb128	3
      0058E4 00 00 A6 B3           4226 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$208)
      0058E8 00 00 A6 B8           4227 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$209)
      0058EC 00 02                 4228 	.dw	2
      0058EE 78                    4229 	.db	120
      0058EF 04                    4230 	.sleb128	4
      0058F0 00 00 A6 AE           4231 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$205)
      0058F4 00 00 A6 B3           4232 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$208)
      0058F8 00 02                 4233 	.dw	2
      0058FA 78                    4234 	.db	120
      0058FB 03                    4235 	.sleb128	3
      0058FC 00 00 A6 AD           4236 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$204)
      005900 00 00 A6 AE           4237 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$205)
      005904 00 02                 4238 	.dw	2
      005906 78                    4239 	.db	120
      005907 05                    4240 	.sleb128	5
      005908 00 00 A6 A8           4241 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$203)
      00590C 00 00 A6 AD           4242 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$204)
      005910 00 02                 4243 	.dw	2
      005912 78                    4244 	.db	120
      005913 0B                    4245 	.sleb128	11
      005914 00 00 A6 A6           4246 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$202)
      005918 00 00 A6 A8           4247 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$203)
      00591C 00 02                 4248 	.dw	2
      00591E 78                    4249 	.db	120
      00591F 0A                    4250 	.sleb128	10
      005920 00 00 A6 A4           4251 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$201)
      005924 00 00 A6 A6           4252 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$202)
      005928 00 02                 4253 	.dw	2
      00592A 78                    4254 	.db	120
      00592B 09                    4255 	.sleb128	9
      00592C 00 00 A6 A2           4256 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$200)
      005930 00 00 A6 A4           4257 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$201)
      005934 00 02                 4258 	.dw	2
      005936 78                    4259 	.db	120
      005937 08                    4260 	.sleb128	8
      005938 00 00 A6 A0           4261 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$199)
      00593C 00 00 A6 A2           4262 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$200)
      005940 00 02                 4263 	.dw	2
      005942 78                    4264 	.db	120
      005943 07                    4265 	.sleb128	7
      005944 00 00 A6 9E           4266 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$198)
      005948 00 00 A6 A0           4267 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$199)
      00594C 00 02                 4268 	.dw	2
      00594E 78                    4269 	.db	120
      00594F 06                    4270 	.sleb128	6
      005950 00 00 A6 9C           4271 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$197)
      005954 00 00 A6 9E           4272 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$198)
      005958 00 02                 4273 	.dw	2
      00595A 78                    4274 	.db	120
      00595B 05                    4275 	.sleb128	5
      00595C 00 00 A6 9B           4276 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$196)
      005960 00 00 A6 9C           4277 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$197)
      005964 00 02                 4278 	.dw	2
      005966 78                    4279 	.db	120
      005967 03                    4280 	.sleb128	3
      005968 00 00 A6 92           4281 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$194)
      00596C 00 00 A6 9B           4282 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$196)
      005970 00 02                 4283 	.dw	2
      005972 78                    4284 	.db	120
      005973 03                    4285 	.sleb128	3
      005974 00 00 A6 91           4286 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$193)
      005978 00 00 A6 92           4287 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$194)
      00597C 00 02                 4288 	.dw	2
      00597E 78                    4289 	.db	120
      00597F 05                    4290 	.sleb128	5
      005980 00 00 A6 8C           4291 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$192)
      005984 00 00 A6 91           4292 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$193)
      005988 00 02                 4293 	.dw	2
      00598A 78                    4294 	.db	120
      00598B 0B                    4295 	.sleb128	11
      00598C 00 00 A6 8A           4296 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$191)
      005990 00 00 A6 8C           4297 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$192)
      005994 00 02                 4298 	.dw	2
      005996 78                    4299 	.db	120
      005997 0A                    4300 	.sleb128	10
      005998 00 00 A6 88           4301 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$190)
      00599C 00 00 A6 8A           4302 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$191)
      0059A0 00 02                 4303 	.dw	2
      0059A2 78                    4304 	.db	120
      0059A3 09                    4305 	.sleb128	9
      0059A4 00 00 A6 86           4306 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$189)
      0059A8 00 00 A6 88           4307 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$190)
      0059AC 00 02                 4308 	.dw	2
      0059AE 78                    4309 	.db	120
      0059AF 08                    4310 	.sleb128	8
      0059B0 00 00 A6 84           4311 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$188)
      0059B4 00 00 A6 86           4312 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$189)
      0059B8 00 02                 4313 	.dw	2
      0059BA 78                    4314 	.db	120
      0059BB 07                    4315 	.sleb128	7
      0059BC 00 00 A6 82           4316 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$187)
      0059C0 00 00 A6 84           4317 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$188)
      0059C4 00 02                 4318 	.dw	2
      0059C6 78                    4319 	.db	120
      0059C7 06                    4320 	.sleb128	6
      0059C8 00 00 A6 80           4321 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$186)
      0059CC 00 00 A6 82           4322 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$187)
      0059D0 00 02                 4323 	.dw	2
      0059D2 78                    4324 	.db	120
      0059D3 05                    4325 	.sleb128	5
      0059D4 00 00 A6 7F           4326 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$185)
      0059D8 00 00 A6 80           4327 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$186)
      0059DC 00 02                 4328 	.dw	2
      0059DE 78                    4329 	.db	120
      0059DF 03                    4330 	.sleb128	3
      0059E0 00 00 A6 7A           4331 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$184)
      0059E4 00 00 A6 7F           4332 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$185)
      0059E8 00 02                 4333 	.dw	2
      0059EA 78                    4334 	.db	120
      0059EB 03                    4335 	.sleb128	3
      0059EC 00 00 A6 75           4336 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$183)
      0059F0 00 00 A6 7A           4337 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$184)
      0059F4 00 02                 4338 	.dw	2
      0059F6 78                    4339 	.db	120
      0059F7 03                    4340 	.sleb128	3
      0059F8 00 00 A6 70           4341 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$182)
      0059FC 00 00 A6 75           4342 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$183)
      005A00 00 02                 4343 	.dw	2
      005A02 78                    4344 	.db	120
      005A03 03                    4345 	.sleb128	3
      005A04 00 00 A6 6B           4346 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$181)
      005A08 00 00 A6 70           4347 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$182)
      005A0C 00 02                 4348 	.dw	2
      005A0E 78                    4349 	.db	120
      005A0F 03                    4350 	.sleb128	3
      005A10 00 00 A6 66           4351 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$180)
      005A14 00 00 A6 6B           4352 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$181)
      005A18 00 02                 4353 	.dw	2
      005A1A 78                    4354 	.db	120
      005A1B 03                    4355 	.sleb128	3
      005A1C 00 00 A6 5F           4356 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$178)
      005A20 00 00 A6 66           4357 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$180)
      005A24 00 02                 4358 	.dw	2
      005A26 78                    4359 	.db	120
      005A27 03                    4360 	.sleb128	3
      005A28 00 00 A6 5E           4361 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$177)
      005A2C 00 00 A6 5F           4362 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$178)
      005A30 00 02                 4363 	.dw	2
      005A32 78                    4364 	.db	120
      005A33 01                    4365 	.sleb128	1
      005A34 00 00 00 00           4366 	.dw	0,0
      005A38 00 00 00 00           4367 	.dw	0,0
      005A3C 00 00 A6 4A           4368 	.dw	0,(Sstm8s_uart1$UART1_Cmd$164)
      005A40 00 00 A6 5E           4369 	.dw	0,(Sstm8s_uart1$UART1_Cmd$175)
      005A44 00 02                 4370 	.dw	2
      005A46 78                    4371 	.db	120
      005A47 01                    4372 	.sleb128	1
      005A48 00 00 00 00           4373 	.dw	0,0
      005A4C 00 00 00 00           4374 	.dw	0,0
      005A50 00 00 A6 49           4375 	.dw	0,(Sstm8s_uart1$UART1_Init$160)
      005A54 00 00 A6 4A           4376 	.dw	0,(Sstm8s_uart1$UART1_Init$162)
      005A58 00 02                 4377 	.dw	2
      005A5A 78                    4378 	.db	120
      005A5B 01                    4379 	.sleb128	1
      005A5C 00 00 A6 42           4380 	.dw	0,(Sstm8s_uart1$UART1_Init$157)
      005A60 00 00 A6 49           4381 	.dw	0,(Sstm8s_uart1$UART1_Init$160)
      005A64 00 02                 4382 	.dw	2
      005A66 78                    4383 	.db	120
      005A67 0E                    4384 	.sleb128	14
      005A68 00 00 A6 3B           4385 	.dw	0,(Sstm8s_uart1$UART1_Init$156)
      005A6C 00 00 A6 42           4386 	.dw	0,(Sstm8s_uart1$UART1_Init$157)
      005A70 00 02                 4387 	.dw	2
      005A72 78                    4388 	.db	120
      005A73 0F                    4389 	.sleb128	15
      005A74 00 00 A6 1E           4390 	.dw	0,(Sstm8s_uart1$UART1_Init$142)
      005A78 00 00 A6 3B           4391 	.dw	0,(Sstm8s_uart1$UART1_Init$156)
      005A7C 00 02                 4392 	.dw	2
      005A7E 78                    4393 	.db	120
      005A7F 0E                    4394 	.sleb128	14
      005A80 00 00 A6 19           4395 	.dw	0,(Sstm8s_uart1$UART1_Init$141)
      005A84 00 00 A6 1E           4396 	.dw	0,(Sstm8s_uart1$UART1_Init$142)
      005A88 00 02                 4397 	.dw	2
      005A8A 78                    4398 	.db	120
      005A8B 0F                    4399 	.sleb128	15
      005A8C 00 00 A6 07           4400 	.dw	0,(Sstm8s_uart1$UART1_Init$132)
      005A90 00 00 A6 19           4401 	.dw	0,(Sstm8s_uart1$UART1_Init$141)
      005A94 00 02                 4402 	.dw	2
      005A96 78                    4403 	.db	120
      005A97 0E                    4404 	.sleb128	14
      005A98 00 00 A6 02           4405 	.dw	0,(Sstm8s_uart1$UART1_Init$131)
      005A9C 00 00 A6 07           4406 	.dw	0,(Sstm8s_uart1$UART1_Init$132)
      005AA0 00 02                 4407 	.dw	2
      005AA2 78                    4408 	.db	120
      005AA3 0F                    4409 	.sleb128	15
      005AA4 00 00 A5 BA           4410 	.dw	0,(Sstm8s_uart1$UART1_Init$123)
      005AA8 00 00 A6 02           4411 	.dw	0,(Sstm8s_uart1$UART1_Init$131)
      005AAC 00 02                 4412 	.dw	2
      005AAE 78                    4413 	.db	120
      005AAF 0E                    4414 	.sleb128	14
      005AB0 00 00 A5 B5           4415 	.dw	0,(Sstm8s_uart1$UART1_Init$122)
      005AB4 00 00 A5 BA           4416 	.dw	0,(Sstm8s_uart1$UART1_Init$123)
      005AB8 00 02                 4417 	.dw	2
      005ABA 78                    4418 	.db	120
      005ABB 16                    4419 	.sleb128	22
      005ABC 00 00 A5 B4           4420 	.dw	0,(Sstm8s_uart1$UART1_Init$121)
      005AC0 00 00 A5 B5           4421 	.dw	0,(Sstm8s_uart1$UART1_Init$122)
      005AC4 00 02                 4422 	.dw	2
      005AC6 78                    4423 	.db	120
      005AC7 14                    4424 	.sleb128	20
      005AC8 00 00 A5 B2           4425 	.dw	0,(Sstm8s_uart1$UART1_Init$120)
      005ACC 00 00 A5 B4           4426 	.dw	0,(Sstm8s_uart1$UART1_Init$121)
      005AD0 00 02                 4427 	.dw	2
      005AD2 78                    4428 	.db	120
      005AD3 12                    4429 	.sleb128	18
      005AD4 00 00 A5 B0           4430 	.dw	0,(Sstm8s_uart1$UART1_Init$119)
      005AD8 00 00 A5 B2           4431 	.dw	0,(Sstm8s_uart1$UART1_Init$120)
      005ADC 00 02                 4432 	.dw	2
      005ADE 78                    4433 	.db	120
      005ADF 11                    4434 	.sleb128	17
      005AE0 00 00 A5 AE           4435 	.dw	0,(Sstm8s_uart1$UART1_Init$118)
      005AE4 00 00 A5 B0           4436 	.dw	0,(Sstm8s_uart1$UART1_Init$119)
      005AE8 00 02                 4437 	.dw	2
      005AEA 78                    4438 	.db	120
      005AEB 10                    4439 	.sleb128	16
      005AEC 00 00 A5 AC           4440 	.dw	0,(Sstm8s_uart1$UART1_Init$117)
      005AF0 00 00 A5 AE           4441 	.dw	0,(Sstm8s_uart1$UART1_Init$118)
      005AF4 00 02                 4442 	.dw	2
      005AF6 78                    4443 	.db	120
      005AF7 0F                    4444 	.sleb128	15
      005AF8 00 00 A5 95           4445 	.dw	0,(Sstm8s_uart1$UART1_Init$116)
      005AFC 00 00 A5 AC           4446 	.dw	0,(Sstm8s_uart1$UART1_Init$117)
      005B00 00 02                 4447 	.dw	2
      005B02 78                    4448 	.db	120
      005B03 0E                    4449 	.sleb128	14
      005B04 00 00 A5 90           4450 	.dw	0,(Sstm8s_uart1$UART1_Init$115)
      005B08 00 00 A5 95           4451 	.dw	0,(Sstm8s_uart1$UART1_Init$116)
      005B0C 00 02                 4452 	.dw	2
      005B0E 78                    4453 	.db	120
      005B0F 0F                    4454 	.sleb128	15
      005B10 00 00 A5 8B           4455 	.dw	0,(Sstm8s_uart1$UART1_Init$114)
      005B14 00 00 A5 90           4456 	.dw	0,(Sstm8s_uart1$UART1_Init$115)
      005B18 00 02                 4457 	.dw	2
      005B1A 78                    4458 	.db	120
      005B1B 17                    4459 	.sleb128	23
      005B1C 00 00 A5 89           4460 	.dw	0,(Sstm8s_uart1$UART1_Init$113)
      005B20 00 00 A5 8B           4461 	.dw	0,(Sstm8s_uart1$UART1_Init$114)
      005B24 00 02                 4462 	.dw	2
      005B26 78                    4463 	.db	120
      005B27 16                    4464 	.sleb128	22
      005B28 00 00 A5 87           4465 	.dw	0,(Sstm8s_uart1$UART1_Init$112)
      005B2C 00 00 A5 89           4466 	.dw	0,(Sstm8s_uart1$UART1_Init$113)
      005B30 00 02                 4467 	.dw	2
      005B32 78                    4468 	.db	120
      005B33 14                    4469 	.sleb128	20
      005B34 00 00 A5 85           4470 	.dw	0,(Sstm8s_uart1$UART1_Init$111)
      005B38 00 00 A5 87           4471 	.dw	0,(Sstm8s_uart1$UART1_Init$112)
      005B3C 00 02                 4472 	.dw	2
      005B3E 78                    4473 	.db	120
      005B3F 13                    4474 	.sleb128	19
      005B40 00 00 A5 82           4475 	.dw	0,(Sstm8s_uart1$UART1_Init$110)
      005B44 00 00 A5 85           4476 	.dw	0,(Sstm8s_uart1$UART1_Init$111)
      005B48 00 02                 4477 	.dw	2
      005B4A 78                    4478 	.db	120
      005B4B 11                    4479 	.sleb128	17
      005B4C 00 00 A5 7F           4480 	.dw	0,(Sstm8s_uart1$UART1_Init$109)
      005B50 00 00 A5 82           4481 	.dw	0,(Sstm8s_uart1$UART1_Init$110)
      005B54 00 02                 4482 	.dw	2
      005B56 78                    4483 	.db	120
      005B57 0F                    4484 	.sleb128	15
      005B58 00 00 A5 7E           4485 	.dw	0,(Sstm8s_uart1$UART1_Init$108)
      005B5C 00 00 A5 7F           4486 	.dw	0,(Sstm8s_uart1$UART1_Init$109)
      005B60 00 02                 4487 	.dw	2
      005B62 78                    4488 	.db	120
      005B63 0E                    4489 	.sleb128	14
      005B64 00 00 A5 7A           4490 	.dw	0,(Sstm8s_uart1$UART1_Init$107)
      005B68 00 00 A5 7E           4491 	.dw	0,(Sstm8s_uart1$UART1_Init$108)
      005B6C 00 02                 4492 	.dw	2
      005B6E 78                    4493 	.db	120
      005B6F 0F                    4494 	.sleb128	15
      005B70 00 00 A5 6E           4495 	.dw	0,(Sstm8s_uart1$UART1_Init$105)
      005B74 00 00 A5 7A           4496 	.dw	0,(Sstm8s_uart1$UART1_Init$107)
      005B78 00 02                 4497 	.dw	2
      005B7A 78                    4498 	.db	120
      005B7B 0E                    4499 	.sleb128	14
      005B7C 00 00 A5 69           4500 	.dw	0,(Sstm8s_uart1$UART1_Init$104)
      005B80 00 00 A5 6E           4501 	.dw	0,(Sstm8s_uart1$UART1_Init$105)
      005B84 00 02                 4502 	.dw	2
      005B86 78                    4503 	.db	120
      005B87 16                    4504 	.sleb128	22
      005B88 00 00 A5 67           4505 	.dw	0,(Sstm8s_uart1$UART1_Init$103)
      005B8C 00 00 A5 69           4506 	.dw	0,(Sstm8s_uart1$UART1_Init$104)
      005B90 00 02                 4507 	.dw	2
      005B92 78                    4508 	.db	120
      005B93 14                    4509 	.sleb128	20
      005B94 00 00 A5 64           4510 	.dw	0,(Sstm8s_uart1$UART1_Init$102)
      005B98 00 00 A5 67           4511 	.dw	0,(Sstm8s_uart1$UART1_Init$103)
      005B9C 00 02                 4512 	.dw	2
      005B9E 78                    4513 	.db	120
      005B9F 12                    4514 	.sleb128	18
      005BA0 00 00 A5 61           4515 	.dw	0,(Sstm8s_uart1$UART1_Init$101)
      005BA4 00 00 A5 64           4516 	.dw	0,(Sstm8s_uart1$UART1_Init$102)
      005BA8 00 02                 4517 	.dw	2
      005BAA 78                    4518 	.db	120
      005BAB 10                    4519 	.sleb128	16
      005BAC 00 00 A5 5C           4520 	.dw	0,(Sstm8s_uart1$UART1_Init$100)
      005BB0 00 00 A5 61           4521 	.dw	0,(Sstm8s_uart1$UART1_Init$101)
      005BB4 00 02                 4522 	.dw	2
      005BB6 78                    4523 	.db	120
      005BB7 0E                    4524 	.sleb128	14
      005BB8 00 00 A5 57           4525 	.dw	0,(Sstm8s_uart1$UART1_Init$99)
      005BBC 00 00 A5 5C           4526 	.dw	0,(Sstm8s_uart1$UART1_Init$100)
      005BC0 00 02                 4527 	.dw	2
      005BC2 78                    4528 	.db	120
      005BC3 16                    4529 	.sleb128	22
      005BC4 00 00 A5 55           4530 	.dw	0,(Sstm8s_uart1$UART1_Init$98)
      005BC8 00 00 A5 57           4531 	.dw	0,(Sstm8s_uart1$UART1_Init$99)
      005BCC 00 02                 4532 	.dw	2
      005BCE 78                    4533 	.db	120
      005BCF 15                    4534 	.sleb128	21
      005BD0 00 00 A5 53           4535 	.dw	0,(Sstm8s_uart1$UART1_Init$97)
      005BD4 00 00 A5 55           4536 	.dw	0,(Sstm8s_uart1$UART1_Init$98)
      005BD8 00 02                 4537 	.dw	2
      005BDA 78                    4538 	.db	120
      005BDB 13                    4539 	.sleb128	19
      005BDC 00 00 A5 51           4540 	.dw	0,(Sstm8s_uart1$UART1_Init$96)
      005BE0 00 00 A5 53           4541 	.dw	0,(Sstm8s_uart1$UART1_Init$97)
      005BE4 00 02                 4542 	.dw	2
      005BE6 78                    4543 	.db	120
      005BE7 12                    4544 	.sleb128	18
      005BE8 00 00 A5 4F           4545 	.dw	0,(Sstm8s_uart1$UART1_Init$95)
      005BEC 00 00 A5 51           4546 	.dw	0,(Sstm8s_uart1$UART1_Init$96)
      005BF0 00 02                 4547 	.dw	2
      005BF2 78                    4548 	.db	120
      005BF3 10                    4549 	.sleb128	16
      005BF4 00 00 A5 47           4550 	.dw	0,(Sstm8s_uart1$UART1_Init$93)
      005BF8 00 00 A5 4F           4551 	.dw	0,(Sstm8s_uart1$UART1_Init$95)
      005BFC 00 02                 4552 	.dw	2
      005BFE 78                    4553 	.db	120
      005BFF 0E                    4554 	.sleb128	14
      005C00 00 00 A5 42           4555 	.dw	0,(Sstm8s_uart1$UART1_Init$92)
      005C04 00 00 A5 47           4556 	.dw	0,(Sstm8s_uart1$UART1_Init$93)
      005C08 00 02                 4557 	.dw	2
      005C0A 78                    4558 	.db	120
      005C0B 16                    4559 	.sleb128	22
      005C0C 00 00 A5 40           4560 	.dw	0,(Sstm8s_uart1$UART1_Init$91)
      005C10 00 00 A5 42           4561 	.dw	0,(Sstm8s_uart1$UART1_Init$92)
      005C14 00 02                 4562 	.dw	2
      005C16 78                    4563 	.db	120
      005C17 14                    4564 	.sleb128	20
      005C18 00 00 A5 3D           4565 	.dw	0,(Sstm8s_uart1$UART1_Init$90)
      005C1C 00 00 A5 40           4566 	.dw	0,(Sstm8s_uart1$UART1_Init$91)
      005C20 00 02                 4567 	.dw	2
      005C22 78                    4568 	.db	120
      005C23 12                    4569 	.sleb128	18
      005C24 00 00 A5 3A           4570 	.dw	0,(Sstm8s_uart1$UART1_Init$89)
      005C28 00 00 A5 3D           4571 	.dw	0,(Sstm8s_uart1$UART1_Init$90)
      005C2C 00 02                 4572 	.dw	2
      005C2E 78                    4573 	.db	120
      005C2F 10                    4574 	.sleb128	16
      005C30 00 00 A4 DF           4575 	.dw	0,(Sstm8s_uart1$UART1_Init$78)
      005C34 00 00 A5 3A           4576 	.dw	0,(Sstm8s_uart1$UART1_Init$89)
      005C38 00 02                 4577 	.dw	2
      005C3A 78                    4578 	.db	120
      005C3B 0E                    4579 	.sleb128	14
      005C3C 00 00 A4 DA           4580 	.dw	0,(Sstm8s_uart1$UART1_Init$77)
      005C40 00 00 A4 DF           4581 	.dw	0,(Sstm8s_uart1$UART1_Init$78)
      005C44 00 02                 4582 	.dw	2
      005C46 78                    4583 	.db	120
      005C47 14                    4584 	.sleb128	20
      005C48 00 00 A4 D8           4585 	.dw	0,(Sstm8s_uart1$UART1_Init$76)
      005C4C 00 00 A4 DA           4586 	.dw	0,(Sstm8s_uart1$UART1_Init$77)
      005C50 00 02                 4587 	.dw	2
      005C52 78                    4588 	.db	120
      005C53 13                    4589 	.sleb128	19
      005C54 00 00 A4 D6           4590 	.dw	0,(Sstm8s_uart1$UART1_Init$75)
      005C58 00 00 A4 D8           4591 	.dw	0,(Sstm8s_uart1$UART1_Init$76)
      005C5C 00 02                 4592 	.dw	2
      005C5E 78                    4593 	.db	120
      005C5F 12                    4594 	.sleb128	18
      005C60 00 00 A4 D4           4595 	.dw	0,(Sstm8s_uart1$UART1_Init$74)
      005C64 00 00 A4 D6           4596 	.dw	0,(Sstm8s_uart1$UART1_Init$75)
      005C68 00 02                 4597 	.dw	2
      005C6A 78                    4598 	.db	120
      005C6B 11                    4599 	.sleb128	17
      005C6C 00 00 A4 D2           4600 	.dw	0,(Sstm8s_uart1$UART1_Init$73)
      005C70 00 00 A4 D4           4601 	.dw	0,(Sstm8s_uart1$UART1_Init$74)
      005C74 00 02                 4602 	.dw	2
      005C76 78                    4603 	.db	120
      005C77 0F                    4604 	.sleb128	15
      005C78 00 00 A4 D0           4605 	.dw	0,(Sstm8s_uart1$UART1_Init$72)
      005C7C 00 00 A4 D2           4606 	.dw	0,(Sstm8s_uart1$UART1_Init$73)
      005C80 00 02                 4607 	.dw	2
      005C82 78                    4608 	.db	120
      005C83 0E                    4609 	.sleb128	14
      005C84 00 00 A4 C8           4610 	.dw	0,(Sstm8s_uart1$UART1_Init$71)
      005C88 00 00 A4 D0           4611 	.dw	0,(Sstm8s_uart1$UART1_Init$72)
      005C8C 00 02                 4612 	.dw	2
      005C8E 78                    4613 	.db	120
      005C8F 0E                    4614 	.sleb128	14
      005C90 00 00 A4 C0           4615 	.dw	0,(Sstm8s_uart1$UART1_Init$70)
      005C94 00 00 A4 C8           4616 	.dw	0,(Sstm8s_uart1$UART1_Init$71)
      005C98 00 02                 4617 	.dw	2
      005C9A 78                    4618 	.db	120
      005C9B 0E                    4619 	.sleb128	14
      005C9C 00 00 A4 B8           4620 	.dw	0,(Sstm8s_uart1$UART1_Init$69)
      005CA0 00 00 A4 C0           4621 	.dw	0,(Sstm8s_uart1$UART1_Init$70)
      005CA4 00 02                 4622 	.dw	2
      005CA6 78                    4623 	.db	120
      005CA7 0E                    4624 	.sleb128	14
      005CA8 00 00 A4 B0           4625 	.dw	0,(Sstm8s_uart1$UART1_Init$67)
      005CAC 00 00 A4 B8           4626 	.dw	0,(Sstm8s_uart1$UART1_Init$69)
      005CB0 00 02                 4627 	.dw	2
      005CB2 78                    4628 	.db	120
      005CB3 0E                    4629 	.sleb128	14
      005CB4 00 00 A4 AB           4630 	.dw	0,(Sstm8s_uart1$UART1_Init$66)
      005CB8 00 00 A4 B0           4631 	.dw	0,(Sstm8s_uart1$UART1_Init$67)
      005CBC 00 02                 4632 	.dw	2
      005CBE 78                    4633 	.db	120
      005CBF 14                    4634 	.sleb128	20
      005CC0 00 00 A4 A9           4635 	.dw	0,(Sstm8s_uart1$UART1_Init$65)
      005CC4 00 00 A4 AB           4636 	.dw	0,(Sstm8s_uart1$UART1_Init$66)
      005CC8 00 02                 4637 	.dw	2
      005CCA 78                    4638 	.db	120
      005CCB 13                    4639 	.sleb128	19
      005CCC 00 00 A4 A7           4640 	.dw	0,(Sstm8s_uart1$UART1_Init$64)
      005CD0 00 00 A4 A9           4641 	.dw	0,(Sstm8s_uart1$UART1_Init$65)
      005CD4 00 02                 4642 	.dw	2
      005CD6 78                    4643 	.db	120
      005CD7 12                    4644 	.sleb128	18
      005CD8 00 00 A4 A5           4645 	.dw	0,(Sstm8s_uart1$UART1_Init$63)
      005CDC 00 00 A4 A7           4646 	.dw	0,(Sstm8s_uart1$UART1_Init$64)
      005CE0 00 02                 4647 	.dw	2
      005CE2 78                    4648 	.db	120
      005CE3 11                    4649 	.sleb128	17
      005CE4 00 00 A4 A3           4650 	.dw	0,(Sstm8s_uart1$UART1_Init$62)
      005CE8 00 00 A4 A5           4651 	.dw	0,(Sstm8s_uart1$UART1_Init$63)
      005CEC 00 02                 4652 	.dw	2
      005CEE 78                    4653 	.db	120
      005CEF 0F                    4654 	.sleb128	15
      005CF0 00 00 A4 A1           4655 	.dw	0,(Sstm8s_uart1$UART1_Init$61)
      005CF4 00 00 A4 A3           4656 	.dw	0,(Sstm8s_uart1$UART1_Init$62)
      005CF8 00 02                 4657 	.dw	2
      005CFA 78                    4658 	.db	120
      005CFB 0E                    4659 	.sleb128	14
      005CFC 00 00 A4 9B           4660 	.dw	0,(Sstm8s_uart1$UART1_Init$60)
      005D00 00 00 A4 A1           4661 	.dw	0,(Sstm8s_uart1$UART1_Init$61)
      005D04 00 02                 4662 	.dw	2
      005D06 78                    4663 	.db	120
      005D07 0E                    4664 	.sleb128	14
      005D08 00 00 A4 95           4665 	.dw	0,(Sstm8s_uart1$UART1_Init$59)
      005D0C 00 00 A4 9B           4666 	.dw	0,(Sstm8s_uart1$UART1_Init$60)
      005D10 00 02                 4667 	.dw	2
      005D12 78                    4668 	.db	120
      005D13 0E                    4669 	.sleb128	14
      005D14 00 00 A4 89           4670 	.dw	0,(Sstm8s_uart1$UART1_Init$58)
      005D18 00 00 A4 95           4671 	.dw	0,(Sstm8s_uart1$UART1_Init$59)
      005D1C 00 02                 4672 	.dw	2
      005D1E 78                    4673 	.db	120
      005D1F 0E                    4674 	.sleb128	14
      005D20 00 00 A4 80           4675 	.dw	0,(Sstm8s_uart1$UART1_Init$57)
      005D24 00 00 A4 89           4676 	.dw	0,(Sstm8s_uart1$UART1_Init$58)
      005D28 00 02                 4677 	.dw	2
      005D2A 78                    4678 	.db	120
      005D2B 0E                    4679 	.sleb128	14
      005D2C 00 00 A4 7A           4680 	.dw	0,(Sstm8s_uart1$UART1_Init$56)
      005D30 00 00 A4 80           4681 	.dw	0,(Sstm8s_uart1$UART1_Init$57)
      005D34 00 02                 4682 	.dw	2
      005D36 78                    4683 	.db	120
      005D37 0E                    4684 	.sleb128	14
      005D38 00 00 A4 74           4685 	.dw	0,(Sstm8s_uart1$UART1_Init$55)
      005D3C 00 00 A4 7A           4686 	.dw	0,(Sstm8s_uart1$UART1_Init$56)
      005D40 00 02                 4687 	.dw	2
      005D42 78                    4688 	.db	120
      005D43 0E                    4689 	.sleb128	14
      005D44 00 00 A4 6B           4690 	.dw	0,(Sstm8s_uart1$UART1_Init$54)
      005D48 00 00 A4 74           4691 	.dw	0,(Sstm8s_uart1$UART1_Init$55)
      005D4C 00 02                 4692 	.dw	2
      005D4E 78                    4693 	.db	120
      005D4F 0E                    4694 	.sleb128	14
      005D50 00 00 A4 62           4695 	.dw	0,(Sstm8s_uart1$UART1_Init$52)
      005D54 00 00 A4 6B           4696 	.dw	0,(Sstm8s_uart1$UART1_Init$54)
      005D58 00 02                 4697 	.dw	2
      005D5A 78                    4698 	.db	120
      005D5B 0E                    4699 	.sleb128	14
      005D5C 00 00 A4 5D           4700 	.dw	0,(Sstm8s_uart1$UART1_Init$51)
      005D60 00 00 A4 62           4701 	.dw	0,(Sstm8s_uart1$UART1_Init$52)
      005D64 00 02                 4702 	.dw	2
      005D66 78                    4703 	.db	120
      005D67 14                    4704 	.sleb128	20
      005D68 00 00 A4 5B           4705 	.dw	0,(Sstm8s_uart1$UART1_Init$50)
      005D6C 00 00 A4 5D           4706 	.dw	0,(Sstm8s_uart1$UART1_Init$51)
      005D70 00 02                 4707 	.dw	2
      005D72 78                    4708 	.db	120
      005D73 13                    4709 	.sleb128	19
      005D74 00 00 A4 59           4710 	.dw	0,(Sstm8s_uart1$UART1_Init$49)
      005D78 00 00 A4 5B           4711 	.dw	0,(Sstm8s_uart1$UART1_Init$50)
      005D7C 00 02                 4712 	.dw	2
      005D7E 78                    4713 	.db	120
      005D7F 12                    4714 	.sleb128	18
      005D80 00 00 A4 57           4715 	.dw	0,(Sstm8s_uart1$UART1_Init$48)
      005D84 00 00 A4 59           4716 	.dw	0,(Sstm8s_uart1$UART1_Init$49)
      005D88 00 02                 4717 	.dw	2
      005D8A 78                    4718 	.db	120
      005D8B 11                    4719 	.sleb128	17
      005D8C 00 00 A4 55           4720 	.dw	0,(Sstm8s_uart1$UART1_Init$47)
      005D90 00 00 A4 57           4721 	.dw	0,(Sstm8s_uart1$UART1_Init$48)
      005D94 00 02                 4722 	.dw	2
      005D96 78                    4723 	.db	120
      005D97 0F                    4724 	.sleb128	15
      005D98 00 00 A4 53           4725 	.dw	0,(Sstm8s_uart1$UART1_Init$46)
      005D9C 00 00 A4 55           4726 	.dw	0,(Sstm8s_uart1$UART1_Init$47)
      005DA0 00 02                 4727 	.dw	2
      005DA2 78                    4728 	.db	120
      005DA3 0E                    4729 	.sleb128	14
      005DA4 00 00 A4 4D           4730 	.dw	0,(Sstm8s_uart1$UART1_Init$45)
      005DA8 00 00 A4 53           4731 	.dw	0,(Sstm8s_uart1$UART1_Init$46)
      005DAC 00 02                 4732 	.dw	2
      005DAE 78                    4733 	.db	120
      005DAF 0E                    4734 	.sleb128	14
      005DB0 00 00 A4 43           4735 	.dw	0,(Sstm8s_uart1$UART1_Init$43)
      005DB4 00 00 A4 4D           4736 	.dw	0,(Sstm8s_uart1$UART1_Init$45)
      005DB8 00 02                 4737 	.dw	2
      005DBA 78                    4738 	.db	120
      005DBB 0E                    4739 	.sleb128	14
      005DBC 00 00 A4 3E           4740 	.dw	0,(Sstm8s_uart1$UART1_Init$42)
      005DC0 00 00 A4 43           4741 	.dw	0,(Sstm8s_uart1$UART1_Init$43)
      005DC4 00 02                 4742 	.dw	2
      005DC6 78                    4743 	.db	120
      005DC7 14                    4744 	.sleb128	20
      005DC8 00 00 A4 3C           4745 	.dw	0,(Sstm8s_uart1$UART1_Init$41)
      005DCC 00 00 A4 3E           4746 	.dw	0,(Sstm8s_uart1$UART1_Init$42)
      005DD0 00 02                 4747 	.dw	2
      005DD2 78                    4748 	.db	120
      005DD3 13                    4749 	.sleb128	19
      005DD4 00 00 A4 3A           4750 	.dw	0,(Sstm8s_uart1$UART1_Init$40)
      005DD8 00 00 A4 3C           4751 	.dw	0,(Sstm8s_uart1$UART1_Init$41)
      005DDC 00 02                 4752 	.dw	2
      005DDE 78                    4753 	.db	120
      005DDF 12                    4754 	.sleb128	18
      005DE0 00 00 A4 38           4755 	.dw	0,(Sstm8s_uart1$UART1_Init$39)
      005DE4 00 00 A4 3A           4756 	.dw	0,(Sstm8s_uart1$UART1_Init$40)
      005DE8 00 02                 4757 	.dw	2
      005DEA 78                    4758 	.db	120
      005DEB 11                    4759 	.sleb128	17
      005DEC 00 00 A4 36           4760 	.dw	0,(Sstm8s_uart1$UART1_Init$38)
      005DF0 00 00 A4 38           4761 	.dw	0,(Sstm8s_uart1$UART1_Init$39)
      005DF4 00 02                 4762 	.dw	2
      005DF6 78                    4763 	.db	120
      005DF7 0F                    4764 	.sleb128	15
      005DF8 00 00 A4 34           4765 	.dw	0,(Sstm8s_uart1$UART1_Init$37)
      005DFC 00 00 A4 36           4766 	.dw	0,(Sstm8s_uart1$UART1_Init$38)
      005E00 00 02                 4767 	.dw	2
      005E02 78                    4768 	.db	120
      005E03 0E                    4769 	.sleb128	14
      005E04 00 00 A4 2E           4770 	.dw	0,(Sstm8s_uart1$UART1_Init$36)
      005E08 00 00 A4 34           4771 	.dw	0,(Sstm8s_uart1$UART1_Init$37)
      005E0C 00 02                 4772 	.dw	2
      005E0E 78                    4773 	.db	120
      005E0F 0E                    4774 	.sleb128	14
      005E10 00 00 A4 28           4775 	.dw	0,(Sstm8s_uart1$UART1_Init$35)
      005E14 00 00 A4 2E           4776 	.dw	0,(Sstm8s_uart1$UART1_Init$36)
      005E18 00 02                 4777 	.dw	2
      005E1A 78                    4778 	.db	120
      005E1B 0E                    4779 	.sleb128	14
      005E1C 00 00 A4 1E           4780 	.dw	0,(Sstm8s_uart1$UART1_Init$33)
      005E20 00 00 A4 28           4781 	.dw	0,(Sstm8s_uart1$UART1_Init$35)
      005E24 00 02                 4782 	.dw	2
      005E26 78                    4783 	.db	120
      005E27 0E                    4784 	.sleb128	14
      005E28 00 00 A4 19           4785 	.dw	0,(Sstm8s_uart1$UART1_Init$32)
      005E2C 00 00 A4 1E           4786 	.dw	0,(Sstm8s_uart1$UART1_Init$33)
      005E30 00 02                 4787 	.dw	2
      005E32 78                    4788 	.db	120
      005E33 14                    4789 	.sleb128	20
      005E34 00 00 A4 17           4790 	.dw	0,(Sstm8s_uart1$UART1_Init$31)
      005E38 00 00 A4 19           4791 	.dw	0,(Sstm8s_uart1$UART1_Init$32)
      005E3C 00 02                 4792 	.dw	2
      005E3E 78                    4793 	.db	120
      005E3F 13                    4794 	.sleb128	19
      005E40 00 00 A4 15           4795 	.dw	0,(Sstm8s_uart1$UART1_Init$30)
      005E44 00 00 A4 17           4796 	.dw	0,(Sstm8s_uart1$UART1_Init$31)
      005E48 00 02                 4797 	.dw	2
      005E4A 78                    4798 	.db	120
      005E4B 12                    4799 	.sleb128	18
      005E4C 00 00 A4 13           4800 	.dw	0,(Sstm8s_uart1$UART1_Init$29)
      005E50 00 00 A4 15           4801 	.dw	0,(Sstm8s_uart1$UART1_Init$30)
      005E54 00 02                 4802 	.dw	2
      005E56 78                    4803 	.db	120
      005E57 11                    4804 	.sleb128	17
      005E58 00 00 A4 11           4805 	.dw	0,(Sstm8s_uart1$UART1_Init$28)
      005E5C 00 00 A4 13           4806 	.dw	0,(Sstm8s_uart1$UART1_Init$29)
      005E60 00 02                 4807 	.dw	2
      005E62 78                    4808 	.db	120
      005E63 0F                    4809 	.sleb128	15
      005E64 00 00 A4 0F           4810 	.dw	0,(Sstm8s_uart1$UART1_Init$27)
      005E68 00 00 A4 11           4811 	.dw	0,(Sstm8s_uart1$UART1_Init$28)
      005E6C 00 02                 4812 	.dw	2
      005E6E 78                    4813 	.db	120
      005E6F 0E                    4814 	.sleb128	14
      005E70 00 00 A4 05           4815 	.dw	0,(Sstm8s_uart1$UART1_Init$25)
      005E74 00 00 A4 0F           4816 	.dw	0,(Sstm8s_uart1$UART1_Init$27)
      005E78 00 02                 4817 	.dw	2
      005E7A 78                    4818 	.db	120
      005E7B 0E                    4819 	.sleb128	14
      005E7C 00 00 A4 00           4820 	.dw	0,(Sstm8s_uart1$UART1_Init$24)
      005E80 00 00 A4 05           4821 	.dw	0,(Sstm8s_uart1$UART1_Init$25)
      005E84 00 02                 4822 	.dw	2
      005E86 78                    4823 	.db	120
      005E87 14                    4824 	.sleb128	20
      005E88 00 00 A3 FE           4825 	.dw	0,(Sstm8s_uart1$UART1_Init$23)
      005E8C 00 00 A4 00           4826 	.dw	0,(Sstm8s_uart1$UART1_Init$24)
      005E90 00 02                 4827 	.dw	2
      005E92 78                    4828 	.db	120
      005E93 13                    4829 	.sleb128	19
      005E94 00 00 A3 FC           4830 	.dw	0,(Sstm8s_uart1$UART1_Init$22)
      005E98 00 00 A3 FE           4831 	.dw	0,(Sstm8s_uart1$UART1_Init$23)
      005E9C 00 02                 4832 	.dw	2
      005E9E 78                    4833 	.db	120
      005E9F 12                    4834 	.sleb128	18
      005EA0 00 00 A3 FA           4835 	.dw	0,(Sstm8s_uart1$UART1_Init$21)
      005EA4 00 00 A3 FC           4836 	.dw	0,(Sstm8s_uart1$UART1_Init$22)
      005EA8 00 02                 4837 	.dw	2
      005EAA 78                    4838 	.db	120
      005EAB 11                    4839 	.sleb128	17
      005EAC 00 00 A3 F8           4840 	.dw	0,(Sstm8s_uart1$UART1_Init$20)
      005EB0 00 00 A3 FA           4841 	.dw	0,(Sstm8s_uart1$UART1_Init$21)
      005EB4 00 02                 4842 	.dw	2
      005EB6 78                    4843 	.db	120
      005EB7 0F                    4844 	.sleb128	15
      005EB8 00 00 A3 E8           4845 	.dw	0,(Sstm8s_uart1$UART1_Init$18)
      005EBC 00 00 A3 F8           4846 	.dw	0,(Sstm8s_uart1$UART1_Init$20)
      005EC0 00 02                 4847 	.dw	2
      005EC2 78                    4848 	.db	120
      005EC3 0E                    4849 	.sleb128	14
      005EC4 00 00 A3 E6           4850 	.dw	0,(Sstm8s_uart1$UART1_Init$17)
      005EC8 00 00 A3 E8           4851 	.dw	0,(Sstm8s_uart1$UART1_Init$18)
      005ECC 00 02                 4852 	.dw	2
      005ECE 78                    4853 	.db	120
      005ECF 01                    4854 	.sleb128	1
      005ED0 00 00 00 00           4855 	.dw	0,0
      005ED4 00 00 00 00           4856 	.dw	0,0
      005ED8 00 00 A3 BB           4857 	.dw	0,(Sstm8s_uart1$UART1_DeInit$1)
      005EDC 00 00 A3 E6           4858 	.dw	0,(Sstm8s_uart1$UART1_DeInit$15)
      005EE0 00 02                 4859 	.dw	2
      005EE2 78                    4860 	.db	120
      005EE3 01                    4861 	.sleb128	1
      005EE4 00 00 00 00           4862 	.dw	0,0
      005EE8 00 00 00 00           4863 	.dw	0,0
                                   4864 
                                   4865 	.area .debug_abbrev (NOLOAD)
      000543                       4866 Ldebug_abbrev:
      000543 09                    4867 	.uleb128	9
      000544 2E                    4868 	.uleb128	46
      000545 00                    4869 	.db	0
      000546 03                    4870 	.uleb128	3
      000547 08                    4871 	.uleb128	8
      000548 11                    4872 	.uleb128	17
      000549 01                    4873 	.uleb128	1
      00054A 12                    4874 	.uleb128	18
      00054B 01                    4875 	.uleb128	1
      00054C 3F                    4876 	.uleb128	63
      00054D 0C                    4877 	.uleb128	12
      00054E 40                    4878 	.uleb128	64
      00054F 06                    4879 	.uleb128	6
      000550 49                    4880 	.uleb128	73
      000551 13                    4881 	.uleb128	19
      000552 00                    4882 	.uleb128	0
      000553 00                    4883 	.uleb128	0
      000554 04                    4884 	.uleb128	4
      000555 05                    4885 	.uleb128	5
      000556 00                    4886 	.db	0
      000557 02                    4887 	.uleb128	2
      000558 0A                    4888 	.uleb128	10
      000559 03                    4889 	.uleb128	3
      00055A 08                    4890 	.uleb128	8
      00055B 49                    4891 	.uleb128	73
      00055C 13                    4892 	.uleb128	19
      00055D 00                    4893 	.uleb128	0
      00055E 00                    4894 	.uleb128	0
      00055F 0C                    4895 	.uleb128	12
      000560 01                    4896 	.uleb128	1
      000561 01                    4897 	.db	1
      000562 01                    4898 	.uleb128	1
      000563 13                    4899 	.uleb128	19
      000564 0B                    4900 	.uleb128	11
      000565 0B                    4901 	.uleb128	11
      000566 49                    4902 	.uleb128	73
      000567 13                    4903 	.uleb128	19
      000568 00                    4904 	.uleb128	0
      000569 00                    4905 	.uleb128	0
      00056A 03                    4906 	.uleb128	3
      00056B 2E                    4907 	.uleb128	46
      00056C 01                    4908 	.db	1
      00056D 01                    4909 	.uleb128	1
      00056E 13                    4910 	.uleb128	19
      00056F 03                    4911 	.uleb128	3
      000570 08                    4912 	.uleb128	8
      000571 11                    4913 	.uleb128	17
      000572 01                    4914 	.uleb128	1
      000573 12                    4915 	.uleb128	18
      000574 01                    4916 	.uleb128	1
      000575 3F                    4917 	.uleb128	63
      000576 0C                    4918 	.uleb128	12
      000577 40                    4919 	.uleb128	64
      000578 06                    4920 	.uleb128	6
      000579 00                    4921 	.uleb128	0
      00057A 00                    4922 	.uleb128	0
      00057B 06                    4923 	.uleb128	6
      00057C 34                    4924 	.uleb128	52
      00057D 00                    4925 	.db	0
      00057E 02                    4926 	.uleb128	2
      00057F 0A                    4927 	.uleb128	10
      000580 03                    4928 	.uleb128	3
      000581 08                    4929 	.uleb128	8
      000582 49                    4930 	.uleb128	73
      000583 13                    4931 	.uleb128	19
      000584 00                    4932 	.uleb128	0
      000585 00                    4933 	.uleb128	0
      000586 0A                    4934 	.uleb128	10
      000587 2E                    4935 	.uleb128	46
      000588 01                    4936 	.db	1
      000589 01                    4937 	.uleb128	1
      00058A 13                    4938 	.uleb128	19
      00058B 03                    4939 	.uleb128	3
      00058C 08                    4940 	.uleb128	8
      00058D 11                    4941 	.uleb128	17
      00058E 01                    4942 	.uleb128	1
      00058F 12                    4943 	.uleb128	18
      000590 01                    4944 	.uleb128	1
      000591 3F                    4945 	.uleb128	63
      000592 0C                    4946 	.uleb128	12
      000593 40                    4947 	.uleb128	64
      000594 06                    4948 	.uleb128	6
      000595 49                    4949 	.uleb128	73
      000596 13                    4950 	.uleb128	19
      000597 00                    4951 	.uleb128	0
      000598 00                    4952 	.uleb128	0
      000599 0B                    4953 	.uleb128	11
      00059A 26                    4954 	.uleb128	38
      00059B 00                    4955 	.db	0
      00059C 49                    4956 	.uleb128	73
      00059D 13                    4957 	.uleb128	19
      00059E 00                    4958 	.uleb128	0
      00059F 00                    4959 	.uleb128	0
      0005A0 01                    4960 	.uleb128	1
      0005A1 11                    4961 	.uleb128	17
      0005A2 01                    4962 	.db	1
      0005A3 03                    4963 	.uleb128	3
      0005A4 08                    4964 	.uleb128	8
      0005A5 10                    4965 	.uleb128	16
      0005A6 06                    4966 	.uleb128	6
      0005A7 13                    4967 	.uleb128	19
      0005A8 0B                    4968 	.uleb128	11
      0005A9 25                    4969 	.uleb128	37
      0005AA 08                    4970 	.uleb128	8
      0005AB 00                    4971 	.uleb128	0
      0005AC 00                    4972 	.uleb128	0
      0005AD 05                    4973 	.uleb128	5
      0005AE 0B                    4974 	.uleb128	11
      0005AF 00                    4975 	.db	0
      0005B0 11                    4976 	.uleb128	17
      0005B1 01                    4977 	.uleb128	1
      0005B2 12                    4978 	.uleb128	18
      0005B3 01                    4979 	.uleb128	1
      0005B4 00                    4980 	.uleb128	0
      0005B5 00                    4981 	.uleb128	0
      0005B6 08                    4982 	.uleb128	8
      0005B7 0B                    4983 	.uleb128	11
      0005B8 01                    4984 	.db	1
      0005B9 01                    4985 	.uleb128	1
      0005BA 13                    4986 	.uleb128	19
      0005BB 11                    4987 	.uleb128	17
      0005BC 01                    4988 	.uleb128	1
      0005BD 00                    4989 	.uleb128	0
      0005BE 00                    4990 	.uleb128	0
      0005BF 02                    4991 	.uleb128	2
      0005C0 2E                    4992 	.uleb128	46
      0005C1 00                    4993 	.db	0
      0005C2 03                    4994 	.uleb128	3
      0005C3 08                    4995 	.uleb128	8
      0005C4 11                    4996 	.uleb128	17
      0005C5 01                    4997 	.uleb128	1
      0005C6 12                    4998 	.uleb128	18
      0005C7 01                    4999 	.uleb128	1
      0005C8 3F                    5000 	.uleb128	63
      0005C9 0C                    5001 	.uleb128	12
      0005CA 40                    5002 	.uleb128	64
      0005CB 06                    5003 	.uleb128	6
      0005CC 00                    5004 	.uleb128	0
      0005CD 00                    5005 	.uleb128	0
      0005CE 0D                    5006 	.uleb128	13
      0005CF 21                    5007 	.uleb128	33
      0005D0 00                    5008 	.db	0
      0005D1 2F                    5009 	.uleb128	47
      0005D2 0B                    5010 	.uleb128	11
      0005D3 00                    5011 	.uleb128	0
      0005D4 00                    5012 	.uleb128	0
      0005D5 07                    5013 	.uleb128	7
      0005D6 24                    5014 	.uleb128	36
      0005D7 00                    5015 	.db	0
      0005D8 03                    5016 	.uleb128	3
      0005D9 08                    5017 	.uleb128	8
      0005DA 0B                    5018 	.uleb128	11
      0005DB 0B                    5019 	.uleb128	11
      0005DC 3E                    5020 	.uleb128	62
      0005DD 0B                    5021 	.uleb128	11
      0005DE 00                    5022 	.uleb128	0
      0005DF 00                    5023 	.uleb128	0
      0005E0 00                    5024 	.uleb128	0
                                   5025 
                                   5026 	.area .debug_info (NOLOAD)
      003577 00 00 08 FE           5027 	.dw	0,Ldebug_info_end-Ldebug_info_start
      00357B                       5028 Ldebug_info_start:
      00357B 00 02                 5029 	.dw	2
      00357D 00 00 05 43           5030 	.dw	0,(Ldebug_abbrev)
      003581 04                    5031 	.db	4
      003582 01                    5032 	.uleb128	1
      003583 64 72 69 76 65 72 73  5033 	.ascii "drivers/src/stm8s_uart1.c"
             2F 73 72 63 2F 73 74
             6D 38 73 5F 75 61 72
             74 31 2E 63
      00359C 00                    5034 	.db	0
      00359D 00 00 26 D4           5035 	.dw	0,(Ldebug_line_start+-4)
      0035A1 01                    5036 	.db	1
      0035A2 53 44 43 43 20 76 65  5037 	.ascii "SDCC version 4.1.0 #12072"
             72 73 69 6F 6E 20 34
             2E 31 2E 30 20 23 31
             32 30 37 32
      0035BB 00                    5038 	.db	0
      0035BC 02                    5039 	.uleb128	2
      0035BD 55 41 52 54 31 5F 44  5040 	.ascii "UART1_DeInit"
             65 49 6E 69 74
      0035C9 00                    5041 	.db	0
      0035CA 00 00 A3 BB           5042 	.dw	0,(_UART1_DeInit)
      0035CE 00 00 A3 E6           5043 	.dw	0,(XG$UART1_DeInit$0$0+1)
      0035D2 01                    5044 	.db	1
      0035D3 00 00 5E D8           5045 	.dw	0,(Ldebug_loc_start+3624)
      0035D7 03                    5046 	.uleb128	3
      0035D8 00 00 01 5A           5047 	.dw	0,346
      0035DC 55 41 52 54 31 5F 49  5048 	.ascii "UART1_Init"
             6E 69 74
      0035E6 00                    5049 	.db	0
      0035E7 00 00 A3 E6           5050 	.dw	0,(_UART1_Init)
      0035EB 00 00 A6 4A           5051 	.dw	0,(XG$UART1_Init$0$0+1)
      0035EF 01                    5052 	.db	1
      0035F0 00 00 5A 50           5053 	.dw	0,(Ldebug_loc_start+2464)
      0035F4 04                    5054 	.uleb128	4
      0035F5 02                    5055 	.db	2
      0035F6 91                    5056 	.db	145
      0035F7 02                    5057 	.sleb128	2
      0035F8 42 61 75 64 52 61 74  5058 	.ascii "BaudRate"
             65
      003600 00                    5059 	.db	0
      003601 00 00 01 5A           5060 	.dw	0,346
      003605 04                    5061 	.uleb128	4
      003606 02                    5062 	.db	2
      003607 91                    5063 	.db	145
      003608 06                    5064 	.sleb128	6
      003609 57 6F 72 64 4C 65 6E  5065 	.ascii "WordLength"
             67 74 68
      003613 00                    5066 	.db	0
      003614 00 00 01 6B           5067 	.dw	0,363
      003618 04                    5068 	.uleb128	4
      003619 02                    5069 	.db	2
      00361A 91                    5070 	.db	145
      00361B 07                    5071 	.sleb128	7
      00361C 53 74 6F 70 42 69 74  5072 	.ascii "StopBits"
             73
      003624 00                    5073 	.db	0
      003625 00 00 01 6B           5074 	.dw	0,363
      003629 04                    5075 	.uleb128	4
      00362A 02                    5076 	.db	2
      00362B 91                    5077 	.db	145
      00362C 08                    5078 	.sleb128	8
      00362D 50 61 72 69 74 79     5079 	.ascii "Parity"
      003633 00                    5080 	.db	0
      003634 00 00 01 6B           5081 	.dw	0,363
      003638 04                    5082 	.uleb128	4
      003639 02                    5083 	.db	2
      00363A 91                    5084 	.db	145
      00363B 09                    5085 	.sleb128	9
      00363C 53 79 6E 63 4D 6F 64  5086 	.ascii "SyncMode"
             65
      003644 00                    5087 	.db	0
      003645 00 00 01 6B           5088 	.dw	0,363
      003649 04                    5089 	.uleb128	4
      00364A 02                    5090 	.db	2
      00364B 91                    5091 	.db	145
      00364C 0A                    5092 	.sleb128	10
      00364D 4D 6F 64 65           5093 	.ascii "Mode"
      003651 00                    5094 	.db	0
      003652 00 00 01 6B           5095 	.dw	0,363
      003656 05                    5096 	.uleb128	5
      003657 00 00 A6 09           5097 	.dw	0,(Sstm8s_uart1$UART1_Init$133)
      00365B 00 00 A6 0E           5098 	.dw	0,(Sstm8s_uart1$UART1_Init$135)
      00365F 05                    5099 	.uleb128	5
      003660 00 00 A6 10           5100 	.dw	0,(Sstm8s_uart1$UART1_Init$136)
      003664 00 00 A6 15           5101 	.dw	0,(Sstm8s_uart1$UART1_Init$138)
      003668 05                    5102 	.uleb128	5
      003669 00 00 A6 20           5103 	.dw	0,(Sstm8s_uart1$UART1_Init$143)
      00366D 00 00 A6 25           5104 	.dw	0,(Sstm8s_uart1$UART1_Init$145)
      003671 05                    5105 	.uleb128	5
      003672 00 00 A6 27           5106 	.dw	0,(Sstm8s_uart1$UART1_Init$146)
      003676 00 00 A6 2C           5107 	.dw	0,(Sstm8s_uart1$UART1_Init$148)
      00367A 05                    5108 	.uleb128	5
      00367B 00 00 A6 33           5109 	.dw	0,(Sstm8s_uart1$UART1_Init$151)
      00367F 00 00 A6 38           5110 	.dw	0,(Sstm8s_uart1$UART1_Init$153)
      003683 05                    5111 	.uleb128	5
      003684 00 00 A6 3A           5112 	.dw	0,(Sstm8s_uart1$UART1_Init$154)
      003688 00 00 A6 47           5113 	.dw	0,(Sstm8s_uart1$UART1_Init$158)
      00368C 06                    5114 	.uleb128	6
      00368D 02                    5115 	.db	2
      00368E 91                    5116 	.db	145
      00368F 73                    5117 	.sleb128	-13
      003690 42 61 75 64 52 61 74  5118 	.ascii "BaudRate_Mantissa"
             65 5F 4D 61 6E 74 69
             73 73 61
      0036A1 00                    5119 	.db	0
      0036A2 00 00 01 5A           5120 	.dw	0,346
      0036A6 06                    5121 	.uleb128	6
      0036A7 0F                    5122 	.db	15
      0036A8 91                    5123 	.db	145
      0036A9 77                    5124 	.sleb128	-9
      0036AA 93                    5125 	.db	147
      0036AB 01                    5126 	.uleb128	1
      0036AC 50                    5127 	.db	80
      0036AD 93                    5128 	.db	147
      0036AE 01                    5129 	.uleb128	1
      0036AF 91                    5130 	.db	145
      0036B0 79                    5131 	.sleb128	-7
      0036B1 93                    5132 	.db	147
      0036B2 01                    5133 	.uleb128	1
      0036B3 91                    5134 	.db	145
      0036B4 7A                    5135 	.sleb128	-6
      0036B5 93                    5136 	.db	147
      0036B6 01                    5137 	.uleb128	1
      0036B7 42 61 75 64 52 61 74  5138 	.ascii "BaudRate_Mantissa100"
             65 5F 4D 61 6E 74 69
             73 73 61 31 30 30
      0036CB 00                    5139 	.db	0
      0036CC 00 00 01 5A           5140 	.dw	0,346
      0036D0 00                    5141 	.uleb128	0
      0036D1 07                    5142 	.uleb128	7
      0036D2 75 6E 73 69 67 6E 65  5143 	.ascii "unsigned long"
             64 20 6C 6F 6E 67
      0036DF 00                    5144 	.db	0
      0036E0 04                    5145 	.db	4
      0036E1 07                    5146 	.db	7
      0036E2 07                    5147 	.uleb128	7
      0036E3 75 6E 73 69 67 6E 65  5148 	.ascii "unsigned char"
             64 20 63 68 61 72
      0036F0 00                    5149 	.db	0
      0036F1 01                    5150 	.db	1
      0036F2 08                    5151 	.db	8
      0036F3 03                    5152 	.uleb128	3
      0036F4 00 00 01 BC           5153 	.dw	0,444
      0036F8 55 41 52 54 31 5F 43  5154 	.ascii "UART1_Cmd"
             6D 64
      003701 00                    5155 	.db	0
      003702 00 00 A6 4A           5156 	.dw	0,(_UART1_Cmd)
      003706 00 00 A6 5E           5157 	.dw	0,(XG$UART1_Cmd$0$0+1)
      00370A 01                    5158 	.db	1
      00370B 00 00 5A 3C           5159 	.dw	0,(Ldebug_loc_start+2444)
      00370F 04                    5160 	.uleb128	4
      003710 02                    5161 	.db	2
      003711 91                    5162 	.db	145
      003712 02                    5163 	.sleb128	2
      003713 4E 65 77 53 74 61 74  5164 	.ascii "NewState"
             65
      00371B 00                    5165 	.db	0
      00371C 00 00 01 6B           5166 	.dw	0,363
      003720 05                    5167 	.uleb128	5
      003721 00 00 A6 51           5168 	.dw	0,(Sstm8s_uart1$UART1_Cmd$167)
      003725 00 00 A6 56           5169 	.dw	0,(Sstm8s_uart1$UART1_Cmd$169)
      003729 05                    5170 	.uleb128	5
      00372A 00 00 A6 58           5171 	.dw	0,(Sstm8s_uart1$UART1_Cmd$170)
      00372E 00 00 A6 5D           5172 	.dw	0,(Sstm8s_uart1$UART1_Cmd$172)
      003732 00                    5173 	.uleb128	0
      003733 03                    5174 	.uleb128	3
      003734 00 00 02 67           5175 	.dw	0,615
      003738 55 41 52 54 31 5F 49  5176 	.ascii "UART1_ITConfig"
             54 43 6F 6E 66 69 67
      003746 00                    5177 	.db	0
      003747 00 00 A6 5E           5178 	.dw	0,(_UART1_ITConfig)
      00374B 00 00 A7 27           5179 	.dw	0,(XG$UART1_ITConfig$0$0+1)
      00374F 01                    5180 	.db	1
      003750 00 00 58 9C           5181 	.dw	0,(Ldebug_loc_start+2028)
      003754 04                    5182 	.uleb128	4
      003755 02                    5183 	.db	2
      003756 91                    5184 	.db	145
      003757 02                    5185 	.sleb128	2
      003758 55 41 52 54 31 5F 49  5186 	.ascii "UART1_IT"
             54
      003760 00                    5187 	.db	0
      003761 00 00 02 67           5188 	.dw	0,615
      003765 04                    5189 	.uleb128	4
      003766 02                    5190 	.db	2
      003767 91                    5191 	.db	145
      003768 04                    5192 	.sleb128	4
      003769 4E 65 77 53 74 61 74  5193 	.ascii "NewState"
             65
      003771 00                    5194 	.db	0
      003772 00 00 01 6B           5195 	.dw	0,363
      003776 08                    5196 	.uleb128	8
      003777 00 00 02 24           5197 	.dw	0,548
      00377B 00 00 A6 D7           5198 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$215)
      00377F 05                    5199 	.uleb128	5
      003780 00 00 A6 DB           5200 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$217)
      003784 00 00 A6 E3           5201 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$219)
      003788 05                    5202 	.uleb128	5
      003789 00 00 A6 E9           5203 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$221)
      00378D 00 00 A6 F1           5204 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$223)
      003791 05                    5205 	.uleb128	5
      003792 00 00 A6 F4           5206 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$224)
      003796 00 00 A6 FC           5207 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$226)
      00379A 00                    5208 	.uleb128	0
      00379B 08                    5209 	.uleb128	8
      00379C 00 00 02 49           5210 	.dw	0,585
      0037A0 00 00 A7 02           5211 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$230)
      0037A4 05                    5212 	.uleb128	5
      0037A5 00 00 A7 06           5213 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$232)
      0037A9 00 00 A7 0E           5214 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$234)
      0037AD 05                    5215 	.uleb128	5
      0037AE 00 00 A7 13           5216 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$236)
      0037B2 00 00 A7 1B           5217 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$238)
      0037B6 05                    5218 	.uleb128	5
      0037B7 00 00 A7 1D           5219 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$239)
      0037BB 00 00 A7 25           5220 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$241)
      0037BF 00                    5221 	.uleb128	0
      0037C0 06                    5222 	.uleb128	6
      0037C1 01                    5223 	.db	1
      0037C2 52                    5224 	.db	82
      0037C3 75 61 72 74 72 65 67  5225 	.ascii "uartreg"
      0037CA 00                    5226 	.db	0
      0037CB 00 00 01 6B           5227 	.dw	0,363
      0037CF 06                    5228 	.uleb128	6
      0037D0 02                    5229 	.db	2
      0037D1 91                    5230 	.db	145
      0037D2 7F                    5231 	.sleb128	-1
      0037D3 69 74 70 6F 73        5232 	.ascii "itpos"
      0037D8 00                    5233 	.db	0
      0037D9 00 00 01 6B           5234 	.dw	0,363
      0037DD 00                    5235 	.uleb128	0
      0037DE 07                    5236 	.uleb128	7
      0037DF 75 6E 73 69 67 6E 65  5237 	.ascii "unsigned int"
             64 20 69 6E 74
      0037EB 00                    5238 	.db	0
      0037EC 02                    5239 	.db	2
      0037ED 07                    5240 	.db	7
      0037EE 03                    5241 	.uleb128	3
      0037EF 00 00 02 C1           5242 	.dw	0,705
      0037F3 55 41 52 54 31 5F 48  5243 	.ascii "UART1_HalfDuplexCmd"
             61 6C 66 44 75 70 6C
             65 78 43 6D 64
      003806 00                    5244 	.db	0
      003807 00 00 A7 27           5245 	.dw	0,(_UART1_HalfDuplexCmd)
      00380B 00 00 A7 53           5246 	.dw	0,(XG$UART1_HalfDuplexCmd$0$0+1)
      00380F 01                    5247 	.db	1
      003810 00 00 58 34           5248 	.dw	0,(Ldebug_loc_start+1924)
      003814 04                    5249 	.uleb128	4
      003815 02                    5250 	.db	2
      003816 91                    5251 	.db	145
      003817 02                    5252 	.sleb128	2
      003818 4E 65 77 53 74 61 74  5253 	.ascii "NewState"
             65
      003820 00                    5254 	.db	0
      003821 00 00 01 6B           5255 	.dw	0,363
      003825 05                    5256 	.uleb128	5
      003826 00 00 A7 46           5257 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$258)
      00382A 00 00 A7 4B           5258 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$260)
      00382E 05                    5259 	.uleb128	5
      00382F 00 00 A7 4D           5260 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$261)
      003833 00 00 A7 52           5261 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$263)
      003837 00                    5262 	.uleb128	0
      003838 03                    5263 	.uleb128	3
      003839 00 00 03 0E           5264 	.dw	0,782
      00383D 55 41 52 54 31 5F 49  5265 	.ascii "UART1_IrDAConfig"
             72 44 41 43 6F 6E 66
             69 67
      00384D 00                    5266 	.db	0
      00384E 00 00 A7 53           5267 	.dw	0,(_UART1_IrDAConfig)
      003852 00 00 A7 7F           5268 	.dw	0,(XG$UART1_IrDAConfig$0$0+1)
      003856 01                    5269 	.db	1
      003857 00 00 57 CC           5270 	.dw	0,(Ldebug_loc_start+1820)
      00385B 04                    5271 	.uleb128	4
      00385C 02                    5272 	.db	2
      00385D 91                    5273 	.db	145
      00385E 02                    5274 	.sleb128	2
      00385F 55 41 52 54 31 5F 49  5275 	.ascii "UART1_IrDAMode"
             72 44 41 4D 6F 64 65
      00386D 00                    5276 	.db	0
      00386E 00 00 01 6B           5277 	.dw	0,363
      003872 05                    5278 	.uleb128	5
      003873 00 00 A7 72           5279 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$279)
      003877 00 00 A7 77           5280 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$281)
      00387B 05                    5281 	.uleb128	5
      00387C 00 00 A7 79           5282 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$282)
      003880 00 00 A7 7E           5283 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$284)
      003884 00                    5284 	.uleb128	0
      003885 03                    5285 	.uleb128	3
      003886 00 00 03 52           5286 	.dw	0,850
      00388A 55 41 52 54 31 5F 49  5287 	.ascii "UART1_IrDACmd"
             72 44 41 43 6D 64
      003897 00                    5288 	.db	0
      003898 00 00 A7 7F           5289 	.dw	0,(_UART1_IrDACmd)
      00389C 00 00 A7 AB           5290 	.dw	0,(XG$UART1_IrDACmd$0$0+1)
      0038A0 01                    5291 	.db	1
      0038A1 00 00 57 64           5292 	.dw	0,(Ldebug_loc_start+1716)
      0038A5 04                    5293 	.uleb128	4
      0038A6 02                    5294 	.db	2
      0038A7 91                    5295 	.db	145
      0038A8 02                    5296 	.sleb128	2
      0038A9 4E 65 77 53 74 61 74  5297 	.ascii "NewState"
             65
      0038B1 00                    5298 	.db	0
      0038B2 00 00 01 6B           5299 	.dw	0,363
      0038B6 05                    5300 	.uleb128	5
      0038B7 00 00 A7 9E           5301 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$300)
      0038BB 00 00 A7 A3           5302 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$302)
      0038BF 05                    5303 	.uleb128	5
      0038C0 00 00 A7 A5           5304 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$303)
      0038C4 00 00 A7 AA           5305 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$305)
      0038C8 00                    5306 	.uleb128	0
      0038C9 03                    5307 	.uleb128	3
      0038CA 00 00 03 BB           5308 	.dw	0,955
      0038CE 55 41 52 54 31 5F 4C  5309 	.ascii "UART1_LINBreakDetectionConfig"
             49 4E 42 72 65 61 6B
             44 65 74 65 63 74 69
             6F 6E 43 6F 6E 66 69
             67
      0038EB 00                    5310 	.db	0
      0038EC 00 00 A7 AB           5311 	.dw	0,(_UART1_LINBreakDetectionConfig)
      0038F0 00 00 A7 D7           5312 	.dw	0,(XG$UART1_LINBreakDetectionConfig$0$0+1)
      0038F4 01                    5313 	.db	1
      0038F5 00 00 56 FC           5314 	.dw	0,(Ldebug_loc_start+1612)
      0038F9 04                    5315 	.uleb128	4
      0038FA 02                    5316 	.db	2
      0038FB 91                    5317 	.db	145
      0038FC 02                    5318 	.sleb128	2
      0038FD 55 41 52 54 31 5F 4C  5319 	.ascii "UART1_LINBreakDetectionLength"
             49 4E 42 72 65 61 6B
             44 65 74 65 63 74 69
             6F 6E 4C 65 6E 67 74
             68
      00391A 00                    5320 	.db	0
      00391B 00 00 01 6B           5321 	.dw	0,363
      00391F 05                    5322 	.uleb128	5
      003920 00 00 A7 CA           5323 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$321)
      003924 00 00 A7 CF           5324 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$323)
      003928 05                    5325 	.uleb128	5
      003929 00 00 A7 D1           5326 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$324)
      00392D 00 00 A7 D6           5327 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$326)
      003931 00                    5328 	.uleb128	0
      003932 03                    5329 	.uleb128	3
      003933 00 00 03 FE           5330 	.dw	0,1022
      003937 55 41 52 54 31 5F 4C  5331 	.ascii "UART1_LINCmd"
             49 4E 43 6D 64
      003943 00                    5332 	.db	0
      003944 00 00 A7 D7           5333 	.dw	0,(_UART1_LINCmd)
      003948 00 00 A8 03           5334 	.dw	0,(XG$UART1_LINCmd$0$0+1)
      00394C 01                    5335 	.db	1
      00394D 00 00 56 94           5336 	.dw	0,(Ldebug_loc_start+1508)
      003951 04                    5337 	.uleb128	4
      003952 02                    5338 	.db	2
      003953 91                    5339 	.db	145
      003954 02                    5340 	.sleb128	2
      003955 4E 65 77 53 74 61 74  5341 	.ascii "NewState"
             65
      00395D 00                    5342 	.db	0
      00395E 00 00 01 6B           5343 	.dw	0,363
      003962 05                    5344 	.uleb128	5
      003963 00 00 A7 F6           5345 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$342)
      003967 00 00 A7 FB           5346 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$344)
      00396B 05                    5347 	.uleb128	5
      00396C 00 00 A7 FD           5348 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$345)
      003970 00 00 A8 02           5349 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$347)
      003974 00                    5350 	.uleb128	0
      003975 03                    5351 	.uleb128	3
      003976 00 00 04 47           5352 	.dw	0,1095
      00397A 55 41 52 54 31 5F 53  5353 	.ascii "UART1_SmartCardCmd"
             6D 61 72 74 43 61 72
             64 43 6D 64
      00398C 00                    5354 	.db	0
      00398D 00 00 A8 03           5355 	.dw	0,(_UART1_SmartCardCmd)
      003991 00 00 A8 2F           5356 	.dw	0,(XG$UART1_SmartCardCmd$0$0+1)
      003995 01                    5357 	.db	1
      003996 00 00 56 2C           5358 	.dw	0,(Ldebug_loc_start+1404)
      00399A 04                    5359 	.uleb128	4
      00399B 02                    5360 	.db	2
      00399C 91                    5361 	.db	145
      00399D 02                    5362 	.sleb128	2
      00399E 4E 65 77 53 74 61 74  5363 	.ascii "NewState"
             65
      0039A6 00                    5364 	.db	0
      0039A7 00 00 01 6B           5365 	.dw	0,363
      0039AB 05                    5366 	.uleb128	5
      0039AC 00 00 A8 22           5367 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$363)
      0039B0 00 00 A8 27           5368 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$365)
      0039B4 05                    5369 	.uleb128	5
      0039B5 00 00 A8 29           5370 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$366)
      0039B9 00 00 A8 2E           5371 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$368)
      0039BD 00                    5372 	.uleb128	0
      0039BE 03                    5373 	.uleb128	3
      0039BF 00 00 04 94           5374 	.dw	0,1172
      0039C3 55 41 52 54 31 5F 53  5375 	.ascii "UART1_SmartCardNACKCmd"
             6D 61 72 74 43 61 72
             64 4E 41 43 4B 43 6D
             64
      0039D9 00                    5376 	.db	0
      0039DA 00 00 A8 2F           5377 	.dw	0,(_UART1_SmartCardNACKCmd)
      0039DE 00 00 A8 5B           5378 	.dw	0,(XG$UART1_SmartCardNACKCmd$0$0+1)
      0039E2 01                    5379 	.db	1
      0039E3 00 00 55 C4           5380 	.dw	0,(Ldebug_loc_start+1300)
      0039E7 04                    5381 	.uleb128	4
      0039E8 02                    5382 	.db	2
      0039E9 91                    5383 	.db	145
      0039EA 02                    5384 	.sleb128	2
      0039EB 4E 65 77 53 74 61 74  5385 	.ascii "NewState"
             65
      0039F3 00                    5386 	.db	0
      0039F4 00 00 01 6B           5387 	.dw	0,363
      0039F8 05                    5388 	.uleb128	5
      0039F9 00 00 A8 4E           5389 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$384)
      0039FD 00 00 A8 53           5390 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$386)
      003A01 05                    5391 	.uleb128	5
      003A02 00 00 A8 55           5392 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$387)
      003A06 00 00 A8 5A           5393 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$389)
      003A0A 00                    5394 	.uleb128	0
      003A0B 03                    5395 	.uleb128	3
      003A0C 00 00 04 CF           5396 	.dw	0,1231
      003A10 55 41 52 54 31 5F 57  5397 	.ascii "UART1_WakeUpConfig"
             61 6B 65 55 70 43 6F
             6E 66 69 67
      003A22 00                    5398 	.db	0
      003A23 00 00 A8 5B           5399 	.dw	0,(_UART1_WakeUpConfig)
      003A27 00 00 A8 81           5400 	.dw	0,(XG$UART1_WakeUpConfig$0$0+1)
      003A2B 01                    5401 	.db	1
      003A2C 00 00 55 5C           5402 	.dw	0,(Ldebug_loc_start+1196)
      003A30 04                    5403 	.uleb128	4
      003A31 02                    5404 	.db	2
      003A32 91                    5405 	.db	145
      003A33 02                    5406 	.sleb128	2
      003A34 55 41 52 54 31 5F 57  5407 	.ascii "UART1_WakeUp"
             61 6B 65 55 70
      003A40 00                    5408 	.db	0
      003A41 00 00 01 6B           5409 	.dw	0,363
      003A45 00                    5410 	.uleb128	0
      003A46 03                    5411 	.uleb128	3
      003A47 00 00 05 1D           5412 	.dw	0,1309
      003A4B 55 41 52 54 31 5F 52  5413 	.ascii "UART1_ReceiverWakeUpCmd"
             65 63 65 69 76 65 72
             57 61 6B 65 55 70 43
             6D 64
      003A62 00                    5414 	.db	0
      003A63 00 00 A8 81           5415 	.dw	0,(_UART1_ReceiverWakeUpCmd)
      003A67 00 00 A8 AD           5416 	.dw	0,(XG$UART1_ReceiverWakeUpCmd$0$0+1)
      003A6B 01                    5417 	.db	1
      003A6C 00 00 54 F4           5418 	.dw	0,(Ldebug_loc_start+1092)
      003A70 04                    5419 	.uleb128	4
      003A71 02                    5420 	.db	2
      003A72 91                    5421 	.db	145
      003A73 02                    5422 	.sleb128	2
      003A74 4E 65 77 53 74 61 74  5423 	.ascii "NewState"
             65
      003A7C 00                    5424 	.db	0
      003A7D 00 00 01 6B           5425 	.dw	0,363
      003A81 05                    5426 	.uleb128	5
      003A82 00 00 A8 A0           5427 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$420)
      003A86 00 00 A8 A5           5428 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$422)
      003A8A 05                    5429 	.uleb128	5
      003A8B 00 00 A8 A7           5430 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$423)
      003A8F 00 00 A8 AC           5431 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$425)
      003A93 00                    5432 	.uleb128	0
      003A94 09                    5433 	.uleb128	9
      003A95 55 41 52 54 31 5F 52  5434 	.ascii "UART1_ReceiveData8"
             65 63 65 69 76 65 44
             61 74 61 38
      003AA7 00                    5435 	.db	0
      003AA8 00 00 A8 AD           5436 	.dw	0,(_UART1_ReceiveData8)
      003AAC 00 00 A8 B1           5437 	.dw	0,(XG$UART1_ReceiveData8$0$0+1)
      003AB0 01                    5438 	.db	1
      003AB1 00 00 54 E0           5439 	.dw	0,(Ldebug_loc_start+1072)
      003AB5 00 00 01 6B           5440 	.dw	0,363
      003AB9 07                    5441 	.uleb128	7
      003ABA 75 6E 73 69 67 6E 65  5442 	.ascii "unsigned int"
             64 20 69 6E 74
      003AC6 00                    5443 	.db	0
      003AC7 02                    5444 	.db	2
      003AC8 07                    5445 	.db	7
      003AC9 0A                    5446 	.uleb128	10
      003ACA 00 00 05 89           5447 	.dw	0,1417
      003ACE 55 41 52 54 31 5F 52  5448 	.ascii "UART1_ReceiveData9"
             65 63 65 69 76 65 44
             61 74 61 39
      003AE0 00                    5449 	.db	0
      003AE1 00 00 A8 B1           5450 	.dw	0,(_UART1_ReceiveData9)
      003AE5 00 00 A8 CC           5451 	.dw	0,(XG$UART1_ReceiveData9$0$0+1)
      003AE9 01                    5452 	.db	1
      003AEA 00 00 54 B4           5453 	.dw	0,(Ldebug_loc_start+1028)
      003AEE 00 00 05 42           5454 	.dw	0,1346
      003AF2 06                    5455 	.uleb128	6
      003AF3 02                    5456 	.db	2
      003AF4 91                    5457 	.db	145
      003AF5 7E                    5458 	.sleb128	-2
      003AF6 74 65 6D 70           5459 	.ascii "temp"
      003AFA 00                    5460 	.db	0
      003AFB 00 00 05 42           5461 	.dw	0,1346
      003AFF 00                    5462 	.uleb128	0
      003B00 03                    5463 	.uleb128	3
      003B01 00 00 05 B9           5464 	.dw	0,1465
      003B05 55 41 52 54 31 5F 53  5465 	.ascii "UART1_SendData8"
             65 6E 64 44 61 74 61
             38
      003B14 00                    5466 	.db	0
      003B15 00 00 A8 CC           5467 	.dw	0,(_UART1_SendData8)
      003B19 00 00 A8 D3           5468 	.dw	0,(XG$UART1_SendData8$0$0+1)
      003B1D 01                    5469 	.db	1
      003B1E 00 00 54 A0           5470 	.dw	0,(Ldebug_loc_start+1008)
      003B22 04                    5471 	.uleb128	4
      003B23 02                    5472 	.db	2
      003B24 91                    5473 	.db	145
      003B25 02                    5474 	.sleb128	2
      003B26 44 61 74 61           5475 	.ascii "Data"
      003B2A 00                    5476 	.db	0
      003B2B 00 00 01 6B           5477 	.dw	0,363
      003B2F 00                    5478 	.uleb128	0
      003B30 03                    5479 	.uleb128	3
      003B31 00 00 05 E9           5480 	.dw	0,1513
      003B35 55 41 52 54 31 5F 53  5481 	.ascii "UART1_SendData9"
             65 6E 64 44 61 74 61
             39
      003B44 00                    5482 	.db	0
      003B45 00 00 A8 D3           5483 	.dw	0,(_UART1_SendData9)
      003B49 00 00 A8 F0           5484 	.dw	0,(XG$UART1_SendData9$0$0+1)
      003B4D 01                    5485 	.db	1
      003B4E 00 00 54 74           5486 	.dw	0,(Ldebug_loc_start+964)
      003B52 04                    5487 	.uleb128	4
      003B53 02                    5488 	.db	2
      003B54 91                    5489 	.db	145
      003B55 02                    5490 	.sleb128	2
      003B56 44 61 74 61           5491 	.ascii "Data"
      003B5A 00                    5492 	.db	0
      003B5B 00 00 05 42           5493 	.dw	0,1346
      003B5F 00                    5494 	.uleb128	0
      003B60 02                    5495 	.uleb128	2
      003B61 55 41 52 54 31 5F 53  5496 	.ascii "UART1_SendBreak"
             65 6E 64 42 72 65 61
             6B
      003B70 00                    5497 	.db	0
      003B71 00 00 A8 F0           5498 	.dw	0,(_UART1_SendBreak)
      003B75 00 00 A8 F5           5499 	.dw	0,(XG$UART1_SendBreak$0$0+1)
      003B79 01                    5500 	.db	1
      003B7A 00 00 54 60           5501 	.dw	0,(Ldebug_loc_start+944)
      003B7E 03                    5502 	.uleb128	3
      003B7F 00 00 06 41           5503 	.dw	0,1601
      003B83 55 41 52 54 31 5F 53  5504 	.ascii "UART1_SetAddress"
             65 74 41 64 64 72 65
             73 73
      003B93 00                    5505 	.db	0
      003B94 00 00 A8 F5           5506 	.dw	0,(_UART1_SetAddress)
      003B98 00 00 A9 1B           5507 	.dw	0,(XG$UART1_SetAddress$0$0+1)
      003B9C 01                    5508 	.db	1
      003B9D 00 00 54 04           5509 	.dw	0,(Ldebug_loc_start+852)
      003BA1 04                    5510 	.uleb128	4
      003BA2 02                    5511 	.db	2
      003BA3 91                    5512 	.db	145
      003BA4 02                    5513 	.sleb128	2
      003BA5 55 41 52 54 31 5F 41  5514 	.ascii "UART1_Address"
             64 64 72 65 73 73
      003BB2 00                    5515 	.db	0
      003BB3 00 00 01 6B           5516 	.dw	0,363
      003BB7 00                    5517 	.uleb128	0
      003BB8 03                    5518 	.uleb128	3
      003BB9 00 00 06 7F           5519 	.dw	0,1663
      003BBD 55 41 52 54 31 5F 53  5520 	.ascii "UART1_SetGuardTime"
             65 74 47 75 61 72 64
             54 69 6D 65
      003BCF 00                    5521 	.db	0
      003BD0 00 00 A9 1B           5522 	.dw	0,(_UART1_SetGuardTime)
      003BD4 00 00 A9 22           5523 	.dw	0,(XG$UART1_SetGuardTime$0$0+1)
      003BD8 01                    5524 	.db	1
      003BD9 00 00 53 F0           5525 	.dw	0,(Ldebug_loc_start+832)
      003BDD 04                    5526 	.uleb128	4
      003BDE 02                    5527 	.db	2
      003BDF 91                    5528 	.db	145
      003BE0 02                    5529 	.sleb128	2
      003BE1 55 41 52 54 31 5F 47  5530 	.ascii "UART1_GuardTime"
             75 61 72 64 54 69 6D
             65
      003BF0 00                    5531 	.db	0
      003BF1 00 00 01 6B           5532 	.dw	0,363
      003BF5 00                    5533 	.uleb128	0
      003BF6 03                    5534 	.uleb128	3
      003BF7 00 00 06 BD           5535 	.dw	0,1725
      003BFB 55 41 52 54 31 5F 53  5536 	.ascii "UART1_SetPrescaler"
             65 74 50 72 65 73 63
             61 6C 65 72
      003C0D 00                    5537 	.db	0
      003C0E 00 00 A9 22           5538 	.dw	0,(_UART1_SetPrescaler)
      003C12 00 00 A9 29           5539 	.dw	0,(XG$UART1_SetPrescaler$0$0+1)
      003C16 01                    5540 	.db	1
      003C17 00 00 53 DC           5541 	.dw	0,(Ldebug_loc_start+812)
      003C1B 04                    5542 	.uleb128	4
      003C1C 02                    5543 	.db	2
      003C1D 91                    5544 	.db	145
      003C1E 02                    5545 	.sleb128	2
      003C1F 55 41 52 54 31 5F 50  5546 	.ascii "UART1_Prescaler"
             72 65 73 63 61 6C 65
             72
      003C2E 00                    5547 	.db	0
      003C2F 00 00 01 6B           5548 	.dw	0,363
      003C33 00                    5549 	.uleb128	0
      003C34 0A                    5550 	.uleb128	10
      003C35 00 00 07 5D           5551 	.dw	0,1885
      003C39 55 41 52 54 31 5F 47  5552 	.ascii "UART1_GetFlagStatus"
             65 74 46 6C 61 67 53
             74 61 74 75 73
      003C4C 00                    5553 	.db	0
      003C4D 00 00 A9 29           5554 	.dw	0,(_UART1_GetFlagStatus)
      003C51 00 00 A9 BD           5555 	.dw	0,(XG$UART1_GetFlagStatus$0$0+1)
      003C55 01                    5556 	.db	1
      003C56 00 00 52 C0           5557 	.dw	0,(Ldebug_loc_start+528)
      003C5A 00 00 01 6B           5558 	.dw	0,363
      003C5E 04                    5559 	.uleb128	4
      003C5F 02                    5560 	.db	2
      003C60 91                    5561 	.db	145
      003C61 02                    5562 	.sleb128	2
      003C62 55 41 52 54 31 5F 46  5563 	.ascii "UART1_FLAG"
             4C 41 47
      003C6C 00                    5564 	.db	0
      003C6D 00 00 02 67           5565 	.dw	0,615
      003C71 08                    5566 	.uleb128	8
      003C72 00 00 07 16           5567 	.dw	0,1814
      003C76 00 00 A9 8E           5568 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$518)
      003C7A 05                    5569 	.uleb128	5
      003C7B 00 00 A9 95           5570 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$520)
      003C7F 00 00 A9 97           5571 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$522)
      003C83 05                    5572 	.uleb128	5
      003C84 00 00 A9 9A           5573 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$523)
      003C88 00 00 A9 9B           5574 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$525)
      003C8C 00                    5575 	.uleb128	0
      003C8D 08                    5576 	.uleb128	8
      003C8E 00 00 07 32           5577 	.dw	0,1842
      003C92 00 00 A9 A1           5578 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$527)
      003C96 05                    5579 	.uleb128	5
      003C97 00 00 A9 A8           5580 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$529)
      003C9B 00 00 A9 AA           5581 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$531)
      003C9F 05                    5582 	.uleb128	5
      003CA0 00 00 A9 AC           5583 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$532)
      003CA4 00 00 A9 AD           5584 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$534)
      003CA8 00                    5585 	.uleb128	0
      003CA9 08                    5586 	.uleb128	8
      003CAA 00 00 07 4E           5587 	.dw	0,1870
      003CAE 00 00 A9 AF           5588 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$535)
      003CB2 05                    5589 	.uleb128	5
      003CB3 00 00 A9 B6           5590 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$537)
      003CB7 00 00 A9 B8           5591 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$539)
      003CBB 05                    5592 	.uleb128	5
      003CBC 00 00 A9 BA           5593 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$540)
      003CC0 00 00 A9 BB           5594 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$542)
      003CC4 00                    5595 	.uleb128	0
      003CC5 06                    5596 	.uleb128	6
      003CC6 01                    5597 	.db	1
      003CC7 50                    5598 	.db	80
      003CC8 73 74 61 74 75 73     5599 	.ascii "status"
      003CCE 00                    5600 	.db	0
      003CCF 00 00 01 6B           5601 	.dw	0,363
      003CD3 00                    5602 	.uleb128	0
      003CD4 03                    5603 	.uleb128	3
      003CD5 00 00 07 A5           5604 	.dw	0,1957
      003CD9 55 41 52 54 31 5F 43  5605 	.ascii "UART1_ClearFlag"
             6C 65 61 72 46 6C 61
             67
      003CE8 00                    5606 	.db	0
      003CE9 00 00 A9 BD           5607 	.dw	0,(_UART1_ClearFlag)
      003CED 00 00 A9 EF           5608 	.dw	0,(XG$UART1_ClearFlag$0$0+1)
      003CF1 01                    5609 	.db	1
      003CF2 00 00 52 34           5610 	.dw	0,(Ldebug_loc_start+388)
      003CF6 04                    5611 	.uleb128	4
      003CF7 02                    5612 	.db	2
      003CF8 91                    5613 	.db	145
      003CF9 02                    5614 	.sleb128	2
      003CFA 55 41 52 54 31 5F 46  5615 	.ascii "UART1_FLAG"
             4C 41 47
      003D04 00                    5616 	.db	0
      003D05 00 00 02 67           5617 	.dw	0,615
      003D09 05                    5618 	.uleb128	5
      003D0A 00 00 A9 E4           5619 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$562)
      003D0E 00 00 A9 E8           5620 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$564)
      003D12 05                    5621 	.uleb128	5
      003D13 00 00 A9 EA           5622 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$565)
      003D17 00 00 A9 EE           5623 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$567)
      003D1B 00                    5624 	.uleb128	0
      003D1C 0A                    5625 	.uleb128	10
      003D1D 00 00 08 8C           5626 	.dw	0,2188
      003D21 55 41 52 54 31 5F 47  5627 	.ascii "UART1_GetITStatus"
             65 74 49 54 53 74 61
             74 75 73
      003D32 00                    5628 	.db	0
      003D33 00 00 A9 EF           5629 	.dw	0,(_UART1_GetITStatus)
      003D37 00 00 AA B3           5630 	.dw	0,(XG$UART1_GetITStatus$0$0+1)
      003D3B 01                    5631 	.db	1
      003D3C 00 00 51 3C           5632 	.dw	0,(Ldebug_loc_start+140)
      003D40 00 00 01 6B           5633 	.dw	0,363
      003D44 04                    5634 	.uleb128	4
      003D45 02                    5635 	.db	2
      003D46 91                    5636 	.db	145
      003D47 02                    5637 	.sleb128	2
      003D48 55 41 52 54 31 5F 49  5638 	.ascii "UART1_IT"
             54
      003D50 00                    5639 	.db	0
      003D51 00 00 02 67           5640 	.dw	0,615
      003D55 08                    5641 	.uleb128	8
      003D56 00 00 07 FA           5642 	.dw	0,2042
      003D5A 00 00 AA 64           5643 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$596)
      003D5E 05                    5644 	.uleb128	5
      003D5F 00 00 AA 75           5645 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$599)
      003D63 00 00 AA 77           5646 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$601)
      003D67 05                    5647 	.uleb128	5
      003D68 00 00 AA 7A           5648 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$602)
      003D6C 00 00 AA 7B           5649 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$604)
      003D70 00                    5650 	.uleb128	0
      003D71 08                    5651 	.uleb128	8
      003D72 00 00 08 16           5652 	.dw	0,2070
      003D76 00 00 AA 82           5653 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$606)
      003D7A 05                    5654 	.uleb128	5
      003D7B 00 00 AA 93           5655 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$609)
      003D7F 00 00 AA 95           5656 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$611)
      003D83 05                    5657 	.uleb128	5
      003D84 00 00 AA 97           5658 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$612)
      003D88 00 00 AA 98           5659 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$614)
      003D8C 00                    5660 	.uleb128	0
      003D8D 08                    5661 	.uleb128	8
      003D8E 00 00 08 32           5662 	.dw	0,2098
      003D92 00 00 AA 9A           5663 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$615)
      003D96 05                    5664 	.uleb128	5
      003D97 00 00 AA AB           5665 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$618)
      003D9B 00 00 AA AD           5666 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$620)
      003D9F 05                    5667 	.uleb128	5
      003DA0 00 00 AA AF           5668 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$621)
      003DA4 00 00 AA B0           5669 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$623)
      003DA8 00                    5670 	.uleb128	0
      003DA9 06                    5671 	.uleb128	6
      003DAA 01                    5672 	.db	1
      003DAB 50                    5673 	.db	80
      003DAC 70 65 6E 64 69 6E 67  5674 	.ascii "pendingbitstatus"
             62 69 74 73 74 61 74
             75 73
      003DBC 00                    5675 	.db	0
      003DBD 00 00 01 6B           5676 	.dw	0,363
      003DC1 06                    5677 	.uleb128	6
      003DC2 02                    5678 	.db	2
      003DC3 91                    5679 	.db	145
      003DC4 7E                    5680 	.sleb128	-2
      003DC5 69 74 70 6F 73        5681 	.ascii "itpos"
      003DCA 00                    5682 	.db	0
      003DCB 00 00 01 6B           5683 	.dw	0,363
      003DCF 06                    5684 	.uleb128	6
      003DD0 01                    5685 	.db	1
      003DD1 50                    5686 	.db	80
      003DD2 69 74 6D 61 73 6B 31  5687 	.ascii "itmask1"
      003DD9 00                    5688 	.db	0
      003DDA 00 00 01 6B           5689 	.dw	0,363
      003DDE 06                    5690 	.uleb128	6
      003DDF 02                    5691 	.db	2
      003DE0 91                    5692 	.db	145
      003DE1 7F                    5693 	.sleb128	-1
      003DE2 69 74 6D 61 73 6B 32  5694 	.ascii "itmask2"
      003DE9 00                    5695 	.db	0
      003DEA 00 00 01 6B           5696 	.dw	0,363
      003DEE 06                    5697 	.uleb128	6
      003DEF 01                    5698 	.db	1
      003DF0 51                    5699 	.db	81
      003DF1 65 6E 61 62 6C 65 73  5700 	.ascii "enablestatus"
             74 61 74 75 73
      003DFD 00                    5701 	.db	0
      003DFE 00 00 01 6B           5702 	.dw	0,363
      003E02 00                    5703 	.uleb128	0
      003E03 03                    5704 	.uleb128	3
      003E04 00 00 08 DA           5705 	.dw	0,2266
      003E08 55 41 52 54 31 5F 43  5706 	.ascii "UART1_ClearITPendingBit"
             6C 65 61 72 49 54 50
             65 6E 64 69 6E 67 42
             69 74
      003E1F 00                    5707 	.db	0
      003E20 00 00 AA B3           5708 	.dw	0,(_UART1_ClearITPendingBit)
      003E24 00 00 AA E5           5709 	.dw	0,(XG$UART1_ClearITPendingBit$0$0+1)
      003E28 01                    5710 	.db	1
      003E29 00 00 50 B0           5711 	.dw	0,(Ldebug_loc_start)
      003E2D 04                    5712 	.uleb128	4
      003E2E 02                    5713 	.db	2
      003E2F 91                    5714 	.db	145
      003E30 02                    5715 	.sleb128	2
      003E31 55 41 52 54 31 5F 49  5716 	.ascii "UART1_IT"
             54
      003E39 00                    5717 	.db	0
      003E3A 00 00 02 67           5718 	.dw	0,615
      003E3E 05                    5719 	.uleb128	5
      003E3F 00 00 AA DA           5720 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$643)
      003E43 00 00 AA DE           5721 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$645)
      003E47 05                    5722 	.uleb128	5
      003E48 00 00 AA E0           5723 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$646)
      003E4C 00 00 AA E4           5724 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$648)
      003E50 00                    5725 	.uleb128	0
      003E51 0B                    5726 	.uleb128	11
      003E52 00 00 01 6B           5727 	.dw	0,363
      003E56 0C                    5728 	.uleb128	12
      003E57 00 00 08 EC           5729 	.dw	0,2284
      003E5B 1A                    5730 	.db	26
      003E5C 00 00 08 DA           5731 	.dw	0,2266
      003E60 0D                    5732 	.uleb128	13
      003E61 19                    5733 	.db	25
      003E62 00                    5734 	.uleb128	0
      003E63 06                    5735 	.uleb128	6
      003E64 05                    5736 	.db	5
      003E65 03                    5737 	.db	3
      003E66 00 00 81 5E           5738 	.dw	0,(___str_0)
      003E6A 5F 5F 73 74 72 5F 30  5739 	.ascii "__str_0"
      003E71 00                    5740 	.db	0
      003E72 00 00 08 DF           5741 	.dw	0,2271
      003E76 00                    5742 	.uleb128	0
      003E77 00                    5743 	.uleb128	0
      003E78 00                    5744 	.uleb128	0
      003E79                       5745 Ldebug_info_end:
                                   5746 
                                   5747 	.area .debug_pubnames (NOLOAD)
      000E4B 00 00 02 31           5748 	.dw	0,Ldebug_pubnames_end-Ldebug_pubnames_start
      000E4F                       5749 Ldebug_pubnames_start:
      000E4F 00 02                 5750 	.dw	2
      000E51 00 00 35 77           5751 	.dw	0,(Ldebug_info_start-4)
      000E55 00 00 09 02           5752 	.dw	0,4+Ldebug_info_end-Ldebug_info_start
      000E59 00 00 00 45           5753 	.dw	0,69
      000E5D 55 41 52 54 31 5F 44  5754 	.ascii "UART1_DeInit"
             65 49 6E 69 74
      000E69 00                    5755 	.db	0
      000E6A 00 00 00 60           5756 	.dw	0,96
      000E6E 55 41 52 54 31 5F 49  5757 	.ascii "UART1_Init"
             6E 69 74
      000E78 00                    5758 	.db	0
      000E79 00 00 01 7C           5759 	.dw	0,380
      000E7D 55 41 52 54 31 5F 43  5760 	.ascii "UART1_Cmd"
             6D 64
      000E86 00                    5761 	.db	0
      000E87 00 00 01 BC           5762 	.dw	0,444
      000E8B 55 41 52 54 31 5F 49  5763 	.ascii "UART1_ITConfig"
             54 43 6F 6E 66 69 67
      000E99 00                    5764 	.db	0
      000E9A 00 00 02 77           5765 	.dw	0,631
      000E9E 55 41 52 54 31 5F 48  5766 	.ascii "UART1_HalfDuplexCmd"
             61 6C 66 44 75 70 6C
             65 78 43 6D 64
      000EB1 00                    5767 	.db	0
      000EB2 00 00 02 C1           5768 	.dw	0,705
      000EB6 55 41 52 54 31 5F 49  5769 	.ascii "UART1_IrDAConfig"
             72 44 41 43 6F 6E 66
             69 67
      000EC6 00                    5770 	.db	0
      000EC7 00 00 03 0E           5771 	.dw	0,782
      000ECB 55 41 52 54 31 5F 49  5772 	.ascii "UART1_IrDACmd"
             72 44 41 43 6D 64
      000ED8 00                    5773 	.db	0
      000ED9 00 00 03 52           5774 	.dw	0,850
      000EDD 55 41 52 54 31 5F 4C  5775 	.ascii "UART1_LINBreakDetectionConfig"
             49 4E 42 72 65 61 6B
             44 65 74 65 63 74 69
             6F 6E 43 6F 6E 66 69
             67
      000EFA 00                    5776 	.db	0
      000EFB 00 00 03 BB           5777 	.dw	0,955
      000EFF 55 41 52 54 31 5F 4C  5778 	.ascii "UART1_LINCmd"
             49 4E 43 6D 64
      000F0B 00                    5779 	.db	0
      000F0C 00 00 03 FE           5780 	.dw	0,1022
      000F10 55 41 52 54 31 5F 53  5781 	.ascii "UART1_SmartCardCmd"
             6D 61 72 74 43 61 72
             64 43 6D 64
      000F22 00                    5782 	.db	0
      000F23 00 00 04 47           5783 	.dw	0,1095
      000F27 55 41 52 54 31 5F 53  5784 	.ascii "UART1_SmartCardNACKCmd"
             6D 61 72 74 43 61 72
             64 4E 41 43 4B 43 6D
             64
      000F3D 00                    5785 	.db	0
      000F3E 00 00 04 94           5786 	.dw	0,1172
      000F42 55 41 52 54 31 5F 57  5787 	.ascii "UART1_WakeUpConfig"
             61 6B 65 55 70 43 6F
             6E 66 69 67
      000F54 00                    5788 	.db	0
      000F55 00 00 04 CF           5789 	.dw	0,1231
      000F59 55 41 52 54 31 5F 52  5790 	.ascii "UART1_ReceiverWakeUpCmd"
             65 63 65 69 76 65 72
             57 61 6B 65 55 70 43
             6D 64
      000F70 00                    5791 	.db	0
      000F71 00 00 05 1D           5792 	.dw	0,1309
      000F75 55 41 52 54 31 5F 52  5793 	.ascii "UART1_ReceiveData8"
             65 63 65 69 76 65 44
             61 74 61 38
      000F87 00                    5794 	.db	0
      000F88 00 00 05 52           5795 	.dw	0,1362
      000F8C 55 41 52 54 31 5F 52  5796 	.ascii "UART1_ReceiveData9"
             65 63 65 69 76 65 44
             61 74 61 39
      000F9E 00                    5797 	.db	0
      000F9F 00 00 05 89           5798 	.dw	0,1417
      000FA3 55 41 52 54 31 5F 53  5799 	.ascii "UART1_SendData8"
             65 6E 64 44 61 74 61
             38
      000FB2 00                    5800 	.db	0
      000FB3 00 00 05 B9           5801 	.dw	0,1465
      000FB7 55 41 52 54 31 5F 53  5802 	.ascii "UART1_SendData9"
             65 6E 64 44 61 74 61
             39
      000FC6 00                    5803 	.db	0
      000FC7 00 00 05 E9           5804 	.dw	0,1513
      000FCB 55 41 52 54 31 5F 53  5805 	.ascii "UART1_SendBreak"
             65 6E 64 42 72 65 61
             6B
      000FDA 00                    5806 	.db	0
      000FDB 00 00 06 07           5807 	.dw	0,1543
      000FDF 55 41 52 54 31 5F 53  5808 	.ascii "UART1_SetAddress"
             65 74 41 64 64 72 65
             73 73
      000FEF 00                    5809 	.db	0
      000FF0 00 00 06 41           5810 	.dw	0,1601
      000FF4 55 41 52 54 31 5F 53  5811 	.ascii "UART1_SetGuardTime"
             65 74 47 75 61 72 64
             54 69 6D 65
      001006 00                    5812 	.db	0
      001007 00 00 06 7F           5813 	.dw	0,1663
      00100B 55 41 52 54 31 5F 53  5814 	.ascii "UART1_SetPrescaler"
             65 74 50 72 65 73 63
             61 6C 65 72
      00101D 00                    5815 	.db	0
      00101E 00 00 06 BD           5816 	.dw	0,1725
      001022 55 41 52 54 31 5F 47  5817 	.ascii "UART1_GetFlagStatus"
             65 74 46 6C 61 67 53
             74 61 74 75 73
      001035 00                    5818 	.db	0
      001036 00 00 07 5D           5819 	.dw	0,1885
      00103A 55 41 52 54 31 5F 43  5820 	.ascii "UART1_ClearFlag"
             6C 65 61 72 46 6C 61
             67
      001049 00                    5821 	.db	0
      00104A 00 00 07 A5           5822 	.dw	0,1957
      00104E 55 41 52 54 31 5F 47  5823 	.ascii "UART1_GetITStatus"
             65 74 49 54 53 74 61
             74 75 73
      00105F 00                    5824 	.db	0
      001060 00 00 08 8C           5825 	.dw	0,2188
      001064 55 41 52 54 31 5F 43  5826 	.ascii "UART1_ClearITPendingBit"
             6C 65 61 72 49 54 50
             65 6E 64 69 6E 67 42
             69 74
      00107B 00                    5827 	.db	0
      00107C 00 00 00 00           5828 	.dw	0,0
      001080                       5829 Ldebug_pubnames_end:
                                   5830 
                                   5831 	.area .debug_frame (NOLOAD)
      00412C 00 00                 5832 	.dw	0
      00412E 00 0E                 5833 	.dw	Ldebug_CIE0_end-Ldebug_CIE0_start
      004130                       5834 Ldebug_CIE0_start:
      004130 FF FF                 5835 	.dw	0xffff
      004132 FF FF                 5836 	.dw	0xffff
      004134 01                    5837 	.db	1
      004135 00                    5838 	.db	0
      004136 01                    5839 	.uleb128	1
      004137 7F                    5840 	.sleb128	-1
      004138 09                    5841 	.db	9
      004139 0C                    5842 	.db	12
      00413A 08                    5843 	.uleb128	8
      00413B 02                    5844 	.uleb128	2
      00413C 89                    5845 	.db	137
      00413D 01                    5846 	.uleb128	1
      00413E                       5847 Ldebug_CIE0_end:
      00413E 00 00 00 59           5848 	.dw	0,89
      004142 00 00 41 2C           5849 	.dw	0,(Ldebug_CIE0_start-4)
      004146 00 00 AA B3           5850 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$630)	;initial loc
      00414A 00 00 00 32           5851 	.dw	0,Sstm8s_uart1$UART1_ClearITPendingBit$651-Sstm8s_uart1$UART1_ClearITPendingBit$630
      00414E 01                    5852 	.db	1
      00414F 00 00 AA B3           5853 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$630)
      004153 0E                    5854 	.db	14
      004154 02                    5855 	.uleb128	2
      004155 01                    5856 	.db	1
      004156 00 00 AA BE           5857 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$632)
      00415A 0E                    5858 	.db	14
      00415B 02                    5859 	.uleb128	2
      00415C 01                    5860 	.db	1
      00415D 00 00 AA C6           5861 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$633)
      004161 0E                    5862 	.db	14
      004162 02                    5863 	.uleb128	2
      004163 01                    5864 	.db	1
      004164 00 00 AA C7           5865 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$634)
      004168 0E                    5866 	.db	14
      004169 03                    5867 	.uleb128	3
      00416A 01                    5868 	.db	1
      00416B 00 00 AA C9           5869 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$635)
      00416F 0E                    5870 	.db	14
      004170 04                    5871 	.uleb128	4
      004171 01                    5872 	.db	1
      004172 00 00 AA CB           5873 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$636)
      004176 0E                    5874 	.db	14
      004177 05                    5875 	.uleb128	5
      004178 01                    5876 	.db	1
      004179 00 00 AA CD           5877 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$637)
      00417D 0E                    5878 	.db	14
      00417E 07                    5879 	.uleb128	7
      00417F 01                    5880 	.db	1
      004180 00 00 AA CF           5881 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$638)
      004184 0E                    5882 	.db	14
      004185 08                    5883 	.uleb128	8
      004186 01                    5884 	.db	1
      004187 00 00 AA D1           5885 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$639)
      00418B 0E                    5886 	.db	14
      00418C 09                    5887 	.uleb128	9
      00418D 01                    5888 	.db	1
      00418E 00 00 AA D6           5889 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$640)
      004192 0E                    5890 	.db	14
      004193 03                    5891 	.uleb128	3
      004194 01                    5892 	.db	1
      004195 00 00 AA D7           5893 	.dw	0,(Sstm8s_uart1$UART1_ClearITPendingBit$641)
      004199 0E                    5894 	.db	14
      00419A 02                    5895 	.uleb128	2
                                   5896 
                                   5897 	.area .debug_frame (NOLOAD)
      00419B 00 00                 5898 	.dw	0
      00419D 00 0E                 5899 	.dw	Ldebug_CIE1_end-Ldebug_CIE1_start
      00419F                       5900 Ldebug_CIE1_start:
      00419F FF FF                 5901 	.dw	0xffff
      0041A1 FF FF                 5902 	.dw	0xffff
      0041A3 01                    5903 	.db	1
      0041A4 00                    5904 	.db	0
      0041A5 01                    5905 	.uleb128	1
      0041A6 7F                    5906 	.sleb128	-1
      0041A7 09                    5907 	.db	9
      0041A8 0C                    5908 	.db	12
      0041A9 08                    5909 	.uleb128	8
      0041AA 02                    5910 	.uleb128	2
      0041AB 89                    5911 	.db	137
      0041AC 01                    5912 	.uleb128	1
      0041AD                       5913 Ldebug_CIE1_end:
      0041AD 00 00 00 98           5914 	.dw	0,152
      0041B1 00 00 41 9B           5915 	.dw	0,(Ldebug_CIE1_start-4)
      0041B5 00 00 A9 EF           5916 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$572)	;initial loc
      0041B9 00 00 00 C4           5917 	.dw	0,Sstm8s_uart1$UART1_GetITStatus$628-Sstm8s_uart1$UART1_GetITStatus$572
      0041BD 01                    5918 	.db	1
      0041BE 00 00 A9 EF           5919 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$572)
      0041C2 0E                    5920 	.db	14
      0041C3 02                    5921 	.uleb128	2
      0041C4 01                    5922 	.db	1
      0041C5 00 00 A9 F1           5923 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$573)
      0041C9 0E                    5924 	.db	14
      0041CA 06                    5925 	.uleb128	6
      0041CB 01                    5926 	.db	1
      0041CC 00 00 A9 FF           5927 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$575)
      0041D0 0E                    5928 	.db	14
      0041D1 06                    5929 	.uleb128	6
      0041D2 01                    5930 	.db	1
      0041D3 00 00 AA 0B           5931 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$576)
      0041D7 0E                    5932 	.db	14
      0041D8 06                    5933 	.uleb128	6
      0041D9 01                    5934 	.db	1
      0041DA 00 00 AA 10           5935 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$577)
      0041DE 0E                    5936 	.db	14
      0041DF 06                    5937 	.uleb128	6
      0041E0 01                    5938 	.db	1
      0041E1 00 00 AA 15           5939 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$578)
      0041E5 0E                    5940 	.db	14
      0041E6 06                    5941 	.uleb128	6
      0041E7 01                    5942 	.db	1
      0041E8 00 00 AA 1A           5943 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$579)
      0041EC 0E                    5944 	.db	14
      0041ED 06                    5945 	.uleb128	6
      0041EE 01                    5946 	.db	1
      0041EF 00 00 AA 1F           5947 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$580)
      0041F3 0E                    5948 	.db	14
      0041F4 06                    5949 	.uleb128	6
      0041F5 01                    5950 	.db	1
      0041F6 00 00 AA 24           5951 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$581)
      0041FA 0E                    5952 	.db	14
      0041FB 06                    5953 	.uleb128	6
      0041FC 01                    5954 	.db	1
      0041FD 00 00 AA 2E           5955 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$582)
      004201 0E                    5956 	.db	14
      004202 07                    5957 	.uleb128	7
      004203 01                    5958 	.db	1
      004204 00 00 AA 30           5959 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$583)
      004208 0E                    5960 	.db	14
      004209 08                    5961 	.uleb128	8
      00420A 01                    5962 	.db	1
      00420B 00 00 AA 32           5963 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$584)
      00420F 0E                    5964 	.db	14
      004210 0A                    5965 	.uleb128	10
      004211 01                    5966 	.db	1
      004212 00 00 AA 34           5967 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$585)
      004216 0E                    5968 	.db	14
      004217 0B                    5969 	.uleb128	11
      004218 01                    5970 	.db	1
      004219 00 00 AA 36           5971 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$586)
      00421D 0E                    5972 	.db	14
      00421E 0C                    5973 	.uleb128	12
      00421F 01                    5974 	.db	1
      004220 00 00 AA 3B           5975 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$587)
      004224 0E                    5976 	.db	14
      004225 06                    5977 	.uleb128	6
      004226 01                    5978 	.db	1
      004227 00 00 AA 41           5979 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$589)
      00422B 0E                    5980 	.db	14
      00422C 07                    5981 	.uleb128	7
      00422D 01                    5982 	.db	1
      00422E 00 00 AA 46           5983 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$590)
      004232 0E                    5984 	.db	14
      004233 06                    5985 	.uleb128	6
      004234 01                    5986 	.db	1
      004235 00 00 AA 53           5987 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$593)
      004239 0E                    5988 	.db	14
      00423A 07                    5989 	.uleb128	7
      00423B 01                    5990 	.db	1
      00423C 00 00 AA 58           5991 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$594)
      004240 0E                    5992 	.db	14
      004241 06                    5993 	.uleb128	6
      004242 01                    5994 	.db	1
      004243 00 00 AA B2           5995 	.dw	0,(Sstm8s_uart1$UART1_GetITStatus$626)
      004247 0E                    5996 	.db	14
      004248 02                    5997 	.uleb128	2
                                   5998 
                                   5999 	.area .debug_frame (NOLOAD)
      004249 00 00                 6000 	.dw	0
      00424B 00 0E                 6001 	.dw	Ldebug_CIE2_end-Ldebug_CIE2_start
      00424D                       6002 Ldebug_CIE2_start:
      00424D FF FF                 6003 	.dw	0xffff
      00424F FF FF                 6004 	.dw	0xffff
      004251 01                    6005 	.db	1
      004252 00                    6006 	.db	0
      004253 01                    6007 	.uleb128	1
      004254 7F                    6008 	.sleb128	-1
      004255 09                    6009 	.db	9
      004256 0C                    6010 	.db	12
      004257 08                    6011 	.uleb128	8
      004258 02                    6012 	.uleb128	2
      004259 89                    6013 	.db	137
      00425A 01                    6014 	.uleb128	1
      00425B                       6015 Ldebug_CIE2_end:
      00425B 00 00 00 59           6016 	.dw	0,89
      00425F 00 00 42 49           6017 	.dw	0,(Ldebug_CIE2_start-4)
      004263 00 00 A9 BD           6018 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$549)	;initial loc
      004267 00 00 00 32           6019 	.dw	0,Sstm8s_uart1$UART1_ClearFlag$570-Sstm8s_uart1$UART1_ClearFlag$549
      00426B 01                    6020 	.db	1
      00426C 00 00 A9 BD           6021 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$549)
      004270 0E                    6022 	.db	14
      004271 02                    6023 	.uleb128	2
      004272 01                    6024 	.db	1
      004273 00 00 A9 C8           6025 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$551)
      004277 0E                    6026 	.db	14
      004278 02                    6027 	.uleb128	2
      004279 01                    6028 	.db	1
      00427A 00 00 A9 D0           6029 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$552)
      00427E 0E                    6030 	.db	14
      00427F 02                    6031 	.uleb128	2
      004280 01                    6032 	.db	1
      004281 00 00 A9 D1           6033 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$553)
      004285 0E                    6034 	.db	14
      004286 03                    6035 	.uleb128	3
      004287 01                    6036 	.db	1
      004288 00 00 A9 D3           6037 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$554)
      00428C 0E                    6038 	.db	14
      00428D 04                    6039 	.uleb128	4
      00428E 01                    6040 	.db	1
      00428F 00 00 A9 D5           6041 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$555)
      004293 0E                    6042 	.db	14
      004294 05                    6043 	.uleb128	5
      004295 01                    6044 	.db	1
      004296 00 00 A9 D7           6045 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$556)
      00429A 0E                    6046 	.db	14
      00429B 07                    6047 	.uleb128	7
      00429C 01                    6048 	.db	1
      00429D 00 00 A9 D9           6049 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$557)
      0042A1 0E                    6050 	.db	14
      0042A2 08                    6051 	.uleb128	8
      0042A3 01                    6052 	.db	1
      0042A4 00 00 A9 DB           6053 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$558)
      0042A8 0E                    6054 	.db	14
      0042A9 09                    6055 	.uleb128	9
      0042AA 01                    6056 	.db	1
      0042AB 00 00 A9 E0           6057 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$559)
      0042AF 0E                    6058 	.db	14
      0042B0 03                    6059 	.uleb128	3
      0042B1 01                    6060 	.db	1
      0042B2 00 00 A9 E1           6061 	.dw	0,(Sstm8s_uart1$UART1_ClearFlag$560)
      0042B6 0E                    6062 	.db	14
      0042B7 02                    6063 	.uleb128	2
                                   6064 
                                   6065 	.area .debug_frame (NOLOAD)
      0042B8 00 00                 6066 	.dw	0
      0042BA 00 0E                 6067 	.dw	Ldebug_CIE3_end-Ldebug_CIE3_start
      0042BC                       6068 Ldebug_CIE3_start:
      0042BC FF FF                 6069 	.dw	0xffff
      0042BE FF FF                 6070 	.dw	0xffff
      0042C0 01                    6071 	.db	1
      0042C1 00                    6072 	.db	0
      0042C2 01                    6073 	.uleb128	1
      0042C3 7F                    6074 	.sleb128	-1
      0042C4 09                    6075 	.db	9
      0042C5 0C                    6076 	.db	12
      0042C6 08                    6077 	.uleb128	8
      0042C7 02                    6078 	.uleb128	2
      0042C8 89                    6079 	.db	137
      0042C9 01                    6080 	.uleb128	1
      0042CA                       6081 Ldebug_CIE3_end:
      0042CA 00 00 00 AD           6082 	.dw	0,173
      0042CE 00 00 42 B8           6083 	.dw	0,(Ldebug_CIE3_start-4)
      0042D2 00 00 A9 29           6084 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$493)	;initial loc
      0042D6 00 00 00 94           6085 	.dw	0,Sstm8s_uart1$UART1_GetFlagStatus$547-Sstm8s_uart1$UART1_GetFlagStatus$493
      0042DA 01                    6086 	.db	1
      0042DB 00 00 A9 29           6087 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$493)
      0042DF 0E                    6088 	.db	14
      0042E0 02                    6089 	.uleb128	2
      0042E1 01                    6090 	.db	1
      0042E2 00 00 A9 2A           6091 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$494)
      0042E6 0E                    6092 	.db	14
      0042E7 04                    6093 	.uleb128	4
      0042E8 01                    6094 	.db	1
      0042E9 00 00 A9 38           6095 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$496)
      0042ED 0E                    6096 	.db	14
      0042EE 04                    6097 	.uleb128	4
      0042EF 01                    6098 	.db	1
      0042F0 00 00 A9 41           6099 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$497)
      0042F4 0E                    6100 	.db	14
      0042F5 04                    6101 	.uleb128	4
      0042F6 01                    6102 	.db	1
      0042F7 00 00 A9 49           6103 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$498)
      0042FB 0E                    6104 	.db	14
      0042FC 04                    6105 	.uleb128	4
      0042FD 01                    6106 	.db	1
      0042FE 00 00 A9 51           6107 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$499)
      004302 0E                    6108 	.db	14
      004303 04                    6109 	.uleb128	4
      004304 01                    6110 	.db	1
      004305 00 00 A9 56           6111 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$500)
      004309 0E                    6112 	.db	14
      00430A 04                    6113 	.uleb128	4
      00430B 01                    6114 	.db	1
      00430C 00 00 A9 5B           6115 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$501)
      004310 0E                    6116 	.db	14
      004311 04                    6117 	.uleb128	4
      004312 01                    6118 	.db	1
      004313 00 00 A9 60           6119 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$502)
      004317 0E                    6120 	.db	14
      004318 04                    6121 	.uleb128	4
      004319 01                    6122 	.db	1
      00431A 00 00 A9 65           6123 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$503)
      00431E 0E                    6124 	.db	14
      00431F 04                    6125 	.uleb128	4
      004320 01                    6126 	.db	1
      004321 00 00 A9 6A           6127 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$504)
      004325 0E                    6128 	.db	14
      004326 04                    6129 	.uleb128	4
      004327 01                    6130 	.db	1
      004328 00 00 A9 6D           6131 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$505)
      00432C 0E                    6132 	.db	14
      00432D 04                    6133 	.uleb128	4
      00432E 01                    6134 	.db	1
      00432F 00 00 A9 75           6135 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$506)
      004333 0E                    6136 	.db	14
      004334 05                    6137 	.uleb128	5
      004335 01                    6138 	.db	1
      004336 00 00 A9 77           6139 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$507)
      00433A 0E                    6140 	.db	14
      00433B 06                    6141 	.uleb128	6
      00433C 01                    6142 	.db	1
      00433D 00 00 A9 79           6143 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$508)
      004341 0E                    6144 	.db	14
      004342 07                    6145 	.uleb128	7
      004343 01                    6146 	.db	1
      004344 00 00 A9 7B           6147 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$509)
      004348 0E                    6148 	.db	14
      004349 09                    6149 	.uleb128	9
      00434A 01                    6150 	.db	1
      00434B 00 00 A9 7D           6151 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$510)
      00434F 0E                    6152 	.db	14
      004350 0A                    6153 	.uleb128	10
      004351 01                    6154 	.db	1
      004352 00 00 A9 7F           6155 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$511)
      004356 0E                    6156 	.db	14
      004357 0B                    6157 	.uleb128	11
      004358 01                    6158 	.db	1
      004359 00 00 A9 84           6159 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$512)
      00435D 0E                    6160 	.db	14
      00435E 05                    6161 	.uleb128	5
      00435F 01                    6162 	.db	1
      004360 00 00 A9 85           6163 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$513)
      004364 0E                    6164 	.db	14
      004365 04                    6165 	.uleb128	4
      004366 01                    6166 	.db	1
      004367 00 00 A9 86           6167 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$515)
      00436B 0E                    6168 	.db	14
      00436C 05                    6169 	.uleb128	5
      00436D 01                    6170 	.db	1
      00436E 00 00 A9 8B           6171 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$516)
      004372 0E                    6172 	.db	14
      004373 04                    6173 	.uleb128	4
      004374 01                    6174 	.db	1
      004375 00 00 A9 BC           6175 	.dw	0,(Sstm8s_uart1$UART1_GetFlagStatus$545)
      004379 0E                    6176 	.db	14
      00437A 02                    6177 	.uleb128	2
                                   6178 
                                   6179 	.area .debug_frame (NOLOAD)
      00437B 00 00                 6180 	.dw	0
      00437D 00 0E                 6181 	.dw	Ldebug_CIE4_end-Ldebug_CIE4_start
      00437F                       6182 Ldebug_CIE4_start:
      00437F FF FF                 6183 	.dw	0xffff
      004381 FF FF                 6184 	.dw	0xffff
      004383 01                    6185 	.db	1
      004384 00                    6186 	.db	0
      004385 01                    6187 	.uleb128	1
      004386 7F                    6188 	.sleb128	-1
      004387 09                    6189 	.db	9
      004388 0C                    6190 	.db	12
      004389 08                    6191 	.uleb128	8
      00438A 02                    6192 	.uleb128	2
      00438B 89                    6193 	.db	137
      00438C 01                    6194 	.uleb128	1
      00438D                       6195 Ldebug_CIE4_end:
      00438D 00 00 00 13           6196 	.dw	0,19
      004391 00 00 43 7B           6197 	.dw	0,(Ldebug_CIE4_start-4)
      004395 00 00 A9 22           6198 	.dw	0,(Sstm8s_uart1$UART1_SetPrescaler$487)	;initial loc
      004399 00 00 00 07           6199 	.dw	0,Sstm8s_uart1$UART1_SetPrescaler$491-Sstm8s_uart1$UART1_SetPrescaler$487
      00439D 01                    6200 	.db	1
      00439E 00 00 A9 22           6201 	.dw	0,(Sstm8s_uart1$UART1_SetPrescaler$487)
      0043A2 0E                    6202 	.db	14
      0043A3 02                    6203 	.uleb128	2
                                   6204 
                                   6205 	.area .debug_frame (NOLOAD)
      0043A4 00 00                 6206 	.dw	0
      0043A6 00 0E                 6207 	.dw	Ldebug_CIE5_end-Ldebug_CIE5_start
      0043A8                       6208 Ldebug_CIE5_start:
      0043A8 FF FF                 6209 	.dw	0xffff
      0043AA FF FF                 6210 	.dw	0xffff
      0043AC 01                    6211 	.db	1
      0043AD 00                    6212 	.db	0
      0043AE 01                    6213 	.uleb128	1
      0043AF 7F                    6214 	.sleb128	-1
      0043B0 09                    6215 	.db	9
      0043B1 0C                    6216 	.db	12
      0043B2 08                    6217 	.uleb128	8
      0043B3 02                    6218 	.uleb128	2
      0043B4 89                    6219 	.db	137
      0043B5 01                    6220 	.uleb128	1
      0043B6                       6221 Ldebug_CIE5_end:
      0043B6 00 00 00 13           6222 	.dw	0,19
      0043BA 00 00 43 A4           6223 	.dw	0,(Ldebug_CIE5_start-4)
      0043BE 00 00 A9 1B           6224 	.dw	0,(Sstm8s_uart1$UART1_SetGuardTime$481)	;initial loc
      0043C2 00 00 00 07           6225 	.dw	0,Sstm8s_uart1$UART1_SetGuardTime$485-Sstm8s_uart1$UART1_SetGuardTime$481
      0043C6 01                    6226 	.db	1
      0043C7 00 00 A9 1B           6227 	.dw	0,(Sstm8s_uart1$UART1_SetGuardTime$481)
      0043CB 0E                    6228 	.db	14
      0043CC 02                    6229 	.uleb128	2
                                   6230 
                                   6231 	.area .debug_frame (NOLOAD)
      0043CD 00 00                 6232 	.dw	0
      0043CF 00 0E                 6233 	.dw	Ldebug_CIE6_end-Ldebug_CIE6_start
      0043D1                       6234 Ldebug_CIE6_start:
      0043D1 FF FF                 6235 	.dw	0xffff
      0043D3 FF FF                 6236 	.dw	0xffff
      0043D5 01                    6237 	.db	1
      0043D6 00                    6238 	.db	0
      0043D7 01                    6239 	.uleb128	1
      0043D8 7F                    6240 	.sleb128	-1
      0043D9 09                    6241 	.db	9
      0043DA 0C                    6242 	.db	12
      0043DB 08                    6243 	.uleb128	8
      0043DC 02                    6244 	.uleb128	2
      0043DD 89                    6245 	.db	137
      0043DE 01                    6246 	.uleb128	1
      0043DF                       6247 Ldebug_CIE6_end:
      0043DF 00 00 00 3D           6248 	.dw	0,61
      0043E3 00 00 43 CD           6249 	.dw	0,(Ldebug_CIE6_start-4)
      0043E7 00 00 A8 F5           6250 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$467)	;initial loc
      0043EB 00 00 00 26           6251 	.dw	0,Sstm8s_uart1$UART1_SetAddress$479-Sstm8s_uart1$UART1_SetAddress$467
      0043EF 01                    6252 	.db	1
      0043F0 00 00 A8 F5           6253 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$467)
      0043F4 0E                    6254 	.db	14
      0043F5 02                    6255 	.uleb128	2
      0043F6 01                    6256 	.db	1
      0043F7 00 00 A8 FD           6257 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$469)
      0043FB 0E                    6258 	.db	14
      0043FC 03                    6259 	.uleb128	3
      0043FD 01                    6260 	.db	1
      0043FE 00 00 A8 FF           6261 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$470)
      004402 0E                    6262 	.db	14
      004403 04                    6263 	.uleb128	4
      004404 01                    6264 	.db	1
      004405 00 00 A9 01           6265 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$471)
      004409 0E                    6266 	.db	14
      00440A 06                    6267 	.uleb128	6
      00440B 01                    6268 	.db	1
      00440C 00 00 A9 03           6269 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$472)
      004410 0E                    6270 	.db	14
      004411 07                    6271 	.uleb128	7
      004412 01                    6272 	.db	1
      004413 00 00 A9 05           6273 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$473)
      004417 0E                    6274 	.db	14
      004418 08                    6275 	.uleb128	8
      004419 01                    6276 	.db	1
      00441A 00 00 A9 0A           6277 	.dw	0,(Sstm8s_uart1$UART1_SetAddress$474)
      00441E 0E                    6278 	.db	14
      00441F 02                    6279 	.uleb128	2
                                   6280 
                                   6281 	.area .debug_frame (NOLOAD)
      004420 00 00                 6282 	.dw	0
      004422 00 0E                 6283 	.dw	Ldebug_CIE7_end-Ldebug_CIE7_start
      004424                       6284 Ldebug_CIE7_start:
      004424 FF FF                 6285 	.dw	0xffff
      004426 FF FF                 6286 	.dw	0xffff
      004428 01                    6287 	.db	1
      004429 00                    6288 	.db	0
      00442A 01                    6289 	.uleb128	1
      00442B 7F                    6290 	.sleb128	-1
      00442C 09                    6291 	.db	9
      00442D 0C                    6292 	.db	12
      00442E 08                    6293 	.uleb128	8
      00442F 02                    6294 	.uleb128	2
      004430 89                    6295 	.db	137
      004431 01                    6296 	.uleb128	1
      004432                       6297 Ldebug_CIE7_end:
      004432 00 00 00 13           6298 	.dw	0,19
      004436 00 00 44 20           6299 	.dw	0,(Ldebug_CIE7_start-4)
      00443A 00 00 A8 F0           6300 	.dw	0,(Sstm8s_uart1$UART1_SendBreak$461)	;initial loc
      00443E 00 00 00 05           6301 	.dw	0,Sstm8s_uart1$UART1_SendBreak$465-Sstm8s_uart1$UART1_SendBreak$461
      004442 01                    6302 	.db	1
      004443 00 00 A8 F0           6303 	.dw	0,(Sstm8s_uart1$UART1_SendBreak$461)
      004447 0E                    6304 	.db	14
      004448 02                    6305 	.uleb128	2
                                   6306 
                                   6307 	.area .debug_frame (NOLOAD)
      004449 00 00                 6308 	.dw	0
      00444B 00 0E                 6309 	.dw	Ldebug_CIE8_end-Ldebug_CIE8_start
      00444D                       6310 Ldebug_CIE8_start:
      00444D FF FF                 6311 	.dw	0xffff
      00444F FF FF                 6312 	.dw	0xffff
      004451 01                    6313 	.db	1
      004452 00                    6314 	.db	0
      004453 01                    6315 	.uleb128	1
      004454 7F                    6316 	.sleb128	-1
      004455 09                    6317 	.db	9
      004456 0C                    6318 	.db	12
      004457 08                    6319 	.uleb128	8
      004458 02                    6320 	.uleb128	2
      004459 89                    6321 	.db	137
      00445A 01                    6322 	.uleb128	1
      00445B                       6323 Ldebug_CIE8_end:
      00445B 00 00 00 21           6324 	.dw	0,33
      00445F 00 00 44 49           6325 	.dw	0,(Ldebug_CIE8_start-4)
      004463 00 00 A8 D3           6326 	.dw	0,(Sstm8s_uart1$UART1_SendData9$451)	;initial loc
      004467 00 00 00 1D           6327 	.dw	0,Sstm8s_uart1$UART1_SendData9$459-Sstm8s_uart1$UART1_SendData9$451
      00446B 01                    6328 	.db	1
      00446C 00 00 A8 D3           6329 	.dw	0,(Sstm8s_uart1$UART1_SendData9$451)
      004470 0E                    6330 	.db	14
      004471 02                    6331 	.uleb128	2
      004472 01                    6332 	.db	1
      004473 00 00 A8 D4           6333 	.dw	0,(Sstm8s_uart1$UART1_SendData9$452)
      004477 0E                    6334 	.db	14
      004478 03                    6335 	.uleb128	3
      004479 01                    6336 	.db	1
      00447A 00 00 A8 EF           6337 	.dw	0,(Sstm8s_uart1$UART1_SendData9$457)
      00447E 0E                    6338 	.db	14
      00447F 02                    6339 	.uleb128	2
                                   6340 
                                   6341 	.area .debug_frame (NOLOAD)
      004480 00 00                 6342 	.dw	0
      004482 00 0E                 6343 	.dw	Ldebug_CIE9_end-Ldebug_CIE9_start
      004484                       6344 Ldebug_CIE9_start:
      004484 FF FF                 6345 	.dw	0xffff
      004486 FF FF                 6346 	.dw	0xffff
      004488 01                    6347 	.db	1
      004489 00                    6348 	.db	0
      00448A 01                    6349 	.uleb128	1
      00448B 7F                    6350 	.sleb128	-1
      00448C 09                    6351 	.db	9
      00448D 0C                    6352 	.db	12
      00448E 08                    6353 	.uleb128	8
      00448F 02                    6354 	.uleb128	2
      004490 89                    6355 	.db	137
      004491 01                    6356 	.uleb128	1
      004492                       6357 Ldebug_CIE9_end:
      004492 00 00 00 13           6358 	.dw	0,19
      004496 00 00 44 80           6359 	.dw	0,(Ldebug_CIE9_start-4)
      00449A 00 00 A8 CC           6360 	.dw	0,(Sstm8s_uart1$UART1_SendData8$445)	;initial loc
      00449E 00 00 00 07           6361 	.dw	0,Sstm8s_uart1$UART1_SendData8$449-Sstm8s_uart1$UART1_SendData8$445
      0044A2 01                    6362 	.db	1
      0044A3 00 00 A8 CC           6363 	.dw	0,(Sstm8s_uart1$UART1_SendData8$445)
      0044A7 0E                    6364 	.db	14
      0044A8 02                    6365 	.uleb128	2
                                   6366 
                                   6367 	.area .debug_frame (NOLOAD)
      0044A9 00 00                 6368 	.dw	0
      0044AB 00 0E                 6369 	.dw	Ldebug_CIE10_end-Ldebug_CIE10_start
      0044AD                       6370 Ldebug_CIE10_start:
      0044AD FF FF                 6371 	.dw	0xffff
      0044AF FF FF                 6372 	.dw	0xffff
      0044B1 01                    6373 	.db	1
      0044B2 00                    6374 	.db	0
      0044B3 01                    6375 	.uleb128	1
      0044B4 7F                    6376 	.sleb128	-1
      0044B5 09                    6377 	.db	9
      0044B6 0C                    6378 	.db	12
      0044B7 08                    6379 	.uleb128	8
      0044B8 02                    6380 	.uleb128	2
      0044B9 89                    6381 	.db	137
      0044BA 01                    6382 	.uleb128	1
      0044BB                       6383 Ldebug_CIE10_end:
      0044BB 00 00 00 21           6384 	.dw	0,33
      0044BF 00 00 44 A9           6385 	.dw	0,(Ldebug_CIE10_start-4)
      0044C3 00 00 A8 B1           6386 	.dw	0,(Sstm8s_uart1$UART1_ReceiveData9$436)	;initial loc
      0044C7 00 00 00 1B           6387 	.dw	0,Sstm8s_uart1$UART1_ReceiveData9$443-Sstm8s_uart1$UART1_ReceiveData9$436
      0044CB 01                    6388 	.db	1
      0044CC 00 00 A8 B1           6389 	.dw	0,(Sstm8s_uart1$UART1_ReceiveData9$436)
      0044D0 0E                    6390 	.db	14
      0044D1 02                    6391 	.uleb128	2
      0044D2 01                    6392 	.db	1
      0044D3 00 00 A8 B2           6393 	.dw	0,(Sstm8s_uart1$UART1_ReceiveData9$437)
      0044D7 0E                    6394 	.db	14
      0044D8 04                    6395 	.uleb128	4
      0044D9 01                    6396 	.db	1
      0044DA 00 00 A8 CB           6397 	.dw	0,(Sstm8s_uart1$UART1_ReceiveData9$441)
      0044DE 0E                    6398 	.db	14
      0044DF 02                    6399 	.uleb128	2
                                   6400 
                                   6401 	.area .debug_frame (NOLOAD)
      0044E0 00 00                 6402 	.dw	0
      0044E2 00 0E                 6403 	.dw	Ldebug_CIE11_end-Ldebug_CIE11_start
      0044E4                       6404 Ldebug_CIE11_start:
      0044E4 FF FF                 6405 	.dw	0xffff
      0044E6 FF FF                 6406 	.dw	0xffff
      0044E8 01                    6407 	.db	1
      0044E9 00                    6408 	.db	0
      0044EA 01                    6409 	.uleb128	1
      0044EB 7F                    6410 	.sleb128	-1
      0044EC 09                    6411 	.db	9
      0044ED 0C                    6412 	.db	12
      0044EE 08                    6413 	.uleb128	8
      0044EF 02                    6414 	.uleb128	2
      0044F0 89                    6415 	.db	137
      0044F1 01                    6416 	.uleb128	1
      0044F2                       6417 Ldebug_CIE11_end:
      0044F2 00 00 00 13           6418 	.dw	0,19
      0044F6 00 00 44 E0           6419 	.dw	0,(Ldebug_CIE11_start-4)
      0044FA 00 00 A8 AD           6420 	.dw	0,(Sstm8s_uart1$UART1_ReceiveData8$430)	;initial loc
      0044FE 00 00 00 04           6421 	.dw	0,Sstm8s_uart1$UART1_ReceiveData8$434-Sstm8s_uart1$UART1_ReceiveData8$430
      004502 01                    6422 	.db	1
      004503 00 00 A8 AD           6423 	.dw	0,(Sstm8s_uart1$UART1_ReceiveData8$430)
      004507 0E                    6424 	.db	14
      004508 02                    6425 	.uleb128	2
                                   6426 
                                   6427 	.area .debug_frame (NOLOAD)
      004509 00 00                 6428 	.dw	0
      00450B 00 0E                 6429 	.dw	Ldebug_CIE12_end-Ldebug_CIE12_start
      00450D                       6430 Ldebug_CIE12_start:
      00450D FF FF                 6431 	.dw	0xffff
      00450F FF FF                 6432 	.dw	0xffff
      004511 01                    6433 	.db	1
      004512 00                    6434 	.db	0
      004513 01                    6435 	.uleb128	1
      004514 7F                    6436 	.sleb128	-1
      004515 09                    6437 	.db	9
      004516 0C                    6438 	.db	12
      004517 08                    6439 	.uleb128	8
      004518 02                    6440 	.uleb128	2
      004519 89                    6441 	.db	137
      00451A 01                    6442 	.uleb128	1
      00451B                       6443 Ldebug_CIE12_end:
      00451B 00 00 00 44           6444 	.dw	0,68
      00451F 00 00 45 09           6445 	.dw	0,(Ldebug_CIE12_start-4)
      004523 00 00 A8 81           6446 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$409)	;initial loc
      004527 00 00 00 2C           6447 	.dw	0,Sstm8s_uart1$UART1_ReceiverWakeUpCmd$428-Sstm8s_uart1$UART1_ReceiverWakeUpCmd$409
      00452B 01                    6448 	.db	1
      00452C 00 00 A8 81           6449 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$409)
      004530 0E                    6450 	.db	14
      004531 02                    6451 	.uleb128	2
      004532 01                    6452 	.db	1
      004533 00 00 A8 8A           6453 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$411)
      004537 0E                    6454 	.db	14
      004538 02                    6455 	.uleb128	2
      004539 01                    6456 	.db	1
      00453A 00 00 A8 8C           6457 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$412)
      00453E 0E                    6458 	.db	14
      00453F 03                    6459 	.uleb128	3
      004540 01                    6460 	.db	1
      004541 00 00 A8 8E           6461 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$413)
      004545 0E                    6462 	.db	14
      004546 04                    6463 	.uleb128	4
      004547 01                    6464 	.db	1
      004548 00 00 A8 90           6465 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$414)
      00454C 0E                    6466 	.db	14
      00454D 06                    6467 	.uleb128	6
      00454E 01                    6468 	.db	1
      00454F 00 00 A8 92           6469 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$415)
      004553 0E                    6470 	.db	14
      004554 07                    6471 	.uleb128	7
      004555 01                    6472 	.db	1
      004556 00 00 A8 94           6473 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$416)
      00455A 0E                    6474 	.db	14
      00455B 08                    6475 	.uleb128	8
      00455C 01                    6476 	.db	1
      00455D 00 00 A8 99           6477 	.dw	0,(Sstm8s_uart1$UART1_ReceiverWakeUpCmd$417)
      004561 0E                    6478 	.db	14
      004562 02                    6479 	.uleb128	2
                                   6480 
                                   6481 	.area .debug_frame (NOLOAD)
      004563 00 00                 6482 	.dw	0
      004565 00 0E                 6483 	.dw	Ldebug_CIE13_end-Ldebug_CIE13_start
      004567                       6484 Ldebug_CIE13_start:
      004567 FF FF                 6485 	.dw	0xffff
      004569 FF FF                 6486 	.dw	0xffff
      00456B 01                    6487 	.db	1
      00456C 00                    6488 	.db	0
      00456D 01                    6489 	.uleb128	1
      00456E 7F                    6490 	.sleb128	-1
      00456F 09                    6491 	.db	9
      004570 0C                    6492 	.db	12
      004571 08                    6493 	.uleb128	8
      004572 02                    6494 	.uleb128	2
      004573 89                    6495 	.db	137
      004574 01                    6496 	.uleb128	1
      004575                       6497 Ldebug_CIE13_end:
      004575 00 00 00 44           6498 	.dw	0,68
      004579 00 00 45 63           6499 	.dw	0,(Ldebug_CIE13_start-4)
      00457D 00 00 A8 5B           6500 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$394)	;initial loc
      004581 00 00 00 26           6501 	.dw	0,Sstm8s_uart1$UART1_WakeUpConfig$407-Sstm8s_uart1$UART1_WakeUpConfig$394
      004585 01                    6502 	.db	1
      004586 00 00 A8 5B           6503 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$394)
      00458A 0E                    6504 	.db	14
      00458B 02                    6505 	.uleb128	2
      00458C 01                    6506 	.db	1
      00458D 00 00 A8 65           6507 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$396)
      004591 0E                    6508 	.db	14
      004592 02                    6509 	.uleb128	2
      004593 01                    6510 	.db	1
      004594 00 00 A8 67           6511 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$397)
      004598 0E                    6512 	.db	14
      004599 03                    6513 	.uleb128	3
      00459A 01                    6514 	.db	1
      00459B 00 00 A8 69           6515 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$398)
      00459F 0E                    6516 	.db	14
      0045A0 04                    6517 	.uleb128	4
      0045A1 01                    6518 	.db	1
      0045A2 00 00 A8 6B           6519 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$399)
      0045A6 0E                    6520 	.db	14
      0045A7 06                    6521 	.uleb128	6
      0045A8 01                    6522 	.db	1
      0045A9 00 00 A8 6D           6523 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$400)
      0045AD 0E                    6524 	.db	14
      0045AE 07                    6525 	.uleb128	7
      0045AF 01                    6526 	.db	1
      0045B0 00 00 A8 6F           6527 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$401)
      0045B4 0E                    6528 	.db	14
      0045B5 08                    6529 	.uleb128	8
      0045B6 01                    6530 	.db	1
      0045B7 00 00 A8 74           6531 	.dw	0,(Sstm8s_uart1$UART1_WakeUpConfig$402)
      0045BB 0E                    6532 	.db	14
      0045BC 02                    6533 	.uleb128	2
                                   6534 
                                   6535 	.area .debug_frame (NOLOAD)
      0045BD 00 00                 6536 	.dw	0
      0045BF 00 0E                 6537 	.dw	Ldebug_CIE14_end-Ldebug_CIE14_start
      0045C1                       6538 Ldebug_CIE14_start:
      0045C1 FF FF                 6539 	.dw	0xffff
      0045C3 FF FF                 6540 	.dw	0xffff
      0045C5 01                    6541 	.db	1
      0045C6 00                    6542 	.db	0
      0045C7 01                    6543 	.uleb128	1
      0045C8 7F                    6544 	.sleb128	-1
      0045C9 09                    6545 	.db	9
      0045CA 0C                    6546 	.db	12
      0045CB 08                    6547 	.uleb128	8
      0045CC 02                    6548 	.uleb128	2
      0045CD 89                    6549 	.db	137
      0045CE 01                    6550 	.uleb128	1
      0045CF                       6551 Ldebug_CIE14_end:
      0045CF 00 00 00 44           6552 	.dw	0,68
      0045D3 00 00 45 BD           6553 	.dw	0,(Ldebug_CIE14_start-4)
      0045D7 00 00 A8 2F           6554 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$373)	;initial loc
      0045DB 00 00 00 2C           6555 	.dw	0,Sstm8s_uart1$UART1_SmartCardNACKCmd$392-Sstm8s_uart1$UART1_SmartCardNACKCmd$373
      0045DF 01                    6556 	.db	1
      0045E0 00 00 A8 2F           6557 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$373)
      0045E4 0E                    6558 	.db	14
      0045E5 02                    6559 	.uleb128	2
      0045E6 01                    6560 	.db	1
      0045E7 00 00 A8 38           6561 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$375)
      0045EB 0E                    6562 	.db	14
      0045EC 02                    6563 	.uleb128	2
      0045ED 01                    6564 	.db	1
      0045EE 00 00 A8 3A           6565 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$376)
      0045F2 0E                    6566 	.db	14
      0045F3 03                    6567 	.uleb128	3
      0045F4 01                    6568 	.db	1
      0045F5 00 00 A8 3C           6569 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$377)
      0045F9 0E                    6570 	.db	14
      0045FA 04                    6571 	.uleb128	4
      0045FB 01                    6572 	.db	1
      0045FC 00 00 A8 3E           6573 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$378)
      004600 0E                    6574 	.db	14
      004601 06                    6575 	.uleb128	6
      004602 01                    6576 	.db	1
      004603 00 00 A8 40           6577 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$379)
      004607 0E                    6578 	.db	14
      004608 07                    6579 	.uleb128	7
      004609 01                    6580 	.db	1
      00460A 00 00 A8 42           6581 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$380)
      00460E 0E                    6582 	.db	14
      00460F 08                    6583 	.uleb128	8
      004610 01                    6584 	.db	1
      004611 00 00 A8 47           6585 	.dw	0,(Sstm8s_uart1$UART1_SmartCardNACKCmd$381)
      004615 0E                    6586 	.db	14
      004616 02                    6587 	.uleb128	2
                                   6588 
                                   6589 	.area .debug_frame (NOLOAD)
      004617 00 00                 6590 	.dw	0
      004619 00 0E                 6591 	.dw	Ldebug_CIE15_end-Ldebug_CIE15_start
      00461B                       6592 Ldebug_CIE15_start:
      00461B FF FF                 6593 	.dw	0xffff
      00461D FF FF                 6594 	.dw	0xffff
      00461F 01                    6595 	.db	1
      004620 00                    6596 	.db	0
      004621 01                    6597 	.uleb128	1
      004622 7F                    6598 	.sleb128	-1
      004623 09                    6599 	.db	9
      004624 0C                    6600 	.db	12
      004625 08                    6601 	.uleb128	8
      004626 02                    6602 	.uleb128	2
      004627 89                    6603 	.db	137
      004628 01                    6604 	.uleb128	1
      004629                       6605 Ldebug_CIE15_end:
      004629 00 00 00 44           6606 	.dw	0,68
      00462D 00 00 46 17           6607 	.dw	0,(Ldebug_CIE15_start-4)
      004631 00 00 A8 03           6608 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$352)	;initial loc
      004635 00 00 00 2C           6609 	.dw	0,Sstm8s_uart1$UART1_SmartCardCmd$371-Sstm8s_uart1$UART1_SmartCardCmd$352
      004639 01                    6610 	.db	1
      00463A 00 00 A8 03           6611 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$352)
      00463E 0E                    6612 	.db	14
      00463F 02                    6613 	.uleb128	2
      004640 01                    6614 	.db	1
      004641 00 00 A8 0C           6615 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$354)
      004645 0E                    6616 	.db	14
      004646 02                    6617 	.uleb128	2
      004647 01                    6618 	.db	1
      004648 00 00 A8 0E           6619 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$355)
      00464C 0E                    6620 	.db	14
      00464D 03                    6621 	.uleb128	3
      00464E 01                    6622 	.db	1
      00464F 00 00 A8 10           6623 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$356)
      004653 0E                    6624 	.db	14
      004654 04                    6625 	.uleb128	4
      004655 01                    6626 	.db	1
      004656 00 00 A8 12           6627 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$357)
      00465A 0E                    6628 	.db	14
      00465B 06                    6629 	.uleb128	6
      00465C 01                    6630 	.db	1
      00465D 00 00 A8 14           6631 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$358)
      004661 0E                    6632 	.db	14
      004662 07                    6633 	.uleb128	7
      004663 01                    6634 	.db	1
      004664 00 00 A8 16           6635 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$359)
      004668 0E                    6636 	.db	14
      004669 08                    6637 	.uleb128	8
      00466A 01                    6638 	.db	1
      00466B 00 00 A8 1B           6639 	.dw	0,(Sstm8s_uart1$UART1_SmartCardCmd$360)
      00466F 0E                    6640 	.db	14
      004670 02                    6641 	.uleb128	2
                                   6642 
                                   6643 	.area .debug_frame (NOLOAD)
      004671 00 00                 6644 	.dw	0
      004673 00 0E                 6645 	.dw	Ldebug_CIE16_end-Ldebug_CIE16_start
      004675                       6646 Ldebug_CIE16_start:
      004675 FF FF                 6647 	.dw	0xffff
      004677 FF FF                 6648 	.dw	0xffff
      004679 01                    6649 	.db	1
      00467A 00                    6650 	.db	0
      00467B 01                    6651 	.uleb128	1
      00467C 7F                    6652 	.sleb128	-1
      00467D 09                    6653 	.db	9
      00467E 0C                    6654 	.db	12
      00467F 08                    6655 	.uleb128	8
      004680 02                    6656 	.uleb128	2
      004681 89                    6657 	.db	137
      004682 01                    6658 	.uleb128	1
      004683                       6659 Ldebug_CIE16_end:
      004683 00 00 00 44           6660 	.dw	0,68
      004687 00 00 46 71           6661 	.dw	0,(Ldebug_CIE16_start-4)
      00468B 00 00 A7 D7           6662 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$331)	;initial loc
      00468F 00 00 00 2C           6663 	.dw	0,Sstm8s_uart1$UART1_LINCmd$350-Sstm8s_uart1$UART1_LINCmd$331
      004693 01                    6664 	.db	1
      004694 00 00 A7 D7           6665 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$331)
      004698 0E                    6666 	.db	14
      004699 02                    6667 	.uleb128	2
      00469A 01                    6668 	.db	1
      00469B 00 00 A7 E0           6669 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$333)
      00469F 0E                    6670 	.db	14
      0046A0 02                    6671 	.uleb128	2
      0046A1 01                    6672 	.db	1
      0046A2 00 00 A7 E2           6673 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$334)
      0046A6 0E                    6674 	.db	14
      0046A7 03                    6675 	.uleb128	3
      0046A8 01                    6676 	.db	1
      0046A9 00 00 A7 E4           6677 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$335)
      0046AD 0E                    6678 	.db	14
      0046AE 04                    6679 	.uleb128	4
      0046AF 01                    6680 	.db	1
      0046B0 00 00 A7 E6           6681 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$336)
      0046B4 0E                    6682 	.db	14
      0046B5 06                    6683 	.uleb128	6
      0046B6 01                    6684 	.db	1
      0046B7 00 00 A7 E8           6685 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$337)
      0046BB 0E                    6686 	.db	14
      0046BC 07                    6687 	.uleb128	7
      0046BD 01                    6688 	.db	1
      0046BE 00 00 A7 EA           6689 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$338)
      0046C2 0E                    6690 	.db	14
      0046C3 08                    6691 	.uleb128	8
      0046C4 01                    6692 	.db	1
      0046C5 00 00 A7 EF           6693 	.dw	0,(Sstm8s_uart1$UART1_LINCmd$339)
      0046C9 0E                    6694 	.db	14
      0046CA 02                    6695 	.uleb128	2
                                   6696 
                                   6697 	.area .debug_frame (NOLOAD)
      0046CB 00 00                 6698 	.dw	0
      0046CD 00 0E                 6699 	.dw	Ldebug_CIE17_end-Ldebug_CIE17_start
      0046CF                       6700 Ldebug_CIE17_start:
      0046CF FF FF                 6701 	.dw	0xffff
      0046D1 FF FF                 6702 	.dw	0xffff
      0046D3 01                    6703 	.db	1
      0046D4 00                    6704 	.db	0
      0046D5 01                    6705 	.uleb128	1
      0046D6 7F                    6706 	.sleb128	-1
      0046D7 09                    6707 	.db	9
      0046D8 0C                    6708 	.db	12
      0046D9 08                    6709 	.uleb128	8
      0046DA 02                    6710 	.uleb128	2
      0046DB 89                    6711 	.db	137
      0046DC 01                    6712 	.uleb128	1
      0046DD                       6713 Ldebug_CIE17_end:
      0046DD 00 00 00 44           6714 	.dw	0,68
      0046E1 00 00 46 CB           6715 	.dw	0,(Ldebug_CIE17_start-4)
      0046E5 00 00 A7 AB           6716 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$310)	;initial loc
      0046E9 00 00 00 2C           6717 	.dw	0,Sstm8s_uart1$UART1_LINBreakDetectionConfig$329-Sstm8s_uart1$UART1_LINBreakDetectionConfig$310
      0046ED 01                    6718 	.db	1
      0046EE 00 00 A7 AB           6719 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$310)
      0046F2 0E                    6720 	.db	14
      0046F3 02                    6721 	.uleb128	2
      0046F4 01                    6722 	.db	1
      0046F5 00 00 A7 B4           6723 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$312)
      0046F9 0E                    6724 	.db	14
      0046FA 02                    6725 	.uleb128	2
      0046FB 01                    6726 	.db	1
      0046FC 00 00 A7 B6           6727 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$313)
      004700 0E                    6728 	.db	14
      004701 03                    6729 	.uleb128	3
      004702 01                    6730 	.db	1
      004703 00 00 A7 B8           6731 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$314)
      004707 0E                    6732 	.db	14
      004708 04                    6733 	.uleb128	4
      004709 01                    6734 	.db	1
      00470A 00 00 A7 BA           6735 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$315)
      00470E 0E                    6736 	.db	14
      00470F 06                    6737 	.uleb128	6
      004710 01                    6738 	.db	1
      004711 00 00 A7 BC           6739 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$316)
      004715 0E                    6740 	.db	14
      004716 07                    6741 	.uleb128	7
      004717 01                    6742 	.db	1
      004718 00 00 A7 BE           6743 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$317)
      00471C 0E                    6744 	.db	14
      00471D 08                    6745 	.uleb128	8
      00471E 01                    6746 	.db	1
      00471F 00 00 A7 C3           6747 	.dw	0,(Sstm8s_uart1$UART1_LINBreakDetectionConfig$318)
      004723 0E                    6748 	.db	14
      004724 02                    6749 	.uleb128	2
                                   6750 
                                   6751 	.area .debug_frame (NOLOAD)
      004725 00 00                 6752 	.dw	0
      004727 00 0E                 6753 	.dw	Ldebug_CIE18_end-Ldebug_CIE18_start
      004729                       6754 Ldebug_CIE18_start:
      004729 FF FF                 6755 	.dw	0xffff
      00472B FF FF                 6756 	.dw	0xffff
      00472D 01                    6757 	.db	1
      00472E 00                    6758 	.db	0
      00472F 01                    6759 	.uleb128	1
      004730 7F                    6760 	.sleb128	-1
      004731 09                    6761 	.db	9
      004732 0C                    6762 	.db	12
      004733 08                    6763 	.uleb128	8
      004734 02                    6764 	.uleb128	2
      004735 89                    6765 	.db	137
      004736 01                    6766 	.uleb128	1
      004737                       6767 Ldebug_CIE18_end:
      004737 00 00 00 44           6768 	.dw	0,68
      00473B 00 00 47 25           6769 	.dw	0,(Ldebug_CIE18_start-4)
      00473F 00 00 A7 7F           6770 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$289)	;initial loc
      004743 00 00 00 2C           6771 	.dw	0,Sstm8s_uart1$UART1_IrDACmd$308-Sstm8s_uart1$UART1_IrDACmd$289
      004747 01                    6772 	.db	1
      004748 00 00 A7 7F           6773 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$289)
      00474C 0E                    6774 	.db	14
      00474D 02                    6775 	.uleb128	2
      00474E 01                    6776 	.db	1
      00474F 00 00 A7 88           6777 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$291)
      004753 0E                    6778 	.db	14
      004754 02                    6779 	.uleb128	2
      004755 01                    6780 	.db	1
      004756 00 00 A7 8A           6781 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$292)
      00475A 0E                    6782 	.db	14
      00475B 03                    6783 	.uleb128	3
      00475C 01                    6784 	.db	1
      00475D 00 00 A7 8C           6785 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$293)
      004761 0E                    6786 	.db	14
      004762 04                    6787 	.uleb128	4
      004763 01                    6788 	.db	1
      004764 00 00 A7 8E           6789 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$294)
      004768 0E                    6790 	.db	14
      004769 06                    6791 	.uleb128	6
      00476A 01                    6792 	.db	1
      00476B 00 00 A7 90           6793 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$295)
      00476F 0E                    6794 	.db	14
      004770 07                    6795 	.uleb128	7
      004771 01                    6796 	.db	1
      004772 00 00 A7 92           6797 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$296)
      004776 0E                    6798 	.db	14
      004777 08                    6799 	.uleb128	8
      004778 01                    6800 	.db	1
      004779 00 00 A7 97           6801 	.dw	0,(Sstm8s_uart1$UART1_IrDACmd$297)
      00477D 0E                    6802 	.db	14
      00477E 02                    6803 	.uleb128	2
                                   6804 
                                   6805 	.area .debug_frame (NOLOAD)
      00477F 00 00                 6806 	.dw	0
      004781 00 0E                 6807 	.dw	Ldebug_CIE19_end-Ldebug_CIE19_start
      004783                       6808 Ldebug_CIE19_start:
      004783 FF FF                 6809 	.dw	0xffff
      004785 FF FF                 6810 	.dw	0xffff
      004787 01                    6811 	.db	1
      004788 00                    6812 	.db	0
      004789 01                    6813 	.uleb128	1
      00478A 7F                    6814 	.sleb128	-1
      00478B 09                    6815 	.db	9
      00478C 0C                    6816 	.db	12
      00478D 08                    6817 	.uleb128	8
      00478E 02                    6818 	.uleb128	2
      00478F 89                    6819 	.db	137
      004790 01                    6820 	.uleb128	1
      004791                       6821 Ldebug_CIE19_end:
      004791 00 00 00 44           6822 	.dw	0,68
      004795 00 00 47 7F           6823 	.dw	0,(Ldebug_CIE19_start-4)
      004799 00 00 A7 53           6824 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$268)	;initial loc
      00479D 00 00 00 2C           6825 	.dw	0,Sstm8s_uart1$UART1_IrDAConfig$287-Sstm8s_uart1$UART1_IrDAConfig$268
      0047A1 01                    6826 	.db	1
      0047A2 00 00 A7 53           6827 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$268)
      0047A6 0E                    6828 	.db	14
      0047A7 02                    6829 	.uleb128	2
      0047A8 01                    6830 	.db	1
      0047A9 00 00 A7 58           6831 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$270)
      0047AD 0E                    6832 	.db	14
      0047AE 02                    6833 	.uleb128	2
      0047AF 01                    6834 	.db	1
      0047B0 00 00 A7 5E           6835 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$271)
      0047B4 0E                    6836 	.db	14
      0047B5 03                    6837 	.uleb128	3
      0047B6 01                    6838 	.db	1
      0047B7 00 00 A7 60           6839 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$272)
      0047BB 0E                    6840 	.db	14
      0047BC 04                    6841 	.uleb128	4
      0047BD 01                    6842 	.db	1
      0047BE 00 00 A7 62           6843 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$273)
      0047C2 0E                    6844 	.db	14
      0047C3 06                    6845 	.uleb128	6
      0047C4 01                    6846 	.db	1
      0047C5 00 00 A7 64           6847 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$274)
      0047C9 0E                    6848 	.db	14
      0047CA 07                    6849 	.uleb128	7
      0047CB 01                    6850 	.db	1
      0047CC 00 00 A7 66           6851 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$275)
      0047D0 0E                    6852 	.db	14
      0047D1 08                    6853 	.uleb128	8
      0047D2 01                    6854 	.db	1
      0047D3 00 00 A7 6B           6855 	.dw	0,(Sstm8s_uart1$UART1_IrDAConfig$276)
      0047D7 0E                    6856 	.db	14
      0047D8 02                    6857 	.uleb128	2
                                   6858 
                                   6859 	.area .debug_frame (NOLOAD)
      0047D9 00 00                 6860 	.dw	0
      0047DB 00 0E                 6861 	.dw	Ldebug_CIE20_end-Ldebug_CIE20_start
      0047DD                       6862 Ldebug_CIE20_start:
      0047DD FF FF                 6863 	.dw	0xffff
      0047DF FF FF                 6864 	.dw	0xffff
      0047E1 01                    6865 	.db	1
      0047E2 00                    6866 	.db	0
      0047E3 01                    6867 	.uleb128	1
      0047E4 7F                    6868 	.sleb128	-1
      0047E5 09                    6869 	.db	9
      0047E6 0C                    6870 	.db	12
      0047E7 08                    6871 	.uleb128	8
      0047E8 02                    6872 	.uleb128	2
      0047E9 89                    6873 	.db	137
      0047EA 01                    6874 	.uleb128	1
      0047EB                       6875 Ldebug_CIE20_end:
      0047EB 00 00 00 44           6876 	.dw	0,68
      0047EF 00 00 47 D9           6877 	.dw	0,(Ldebug_CIE20_start-4)
      0047F3 00 00 A7 27           6878 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$247)	;initial loc
      0047F7 00 00 00 2C           6879 	.dw	0,Sstm8s_uart1$UART1_HalfDuplexCmd$266-Sstm8s_uart1$UART1_HalfDuplexCmd$247
      0047FB 01                    6880 	.db	1
      0047FC 00 00 A7 27           6881 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$247)
      004800 0E                    6882 	.db	14
      004801 02                    6883 	.uleb128	2
      004802 01                    6884 	.db	1
      004803 00 00 A7 30           6885 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$249)
      004807 0E                    6886 	.db	14
      004808 02                    6887 	.uleb128	2
      004809 01                    6888 	.db	1
      00480A 00 00 A7 32           6889 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$250)
      00480E 0E                    6890 	.db	14
      00480F 03                    6891 	.uleb128	3
      004810 01                    6892 	.db	1
      004811 00 00 A7 34           6893 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$251)
      004815 0E                    6894 	.db	14
      004816 04                    6895 	.uleb128	4
      004817 01                    6896 	.db	1
      004818 00 00 A7 36           6897 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$252)
      00481C 0E                    6898 	.db	14
      00481D 06                    6899 	.uleb128	6
      00481E 01                    6900 	.db	1
      00481F 00 00 A7 38           6901 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$253)
      004823 0E                    6902 	.db	14
      004824 07                    6903 	.uleb128	7
      004825 01                    6904 	.db	1
      004826 00 00 A7 3A           6905 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$254)
      00482A 0E                    6906 	.db	14
      00482B 08                    6907 	.uleb128	8
      00482C 01                    6908 	.db	1
      00482D 00 00 A7 3F           6909 	.dw	0,(Sstm8s_uart1$UART1_HalfDuplexCmd$255)
      004831 0E                    6910 	.db	14
      004832 02                    6911 	.uleb128	2
                                   6912 
                                   6913 	.area .debug_frame (NOLOAD)
      004833 00 00                 6914 	.dw	0
      004835 00 0E                 6915 	.dw	Ldebug_CIE21_end-Ldebug_CIE21_start
      004837                       6916 Ldebug_CIE21_start:
      004837 FF FF                 6917 	.dw	0xffff
      004839 FF FF                 6918 	.dw	0xffff
      00483B 01                    6919 	.db	1
      00483C 00                    6920 	.db	0
      00483D 01                    6921 	.uleb128	1
      00483E 7F                    6922 	.sleb128	-1
      00483F 09                    6923 	.db	9
      004840 0C                    6924 	.db	12
      004841 08                    6925 	.uleb128	8
      004842 02                    6926 	.uleb128	2
      004843 89                    6927 	.db	137
      004844 01                    6928 	.uleb128	1
      004845                       6929 Ldebug_CIE21_end:
      004845 00 00 00 FA           6930 	.dw	0,250
      004849 00 00 48 33           6931 	.dw	0,(Ldebug_CIE21_start-4)
      00484D 00 00 A6 5E           6932 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$177)	;initial loc
      004851 00 00 00 C9           6933 	.dw	0,Sstm8s_uart1$UART1_ITConfig$245-Sstm8s_uart1$UART1_ITConfig$177
      004855 01                    6934 	.db	1
      004856 00 00 A6 5E           6935 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$177)
      00485A 0E                    6936 	.db	14
      00485B 02                    6937 	.uleb128	2
      00485C 01                    6938 	.db	1
      00485D 00 00 A6 5F           6939 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$178)
      004861 0E                    6940 	.db	14
      004862 04                    6941 	.uleb128	4
      004863 01                    6942 	.db	1
      004864 00 00 A6 66           6943 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$180)
      004868 0E                    6944 	.db	14
      004869 04                    6945 	.uleb128	4
      00486A 01                    6946 	.db	1
      00486B 00 00 A6 6B           6947 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$181)
      00486F 0E                    6948 	.db	14
      004870 04                    6949 	.uleb128	4
      004871 01                    6950 	.db	1
      004872 00 00 A6 70           6951 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$182)
      004876 0E                    6952 	.db	14
      004877 04                    6953 	.uleb128	4
      004878 01                    6954 	.db	1
      004879 00 00 A6 75           6955 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$183)
      00487D 0E                    6956 	.db	14
      00487E 04                    6957 	.uleb128	4
      00487F 01                    6958 	.db	1
      004880 00 00 A6 7A           6959 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$184)
      004884 0E                    6960 	.db	14
      004885 04                    6961 	.uleb128	4
      004886 01                    6962 	.db	1
      004887 00 00 A6 7F           6963 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$185)
      00488B 0E                    6964 	.db	14
      00488C 04                    6965 	.uleb128	4
      00488D 01                    6966 	.db	1
      00488E 00 00 A6 80           6967 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$186)
      004892 0E                    6968 	.db	14
      004893 06                    6969 	.uleb128	6
      004894 01                    6970 	.db	1
      004895 00 00 A6 82           6971 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$187)
      004899 0E                    6972 	.db	14
      00489A 07                    6973 	.uleb128	7
      00489B 01                    6974 	.db	1
      00489C 00 00 A6 84           6975 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$188)
      0048A0 0E                    6976 	.db	14
      0048A1 08                    6977 	.uleb128	8
      0048A2 01                    6978 	.db	1
      0048A3 00 00 A6 86           6979 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$189)
      0048A7 0E                    6980 	.db	14
      0048A8 09                    6981 	.uleb128	9
      0048A9 01                    6982 	.db	1
      0048AA 00 00 A6 88           6983 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$190)
      0048AE 0E                    6984 	.db	14
      0048AF 0A                    6985 	.uleb128	10
      0048B0 01                    6986 	.db	1
      0048B1 00 00 A6 8A           6987 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$191)
      0048B5 0E                    6988 	.db	14
      0048B6 0B                    6989 	.uleb128	11
      0048B7 01                    6990 	.db	1
      0048B8 00 00 A6 8C           6991 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$192)
      0048BC 0E                    6992 	.db	14
      0048BD 0C                    6993 	.uleb128	12
      0048BE 01                    6994 	.db	1
      0048BF 00 00 A6 91           6995 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$193)
      0048C3 0E                    6996 	.db	14
      0048C4 06                    6997 	.uleb128	6
      0048C5 01                    6998 	.db	1
      0048C6 00 00 A6 92           6999 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$194)
      0048CA 0E                    7000 	.db	14
      0048CB 04                    7001 	.uleb128	4
      0048CC 01                    7002 	.db	1
      0048CD 00 00 A6 9B           7003 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$196)
      0048D1 0E                    7004 	.db	14
      0048D2 04                    7005 	.uleb128	4
      0048D3 01                    7006 	.db	1
      0048D4 00 00 A6 9C           7007 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$197)
      0048D8 0E                    7008 	.db	14
      0048D9 06                    7009 	.uleb128	6
      0048DA 01                    7010 	.db	1
      0048DB 00 00 A6 9E           7011 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$198)
      0048DF 0E                    7012 	.db	14
      0048E0 07                    7013 	.uleb128	7
      0048E1 01                    7014 	.db	1
      0048E2 00 00 A6 A0           7015 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$199)
      0048E6 0E                    7016 	.db	14
      0048E7 08                    7017 	.uleb128	8
      0048E8 01                    7018 	.db	1
      0048E9 00 00 A6 A2           7019 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$200)
      0048ED 0E                    7020 	.db	14
      0048EE 09                    7021 	.uleb128	9
      0048EF 01                    7022 	.db	1
      0048F0 00 00 A6 A4           7023 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$201)
      0048F4 0E                    7024 	.db	14
      0048F5 0A                    7025 	.uleb128	10
      0048F6 01                    7026 	.db	1
      0048F7 00 00 A6 A6           7027 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$202)
      0048FB 0E                    7028 	.db	14
      0048FC 0B                    7029 	.uleb128	11
      0048FD 01                    7030 	.db	1
      0048FE 00 00 A6 A8           7031 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$203)
      004902 0E                    7032 	.db	14
      004903 0C                    7033 	.uleb128	12
      004904 01                    7034 	.db	1
      004905 00 00 A6 AD           7035 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$204)
      004909 0E                    7036 	.db	14
      00490A 06                    7037 	.uleb128	6
      00490B 01                    7038 	.db	1
      00490C 00 00 A6 AE           7039 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$205)
      004910 0E                    7040 	.db	14
      004911 04                    7041 	.uleb128	4
      004912 01                    7042 	.db	1
      004913 00 00 A6 B3           7043 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$208)
      004917 0E                    7044 	.db	14
      004918 05                    7045 	.uleb128	5
      004919 01                    7046 	.db	1
      00491A 00 00 A6 B8           7047 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$209)
      00491E 0E                    7048 	.db	14
      00491F 04                    7049 	.uleb128	4
      004920 01                    7050 	.db	1
      004921 00 00 A6 CB           7051 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$211)
      004925 0E                    7052 	.db	14
      004926 04                    7053 	.uleb128	4
      004927 01                    7054 	.db	1
      004928 00 00 A6 D3           7055 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$213)
      00492C 0E                    7056 	.db	14
      00492D 04                    7057 	.uleb128	4
      00492E 01                    7058 	.db	1
      00492F 00 00 A6 FF           7059 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$228)
      004933 0E                    7060 	.db	14
      004934 05                    7061 	.uleb128	5
      004935 01                    7062 	.db	1
      004936 00 00 A7 02           7063 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$229)
      00493A 0E                    7064 	.db	14
      00493B 04                    7065 	.uleb128	4
      00493C 01                    7066 	.db	1
      00493D 00 00 A7 26           7067 	.dw	0,(Sstm8s_uart1$UART1_ITConfig$243)
      004941 0E                    7068 	.db	14
      004942 02                    7069 	.uleb128	2
                                   7070 
                                   7071 	.area .debug_frame (NOLOAD)
      004943 00 00                 7072 	.dw	0
      004945 00 0E                 7073 	.dw	Ldebug_CIE22_end-Ldebug_CIE22_start
      004947                       7074 Ldebug_CIE22_start:
      004947 FF FF                 7075 	.dw	0xffff
      004949 FF FF                 7076 	.dw	0xffff
      00494B 01                    7077 	.db	1
      00494C 00                    7078 	.db	0
      00494D 01                    7079 	.uleb128	1
      00494E 7F                    7080 	.sleb128	-1
      00494F 09                    7081 	.db	9
      004950 0C                    7082 	.db	12
      004951 08                    7083 	.uleb128	8
      004952 02                    7084 	.uleb128	2
      004953 89                    7085 	.db	137
      004954 01                    7086 	.uleb128	1
      004955                       7087 Ldebug_CIE22_end:
      004955 00 00 00 13           7088 	.dw	0,19
      004959 00 00 49 43           7089 	.dw	0,(Ldebug_CIE22_start-4)
      00495D 00 00 A6 4A           7090 	.dw	0,(Sstm8s_uart1$UART1_Cmd$164)	;initial loc
      004961 00 00 00 14           7091 	.dw	0,Sstm8s_uart1$UART1_Cmd$175-Sstm8s_uart1$UART1_Cmd$164
      004965 01                    7092 	.db	1
      004966 00 00 A6 4A           7093 	.dw	0,(Sstm8s_uart1$UART1_Cmd$164)
      00496A 0E                    7094 	.db	14
      00496B 02                    7095 	.uleb128	2
                                   7096 
                                   7097 	.area .debug_frame (NOLOAD)
      00496C 00 00                 7098 	.dw	0
      00496E 00 0E                 7099 	.dw	Ldebug_CIE23_end-Ldebug_CIE23_start
      004970                       7100 Ldebug_CIE23_start:
      004970 FF FF                 7101 	.dw	0xffff
      004972 FF FF                 7102 	.dw	0xffff
      004974 01                    7103 	.db	1
      004975 00                    7104 	.db	0
      004976 01                    7105 	.uleb128	1
      004977 7F                    7106 	.sleb128	-1
      004978 09                    7107 	.db	9
      004979 0C                    7108 	.db	12
      00497A 08                    7109 	.uleb128	8
      00497B 02                    7110 	.uleb128	2
      00497C 89                    7111 	.db	137
      00497D 01                    7112 	.uleb128	1
      00497E                       7113 Ldebug_CIE23_end:
      00497E 00 00 02 AC           7114 	.dw	0,684
      004982 00 00 49 6C           7115 	.dw	0,(Ldebug_CIE23_start-4)
      004986 00 00 A3 E6           7116 	.dw	0,(Sstm8s_uart1$UART1_Init$17)	;initial loc
      00498A 00 00 02 64           7117 	.dw	0,Sstm8s_uart1$UART1_Init$162-Sstm8s_uart1$UART1_Init$17
      00498E 01                    7118 	.db	1
      00498F 00 00 A3 E6           7119 	.dw	0,(Sstm8s_uart1$UART1_Init$17)
      004993 0E                    7120 	.db	14
      004994 02                    7121 	.uleb128	2
      004995 01                    7122 	.db	1
      004996 00 00 A3 E8           7123 	.dw	0,(Sstm8s_uart1$UART1_Init$18)
      00499A 0E                    7124 	.db	14
      00499B 0F                    7125 	.uleb128	15
      00499C 01                    7126 	.db	1
      00499D 00 00 A3 F8           7127 	.dw	0,(Sstm8s_uart1$UART1_Init$20)
      0049A1 0E                    7128 	.db	14
      0049A2 10                    7129 	.uleb128	16
      0049A3 01                    7130 	.db	1
      0049A4 00 00 A3 FA           7131 	.dw	0,(Sstm8s_uart1$UART1_Init$21)
      0049A8 0E                    7132 	.db	14
      0049A9 12                    7133 	.uleb128	18
      0049AA 01                    7134 	.db	1
      0049AB 00 00 A3 FC           7135 	.dw	0,(Sstm8s_uart1$UART1_Init$22)
      0049AF 0E                    7136 	.db	14
      0049B0 13                    7137 	.uleb128	19
      0049B1 01                    7138 	.db	1
      0049B2 00 00 A3 FE           7139 	.dw	0,(Sstm8s_uart1$UART1_Init$23)
      0049B6 0E                    7140 	.db	14
      0049B7 14                    7141 	.uleb128	20
      0049B8 01                    7142 	.db	1
      0049B9 00 00 A4 00           7143 	.dw	0,(Sstm8s_uart1$UART1_Init$24)
      0049BD 0E                    7144 	.db	14
      0049BE 15                    7145 	.uleb128	21
      0049BF 01                    7146 	.db	1
      0049C0 00 00 A4 05           7147 	.dw	0,(Sstm8s_uart1$UART1_Init$25)
      0049C4 0E                    7148 	.db	14
      0049C5 0F                    7149 	.uleb128	15
      0049C6 01                    7150 	.db	1
      0049C7 00 00 A4 0F           7151 	.dw	0,(Sstm8s_uart1$UART1_Init$27)
      0049CB 0E                    7152 	.db	14
      0049CC 0F                    7153 	.uleb128	15
      0049CD 01                    7154 	.db	1
      0049CE 00 00 A4 11           7155 	.dw	0,(Sstm8s_uart1$UART1_Init$28)
      0049D2 0E                    7156 	.db	14
      0049D3 10                    7157 	.uleb128	16
      0049D4 01                    7158 	.db	1
      0049D5 00 00 A4 13           7159 	.dw	0,(Sstm8s_uart1$UART1_Init$29)
      0049D9 0E                    7160 	.db	14
      0049DA 12                    7161 	.uleb128	18
      0049DB 01                    7162 	.db	1
      0049DC 00 00 A4 15           7163 	.dw	0,(Sstm8s_uart1$UART1_Init$30)
      0049E0 0E                    7164 	.db	14
      0049E1 13                    7165 	.uleb128	19
      0049E2 01                    7166 	.db	1
      0049E3 00 00 A4 17           7167 	.dw	0,(Sstm8s_uart1$UART1_Init$31)
      0049E7 0E                    7168 	.db	14
      0049E8 14                    7169 	.uleb128	20
      0049E9 01                    7170 	.db	1
      0049EA 00 00 A4 19           7171 	.dw	0,(Sstm8s_uart1$UART1_Init$32)
      0049EE 0E                    7172 	.db	14
      0049EF 15                    7173 	.uleb128	21
      0049F0 01                    7174 	.db	1
      0049F1 00 00 A4 1E           7175 	.dw	0,(Sstm8s_uart1$UART1_Init$33)
      0049F5 0E                    7176 	.db	14
      0049F6 0F                    7177 	.uleb128	15
      0049F7 01                    7178 	.db	1
      0049F8 00 00 A4 28           7179 	.dw	0,(Sstm8s_uart1$UART1_Init$35)
      0049FC 0E                    7180 	.db	14
      0049FD 0F                    7181 	.uleb128	15
      0049FE 01                    7182 	.db	1
      0049FF 00 00 A4 2E           7183 	.dw	0,(Sstm8s_uart1$UART1_Init$36)
      004A03 0E                    7184 	.db	14
      004A04 0F                    7185 	.uleb128	15
      004A05 01                    7186 	.db	1
      004A06 00 00 A4 34           7187 	.dw	0,(Sstm8s_uart1$UART1_Init$37)
      004A0A 0E                    7188 	.db	14
      004A0B 0F                    7189 	.uleb128	15
      004A0C 01                    7190 	.db	1
      004A0D 00 00 A4 36           7191 	.dw	0,(Sstm8s_uart1$UART1_Init$38)
      004A11 0E                    7192 	.db	14
      004A12 10                    7193 	.uleb128	16
      004A13 01                    7194 	.db	1
      004A14 00 00 A4 38           7195 	.dw	0,(Sstm8s_uart1$UART1_Init$39)
      004A18 0E                    7196 	.db	14
      004A19 12                    7197 	.uleb128	18
      004A1A 01                    7198 	.db	1
      004A1B 00 00 A4 3A           7199 	.dw	0,(Sstm8s_uart1$UART1_Init$40)
      004A1F 0E                    7200 	.db	14
      004A20 13                    7201 	.uleb128	19
      004A21 01                    7202 	.db	1
      004A22 00 00 A4 3C           7203 	.dw	0,(Sstm8s_uart1$UART1_Init$41)
      004A26 0E                    7204 	.db	14
      004A27 14                    7205 	.uleb128	20
      004A28 01                    7206 	.db	1
      004A29 00 00 A4 3E           7207 	.dw	0,(Sstm8s_uart1$UART1_Init$42)
      004A2D 0E                    7208 	.db	14
      004A2E 15                    7209 	.uleb128	21
      004A2F 01                    7210 	.db	1
      004A30 00 00 A4 43           7211 	.dw	0,(Sstm8s_uart1$UART1_Init$43)
      004A34 0E                    7212 	.db	14
      004A35 0F                    7213 	.uleb128	15
      004A36 01                    7214 	.db	1
      004A37 00 00 A4 4D           7215 	.dw	0,(Sstm8s_uart1$UART1_Init$45)
      004A3B 0E                    7216 	.db	14
      004A3C 0F                    7217 	.uleb128	15
      004A3D 01                    7218 	.db	1
      004A3E 00 00 A4 53           7219 	.dw	0,(Sstm8s_uart1$UART1_Init$46)
      004A42 0E                    7220 	.db	14
      004A43 0F                    7221 	.uleb128	15
      004A44 01                    7222 	.db	1
      004A45 00 00 A4 55           7223 	.dw	0,(Sstm8s_uart1$UART1_Init$47)
      004A49 0E                    7224 	.db	14
      004A4A 10                    7225 	.uleb128	16
      004A4B 01                    7226 	.db	1
      004A4C 00 00 A4 57           7227 	.dw	0,(Sstm8s_uart1$UART1_Init$48)
      004A50 0E                    7228 	.db	14
      004A51 12                    7229 	.uleb128	18
      004A52 01                    7230 	.db	1
      004A53 00 00 A4 59           7231 	.dw	0,(Sstm8s_uart1$UART1_Init$49)
      004A57 0E                    7232 	.db	14
      004A58 13                    7233 	.uleb128	19
      004A59 01                    7234 	.db	1
      004A5A 00 00 A4 5B           7235 	.dw	0,(Sstm8s_uart1$UART1_Init$50)
      004A5E 0E                    7236 	.db	14
      004A5F 14                    7237 	.uleb128	20
      004A60 01                    7238 	.db	1
      004A61 00 00 A4 5D           7239 	.dw	0,(Sstm8s_uart1$UART1_Init$51)
      004A65 0E                    7240 	.db	14
      004A66 15                    7241 	.uleb128	21
      004A67 01                    7242 	.db	1
      004A68 00 00 A4 62           7243 	.dw	0,(Sstm8s_uart1$UART1_Init$52)
      004A6C 0E                    7244 	.db	14
      004A6D 0F                    7245 	.uleb128	15
      004A6E 01                    7246 	.db	1
      004A6F 00 00 A4 6B           7247 	.dw	0,(Sstm8s_uart1$UART1_Init$54)
      004A73 0E                    7248 	.db	14
      004A74 0F                    7249 	.uleb128	15
      004A75 01                    7250 	.db	1
      004A76 00 00 A4 74           7251 	.dw	0,(Sstm8s_uart1$UART1_Init$55)
      004A7A 0E                    7252 	.db	14
      004A7B 0F                    7253 	.uleb128	15
      004A7C 01                    7254 	.db	1
      004A7D 00 00 A4 7A           7255 	.dw	0,(Sstm8s_uart1$UART1_Init$56)
      004A81 0E                    7256 	.db	14
      004A82 0F                    7257 	.uleb128	15
      004A83 01                    7258 	.db	1
      004A84 00 00 A4 80           7259 	.dw	0,(Sstm8s_uart1$UART1_Init$57)
      004A88 0E                    7260 	.db	14
      004A89 0F                    7261 	.uleb128	15
      004A8A 01                    7262 	.db	1
      004A8B 00 00 A4 89           7263 	.dw	0,(Sstm8s_uart1$UART1_Init$58)
      004A8F 0E                    7264 	.db	14
      004A90 0F                    7265 	.uleb128	15
      004A91 01                    7266 	.db	1
      004A92 00 00 A4 95           7267 	.dw	0,(Sstm8s_uart1$UART1_Init$59)
      004A96 0E                    7268 	.db	14
      004A97 0F                    7269 	.uleb128	15
      004A98 01                    7270 	.db	1
      004A99 00 00 A4 9B           7271 	.dw	0,(Sstm8s_uart1$UART1_Init$60)
      004A9D 0E                    7272 	.db	14
      004A9E 0F                    7273 	.uleb128	15
      004A9F 01                    7274 	.db	1
      004AA0 00 00 A4 A1           7275 	.dw	0,(Sstm8s_uart1$UART1_Init$61)
      004AA4 0E                    7276 	.db	14
      004AA5 0F                    7277 	.uleb128	15
      004AA6 01                    7278 	.db	1
      004AA7 00 00 A4 A3           7279 	.dw	0,(Sstm8s_uart1$UART1_Init$62)
      004AAB 0E                    7280 	.db	14
      004AAC 10                    7281 	.uleb128	16
      004AAD 01                    7282 	.db	1
      004AAE 00 00 A4 A5           7283 	.dw	0,(Sstm8s_uart1$UART1_Init$63)
      004AB2 0E                    7284 	.db	14
      004AB3 12                    7285 	.uleb128	18
      004AB4 01                    7286 	.db	1
      004AB5 00 00 A4 A7           7287 	.dw	0,(Sstm8s_uart1$UART1_Init$64)
      004AB9 0E                    7288 	.db	14
      004ABA 13                    7289 	.uleb128	19
      004ABB 01                    7290 	.db	1
      004ABC 00 00 A4 A9           7291 	.dw	0,(Sstm8s_uart1$UART1_Init$65)
      004AC0 0E                    7292 	.db	14
      004AC1 14                    7293 	.uleb128	20
      004AC2 01                    7294 	.db	1
      004AC3 00 00 A4 AB           7295 	.dw	0,(Sstm8s_uart1$UART1_Init$66)
      004AC7 0E                    7296 	.db	14
      004AC8 15                    7297 	.uleb128	21
      004AC9 01                    7298 	.db	1
      004ACA 00 00 A4 B0           7299 	.dw	0,(Sstm8s_uart1$UART1_Init$67)
      004ACE 0E                    7300 	.db	14
      004ACF 0F                    7301 	.uleb128	15
      004AD0 01                    7302 	.db	1
      004AD1 00 00 A4 B8           7303 	.dw	0,(Sstm8s_uart1$UART1_Init$69)
      004AD5 0E                    7304 	.db	14
      004AD6 0F                    7305 	.uleb128	15
      004AD7 01                    7306 	.db	1
      004AD8 00 00 A4 C0           7307 	.dw	0,(Sstm8s_uart1$UART1_Init$70)
      004ADC 0E                    7308 	.db	14
      004ADD 0F                    7309 	.uleb128	15
      004ADE 01                    7310 	.db	1
      004ADF 00 00 A4 C8           7311 	.dw	0,(Sstm8s_uart1$UART1_Init$71)
      004AE3 0E                    7312 	.db	14
      004AE4 0F                    7313 	.uleb128	15
      004AE5 01                    7314 	.db	1
      004AE6 00 00 A4 D0           7315 	.dw	0,(Sstm8s_uart1$UART1_Init$72)
      004AEA 0E                    7316 	.db	14
      004AEB 0F                    7317 	.uleb128	15
      004AEC 01                    7318 	.db	1
      004AED 00 00 A4 D2           7319 	.dw	0,(Sstm8s_uart1$UART1_Init$73)
      004AF1 0E                    7320 	.db	14
      004AF2 10                    7321 	.uleb128	16
      004AF3 01                    7322 	.db	1
      004AF4 00 00 A4 D4           7323 	.dw	0,(Sstm8s_uart1$UART1_Init$74)
      004AF8 0E                    7324 	.db	14
      004AF9 12                    7325 	.uleb128	18
      004AFA 01                    7326 	.db	1
      004AFB 00 00 A4 D6           7327 	.dw	0,(Sstm8s_uart1$UART1_Init$75)
      004AFF 0E                    7328 	.db	14
      004B00 13                    7329 	.uleb128	19
      004B01 01                    7330 	.db	1
      004B02 00 00 A4 D8           7331 	.dw	0,(Sstm8s_uart1$UART1_Init$76)
      004B06 0E                    7332 	.db	14
      004B07 14                    7333 	.uleb128	20
      004B08 01                    7334 	.db	1
      004B09 00 00 A4 DA           7335 	.dw	0,(Sstm8s_uart1$UART1_Init$77)
      004B0D 0E                    7336 	.db	14
      004B0E 15                    7337 	.uleb128	21
      004B0F 01                    7338 	.db	1
      004B10 00 00 A4 DF           7339 	.dw	0,(Sstm8s_uart1$UART1_Init$78)
      004B14 0E                    7340 	.db	14
      004B15 0F                    7341 	.uleb128	15
      004B16 01                    7342 	.db	1
      004B17 00 00 A5 3A           7343 	.dw	0,(Sstm8s_uart1$UART1_Init$89)
      004B1B 0E                    7344 	.db	14
      004B1C 11                    7345 	.uleb128	17
      004B1D 01                    7346 	.db	1
      004B1E 00 00 A5 3D           7347 	.dw	0,(Sstm8s_uart1$UART1_Init$90)
      004B22 0E                    7348 	.db	14
      004B23 13                    7349 	.uleb128	19
      004B24 01                    7350 	.db	1
      004B25 00 00 A5 40           7351 	.dw	0,(Sstm8s_uart1$UART1_Init$91)
      004B29 0E                    7352 	.db	14
      004B2A 15                    7353 	.uleb128	21
      004B2B 01                    7354 	.db	1
      004B2C 00 00 A5 42           7355 	.dw	0,(Sstm8s_uart1$UART1_Init$92)
      004B30 0E                    7356 	.db	14
      004B31 17                    7357 	.uleb128	23
      004B32 01                    7358 	.db	1
      004B33 00 00 A5 47           7359 	.dw	0,(Sstm8s_uart1$UART1_Init$93)
      004B37 0E                    7360 	.db	14
      004B38 0F                    7361 	.uleb128	15
      004B39 01                    7362 	.db	1
      004B3A 00 00 A5 4F           7363 	.dw	0,(Sstm8s_uart1$UART1_Init$95)
      004B3E 0E                    7364 	.db	14
      004B3F 11                    7365 	.uleb128	17
      004B40 01                    7366 	.db	1
      004B41 00 00 A5 51           7367 	.dw	0,(Sstm8s_uart1$UART1_Init$96)
      004B45 0E                    7368 	.db	14
      004B46 13                    7369 	.uleb128	19
      004B47 01                    7370 	.db	1
      004B48 00 00 A5 53           7371 	.dw	0,(Sstm8s_uart1$UART1_Init$97)
      004B4C 0E                    7372 	.db	14
      004B4D 14                    7373 	.uleb128	20
      004B4E 01                    7374 	.db	1
      004B4F 00 00 A5 55           7375 	.dw	0,(Sstm8s_uart1$UART1_Init$98)
      004B53 0E                    7376 	.db	14
      004B54 16                    7377 	.uleb128	22
      004B55 01                    7378 	.db	1
      004B56 00 00 A5 57           7379 	.dw	0,(Sstm8s_uart1$UART1_Init$99)
      004B5A 0E                    7380 	.db	14
      004B5B 17                    7381 	.uleb128	23
      004B5C 01                    7382 	.db	1
      004B5D 00 00 A5 5C           7383 	.dw	0,(Sstm8s_uart1$UART1_Init$100)
      004B61 0E                    7384 	.db	14
      004B62 0F                    7385 	.uleb128	15
      004B63 01                    7386 	.db	1
      004B64 00 00 A5 61           7387 	.dw	0,(Sstm8s_uart1$UART1_Init$101)
      004B68 0E                    7388 	.db	14
      004B69 11                    7389 	.uleb128	17
      004B6A 01                    7390 	.db	1
      004B6B 00 00 A5 64           7391 	.dw	0,(Sstm8s_uart1$UART1_Init$102)
      004B6F 0E                    7392 	.db	14
      004B70 13                    7393 	.uleb128	19
      004B71 01                    7394 	.db	1
      004B72 00 00 A5 67           7395 	.dw	0,(Sstm8s_uart1$UART1_Init$103)
      004B76 0E                    7396 	.db	14
      004B77 15                    7397 	.uleb128	21
      004B78 01                    7398 	.db	1
      004B79 00 00 A5 69           7399 	.dw	0,(Sstm8s_uart1$UART1_Init$104)
      004B7D 0E                    7400 	.db	14
      004B7E 17                    7401 	.uleb128	23
      004B7F 01                    7402 	.db	1
      004B80 00 00 A5 6E           7403 	.dw	0,(Sstm8s_uart1$UART1_Init$105)
      004B84 0E                    7404 	.db	14
      004B85 0F                    7405 	.uleb128	15
      004B86 01                    7406 	.db	1
      004B87 00 00 A5 7A           7407 	.dw	0,(Sstm8s_uart1$UART1_Init$107)
      004B8B 0E                    7408 	.db	14
      004B8C 10                    7409 	.uleb128	16
      004B8D 01                    7410 	.db	1
      004B8E 00 00 A5 7E           7411 	.dw	0,(Sstm8s_uart1$UART1_Init$108)
      004B92 0E                    7412 	.db	14
      004B93 0F                    7413 	.uleb128	15
      004B94 01                    7414 	.db	1
      004B95 00 00 A5 7F           7415 	.dw	0,(Sstm8s_uart1$UART1_Init$109)
      004B99 0E                    7416 	.db	14
      004B9A 10                    7417 	.uleb128	16
      004B9B 01                    7418 	.db	1
      004B9C 00 00 A5 82           7419 	.dw	0,(Sstm8s_uart1$UART1_Init$110)
      004BA0 0E                    7420 	.db	14
      004BA1 12                    7421 	.uleb128	18
      004BA2 01                    7422 	.db	1
      004BA3 00 00 A5 85           7423 	.dw	0,(Sstm8s_uart1$UART1_Init$111)
      004BA7 0E                    7424 	.db	14
      004BA8 14                    7425 	.uleb128	20
      004BA9 01                    7426 	.db	1
      004BAA 00 00 A5 87           7427 	.dw	0,(Sstm8s_uart1$UART1_Init$112)
      004BAE 0E                    7428 	.db	14
      004BAF 15                    7429 	.uleb128	21
      004BB0 01                    7430 	.db	1
      004BB1 00 00 A5 89           7431 	.dw	0,(Sstm8s_uart1$UART1_Init$113)
      004BB5 0E                    7432 	.db	14
      004BB6 17                    7433 	.uleb128	23
      004BB7 01                    7434 	.db	1
      004BB8 00 00 A5 8B           7435 	.dw	0,(Sstm8s_uart1$UART1_Init$114)
      004BBC 0E                    7436 	.db	14
      004BBD 18                    7437 	.uleb128	24
      004BBE 01                    7438 	.db	1
      004BBF 00 00 A5 90           7439 	.dw	0,(Sstm8s_uart1$UART1_Init$115)
      004BC3 0E                    7440 	.db	14
      004BC4 10                    7441 	.uleb128	16
      004BC5 01                    7442 	.db	1
      004BC6 00 00 A5 95           7443 	.dw	0,(Sstm8s_uart1$UART1_Init$116)
      004BCA 0E                    7444 	.db	14
      004BCB 0F                    7445 	.uleb128	15
      004BCC 01                    7446 	.db	1
      004BCD 00 00 A5 AC           7447 	.dw	0,(Sstm8s_uart1$UART1_Init$117)
      004BD1 0E                    7448 	.db	14
      004BD2 10                    7449 	.uleb128	16
      004BD3 01                    7450 	.db	1
      004BD4 00 00 A5 AE           7451 	.dw	0,(Sstm8s_uart1$UART1_Init$118)
      004BD8 0E                    7452 	.db	14
      004BD9 11                    7453 	.uleb128	17
      004BDA 01                    7454 	.db	1
      004BDB 00 00 A5 B0           7455 	.dw	0,(Sstm8s_uart1$UART1_Init$119)
      004BDF 0E                    7456 	.db	14
      004BE0 12                    7457 	.uleb128	18
      004BE1 01                    7458 	.db	1
      004BE2 00 00 A5 B2           7459 	.dw	0,(Sstm8s_uart1$UART1_Init$120)
      004BE6 0E                    7460 	.db	14
      004BE7 13                    7461 	.uleb128	19
      004BE8 01                    7462 	.db	1
      004BE9 00 00 A5 B4           7463 	.dw	0,(Sstm8s_uart1$UART1_Init$121)
      004BED 0E                    7464 	.db	14
      004BEE 15                    7465 	.uleb128	21
      004BEF 01                    7466 	.db	1
      004BF0 00 00 A5 B5           7467 	.dw	0,(Sstm8s_uart1$UART1_Init$122)
      004BF4 0E                    7468 	.db	14
      004BF5 17                    7469 	.uleb128	23
      004BF6 01                    7470 	.db	1
      004BF7 00 00 A5 BA           7471 	.dw	0,(Sstm8s_uart1$UART1_Init$123)
      004BFB 0E                    7472 	.db	14
      004BFC 0F                    7473 	.uleb128	15
      004BFD 01                    7474 	.db	1
      004BFE 00 00 A6 02           7475 	.dw	0,(Sstm8s_uart1$UART1_Init$131)
      004C02 0E                    7476 	.db	14
      004C03 10                    7477 	.uleb128	16
      004C04 01                    7478 	.db	1
      004C05 00 00 A6 07           7479 	.dw	0,(Sstm8s_uart1$UART1_Init$132)
      004C09 0E                    7480 	.db	14
      004C0A 0F                    7481 	.uleb128	15
      004C0B 01                    7482 	.db	1
      004C0C 00 00 A6 19           7483 	.dw	0,(Sstm8s_uart1$UART1_Init$141)
      004C10 0E                    7484 	.db	14
      004C11 10                    7485 	.uleb128	16
      004C12 01                    7486 	.db	1
      004C13 00 00 A6 1E           7487 	.dw	0,(Sstm8s_uart1$UART1_Init$142)
      004C17 0E                    7488 	.db	14
      004C18 0F                    7489 	.uleb128	15
      004C19 01                    7490 	.db	1
      004C1A 00 00 A6 3B           7491 	.dw	0,(Sstm8s_uart1$UART1_Init$156)
      004C1E 0E                    7492 	.db	14
      004C1F 10                    7493 	.uleb128	16
      004C20 01                    7494 	.db	1
      004C21 00 00 A6 42           7495 	.dw	0,(Sstm8s_uart1$UART1_Init$157)
      004C25 0E                    7496 	.db	14
      004C26 0F                    7497 	.uleb128	15
      004C27 01                    7498 	.db	1
      004C28 00 00 A6 49           7499 	.dw	0,(Sstm8s_uart1$UART1_Init$160)
      004C2C 0E                    7500 	.db	14
      004C2D 02                    7501 	.uleb128	2
                                   7502 
                                   7503 	.area .debug_frame (NOLOAD)
      004C2E 00 00                 7504 	.dw	0
      004C30 00 0E                 7505 	.dw	Ldebug_CIE24_end-Ldebug_CIE24_start
      004C32                       7506 Ldebug_CIE24_start:
      004C32 FF FF                 7507 	.dw	0xffff
      004C34 FF FF                 7508 	.dw	0xffff
      004C36 01                    7509 	.db	1
      004C37 00                    7510 	.db	0
      004C38 01                    7511 	.uleb128	1
      004C39 7F                    7512 	.sleb128	-1
      004C3A 09                    7513 	.db	9
      004C3B 0C                    7514 	.db	12
      004C3C 08                    7515 	.uleb128	8
      004C3D 02                    7516 	.uleb128	2
      004C3E 89                    7517 	.db	137
      004C3F 01                    7518 	.uleb128	1
      004C40                       7519 Ldebug_CIE24_end:
      004C40 00 00 00 13           7520 	.dw	0,19
      004C44 00 00 4C 2E           7521 	.dw	0,(Ldebug_CIE24_start-4)
      004C48 00 00 A3 BB           7522 	.dw	0,(Sstm8s_uart1$UART1_DeInit$1)	;initial loc
      004C4C 00 00 00 2B           7523 	.dw	0,Sstm8s_uart1$UART1_DeInit$15-Sstm8s_uart1$UART1_DeInit$1
      004C50 01                    7524 	.db	1
      004C51 00 00 A3 BB           7525 	.dw	0,(Sstm8s_uart1$UART1_DeInit$1)
      004C55 0E                    7526 	.db	14
      004C56 02                    7527 	.uleb128	2
