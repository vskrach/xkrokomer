                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.1.0 #12072 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module stm8s_exti
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _assert_failed
                                     12 	.globl _EXTI_DeInit
                                     13 	.globl _EXTI_SetExtIntSensitivity
                                     14 	.globl _EXTI_SetTLISensitivity
                                     15 	.globl _EXTI_GetExtIntSensitivity
                                     16 	.globl _EXTI_GetTLISensitivity
                                     17 ;--------------------------------------------------------
                                     18 ; ram data
                                     19 ;--------------------------------------------------------
                                     20 	.area DATA
                                     21 ;--------------------------------------------------------
                                     22 ; ram data
                                     23 ;--------------------------------------------------------
                                     24 	.area INITIALIZED
                                     25 ;--------------------------------------------------------
                                     26 ; absolute external ram data
                                     27 ;--------------------------------------------------------
                                     28 	.area DABS (ABS)
                                     29 
                                     30 ; default segment ordering for linker
                                     31 	.area HOME
                                     32 	.area GSINIT
                                     33 	.area GSFINAL
                                     34 	.area CONST
                                     35 	.area INITIALIZER
                                     36 	.area CODE
                                     37 
                                     38 ;--------------------------------------------------------
                                     39 ; global & static initialisations
                                     40 ;--------------------------------------------------------
                                     41 	.area HOME
                                     42 	.area GSINIT
                                     43 	.area GSFINAL
                                     44 	.area GSINIT
                                     45 ;--------------------------------------------------------
                                     46 ; Home
                                     47 ;--------------------------------------------------------
                                     48 	.area HOME
                                     49 	.area HOME
                                     50 ;--------------------------------------------------------
                                     51 ; code
                                     52 ;--------------------------------------------------------
                                     53 	.area CODE
                           000000    54 	Sstm8s_exti$EXTI_DeInit$0 ==.
                                     55 ;	drivers/src/stm8s_exti.c: 53: void EXTI_DeInit(void)
                                     56 ;	-----------------------------------------
                                     57 ;	 function EXTI_DeInit
                                     58 ;	-----------------------------------------
      008C06                         59 _EXTI_DeInit:
                           000000    60 	Sstm8s_exti$EXTI_DeInit$1 ==.
                           000000    61 	Sstm8s_exti$EXTI_DeInit$2 ==.
                                     62 ;	drivers/src/stm8s_exti.c: 55: EXTI->CR1 = EXTI_CR1_RESET_VALUE;
      008C06 35 00 50 A0      [ 1]   63 	mov	0x50a0+0, #0x00
                           000004    64 	Sstm8s_exti$EXTI_DeInit$3 ==.
                                     65 ;	drivers/src/stm8s_exti.c: 56: EXTI->CR2 = EXTI_CR2_RESET_VALUE;
      008C0A 35 00 50 A1      [ 1]   66 	mov	0x50a1+0, #0x00
                           000008    67 	Sstm8s_exti$EXTI_DeInit$4 ==.
                                     68 ;	drivers/src/stm8s_exti.c: 57: }
                           000008    69 	Sstm8s_exti$EXTI_DeInit$5 ==.
                           000008    70 	XG$EXTI_DeInit$0$0 ==.
      008C0E 81               [ 4]   71 	ret
                           000009    72 	Sstm8s_exti$EXTI_DeInit$6 ==.
                           000009    73 	Sstm8s_exti$EXTI_SetExtIntSensitivity$7 ==.
                                     74 ;	drivers/src/stm8s_exti.c: 70: void EXTI_SetExtIntSensitivity(EXTI_Port_TypeDef Port, EXTI_Sensitivity_TypeDef SensitivityValue)
                                     75 ;	-----------------------------------------
                                     76 ;	 function EXTI_SetExtIntSensitivity
                                     77 ;	-----------------------------------------
      008C0F                         78 _EXTI_SetExtIntSensitivity:
                           000009    79 	Sstm8s_exti$EXTI_SetExtIntSensitivity$8 ==.
      008C0F 88               [ 1]   80 	push	a
                           00000A    81 	Sstm8s_exti$EXTI_SetExtIntSensitivity$9 ==.
                           00000A    82 	Sstm8s_exti$EXTI_SetExtIntSensitivity$10 ==.
                                     83 ;	drivers/src/stm8s_exti.c: 73: assert_param(IS_EXTI_PORT_OK(Port));
      008C10 0D 04            [ 1]   84 	tnz	(0x04, sp)
      008C12 27 26            [ 1]   85 	jreq	00111$
      008C14 7B 04            [ 1]   86 	ld	a, (0x04, sp)
      008C16 4A               [ 1]   87 	dec	a
      008C17 27 21            [ 1]   88 	jreq	00111$
                           000013    89 	Sstm8s_exti$EXTI_SetExtIntSensitivity$11 ==.
      008C19 7B 04            [ 1]   90 	ld	a, (0x04, sp)
      008C1B A1 02            [ 1]   91 	cp	a, #0x02
      008C1D 27 1B            [ 1]   92 	jreq	00111$
                           000019    93 	Sstm8s_exti$EXTI_SetExtIntSensitivity$12 ==.
      008C1F 7B 04            [ 1]   94 	ld	a, (0x04, sp)
      008C21 A1 03            [ 1]   95 	cp	a, #0x03
      008C23 27 15            [ 1]   96 	jreq	00111$
                           00001F    97 	Sstm8s_exti$EXTI_SetExtIntSensitivity$13 ==.
      008C25 7B 04            [ 1]   98 	ld	a, (0x04, sp)
      008C27 A1 04            [ 1]   99 	cp	a, #0x04
      008C29 27 0F            [ 1]  100 	jreq	00111$
                           000025   101 	Sstm8s_exti$EXTI_SetExtIntSensitivity$14 ==.
      008C2B 4B 49            [ 1]  102 	push	#0x49
                           000027   103 	Sstm8s_exti$EXTI_SetExtIntSensitivity$15 ==.
      008C2D 5F               [ 1]  104 	clrw	x
      008C2E 89               [ 2]  105 	pushw	x
                           000029   106 	Sstm8s_exti$EXTI_SetExtIntSensitivity$16 ==.
      008C2F 4B 00            [ 1]  107 	push	#0x00
                           00002B   108 	Sstm8s_exti$EXTI_SetExtIntSensitivity$17 ==.
      008C31 4B FA            [ 1]  109 	push	#<(___str_0+0)
                           00002D   110 	Sstm8s_exti$EXTI_SetExtIntSensitivity$18 ==.
      008C33 4B 80            [ 1]  111 	push	#((___str_0+0) >> 8)
                           00002F   112 	Sstm8s_exti$EXTI_SetExtIntSensitivity$19 ==.
      008C35 CD 84 F3         [ 4]  113 	call	_assert_failed
      008C38 5B 06            [ 2]  114 	addw	sp, #6
                           000034   115 	Sstm8s_exti$EXTI_SetExtIntSensitivity$20 ==.
      008C3A                        116 00111$:
                           000034   117 	Sstm8s_exti$EXTI_SetExtIntSensitivity$21 ==.
                                    118 ;	drivers/src/stm8s_exti.c: 74: assert_param(IS_EXTI_SENSITIVITY_OK(SensitivityValue));
      008C3A 0D 05            [ 1]  119 	tnz	(0x05, sp)
      008C3C 27 20            [ 1]  120 	jreq	00125$
      008C3E 7B 05            [ 1]  121 	ld	a, (0x05, sp)
      008C40 4A               [ 1]  122 	dec	a
      008C41 27 1B            [ 1]  123 	jreq	00125$
                           00003D   124 	Sstm8s_exti$EXTI_SetExtIntSensitivity$22 ==.
      008C43 7B 05            [ 1]  125 	ld	a, (0x05, sp)
      008C45 A1 02            [ 1]  126 	cp	a, #0x02
      008C47 27 15            [ 1]  127 	jreq	00125$
                           000043   128 	Sstm8s_exti$EXTI_SetExtIntSensitivity$23 ==.
      008C49 7B 05            [ 1]  129 	ld	a, (0x05, sp)
      008C4B A1 03            [ 1]  130 	cp	a, #0x03
      008C4D 27 0F            [ 1]  131 	jreq	00125$
                           000049   132 	Sstm8s_exti$EXTI_SetExtIntSensitivity$24 ==.
      008C4F 4B 4A            [ 1]  133 	push	#0x4a
                           00004B   134 	Sstm8s_exti$EXTI_SetExtIntSensitivity$25 ==.
      008C51 5F               [ 1]  135 	clrw	x
      008C52 89               [ 2]  136 	pushw	x
                           00004D   137 	Sstm8s_exti$EXTI_SetExtIntSensitivity$26 ==.
      008C53 4B 00            [ 1]  138 	push	#0x00
                           00004F   139 	Sstm8s_exti$EXTI_SetExtIntSensitivity$27 ==.
      008C55 4B FA            [ 1]  140 	push	#<(___str_0+0)
                           000051   141 	Sstm8s_exti$EXTI_SetExtIntSensitivity$28 ==.
      008C57 4B 80            [ 1]  142 	push	#((___str_0+0) >> 8)
                           000053   143 	Sstm8s_exti$EXTI_SetExtIntSensitivity$29 ==.
      008C59 CD 84 F3         [ 4]  144 	call	_assert_failed
      008C5C 5B 06            [ 2]  145 	addw	sp, #6
                           000058   146 	Sstm8s_exti$EXTI_SetExtIntSensitivity$30 ==.
      008C5E                        147 00125$:
                           000058   148 	Sstm8s_exti$EXTI_SetExtIntSensitivity$31 ==.
                                    149 ;	drivers/src/stm8s_exti.c: 77: switch (Port)
      008C5E 7B 04            [ 1]  150 	ld	a, (0x04, sp)
      008C60 A1 04            [ 1]  151 	cp	a, #0x04
      008C62 23 03            [ 2]  152 	jrule	00208$
      008C64 CC 8C ED         [ 2]  153 	jp	00108$
      008C67                        154 00208$:
                           000061   155 	Sstm8s_exti$EXTI_SetExtIntSensitivity$32 ==.
                                    156 ;	drivers/src/stm8s_exti.c: 85: EXTI->CR1 |= (uint8_t)((uint8_t)(SensitivityValue) << 2);
      008C67 7B 05            [ 1]  157 	ld	a, (0x05, sp)
      008C69 90 97            [ 1]  158 	ld	yl, a
                           000065   159 	Sstm8s_exti$EXTI_SetExtIntSensitivity$33 ==.
                                    160 ;	drivers/src/stm8s_exti.c: 77: switch (Port)
      008C6B 5F               [ 1]  161 	clrw	x
      008C6C 7B 04            [ 1]  162 	ld	a, (0x04, sp)
      008C6E 97               [ 1]  163 	ld	xl, a
      008C6F 58               [ 2]  164 	sllw	x
      008C70 DE 8C 74         [ 2]  165 	ldw	x, (#00209$, x)
      008C73 FC               [ 2]  166 	jp	(x)
      008C74                        167 00209$:
      008C74 8C 7E                  168 	.dw	#00101$
      008C76 8C 91                  169 	.dw	#00102$
      008C78 8C A9                  170 	.dw	#00103$
      008C7A 8C C2                  171 	.dw	#00104$
      008C7C 8C DD                  172 	.dw	#00105$
                           000078   173 	Sstm8s_exti$EXTI_SetExtIntSensitivity$34 ==.
                           000078   174 	Sstm8s_exti$EXTI_SetExtIntSensitivity$35 ==.
                                    175 ;	drivers/src/stm8s_exti.c: 79: case EXTI_PORT_GPIOA:
      008C7E                        176 00101$:
                           000078   177 	Sstm8s_exti$EXTI_SetExtIntSensitivity$36 ==.
                                    178 ;	drivers/src/stm8s_exti.c: 80: EXTI->CR1 &= (uint8_t)(~EXTI_CR1_PAIS);
      008C7E C6 50 A0         [ 1]  179 	ld	a, 0x50a0
      008C81 A4 FC            [ 1]  180 	and	a, #0xfc
      008C83 C7 50 A0         [ 1]  181 	ld	0x50a0, a
                           000080   182 	Sstm8s_exti$EXTI_SetExtIntSensitivity$37 ==.
                                    183 ;	drivers/src/stm8s_exti.c: 81: EXTI->CR1 |= (uint8_t)(SensitivityValue);
      008C86 C6 50 A0         [ 1]  184 	ld	a, 0x50a0
      008C89 1A 05            [ 1]  185 	or	a, (0x05, sp)
      008C8B C7 50 A0         [ 1]  186 	ld	0x50a0, a
                           000088   187 	Sstm8s_exti$EXTI_SetExtIntSensitivity$38 ==.
                                    188 ;	drivers/src/stm8s_exti.c: 82: break;
      008C8E CC 8C ED         [ 2]  189 	jp	00108$
                           00008B   190 	Sstm8s_exti$EXTI_SetExtIntSensitivity$39 ==.
                                    191 ;	drivers/src/stm8s_exti.c: 83: case EXTI_PORT_GPIOB:
      008C91                        192 00102$:
                           00008B   193 	Sstm8s_exti$EXTI_SetExtIntSensitivity$40 ==.
                                    194 ;	drivers/src/stm8s_exti.c: 84: EXTI->CR1 &= (uint8_t)(~EXTI_CR1_PBIS);
      008C91 C6 50 A0         [ 1]  195 	ld	a, 0x50a0
      008C94 A4 F3            [ 1]  196 	and	a, #0xf3
      008C96 C7 50 A0         [ 1]  197 	ld	0x50a0, a
                           000093   198 	Sstm8s_exti$EXTI_SetExtIntSensitivity$41 ==.
                                    199 ;	drivers/src/stm8s_exti.c: 85: EXTI->CR1 |= (uint8_t)((uint8_t)(SensitivityValue) << 2);
      008C99 C6 50 A0         [ 1]  200 	ld	a, 0x50a0
      008C9C 93               [ 1]  201 	ldw	x, y
      008C9D 58               [ 2]  202 	sllw	x
      008C9E 58               [ 2]  203 	sllw	x
      008C9F 89               [ 2]  204 	pushw	x
                           00009A   205 	Sstm8s_exti$EXTI_SetExtIntSensitivity$42 ==.
      008CA0 1A 02            [ 1]  206 	or	a, (2, sp)
      008CA2 85               [ 2]  207 	popw	x
                           00009D   208 	Sstm8s_exti$EXTI_SetExtIntSensitivity$43 ==.
      008CA3 C7 50 A0         [ 1]  209 	ld	0x50a0, a
                           0000A0   210 	Sstm8s_exti$EXTI_SetExtIntSensitivity$44 ==.
                                    211 ;	drivers/src/stm8s_exti.c: 86: break;
      008CA6 CC 8C ED         [ 2]  212 	jp	00108$
                           0000A3   213 	Sstm8s_exti$EXTI_SetExtIntSensitivity$45 ==.
                                    214 ;	drivers/src/stm8s_exti.c: 87: case EXTI_PORT_GPIOC:
      008CA9                        215 00103$:
                           0000A3   216 	Sstm8s_exti$EXTI_SetExtIntSensitivity$46 ==.
                                    217 ;	drivers/src/stm8s_exti.c: 88: EXTI->CR1 &= (uint8_t)(~EXTI_CR1_PCIS);
      008CA9 C6 50 A0         [ 1]  218 	ld	a, 0x50a0
      008CAC A4 CF            [ 1]  219 	and	a, #0xcf
      008CAE C7 50 A0         [ 1]  220 	ld	0x50a0, a
                           0000AB   221 	Sstm8s_exti$EXTI_SetExtIntSensitivity$47 ==.
                                    222 ;	drivers/src/stm8s_exti.c: 89: EXTI->CR1 |= (uint8_t)((uint8_t)(SensitivityValue) << 4);
      008CB1 C6 50 A0         [ 1]  223 	ld	a, 0x50a0
      008CB4 6B 01            [ 1]  224 	ld	(0x01, sp), a
      008CB6 90 9F            [ 1]  225 	ld	a, yl
      008CB8 4E               [ 1]  226 	swap	a
      008CB9 A4 F0            [ 1]  227 	and	a, #0xf0
      008CBB 1A 01            [ 1]  228 	or	a, (0x01, sp)
      008CBD C7 50 A0         [ 1]  229 	ld	0x50a0, a
                           0000BA   230 	Sstm8s_exti$EXTI_SetExtIntSensitivity$48 ==.
                                    231 ;	drivers/src/stm8s_exti.c: 90: break;
      008CC0 20 2B            [ 2]  232 	jra	00108$
                           0000BC   233 	Sstm8s_exti$EXTI_SetExtIntSensitivity$49 ==.
                                    234 ;	drivers/src/stm8s_exti.c: 91: case EXTI_PORT_GPIOD:
      008CC2                        235 00104$:
                           0000BC   236 	Sstm8s_exti$EXTI_SetExtIntSensitivity$50 ==.
                                    237 ;	drivers/src/stm8s_exti.c: 92: EXTI->CR1 &= (uint8_t)(~EXTI_CR1_PDIS);
      008CC2 C6 50 A0         [ 1]  238 	ld	a, 0x50a0
      008CC5 A4 3F            [ 1]  239 	and	a, #0x3f
      008CC7 C7 50 A0         [ 1]  240 	ld	0x50a0, a
                           0000C4   241 	Sstm8s_exti$EXTI_SetExtIntSensitivity$51 ==.
                                    242 ;	drivers/src/stm8s_exti.c: 93: EXTI->CR1 |= (uint8_t)((uint8_t)(SensitivityValue) << 6);
      008CCA C6 50 A0         [ 1]  243 	ld	a, 0x50a0
      008CCD 6B 01            [ 1]  244 	ld	(0x01, sp), a
      008CCF 90 9F            [ 1]  245 	ld	a, yl
      008CD1 4E               [ 1]  246 	swap	a
      008CD2 A4 F0            [ 1]  247 	and	a, #0xf0
      008CD4 48               [ 1]  248 	sll	a
      008CD5 48               [ 1]  249 	sll	a
      008CD6 1A 01            [ 1]  250 	or	a, (0x01, sp)
      008CD8 C7 50 A0         [ 1]  251 	ld	0x50a0, a
                           0000D5   252 	Sstm8s_exti$EXTI_SetExtIntSensitivity$52 ==.
                                    253 ;	drivers/src/stm8s_exti.c: 94: break;
      008CDB 20 10            [ 2]  254 	jra	00108$
                           0000D7   255 	Sstm8s_exti$EXTI_SetExtIntSensitivity$53 ==.
                                    256 ;	drivers/src/stm8s_exti.c: 95: case EXTI_PORT_GPIOE:
      008CDD                        257 00105$:
                           0000D7   258 	Sstm8s_exti$EXTI_SetExtIntSensitivity$54 ==.
                                    259 ;	drivers/src/stm8s_exti.c: 96: EXTI->CR2 &= (uint8_t)(~EXTI_CR2_PEIS);
      008CDD C6 50 A1         [ 1]  260 	ld	a, 0x50a1
      008CE0 A4 FC            [ 1]  261 	and	a, #0xfc
      008CE2 C7 50 A1         [ 1]  262 	ld	0x50a1, a
                           0000DF   263 	Sstm8s_exti$EXTI_SetExtIntSensitivity$55 ==.
                                    264 ;	drivers/src/stm8s_exti.c: 97: EXTI->CR2 |= (uint8_t)(SensitivityValue);
      008CE5 C6 50 A1         [ 1]  265 	ld	a, 0x50a1
      008CE8 1A 05            [ 1]  266 	or	a, (0x05, sp)
      008CEA C7 50 A1         [ 1]  267 	ld	0x50a1, a
                           0000E7   268 	Sstm8s_exti$EXTI_SetExtIntSensitivity$56 ==.
                           0000E7   269 	Sstm8s_exti$EXTI_SetExtIntSensitivity$57 ==.
                                    270 ;	drivers/src/stm8s_exti.c: 101: }
      008CED                        271 00108$:
                           0000E7   272 	Sstm8s_exti$EXTI_SetExtIntSensitivity$58 ==.
                                    273 ;	drivers/src/stm8s_exti.c: 102: }
      008CED 84               [ 1]  274 	pop	a
                           0000E8   275 	Sstm8s_exti$EXTI_SetExtIntSensitivity$59 ==.
                           0000E8   276 	Sstm8s_exti$EXTI_SetExtIntSensitivity$60 ==.
                           0000E8   277 	XG$EXTI_SetExtIntSensitivity$0$0 ==.
      008CEE 81               [ 4]  278 	ret
                           0000E9   279 	Sstm8s_exti$EXTI_SetExtIntSensitivity$61 ==.
                           0000E9   280 	Sstm8s_exti$EXTI_SetTLISensitivity$62 ==.
                                    281 ;	drivers/src/stm8s_exti.c: 111: void EXTI_SetTLISensitivity(EXTI_TLISensitivity_TypeDef SensitivityValue)
                                    282 ;	-----------------------------------------
                                    283 ;	 function EXTI_SetTLISensitivity
                                    284 ;	-----------------------------------------
      008CEF                        285 _EXTI_SetTLISensitivity:
                           0000E9   286 	Sstm8s_exti$EXTI_SetTLISensitivity$63 ==.
                           0000E9   287 	Sstm8s_exti$EXTI_SetTLISensitivity$64 ==.
                                    288 ;	drivers/src/stm8s_exti.c: 114: assert_param(IS_EXTI_TLISENSITIVITY_OK(SensitivityValue));
      008CEF 0D 03            [ 1]  289 	tnz	(0x03, sp)
      008CF1 27 15            [ 1]  290 	jreq	00104$
      008CF3 7B 03            [ 1]  291 	ld	a, (0x03, sp)
      008CF5 A1 04            [ 1]  292 	cp	a, #0x04
      008CF7 27 0F            [ 1]  293 	jreq	00104$
                           0000F3   294 	Sstm8s_exti$EXTI_SetTLISensitivity$65 ==.
      008CF9 4B 72            [ 1]  295 	push	#0x72
                           0000F5   296 	Sstm8s_exti$EXTI_SetTLISensitivity$66 ==.
      008CFB 5F               [ 1]  297 	clrw	x
      008CFC 89               [ 2]  298 	pushw	x
                           0000F7   299 	Sstm8s_exti$EXTI_SetTLISensitivity$67 ==.
      008CFD 4B 00            [ 1]  300 	push	#0x00
                           0000F9   301 	Sstm8s_exti$EXTI_SetTLISensitivity$68 ==.
      008CFF 4B FA            [ 1]  302 	push	#<(___str_0+0)
                           0000FB   303 	Sstm8s_exti$EXTI_SetTLISensitivity$69 ==.
      008D01 4B 80            [ 1]  304 	push	#((___str_0+0) >> 8)
                           0000FD   305 	Sstm8s_exti$EXTI_SetTLISensitivity$70 ==.
      008D03 CD 84 F3         [ 4]  306 	call	_assert_failed
      008D06 5B 06            [ 2]  307 	addw	sp, #6
                           000102   308 	Sstm8s_exti$EXTI_SetTLISensitivity$71 ==.
      008D08                        309 00104$:
                           000102   310 	Sstm8s_exti$EXTI_SetTLISensitivity$72 ==.
                                    311 ;	drivers/src/stm8s_exti.c: 117: EXTI->CR2 &= (uint8_t)(~EXTI_CR2_TLIS);
      008D08 C6 50 A1         [ 1]  312 	ld	a, 0x50a1
      008D0B A4 FB            [ 1]  313 	and	a, #0xfb
      008D0D C7 50 A1         [ 1]  314 	ld	0x50a1, a
                           00010A   315 	Sstm8s_exti$EXTI_SetTLISensitivity$73 ==.
                                    316 ;	drivers/src/stm8s_exti.c: 118: EXTI->CR2 |= (uint8_t)(SensitivityValue);
      008D10 C6 50 A1         [ 1]  317 	ld	a, 0x50a1
      008D13 1A 03            [ 1]  318 	or	a, (0x03, sp)
      008D15 C7 50 A1         [ 1]  319 	ld	0x50a1, a
                           000112   320 	Sstm8s_exti$EXTI_SetTLISensitivity$74 ==.
                                    321 ;	drivers/src/stm8s_exti.c: 119: }
                           000112   322 	Sstm8s_exti$EXTI_SetTLISensitivity$75 ==.
                           000112   323 	XG$EXTI_SetTLISensitivity$0$0 ==.
      008D18 81               [ 4]  324 	ret
                           000113   325 	Sstm8s_exti$EXTI_SetTLISensitivity$76 ==.
                           000113   326 	Sstm8s_exti$EXTI_GetExtIntSensitivity$77 ==.
                                    327 ;	drivers/src/stm8s_exti.c: 126: EXTI_Sensitivity_TypeDef EXTI_GetExtIntSensitivity(EXTI_Port_TypeDef Port)
                                    328 ;	-----------------------------------------
                                    329 ;	 function EXTI_GetExtIntSensitivity
                                    330 ;	-----------------------------------------
      008D19                        331 _EXTI_GetExtIntSensitivity:
                           000113   332 	Sstm8s_exti$EXTI_GetExtIntSensitivity$78 ==.
                           000113   333 	Sstm8s_exti$EXTI_GetExtIntSensitivity$79 ==.
                                    334 ;	drivers/src/stm8s_exti.c: 128: uint8_t value = 0;
      008D19 4F               [ 1]  335 	clr	a
                           000114   336 	Sstm8s_exti$EXTI_GetExtIntSensitivity$80 ==.
                                    337 ;	drivers/src/stm8s_exti.c: 131: assert_param(IS_EXTI_PORT_OK(Port));
      008D1A 0D 03            [ 1]  338 	tnz	(0x03, sp)
      008D1C 26 03            [ 1]  339 	jrne	00154$
      008D1E CC 8D 54         [ 2]  340 	jp	00111$
      008D21                        341 00154$:
      008D21 88               [ 1]  342 	push	a
                           00011C   343 	Sstm8s_exti$EXTI_GetExtIntSensitivity$81 ==.
      008D22 7B 04            [ 1]  344 	ld	a, (0x04, sp)
      008D24 4A               [ 1]  345 	dec	a
      008D25 84               [ 1]  346 	pop	a
                           000120   347 	Sstm8s_exti$EXTI_GetExtIntSensitivity$82 ==.
      008D26 26 03            [ 1]  348 	jrne	00156$
      008D28 CC 8D 54         [ 2]  349 	jp	00111$
      008D2B                        350 00156$:
                           000125   351 	Sstm8s_exti$EXTI_GetExtIntSensitivity$83 ==.
      008D2B 88               [ 1]  352 	push	a
                           000126   353 	Sstm8s_exti$EXTI_GetExtIntSensitivity$84 ==.
      008D2C 7B 04            [ 1]  354 	ld	a, (0x04, sp)
      008D2E A1 02            [ 1]  355 	cp	a, #0x02
      008D30 84               [ 1]  356 	pop	a
                           00012B   357 	Sstm8s_exti$EXTI_GetExtIntSensitivity$85 ==.
      008D31 27 21            [ 1]  358 	jreq	00111$
                           00012D   359 	Sstm8s_exti$EXTI_GetExtIntSensitivity$86 ==.
      008D33 88               [ 1]  360 	push	a
                           00012E   361 	Sstm8s_exti$EXTI_GetExtIntSensitivity$87 ==.
      008D34 7B 04            [ 1]  362 	ld	a, (0x04, sp)
      008D36 A1 03            [ 1]  363 	cp	a, #0x03
      008D38 84               [ 1]  364 	pop	a
                           000133   365 	Sstm8s_exti$EXTI_GetExtIntSensitivity$88 ==.
      008D39 27 19            [ 1]  366 	jreq	00111$
                           000135   367 	Sstm8s_exti$EXTI_GetExtIntSensitivity$89 ==.
      008D3B 88               [ 1]  368 	push	a
                           000136   369 	Sstm8s_exti$EXTI_GetExtIntSensitivity$90 ==.
      008D3C 7B 04            [ 1]  370 	ld	a, (0x04, sp)
      008D3E A1 04            [ 1]  371 	cp	a, #0x04
      008D40 84               [ 1]  372 	pop	a
                           00013B   373 	Sstm8s_exti$EXTI_GetExtIntSensitivity$91 ==.
      008D41 27 11            [ 1]  374 	jreq	00111$
                           00013D   375 	Sstm8s_exti$EXTI_GetExtIntSensitivity$92 ==.
      008D43 88               [ 1]  376 	push	a
                           00013E   377 	Sstm8s_exti$EXTI_GetExtIntSensitivity$93 ==.
      008D44 4B 83            [ 1]  378 	push	#0x83
                           000140   379 	Sstm8s_exti$EXTI_GetExtIntSensitivity$94 ==.
      008D46 5F               [ 1]  380 	clrw	x
      008D47 89               [ 2]  381 	pushw	x
                           000142   382 	Sstm8s_exti$EXTI_GetExtIntSensitivity$95 ==.
      008D48 4B 00            [ 1]  383 	push	#0x00
                           000144   384 	Sstm8s_exti$EXTI_GetExtIntSensitivity$96 ==.
      008D4A 4B FA            [ 1]  385 	push	#<(___str_0+0)
                           000146   386 	Sstm8s_exti$EXTI_GetExtIntSensitivity$97 ==.
      008D4C 4B 80            [ 1]  387 	push	#((___str_0+0) >> 8)
                           000148   388 	Sstm8s_exti$EXTI_GetExtIntSensitivity$98 ==.
      008D4E CD 84 F3         [ 4]  389 	call	_assert_failed
      008D51 5B 06            [ 2]  390 	addw	sp, #6
                           00014D   391 	Sstm8s_exti$EXTI_GetExtIntSensitivity$99 ==.
      008D53 84               [ 1]  392 	pop	a
                           00014E   393 	Sstm8s_exti$EXTI_GetExtIntSensitivity$100 ==.
      008D54                        394 00111$:
                           00014E   395 	Sstm8s_exti$EXTI_GetExtIntSensitivity$101 ==.
                                    396 ;	drivers/src/stm8s_exti.c: 133: switch (Port)
      008D54 88               [ 1]  397 	push	a
                           00014F   398 	Sstm8s_exti$EXTI_GetExtIntSensitivity$102 ==.
      008D55 7B 04            [ 1]  399 	ld	a, (0x04, sp)
      008D57 A1 04            [ 1]  400 	cp	a, #0x04
      008D59 84               [ 1]  401 	pop	a
                           000154   402 	Sstm8s_exti$EXTI_GetExtIntSensitivity$103 ==.
      008D5A 23 03            [ 2]  403 	jrule	00167$
      008D5C CC 8D 9D         [ 2]  404 	jp	00107$
      008D5F                        405 00167$:
      008D5F 5F               [ 1]  406 	clrw	x
      008D60 7B 03            [ 1]  407 	ld	a, (0x03, sp)
      008D62 97               [ 1]  408 	ld	xl, a
      008D63 58               [ 2]  409 	sllw	x
      008D64 DE 8D 68         [ 2]  410 	ldw	x, (#00168$, x)
      008D67 FC               [ 2]  411 	jp	(x)
      008D68                        412 00168$:
      008D68 8D 72                  413 	.dw	#00101$
      008D6A 8D 79                  414 	.dw	#00102$
      008D6C 8D 82                  415 	.dw	#00103$
      008D6E 8D 8C                  416 	.dw	#00104$
      008D70 8D 98                  417 	.dw	#00105$
                           00016C   418 	Sstm8s_exti$EXTI_GetExtIntSensitivity$104 ==.
                           00016C   419 	Sstm8s_exti$EXTI_GetExtIntSensitivity$105 ==.
                                    420 ;	drivers/src/stm8s_exti.c: 135: case EXTI_PORT_GPIOA:
      008D72                        421 00101$:
                           00016C   422 	Sstm8s_exti$EXTI_GetExtIntSensitivity$106 ==.
                                    423 ;	drivers/src/stm8s_exti.c: 136: value = (uint8_t)(EXTI->CR1 & EXTI_CR1_PAIS);
      008D72 C6 50 A0         [ 1]  424 	ld	a, 0x50a0
      008D75 A4 03            [ 1]  425 	and	a, #0x03
                           000171   426 	Sstm8s_exti$EXTI_GetExtIntSensitivity$107 ==.
                                    427 ;	drivers/src/stm8s_exti.c: 137: break;
      008D77 20 24            [ 2]  428 	jra	00107$
                           000173   429 	Sstm8s_exti$EXTI_GetExtIntSensitivity$108 ==.
                                    430 ;	drivers/src/stm8s_exti.c: 138: case EXTI_PORT_GPIOB:
      008D79                        431 00102$:
                           000173   432 	Sstm8s_exti$EXTI_GetExtIntSensitivity$109 ==.
                                    433 ;	drivers/src/stm8s_exti.c: 139: value = (uint8_t)((uint8_t)(EXTI->CR1 & EXTI_CR1_PBIS) >> 2);
      008D79 C6 50 A0         [ 1]  434 	ld	a, 0x50a0
      008D7C A4 0C            [ 1]  435 	and	a, #0x0c
      008D7E 44               [ 1]  436 	srl	a
      008D7F 44               [ 1]  437 	srl	a
                           00017A   438 	Sstm8s_exti$EXTI_GetExtIntSensitivity$110 ==.
                                    439 ;	drivers/src/stm8s_exti.c: 140: break;
      008D80 20 1B            [ 2]  440 	jra	00107$
                           00017C   441 	Sstm8s_exti$EXTI_GetExtIntSensitivity$111 ==.
                                    442 ;	drivers/src/stm8s_exti.c: 141: case EXTI_PORT_GPIOC:
      008D82                        443 00103$:
                           00017C   444 	Sstm8s_exti$EXTI_GetExtIntSensitivity$112 ==.
                                    445 ;	drivers/src/stm8s_exti.c: 142: value = (uint8_t)((uint8_t)(EXTI->CR1 & EXTI_CR1_PCIS) >> 4);
      008D82 C6 50 A0         [ 1]  446 	ld	a, 0x50a0
      008D85 A4 30            [ 1]  447 	and	a, #0x30
      008D87 4E               [ 1]  448 	swap	a
      008D88 A4 0F            [ 1]  449 	and	a, #0x0f
                           000184   450 	Sstm8s_exti$EXTI_GetExtIntSensitivity$113 ==.
                                    451 ;	drivers/src/stm8s_exti.c: 143: break;
      008D8A 20 11            [ 2]  452 	jra	00107$
                           000186   453 	Sstm8s_exti$EXTI_GetExtIntSensitivity$114 ==.
                                    454 ;	drivers/src/stm8s_exti.c: 144: case EXTI_PORT_GPIOD:
      008D8C                        455 00104$:
                           000186   456 	Sstm8s_exti$EXTI_GetExtIntSensitivity$115 ==.
                                    457 ;	drivers/src/stm8s_exti.c: 145: value = (uint8_t)((uint8_t)(EXTI->CR1 & EXTI_CR1_PDIS) >> 6);
      008D8C C6 50 A0         [ 1]  458 	ld	a, 0x50a0
      008D8F A4 C0            [ 1]  459 	and	a, #0xc0
      008D91 4E               [ 1]  460 	swap	a
      008D92 A4 0F            [ 1]  461 	and	a, #0x0f
      008D94 44               [ 1]  462 	srl	a
      008D95 44               [ 1]  463 	srl	a
                           000190   464 	Sstm8s_exti$EXTI_GetExtIntSensitivity$116 ==.
                                    465 ;	drivers/src/stm8s_exti.c: 146: break;
      008D96 20 05            [ 2]  466 	jra	00107$
                           000192   467 	Sstm8s_exti$EXTI_GetExtIntSensitivity$117 ==.
                                    468 ;	drivers/src/stm8s_exti.c: 147: case EXTI_PORT_GPIOE:
      008D98                        469 00105$:
                           000192   470 	Sstm8s_exti$EXTI_GetExtIntSensitivity$118 ==.
                                    471 ;	drivers/src/stm8s_exti.c: 148: value = (uint8_t)(EXTI->CR2 & EXTI_CR2_PEIS);
      008D98 C6 50 A1         [ 1]  472 	ld	a, 0x50a1
      008D9B A4 03            [ 1]  473 	and	a, #0x03
                           000197   474 	Sstm8s_exti$EXTI_GetExtIntSensitivity$119 ==.
                           000197   475 	Sstm8s_exti$EXTI_GetExtIntSensitivity$120 ==.
                                    476 ;	drivers/src/stm8s_exti.c: 152: }
      008D9D                        477 00107$:
                           000197   478 	Sstm8s_exti$EXTI_GetExtIntSensitivity$121 ==.
                                    479 ;	drivers/src/stm8s_exti.c: 154: return((EXTI_Sensitivity_TypeDef)value);
                           000197   480 	Sstm8s_exti$EXTI_GetExtIntSensitivity$122 ==.
                                    481 ;	drivers/src/stm8s_exti.c: 155: }
                           000197   482 	Sstm8s_exti$EXTI_GetExtIntSensitivity$123 ==.
                           000197   483 	XG$EXTI_GetExtIntSensitivity$0$0 ==.
      008D9D 81               [ 4]  484 	ret
                           000198   485 	Sstm8s_exti$EXTI_GetExtIntSensitivity$124 ==.
                           000198   486 	Sstm8s_exti$EXTI_GetTLISensitivity$125 ==.
                                    487 ;	drivers/src/stm8s_exti.c: 162: EXTI_TLISensitivity_TypeDef EXTI_GetTLISensitivity(void)
                                    488 ;	-----------------------------------------
                                    489 ;	 function EXTI_GetTLISensitivity
                                    490 ;	-----------------------------------------
      008D9E                        491 _EXTI_GetTLISensitivity:
                           000198   492 	Sstm8s_exti$EXTI_GetTLISensitivity$126 ==.
                           000198   493 	Sstm8s_exti$EXTI_GetTLISensitivity$127 ==.
                                    494 ;	drivers/src/stm8s_exti.c: 167: value = (uint8_t)(EXTI->CR2 & EXTI_CR2_TLIS);
      008D9E C6 50 A1         [ 1]  495 	ld	a, 0x50a1
      008DA1 A4 04            [ 1]  496 	and	a, #0x04
                           00019D   497 	Sstm8s_exti$EXTI_GetTLISensitivity$128 ==.
                                    498 ;	drivers/src/stm8s_exti.c: 169: return((EXTI_TLISensitivity_TypeDef)value);
                           00019D   499 	Sstm8s_exti$EXTI_GetTLISensitivity$129 ==.
                                    500 ;	drivers/src/stm8s_exti.c: 170: }
                           00019D   501 	Sstm8s_exti$EXTI_GetTLISensitivity$130 ==.
                           00019D   502 	XG$EXTI_GetTLISensitivity$0$0 ==.
      008DA3 81               [ 4]  503 	ret
                           00019E   504 	Sstm8s_exti$EXTI_GetTLISensitivity$131 ==.
                                    505 	.area CODE
                                    506 	.area CONST
                           000000   507 Fstm8s_exti$__str_0$0_0$0 == .
                                    508 	.area CONST
      0080FA                        509 ___str_0:
      0080FA 64 72 69 76 65 72 73   510 	.ascii "drivers/src/stm8s_exti.c"
             2F 73 72 63 2F 73 74
             6D 38 73 5F 65 78 74
             69 2E 63
      008112 00                     511 	.db 0x00
                                    512 	.area CODE
                                    513 	.area INITIALIZER
                                    514 	.area CABS (ABS)
                                    515 
                                    516 	.area .debug_line (NOLOAD)
      000EBB 00 00 02 23            517 	.dw	0,Ldebug_line_end-Ldebug_line_start
      000EBF                        518 Ldebug_line_start:
      000EBF 00 02                  519 	.dw	2
      000EC1 00 00 00 79            520 	.dw	0,Ldebug_line_stmt-6-Ldebug_line_start
      000EC5 01                     521 	.db	1
      000EC6 01                     522 	.db	1
      000EC7 FB                     523 	.db	-5
      000EC8 0F                     524 	.db	15
      000EC9 0A                     525 	.db	10
      000ECA 00                     526 	.db	0
      000ECB 01                     527 	.db	1
      000ECC 01                     528 	.db	1
      000ECD 01                     529 	.db	1
      000ECE 01                     530 	.db	1
      000ECF 00                     531 	.db	0
      000ED0 00                     532 	.db	0
      000ED1 00                     533 	.db	0
      000ED2 01                     534 	.db	1
      000ED3 43 3A 5C 50 72 6F 67   535 	.ascii "C:\Program Files\SDCC\bin\..\include\stm8"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
             5C 73 74 6D 38
      000EFB 00                     536 	.db	0
      000EFC 43 3A 5C 50 72 6F 67   537 	.ascii "C:\Program Files\SDCC\bin\..\include"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
      000F1F 00                     538 	.db	0
      000F20 00                     539 	.db	0
      000F21 64 72 69 76 65 72 73   540 	.ascii "drivers/src/stm8s_exti.c"
             2F 73 72 63 2F 73 74
             6D 38 73 5F 65 78 74
             69 2E 63
      000F39 00                     541 	.db	0
      000F3A 00                     542 	.uleb128	0
      000F3B 00                     543 	.uleb128	0
      000F3C 00                     544 	.uleb128	0
      000F3D 00                     545 	.db	0
      000F3E                        546 Ldebug_line_stmt:
      000F3E 00                     547 	.db	0
      000F3F 05                     548 	.uleb128	5
      000F40 02                     549 	.db	2
      000F41 00 00 8C 06            550 	.dw	0,(Sstm8s_exti$EXTI_DeInit$0)
      000F45 03                     551 	.db	3
      000F46 34                     552 	.sleb128	52
      000F47 01                     553 	.db	1
      000F48 09                     554 	.db	9
      000F49 00 00                  555 	.dw	Sstm8s_exti$EXTI_DeInit$2-Sstm8s_exti$EXTI_DeInit$0
      000F4B 03                     556 	.db	3
      000F4C 02                     557 	.sleb128	2
      000F4D 01                     558 	.db	1
      000F4E 09                     559 	.db	9
      000F4F 00 04                  560 	.dw	Sstm8s_exti$EXTI_DeInit$3-Sstm8s_exti$EXTI_DeInit$2
      000F51 03                     561 	.db	3
      000F52 01                     562 	.sleb128	1
      000F53 01                     563 	.db	1
      000F54 09                     564 	.db	9
      000F55 00 04                  565 	.dw	Sstm8s_exti$EXTI_DeInit$4-Sstm8s_exti$EXTI_DeInit$3
      000F57 03                     566 	.db	3
      000F58 01                     567 	.sleb128	1
      000F59 01                     568 	.db	1
      000F5A 09                     569 	.db	9
      000F5B 00 01                  570 	.dw	1+Sstm8s_exti$EXTI_DeInit$5-Sstm8s_exti$EXTI_DeInit$4
      000F5D 00                     571 	.db	0
      000F5E 01                     572 	.uleb128	1
      000F5F 01                     573 	.db	1
      000F60 00                     574 	.db	0
      000F61 05                     575 	.uleb128	5
      000F62 02                     576 	.db	2
      000F63 00 00 8C 0F            577 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$7)
      000F67 03                     578 	.db	3
      000F68 C5 00                  579 	.sleb128	69
      000F6A 01                     580 	.db	1
      000F6B 09                     581 	.db	9
      000F6C 00 01                  582 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$10-Sstm8s_exti$EXTI_SetExtIntSensitivity$7
      000F6E 03                     583 	.db	3
      000F6F 03                     584 	.sleb128	3
      000F70 01                     585 	.db	1
      000F71 09                     586 	.db	9
      000F72 00 2A                  587 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$21-Sstm8s_exti$EXTI_SetExtIntSensitivity$10
      000F74 03                     588 	.db	3
      000F75 01                     589 	.sleb128	1
      000F76 01                     590 	.db	1
      000F77 09                     591 	.db	9
      000F78 00 24                  592 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$31-Sstm8s_exti$EXTI_SetExtIntSensitivity$21
      000F7A 03                     593 	.db	3
      000F7B 03                     594 	.sleb128	3
      000F7C 01                     595 	.db	1
      000F7D 09                     596 	.db	9
      000F7E 00 09                  597 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$32-Sstm8s_exti$EXTI_SetExtIntSensitivity$31
      000F80 03                     598 	.db	3
      000F81 08                     599 	.sleb128	8
      000F82 01                     600 	.db	1
      000F83 09                     601 	.db	9
      000F84 00 04                  602 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$33-Sstm8s_exti$EXTI_SetExtIntSensitivity$32
      000F86 03                     603 	.db	3
      000F87 78                     604 	.sleb128	-8
      000F88 01                     605 	.db	1
      000F89 09                     606 	.db	9
      000F8A 00 13                  607 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$35-Sstm8s_exti$EXTI_SetExtIntSensitivity$33
      000F8C 03                     608 	.db	3
      000F8D 02                     609 	.sleb128	2
      000F8E 01                     610 	.db	1
      000F8F 09                     611 	.db	9
      000F90 00 00                  612 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$36-Sstm8s_exti$EXTI_SetExtIntSensitivity$35
      000F92 03                     613 	.db	3
      000F93 01                     614 	.sleb128	1
      000F94 01                     615 	.db	1
      000F95 09                     616 	.db	9
      000F96 00 08                  617 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$37-Sstm8s_exti$EXTI_SetExtIntSensitivity$36
      000F98 03                     618 	.db	3
      000F99 01                     619 	.sleb128	1
      000F9A 01                     620 	.db	1
      000F9B 09                     621 	.db	9
      000F9C 00 08                  622 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$38-Sstm8s_exti$EXTI_SetExtIntSensitivity$37
      000F9E 03                     623 	.db	3
      000F9F 01                     624 	.sleb128	1
      000FA0 01                     625 	.db	1
      000FA1 09                     626 	.db	9
      000FA2 00 03                  627 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$39-Sstm8s_exti$EXTI_SetExtIntSensitivity$38
      000FA4 03                     628 	.db	3
      000FA5 01                     629 	.sleb128	1
      000FA6 01                     630 	.db	1
      000FA7 09                     631 	.db	9
      000FA8 00 00                  632 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$40-Sstm8s_exti$EXTI_SetExtIntSensitivity$39
      000FAA 03                     633 	.db	3
      000FAB 01                     634 	.sleb128	1
      000FAC 01                     635 	.db	1
      000FAD 09                     636 	.db	9
      000FAE 00 08                  637 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$41-Sstm8s_exti$EXTI_SetExtIntSensitivity$40
      000FB0 03                     638 	.db	3
      000FB1 01                     639 	.sleb128	1
      000FB2 01                     640 	.db	1
      000FB3 09                     641 	.db	9
      000FB4 00 0D                  642 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$44-Sstm8s_exti$EXTI_SetExtIntSensitivity$41
      000FB6 03                     643 	.db	3
      000FB7 01                     644 	.sleb128	1
      000FB8 01                     645 	.db	1
      000FB9 09                     646 	.db	9
      000FBA 00 03                  647 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$45-Sstm8s_exti$EXTI_SetExtIntSensitivity$44
      000FBC 03                     648 	.db	3
      000FBD 01                     649 	.sleb128	1
      000FBE 01                     650 	.db	1
      000FBF 09                     651 	.db	9
      000FC0 00 00                  652 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$46-Sstm8s_exti$EXTI_SetExtIntSensitivity$45
      000FC2 03                     653 	.db	3
      000FC3 01                     654 	.sleb128	1
      000FC4 01                     655 	.db	1
      000FC5 09                     656 	.db	9
      000FC6 00 08                  657 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$47-Sstm8s_exti$EXTI_SetExtIntSensitivity$46
      000FC8 03                     658 	.db	3
      000FC9 01                     659 	.sleb128	1
      000FCA 01                     660 	.db	1
      000FCB 09                     661 	.db	9
      000FCC 00 0F                  662 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$48-Sstm8s_exti$EXTI_SetExtIntSensitivity$47
      000FCE 03                     663 	.db	3
      000FCF 01                     664 	.sleb128	1
      000FD0 01                     665 	.db	1
      000FD1 09                     666 	.db	9
      000FD2 00 02                  667 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$49-Sstm8s_exti$EXTI_SetExtIntSensitivity$48
      000FD4 03                     668 	.db	3
      000FD5 01                     669 	.sleb128	1
      000FD6 01                     670 	.db	1
      000FD7 09                     671 	.db	9
      000FD8 00 00                  672 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$50-Sstm8s_exti$EXTI_SetExtIntSensitivity$49
      000FDA 03                     673 	.db	3
      000FDB 01                     674 	.sleb128	1
      000FDC 01                     675 	.db	1
      000FDD 09                     676 	.db	9
      000FDE 00 08                  677 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$51-Sstm8s_exti$EXTI_SetExtIntSensitivity$50
      000FE0 03                     678 	.db	3
      000FE1 01                     679 	.sleb128	1
      000FE2 01                     680 	.db	1
      000FE3 09                     681 	.db	9
      000FE4 00 11                  682 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$52-Sstm8s_exti$EXTI_SetExtIntSensitivity$51
      000FE6 03                     683 	.db	3
      000FE7 01                     684 	.sleb128	1
      000FE8 01                     685 	.db	1
      000FE9 09                     686 	.db	9
      000FEA 00 02                  687 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$53-Sstm8s_exti$EXTI_SetExtIntSensitivity$52
      000FEC 03                     688 	.db	3
      000FED 01                     689 	.sleb128	1
      000FEE 01                     690 	.db	1
      000FEF 09                     691 	.db	9
      000FF0 00 00                  692 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$54-Sstm8s_exti$EXTI_SetExtIntSensitivity$53
      000FF2 03                     693 	.db	3
      000FF3 01                     694 	.sleb128	1
      000FF4 01                     695 	.db	1
      000FF5 09                     696 	.db	9
      000FF6 00 08                  697 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$55-Sstm8s_exti$EXTI_SetExtIntSensitivity$54
      000FF8 03                     698 	.db	3
      000FF9 01                     699 	.sleb128	1
      000FFA 01                     700 	.db	1
      000FFB 09                     701 	.db	9
      000FFC 00 08                  702 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$57-Sstm8s_exti$EXTI_SetExtIntSensitivity$55
      000FFE 03                     703 	.db	3
      000FFF 04                     704 	.sleb128	4
      001000 01                     705 	.db	1
      001001 09                     706 	.db	9
      001002 00 00                  707 	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$58-Sstm8s_exti$EXTI_SetExtIntSensitivity$57
      001004 03                     708 	.db	3
      001005 01                     709 	.sleb128	1
      001006 01                     710 	.db	1
      001007 09                     711 	.db	9
      001008 00 02                  712 	.dw	1+Sstm8s_exti$EXTI_SetExtIntSensitivity$60-Sstm8s_exti$EXTI_SetExtIntSensitivity$58
      00100A 00                     713 	.db	0
      00100B 01                     714 	.uleb128	1
      00100C 01                     715 	.db	1
      00100D 00                     716 	.db	0
      00100E 05                     717 	.uleb128	5
      00100F 02                     718 	.db	2
      001010 00 00 8C EF            719 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$62)
      001014 03                     720 	.db	3
      001015 EE 00                  721 	.sleb128	110
      001017 01                     722 	.db	1
      001018 09                     723 	.db	9
      001019 00 00                  724 	.dw	Sstm8s_exti$EXTI_SetTLISensitivity$64-Sstm8s_exti$EXTI_SetTLISensitivity$62
      00101B 03                     725 	.db	3
      00101C 03                     726 	.sleb128	3
      00101D 01                     727 	.db	1
      00101E 09                     728 	.db	9
      00101F 00 19                  729 	.dw	Sstm8s_exti$EXTI_SetTLISensitivity$72-Sstm8s_exti$EXTI_SetTLISensitivity$64
      001021 03                     730 	.db	3
      001022 03                     731 	.sleb128	3
      001023 01                     732 	.db	1
      001024 09                     733 	.db	9
      001025 00 08                  734 	.dw	Sstm8s_exti$EXTI_SetTLISensitivity$73-Sstm8s_exti$EXTI_SetTLISensitivity$72
      001027 03                     735 	.db	3
      001028 01                     736 	.sleb128	1
      001029 01                     737 	.db	1
      00102A 09                     738 	.db	9
      00102B 00 08                  739 	.dw	Sstm8s_exti$EXTI_SetTLISensitivity$74-Sstm8s_exti$EXTI_SetTLISensitivity$73
      00102D 03                     740 	.db	3
      00102E 01                     741 	.sleb128	1
      00102F 01                     742 	.db	1
      001030 09                     743 	.db	9
      001031 00 01                  744 	.dw	1+Sstm8s_exti$EXTI_SetTLISensitivity$75-Sstm8s_exti$EXTI_SetTLISensitivity$74
      001033 00                     745 	.db	0
      001034 01                     746 	.uleb128	1
      001035 01                     747 	.db	1
      001036 00                     748 	.db	0
      001037 05                     749 	.uleb128	5
      001038 02                     750 	.db	2
      001039 00 00 8D 19            751 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$77)
      00103D 03                     752 	.db	3
      00103E FD 00                  753 	.sleb128	125
      001040 01                     754 	.db	1
      001041 09                     755 	.db	9
      001042 00 00                  756 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$79-Sstm8s_exti$EXTI_GetExtIntSensitivity$77
      001044 03                     757 	.db	3
      001045 02                     758 	.sleb128	2
      001046 01                     759 	.db	1
      001047 09                     760 	.db	9
      001048 00 01                  761 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$80-Sstm8s_exti$EXTI_GetExtIntSensitivity$79
      00104A 03                     762 	.db	3
      00104B 03                     763 	.sleb128	3
      00104C 01                     764 	.db	1
      00104D 09                     765 	.db	9
      00104E 00 3A                  766 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$101-Sstm8s_exti$EXTI_GetExtIntSensitivity$80
      001050 03                     767 	.db	3
      001051 02                     768 	.sleb128	2
      001052 01                     769 	.db	1
      001053 09                     770 	.db	9
      001054 00 1E                  771 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$105-Sstm8s_exti$EXTI_GetExtIntSensitivity$101
      001056 03                     772 	.db	3
      001057 02                     773 	.sleb128	2
      001058 01                     774 	.db	1
      001059 09                     775 	.db	9
      00105A 00 00                  776 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$106-Sstm8s_exti$EXTI_GetExtIntSensitivity$105
      00105C 03                     777 	.db	3
      00105D 01                     778 	.sleb128	1
      00105E 01                     779 	.db	1
      00105F 09                     780 	.db	9
      001060 00 05                  781 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$107-Sstm8s_exti$EXTI_GetExtIntSensitivity$106
      001062 03                     782 	.db	3
      001063 01                     783 	.sleb128	1
      001064 01                     784 	.db	1
      001065 09                     785 	.db	9
      001066 00 02                  786 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$108-Sstm8s_exti$EXTI_GetExtIntSensitivity$107
      001068 03                     787 	.db	3
      001069 01                     788 	.sleb128	1
      00106A 01                     789 	.db	1
      00106B 09                     790 	.db	9
      00106C 00 00                  791 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$109-Sstm8s_exti$EXTI_GetExtIntSensitivity$108
      00106E 03                     792 	.db	3
      00106F 01                     793 	.sleb128	1
      001070 01                     794 	.db	1
      001071 09                     795 	.db	9
      001072 00 07                  796 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$110-Sstm8s_exti$EXTI_GetExtIntSensitivity$109
      001074 03                     797 	.db	3
      001075 01                     798 	.sleb128	1
      001076 01                     799 	.db	1
      001077 09                     800 	.db	9
      001078 00 02                  801 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$111-Sstm8s_exti$EXTI_GetExtIntSensitivity$110
      00107A 03                     802 	.db	3
      00107B 01                     803 	.sleb128	1
      00107C 01                     804 	.db	1
      00107D 09                     805 	.db	9
      00107E 00 00                  806 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$112-Sstm8s_exti$EXTI_GetExtIntSensitivity$111
      001080 03                     807 	.db	3
      001081 01                     808 	.sleb128	1
      001082 01                     809 	.db	1
      001083 09                     810 	.db	9
      001084 00 08                  811 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$113-Sstm8s_exti$EXTI_GetExtIntSensitivity$112
      001086 03                     812 	.db	3
      001087 01                     813 	.sleb128	1
      001088 01                     814 	.db	1
      001089 09                     815 	.db	9
      00108A 00 02                  816 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$114-Sstm8s_exti$EXTI_GetExtIntSensitivity$113
      00108C 03                     817 	.db	3
      00108D 01                     818 	.sleb128	1
      00108E 01                     819 	.db	1
      00108F 09                     820 	.db	9
      001090 00 00                  821 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$115-Sstm8s_exti$EXTI_GetExtIntSensitivity$114
      001092 03                     822 	.db	3
      001093 01                     823 	.sleb128	1
      001094 01                     824 	.db	1
      001095 09                     825 	.db	9
      001096 00 0A                  826 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$116-Sstm8s_exti$EXTI_GetExtIntSensitivity$115
      001098 03                     827 	.db	3
      001099 01                     828 	.sleb128	1
      00109A 01                     829 	.db	1
      00109B 09                     830 	.db	9
      00109C 00 02                  831 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$117-Sstm8s_exti$EXTI_GetExtIntSensitivity$116
      00109E 03                     832 	.db	3
      00109F 01                     833 	.sleb128	1
      0010A0 01                     834 	.db	1
      0010A1 09                     835 	.db	9
      0010A2 00 00                  836 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$118-Sstm8s_exti$EXTI_GetExtIntSensitivity$117
      0010A4 03                     837 	.db	3
      0010A5 01                     838 	.sleb128	1
      0010A6 01                     839 	.db	1
      0010A7 09                     840 	.db	9
      0010A8 00 05                  841 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$120-Sstm8s_exti$EXTI_GetExtIntSensitivity$118
      0010AA 03                     842 	.db	3
      0010AB 04                     843 	.sleb128	4
      0010AC 01                     844 	.db	1
      0010AD 09                     845 	.db	9
      0010AE 00 00                  846 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$121-Sstm8s_exti$EXTI_GetExtIntSensitivity$120
      0010B0 03                     847 	.db	3
      0010B1 02                     848 	.sleb128	2
      0010B2 01                     849 	.db	1
      0010B3 09                     850 	.db	9
      0010B4 00 00                  851 	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$122-Sstm8s_exti$EXTI_GetExtIntSensitivity$121
      0010B6 03                     852 	.db	3
      0010B7 01                     853 	.sleb128	1
      0010B8 01                     854 	.db	1
      0010B9 09                     855 	.db	9
      0010BA 00 01                  856 	.dw	1+Sstm8s_exti$EXTI_GetExtIntSensitivity$123-Sstm8s_exti$EXTI_GetExtIntSensitivity$122
      0010BC 00                     857 	.db	0
      0010BD 01                     858 	.uleb128	1
      0010BE 01                     859 	.db	1
      0010BF 00                     860 	.db	0
      0010C0 05                     861 	.uleb128	5
      0010C1 02                     862 	.db	2
      0010C2 00 00 8D 9E            863 	.dw	0,(Sstm8s_exti$EXTI_GetTLISensitivity$125)
      0010C6 03                     864 	.db	3
      0010C7 A1 01                  865 	.sleb128	161
      0010C9 01                     866 	.db	1
      0010CA 09                     867 	.db	9
      0010CB 00 00                  868 	.dw	Sstm8s_exti$EXTI_GetTLISensitivity$127-Sstm8s_exti$EXTI_GetTLISensitivity$125
      0010CD 03                     869 	.db	3
      0010CE 05                     870 	.sleb128	5
      0010CF 01                     871 	.db	1
      0010D0 09                     872 	.db	9
      0010D1 00 05                  873 	.dw	Sstm8s_exti$EXTI_GetTLISensitivity$128-Sstm8s_exti$EXTI_GetTLISensitivity$127
      0010D3 03                     874 	.db	3
      0010D4 02                     875 	.sleb128	2
      0010D5 01                     876 	.db	1
      0010D6 09                     877 	.db	9
      0010D7 00 00                  878 	.dw	Sstm8s_exti$EXTI_GetTLISensitivity$129-Sstm8s_exti$EXTI_GetTLISensitivity$128
      0010D9 03                     879 	.db	3
      0010DA 01                     880 	.sleb128	1
      0010DB 01                     881 	.db	1
      0010DC 09                     882 	.db	9
      0010DD 00 01                  883 	.dw	1+Sstm8s_exti$EXTI_GetTLISensitivity$130-Sstm8s_exti$EXTI_GetTLISensitivity$129
      0010DF 00                     884 	.db	0
      0010E0 01                     885 	.uleb128	1
      0010E1 01                     886 	.db	1
      0010E2                        887 Ldebug_line_end:
                                    888 
                                    889 	.area .debug_loc (NOLOAD)
      001A68                        890 Ldebug_loc_start:
      001A68 00 00 8D 9E            891 	.dw	0,(Sstm8s_exti$EXTI_GetTLISensitivity$126)
      001A6C 00 00 8D A4            892 	.dw	0,(Sstm8s_exti$EXTI_GetTLISensitivity$131)
      001A70 00 02                  893 	.dw	2
      001A72 78                     894 	.db	120
      001A73 01                     895 	.sleb128	1
      001A74 00 00 00 00            896 	.dw	0,0
      001A78 00 00 00 00            897 	.dw	0,0
      001A7C 00 00 8D 5A            898 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$103)
      001A80 00 00 8D 9E            899 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$124)
      001A84 00 02                  900 	.dw	2
      001A86 78                     901 	.db	120
      001A87 01                     902 	.sleb128	1
      001A88 00 00 8D 55            903 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$102)
      001A8C 00 00 8D 5A            904 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$103)
      001A90 00 02                  905 	.dw	2
      001A92 78                     906 	.db	120
      001A93 02                     907 	.sleb128	2
      001A94 00 00 8D 54            908 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$100)
      001A98 00 00 8D 55            909 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$102)
      001A9C 00 02                  910 	.dw	2
      001A9E 78                     911 	.db	120
      001A9F 01                     912 	.sleb128	1
      001AA0 00 00 8D 53            913 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$99)
      001AA4 00 00 8D 54            914 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$100)
      001AA8 00 02                  915 	.dw	2
      001AAA 78                     916 	.db	120
      001AAB 02                     917 	.sleb128	2
      001AAC 00 00 8D 4E            918 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$98)
      001AB0 00 00 8D 53            919 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$99)
      001AB4 00 02                  920 	.dw	2
      001AB6 78                     921 	.db	120
      001AB7 08                     922 	.sleb128	8
      001AB8 00 00 8D 4C            923 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$97)
      001ABC 00 00 8D 4E            924 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$98)
      001AC0 00 02                  925 	.dw	2
      001AC2 78                     926 	.db	120
      001AC3 07                     927 	.sleb128	7
      001AC4 00 00 8D 4A            928 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$96)
      001AC8 00 00 8D 4C            929 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$97)
      001ACC 00 02                  930 	.dw	2
      001ACE 78                     931 	.db	120
      001ACF 06                     932 	.sleb128	6
      001AD0 00 00 8D 48            933 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$95)
      001AD4 00 00 8D 4A            934 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$96)
      001AD8 00 02                  935 	.dw	2
      001ADA 78                     936 	.db	120
      001ADB 05                     937 	.sleb128	5
      001ADC 00 00 8D 46            938 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$94)
      001AE0 00 00 8D 48            939 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$95)
      001AE4 00 02                  940 	.dw	2
      001AE6 78                     941 	.db	120
      001AE7 03                     942 	.sleb128	3
      001AE8 00 00 8D 44            943 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$93)
      001AEC 00 00 8D 46            944 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$94)
      001AF0 00 02                  945 	.dw	2
      001AF2 78                     946 	.db	120
      001AF3 02                     947 	.sleb128	2
      001AF4 00 00 8D 43            948 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$92)
      001AF8 00 00 8D 44            949 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$93)
      001AFC 00 02                  950 	.dw	2
      001AFE 78                     951 	.db	120
      001AFF 01                     952 	.sleb128	1
      001B00 00 00 8D 41            953 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$91)
      001B04 00 00 8D 43            954 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$92)
      001B08 00 02                  955 	.dw	2
      001B0A 78                     956 	.db	120
      001B0B 01                     957 	.sleb128	1
      001B0C 00 00 8D 3C            958 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$90)
      001B10 00 00 8D 41            959 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$91)
      001B14 00 02                  960 	.dw	2
      001B16 78                     961 	.db	120
      001B17 02                     962 	.sleb128	2
      001B18 00 00 8D 3B            963 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$89)
      001B1C 00 00 8D 3C            964 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$90)
      001B20 00 02                  965 	.dw	2
      001B22 78                     966 	.db	120
      001B23 01                     967 	.sleb128	1
      001B24 00 00 8D 39            968 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$88)
      001B28 00 00 8D 3B            969 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$89)
      001B2C 00 02                  970 	.dw	2
      001B2E 78                     971 	.db	120
      001B2F 01                     972 	.sleb128	1
      001B30 00 00 8D 34            973 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$87)
      001B34 00 00 8D 39            974 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$88)
      001B38 00 02                  975 	.dw	2
      001B3A 78                     976 	.db	120
      001B3B 02                     977 	.sleb128	2
      001B3C 00 00 8D 33            978 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$86)
      001B40 00 00 8D 34            979 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$87)
      001B44 00 02                  980 	.dw	2
      001B46 78                     981 	.db	120
      001B47 01                     982 	.sleb128	1
      001B48 00 00 8D 31            983 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$85)
      001B4C 00 00 8D 33            984 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$86)
      001B50 00 02                  985 	.dw	2
      001B52 78                     986 	.db	120
      001B53 01                     987 	.sleb128	1
      001B54 00 00 8D 2C            988 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$84)
      001B58 00 00 8D 31            989 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$85)
      001B5C 00 02                  990 	.dw	2
      001B5E 78                     991 	.db	120
      001B5F 02                     992 	.sleb128	2
      001B60 00 00 8D 2B            993 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$83)
      001B64 00 00 8D 2C            994 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$84)
      001B68 00 02                  995 	.dw	2
      001B6A 78                     996 	.db	120
      001B6B 01                     997 	.sleb128	1
      001B6C 00 00 8D 26            998 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$82)
      001B70 00 00 8D 2B            999 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$83)
      001B74 00 02                 1000 	.dw	2
      001B76 78                    1001 	.db	120
      001B77 01                    1002 	.sleb128	1
      001B78 00 00 8D 22           1003 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$81)
      001B7C 00 00 8D 26           1004 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$82)
      001B80 00 02                 1005 	.dw	2
      001B82 78                    1006 	.db	120
      001B83 02                    1007 	.sleb128	2
      001B84 00 00 8D 19           1008 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$78)
      001B88 00 00 8D 22           1009 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$81)
      001B8C 00 02                 1010 	.dw	2
      001B8E 78                    1011 	.db	120
      001B8F 01                    1012 	.sleb128	1
      001B90 00 00 00 00           1013 	.dw	0,0
      001B94 00 00 00 00           1014 	.dw	0,0
      001B98 00 00 8D 08           1015 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$71)
      001B9C 00 00 8D 19           1016 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$76)
      001BA0 00 02                 1017 	.dw	2
      001BA2 78                    1018 	.db	120
      001BA3 01                    1019 	.sleb128	1
      001BA4 00 00 8D 03           1020 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$70)
      001BA8 00 00 8D 08           1021 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$71)
      001BAC 00 02                 1022 	.dw	2
      001BAE 78                    1023 	.db	120
      001BAF 07                    1024 	.sleb128	7
      001BB0 00 00 8D 01           1025 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$69)
      001BB4 00 00 8D 03           1026 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$70)
      001BB8 00 02                 1027 	.dw	2
      001BBA 78                    1028 	.db	120
      001BBB 06                    1029 	.sleb128	6
      001BBC 00 00 8C FF           1030 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$68)
      001BC0 00 00 8D 01           1031 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$69)
      001BC4 00 02                 1032 	.dw	2
      001BC6 78                    1033 	.db	120
      001BC7 05                    1034 	.sleb128	5
      001BC8 00 00 8C FD           1035 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$67)
      001BCC 00 00 8C FF           1036 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$68)
      001BD0 00 02                 1037 	.dw	2
      001BD2 78                    1038 	.db	120
      001BD3 04                    1039 	.sleb128	4
      001BD4 00 00 8C FB           1040 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$66)
      001BD8 00 00 8C FD           1041 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$67)
      001BDC 00 02                 1042 	.dw	2
      001BDE 78                    1043 	.db	120
      001BDF 02                    1044 	.sleb128	2
      001BE0 00 00 8C F9           1045 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$65)
      001BE4 00 00 8C FB           1046 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$66)
      001BE8 00 02                 1047 	.dw	2
      001BEA 78                    1048 	.db	120
      001BEB 01                    1049 	.sleb128	1
      001BEC 00 00 8C EF           1050 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$63)
      001BF0 00 00 8C F9           1051 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$65)
      001BF4 00 02                 1052 	.dw	2
      001BF6 78                    1053 	.db	120
      001BF7 01                    1054 	.sleb128	1
      001BF8 00 00 00 00           1055 	.dw	0,0
      001BFC 00 00 00 00           1056 	.dw	0,0
      001C00 00 00 8C EE           1057 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$59)
      001C04 00 00 8C EF           1058 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$61)
      001C08 00 02                 1059 	.dw	2
      001C0A 78                    1060 	.db	120
      001C0B 01                    1061 	.sleb128	1
      001C0C 00 00 8C A3           1062 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$43)
      001C10 00 00 8C EE           1063 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$59)
      001C14 00 02                 1064 	.dw	2
      001C16 78                    1065 	.db	120
      001C17 02                    1066 	.sleb128	2
      001C18 00 00 8C A0           1067 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$42)
      001C1C 00 00 8C A3           1068 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$43)
      001C20 00 02                 1069 	.dw	2
      001C22 78                    1070 	.db	120
      001C23 04                    1071 	.sleb128	4
      001C24 00 00 8C 5E           1072 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$30)
      001C28 00 00 8C A0           1073 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$42)
      001C2C 00 02                 1074 	.dw	2
      001C2E 78                    1075 	.db	120
      001C2F 02                    1076 	.sleb128	2
      001C30 00 00 8C 59           1077 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$29)
      001C34 00 00 8C 5E           1078 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$30)
      001C38 00 02                 1079 	.dw	2
      001C3A 78                    1080 	.db	120
      001C3B 08                    1081 	.sleb128	8
      001C3C 00 00 8C 57           1082 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$28)
      001C40 00 00 8C 59           1083 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$29)
      001C44 00 02                 1084 	.dw	2
      001C46 78                    1085 	.db	120
      001C47 07                    1086 	.sleb128	7
      001C48 00 00 8C 55           1087 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$27)
      001C4C 00 00 8C 57           1088 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$28)
      001C50 00 02                 1089 	.dw	2
      001C52 78                    1090 	.db	120
      001C53 06                    1091 	.sleb128	6
      001C54 00 00 8C 53           1092 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$26)
      001C58 00 00 8C 55           1093 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$27)
      001C5C 00 02                 1094 	.dw	2
      001C5E 78                    1095 	.db	120
      001C5F 05                    1096 	.sleb128	5
      001C60 00 00 8C 51           1097 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$25)
      001C64 00 00 8C 53           1098 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$26)
      001C68 00 02                 1099 	.dw	2
      001C6A 78                    1100 	.db	120
      001C6B 03                    1101 	.sleb128	3
      001C6C 00 00 8C 4F           1102 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$24)
      001C70 00 00 8C 51           1103 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$25)
      001C74 00 02                 1104 	.dw	2
      001C76 78                    1105 	.db	120
      001C77 02                    1106 	.sleb128	2
      001C78 00 00 8C 49           1107 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$23)
      001C7C 00 00 8C 4F           1108 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$24)
      001C80 00 02                 1109 	.dw	2
      001C82 78                    1110 	.db	120
      001C83 02                    1111 	.sleb128	2
      001C84 00 00 8C 43           1112 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$22)
      001C88 00 00 8C 49           1113 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$23)
      001C8C 00 02                 1114 	.dw	2
      001C8E 78                    1115 	.db	120
      001C8F 02                    1116 	.sleb128	2
      001C90 00 00 8C 3A           1117 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$20)
      001C94 00 00 8C 43           1118 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$22)
      001C98 00 02                 1119 	.dw	2
      001C9A 78                    1120 	.db	120
      001C9B 02                    1121 	.sleb128	2
      001C9C 00 00 8C 35           1122 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$19)
      001CA0 00 00 8C 3A           1123 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$20)
      001CA4 00 02                 1124 	.dw	2
      001CA6 78                    1125 	.db	120
      001CA7 08                    1126 	.sleb128	8
      001CA8 00 00 8C 33           1127 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$18)
      001CAC 00 00 8C 35           1128 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$19)
      001CB0 00 02                 1129 	.dw	2
      001CB2 78                    1130 	.db	120
      001CB3 07                    1131 	.sleb128	7
      001CB4 00 00 8C 31           1132 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$17)
      001CB8 00 00 8C 33           1133 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$18)
      001CBC 00 02                 1134 	.dw	2
      001CBE 78                    1135 	.db	120
      001CBF 06                    1136 	.sleb128	6
      001CC0 00 00 8C 2F           1137 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$16)
      001CC4 00 00 8C 31           1138 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$17)
      001CC8 00 02                 1139 	.dw	2
      001CCA 78                    1140 	.db	120
      001CCB 05                    1141 	.sleb128	5
      001CCC 00 00 8C 2D           1142 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$15)
      001CD0 00 00 8C 2F           1143 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$16)
      001CD4 00 02                 1144 	.dw	2
      001CD6 78                    1145 	.db	120
      001CD7 03                    1146 	.sleb128	3
      001CD8 00 00 8C 2B           1147 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$14)
      001CDC 00 00 8C 2D           1148 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$15)
      001CE0 00 02                 1149 	.dw	2
      001CE2 78                    1150 	.db	120
      001CE3 02                    1151 	.sleb128	2
      001CE4 00 00 8C 25           1152 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$13)
      001CE8 00 00 8C 2B           1153 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$14)
      001CEC 00 02                 1154 	.dw	2
      001CEE 78                    1155 	.db	120
      001CEF 02                    1156 	.sleb128	2
      001CF0 00 00 8C 1F           1157 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$12)
      001CF4 00 00 8C 25           1158 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$13)
      001CF8 00 02                 1159 	.dw	2
      001CFA 78                    1160 	.db	120
      001CFB 02                    1161 	.sleb128	2
      001CFC 00 00 8C 19           1162 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$11)
      001D00 00 00 8C 1F           1163 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$12)
      001D04 00 02                 1164 	.dw	2
      001D06 78                    1165 	.db	120
      001D07 02                    1166 	.sleb128	2
      001D08 00 00 8C 10           1167 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$9)
      001D0C 00 00 8C 19           1168 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$11)
      001D10 00 02                 1169 	.dw	2
      001D12 78                    1170 	.db	120
      001D13 02                    1171 	.sleb128	2
      001D14 00 00 8C 0F           1172 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$8)
      001D18 00 00 8C 10           1173 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$9)
      001D1C 00 02                 1174 	.dw	2
      001D1E 78                    1175 	.db	120
      001D1F 01                    1176 	.sleb128	1
      001D20 00 00 00 00           1177 	.dw	0,0
      001D24 00 00 00 00           1178 	.dw	0,0
      001D28 00 00 8C 06           1179 	.dw	0,(Sstm8s_exti$EXTI_DeInit$1)
      001D2C 00 00 8C 0F           1180 	.dw	0,(Sstm8s_exti$EXTI_DeInit$6)
      001D30 00 02                 1181 	.dw	2
      001D32 78                    1182 	.db	120
      001D33 01                    1183 	.sleb128	1
      001D34 00 00 00 00           1184 	.dw	0,0
      001D38 00 00 00 00           1185 	.dw	0,0
                                   1186 
                                   1187 	.area .debug_abbrev (NOLOAD)
      0002CF                       1188 Ldebug_abbrev:
      0002CF 04                    1189 	.uleb128	4
      0002D0 05                    1190 	.uleb128	5
      0002D1 00                    1191 	.db	0
      0002D2 02                    1192 	.uleb128	2
      0002D3 0A                    1193 	.uleb128	10
      0002D4 03                    1194 	.uleb128	3
      0002D5 08                    1195 	.uleb128	8
      0002D6 49                    1196 	.uleb128	73
      0002D7 13                    1197 	.uleb128	19
      0002D8 00                    1198 	.uleb128	0
      0002D9 00                    1199 	.uleb128	0
      0002DA 0A                    1200 	.uleb128	10
      0002DB 01                    1201 	.uleb128	1
      0002DC 01                    1202 	.db	1
      0002DD 01                    1203 	.uleb128	1
      0002DE 13                    1204 	.uleb128	19
      0002DF 0B                    1205 	.uleb128	11
      0002E0 0B                    1206 	.uleb128	11
      0002E1 49                    1207 	.uleb128	73
      0002E2 13                    1208 	.uleb128	19
      0002E3 00                    1209 	.uleb128	0
      0002E4 00                    1210 	.uleb128	0
      0002E5 03                    1211 	.uleb128	3
      0002E6 2E                    1212 	.uleb128	46
      0002E7 01                    1213 	.db	1
      0002E8 01                    1214 	.uleb128	1
      0002E9 13                    1215 	.uleb128	19
      0002EA 03                    1216 	.uleb128	3
      0002EB 08                    1217 	.uleb128	8
      0002EC 11                    1218 	.uleb128	17
      0002ED 01                    1219 	.uleb128	1
      0002EE 12                    1220 	.uleb128	18
      0002EF 01                    1221 	.uleb128	1
      0002F0 3F                    1222 	.uleb128	63
      0002F1 0C                    1223 	.uleb128	12
      0002F2 40                    1224 	.uleb128	64
      0002F3 06                    1225 	.uleb128	6
      0002F4 00                    1226 	.uleb128	0
      0002F5 00                    1227 	.uleb128	0
      0002F6 08                    1228 	.uleb128	8
      0002F7 34                    1229 	.uleb128	52
      0002F8 00                    1230 	.db	0
      0002F9 02                    1231 	.uleb128	2
      0002FA 0A                    1232 	.uleb128	10
      0002FB 03                    1233 	.uleb128	3
      0002FC 08                    1234 	.uleb128	8
      0002FD 49                    1235 	.uleb128	73
      0002FE 13                    1236 	.uleb128	19
      0002FF 00                    1237 	.uleb128	0
      000300 00                    1238 	.uleb128	0
      000301 07                    1239 	.uleb128	7
      000302 2E                    1240 	.uleb128	46
      000303 01                    1241 	.db	1
      000304 01                    1242 	.uleb128	1
      000305 13                    1243 	.uleb128	19
      000306 03                    1244 	.uleb128	3
      000307 08                    1245 	.uleb128	8
      000308 11                    1246 	.uleb128	17
      000309 01                    1247 	.uleb128	1
      00030A 12                    1248 	.uleb128	18
      00030B 01                    1249 	.uleb128	1
      00030C 3F                    1250 	.uleb128	63
      00030D 0C                    1251 	.uleb128	12
      00030E 40                    1252 	.uleb128	64
      00030F 06                    1253 	.uleb128	6
      000310 49                    1254 	.uleb128	73
      000311 13                    1255 	.uleb128	19
      000312 00                    1256 	.uleb128	0
      000313 00                    1257 	.uleb128	0
      000314 09                    1258 	.uleb128	9
      000315 26                    1259 	.uleb128	38
      000316 00                    1260 	.db	0
      000317 49                    1261 	.uleb128	73
      000318 13                    1262 	.uleb128	19
      000319 00                    1263 	.uleb128	0
      00031A 00                    1264 	.uleb128	0
      00031B 01                    1265 	.uleb128	1
      00031C 11                    1266 	.uleb128	17
      00031D 01                    1267 	.db	1
      00031E 03                    1268 	.uleb128	3
      00031F 08                    1269 	.uleb128	8
      000320 10                    1270 	.uleb128	16
      000321 06                    1271 	.uleb128	6
      000322 13                    1272 	.uleb128	19
      000323 0B                    1273 	.uleb128	11
      000324 25                    1274 	.uleb128	37
      000325 08                    1275 	.uleb128	8
      000326 00                    1276 	.uleb128	0
      000327 00                    1277 	.uleb128	0
      000328 05                    1278 	.uleb128	5
      000329 0B                    1279 	.uleb128	11
      00032A 00                    1280 	.db	0
      00032B 11                    1281 	.uleb128	17
      00032C 01                    1282 	.uleb128	1
      00032D 12                    1283 	.uleb128	18
      00032E 01                    1284 	.uleb128	1
      00032F 00                    1285 	.uleb128	0
      000330 00                    1286 	.uleb128	0
      000331 02                    1287 	.uleb128	2
      000332 2E                    1288 	.uleb128	46
      000333 00                    1289 	.db	0
      000334 03                    1290 	.uleb128	3
      000335 08                    1291 	.uleb128	8
      000336 11                    1292 	.uleb128	17
      000337 01                    1293 	.uleb128	1
      000338 12                    1294 	.uleb128	18
      000339 01                    1295 	.uleb128	1
      00033A 3F                    1296 	.uleb128	63
      00033B 0C                    1297 	.uleb128	12
      00033C 40                    1298 	.uleb128	64
      00033D 06                    1299 	.uleb128	6
      00033E 00                    1300 	.uleb128	0
      00033F 00                    1301 	.uleb128	0
      000340 0B                    1302 	.uleb128	11
      000341 21                    1303 	.uleb128	33
      000342 00                    1304 	.db	0
      000343 2F                    1305 	.uleb128	47
      000344 0B                    1306 	.uleb128	11
      000345 00                    1307 	.uleb128	0
      000346 00                    1308 	.uleb128	0
      000347 06                    1309 	.uleb128	6
      000348 24                    1310 	.uleb128	36
      000349 00                    1311 	.db	0
      00034A 03                    1312 	.uleb128	3
      00034B 08                    1313 	.uleb128	8
      00034C 0B                    1314 	.uleb128	11
      00034D 0B                    1315 	.uleb128	11
      00034E 3E                    1316 	.uleb128	62
      00034F 0B                    1317 	.uleb128	11
      000350 00                    1318 	.uleb128	0
      000351 00                    1319 	.uleb128	0
      000352 00                    1320 	.uleb128	0
                                   1321 
                                   1322 	.area .debug_info (NOLOAD)
      001223 00 00 01 C1           1323 	.dw	0,Ldebug_info_end-Ldebug_info_start
      001227                       1324 Ldebug_info_start:
      001227 00 02                 1325 	.dw	2
      001229 00 00 02 CF           1326 	.dw	0,(Ldebug_abbrev)
      00122D 04                    1327 	.db	4
      00122E 01                    1328 	.uleb128	1
      00122F 64 72 69 76 65 72 73  1329 	.ascii "drivers/src/stm8s_exti.c"
             2F 73 72 63 2F 73 74
             6D 38 73 5F 65 78 74
             69 2E 63
      001247 00                    1330 	.db	0
      001248 00 00 0E BB           1331 	.dw	0,(Ldebug_line_start+-4)
      00124C 01                    1332 	.db	1
      00124D 53 44 43 43 20 76 65  1333 	.ascii "SDCC version 4.1.0 #12072"
             72 73 69 6F 6E 20 34
             2E 31 2E 30 20 23 31
             32 30 37 32
      001266 00                    1334 	.db	0
      001267 02                    1335 	.uleb128	2
      001268 45 58 54 49 5F 44 65  1336 	.ascii "EXTI_DeInit"
             49 6E 69 74
      001273 00                    1337 	.db	0
      001274 00 00 8C 06           1338 	.dw	0,(_EXTI_DeInit)
      001278 00 00 8C 0F           1339 	.dw	0,(XG$EXTI_DeInit$0$0+1)
      00127C 01                    1340 	.db	1
      00127D 00 00 1D 28           1341 	.dw	0,(Ldebug_loc_start+704)
      001281 03                    1342 	.uleb128	3
      001282 00 00 00 BA           1343 	.dw	0,186
      001286 45 58 54 49 5F 53 65  1344 	.ascii "EXTI_SetExtIntSensitivity"
             74 45 78 74 49 6E 74
             53 65 6E 73 69 74 69
             76 69 74 79
      00129F 00                    1345 	.db	0
      0012A0 00 00 8C 0F           1346 	.dw	0,(_EXTI_SetExtIntSensitivity)
      0012A4 00 00 8C EF           1347 	.dw	0,(XG$EXTI_SetExtIntSensitivity$0$0+1)
      0012A8 01                    1348 	.db	1
      0012A9 00 00 1C 00           1349 	.dw	0,(Ldebug_loc_start+408)
      0012AD 04                    1350 	.uleb128	4
      0012AE 02                    1351 	.db	2
      0012AF 91                    1352 	.db	145
      0012B0 02                    1353 	.sleb128	2
      0012B1 50 6F 72 74           1354 	.ascii "Port"
      0012B5 00                    1355 	.db	0
      0012B6 00 00 00 BA           1356 	.dw	0,186
      0012BA 04                    1357 	.uleb128	4
      0012BB 02                    1358 	.db	2
      0012BC 91                    1359 	.db	145
      0012BD 03                    1360 	.sleb128	3
      0012BE 53 65 6E 73 69 74 69  1361 	.ascii "SensitivityValue"
             76 69 74 79 56 61 6C
             75 65
      0012CE 00                    1362 	.db	0
      0012CF 00 00 00 BA           1363 	.dw	0,186
      0012D3 05                    1364 	.uleb128	5
      0012D4 00 00 8C 7E           1365 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$34)
      0012D8 00 00 8C ED           1366 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$56)
      0012DC 00                    1367 	.uleb128	0
      0012DD 06                    1368 	.uleb128	6
      0012DE 75 6E 73 69 67 6E 65  1369 	.ascii "unsigned char"
             64 20 63 68 61 72
      0012EB 00                    1370 	.db	0
      0012EC 01                    1371 	.db	1
      0012ED 08                    1372 	.db	8
      0012EE 03                    1373 	.uleb128	3
      0012EF 00 00 01 0E           1374 	.dw	0,270
      0012F3 45 58 54 49 5F 53 65  1375 	.ascii "EXTI_SetTLISensitivity"
             74 54 4C 49 53 65 6E
             73 69 74 69 76 69 74
             79
      001309 00                    1376 	.db	0
      00130A 00 00 8C EF           1377 	.dw	0,(_EXTI_SetTLISensitivity)
      00130E 00 00 8D 19           1378 	.dw	0,(XG$EXTI_SetTLISensitivity$0$0+1)
      001312 01                    1379 	.db	1
      001313 00 00 1B 98           1380 	.dw	0,(Ldebug_loc_start+304)
      001317 04                    1381 	.uleb128	4
      001318 02                    1382 	.db	2
      001319 91                    1383 	.db	145
      00131A 02                    1384 	.sleb128	2
      00131B 53 65 6E 73 69 74 69  1385 	.ascii "SensitivityValue"
             76 69 74 79 56 61 6C
             75 65
      00132B 00                    1386 	.db	0
      00132C 00 00 00 BA           1387 	.dw	0,186
      001330 00                    1388 	.uleb128	0
      001331 07                    1389 	.uleb128	7
      001332 00 00 01 62           1390 	.dw	0,354
      001336 45 58 54 49 5F 47 65  1391 	.ascii "EXTI_GetExtIntSensitivity"
             74 45 78 74 49 6E 74
             53 65 6E 73 69 74 69
             76 69 74 79
      00134F 00                    1392 	.db	0
      001350 00 00 8D 19           1393 	.dw	0,(_EXTI_GetExtIntSensitivity)
      001354 00 00 8D 9E           1394 	.dw	0,(XG$EXTI_GetExtIntSensitivity$0$0+1)
      001358 01                    1395 	.db	1
      001359 00 00 1A 7C           1396 	.dw	0,(Ldebug_loc_start+20)
      00135D 00 00 00 BA           1397 	.dw	0,186
      001361 04                    1398 	.uleb128	4
      001362 02                    1399 	.db	2
      001363 91                    1400 	.db	145
      001364 02                    1401 	.sleb128	2
      001365 50 6F 72 74           1402 	.ascii "Port"
      001369 00                    1403 	.db	0
      00136A 00 00 00 BA           1404 	.dw	0,186
      00136E 05                    1405 	.uleb128	5
      00136F 00 00 8D 72           1406 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$104)
      001373 00 00 8D 9D           1407 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$119)
      001377 08                    1408 	.uleb128	8
      001378 01                    1409 	.db	1
      001379 50                    1410 	.db	80
      00137A 76 61 6C 75 65        1411 	.ascii "value"
      00137F 00                    1412 	.db	0
      001380 00 00 00 BA           1413 	.dw	0,186
      001384 00                    1414 	.uleb128	0
      001385 07                    1415 	.uleb128	7
      001386 00 00 01 9D           1416 	.dw	0,413
      00138A 45 58 54 49 5F 47 65  1417 	.ascii "EXTI_GetTLISensitivity"
             74 54 4C 49 53 65 6E
             73 69 74 69 76 69 74
             79
      0013A0 00                    1418 	.db	0
      0013A1 00 00 8D 9E           1419 	.dw	0,(_EXTI_GetTLISensitivity)
      0013A5 00 00 8D A4           1420 	.dw	0,(XG$EXTI_GetTLISensitivity$0$0+1)
      0013A9 01                    1421 	.db	1
      0013AA 00 00 1A 68           1422 	.dw	0,(Ldebug_loc_start)
      0013AE 00 00 00 BA           1423 	.dw	0,186
      0013B2 08                    1424 	.uleb128	8
      0013B3 01                    1425 	.db	1
      0013B4 50                    1426 	.db	80
      0013B5 76 61 6C 75 65        1427 	.ascii "value"
      0013BA 00                    1428 	.db	0
      0013BB 00 00 00 BA           1429 	.dw	0,186
      0013BF 00                    1430 	.uleb128	0
      0013C0 09                    1431 	.uleb128	9
      0013C1 00 00 00 BA           1432 	.dw	0,186
      0013C5 0A                    1433 	.uleb128	10
      0013C6 00 00 01 AF           1434 	.dw	0,431
      0013CA 19                    1435 	.db	25
      0013CB 00 00 01 9D           1436 	.dw	0,413
      0013CF 0B                    1437 	.uleb128	11
      0013D0 18                    1438 	.db	24
      0013D1 00                    1439 	.uleb128	0
      0013D2 08                    1440 	.uleb128	8
      0013D3 05                    1441 	.db	5
      0013D4 03                    1442 	.db	3
      0013D5 00 00 80 FA           1443 	.dw	0,(___str_0)
      0013D9 5F 5F 73 74 72 5F 30  1444 	.ascii "__str_0"
      0013E0 00                    1445 	.db	0
      0013E1 00 00 01 A2           1446 	.dw	0,418
      0013E5 00                    1447 	.uleb128	0
      0013E6 00                    1448 	.uleb128	0
      0013E7 00                    1449 	.uleb128	0
      0013E8                       1450 Ldebug_info_end:
                                   1451 
                                   1452 	.area .debug_pubnames (NOLOAD)
      0005D4 00 00 00 90           1453 	.dw	0,Ldebug_pubnames_end-Ldebug_pubnames_start
      0005D8                       1454 Ldebug_pubnames_start:
      0005D8 00 02                 1455 	.dw	2
      0005DA 00 00 12 23           1456 	.dw	0,(Ldebug_info_start-4)
      0005DE 00 00 01 C5           1457 	.dw	0,4+Ldebug_info_end-Ldebug_info_start
      0005E2 00 00 00 44           1458 	.dw	0,68
      0005E6 45 58 54 49 5F 44 65  1459 	.ascii "EXTI_DeInit"
             49 6E 69 74
      0005F1 00                    1460 	.db	0
      0005F2 00 00 00 5E           1461 	.dw	0,94
      0005F6 45 58 54 49 5F 53 65  1462 	.ascii "EXTI_SetExtIntSensitivity"
             74 45 78 74 49 6E 74
             53 65 6E 73 69 74 69
             76 69 74 79
      00060F 00                    1463 	.db	0
      000610 00 00 00 CB           1464 	.dw	0,203
      000614 45 58 54 49 5F 53 65  1465 	.ascii "EXTI_SetTLISensitivity"
             74 54 4C 49 53 65 6E
             73 69 74 69 76 69 74
             79
      00062A 00                    1466 	.db	0
      00062B 00 00 01 0E           1467 	.dw	0,270
      00062F 45 58 54 49 5F 47 65  1468 	.ascii "EXTI_GetExtIntSensitivity"
             74 45 78 74 49 6E 74
             53 65 6E 73 69 74 69
             76 69 74 79
      000648 00                    1469 	.db	0
      000649 00 00 01 62           1470 	.dw	0,354
      00064D 45 58 54 49 5F 47 65  1471 	.ascii "EXTI_GetTLISensitivity"
             74 54 4C 49 53 65 6E
             73 69 74 69 76 69 74
             79
      000663 00                    1472 	.db	0
      000664 00 00 00 00           1473 	.dw	0,0
      000668                       1474 Ldebug_pubnames_end:
                                   1475 
                                   1476 	.area .debug_frame (NOLOAD)
      00162A 00 00                 1477 	.dw	0
      00162C 00 0E                 1478 	.dw	Ldebug_CIE0_end-Ldebug_CIE0_start
      00162E                       1479 Ldebug_CIE0_start:
      00162E FF FF                 1480 	.dw	0xffff
      001630 FF FF                 1481 	.dw	0xffff
      001632 01                    1482 	.db	1
      001633 00                    1483 	.db	0
      001634 01                    1484 	.uleb128	1
      001635 7F                    1485 	.sleb128	-1
      001636 09                    1486 	.db	9
      001637 0C                    1487 	.db	12
      001638 08                    1488 	.uleb128	8
      001639 02                    1489 	.uleb128	2
      00163A 89                    1490 	.db	137
      00163B 01                    1491 	.uleb128	1
      00163C                       1492 Ldebug_CIE0_end:
      00163C 00 00 00 13           1493 	.dw	0,19
      001640 00 00 16 2A           1494 	.dw	0,(Ldebug_CIE0_start-4)
      001644 00 00 8D 9E           1495 	.dw	0,(Sstm8s_exti$EXTI_GetTLISensitivity$126)	;initial loc
      001648 00 00 00 06           1496 	.dw	0,Sstm8s_exti$EXTI_GetTLISensitivity$131-Sstm8s_exti$EXTI_GetTLISensitivity$126
      00164C 01                    1497 	.db	1
      00164D 00 00 8D 9E           1498 	.dw	0,(Sstm8s_exti$EXTI_GetTLISensitivity$126)
      001651 0E                    1499 	.db	14
      001652 02                    1500 	.uleb128	2
                                   1501 
                                   1502 	.area .debug_frame (NOLOAD)
      001653 00 00                 1503 	.dw	0
      001655 00 0E                 1504 	.dw	Ldebug_CIE1_end-Ldebug_CIE1_start
      001657                       1505 Ldebug_CIE1_start:
      001657 FF FF                 1506 	.dw	0xffff
      001659 FF FF                 1507 	.dw	0xffff
      00165B 01                    1508 	.db	1
      00165C 00                    1509 	.db	0
      00165D 01                    1510 	.uleb128	1
      00165E 7F                    1511 	.sleb128	-1
      00165F 09                    1512 	.db	9
      001660 0C                    1513 	.db	12
      001661 08                    1514 	.uleb128	8
      001662 02                    1515 	.uleb128	2
      001663 89                    1516 	.db	137
      001664 01                    1517 	.uleb128	1
      001665                       1518 Ldebug_CIE1_end:
      001665 00 00 00 AD           1519 	.dw	0,173
      001669 00 00 16 53           1520 	.dw	0,(Ldebug_CIE1_start-4)
      00166D 00 00 8D 19           1521 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$78)	;initial loc
      001671 00 00 00 85           1522 	.dw	0,Sstm8s_exti$EXTI_GetExtIntSensitivity$124-Sstm8s_exti$EXTI_GetExtIntSensitivity$78
      001675 01                    1523 	.db	1
      001676 00 00 8D 19           1524 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$78)
      00167A 0E                    1525 	.db	14
      00167B 02                    1526 	.uleb128	2
      00167C 01                    1527 	.db	1
      00167D 00 00 8D 22           1528 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$81)
      001681 0E                    1529 	.db	14
      001682 03                    1530 	.uleb128	3
      001683 01                    1531 	.db	1
      001684 00 00 8D 26           1532 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$82)
      001688 0E                    1533 	.db	14
      001689 02                    1534 	.uleb128	2
      00168A 01                    1535 	.db	1
      00168B 00 00 8D 2B           1536 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$83)
      00168F 0E                    1537 	.db	14
      001690 02                    1538 	.uleb128	2
      001691 01                    1539 	.db	1
      001692 00 00 8D 2C           1540 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$84)
      001696 0E                    1541 	.db	14
      001697 03                    1542 	.uleb128	3
      001698 01                    1543 	.db	1
      001699 00 00 8D 31           1544 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$85)
      00169D 0E                    1545 	.db	14
      00169E 02                    1546 	.uleb128	2
      00169F 01                    1547 	.db	1
      0016A0 00 00 8D 33           1548 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$86)
      0016A4 0E                    1549 	.db	14
      0016A5 02                    1550 	.uleb128	2
      0016A6 01                    1551 	.db	1
      0016A7 00 00 8D 34           1552 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$87)
      0016AB 0E                    1553 	.db	14
      0016AC 03                    1554 	.uleb128	3
      0016AD 01                    1555 	.db	1
      0016AE 00 00 8D 39           1556 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$88)
      0016B2 0E                    1557 	.db	14
      0016B3 02                    1558 	.uleb128	2
      0016B4 01                    1559 	.db	1
      0016B5 00 00 8D 3B           1560 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$89)
      0016B9 0E                    1561 	.db	14
      0016BA 02                    1562 	.uleb128	2
      0016BB 01                    1563 	.db	1
      0016BC 00 00 8D 3C           1564 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$90)
      0016C0 0E                    1565 	.db	14
      0016C1 03                    1566 	.uleb128	3
      0016C2 01                    1567 	.db	1
      0016C3 00 00 8D 41           1568 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$91)
      0016C7 0E                    1569 	.db	14
      0016C8 02                    1570 	.uleb128	2
      0016C9 01                    1571 	.db	1
      0016CA 00 00 8D 43           1572 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$92)
      0016CE 0E                    1573 	.db	14
      0016CF 02                    1574 	.uleb128	2
      0016D0 01                    1575 	.db	1
      0016D1 00 00 8D 44           1576 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$93)
      0016D5 0E                    1577 	.db	14
      0016D6 03                    1578 	.uleb128	3
      0016D7 01                    1579 	.db	1
      0016D8 00 00 8D 46           1580 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$94)
      0016DC 0E                    1581 	.db	14
      0016DD 04                    1582 	.uleb128	4
      0016DE 01                    1583 	.db	1
      0016DF 00 00 8D 48           1584 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$95)
      0016E3 0E                    1585 	.db	14
      0016E4 06                    1586 	.uleb128	6
      0016E5 01                    1587 	.db	1
      0016E6 00 00 8D 4A           1588 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$96)
      0016EA 0E                    1589 	.db	14
      0016EB 07                    1590 	.uleb128	7
      0016EC 01                    1591 	.db	1
      0016ED 00 00 8D 4C           1592 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$97)
      0016F1 0E                    1593 	.db	14
      0016F2 08                    1594 	.uleb128	8
      0016F3 01                    1595 	.db	1
      0016F4 00 00 8D 4E           1596 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$98)
      0016F8 0E                    1597 	.db	14
      0016F9 09                    1598 	.uleb128	9
      0016FA 01                    1599 	.db	1
      0016FB 00 00 8D 53           1600 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$99)
      0016FF 0E                    1601 	.db	14
      001700 03                    1602 	.uleb128	3
      001701 01                    1603 	.db	1
      001702 00 00 8D 54           1604 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$100)
      001706 0E                    1605 	.db	14
      001707 02                    1606 	.uleb128	2
      001708 01                    1607 	.db	1
      001709 00 00 8D 55           1608 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$102)
      00170D 0E                    1609 	.db	14
      00170E 03                    1610 	.uleb128	3
      00170F 01                    1611 	.db	1
      001710 00 00 8D 5A           1612 	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$103)
      001714 0E                    1613 	.db	14
      001715 02                    1614 	.uleb128	2
                                   1615 
                                   1616 	.area .debug_frame (NOLOAD)
      001716 00 00                 1617 	.dw	0
      001718 00 0E                 1618 	.dw	Ldebug_CIE2_end-Ldebug_CIE2_start
      00171A                       1619 Ldebug_CIE2_start:
      00171A FF FF                 1620 	.dw	0xffff
      00171C FF FF                 1621 	.dw	0xffff
      00171E 01                    1622 	.db	1
      00171F 00                    1623 	.db	0
      001720 01                    1624 	.uleb128	1
      001721 7F                    1625 	.sleb128	-1
      001722 09                    1626 	.db	9
      001723 0C                    1627 	.db	12
      001724 08                    1628 	.uleb128	8
      001725 02                    1629 	.uleb128	2
      001726 89                    1630 	.db	137
      001727 01                    1631 	.uleb128	1
      001728                       1632 Ldebug_CIE2_end:
      001728 00 00 00 44           1633 	.dw	0,68
      00172C 00 00 17 16           1634 	.dw	0,(Ldebug_CIE2_start-4)
      001730 00 00 8C EF           1635 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$63)	;initial loc
      001734 00 00 00 2A           1636 	.dw	0,Sstm8s_exti$EXTI_SetTLISensitivity$76-Sstm8s_exti$EXTI_SetTLISensitivity$63
      001738 01                    1637 	.db	1
      001739 00 00 8C EF           1638 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$63)
      00173D 0E                    1639 	.db	14
      00173E 02                    1640 	.uleb128	2
      00173F 01                    1641 	.db	1
      001740 00 00 8C F9           1642 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$65)
      001744 0E                    1643 	.db	14
      001745 02                    1644 	.uleb128	2
      001746 01                    1645 	.db	1
      001747 00 00 8C FB           1646 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$66)
      00174B 0E                    1647 	.db	14
      00174C 03                    1648 	.uleb128	3
      00174D 01                    1649 	.db	1
      00174E 00 00 8C FD           1650 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$67)
      001752 0E                    1651 	.db	14
      001753 05                    1652 	.uleb128	5
      001754 01                    1653 	.db	1
      001755 00 00 8C FF           1654 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$68)
      001759 0E                    1655 	.db	14
      00175A 06                    1656 	.uleb128	6
      00175B 01                    1657 	.db	1
      00175C 00 00 8D 01           1658 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$69)
      001760 0E                    1659 	.db	14
      001761 07                    1660 	.uleb128	7
      001762 01                    1661 	.db	1
      001763 00 00 8D 03           1662 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$70)
      001767 0E                    1663 	.db	14
      001768 08                    1664 	.uleb128	8
      001769 01                    1665 	.db	1
      00176A 00 00 8D 08           1666 	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$71)
      00176E 0E                    1667 	.db	14
      00176F 02                    1668 	.uleb128	2
                                   1669 
                                   1670 	.area .debug_frame (NOLOAD)
      001770 00 00                 1671 	.dw	0
      001772 00 0E                 1672 	.dw	Ldebug_CIE3_end-Ldebug_CIE3_start
      001774                       1673 Ldebug_CIE3_start:
      001774 FF FF                 1674 	.dw	0xffff
      001776 FF FF                 1675 	.dw	0xffff
      001778 01                    1676 	.db	1
      001779 00                    1677 	.db	0
      00177A 01                    1678 	.uleb128	1
      00177B 7F                    1679 	.sleb128	-1
      00177C 09                    1680 	.db	9
      00177D 0C                    1681 	.db	12
      00177E 08                    1682 	.uleb128	8
      00177F 02                    1683 	.uleb128	2
      001780 89                    1684 	.db	137
      001781 01                    1685 	.uleb128	1
      001782                       1686 Ldebug_CIE3_end:
      001782 00 00 00 B4           1687 	.dw	0,180
      001786 00 00 17 70           1688 	.dw	0,(Ldebug_CIE3_start-4)
      00178A 00 00 8C 0F           1689 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$8)	;initial loc
      00178E 00 00 00 E0           1690 	.dw	0,Sstm8s_exti$EXTI_SetExtIntSensitivity$61-Sstm8s_exti$EXTI_SetExtIntSensitivity$8
      001792 01                    1691 	.db	1
      001793 00 00 8C 0F           1692 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$8)
      001797 0E                    1693 	.db	14
      001798 02                    1694 	.uleb128	2
      001799 01                    1695 	.db	1
      00179A 00 00 8C 10           1696 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$9)
      00179E 0E                    1697 	.db	14
      00179F 03                    1698 	.uleb128	3
      0017A0 01                    1699 	.db	1
      0017A1 00 00 8C 19           1700 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$11)
      0017A5 0E                    1701 	.db	14
      0017A6 03                    1702 	.uleb128	3
      0017A7 01                    1703 	.db	1
      0017A8 00 00 8C 1F           1704 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$12)
      0017AC 0E                    1705 	.db	14
      0017AD 03                    1706 	.uleb128	3
      0017AE 01                    1707 	.db	1
      0017AF 00 00 8C 25           1708 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$13)
      0017B3 0E                    1709 	.db	14
      0017B4 03                    1710 	.uleb128	3
      0017B5 01                    1711 	.db	1
      0017B6 00 00 8C 2B           1712 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$14)
      0017BA 0E                    1713 	.db	14
      0017BB 03                    1714 	.uleb128	3
      0017BC 01                    1715 	.db	1
      0017BD 00 00 8C 2D           1716 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$15)
      0017C1 0E                    1717 	.db	14
      0017C2 04                    1718 	.uleb128	4
      0017C3 01                    1719 	.db	1
      0017C4 00 00 8C 2F           1720 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$16)
      0017C8 0E                    1721 	.db	14
      0017C9 06                    1722 	.uleb128	6
      0017CA 01                    1723 	.db	1
      0017CB 00 00 8C 31           1724 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$17)
      0017CF 0E                    1725 	.db	14
      0017D0 07                    1726 	.uleb128	7
      0017D1 01                    1727 	.db	1
      0017D2 00 00 8C 33           1728 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$18)
      0017D6 0E                    1729 	.db	14
      0017D7 08                    1730 	.uleb128	8
      0017D8 01                    1731 	.db	1
      0017D9 00 00 8C 35           1732 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$19)
      0017DD 0E                    1733 	.db	14
      0017DE 09                    1734 	.uleb128	9
      0017DF 01                    1735 	.db	1
      0017E0 00 00 8C 3A           1736 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$20)
      0017E4 0E                    1737 	.db	14
      0017E5 03                    1738 	.uleb128	3
      0017E6 01                    1739 	.db	1
      0017E7 00 00 8C 43           1740 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$22)
      0017EB 0E                    1741 	.db	14
      0017EC 03                    1742 	.uleb128	3
      0017ED 01                    1743 	.db	1
      0017EE 00 00 8C 49           1744 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$23)
      0017F2 0E                    1745 	.db	14
      0017F3 03                    1746 	.uleb128	3
      0017F4 01                    1747 	.db	1
      0017F5 00 00 8C 4F           1748 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$24)
      0017F9 0E                    1749 	.db	14
      0017FA 03                    1750 	.uleb128	3
      0017FB 01                    1751 	.db	1
      0017FC 00 00 8C 51           1752 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$25)
      001800 0E                    1753 	.db	14
      001801 04                    1754 	.uleb128	4
      001802 01                    1755 	.db	1
      001803 00 00 8C 53           1756 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$26)
      001807 0E                    1757 	.db	14
      001808 06                    1758 	.uleb128	6
      001809 01                    1759 	.db	1
      00180A 00 00 8C 55           1760 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$27)
      00180E 0E                    1761 	.db	14
      00180F 07                    1762 	.uleb128	7
      001810 01                    1763 	.db	1
      001811 00 00 8C 57           1764 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$28)
      001815 0E                    1765 	.db	14
      001816 08                    1766 	.uleb128	8
      001817 01                    1767 	.db	1
      001818 00 00 8C 59           1768 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$29)
      00181C 0E                    1769 	.db	14
      00181D 09                    1770 	.uleb128	9
      00181E 01                    1771 	.db	1
      00181F 00 00 8C 5E           1772 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$30)
      001823 0E                    1773 	.db	14
      001824 03                    1774 	.uleb128	3
      001825 01                    1775 	.db	1
      001826 00 00 8C A0           1776 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$42)
      00182A 0E                    1777 	.db	14
      00182B 05                    1778 	.uleb128	5
      00182C 01                    1779 	.db	1
      00182D 00 00 8C A3           1780 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$43)
      001831 0E                    1781 	.db	14
      001832 03                    1782 	.uleb128	3
      001833 01                    1783 	.db	1
      001834 00 00 8C EE           1784 	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$59)
      001838 0E                    1785 	.db	14
      001839 02                    1786 	.uleb128	2
                                   1787 
                                   1788 	.area .debug_frame (NOLOAD)
      00183A 00 00                 1789 	.dw	0
      00183C 00 0E                 1790 	.dw	Ldebug_CIE4_end-Ldebug_CIE4_start
      00183E                       1791 Ldebug_CIE4_start:
      00183E FF FF                 1792 	.dw	0xffff
      001840 FF FF                 1793 	.dw	0xffff
      001842 01                    1794 	.db	1
      001843 00                    1795 	.db	0
      001844 01                    1796 	.uleb128	1
      001845 7F                    1797 	.sleb128	-1
      001846 09                    1798 	.db	9
      001847 0C                    1799 	.db	12
      001848 08                    1800 	.uleb128	8
      001849 02                    1801 	.uleb128	2
      00184A 89                    1802 	.db	137
      00184B 01                    1803 	.uleb128	1
      00184C                       1804 Ldebug_CIE4_end:
      00184C 00 00 00 13           1805 	.dw	0,19
      001850 00 00 18 3A           1806 	.dw	0,(Ldebug_CIE4_start-4)
      001854 00 00 8C 06           1807 	.dw	0,(Sstm8s_exti$EXTI_DeInit$1)	;initial loc
      001858 00 00 00 09           1808 	.dw	0,Sstm8s_exti$EXTI_DeInit$6-Sstm8s_exti$EXTI_DeInit$1
      00185C 01                    1809 	.db	1
      00185D 00 00 8C 06           1810 	.dw	0,(Sstm8s_exti$EXTI_DeInit$1)
      001861 0E                    1811 	.db	14
      001862 02                    1812 	.uleb128	2
