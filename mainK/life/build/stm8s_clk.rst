                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.1.0 #12072 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module stm8s_clk
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _CLKPrescTable
                                     12 	.globl _HSIDivFactor
                                     13 	.globl _assert_failed
                                     14 	.globl _CLK_DeInit
                                     15 	.globl _CLK_FastHaltWakeUpCmd
                                     16 	.globl _CLK_HSECmd
                                     17 	.globl _CLK_HSICmd
                                     18 	.globl _CLK_LSICmd
                                     19 	.globl _CLK_CCOCmd
                                     20 	.globl _CLK_ClockSwitchCmd
                                     21 	.globl _CLK_SlowActiveHaltWakeUpCmd
                                     22 	.globl _CLK_PeripheralClockConfig
                                     23 	.globl _CLK_ClockSwitchConfig
                                     24 	.globl _CLK_HSIPrescalerConfig
                                     25 	.globl _CLK_CCOConfig
                                     26 	.globl _CLK_ITConfig
                                     27 	.globl _CLK_SYSCLKConfig
                                     28 	.globl _CLK_SWIMConfig
                                     29 	.globl _CLK_ClockSecuritySystemEnable
                                     30 	.globl _CLK_GetSYSCLKSource
                                     31 	.globl _CLK_GetClockFreq
                                     32 	.globl _CLK_AdjustHSICalibrationValue
                                     33 	.globl _CLK_SYSCLKEmergencyClear
                                     34 	.globl _CLK_GetFlagStatus
                                     35 	.globl _CLK_GetITStatus
                                     36 	.globl _CLK_ClearITPendingBit
                                     37 ;--------------------------------------------------------
                                     38 ; ram data
                                     39 ;--------------------------------------------------------
                                     40 	.area DATA
                                     41 ;--------------------------------------------------------
                                     42 ; ram data
                                     43 ;--------------------------------------------------------
                                     44 	.area INITIALIZED
                                     45 ;--------------------------------------------------------
                                     46 ; absolute external ram data
                                     47 ;--------------------------------------------------------
                                     48 	.area DABS (ABS)
                                     49 
                                     50 ; default segment ordering for linker
                                     51 	.area HOME
                                     52 	.area GSINIT
                                     53 	.area GSFINAL
                                     54 	.area CONST
                                     55 	.area INITIALIZER
                                     56 	.area CODE
                                     57 
                                     58 ;--------------------------------------------------------
                                     59 ; global & static initialisations
                                     60 ;--------------------------------------------------------
                                     61 	.area HOME
                                     62 	.area GSINIT
                                     63 	.area GSFINAL
                                     64 	.area GSINIT
                                     65 ;--------------------------------------------------------
                                     66 ; Home
                                     67 ;--------------------------------------------------------
                                     68 	.area HOME
                                     69 	.area HOME
                                     70 ;--------------------------------------------------------
                                     71 ; code
                                     72 ;--------------------------------------------------------
                                     73 	.area CODE
                           000000    74 	Sstm8s_clk$CLK_DeInit$0 ==.
                                     75 ;	drivers/src/stm8s_clk.c: 72: void CLK_DeInit(void)
                                     76 ;	-----------------------------------------
                                     77 ;	 function CLK_DeInit
                                     78 ;	-----------------------------------------
      0084F6                         79 _CLK_DeInit:
                           000000    80 	Sstm8s_clk$CLK_DeInit$1 ==.
                           000000    81 	Sstm8s_clk$CLK_DeInit$2 ==.
                                     82 ;	drivers/src/stm8s_clk.c: 74: CLK->ICKR = CLK_ICKR_RESET_VALUE;
      0084F6 35 01 50 C0      [ 1]   83 	mov	0x50c0+0, #0x01
                           000004    84 	Sstm8s_clk$CLK_DeInit$3 ==.
                                     85 ;	drivers/src/stm8s_clk.c: 75: CLK->ECKR = CLK_ECKR_RESET_VALUE;
      0084FA 35 00 50 C1      [ 1]   86 	mov	0x50c1+0, #0x00
                           000008    87 	Sstm8s_clk$CLK_DeInit$4 ==.
                                     88 ;	drivers/src/stm8s_clk.c: 76: CLK->SWR  = CLK_SWR_RESET_VALUE;
      0084FE 35 E1 50 C4      [ 1]   89 	mov	0x50c4+0, #0xe1
                           00000C    90 	Sstm8s_clk$CLK_DeInit$5 ==.
                                     91 ;	drivers/src/stm8s_clk.c: 77: CLK->SWCR = CLK_SWCR_RESET_VALUE;
      008502 35 00 50 C5      [ 1]   92 	mov	0x50c5+0, #0x00
                           000010    93 	Sstm8s_clk$CLK_DeInit$6 ==.
                                     94 ;	drivers/src/stm8s_clk.c: 78: CLK->CKDIVR = CLK_CKDIVR_RESET_VALUE;
      008506 35 18 50 C6      [ 1]   95 	mov	0x50c6+0, #0x18
                           000014    96 	Sstm8s_clk$CLK_DeInit$7 ==.
                                     97 ;	drivers/src/stm8s_clk.c: 79: CLK->PCKENR1 = CLK_PCKENR1_RESET_VALUE;
      00850A 35 FF 50 C7      [ 1]   98 	mov	0x50c7+0, #0xff
                           000018    99 	Sstm8s_clk$CLK_DeInit$8 ==.
                                    100 ;	drivers/src/stm8s_clk.c: 80: CLK->PCKENR2 = CLK_PCKENR2_RESET_VALUE;
      00850E 35 FF 50 CA      [ 1]  101 	mov	0x50ca+0, #0xff
                           00001C   102 	Sstm8s_clk$CLK_DeInit$9 ==.
                                    103 ;	drivers/src/stm8s_clk.c: 81: CLK->CSSR = CLK_CSSR_RESET_VALUE;
      008512 35 00 50 C8      [ 1]  104 	mov	0x50c8+0, #0x00
                           000020   105 	Sstm8s_clk$CLK_DeInit$10 ==.
                                    106 ;	drivers/src/stm8s_clk.c: 82: CLK->CCOR = CLK_CCOR_RESET_VALUE;
      008516 35 00 50 C9      [ 1]  107 	mov	0x50c9+0, #0x00
                           000024   108 	Sstm8s_clk$CLK_DeInit$11 ==.
                                    109 ;	drivers/src/stm8s_clk.c: 83: while ((CLK->CCOR & CLK_CCOR_CCOEN)!= 0)
      00851A                        110 00101$:
      00851A C6 50 C9         [ 1]  111 	ld	a, 0x50c9
      00851D 44               [ 1]  112 	srl	a
      00851E 25 FA            [ 1]  113 	jrc	00101$
                           00002A   114 	Sstm8s_clk$CLK_DeInit$12 ==.
                                    115 ;	drivers/src/stm8s_clk.c: 85: CLK->CCOR = CLK_CCOR_RESET_VALUE;
      008520 35 00 50 C9      [ 1]  116 	mov	0x50c9+0, #0x00
                           00002E   117 	Sstm8s_clk$CLK_DeInit$13 ==.
                                    118 ;	drivers/src/stm8s_clk.c: 86: CLK->HSITRIMR = CLK_HSITRIMR_RESET_VALUE;
      008524 35 00 50 CC      [ 1]  119 	mov	0x50cc+0, #0x00
                           000032   120 	Sstm8s_clk$CLK_DeInit$14 ==.
                                    121 ;	drivers/src/stm8s_clk.c: 87: CLK->SWIMCCR = CLK_SWIMCCR_RESET_VALUE;
      008528 35 00 50 CD      [ 1]  122 	mov	0x50cd+0, #0x00
                           000036   123 	Sstm8s_clk$CLK_DeInit$15 ==.
                                    124 ;	drivers/src/stm8s_clk.c: 88: }
                           000036   125 	Sstm8s_clk$CLK_DeInit$16 ==.
                           000036   126 	XG$CLK_DeInit$0$0 ==.
      00852C 81               [ 4]  127 	ret
                           000037   128 	Sstm8s_clk$CLK_DeInit$17 ==.
                           000037   129 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$18 ==.
                                    130 ;	drivers/src/stm8s_clk.c: 99: void CLK_FastHaltWakeUpCmd(FunctionalState NewState)
                                    131 ;	-----------------------------------------
                                    132 ;	 function CLK_FastHaltWakeUpCmd
                                    133 ;	-----------------------------------------
      00852D                        134 _CLK_FastHaltWakeUpCmd:
                           000037   135 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$19 ==.
                           000037   136 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$20 ==.
                                    137 ;	drivers/src/stm8s_clk.c: 102: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
      00852D 0D 03            [ 1]  138 	tnz	(0x03, sp)
      00852F 27 14            [ 1]  139 	jreq	00107$
      008531 7B 03            [ 1]  140 	ld	a, (0x03, sp)
      008533 4A               [ 1]  141 	dec	a
      008534 27 0F            [ 1]  142 	jreq	00107$
                           000040   143 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$21 ==.
      008536 4B 66            [ 1]  144 	push	#0x66
                           000042   145 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$22 ==.
      008538 5F               [ 1]  146 	clrw	x
      008539 89               [ 2]  147 	pushw	x
                           000044   148 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$23 ==.
      00853A 4B 00            [ 1]  149 	push	#0x00
                           000046   150 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$24 ==.
      00853C 4B E2            [ 1]  151 	push	#<(___str_0+0)
                           000048   152 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$25 ==.
      00853E 4B 80            [ 1]  153 	push	#((___str_0+0) >> 8)
                           00004A   154 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$26 ==.
      008540 CD 84 F3         [ 4]  155 	call	_assert_failed
      008543 5B 06            [ 2]  156 	addw	sp, #6
                           00004F   157 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$27 ==.
      008545                        158 00107$:
                           00004F   159 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$28 ==.
                                    160 ;	drivers/src/stm8s_clk.c: 107: CLK->ICKR |= CLK_ICKR_FHWU;
      008545 C6 50 C0         [ 1]  161 	ld	a, 0x50c0
                           000052   162 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$29 ==.
                                    163 ;	drivers/src/stm8s_clk.c: 104: if (NewState != DISABLE)
      008548 0D 03            [ 1]  164 	tnz	(0x03, sp)
      00854A 27 07            [ 1]  165 	jreq	00102$
                           000056   166 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$30 ==.
                           000056   167 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$31 ==.
                                    168 ;	drivers/src/stm8s_clk.c: 107: CLK->ICKR |= CLK_ICKR_FHWU;
      00854C AA 04            [ 1]  169 	or	a, #0x04
      00854E C7 50 C0         [ 1]  170 	ld	0x50c0, a
                           00005B   171 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$32 ==.
      008551 20 05            [ 2]  172 	jra	00104$
      008553                        173 00102$:
                           00005D   174 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$33 ==.
                           00005D   175 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$34 ==.
                                    176 ;	drivers/src/stm8s_clk.c: 112: CLK->ICKR &= (uint8_t)(~CLK_ICKR_FHWU);
      008553 A4 FB            [ 1]  177 	and	a, #0xfb
      008555 C7 50 C0         [ 1]  178 	ld	0x50c0, a
                           000062   179 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$35 ==.
      008558                        180 00104$:
                           000062   181 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$36 ==.
                                    182 ;	drivers/src/stm8s_clk.c: 114: }
                           000062   183 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$37 ==.
                           000062   184 	XG$CLK_FastHaltWakeUpCmd$0$0 ==.
      008558 81               [ 4]  185 	ret
                           000063   186 	Sstm8s_clk$CLK_FastHaltWakeUpCmd$38 ==.
                           000063   187 	Sstm8s_clk$CLK_HSECmd$39 ==.
                                    188 ;	drivers/src/stm8s_clk.c: 121: void CLK_HSECmd(FunctionalState NewState)
                                    189 ;	-----------------------------------------
                                    190 ;	 function CLK_HSECmd
                                    191 ;	-----------------------------------------
      008559                        192 _CLK_HSECmd:
                           000063   193 	Sstm8s_clk$CLK_HSECmd$40 ==.
                           000063   194 	Sstm8s_clk$CLK_HSECmd$41 ==.
                                    195 ;	drivers/src/stm8s_clk.c: 124: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
      008559 0D 03            [ 1]  196 	tnz	(0x03, sp)
      00855B 27 14            [ 1]  197 	jreq	00107$
      00855D 7B 03            [ 1]  198 	ld	a, (0x03, sp)
      00855F 4A               [ 1]  199 	dec	a
      008560 27 0F            [ 1]  200 	jreq	00107$
                           00006C   201 	Sstm8s_clk$CLK_HSECmd$42 ==.
      008562 4B 7C            [ 1]  202 	push	#0x7c
                           00006E   203 	Sstm8s_clk$CLK_HSECmd$43 ==.
      008564 5F               [ 1]  204 	clrw	x
      008565 89               [ 2]  205 	pushw	x
                           000070   206 	Sstm8s_clk$CLK_HSECmd$44 ==.
      008566 4B 00            [ 1]  207 	push	#0x00
                           000072   208 	Sstm8s_clk$CLK_HSECmd$45 ==.
      008568 4B E2            [ 1]  209 	push	#<(___str_0+0)
                           000074   210 	Sstm8s_clk$CLK_HSECmd$46 ==.
      00856A 4B 80            [ 1]  211 	push	#((___str_0+0) >> 8)
                           000076   212 	Sstm8s_clk$CLK_HSECmd$47 ==.
      00856C CD 84 F3         [ 4]  213 	call	_assert_failed
      00856F 5B 06            [ 2]  214 	addw	sp, #6
                           00007B   215 	Sstm8s_clk$CLK_HSECmd$48 ==.
      008571                        216 00107$:
                           00007B   217 	Sstm8s_clk$CLK_HSECmd$49 ==.
                                    218 ;	drivers/src/stm8s_clk.c: 129: CLK->ECKR |= CLK_ECKR_HSEEN;
      008571 C6 50 C1         [ 1]  219 	ld	a, 0x50c1
                           00007E   220 	Sstm8s_clk$CLK_HSECmd$50 ==.
                                    221 ;	drivers/src/stm8s_clk.c: 126: if (NewState != DISABLE)
      008574 0D 03            [ 1]  222 	tnz	(0x03, sp)
      008576 27 07            [ 1]  223 	jreq	00102$
                           000082   224 	Sstm8s_clk$CLK_HSECmd$51 ==.
                           000082   225 	Sstm8s_clk$CLK_HSECmd$52 ==.
                                    226 ;	drivers/src/stm8s_clk.c: 129: CLK->ECKR |= CLK_ECKR_HSEEN;
      008578 AA 01            [ 1]  227 	or	a, #0x01
      00857A C7 50 C1         [ 1]  228 	ld	0x50c1, a
                           000087   229 	Sstm8s_clk$CLK_HSECmd$53 ==.
      00857D 20 05            [ 2]  230 	jra	00104$
      00857F                        231 00102$:
                           000089   232 	Sstm8s_clk$CLK_HSECmd$54 ==.
                           000089   233 	Sstm8s_clk$CLK_HSECmd$55 ==.
                                    234 ;	drivers/src/stm8s_clk.c: 134: CLK->ECKR &= (uint8_t)(~CLK_ECKR_HSEEN);
      00857F A4 FE            [ 1]  235 	and	a, #0xfe
      008581 C7 50 C1         [ 1]  236 	ld	0x50c1, a
                           00008E   237 	Sstm8s_clk$CLK_HSECmd$56 ==.
      008584                        238 00104$:
                           00008E   239 	Sstm8s_clk$CLK_HSECmd$57 ==.
                                    240 ;	drivers/src/stm8s_clk.c: 136: }
                           00008E   241 	Sstm8s_clk$CLK_HSECmd$58 ==.
                           00008E   242 	XG$CLK_HSECmd$0$0 ==.
      008584 81               [ 4]  243 	ret
                           00008F   244 	Sstm8s_clk$CLK_HSECmd$59 ==.
                           00008F   245 	Sstm8s_clk$CLK_HSICmd$60 ==.
                                    246 ;	drivers/src/stm8s_clk.c: 143: void CLK_HSICmd(FunctionalState NewState)
                                    247 ;	-----------------------------------------
                                    248 ;	 function CLK_HSICmd
                                    249 ;	-----------------------------------------
      008585                        250 _CLK_HSICmd:
                           00008F   251 	Sstm8s_clk$CLK_HSICmd$61 ==.
                           00008F   252 	Sstm8s_clk$CLK_HSICmd$62 ==.
                                    253 ;	drivers/src/stm8s_clk.c: 146: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
      008585 0D 03            [ 1]  254 	tnz	(0x03, sp)
      008587 27 14            [ 1]  255 	jreq	00107$
      008589 7B 03            [ 1]  256 	ld	a, (0x03, sp)
      00858B 4A               [ 1]  257 	dec	a
      00858C 27 0F            [ 1]  258 	jreq	00107$
                           000098   259 	Sstm8s_clk$CLK_HSICmd$63 ==.
      00858E 4B 92            [ 1]  260 	push	#0x92
                           00009A   261 	Sstm8s_clk$CLK_HSICmd$64 ==.
      008590 5F               [ 1]  262 	clrw	x
      008591 89               [ 2]  263 	pushw	x
                           00009C   264 	Sstm8s_clk$CLK_HSICmd$65 ==.
      008592 4B 00            [ 1]  265 	push	#0x00
                           00009E   266 	Sstm8s_clk$CLK_HSICmd$66 ==.
      008594 4B E2            [ 1]  267 	push	#<(___str_0+0)
                           0000A0   268 	Sstm8s_clk$CLK_HSICmd$67 ==.
      008596 4B 80            [ 1]  269 	push	#((___str_0+0) >> 8)
                           0000A2   270 	Sstm8s_clk$CLK_HSICmd$68 ==.
      008598 CD 84 F3         [ 4]  271 	call	_assert_failed
      00859B 5B 06            [ 2]  272 	addw	sp, #6
                           0000A7   273 	Sstm8s_clk$CLK_HSICmd$69 ==.
      00859D                        274 00107$:
                           0000A7   275 	Sstm8s_clk$CLK_HSICmd$70 ==.
                                    276 ;	drivers/src/stm8s_clk.c: 151: CLK->ICKR |= CLK_ICKR_HSIEN;
      00859D C6 50 C0         [ 1]  277 	ld	a, 0x50c0
                           0000AA   278 	Sstm8s_clk$CLK_HSICmd$71 ==.
                                    279 ;	drivers/src/stm8s_clk.c: 148: if (NewState != DISABLE)
      0085A0 0D 03            [ 1]  280 	tnz	(0x03, sp)
      0085A2 27 07            [ 1]  281 	jreq	00102$
                           0000AE   282 	Sstm8s_clk$CLK_HSICmd$72 ==.
                           0000AE   283 	Sstm8s_clk$CLK_HSICmd$73 ==.
                                    284 ;	drivers/src/stm8s_clk.c: 151: CLK->ICKR |= CLK_ICKR_HSIEN;
      0085A4 AA 01            [ 1]  285 	or	a, #0x01
      0085A6 C7 50 C0         [ 1]  286 	ld	0x50c0, a
                           0000B3   287 	Sstm8s_clk$CLK_HSICmd$74 ==.
      0085A9 20 05            [ 2]  288 	jra	00104$
      0085AB                        289 00102$:
                           0000B5   290 	Sstm8s_clk$CLK_HSICmd$75 ==.
                           0000B5   291 	Sstm8s_clk$CLK_HSICmd$76 ==.
                                    292 ;	drivers/src/stm8s_clk.c: 156: CLK->ICKR &= (uint8_t)(~CLK_ICKR_HSIEN);
      0085AB A4 FE            [ 1]  293 	and	a, #0xfe
      0085AD C7 50 C0         [ 1]  294 	ld	0x50c0, a
                           0000BA   295 	Sstm8s_clk$CLK_HSICmd$77 ==.
      0085B0                        296 00104$:
                           0000BA   297 	Sstm8s_clk$CLK_HSICmd$78 ==.
                                    298 ;	drivers/src/stm8s_clk.c: 158: }
                           0000BA   299 	Sstm8s_clk$CLK_HSICmd$79 ==.
                           0000BA   300 	XG$CLK_HSICmd$0$0 ==.
      0085B0 81               [ 4]  301 	ret
                           0000BB   302 	Sstm8s_clk$CLK_HSICmd$80 ==.
                           0000BB   303 	Sstm8s_clk$CLK_LSICmd$81 ==.
                                    304 ;	drivers/src/stm8s_clk.c: 166: void CLK_LSICmd(FunctionalState NewState)
                                    305 ;	-----------------------------------------
                                    306 ;	 function CLK_LSICmd
                                    307 ;	-----------------------------------------
      0085B1                        308 _CLK_LSICmd:
                           0000BB   309 	Sstm8s_clk$CLK_LSICmd$82 ==.
                           0000BB   310 	Sstm8s_clk$CLK_LSICmd$83 ==.
                                    311 ;	drivers/src/stm8s_clk.c: 169: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
      0085B1 0D 03            [ 1]  312 	tnz	(0x03, sp)
      0085B3 27 14            [ 1]  313 	jreq	00107$
      0085B5 7B 03            [ 1]  314 	ld	a, (0x03, sp)
      0085B7 4A               [ 1]  315 	dec	a
      0085B8 27 0F            [ 1]  316 	jreq	00107$
                           0000C4   317 	Sstm8s_clk$CLK_LSICmd$84 ==.
      0085BA 4B A9            [ 1]  318 	push	#0xa9
                           0000C6   319 	Sstm8s_clk$CLK_LSICmd$85 ==.
      0085BC 5F               [ 1]  320 	clrw	x
      0085BD 89               [ 2]  321 	pushw	x
                           0000C8   322 	Sstm8s_clk$CLK_LSICmd$86 ==.
      0085BE 4B 00            [ 1]  323 	push	#0x00
                           0000CA   324 	Sstm8s_clk$CLK_LSICmd$87 ==.
      0085C0 4B E2            [ 1]  325 	push	#<(___str_0+0)
                           0000CC   326 	Sstm8s_clk$CLK_LSICmd$88 ==.
      0085C2 4B 80            [ 1]  327 	push	#((___str_0+0) >> 8)
                           0000CE   328 	Sstm8s_clk$CLK_LSICmd$89 ==.
      0085C4 CD 84 F3         [ 4]  329 	call	_assert_failed
      0085C7 5B 06            [ 2]  330 	addw	sp, #6
                           0000D3   331 	Sstm8s_clk$CLK_LSICmd$90 ==.
      0085C9                        332 00107$:
                           0000D3   333 	Sstm8s_clk$CLK_LSICmd$91 ==.
                                    334 ;	drivers/src/stm8s_clk.c: 174: CLK->ICKR |= CLK_ICKR_LSIEN;
      0085C9 C6 50 C0         [ 1]  335 	ld	a, 0x50c0
                           0000D6   336 	Sstm8s_clk$CLK_LSICmd$92 ==.
                                    337 ;	drivers/src/stm8s_clk.c: 171: if (NewState != DISABLE)
      0085CC 0D 03            [ 1]  338 	tnz	(0x03, sp)
      0085CE 27 07            [ 1]  339 	jreq	00102$
                           0000DA   340 	Sstm8s_clk$CLK_LSICmd$93 ==.
                           0000DA   341 	Sstm8s_clk$CLK_LSICmd$94 ==.
                                    342 ;	drivers/src/stm8s_clk.c: 174: CLK->ICKR |= CLK_ICKR_LSIEN;
      0085D0 AA 08            [ 1]  343 	or	a, #0x08
      0085D2 C7 50 C0         [ 1]  344 	ld	0x50c0, a
                           0000DF   345 	Sstm8s_clk$CLK_LSICmd$95 ==.
      0085D5 20 05            [ 2]  346 	jra	00104$
      0085D7                        347 00102$:
                           0000E1   348 	Sstm8s_clk$CLK_LSICmd$96 ==.
                           0000E1   349 	Sstm8s_clk$CLK_LSICmd$97 ==.
                                    350 ;	drivers/src/stm8s_clk.c: 179: CLK->ICKR &= (uint8_t)(~CLK_ICKR_LSIEN);
      0085D7 A4 F7            [ 1]  351 	and	a, #0xf7
      0085D9 C7 50 C0         [ 1]  352 	ld	0x50c0, a
                           0000E6   353 	Sstm8s_clk$CLK_LSICmd$98 ==.
      0085DC                        354 00104$:
                           0000E6   355 	Sstm8s_clk$CLK_LSICmd$99 ==.
                                    356 ;	drivers/src/stm8s_clk.c: 181: }
                           0000E6   357 	Sstm8s_clk$CLK_LSICmd$100 ==.
                           0000E6   358 	XG$CLK_LSICmd$0$0 ==.
      0085DC 81               [ 4]  359 	ret
                           0000E7   360 	Sstm8s_clk$CLK_LSICmd$101 ==.
                           0000E7   361 	Sstm8s_clk$CLK_CCOCmd$102 ==.
                                    362 ;	drivers/src/stm8s_clk.c: 189: void CLK_CCOCmd(FunctionalState NewState)
                                    363 ;	-----------------------------------------
                                    364 ;	 function CLK_CCOCmd
                                    365 ;	-----------------------------------------
      0085DD                        366 _CLK_CCOCmd:
                           0000E7   367 	Sstm8s_clk$CLK_CCOCmd$103 ==.
                           0000E7   368 	Sstm8s_clk$CLK_CCOCmd$104 ==.
                                    369 ;	drivers/src/stm8s_clk.c: 192: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
      0085DD 0D 03            [ 1]  370 	tnz	(0x03, sp)
      0085DF 27 14            [ 1]  371 	jreq	00107$
      0085E1 7B 03            [ 1]  372 	ld	a, (0x03, sp)
      0085E3 4A               [ 1]  373 	dec	a
      0085E4 27 0F            [ 1]  374 	jreq	00107$
                           0000F0   375 	Sstm8s_clk$CLK_CCOCmd$105 ==.
      0085E6 4B C0            [ 1]  376 	push	#0xc0
                           0000F2   377 	Sstm8s_clk$CLK_CCOCmd$106 ==.
      0085E8 5F               [ 1]  378 	clrw	x
      0085E9 89               [ 2]  379 	pushw	x
                           0000F4   380 	Sstm8s_clk$CLK_CCOCmd$107 ==.
      0085EA 4B 00            [ 1]  381 	push	#0x00
                           0000F6   382 	Sstm8s_clk$CLK_CCOCmd$108 ==.
      0085EC 4B E2            [ 1]  383 	push	#<(___str_0+0)
                           0000F8   384 	Sstm8s_clk$CLK_CCOCmd$109 ==.
      0085EE 4B 80            [ 1]  385 	push	#((___str_0+0) >> 8)
                           0000FA   386 	Sstm8s_clk$CLK_CCOCmd$110 ==.
      0085F0 CD 84 F3         [ 4]  387 	call	_assert_failed
      0085F3 5B 06            [ 2]  388 	addw	sp, #6
                           0000FF   389 	Sstm8s_clk$CLK_CCOCmd$111 ==.
      0085F5                        390 00107$:
                           0000FF   391 	Sstm8s_clk$CLK_CCOCmd$112 ==.
                                    392 ;	drivers/src/stm8s_clk.c: 197: CLK->CCOR |= CLK_CCOR_CCOEN;
      0085F5 C6 50 C9         [ 1]  393 	ld	a, 0x50c9
                           000102   394 	Sstm8s_clk$CLK_CCOCmd$113 ==.
                                    395 ;	drivers/src/stm8s_clk.c: 194: if (NewState != DISABLE)
      0085F8 0D 03            [ 1]  396 	tnz	(0x03, sp)
      0085FA 27 07            [ 1]  397 	jreq	00102$
                           000106   398 	Sstm8s_clk$CLK_CCOCmd$114 ==.
                           000106   399 	Sstm8s_clk$CLK_CCOCmd$115 ==.
                                    400 ;	drivers/src/stm8s_clk.c: 197: CLK->CCOR |= CLK_CCOR_CCOEN;
      0085FC AA 01            [ 1]  401 	or	a, #0x01
      0085FE C7 50 C9         [ 1]  402 	ld	0x50c9, a
                           00010B   403 	Sstm8s_clk$CLK_CCOCmd$116 ==.
      008601 20 05            [ 2]  404 	jra	00104$
      008603                        405 00102$:
                           00010D   406 	Sstm8s_clk$CLK_CCOCmd$117 ==.
                           00010D   407 	Sstm8s_clk$CLK_CCOCmd$118 ==.
                                    408 ;	drivers/src/stm8s_clk.c: 202: CLK->CCOR &= (uint8_t)(~CLK_CCOR_CCOEN);
      008603 A4 FE            [ 1]  409 	and	a, #0xfe
      008605 C7 50 C9         [ 1]  410 	ld	0x50c9, a
                           000112   411 	Sstm8s_clk$CLK_CCOCmd$119 ==.
      008608                        412 00104$:
                           000112   413 	Sstm8s_clk$CLK_CCOCmd$120 ==.
                                    414 ;	drivers/src/stm8s_clk.c: 204: }
                           000112   415 	Sstm8s_clk$CLK_CCOCmd$121 ==.
                           000112   416 	XG$CLK_CCOCmd$0$0 ==.
      008608 81               [ 4]  417 	ret
                           000113   418 	Sstm8s_clk$CLK_CCOCmd$122 ==.
                           000113   419 	Sstm8s_clk$CLK_ClockSwitchCmd$123 ==.
                                    420 ;	drivers/src/stm8s_clk.c: 213: void CLK_ClockSwitchCmd(FunctionalState NewState)
                                    421 ;	-----------------------------------------
                                    422 ;	 function CLK_ClockSwitchCmd
                                    423 ;	-----------------------------------------
      008609                        424 _CLK_ClockSwitchCmd:
                           000113   425 	Sstm8s_clk$CLK_ClockSwitchCmd$124 ==.
                           000113   426 	Sstm8s_clk$CLK_ClockSwitchCmd$125 ==.
                                    427 ;	drivers/src/stm8s_clk.c: 216: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
      008609 0D 03            [ 1]  428 	tnz	(0x03, sp)
      00860B 27 14            [ 1]  429 	jreq	00107$
      00860D 7B 03            [ 1]  430 	ld	a, (0x03, sp)
      00860F 4A               [ 1]  431 	dec	a
      008610 27 0F            [ 1]  432 	jreq	00107$
                           00011C   433 	Sstm8s_clk$CLK_ClockSwitchCmd$126 ==.
      008612 4B D8            [ 1]  434 	push	#0xd8
                           00011E   435 	Sstm8s_clk$CLK_ClockSwitchCmd$127 ==.
      008614 5F               [ 1]  436 	clrw	x
      008615 89               [ 2]  437 	pushw	x
                           000120   438 	Sstm8s_clk$CLK_ClockSwitchCmd$128 ==.
      008616 4B 00            [ 1]  439 	push	#0x00
                           000122   440 	Sstm8s_clk$CLK_ClockSwitchCmd$129 ==.
      008618 4B E2            [ 1]  441 	push	#<(___str_0+0)
                           000124   442 	Sstm8s_clk$CLK_ClockSwitchCmd$130 ==.
      00861A 4B 80            [ 1]  443 	push	#((___str_0+0) >> 8)
                           000126   444 	Sstm8s_clk$CLK_ClockSwitchCmd$131 ==.
      00861C CD 84 F3         [ 4]  445 	call	_assert_failed
      00861F 5B 06            [ 2]  446 	addw	sp, #6
                           00012B   447 	Sstm8s_clk$CLK_ClockSwitchCmd$132 ==.
      008621                        448 00107$:
                           00012B   449 	Sstm8s_clk$CLK_ClockSwitchCmd$133 ==.
                                    450 ;	drivers/src/stm8s_clk.c: 221: CLK->SWCR |= CLK_SWCR_SWEN;
      008621 C6 50 C5         [ 1]  451 	ld	a, 0x50c5
                           00012E   452 	Sstm8s_clk$CLK_ClockSwitchCmd$134 ==.
                                    453 ;	drivers/src/stm8s_clk.c: 218: if (NewState != DISABLE )
      008624 0D 03            [ 1]  454 	tnz	(0x03, sp)
      008626 27 07            [ 1]  455 	jreq	00102$
                           000132   456 	Sstm8s_clk$CLK_ClockSwitchCmd$135 ==.
                           000132   457 	Sstm8s_clk$CLK_ClockSwitchCmd$136 ==.
                                    458 ;	drivers/src/stm8s_clk.c: 221: CLK->SWCR |= CLK_SWCR_SWEN;
      008628 AA 02            [ 1]  459 	or	a, #0x02
      00862A C7 50 C5         [ 1]  460 	ld	0x50c5, a
                           000137   461 	Sstm8s_clk$CLK_ClockSwitchCmd$137 ==.
      00862D 20 05            [ 2]  462 	jra	00104$
      00862F                        463 00102$:
                           000139   464 	Sstm8s_clk$CLK_ClockSwitchCmd$138 ==.
                           000139   465 	Sstm8s_clk$CLK_ClockSwitchCmd$139 ==.
                                    466 ;	drivers/src/stm8s_clk.c: 226: CLK->SWCR &= (uint8_t)(~CLK_SWCR_SWEN);
      00862F A4 FD            [ 1]  467 	and	a, #0xfd
      008631 C7 50 C5         [ 1]  468 	ld	0x50c5, a
                           00013E   469 	Sstm8s_clk$CLK_ClockSwitchCmd$140 ==.
      008634                        470 00104$:
                           00013E   471 	Sstm8s_clk$CLK_ClockSwitchCmd$141 ==.
                                    472 ;	drivers/src/stm8s_clk.c: 228: }
                           00013E   473 	Sstm8s_clk$CLK_ClockSwitchCmd$142 ==.
                           00013E   474 	XG$CLK_ClockSwitchCmd$0$0 ==.
      008634 81               [ 4]  475 	ret
                           00013F   476 	Sstm8s_clk$CLK_ClockSwitchCmd$143 ==.
                           00013F   477 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$144 ==.
                                    478 ;	drivers/src/stm8s_clk.c: 238: void CLK_SlowActiveHaltWakeUpCmd(FunctionalState NewState)
                                    479 ;	-----------------------------------------
                                    480 ;	 function CLK_SlowActiveHaltWakeUpCmd
                                    481 ;	-----------------------------------------
      008635                        482 _CLK_SlowActiveHaltWakeUpCmd:
                           00013F   483 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$145 ==.
                           00013F   484 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$146 ==.
                                    485 ;	drivers/src/stm8s_clk.c: 241: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
      008635 0D 03            [ 1]  486 	tnz	(0x03, sp)
      008637 27 14            [ 1]  487 	jreq	00107$
      008639 7B 03            [ 1]  488 	ld	a, (0x03, sp)
      00863B 4A               [ 1]  489 	dec	a
      00863C 27 0F            [ 1]  490 	jreq	00107$
                           000148   491 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$147 ==.
      00863E 4B F1            [ 1]  492 	push	#0xf1
                           00014A   493 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$148 ==.
      008640 5F               [ 1]  494 	clrw	x
      008641 89               [ 2]  495 	pushw	x
                           00014C   496 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$149 ==.
      008642 4B 00            [ 1]  497 	push	#0x00
                           00014E   498 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$150 ==.
      008644 4B E2            [ 1]  499 	push	#<(___str_0+0)
                           000150   500 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$151 ==.
      008646 4B 80            [ 1]  501 	push	#((___str_0+0) >> 8)
                           000152   502 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$152 ==.
      008648 CD 84 F3         [ 4]  503 	call	_assert_failed
      00864B 5B 06            [ 2]  504 	addw	sp, #6
                           000157   505 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$153 ==.
      00864D                        506 00107$:
                           000157   507 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$154 ==.
                                    508 ;	drivers/src/stm8s_clk.c: 246: CLK->ICKR |= CLK_ICKR_SWUAH;
      00864D C6 50 C0         [ 1]  509 	ld	a, 0x50c0
                           00015A   510 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$155 ==.
                                    511 ;	drivers/src/stm8s_clk.c: 243: if (NewState != DISABLE)
      008650 0D 03            [ 1]  512 	tnz	(0x03, sp)
      008652 27 07            [ 1]  513 	jreq	00102$
                           00015E   514 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$156 ==.
                           00015E   515 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$157 ==.
                                    516 ;	drivers/src/stm8s_clk.c: 246: CLK->ICKR |= CLK_ICKR_SWUAH;
      008654 AA 20            [ 1]  517 	or	a, #0x20
      008656 C7 50 C0         [ 1]  518 	ld	0x50c0, a
                           000163   519 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$158 ==.
      008659 20 05            [ 2]  520 	jra	00104$
      00865B                        521 00102$:
                           000165   522 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$159 ==.
                           000165   523 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$160 ==.
                                    524 ;	drivers/src/stm8s_clk.c: 251: CLK->ICKR &= (uint8_t)(~CLK_ICKR_SWUAH);
      00865B A4 DF            [ 1]  525 	and	a, #0xdf
      00865D C7 50 C0         [ 1]  526 	ld	0x50c0, a
                           00016A   527 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$161 ==.
      008660                        528 00104$:
                           00016A   529 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$162 ==.
                                    530 ;	drivers/src/stm8s_clk.c: 253: }
                           00016A   531 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$163 ==.
                           00016A   532 	XG$CLK_SlowActiveHaltWakeUpCmd$0$0 ==.
      008660 81               [ 4]  533 	ret
                           00016B   534 	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$164 ==.
                           00016B   535 	Sstm8s_clk$CLK_PeripheralClockConfig$165 ==.
                                    536 ;	drivers/src/stm8s_clk.c: 263: void CLK_PeripheralClockConfig(CLK_Peripheral_TypeDef CLK_Peripheral, FunctionalState NewState)
                                    537 ;	-----------------------------------------
                                    538 ;	 function CLK_PeripheralClockConfig
                                    539 ;	-----------------------------------------
      008661                        540 _CLK_PeripheralClockConfig:
                           00016B   541 	Sstm8s_clk$CLK_PeripheralClockConfig$166 ==.
      008661 89               [ 2]  542 	pushw	x
                           00016C   543 	Sstm8s_clk$CLK_PeripheralClockConfig$167 ==.
                           00016C   544 	Sstm8s_clk$CLK_PeripheralClockConfig$168 ==.
                                    545 ;	drivers/src/stm8s_clk.c: 266: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
      008662 0D 06            [ 1]  546 	tnz	(0x06, sp)
      008664 27 14            [ 1]  547 	jreq	00113$
      008666 7B 06            [ 1]  548 	ld	a, (0x06, sp)
      008668 4A               [ 1]  549 	dec	a
      008669 27 0F            [ 1]  550 	jreq	00113$
                           000175   551 	Sstm8s_clk$CLK_PeripheralClockConfig$169 ==.
      00866B 4B 0A            [ 1]  552 	push	#0x0a
                           000177   553 	Sstm8s_clk$CLK_PeripheralClockConfig$170 ==.
      00866D 4B 01            [ 1]  554 	push	#0x01
                           000179   555 	Sstm8s_clk$CLK_PeripheralClockConfig$171 ==.
      00866F 5F               [ 1]  556 	clrw	x
      008670 89               [ 2]  557 	pushw	x
                           00017B   558 	Sstm8s_clk$CLK_PeripheralClockConfig$172 ==.
      008671 4B E2            [ 1]  559 	push	#<(___str_0+0)
                           00017D   560 	Sstm8s_clk$CLK_PeripheralClockConfig$173 ==.
      008673 4B 80            [ 1]  561 	push	#((___str_0+0) >> 8)
                           00017F   562 	Sstm8s_clk$CLK_PeripheralClockConfig$174 ==.
      008675 CD 84 F3         [ 4]  563 	call	_assert_failed
      008678 5B 06            [ 2]  564 	addw	sp, #6
                           000184   565 	Sstm8s_clk$CLK_PeripheralClockConfig$175 ==.
      00867A                        566 00113$:
                           000184   567 	Sstm8s_clk$CLK_PeripheralClockConfig$176 ==.
                                    568 ;	drivers/src/stm8s_clk.c: 267: assert_param(IS_CLK_PERIPHERAL_OK(CLK_Peripheral));
      00867A 0D 05            [ 1]  569 	tnz	(0x05, sp)
      00867C 26 03            [ 1]  570 	jrne	00257$
      00867E CC 86 FA         [ 2]  571 	jp	00118$
      008681                        572 00257$:
      008681 7B 05            [ 1]  573 	ld	a, (0x05, sp)
      008683 4A               [ 1]  574 	dec	a
      008684 26 03            [ 1]  575 	jrne	00259$
      008686 CC 86 FA         [ 2]  576 	jp	00118$
      008689                        577 00259$:
                           000193   578 	Sstm8s_clk$CLK_PeripheralClockConfig$177 ==.
      008689 7B 05            [ 1]  579 	ld	a, (0x05, sp)
      00868B A0 03            [ 1]  580 	sub	a, #0x03
      00868D 26 02            [ 1]  581 	jrne	00262$
      00868F 4C               [ 1]  582 	inc	a
      008690 21                     583 	.byte 0x21
      008691                        584 00262$:
      008691 4F               [ 1]  585 	clr	a
      008692                        586 00263$:
                           00019C   587 	Sstm8s_clk$CLK_PeripheralClockConfig$178 ==.
      008692 4D               [ 1]  588 	tnz	a
      008693 27 03            [ 1]  589 	jreq	00264$
      008695 CC 86 FA         [ 2]  590 	jp	00118$
      008698                        591 00264$:
      008698 4D               [ 1]  592 	tnz	a
      008699 27 03            [ 1]  593 	jreq	00265$
      00869B CC 86 FA         [ 2]  594 	jp	00118$
      00869E                        595 00265$:
      00869E 7B 05            [ 1]  596 	ld	a, (0x05, sp)
      0086A0 A1 02            [ 1]  597 	cp	a, #0x02
      0086A2 26 03            [ 1]  598 	jrne	00267$
      0086A4 CC 86 FA         [ 2]  599 	jp	00118$
      0086A7                        600 00267$:
                           0001B1   601 	Sstm8s_clk$CLK_PeripheralClockConfig$179 ==.
      0086A7 7B 05            [ 1]  602 	ld	a, (0x05, sp)
      0086A9 A0 04            [ 1]  603 	sub	a, #0x04
      0086AB 26 04            [ 1]  604 	jrne	00270$
      0086AD 4C               [ 1]  605 	inc	a
      0086AE 97               [ 1]  606 	ld	xl, a
      0086AF 20 02            [ 2]  607 	jra	00271$
      0086B1                        608 00270$:
      0086B1 4F               [ 1]  609 	clr	a
      0086B2 97               [ 1]  610 	ld	xl, a
      0086B3                        611 00271$:
                           0001BD   612 	Sstm8s_clk$CLK_PeripheralClockConfig$180 ==.
      0086B3 9F               [ 1]  613 	ld	a, xl
      0086B4 4D               [ 1]  614 	tnz	a
      0086B5 27 03            [ 1]  615 	jreq	00272$
      0086B7 CC 86 FA         [ 2]  616 	jp	00118$
      0086BA                        617 00272$:
      0086BA 7B 05            [ 1]  618 	ld	a, (0x05, sp)
      0086BC A0 05            [ 1]  619 	sub	a, #0x05
      0086BE 26 02            [ 1]  620 	jrne	00274$
      0086C0 4C               [ 1]  621 	inc	a
      0086C1 21                     622 	.byte 0x21
      0086C2                        623 00274$:
      0086C2 4F               [ 1]  624 	clr	a
      0086C3                        625 00275$:
                           0001CD   626 	Sstm8s_clk$CLK_PeripheralClockConfig$181 ==.
      0086C3 4D               [ 1]  627 	tnz	a
      0086C4 26 34            [ 1]  628 	jrne	00118$
      0086C6 4D               [ 1]  629 	tnz	a
      0086C7 26 31            [ 1]  630 	jrne	00118$
      0086C9 9F               [ 1]  631 	ld	a, xl
      0086CA 4D               [ 1]  632 	tnz	a
      0086CB 26 2D            [ 1]  633 	jrne	00118$
      0086CD 7B 05            [ 1]  634 	ld	a, (0x05, sp)
      0086CF A1 06            [ 1]  635 	cp	a, #0x06
      0086D1 27 27            [ 1]  636 	jreq	00118$
                           0001DD   637 	Sstm8s_clk$CLK_PeripheralClockConfig$182 ==.
      0086D3 7B 05            [ 1]  638 	ld	a, (0x05, sp)
      0086D5 A1 07            [ 1]  639 	cp	a, #0x07
      0086D7 27 21            [ 1]  640 	jreq	00118$
                           0001E3   641 	Sstm8s_clk$CLK_PeripheralClockConfig$183 ==.
      0086D9 7B 05            [ 1]  642 	ld	a, (0x05, sp)
      0086DB A1 17            [ 1]  643 	cp	a, #0x17
      0086DD 27 1B            [ 1]  644 	jreq	00118$
                           0001E9   645 	Sstm8s_clk$CLK_PeripheralClockConfig$184 ==.
      0086DF 7B 05            [ 1]  646 	ld	a, (0x05, sp)
      0086E1 A1 13            [ 1]  647 	cp	a, #0x13
      0086E3 27 15            [ 1]  648 	jreq	00118$
                           0001EF   649 	Sstm8s_clk$CLK_PeripheralClockConfig$185 ==.
      0086E5 7B 05            [ 1]  650 	ld	a, (0x05, sp)
      0086E7 A1 12            [ 1]  651 	cp	a, #0x12
      0086E9 27 0F            [ 1]  652 	jreq	00118$
                           0001F5   653 	Sstm8s_clk$CLK_PeripheralClockConfig$186 ==.
      0086EB 4B 0B            [ 1]  654 	push	#0x0b
                           0001F7   655 	Sstm8s_clk$CLK_PeripheralClockConfig$187 ==.
      0086ED 4B 01            [ 1]  656 	push	#0x01
                           0001F9   657 	Sstm8s_clk$CLK_PeripheralClockConfig$188 ==.
      0086EF 5F               [ 1]  658 	clrw	x
      0086F0 89               [ 2]  659 	pushw	x
                           0001FB   660 	Sstm8s_clk$CLK_PeripheralClockConfig$189 ==.
      0086F1 4B E2            [ 1]  661 	push	#<(___str_0+0)
                           0001FD   662 	Sstm8s_clk$CLK_PeripheralClockConfig$190 ==.
      0086F3 4B 80            [ 1]  663 	push	#((___str_0+0) >> 8)
                           0001FF   664 	Sstm8s_clk$CLK_PeripheralClockConfig$191 ==.
      0086F5 CD 84 F3         [ 4]  665 	call	_assert_failed
      0086F8 5B 06            [ 2]  666 	addw	sp, #6
                           000204   667 	Sstm8s_clk$CLK_PeripheralClockConfig$192 ==.
      0086FA                        668 00118$:
                           000204   669 	Sstm8s_clk$CLK_PeripheralClockConfig$193 ==.
                                    670 ;	drivers/src/stm8s_clk.c: 274: CLK->PCKENR1 |= (uint8_t)((uint8_t)1 << ((uint8_t)CLK_Peripheral & (uint8_t)0x0F));
      0086FA 7B 05            [ 1]  671 	ld	a, (0x05, sp)
      0086FC A4 0F            [ 1]  672 	and	a, #0x0f
      0086FE 88               [ 1]  673 	push	a
                           000209   674 	Sstm8s_clk$CLK_PeripheralClockConfig$194 ==.
      0086FF A6 01            [ 1]  675 	ld	a, #0x01
      008701 6B 02            [ 1]  676 	ld	(0x02, sp), a
      008703 84               [ 1]  677 	pop	a
                           00020E   678 	Sstm8s_clk$CLK_PeripheralClockConfig$195 ==.
      008704 4D               [ 1]  679 	tnz	a
      008705 27 05            [ 1]  680 	jreq	00295$
      008707                        681 00294$:
      008707 08 01            [ 1]  682 	sll	(0x01, sp)
      008709 4A               [ 1]  683 	dec	a
      00870A 26 FB            [ 1]  684 	jrne	00294$
      00870C                        685 00295$:
                           000216   686 	Sstm8s_clk$CLK_PeripheralClockConfig$196 ==.
                                    687 ;	drivers/src/stm8s_clk.c: 279: CLK->PCKENR1 &= (uint8_t)(~(uint8_t)(((uint8_t)1 << ((uint8_t)CLK_Peripheral & (uint8_t)0x0F))));
      00870C 7B 01            [ 1]  688 	ld	a, (0x01, sp)
      00870E 43               [ 1]  689 	cpl	a
      00870F 6B 02            [ 1]  690 	ld	(0x02, sp), a
                           00021B   691 	Sstm8s_clk$CLK_PeripheralClockConfig$197 ==.
                                    692 ;	drivers/src/stm8s_clk.c: 269: if (((uint8_t)CLK_Peripheral & (uint8_t)0x10) == 0x00)
      008711 7B 05            [ 1]  693 	ld	a, (0x05, sp)
      008713 A5 10            [ 1]  694 	bcp	a, #0x10
      008715 26 15            [ 1]  695 	jrne	00108$
                           000221   696 	Sstm8s_clk$CLK_PeripheralClockConfig$198 ==.
                                    697 ;	drivers/src/stm8s_clk.c: 274: CLK->PCKENR1 |= (uint8_t)((uint8_t)1 << ((uint8_t)CLK_Peripheral & (uint8_t)0x0F));
      008717 C6 50 C7         [ 1]  698 	ld	a, 0x50c7
                           000224   699 	Sstm8s_clk$CLK_PeripheralClockConfig$199 ==.
                           000224   700 	Sstm8s_clk$CLK_PeripheralClockConfig$200 ==.
                                    701 ;	drivers/src/stm8s_clk.c: 271: if (NewState != DISABLE)
      00871A 0D 06            [ 1]  702 	tnz	(0x06, sp)
      00871C 27 07            [ 1]  703 	jreq	00102$
                           000228   704 	Sstm8s_clk$CLK_PeripheralClockConfig$201 ==.
                           000228   705 	Sstm8s_clk$CLK_PeripheralClockConfig$202 ==.
                                    706 ;	drivers/src/stm8s_clk.c: 274: CLK->PCKENR1 |= (uint8_t)((uint8_t)1 << ((uint8_t)CLK_Peripheral & (uint8_t)0x0F));
      00871E 1A 01            [ 1]  707 	or	a, (0x01, sp)
      008720 C7 50 C7         [ 1]  708 	ld	0x50c7, a
                           00022D   709 	Sstm8s_clk$CLK_PeripheralClockConfig$203 ==.
      008723 20 1A            [ 2]  710 	jra	00110$
      008725                        711 00102$:
                           00022F   712 	Sstm8s_clk$CLK_PeripheralClockConfig$204 ==.
                           00022F   713 	Sstm8s_clk$CLK_PeripheralClockConfig$205 ==.
                                    714 ;	drivers/src/stm8s_clk.c: 279: CLK->PCKENR1 &= (uint8_t)(~(uint8_t)(((uint8_t)1 << ((uint8_t)CLK_Peripheral & (uint8_t)0x0F))));
      008725 14 02            [ 1]  715 	and	a, (0x02, sp)
      008727 C7 50 C7         [ 1]  716 	ld	0x50c7, a
                           000234   717 	Sstm8s_clk$CLK_PeripheralClockConfig$206 ==.
      00872A 20 13            [ 2]  718 	jra	00110$
      00872C                        719 00108$:
                           000236   720 	Sstm8s_clk$CLK_PeripheralClockConfig$207 ==.
                                    721 ;	drivers/src/stm8s_clk.c: 287: CLK->PCKENR2 |= (uint8_t)((uint8_t)1 << ((uint8_t)CLK_Peripheral & (uint8_t)0x0F));
      00872C C6 50 CA         [ 1]  722 	ld	a, 0x50ca
                           000239   723 	Sstm8s_clk$CLK_PeripheralClockConfig$208 ==.
                           000239   724 	Sstm8s_clk$CLK_PeripheralClockConfig$209 ==.
                                    725 ;	drivers/src/stm8s_clk.c: 284: if (NewState != DISABLE)
      00872F 0D 06            [ 1]  726 	tnz	(0x06, sp)
      008731 27 07            [ 1]  727 	jreq	00105$
                           00023D   728 	Sstm8s_clk$CLK_PeripheralClockConfig$210 ==.
                           00023D   729 	Sstm8s_clk$CLK_PeripheralClockConfig$211 ==.
                                    730 ;	drivers/src/stm8s_clk.c: 287: CLK->PCKENR2 |= (uint8_t)((uint8_t)1 << ((uint8_t)CLK_Peripheral & (uint8_t)0x0F));
      008733 1A 01            [ 1]  731 	or	a, (0x01, sp)
      008735 C7 50 CA         [ 1]  732 	ld	0x50ca, a
                           000242   733 	Sstm8s_clk$CLK_PeripheralClockConfig$212 ==.
      008738 20 05            [ 2]  734 	jra	00110$
      00873A                        735 00105$:
                           000244   736 	Sstm8s_clk$CLK_PeripheralClockConfig$213 ==.
                           000244   737 	Sstm8s_clk$CLK_PeripheralClockConfig$214 ==.
                                    738 ;	drivers/src/stm8s_clk.c: 292: CLK->PCKENR2 &= (uint8_t)(~(uint8_t)(((uint8_t)1 << ((uint8_t)CLK_Peripheral & (uint8_t)0x0F))));
      00873A 14 02            [ 1]  739 	and	a, (0x02, sp)
      00873C C7 50 CA         [ 1]  740 	ld	0x50ca, a
                           000249   741 	Sstm8s_clk$CLK_PeripheralClockConfig$215 ==.
      00873F                        742 00110$:
                           000249   743 	Sstm8s_clk$CLK_PeripheralClockConfig$216 ==.
                                    744 ;	drivers/src/stm8s_clk.c: 295: }
      00873F 85               [ 2]  745 	popw	x
                           00024A   746 	Sstm8s_clk$CLK_PeripheralClockConfig$217 ==.
                           00024A   747 	Sstm8s_clk$CLK_PeripheralClockConfig$218 ==.
                           00024A   748 	XG$CLK_PeripheralClockConfig$0$0 ==.
      008740 81               [ 4]  749 	ret
                           00024B   750 	Sstm8s_clk$CLK_PeripheralClockConfig$219 ==.
                           00024B   751 	Sstm8s_clk$CLK_ClockSwitchConfig$220 ==.
                                    752 ;	drivers/src/stm8s_clk.c: 309: ErrorStatus CLK_ClockSwitchConfig(CLK_SwitchMode_TypeDef CLK_SwitchMode, CLK_Source_TypeDef CLK_NewClock, FunctionalState ITState, CLK_CurrentClockState_TypeDef CLK_CurrentClockState)
                                    753 ;	-----------------------------------------
                                    754 ;	 function CLK_ClockSwitchConfig
                                    755 ;	-----------------------------------------
      008741                        756 _CLK_ClockSwitchConfig:
                           00024B   757 	Sstm8s_clk$CLK_ClockSwitchConfig$221 ==.
      008741 88               [ 1]  758 	push	a
                           00024C   759 	Sstm8s_clk$CLK_ClockSwitchConfig$222 ==.
                           00024C   760 	Sstm8s_clk$CLK_ClockSwitchConfig$223 ==.
                                    761 ;	drivers/src/stm8s_clk.c: 316: assert_param(IS_CLK_SOURCE_OK(CLK_NewClock));
      008742 7B 05            [ 1]  762 	ld	a, (0x05, sp)
      008744 A1 E1            [ 1]  763 	cp	a, #0xe1
      008746 27 1B            [ 1]  764 	jreq	00140$
                           000252   765 	Sstm8s_clk$CLK_ClockSwitchConfig$224 ==.
      008748 7B 05            [ 1]  766 	ld	a, (0x05, sp)
      00874A A1 D2            [ 1]  767 	cp	a, #0xd2
      00874C 27 15            [ 1]  768 	jreq	00140$
                           000258   769 	Sstm8s_clk$CLK_ClockSwitchConfig$225 ==.
      00874E 7B 05            [ 1]  770 	ld	a, (0x05, sp)
      008750 A1 B4            [ 1]  771 	cp	a, #0xb4
      008752 27 0F            [ 1]  772 	jreq	00140$
                           00025E   773 	Sstm8s_clk$CLK_ClockSwitchConfig$226 ==.
      008754 4B 3C            [ 1]  774 	push	#0x3c
                           000260   775 	Sstm8s_clk$CLK_ClockSwitchConfig$227 ==.
      008756 4B 01            [ 1]  776 	push	#0x01
                           000262   777 	Sstm8s_clk$CLK_ClockSwitchConfig$228 ==.
      008758 5F               [ 1]  778 	clrw	x
      008759 89               [ 2]  779 	pushw	x
                           000264   780 	Sstm8s_clk$CLK_ClockSwitchConfig$229 ==.
      00875A 4B E2            [ 1]  781 	push	#<(___str_0+0)
                           000266   782 	Sstm8s_clk$CLK_ClockSwitchConfig$230 ==.
      00875C 4B 80            [ 1]  783 	push	#((___str_0+0) >> 8)
                           000268   784 	Sstm8s_clk$CLK_ClockSwitchConfig$231 ==.
      00875E CD 84 F3         [ 4]  785 	call	_assert_failed
      008761 5B 06            [ 2]  786 	addw	sp, #6
                           00026D   787 	Sstm8s_clk$CLK_ClockSwitchConfig$232 ==.
      008763                        788 00140$:
                           00026D   789 	Sstm8s_clk$CLK_ClockSwitchConfig$233 ==.
                                    790 ;	drivers/src/stm8s_clk.c: 317: assert_param(IS_CLK_SWITCHMODE_OK(CLK_SwitchMode));
      008763 7B 04            [ 1]  791 	ld	a, (0x04, sp)
      008765 4A               [ 1]  792 	dec	a
      008766 26 05            [ 1]  793 	jrne	00309$
      008768 A6 01            [ 1]  794 	ld	a, #0x01
      00876A 6B 01            [ 1]  795 	ld	(0x01, sp), a
      00876C C5                     796 	.byte 0xc5
      00876D                        797 00309$:
      00876D 0F 01            [ 1]  798 	clr	(0x01, sp)
      00876F                        799 00310$:
                           000279   800 	Sstm8s_clk$CLK_ClockSwitchConfig$234 ==.
      00876F 0D 04            [ 1]  801 	tnz	(0x04, sp)
      008771 27 13            [ 1]  802 	jreq	00148$
      008773 0D 01            [ 1]  803 	tnz	(0x01, sp)
      008775 26 0F            [ 1]  804 	jrne	00148$
      008777 4B 3D            [ 1]  805 	push	#0x3d
                           000283   806 	Sstm8s_clk$CLK_ClockSwitchConfig$235 ==.
      008779 4B 01            [ 1]  807 	push	#0x01
                           000285   808 	Sstm8s_clk$CLK_ClockSwitchConfig$236 ==.
      00877B 5F               [ 1]  809 	clrw	x
      00877C 89               [ 2]  810 	pushw	x
                           000287   811 	Sstm8s_clk$CLK_ClockSwitchConfig$237 ==.
      00877D 4B E2            [ 1]  812 	push	#<(___str_0+0)
                           000289   813 	Sstm8s_clk$CLK_ClockSwitchConfig$238 ==.
      00877F 4B 80            [ 1]  814 	push	#((___str_0+0) >> 8)
                           00028B   815 	Sstm8s_clk$CLK_ClockSwitchConfig$239 ==.
      008781 CD 84 F3         [ 4]  816 	call	_assert_failed
      008784 5B 06            [ 2]  817 	addw	sp, #6
                           000290   818 	Sstm8s_clk$CLK_ClockSwitchConfig$240 ==.
      008786                        819 00148$:
                           000290   820 	Sstm8s_clk$CLK_ClockSwitchConfig$241 ==.
                                    821 ;	drivers/src/stm8s_clk.c: 318: assert_param(IS_FUNCTIONALSTATE_OK(ITState));
      008786 0D 06            [ 1]  822 	tnz	(0x06, sp)
      008788 27 14            [ 1]  823 	jreq	00153$
      00878A 7B 06            [ 1]  824 	ld	a, (0x06, sp)
      00878C 4A               [ 1]  825 	dec	a
      00878D 27 0F            [ 1]  826 	jreq	00153$
                           000299   827 	Sstm8s_clk$CLK_ClockSwitchConfig$242 ==.
      00878F 4B 3E            [ 1]  828 	push	#0x3e
                           00029B   829 	Sstm8s_clk$CLK_ClockSwitchConfig$243 ==.
      008791 4B 01            [ 1]  830 	push	#0x01
                           00029D   831 	Sstm8s_clk$CLK_ClockSwitchConfig$244 ==.
      008793 5F               [ 1]  832 	clrw	x
      008794 89               [ 2]  833 	pushw	x
                           00029F   834 	Sstm8s_clk$CLK_ClockSwitchConfig$245 ==.
      008795 4B E2            [ 1]  835 	push	#<(___str_0+0)
                           0002A1   836 	Sstm8s_clk$CLK_ClockSwitchConfig$246 ==.
      008797 4B 80            [ 1]  837 	push	#((___str_0+0) >> 8)
                           0002A3   838 	Sstm8s_clk$CLK_ClockSwitchConfig$247 ==.
      008799 CD 84 F3         [ 4]  839 	call	_assert_failed
      00879C 5B 06            [ 2]  840 	addw	sp, #6
                           0002A8   841 	Sstm8s_clk$CLK_ClockSwitchConfig$248 ==.
      00879E                        842 00153$:
                           0002A8   843 	Sstm8s_clk$CLK_ClockSwitchConfig$249 ==.
                                    844 ;	drivers/src/stm8s_clk.c: 319: assert_param(IS_CLK_CURRENTCLOCKSTATE_OK(CLK_CurrentClockState));
      00879E 0D 07            [ 1]  845 	tnz	(0x07, sp)
      0087A0 27 14            [ 1]  846 	jreq	00158$
      0087A2 7B 07            [ 1]  847 	ld	a, (0x07, sp)
      0087A4 4A               [ 1]  848 	dec	a
      0087A5 27 0F            [ 1]  849 	jreq	00158$
                           0002B1   850 	Sstm8s_clk$CLK_ClockSwitchConfig$250 ==.
      0087A7 4B 3F            [ 1]  851 	push	#0x3f
                           0002B3   852 	Sstm8s_clk$CLK_ClockSwitchConfig$251 ==.
      0087A9 4B 01            [ 1]  853 	push	#0x01
                           0002B5   854 	Sstm8s_clk$CLK_ClockSwitchConfig$252 ==.
      0087AB 5F               [ 1]  855 	clrw	x
      0087AC 89               [ 2]  856 	pushw	x
                           0002B7   857 	Sstm8s_clk$CLK_ClockSwitchConfig$253 ==.
      0087AD 4B E2            [ 1]  858 	push	#<(___str_0+0)
                           0002B9   859 	Sstm8s_clk$CLK_ClockSwitchConfig$254 ==.
      0087AF 4B 80            [ 1]  860 	push	#((___str_0+0) >> 8)
                           0002BB   861 	Sstm8s_clk$CLK_ClockSwitchConfig$255 ==.
      0087B1 CD 84 F3         [ 4]  862 	call	_assert_failed
      0087B4 5B 06            [ 2]  863 	addw	sp, #6
                           0002C0   864 	Sstm8s_clk$CLK_ClockSwitchConfig$256 ==.
      0087B6                        865 00158$:
                           0002C0   866 	Sstm8s_clk$CLK_ClockSwitchConfig$257 ==.
                                    867 ;	drivers/src/stm8s_clk.c: 322: clock_master = (CLK_Source_TypeDef)CLK->CMSR;
      0087B6 C6 50 C3         [ 1]  868 	ld	a, 0x50c3
      0087B9 90 97            [ 1]  869 	ld	yl, a
                           0002C5   870 	Sstm8s_clk$CLK_ClockSwitchConfig$258 ==.
                                    871 ;	drivers/src/stm8s_clk.c: 328: CLK->SWCR |= CLK_SWCR_SWEN;
      0087BB C6 50 C5         [ 1]  872 	ld	a, 0x50c5
      0087BE 97               [ 1]  873 	ld	xl, a
                           0002C9   874 	Sstm8s_clk$CLK_ClockSwitchConfig$259 ==.
                                    875 ;	drivers/src/stm8s_clk.c: 325: if (CLK_SwitchMode == CLK_SWITCHMODE_AUTO)
      0087BF 7B 01            [ 1]  876 	ld	a, (0x01, sp)
      0087C1 26 03            [ 1]  877 	jrne	00321$
      0087C3 CC 88 00         [ 2]  878 	jp	00122$
      0087C6                        879 00321$:
                           0002D0   880 	Sstm8s_clk$CLK_ClockSwitchConfig$260 ==.
                           0002D0   881 	Sstm8s_clk$CLK_ClockSwitchConfig$261 ==.
                                    882 ;	drivers/src/stm8s_clk.c: 328: CLK->SWCR |= CLK_SWCR_SWEN;
      0087C6 9F               [ 1]  883 	ld	a, xl
      0087C7 AA 02            [ 1]  884 	or	a, #0x02
      0087C9 C7 50 C5         [ 1]  885 	ld	0x50c5, a
                           0002D6   886 	Sstm8s_clk$CLK_ClockSwitchConfig$262 ==.
      0087CC C6 50 C5         [ 1]  887 	ld	a, 0x50c5
                           0002D9   888 	Sstm8s_clk$CLK_ClockSwitchConfig$263 ==.
                                    889 ;	drivers/src/stm8s_clk.c: 331: if (ITState != DISABLE)
      0087CF 0D 06            [ 1]  890 	tnz	(0x06, sp)
      0087D1 27 07            [ 1]  891 	jreq	00102$
                           0002DD   892 	Sstm8s_clk$CLK_ClockSwitchConfig$264 ==.
                           0002DD   893 	Sstm8s_clk$CLK_ClockSwitchConfig$265 ==.
                                    894 ;	drivers/src/stm8s_clk.c: 333: CLK->SWCR |= CLK_SWCR_SWIEN;
      0087D3 AA 04            [ 1]  895 	or	a, #0x04
      0087D5 C7 50 C5         [ 1]  896 	ld	0x50c5, a
                           0002E2   897 	Sstm8s_clk$CLK_ClockSwitchConfig$266 ==.
      0087D8 20 05            [ 2]  898 	jra	00103$
      0087DA                        899 00102$:
                           0002E4   900 	Sstm8s_clk$CLK_ClockSwitchConfig$267 ==.
                           0002E4   901 	Sstm8s_clk$CLK_ClockSwitchConfig$268 ==.
                                    902 ;	drivers/src/stm8s_clk.c: 337: CLK->SWCR &= (uint8_t)(~CLK_SWCR_SWIEN);
      0087DA A4 FB            [ 1]  903 	and	a, #0xfb
      0087DC C7 50 C5         [ 1]  904 	ld	0x50c5, a
                           0002E9   905 	Sstm8s_clk$CLK_ClockSwitchConfig$269 ==.
      0087DF                        906 00103$:
                           0002E9   907 	Sstm8s_clk$CLK_ClockSwitchConfig$270 ==.
                                    908 ;	drivers/src/stm8s_clk.c: 341: CLK->SWR = (uint8_t)CLK_NewClock;
      0087DF AE 50 C4         [ 2]  909 	ldw	x, #0x50c4
      0087E2 7B 05            [ 1]  910 	ld	a, (0x05, sp)
      0087E4 F7               [ 1]  911 	ld	(x), a
                           0002EF   912 	Sstm8s_clk$CLK_ClockSwitchConfig$271 ==.
                           0002EF   913 	Sstm8s_clk$CLK_ClockSwitchConfig$272 ==.
                                    914 ;	drivers/src/stm8s_clk.c: 344: while((((CLK->SWCR & CLK_SWCR_SWBSY) != 0 )&& (DownCounter != 0)))
      0087E5 5F               [ 1]  915 	clrw	x
      0087E6 5A               [ 2]  916 	decw	x
      0087E7                        917 00105$:
      0087E7 C6 50 C5         [ 1]  918 	ld	a, 0x50c5
      0087EA 44               [ 1]  919 	srl	a
      0087EB 24 06            [ 1]  920 	jrnc	00107$
      0087ED 5D               [ 2]  921 	tnzw	x
      0087EE 27 03            [ 1]  922 	jreq	00107$
                           0002FA   923 	Sstm8s_clk$CLK_ClockSwitchConfig$273 ==.
                           0002FA   924 	Sstm8s_clk$CLK_ClockSwitchConfig$274 ==.
                                    925 ;	drivers/src/stm8s_clk.c: 346: DownCounter--;
      0087F0 5A               [ 2]  926 	decw	x
                           0002FB   927 	Sstm8s_clk$CLK_ClockSwitchConfig$275 ==.
      0087F1 20 F4            [ 2]  928 	jra	00105$
      0087F3                        929 00107$:
                           0002FD   930 	Sstm8s_clk$CLK_ClockSwitchConfig$276 ==.
                                    931 ;	drivers/src/stm8s_clk.c: 349: if(DownCounter != 0)
      0087F3 5D               [ 2]  932 	tnzw	x
      0087F4 27 06            [ 1]  933 	jreq	00109$
                           000300   934 	Sstm8s_clk$CLK_ClockSwitchConfig$277 ==.
                           000300   935 	Sstm8s_clk$CLK_ClockSwitchConfig$278 ==.
                                    936 ;	drivers/src/stm8s_clk.c: 351: Swif = SUCCESS;
      0087F6 A6 01            [ 1]  937 	ld	a, #0x01
      0087F8 97               [ 1]  938 	ld	xl, a
                           000303   939 	Sstm8s_clk$CLK_ClockSwitchConfig$279 ==.
      0087F9 CC 88 34         [ 2]  940 	jp	00123$
      0087FC                        941 00109$:
                           000306   942 	Sstm8s_clk$CLK_ClockSwitchConfig$280 ==.
                           000306   943 	Sstm8s_clk$CLK_ClockSwitchConfig$281 ==.
                                    944 ;	drivers/src/stm8s_clk.c: 355: Swif = ERROR;
      0087FC 5F               [ 1]  945 	clrw	x
                           000307   946 	Sstm8s_clk$CLK_ClockSwitchConfig$282 ==.
      0087FD CC 88 34         [ 2]  947 	jp	00123$
      008800                        948 00122$:
                           00030A   949 	Sstm8s_clk$CLK_ClockSwitchConfig$283 ==.
                           00030A   950 	Sstm8s_clk$CLK_ClockSwitchConfig$284 ==.
                                    951 ;	drivers/src/stm8s_clk.c: 361: if (ITState != DISABLE)
      008800 0D 06            [ 1]  952 	tnz	(0x06, sp)
      008802 27 08            [ 1]  953 	jreq	00112$
                           00030E   954 	Sstm8s_clk$CLK_ClockSwitchConfig$285 ==.
                           00030E   955 	Sstm8s_clk$CLK_ClockSwitchConfig$286 ==.
                                    956 ;	drivers/src/stm8s_clk.c: 363: CLK->SWCR |= CLK_SWCR_SWIEN;
      008804 9F               [ 1]  957 	ld	a, xl
      008805 AA 04            [ 1]  958 	or	a, #0x04
      008807 C7 50 C5         [ 1]  959 	ld	0x50c5, a
                           000314   960 	Sstm8s_clk$CLK_ClockSwitchConfig$287 ==.
      00880A 20 06            [ 2]  961 	jra	00113$
      00880C                        962 00112$:
                           000316   963 	Sstm8s_clk$CLK_ClockSwitchConfig$288 ==.
                           000316   964 	Sstm8s_clk$CLK_ClockSwitchConfig$289 ==.
                                    965 ;	drivers/src/stm8s_clk.c: 367: CLK->SWCR &= (uint8_t)(~CLK_SWCR_SWIEN);
      00880C 9F               [ 1]  966 	ld	a, xl
      00880D A4 FB            [ 1]  967 	and	a, #0xfb
      00880F C7 50 C5         [ 1]  968 	ld	0x50c5, a
                           00031C   969 	Sstm8s_clk$CLK_ClockSwitchConfig$290 ==.
      008812                        970 00113$:
                           00031C   971 	Sstm8s_clk$CLK_ClockSwitchConfig$291 ==.
                                    972 ;	drivers/src/stm8s_clk.c: 371: CLK->SWR = (uint8_t)CLK_NewClock;
      008812 AE 50 C4         [ 2]  973 	ldw	x, #0x50c4
      008815 7B 05            [ 1]  974 	ld	a, (0x05, sp)
      008817 F7               [ 1]  975 	ld	(x), a
                           000322   976 	Sstm8s_clk$CLK_ClockSwitchConfig$292 ==.
                           000322   977 	Sstm8s_clk$CLK_ClockSwitchConfig$293 ==.
                                    978 ;	drivers/src/stm8s_clk.c: 374: while((((CLK->SWCR & CLK_SWCR_SWIF) != 0 ) && (DownCounter != 0)))
      008818 5F               [ 1]  979 	clrw	x
      008819 5A               [ 2]  980 	decw	x
      00881A                        981 00115$:
      00881A C6 50 C5         [ 1]  982 	ld	a, 0x50c5
      00881D A5 08            [ 1]  983 	bcp	a, #0x08
      00881F 27 06            [ 1]  984 	jreq	00117$
      008821 5D               [ 2]  985 	tnzw	x
      008822 27 03            [ 1]  986 	jreq	00117$
                           00032E   987 	Sstm8s_clk$CLK_ClockSwitchConfig$294 ==.
                           00032E   988 	Sstm8s_clk$CLK_ClockSwitchConfig$295 ==.
                                    989 ;	drivers/src/stm8s_clk.c: 376: DownCounter--;
      008824 5A               [ 2]  990 	decw	x
                           00032F   991 	Sstm8s_clk$CLK_ClockSwitchConfig$296 ==.
      008825 20 F3            [ 2]  992 	jra	00115$
      008827                        993 00117$:
                           000331   994 	Sstm8s_clk$CLK_ClockSwitchConfig$297 ==.
                                    995 ;	drivers/src/stm8s_clk.c: 379: if(DownCounter != 0)
      008827 5D               [ 2]  996 	tnzw	x
      008828 27 09            [ 1]  997 	jreq	00119$
                           000334   998 	Sstm8s_clk$CLK_ClockSwitchConfig$298 ==.
                           000334   999 	Sstm8s_clk$CLK_ClockSwitchConfig$299 ==.
                                   1000 ;	drivers/src/stm8s_clk.c: 382: CLK->SWCR |= CLK_SWCR_SWEN;
      00882A 72 12 50 C5      [ 1] 1001 	bset	20677, #1
                           000338  1002 	Sstm8s_clk$CLK_ClockSwitchConfig$300 ==.
                                   1003 ;	drivers/src/stm8s_clk.c: 383: Swif = SUCCESS;
      00882E A6 01            [ 1] 1004 	ld	a, #0x01
      008830 97               [ 1] 1005 	ld	xl, a
                           00033B  1006 	Sstm8s_clk$CLK_ClockSwitchConfig$301 ==.
      008831 20 01            [ 2] 1007 	jra	00123$
      008833                       1008 00119$:
                           00033D  1009 	Sstm8s_clk$CLK_ClockSwitchConfig$302 ==.
                           00033D  1010 	Sstm8s_clk$CLK_ClockSwitchConfig$303 ==.
                                   1011 ;	drivers/src/stm8s_clk.c: 387: Swif = ERROR;
      008833 5F               [ 1] 1012 	clrw	x
                           00033E  1013 	Sstm8s_clk$CLK_ClockSwitchConfig$304 ==.
      008834                       1014 00123$:
                           00033E  1015 	Sstm8s_clk$CLK_ClockSwitchConfig$305 ==.
                                   1016 ;	drivers/src/stm8s_clk.c: 390: if(Swif != ERROR)
      008834 9F               [ 1] 1017 	ld	a, xl
      008835 4D               [ 1] 1018 	tnz	a
      008836 26 03            [ 1] 1019 	jrne	00330$
      008838 CC 88 69         [ 2] 1020 	jp	00136$
      00883B                       1021 00330$:
                           000345  1022 	Sstm8s_clk$CLK_ClockSwitchConfig$306 ==.
                           000345  1023 	Sstm8s_clk$CLK_ClockSwitchConfig$307 ==.
                                   1024 ;	drivers/src/stm8s_clk.c: 393: if((CLK_CurrentClockState == CLK_CURRENTCLOCKSTATE_DISABLE) && ( clock_master == CLK_SOURCE_HSI))
      00883B 0D 07            [ 1] 1025 	tnz	(0x07, sp)
      00883D 26 0C            [ 1] 1026 	jrne	00132$
      00883F 90 9F            [ 1] 1027 	ld	a, yl
      008841 A1 E1            [ 1] 1028 	cp	a, #0xe1
      008843 26 06            [ 1] 1029 	jrne	00132$
                           00034F  1030 	Sstm8s_clk$CLK_ClockSwitchConfig$308 ==.
                           00034F  1031 	Sstm8s_clk$CLK_ClockSwitchConfig$309 ==.
                           00034F  1032 	Sstm8s_clk$CLK_ClockSwitchConfig$310 ==.
                                   1033 ;	drivers/src/stm8s_clk.c: 395: CLK->ICKR &= (uint8_t)(~CLK_ICKR_HSIEN);
      008845 72 11 50 C0      [ 1] 1034 	bres	20672, #0
                           000353  1035 	Sstm8s_clk$CLK_ClockSwitchConfig$311 ==.
      008849 20 1E            [ 2] 1036 	jra	00136$
      00884B                       1037 00132$:
                           000355  1038 	Sstm8s_clk$CLK_ClockSwitchConfig$312 ==.
                                   1039 ;	drivers/src/stm8s_clk.c: 397: else if((CLK_CurrentClockState == CLK_CURRENTCLOCKSTATE_DISABLE) && ( clock_master == CLK_SOURCE_LSI))
      00884B 0D 07            [ 1] 1040 	tnz	(0x07, sp)
      00884D 26 0C            [ 1] 1041 	jrne	00128$
      00884F 90 9F            [ 1] 1042 	ld	a, yl
      008851 A1 D2            [ 1] 1043 	cp	a, #0xd2
      008853 26 06            [ 1] 1044 	jrne	00128$
                           00035F  1045 	Sstm8s_clk$CLK_ClockSwitchConfig$313 ==.
                           00035F  1046 	Sstm8s_clk$CLK_ClockSwitchConfig$314 ==.
                           00035F  1047 	Sstm8s_clk$CLK_ClockSwitchConfig$315 ==.
                                   1048 ;	drivers/src/stm8s_clk.c: 399: CLK->ICKR &= (uint8_t)(~CLK_ICKR_LSIEN);
      008855 72 17 50 C0      [ 1] 1049 	bres	20672, #3
                           000363  1050 	Sstm8s_clk$CLK_ClockSwitchConfig$316 ==.
      008859 20 0E            [ 2] 1051 	jra	00136$
      00885B                       1052 00128$:
                           000365  1053 	Sstm8s_clk$CLK_ClockSwitchConfig$317 ==.
                                   1054 ;	drivers/src/stm8s_clk.c: 401: else if ((CLK_CurrentClockState == CLK_CURRENTCLOCKSTATE_DISABLE) && ( clock_master == CLK_SOURCE_HSE))
      00885B 0D 07            [ 1] 1055 	tnz	(0x07, sp)
      00885D 26 0A            [ 1] 1056 	jrne	00136$
      00885F 90 9F            [ 1] 1057 	ld	a, yl
      008861 A1 B4            [ 1] 1058 	cp	a, #0xb4
      008863 26 04            [ 1] 1059 	jrne	00136$
                           00036F  1060 	Sstm8s_clk$CLK_ClockSwitchConfig$318 ==.
                           00036F  1061 	Sstm8s_clk$CLK_ClockSwitchConfig$319 ==.
                           00036F  1062 	Sstm8s_clk$CLK_ClockSwitchConfig$320 ==.
                                   1063 ;	drivers/src/stm8s_clk.c: 403: CLK->ECKR &= (uint8_t)(~CLK_ECKR_HSEEN);
      008865 72 11 50 C1      [ 1] 1064 	bres	20673, #0
                           000373  1065 	Sstm8s_clk$CLK_ClockSwitchConfig$321 ==.
      008869                       1066 00136$:
                           000373  1067 	Sstm8s_clk$CLK_ClockSwitchConfig$322 ==.
                                   1068 ;	drivers/src/stm8s_clk.c: 406: return(Swif);
      008869 9F               [ 1] 1069 	ld	a, xl
                           000374  1070 	Sstm8s_clk$CLK_ClockSwitchConfig$323 ==.
                                   1071 ;	drivers/src/stm8s_clk.c: 407: }
      00886A 5B 01            [ 2] 1072 	addw	sp, #1
                           000376  1073 	Sstm8s_clk$CLK_ClockSwitchConfig$324 ==.
                           000376  1074 	Sstm8s_clk$CLK_ClockSwitchConfig$325 ==.
                           000376  1075 	XG$CLK_ClockSwitchConfig$0$0 ==.
      00886C 81               [ 4] 1076 	ret
                           000377  1077 	Sstm8s_clk$CLK_ClockSwitchConfig$326 ==.
                           000377  1078 	Sstm8s_clk$CLK_HSIPrescalerConfig$327 ==.
                                   1079 ;	drivers/src/stm8s_clk.c: 415: void CLK_HSIPrescalerConfig(CLK_Prescaler_TypeDef HSIPrescaler)
                                   1080 ;	-----------------------------------------
                                   1081 ;	 function CLK_HSIPrescalerConfig
                                   1082 ;	-----------------------------------------
      00886D                       1083 _CLK_HSIPrescalerConfig:
                           000377  1084 	Sstm8s_clk$CLK_HSIPrescalerConfig$328 ==.
                           000377  1085 	Sstm8s_clk$CLK_HSIPrescalerConfig$329 ==.
                                   1086 ;	drivers/src/stm8s_clk.c: 418: assert_param(IS_CLK_HSIPRESCALER_OK(HSIPrescaler));
      00886D 0D 03            [ 1] 1087 	tnz	(0x03, sp)
      00886F 27 21            [ 1] 1088 	jreq	00104$
      008871 7B 03            [ 1] 1089 	ld	a, (0x03, sp)
      008873 A1 08            [ 1] 1090 	cp	a, #0x08
      008875 27 1B            [ 1] 1091 	jreq	00104$
                           000381  1092 	Sstm8s_clk$CLK_HSIPrescalerConfig$330 ==.
      008877 7B 03            [ 1] 1093 	ld	a, (0x03, sp)
      008879 A1 10            [ 1] 1094 	cp	a, #0x10
      00887B 27 15            [ 1] 1095 	jreq	00104$
                           000387  1096 	Sstm8s_clk$CLK_HSIPrescalerConfig$331 ==.
      00887D 7B 03            [ 1] 1097 	ld	a, (0x03, sp)
      00887F A1 18            [ 1] 1098 	cp	a, #0x18
      008881 27 0F            [ 1] 1099 	jreq	00104$
                           00038D  1100 	Sstm8s_clk$CLK_HSIPrescalerConfig$332 ==.
      008883 4B A2            [ 1] 1101 	push	#0xa2
                           00038F  1102 	Sstm8s_clk$CLK_HSIPrescalerConfig$333 ==.
      008885 4B 01            [ 1] 1103 	push	#0x01
                           000391  1104 	Sstm8s_clk$CLK_HSIPrescalerConfig$334 ==.
      008887 5F               [ 1] 1105 	clrw	x
      008888 89               [ 2] 1106 	pushw	x
                           000393  1107 	Sstm8s_clk$CLK_HSIPrescalerConfig$335 ==.
      008889 4B E2            [ 1] 1108 	push	#<(___str_0+0)
                           000395  1109 	Sstm8s_clk$CLK_HSIPrescalerConfig$336 ==.
      00888B 4B 80            [ 1] 1110 	push	#((___str_0+0) >> 8)
                           000397  1111 	Sstm8s_clk$CLK_HSIPrescalerConfig$337 ==.
      00888D CD 84 F3         [ 4] 1112 	call	_assert_failed
      008890 5B 06            [ 2] 1113 	addw	sp, #6
                           00039C  1114 	Sstm8s_clk$CLK_HSIPrescalerConfig$338 ==.
      008892                       1115 00104$:
                           00039C  1116 	Sstm8s_clk$CLK_HSIPrescalerConfig$339 ==.
                                   1117 ;	drivers/src/stm8s_clk.c: 421: CLK->CKDIVR &= (uint8_t)(~CLK_CKDIVR_HSIDIV);
      008892 C6 50 C6         [ 1] 1118 	ld	a, 0x50c6
      008895 A4 E7            [ 1] 1119 	and	a, #0xe7
      008897 C7 50 C6         [ 1] 1120 	ld	0x50c6, a
                           0003A4  1121 	Sstm8s_clk$CLK_HSIPrescalerConfig$340 ==.
                                   1122 ;	drivers/src/stm8s_clk.c: 424: CLK->CKDIVR |= (uint8_t)HSIPrescaler;
      00889A C6 50 C6         [ 1] 1123 	ld	a, 0x50c6
      00889D 1A 03            [ 1] 1124 	or	a, (0x03, sp)
      00889F C7 50 C6         [ 1] 1125 	ld	0x50c6, a
                           0003AC  1126 	Sstm8s_clk$CLK_HSIPrescalerConfig$341 ==.
                                   1127 ;	drivers/src/stm8s_clk.c: 425: }
                           0003AC  1128 	Sstm8s_clk$CLK_HSIPrescalerConfig$342 ==.
                           0003AC  1129 	XG$CLK_HSIPrescalerConfig$0$0 ==.
      0088A2 81               [ 4] 1130 	ret
                           0003AD  1131 	Sstm8s_clk$CLK_HSIPrescalerConfig$343 ==.
                           0003AD  1132 	Sstm8s_clk$CLK_CCOConfig$344 ==.
                                   1133 ;	drivers/src/stm8s_clk.c: 436: void CLK_CCOConfig(CLK_Output_TypeDef CLK_CCO)
                                   1134 ;	-----------------------------------------
                                   1135 ;	 function CLK_CCOConfig
                                   1136 ;	-----------------------------------------
      0088A3                       1137 _CLK_CCOConfig:
                           0003AD  1138 	Sstm8s_clk$CLK_CCOConfig$345 ==.
                           0003AD  1139 	Sstm8s_clk$CLK_CCOConfig$346 ==.
                                   1140 ;	drivers/src/stm8s_clk.c: 439: assert_param(IS_CLK_OUTPUT_OK(CLK_CCO));
      0088A3 0D 03            [ 1] 1141 	tnz	(0x03, sp)
      0088A5 26 03            [ 1] 1142 	jrne	00206$
      0088A7 CC 89 10         [ 2] 1143 	jp	00104$
      0088AA                       1144 00206$:
      0088AA 7B 03            [ 1] 1145 	ld	a, (0x03, sp)
      0088AC A1 04            [ 1] 1146 	cp	a, #0x04
      0088AE 26 03            [ 1] 1147 	jrne	00208$
      0088B0 CC 89 10         [ 2] 1148 	jp	00104$
      0088B3                       1149 00208$:
                           0003BD  1150 	Sstm8s_clk$CLK_CCOConfig$347 ==.
      0088B3 7B 03            [ 1] 1151 	ld	a, (0x03, sp)
      0088B5 A1 02            [ 1] 1152 	cp	a, #0x02
      0088B7 26 03            [ 1] 1153 	jrne	00211$
      0088B9 CC 89 10         [ 2] 1154 	jp	00104$
      0088BC                       1155 00211$:
                           0003C6  1156 	Sstm8s_clk$CLK_CCOConfig$348 ==.
      0088BC 7B 03            [ 1] 1157 	ld	a, (0x03, sp)
      0088BE A1 08            [ 1] 1158 	cp	a, #0x08
      0088C0 26 03            [ 1] 1159 	jrne	00214$
      0088C2 CC 89 10         [ 2] 1160 	jp	00104$
      0088C5                       1161 00214$:
                           0003CF  1162 	Sstm8s_clk$CLK_CCOConfig$349 ==.
      0088C5 7B 03            [ 1] 1163 	ld	a, (0x03, sp)
      0088C7 A1 0A            [ 1] 1164 	cp	a, #0x0a
      0088C9 26 03            [ 1] 1165 	jrne	00217$
      0088CB CC 89 10         [ 2] 1166 	jp	00104$
      0088CE                       1167 00217$:
                           0003D8  1168 	Sstm8s_clk$CLK_CCOConfig$350 ==.
      0088CE 7B 03            [ 1] 1169 	ld	a, (0x03, sp)
      0088D0 A1 0C            [ 1] 1170 	cp	a, #0x0c
      0088D2 26 03            [ 1] 1171 	jrne	00220$
      0088D4 CC 89 10         [ 2] 1172 	jp	00104$
      0088D7                       1173 00220$:
                           0003E1  1174 	Sstm8s_clk$CLK_CCOConfig$351 ==.
      0088D7 7B 03            [ 1] 1175 	ld	a, (0x03, sp)
      0088D9 A1 0E            [ 1] 1176 	cp	a, #0x0e
      0088DB 27 33            [ 1] 1177 	jreq	00104$
                           0003E7  1178 	Sstm8s_clk$CLK_CCOConfig$352 ==.
      0088DD 7B 03            [ 1] 1179 	ld	a, (0x03, sp)
      0088DF A1 10            [ 1] 1180 	cp	a, #0x10
      0088E1 27 2D            [ 1] 1181 	jreq	00104$
                           0003ED  1182 	Sstm8s_clk$CLK_CCOConfig$353 ==.
      0088E3 7B 03            [ 1] 1183 	ld	a, (0x03, sp)
      0088E5 A1 12            [ 1] 1184 	cp	a, #0x12
      0088E7 27 27            [ 1] 1185 	jreq	00104$
                           0003F3  1186 	Sstm8s_clk$CLK_CCOConfig$354 ==.
      0088E9 7B 03            [ 1] 1187 	ld	a, (0x03, sp)
      0088EB A1 14            [ 1] 1188 	cp	a, #0x14
      0088ED 27 21            [ 1] 1189 	jreq	00104$
                           0003F9  1190 	Sstm8s_clk$CLK_CCOConfig$355 ==.
      0088EF 7B 03            [ 1] 1191 	ld	a, (0x03, sp)
      0088F1 A1 16            [ 1] 1192 	cp	a, #0x16
      0088F3 27 1B            [ 1] 1193 	jreq	00104$
                           0003FF  1194 	Sstm8s_clk$CLK_CCOConfig$356 ==.
      0088F5 7B 03            [ 1] 1195 	ld	a, (0x03, sp)
      0088F7 A1 18            [ 1] 1196 	cp	a, #0x18
      0088F9 27 15            [ 1] 1197 	jreq	00104$
                           000405  1198 	Sstm8s_clk$CLK_CCOConfig$357 ==.
      0088FB 7B 03            [ 1] 1199 	ld	a, (0x03, sp)
      0088FD A1 1A            [ 1] 1200 	cp	a, #0x1a
      0088FF 27 0F            [ 1] 1201 	jreq	00104$
                           00040B  1202 	Sstm8s_clk$CLK_CCOConfig$358 ==.
      008901 4B B7            [ 1] 1203 	push	#0xb7
                           00040D  1204 	Sstm8s_clk$CLK_CCOConfig$359 ==.
      008903 4B 01            [ 1] 1205 	push	#0x01
                           00040F  1206 	Sstm8s_clk$CLK_CCOConfig$360 ==.
      008905 5F               [ 1] 1207 	clrw	x
      008906 89               [ 2] 1208 	pushw	x
                           000411  1209 	Sstm8s_clk$CLK_CCOConfig$361 ==.
      008907 4B E2            [ 1] 1210 	push	#<(___str_0+0)
                           000413  1211 	Sstm8s_clk$CLK_CCOConfig$362 ==.
      008909 4B 80            [ 1] 1212 	push	#((___str_0+0) >> 8)
                           000415  1213 	Sstm8s_clk$CLK_CCOConfig$363 ==.
      00890B CD 84 F3         [ 4] 1214 	call	_assert_failed
      00890E 5B 06            [ 2] 1215 	addw	sp, #6
                           00041A  1216 	Sstm8s_clk$CLK_CCOConfig$364 ==.
      008910                       1217 00104$:
                           00041A  1218 	Sstm8s_clk$CLK_CCOConfig$365 ==.
                                   1219 ;	drivers/src/stm8s_clk.c: 442: CLK->CCOR &= (uint8_t)(~CLK_CCOR_CCOSEL);
      008910 C6 50 C9         [ 1] 1220 	ld	a, 0x50c9
      008913 A4 E1            [ 1] 1221 	and	a, #0xe1
      008915 C7 50 C9         [ 1] 1222 	ld	0x50c9, a
                           000422  1223 	Sstm8s_clk$CLK_CCOConfig$366 ==.
                                   1224 ;	drivers/src/stm8s_clk.c: 445: CLK->CCOR |= (uint8_t)CLK_CCO;
      008918 C6 50 C9         [ 1] 1225 	ld	a, 0x50c9
      00891B 1A 03            [ 1] 1226 	or	a, (0x03, sp)
      00891D C7 50 C9         [ 1] 1227 	ld	0x50c9, a
                           00042A  1228 	Sstm8s_clk$CLK_CCOConfig$367 ==.
                                   1229 ;	drivers/src/stm8s_clk.c: 448: CLK->CCOR |= CLK_CCOR_CCOEN;
      008920 72 10 50 C9      [ 1] 1230 	bset	20681, #0
                           00042E  1231 	Sstm8s_clk$CLK_CCOConfig$368 ==.
                                   1232 ;	drivers/src/stm8s_clk.c: 449: }
                           00042E  1233 	Sstm8s_clk$CLK_CCOConfig$369 ==.
                           00042E  1234 	XG$CLK_CCOConfig$0$0 ==.
      008924 81               [ 4] 1235 	ret
                           00042F  1236 	Sstm8s_clk$CLK_CCOConfig$370 ==.
                           00042F  1237 	Sstm8s_clk$CLK_ITConfig$371 ==.
                                   1238 ;	drivers/src/stm8s_clk.c: 459: void CLK_ITConfig(CLK_IT_TypeDef CLK_IT, FunctionalState NewState)
                                   1239 ;	-----------------------------------------
                                   1240 ;	 function CLK_ITConfig
                                   1241 ;	-----------------------------------------
      008925                       1242 _CLK_ITConfig:
                           00042F  1243 	Sstm8s_clk$CLK_ITConfig$372 ==.
      008925 88               [ 1] 1244 	push	a
                           000430  1245 	Sstm8s_clk$CLK_ITConfig$373 ==.
                           000430  1246 	Sstm8s_clk$CLK_ITConfig$374 ==.
                                   1247 ;	drivers/src/stm8s_clk.c: 462: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
      008926 0D 05            [ 1] 1248 	tnz	(0x05, sp)
      008928 27 14            [ 1] 1249 	jreq	00115$
      00892A 7B 05            [ 1] 1250 	ld	a, (0x05, sp)
      00892C 4A               [ 1] 1251 	dec	a
      00892D 27 0F            [ 1] 1252 	jreq	00115$
                           000439  1253 	Sstm8s_clk$CLK_ITConfig$375 ==.
      00892F 4B CE            [ 1] 1254 	push	#0xce
                           00043B  1255 	Sstm8s_clk$CLK_ITConfig$376 ==.
      008931 4B 01            [ 1] 1256 	push	#0x01
                           00043D  1257 	Sstm8s_clk$CLK_ITConfig$377 ==.
      008933 5F               [ 1] 1258 	clrw	x
      008934 89               [ 2] 1259 	pushw	x
                           00043F  1260 	Sstm8s_clk$CLK_ITConfig$378 ==.
      008935 4B E2            [ 1] 1261 	push	#<(___str_0+0)
                           000441  1262 	Sstm8s_clk$CLK_ITConfig$379 ==.
      008937 4B 80            [ 1] 1263 	push	#((___str_0+0) >> 8)
                           000443  1264 	Sstm8s_clk$CLK_ITConfig$380 ==.
      008939 CD 84 F3         [ 4] 1265 	call	_assert_failed
      00893C 5B 06            [ 2] 1266 	addw	sp, #6
                           000448  1267 	Sstm8s_clk$CLK_ITConfig$381 ==.
      00893E                       1268 00115$:
                           000448  1269 	Sstm8s_clk$CLK_ITConfig$382 ==.
                                   1270 ;	drivers/src/stm8s_clk.c: 463: assert_param(IS_CLK_IT_OK(CLK_IT));
      00893E 7B 04            [ 1] 1271 	ld	a, (0x04, sp)
      008940 A0 0C            [ 1] 1272 	sub	a, #0x0c
      008942 26 02            [ 1] 1273 	jrne	00174$
      008944 4C               [ 1] 1274 	inc	a
      008945 21                    1275 	.byte 0x21
      008946                       1276 00174$:
      008946 4F               [ 1] 1277 	clr	a
      008947                       1278 00175$:
                           000451  1279 	Sstm8s_clk$CLK_ITConfig$383 ==.
      008947 88               [ 1] 1280 	push	a
                           000452  1281 	Sstm8s_clk$CLK_ITConfig$384 ==.
      008948 7B 05            [ 1] 1282 	ld	a, (0x05, sp)
      00894A A1 1C            [ 1] 1283 	cp	a, #0x1c
      00894C 84               [ 1] 1284 	pop	a
                           000457  1285 	Sstm8s_clk$CLK_ITConfig$385 ==.
      00894D 26 07            [ 1] 1286 	jrne	00177$
      00894F 88               [ 1] 1287 	push	a
                           00045A  1288 	Sstm8s_clk$CLK_ITConfig$386 ==.
      008950 A6 01            [ 1] 1289 	ld	a, #0x01
      008952 6B 02            [ 1] 1290 	ld	(0x02, sp), a
      008954 84               [ 1] 1291 	pop	a
                           00045F  1292 	Sstm8s_clk$CLK_ITConfig$387 ==.
      008955 C5                    1293 	.byte 0xc5
      008956                       1294 00177$:
      008956 0F 01            [ 1] 1295 	clr	(0x01, sp)
      008958                       1296 00178$:
                           000462  1297 	Sstm8s_clk$CLK_ITConfig$388 ==.
      008958 4D               [ 1] 1298 	tnz	a
      008959 26 15            [ 1] 1299 	jrne	00120$
      00895B 0D 01            [ 1] 1300 	tnz	(0x01, sp)
      00895D 26 11            [ 1] 1301 	jrne	00120$
      00895F 88               [ 1] 1302 	push	a
                           00046A  1303 	Sstm8s_clk$CLK_ITConfig$389 ==.
      008960 4B CF            [ 1] 1304 	push	#0xcf
                           00046C  1305 	Sstm8s_clk$CLK_ITConfig$390 ==.
      008962 4B 01            [ 1] 1306 	push	#0x01
                           00046E  1307 	Sstm8s_clk$CLK_ITConfig$391 ==.
      008964 5F               [ 1] 1308 	clrw	x
      008965 89               [ 2] 1309 	pushw	x
                           000470  1310 	Sstm8s_clk$CLK_ITConfig$392 ==.
      008966 4B E2            [ 1] 1311 	push	#<(___str_0+0)
                           000472  1312 	Sstm8s_clk$CLK_ITConfig$393 ==.
      008968 4B 80            [ 1] 1313 	push	#((___str_0+0) >> 8)
                           000474  1314 	Sstm8s_clk$CLK_ITConfig$394 ==.
      00896A CD 84 F3         [ 4] 1315 	call	_assert_failed
      00896D 5B 06            [ 2] 1316 	addw	sp, #6
                           000479  1317 	Sstm8s_clk$CLK_ITConfig$395 ==.
      00896F 84               [ 1] 1318 	pop	a
                           00047A  1319 	Sstm8s_clk$CLK_ITConfig$396 ==.
      008970                       1320 00120$:
                           00047A  1321 	Sstm8s_clk$CLK_ITConfig$397 ==.
                                   1322 ;	drivers/src/stm8s_clk.c: 465: if (NewState != DISABLE)
      008970 0D 05            [ 1] 1323 	tnz	(0x05, sp)
      008972 27 13            [ 1] 1324 	jreq	00110$
                           00047E  1325 	Sstm8s_clk$CLK_ITConfig$398 ==.
                           00047E  1326 	Sstm8s_clk$CLK_ITConfig$399 ==.
                                   1327 ;	drivers/src/stm8s_clk.c: 467: switch (CLK_IT)
      008974 4D               [ 1] 1328 	tnz	a
      008975 26 0A            [ 1] 1329 	jrne	00102$
      008977 7B 01            [ 1] 1330 	ld	a, (0x01, sp)
      008979 27 1D            [ 1] 1331 	jreq	00112$
                           000485  1332 	Sstm8s_clk$CLK_ITConfig$400 ==.
                           000485  1333 	Sstm8s_clk$CLK_ITConfig$401 ==.
                                   1334 ;	drivers/src/stm8s_clk.c: 470: CLK->SWCR |= CLK_SWCR_SWIEN;
      00897B 72 14 50 C5      [ 1] 1335 	bset	20677, #2
                           000489  1336 	Sstm8s_clk$CLK_ITConfig$402 ==.
                                   1337 ;	drivers/src/stm8s_clk.c: 471: break;
      00897F 20 17            [ 2] 1338 	jra	00112$
                           00048B  1339 	Sstm8s_clk$CLK_ITConfig$403 ==.
                                   1340 ;	drivers/src/stm8s_clk.c: 472: case CLK_IT_CSSD: /* Enable the clock security system detection interrupt */
      008981                       1341 00102$:
                           00048B  1342 	Sstm8s_clk$CLK_ITConfig$404 ==.
                                   1343 ;	drivers/src/stm8s_clk.c: 473: CLK->CSSR |= CLK_CSSR_CSSDIE;
      008981 72 14 50 C8      [ 1] 1344 	bset	20680, #2
                           00048F  1345 	Sstm8s_clk$CLK_ITConfig$405 ==.
                                   1346 ;	drivers/src/stm8s_clk.c: 474: break;
      008985 20 11            [ 2] 1347 	jra	00112$
                           000491  1348 	Sstm8s_clk$CLK_ITConfig$406 ==.
                           000491  1349 	Sstm8s_clk$CLK_ITConfig$407 ==.
                                   1350 ;	drivers/src/stm8s_clk.c: 477: }
      008987                       1351 00110$:
                           000491  1352 	Sstm8s_clk$CLK_ITConfig$408 ==.
                           000491  1353 	Sstm8s_clk$CLK_ITConfig$409 ==.
                                   1354 ;	drivers/src/stm8s_clk.c: 481: switch (CLK_IT)
      008987 4D               [ 1] 1355 	tnz	a
      008988 26 0A            [ 1] 1356 	jrne	00106$
      00898A 7B 01            [ 1] 1357 	ld	a, (0x01, sp)
      00898C 27 0A            [ 1] 1358 	jreq	00112$
                           000498  1359 	Sstm8s_clk$CLK_ITConfig$410 ==.
                           000498  1360 	Sstm8s_clk$CLK_ITConfig$411 ==.
                                   1361 ;	drivers/src/stm8s_clk.c: 484: CLK->SWCR  &= (uint8_t)(~CLK_SWCR_SWIEN);
      00898E 72 15 50 C5      [ 1] 1362 	bres	20677, #2
                           00049C  1363 	Sstm8s_clk$CLK_ITConfig$412 ==.
                                   1364 ;	drivers/src/stm8s_clk.c: 485: break;
      008992 20 04            [ 2] 1365 	jra	00112$
                           00049E  1366 	Sstm8s_clk$CLK_ITConfig$413 ==.
                                   1367 ;	drivers/src/stm8s_clk.c: 486: case CLK_IT_CSSD: /* Disable the clock security system detection interrupt */
      008994                       1368 00106$:
                           00049E  1369 	Sstm8s_clk$CLK_ITConfig$414 ==.
                                   1370 ;	drivers/src/stm8s_clk.c: 487: CLK->CSSR &= (uint8_t)(~CLK_CSSR_CSSDIE);
      008994 72 15 50 C8      [ 1] 1371 	bres	20680, #2
                           0004A2  1372 	Sstm8s_clk$CLK_ITConfig$415 ==.
                           0004A2  1373 	Sstm8s_clk$CLK_ITConfig$416 ==.
                                   1374 ;	drivers/src/stm8s_clk.c: 491: }
      008998                       1375 00112$:
                           0004A2  1376 	Sstm8s_clk$CLK_ITConfig$417 ==.
                                   1377 ;	drivers/src/stm8s_clk.c: 493: }
      008998 84               [ 1] 1378 	pop	a
                           0004A3  1379 	Sstm8s_clk$CLK_ITConfig$418 ==.
                           0004A3  1380 	Sstm8s_clk$CLK_ITConfig$419 ==.
                           0004A3  1381 	XG$CLK_ITConfig$0$0 ==.
      008999 81               [ 4] 1382 	ret
                           0004A4  1383 	Sstm8s_clk$CLK_ITConfig$420 ==.
                           0004A4  1384 	Sstm8s_clk$CLK_SYSCLKConfig$421 ==.
                                   1385 ;	drivers/src/stm8s_clk.c: 500: void CLK_SYSCLKConfig(CLK_Prescaler_TypeDef CLK_Prescaler)
                                   1386 ;	-----------------------------------------
                                   1387 ;	 function CLK_SYSCLKConfig
                                   1388 ;	-----------------------------------------
      00899A                       1389 _CLK_SYSCLKConfig:
                           0004A4  1390 	Sstm8s_clk$CLK_SYSCLKConfig$422 ==.
      00899A 88               [ 1] 1391 	push	a
                           0004A5  1392 	Sstm8s_clk$CLK_SYSCLKConfig$423 ==.
                           0004A5  1393 	Sstm8s_clk$CLK_SYSCLKConfig$424 ==.
                                   1394 ;	drivers/src/stm8s_clk.c: 503: assert_param(IS_CLK_PRESCALER_OK(CLK_Prescaler));
      00899B 0D 04            [ 1] 1395 	tnz	(0x04, sp)
      00899D 26 03            [ 1] 1396 	jrne	00206$
      00899F CC 89 FF         [ 2] 1397 	jp	00107$
      0089A2                       1398 00206$:
      0089A2 7B 04            [ 1] 1399 	ld	a, (0x04, sp)
      0089A4 A1 08            [ 1] 1400 	cp	a, #0x08
      0089A6 26 03            [ 1] 1401 	jrne	00208$
      0089A8 CC 89 FF         [ 2] 1402 	jp	00107$
      0089AB                       1403 00208$:
                           0004B5  1404 	Sstm8s_clk$CLK_SYSCLKConfig$425 ==.
      0089AB 7B 04            [ 1] 1405 	ld	a, (0x04, sp)
      0089AD A1 10            [ 1] 1406 	cp	a, #0x10
      0089AF 26 03            [ 1] 1407 	jrne	00211$
      0089B1 CC 89 FF         [ 2] 1408 	jp	00107$
      0089B4                       1409 00211$:
                           0004BE  1410 	Sstm8s_clk$CLK_SYSCLKConfig$426 ==.
      0089B4 7B 04            [ 1] 1411 	ld	a, (0x04, sp)
      0089B6 A1 18            [ 1] 1412 	cp	a, #0x18
      0089B8 26 03            [ 1] 1413 	jrne	00214$
      0089BA CC 89 FF         [ 2] 1414 	jp	00107$
      0089BD                       1415 00214$:
                           0004C7  1416 	Sstm8s_clk$CLK_SYSCLKConfig$427 ==.
      0089BD 7B 04            [ 1] 1417 	ld	a, (0x04, sp)
      0089BF A1 80            [ 1] 1418 	cp	a, #0x80
      0089C1 26 03            [ 1] 1419 	jrne	00217$
      0089C3 CC 89 FF         [ 2] 1420 	jp	00107$
      0089C6                       1421 00217$:
                           0004D0  1422 	Sstm8s_clk$CLK_SYSCLKConfig$428 ==.
      0089C6 7B 04            [ 1] 1423 	ld	a, (0x04, sp)
      0089C8 A1 81            [ 1] 1424 	cp	a, #0x81
      0089CA 27 33            [ 1] 1425 	jreq	00107$
                           0004D6  1426 	Sstm8s_clk$CLK_SYSCLKConfig$429 ==.
      0089CC 7B 04            [ 1] 1427 	ld	a, (0x04, sp)
      0089CE A1 82            [ 1] 1428 	cp	a, #0x82
      0089D0 27 2D            [ 1] 1429 	jreq	00107$
                           0004DC  1430 	Sstm8s_clk$CLK_SYSCLKConfig$430 ==.
      0089D2 7B 04            [ 1] 1431 	ld	a, (0x04, sp)
      0089D4 A1 83            [ 1] 1432 	cp	a, #0x83
      0089D6 27 27            [ 1] 1433 	jreq	00107$
                           0004E2  1434 	Sstm8s_clk$CLK_SYSCLKConfig$431 ==.
      0089D8 7B 04            [ 1] 1435 	ld	a, (0x04, sp)
      0089DA A1 84            [ 1] 1436 	cp	a, #0x84
      0089DC 27 21            [ 1] 1437 	jreq	00107$
                           0004E8  1438 	Sstm8s_clk$CLK_SYSCLKConfig$432 ==.
      0089DE 7B 04            [ 1] 1439 	ld	a, (0x04, sp)
      0089E0 A1 85            [ 1] 1440 	cp	a, #0x85
      0089E2 27 1B            [ 1] 1441 	jreq	00107$
                           0004EE  1442 	Sstm8s_clk$CLK_SYSCLKConfig$433 ==.
      0089E4 7B 04            [ 1] 1443 	ld	a, (0x04, sp)
      0089E6 A1 86            [ 1] 1444 	cp	a, #0x86
      0089E8 27 15            [ 1] 1445 	jreq	00107$
                           0004F4  1446 	Sstm8s_clk$CLK_SYSCLKConfig$434 ==.
      0089EA 7B 04            [ 1] 1447 	ld	a, (0x04, sp)
      0089EC A1 87            [ 1] 1448 	cp	a, #0x87
      0089EE 27 0F            [ 1] 1449 	jreq	00107$
                           0004FA  1450 	Sstm8s_clk$CLK_SYSCLKConfig$435 ==.
      0089F0 4B F7            [ 1] 1451 	push	#0xf7
                           0004FC  1452 	Sstm8s_clk$CLK_SYSCLKConfig$436 ==.
      0089F2 4B 01            [ 1] 1453 	push	#0x01
                           0004FE  1454 	Sstm8s_clk$CLK_SYSCLKConfig$437 ==.
      0089F4 5F               [ 1] 1455 	clrw	x
      0089F5 89               [ 2] 1456 	pushw	x
                           000500  1457 	Sstm8s_clk$CLK_SYSCLKConfig$438 ==.
      0089F6 4B E2            [ 1] 1458 	push	#<(___str_0+0)
                           000502  1459 	Sstm8s_clk$CLK_SYSCLKConfig$439 ==.
      0089F8 4B 80            [ 1] 1460 	push	#((___str_0+0) >> 8)
                           000504  1461 	Sstm8s_clk$CLK_SYSCLKConfig$440 ==.
      0089FA CD 84 F3         [ 4] 1462 	call	_assert_failed
      0089FD 5B 06            [ 2] 1463 	addw	sp, #6
                           000509  1464 	Sstm8s_clk$CLK_SYSCLKConfig$441 ==.
      0089FF                       1465 00107$:
                           000509  1466 	Sstm8s_clk$CLK_SYSCLKConfig$442 ==.
                                   1467 ;	drivers/src/stm8s_clk.c: 507: CLK->CKDIVR &= (uint8_t)(~CLK_CKDIVR_HSIDIV);
      0089FF C6 50 C6         [ 1] 1468 	ld	a, 0x50c6
                           00050C  1469 	Sstm8s_clk$CLK_SYSCLKConfig$443 ==.
                                   1470 ;	drivers/src/stm8s_clk.c: 505: if (((uint8_t)CLK_Prescaler & (uint8_t)0x80) == 0x00) /* Bit7 = 0 means HSI divider */
      008A02 0D 04            [ 1] 1471 	tnz	(0x04, sp)
      008A04 2B 15            [ 1] 1472 	jrmi	00102$
                           000510  1473 	Sstm8s_clk$CLK_SYSCLKConfig$444 ==.
                           000510  1474 	Sstm8s_clk$CLK_SYSCLKConfig$445 ==.
                                   1475 ;	drivers/src/stm8s_clk.c: 507: CLK->CKDIVR &= (uint8_t)(~CLK_CKDIVR_HSIDIV);
      008A06 A4 E7            [ 1] 1476 	and	a, #0xe7
      008A08 C7 50 C6         [ 1] 1477 	ld	0x50c6, a
                           000515  1478 	Sstm8s_clk$CLK_SYSCLKConfig$446 ==.
                                   1479 ;	drivers/src/stm8s_clk.c: 508: CLK->CKDIVR |= (uint8_t)((uint8_t)CLK_Prescaler & (uint8_t)CLK_CKDIVR_HSIDIV);
      008A0B C6 50 C6         [ 1] 1480 	ld	a, 0x50c6
      008A0E 6B 01            [ 1] 1481 	ld	(0x01, sp), a
      008A10 7B 04            [ 1] 1482 	ld	a, (0x04, sp)
      008A12 A4 18            [ 1] 1483 	and	a, #0x18
      008A14 1A 01            [ 1] 1484 	or	a, (0x01, sp)
      008A16 C7 50 C6         [ 1] 1485 	ld	0x50c6, a
                           000523  1486 	Sstm8s_clk$CLK_SYSCLKConfig$447 ==.
      008A19 20 13            [ 2] 1487 	jra	00104$
      008A1B                       1488 00102$:
                           000525  1489 	Sstm8s_clk$CLK_SYSCLKConfig$448 ==.
                           000525  1490 	Sstm8s_clk$CLK_SYSCLKConfig$449 ==.
                                   1491 ;	drivers/src/stm8s_clk.c: 512: CLK->CKDIVR &= (uint8_t)(~CLK_CKDIVR_CPUDIV);
      008A1B A4 F8            [ 1] 1492 	and	a, #0xf8
      008A1D C7 50 C6         [ 1] 1493 	ld	0x50c6, a
                           00052A  1494 	Sstm8s_clk$CLK_SYSCLKConfig$450 ==.
                                   1495 ;	drivers/src/stm8s_clk.c: 513: CLK->CKDIVR |= (uint8_t)((uint8_t)CLK_Prescaler & (uint8_t)CLK_CKDIVR_CPUDIV);
      008A20 C6 50 C6         [ 1] 1496 	ld	a, 0x50c6
      008A23 6B 01            [ 1] 1497 	ld	(0x01, sp), a
      008A25 7B 04            [ 1] 1498 	ld	a, (0x04, sp)
      008A27 A4 07            [ 1] 1499 	and	a, #0x07
      008A29 1A 01            [ 1] 1500 	or	a, (0x01, sp)
      008A2B C7 50 C6         [ 1] 1501 	ld	0x50c6, a
                           000538  1502 	Sstm8s_clk$CLK_SYSCLKConfig$451 ==.
      008A2E                       1503 00104$:
                           000538  1504 	Sstm8s_clk$CLK_SYSCLKConfig$452 ==.
                                   1505 ;	drivers/src/stm8s_clk.c: 515: }
      008A2E 84               [ 1] 1506 	pop	a
                           000539  1507 	Sstm8s_clk$CLK_SYSCLKConfig$453 ==.
                           000539  1508 	Sstm8s_clk$CLK_SYSCLKConfig$454 ==.
                           000539  1509 	XG$CLK_SYSCLKConfig$0$0 ==.
      008A2F 81               [ 4] 1510 	ret
                           00053A  1511 	Sstm8s_clk$CLK_SYSCLKConfig$455 ==.
                           00053A  1512 	Sstm8s_clk$CLK_SWIMConfig$456 ==.
                                   1513 ;	drivers/src/stm8s_clk.c: 523: void CLK_SWIMConfig(CLK_SWIMDivider_TypeDef CLK_SWIMDivider)
                                   1514 ;	-----------------------------------------
                                   1515 ;	 function CLK_SWIMConfig
                                   1516 ;	-----------------------------------------
      008A30                       1517 _CLK_SWIMConfig:
                           00053A  1518 	Sstm8s_clk$CLK_SWIMConfig$457 ==.
                           00053A  1519 	Sstm8s_clk$CLK_SWIMConfig$458 ==.
                                   1520 ;	drivers/src/stm8s_clk.c: 526: assert_param(IS_CLK_SWIMDIVIDER_OK(CLK_SWIMDivider));
      008A30 0D 03            [ 1] 1521 	tnz	(0x03, sp)
      008A32 27 14            [ 1] 1522 	jreq	00107$
      008A34 7B 03            [ 1] 1523 	ld	a, (0x03, sp)
      008A36 4A               [ 1] 1524 	dec	a
      008A37 27 0F            [ 1] 1525 	jreq	00107$
                           000543  1526 	Sstm8s_clk$CLK_SWIMConfig$459 ==.
      008A39 4B 0E            [ 1] 1527 	push	#0x0e
                           000545  1528 	Sstm8s_clk$CLK_SWIMConfig$460 ==.
      008A3B 4B 02            [ 1] 1529 	push	#0x02
                           000547  1530 	Sstm8s_clk$CLK_SWIMConfig$461 ==.
      008A3D 5F               [ 1] 1531 	clrw	x
      008A3E 89               [ 2] 1532 	pushw	x
                           000549  1533 	Sstm8s_clk$CLK_SWIMConfig$462 ==.
      008A3F 4B E2            [ 1] 1534 	push	#<(___str_0+0)
                           00054B  1535 	Sstm8s_clk$CLK_SWIMConfig$463 ==.
      008A41 4B 80            [ 1] 1536 	push	#((___str_0+0) >> 8)
                           00054D  1537 	Sstm8s_clk$CLK_SWIMConfig$464 ==.
      008A43 CD 84 F3         [ 4] 1538 	call	_assert_failed
      008A46 5B 06            [ 2] 1539 	addw	sp, #6
                           000552  1540 	Sstm8s_clk$CLK_SWIMConfig$465 ==.
      008A48                       1541 00107$:
                           000552  1542 	Sstm8s_clk$CLK_SWIMConfig$466 ==.
                                   1543 ;	drivers/src/stm8s_clk.c: 531: CLK->SWIMCCR |= CLK_SWIMCCR_SWIMDIV;
      008A48 C6 50 CD         [ 1] 1544 	ld	a, 0x50cd
                           000555  1545 	Sstm8s_clk$CLK_SWIMConfig$467 ==.
                                   1546 ;	drivers/src/stm8s_clk.c: 528: if (CLK_SWIMDivider != CLK_SWIMDIVIDER_2)
      008A4B 0D 03            [ 1] 1547 	tnz	(0x03, sp)
      008A4D 27 07            [ 1] 1548 	jreq	00102$
                           000559  1549 	Sstm8s_clk$CLK_SWIMConfig$468 ==.
                           000559  1550 	Sstm8s_clk$CLK_SWIMConfig$469 ==.
                                   1551 ;	drivers/src/stm8s_clk.c: 531: CLK->SWIMCCR |= CLK_SWIMCCR_SWIMDIV;
      008A4F AA 01            [ 1] 1552 	or	a, #0x01
      008A51 C7 50 CD         [ 1] 1553 	ld	0x50cd, a
                           00055E  1554 	Sstm8s_clk$CLK_SWIMConfig$470 ==.
      008A54 20 05            [ 2] 1555 	jra	00104$
      008A56                       1556 00102$:
                           000560  1557 	Sstm8s_clk$CLK_SWIMConfig$471 ==.
                           000560  1558 	Sstm8s_clk$CLK_SWIMConfig$472 ==.
                                   1559 ;	drivers/src/stm8s_clk.c: 536: CLK->SWIMCCR &= (uint8_t)(~CLK_SWIMCCR_SWIMDIV);
      008A56 A4 FE            [ 1] 1560 	and	a, #0xfe
      008A58 C7 50 CD         [ 1] 1561 	ld	0x50cd, a
                           000565  1562 	Sstm8s_clk$CLK_SWIMConfig$473 ==.
      008A5B                       1563 00104$:
                           000565  1564 	Sstm8s_clk$CLK_SWIMConfig$474 ==.
                                   1565 ;	drivers/src/stm8s_clk.c: 538: }
                           000565  1566 	Sstm8s_clk$CLK_SWIMConfig$475 ==.
                           000565  1567 	XG$CLK_SWIMConfig$0$0 ==.
      008A5B 81               [ 4] 1568 	ret
                           000566  1569 	Sstm8s_clk$CLK_SWIMConfig$476 ==.
                           000566  1570 	Sstm8s_clk$CLK_ClockSecuritySystemEnable$477 ==.
                                   1571 ;	drivers/src/stm8s_clk.c: 547: void CLK_ClockSecuritySystemEnable(void)
                                   1572 ;	-----------------------------------------
                                   1573 ;	 function CLK_ClockSecuritySystemEnable
                                   1574 ;	-----------------------------------------
      008A5C                       1575 _CLK_ClockSecuritySystemEnable:
                           000566  1576 	Sstm8s_clk$CLK_ClockSecuritySystemEnable$478 ==.
                           000566  1577 	Sstm8s_clk$CLK_ClockSecuritySystemEnable$479 ==.
                                   1578 ;	drivers/src/stm8s_clk.c: 550: CLK->CSSR |= CLK_CSSR_CSSEN;
      008A5C 72 10 50 C8      [ 1] 1579 	bset	20680, #0
                           00056A  1580 	Sstm8s_clk$CLK_ClockSecuritySystemEnable$480 ==.
                                   1581 ;	drivers/src/stm8s_clk.c: 551: }
                           00056A  1582 	Sstm8s_clk$CLK_ClockSecuritySystemEnable$481 ==.
                           00056A  1583 	XG$CLK_ClockSecuritySystemEnable$0$0 ==.
      008A60 81               [ 4] 1584 	ret
                           00056B  1585 	Sstm8s_clk$CLK_ClockSecuritySystemEnable$482 ==.
                           00056B  1586 	Sstm8s_clk$CLK_GetSYSCLKSource$483 ==.
                                   1587 ;	drivers/src/stm8s_clk.c: 559: CLK_Source_TypeDef CLK_GetSYSCLKSource(void)
                                   1588 ;	-----------------------------------------
                                   1589 ;	 function CLK_GetSYSCLKSource
                                   1590 ;	-----------------------------------------
      008A61                       1591 _CLK_GetSYSCLKSource:
                           00056B  1592 	Sstm8s_clk$CLK_GetSYSCLKSource$484 ==.
                           00056B  1593 	Sstm8s_clk$CLK_GetSYSCLKSource$485 ==.
                                   1594 ;	drivers/src/stm8s_clk.c: 561: return((CLK_Source_TypeDef)CLK->CMSR);
      008A61 C6 50 C3         [ 1] 1595 	ld	a, 0x50c3
                           00056E  1596 	Sstm8s_clk$CLK_GetSYSCLKSource$486 ==.
                                   1597 ;	drivers/src/stm8s_clk.c: 562: }
                           00056E  1598 	Sstm8s_clk$CLK_GetSYSCLKSource$487 ==.
                           00056E  1599 	XG$CLK_GetSYSCLKSource$0$0 ==.
      008A64 81               [ 4] 1600 	ret
                           00056F  1601 	Sstm8s_clk$CLK_GetSYSCLKSource$488 ==.
                           00056F  1602 	Sstm8s_clk$CLK_GetClockFreq$489 ==.
                                   1603 ;	drivers/src/stm8s_clk.c: 569: uint32_t CLK_GetClockFreq(void)
                                   1604 ;	-----------------------------------------
                                   1605 ;	 function CLK_GetClockFreq
                                   1606 ;	-----------------------------------------
      008A65                       1607 _CLK_GetClockFreq:
                           00056F  1608 	Sstm8s_clk$CLK_GetClockFreq$490 ==.
      008A65 52 04            [ 2] 1609 	sub	sp, #4
                           000571  1610 	Sstm8s_clk$CLK_GetClockFreq$491 ==.
                           000571  1611 	Sstm8s_clk$CLK_GetClockFreq$492 ==.
                                   1612 ;	drivers/src/stm8s_clk.c: 576: clocksource = (CLK_Source_TypeDef)CLK->CMSR;
      008A67 C6 50 C3         [ 1] 1613 	ld	a, 0x50c3
      008A6A 6B 04            [ 1] 1614 	ld	(0x04, sp), a
                           000576  1615 	Sstm8s_clk$CLK_GetClockFreq$493 ==.
                                   1616 ;	drivers/src/stm8s_clk.c: 578: if (clocksource == CLK_SOURCE_HSI)
      008A6C 7B 04            [ 1] 1617 	ld	a, (0x04, sp)
      008A6E A1 E1            [ 1] 1618 	cp	a, #0xe1
      008A70 26 26            [ 1] 1619 	jrne	00105$
                           00057C  1620 	Sstm8s_clk$CLK_GetClockFreq$494 ==.
                           00057C  1621 	Sstm8s_clk$CLK_GetClockFreq$495 ==.
                           00057C  1622 	Sstm8s_clk$CLK_GetClockFreq$496 ==.
                                   1623 ;	drivers/src/stm8s_clk.c: 580: tmp = (uint8_t)(CLK->CKDIVR & CLK_CKDIVR_HSIDIV);
      008A72 C6 50 C6         [ 1] 1624 	ld	a, 0x50c6
      008A75 A4 18            [ 1] 1625 	and	a, #0x18
                           000581  1626 	Sstm8s_clk$CLK_GetClockFreq$497 ==.
                                   1627 ;	drivers/src/stm8s_clk.c: 581: tmp = (uint8_t)(tmp >> 3);
      008A77 44               [ 1] 1628 	srl	a
      008A78 44               [ 1] 1629 	srl	a
      008A79 44               [ 1] 1630 	srl	a
                           000584  1631 	Sstm8s_clk$CLK_GetClockFreq$498 ==.
                                   1632 ;	drivers/src/stm8s_clk.c: 582: presc = HSIDivFactor[tmp];
      008A7A 5F               [ 1] 1633 	clrw	x
      008A7B 97               [ 1] 1634 	ld	xl, a
      008A7C D6 80 D6         [ 1] 1635 	ld	a, (_HSIDivFactor+0, x)
                           000589  1636 	Sstm8s_clk$CLK_GetClockFreq$499 ==.
                                   1637 ;	drivers/src/stm8s_clk.c: 583: clockfrequency = HSI_VALUE / presc;
      008A7F 5F               [ 1] 1638 	clrw	x
      008A80 97               [ 1] 1639 	ld	xl, a
      008A81 90 5F            [ 1] 1640 	clrw	y
                           00058D  1641 	Sstm8s_clk$CLK_GetClockFreq$500 ==.
      008A83 89               [ 2] 1642 	pushw	x
                           00058E  1643 	Sstm8s_clk$CLK_GetClockFreq$501 ==.
      008A84 90 89            [ 2] 1644 	pushw	y
                           000590  1645 	Sstm8s_clk$CLK_GetClockFreq$502 ==.
      008A86 4B 00            [ 1] 1646 	push	#0x00
                           000592  1647 	Sstm8s_clk$CLK_GetClockFreq$503 ==.
      008A88 4B 24            [ 1] 1648 	push	#0x24
                           000594  1649 	Sstm8s_clk$CLK_GetClockFreq$504 ==.
      008A8A 4B F4            [ 1] 1650 	push	#0xf4
                           000596  1651 	Sstm8s_clk$CLK_GetClockFreq$505 ==.
      008A8C 4B 00            [ 1] 1652 	push	#0x00
                           000598  1653 	Sstm8s_clk$CLK_GetClockFreq$506 ==.
      008A8E CD AB 55         [ 4] 1654 	call	__divulong
      008A91 5B 08            [ 2] 1655 	addw	sp, #8
                           00059D  1656 	Sstm8s_clk$CLK_GetClockFreq$507 ==.
      008A93 51               [ 1] 1657 	exgw	x, y
      008A94 17 03            [ 2] 1658 	ldw	(0x03, sp), y
      008A96 20 17            [ 2] 1659 	jra	00106$
      008A98                       1660 00105$:
                           0005A2  1661 	Sstm8s_clk$CLK_GetClockFreq$508 ==.
                                   1662 ;	drivers/src/stm8s_clk.c: 585: else if ( clocksource == CLK_SOURCE_LSI)
      008A98 7B 04            [ 1] 1663 	ld	a, (0x04, sp)
      008A9A A1 D2            [ 1] 1664 	cp	a, #0xd2
      008A9C 26 09            [ 1] 1665 	jrne	00102$
                           0005A8  1666 	Sstm8s_clk$CLK_GetClockFreq$509 ==.
                           0005A8  1667 	Sstm8s_clk$CLK_GetClockFreq$510 ==.
                           0005A8  1668 	Sstm8s_clk$CLK_GetClockFreq$511 ==.
                                   1669 ;	drivers/src/stm8s_clk.c: 587: clockfrequency = LSI_VALUE;
      008A9E AE F4 00         [ 2] 1670 	ldw	x, #0xf400
      008AA1 1F 03            [ 2] 1671 	ldw	(0x03, sp), x
      008AA3 5F               [ 1] 1672 	clrw	x
      008AA4 5C               [ 1] 1673 	incw	x
                           0005AF  1674 	Sstm8s_clk$CLK_GetClockFreq$512 ==.
      008AA5 20 08            [ 2] 1675 	jra	00106$
      008AA7                       1676 00102$:
                           0005B1  1677 	Sstm8s_clk$CLK_GetClockFreq$513 ==.
                           0005B1  1678 	Sstm8s_clk$CLK_GetClockFreq$514 ==.
                                   1679 ;	drivers/src/stm8s_clk.c: 591: clockfrequency = HSE_VALUE;
      008AA7 AE 36 00         [ 2] 1680 	ldw	x, #0x3600
      008AAA 1F 03            [ 2] 1681 	ldw	(0x03, sp), x
      008AAC AE 01 6E         [ 2] 1682 	ldw	x, #0x016e
                           0005B9  1683 	Sstm8s_clk$CLK_GetClockFreq$515 ==.
      008AAF                       1684 00106$:
                           0005B9  1685 	Sstm8s_clk$CLK_GetClockFreq$516 ==.
                                   1686 ;	drivers/src/stm8s_clk.c: 594: return((uint32_t)clockfrequency);
      008AAF 51               [ 1] 1687 	exgw	x, y
      008AB0 1E 03            [ 2] 1688 	ldw	x, (0x03, sp)
                           0005BC  1689 	Sstm8s_clk$CLK_GetClockFreq$517 ==.
                                   1690 ;	drivers/src/stm8s_clk.c: 595: }
      008AB2 5B 04            [ 2] 1691 	addw	sp, #4
                           0005BE  1692 	Sstm8s_clk$CLK_GetClockFreq$518 ==.
                           0005BE  1693 	Sstm8s_clk$CLK_GetClockFreq$519 ==.
                           0005BE  1694 	XG$CLK_GetClockFreq$0$0 ==.
      008AB4 81               [ 4] 1695 	ret
                           0005BF  1696 	Sstm8s_clk$CLK_GetClockFreq$520 ==.
                           0005BF  1697 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$521 ==.
                                   1698 ;	drivers/src/stm8s_clk.c: 604: void CLK_AdjustHSICalibrationValue(CLK_HSITrimValue_TypeDef CLK_HSICalibrationValue)
                                   1699 ;	-----------------------------------------
                                   1700 ;	 function CLK_AdjustHSICalibrationValue
                                   1701 ;	-----------------------------------------
      008AB5                       1702 _CLK_AdjustHSICalibrationValue:
                           0005BF  1703 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$522 ==.
                           0005BF  1704 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$523 ==.
                                   1705 ;	drivers/src/stm8s_clk.c: 607: assert_param(IS_CLK_HSITRIMVALUE_OK(CLK_HSICalibrationValue));
      008AB5 0D 03            [ 1] 1706 	tnz	(0x03, sp)
      008AB7 27 38            [ 1] 1707 	jreq	00104$
      008AB9 7B 03            [ 1] 1708 	ld	a, (0x03, sp)
      008ABB 4A               [ 1] 1709 	dec	a
      008ABC 27 33            [ 1] 1710 	jreq	00104$
                           0005C8  1711 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$524 ==.
      008ABE 7B 03            [ 1] 1712 	ld	a, (0x03, sp)
      008AC0 A1 02            [ 1] 1713 	cp	a, #0x02
      008AC2 27 2D            [ 1] 1714 	jreq	00104$
                           0005CE  1715 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$525 ==.
      008AC4 7B 03            [ 1] 1716 	ld	a, (0x03, sp)
      008AC6 A1 03            [ 1] 1717 	cp	a, #0x03
      008AC8 27 27            [ 1] 1718 	jreq	00104$
                           0005D4  1719 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$526 ==.
      008ACA 7B 03            [ 1] 1720 	ld	a, (0x03, sp)
      008ACC A1 04            [ 1] 1721 	cp	a, #0x04
      008ACE 27 21            [ 1] 1722 	jreq	00104$
                           0005DA  1723 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$527 ==.
      008AD0 7B 03            [ 1] 1724 	ld	a, (0x03, sp)
      008AD2 A1 05            [ 1] 1725 	cp	a, #0x05
      008AD4 27 1B            [ 1] 1726 	jreq	00104$
                           0005E0  1727 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$528 ==.
      008AD6 7B 03            [ 1] 1728 	ld	a, (0x03, sp)
      008AD8 A1 06            [ 1] 1729 	cp	a, #0x06
      008ADA 27 15            [ 1] 1730 	jreq	00104$
                           0005E6  1731 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$529 ==.
      008ADC 7B 03            [ 1] 1732 	ld	a, (0x03, sp)
      008ADE A1 07            [ 1] 1733 	cp	a, #0x07
      008AE0 27 0F            [ 1] 1734 	jreq	00104$
                           0005EC  1735 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$530 ==.
      008AE2 4B 5F            [ 1] 1736 	push	#0x5f
                           0005EE  1737 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$531 ==.
      008AE4 4B 02            [ 1] 1738 	push	#0x02
                           0005F0  1739 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$532 ==.
      008AE6 5F               [ 1] 1740 	clrw	x
      008AE7 89               [ 2] 1741 	pushw	x
                           0005F2  1742 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$533 ==.
      008AE8 4B E2            [ 1] 1743 	push	#<(___str_0+0)
                           0005F4  1744 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$534 ==.
      008AEA 4B 80            [ 1] 1745 	push	#((___str_0+0) >> 8)
                           0005F6  1746 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$535 ==.
      008AEC CD 84 F3         [ 4] 1747 	call	_assert_failed
      008AEF 5B 06            [ 2] 1748 	addw	sp, #6
                           0005FB  1749 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$536 ==.
      008AF1                       1750 00104$:
                           0005FB  1751 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$537 ==.
                                   1752 ;	drivers/src/stm8s_clk.c: 610: CLK->HSITRIMR = (uint8_t)( (uint8_t)(CLK->HSITRIMR & (uint8_t)(~CLK_HSITRIMR_HSITRIM))|((uint8_t)CLK_HSICalibrationValue));
      008AF1 C6 50 CC         [ 1] 1753 	ld	a, 0x50cc
      008AF4 A4 F8            [ 1] 1754 	and	a, #0xf8
      008AF6 1A 03            [ 1] 1755 	or	a, (0x03, sp)
      008AF8 C7 50 CC         [ 1] 1756 	ld	0x50cc, a
                           000605  1757 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$538 ==.
                                   1758 ;	drivers/src/stm8s_clk.c: 611: }
                           000605  1759 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$539 ==.
                           000605  1760 	XG$CLK_AdjustHSICalibrationValue$0$0 ==.
      008AFB 81               [ 4] 1761 	ret
                           000606  1762 	Sstm8s_clk$CLK_AdjustHSICalibrationValue$540 ==.
                           000606  1763 	Sstm8s_clk$CLK_SYSCLKEmergencyClear$541 ==.
                                   1764 ;	drivers/src/stm8s_clk.c: 622: void CLK_SYSCLKEmergencyClear(void)
                                   1765 ;	-----------------------------------------
                                   1766 ;	 function CLK_SYSCLKEmergencyClear
                                   1767 ;	-----------------------------------------
      008AFC                       1768 _CLK_SYSCLKEmergencyClear:
                           000606  1769 	Sstm8s_clk$CLK_SYSCLKEmergencyClear$542 ==.
                           000606  1770 	Sstm8s_clk$CLK_SYSCLKEmergencyClear$543 ==.
                                   1771 ;	drivers/src/stm8s_clk.c: 624: CLK->SWCR &= (uint8_t)(~CLK_SWCR_SWBSY);
      008AFC 72 11 50 C5      [ 1] 1772 	bres	20677, #0
                           00060A  1773 	Sstm8s_clk$CLK_SYSCLKEmergencyClear$544 ==.
                                   1774 ;	drivers/src/stm8s_clk.c: 625: }
                           00060A  1775 	Sstm8s_clk$CLK_SYSCLKEmergencyClear$545 ==.
                           00060A  1776 	XG$CLK_SYSCLKEmergencyClear$0$0 ==.
      008B00 81               [ 4] 1777 	ret
                           00060B  1778 	Sstm8s_clk$CLK_SYSCLKEmergencyClear$546 ==.
                           00060B  1779 	Sstm8s_clk$CLK_GetFlagStatus$547 ==.
                                   1780 ;	drivers/src/stm8s_clk.c: 634: FlagStatus CLK_GetFlagStatus(CLK_Flag_TypeDef CLK_FLAG)
                                   1781 ;	-----------------------------------------
                                   1782 ;	 function CLK_GetFlagStatus
                                   1783 ;	-----------------------------------------
      008B01                       1784 _CLK_GetFlagStatus:
                           00060B  1785 	Sstm8s_clk$CLK_GetFlagStatus$548 ==.
      008B01 88               [ 1] 1786 	push	a
                           00060C  1787 	Sstm8s_clk$CLK_GetFlagStatus$549 ==.
                           00060C  1788 	Sstm8s_clk$CLK_GetFlagStatus$550 ==.
                                   1789 ;	drivers/src/stm8s_clk.c: 641: assert_param(IS_CLK_FLAG_OK(CLK_FLAG));
      008B02 1E 04            [ 2] 1790 	ldw	x, (0x04, sp)
      008B04 A3 01 10         [ 2] 1791 	cpw	x, #0x0110
      008B07 26 03            [ 1] 1792 	jrne	00215$
      008B09 CC 8B 4D         [ 2] 1793 	jp	00119$
      008B0C                       1794 00215$:
                           000616  1795 	Sstm8s_clk$CLK_GetFlagStatus$551 ==.
      008B0C A3 01 02         [ 2] 1796 	cpw	x, #0x0102
      008B0F 26 03            [ 1] 1797 	jrne	00218$
      008B11 CC 8B 4D         [ 2] 1798 	jp	00119$
      008B14                       1799 00218$:
                           00061E  1800 	Sstm8s_clk$CLK_GetFlagStatus$552 ==.
      008B14 A3 02 02         [ 2] 1801 	cpw	x, #0x0202
      008B17 26 03            [ 1] 1802 	jrne	00221$
      008B19 CC 8B 4D         [ 2] 1803 	jp	00119$
      008B1C                       1804 00221$:
                           000626  1805 	Sstm8s_clk$CLK_GetFlagStatus$553 ==.
      008B1C A3 03 08         [ 2] 1806 	cpw	x, #0x0308
      008B1F 27 2C            [ 1] 1807 	jreq	00119$
                           00062B  1808 	Sstm8s_clk$CLK_GetFlagStatus$554 ==.
      008B21 A3 03 01         [ 2] 1809 	cpw	x, #0x0301
      008B24 27 27            [ 1] 1810 	jreq	00119$
                           000630  1811 	Sstm8s_clk$CLK_GetFlagStatus$555 ==.
      008B26 A3 04 08         [ 2] 1812 	cpw	x, #0x0408
      008B29 27 22            [ 1] 1813 	jreq	00119$
                           000635  1814 	Sstm8s_clk$CLK_GetFlagStatus$556 ==.
      008B2B A3 04 02         [ 2] 1815 	cpw	x, #0x0402
      008B2E 27 1D            [ 1] 1816 	jreq	00119$
                           00063A  1817 	Sstm8s_clk$CLK_GetFlagStatus$557 ==.
      008B30 A3 05 04         [ 2] 1818 	cpw	x, #0x0504
      008B33 27 18            [ 1] 1819 	jreq	00119$
                           00063F  1820 	Sstm8s_clk$CLK_GetFlagStatus$558 ==.
      008B35 A3 05 02         [ 2] 1821 	cpw	x, #0x0502
      008B38 27 13            [ 1] 1822 	jreq	00119$
                           000644  1823 	Sstm8s_clk$CLK_GetFlagStatus$559 ==.
      008B3A 89               [ 2] 1824 	pushw	x
                           000645  1825 	Sstm8s_clk$CLK_GetFlagStatus$560 ==.
      008B3B 4B 81            [ 1] 1826 	push	#0x81
                           000647  1827 	Sstm8s_clk$CLK_GetFlagStatus$561 ==.
      008B3D 4B 02            [ 1] 1828 	push	#0x02
                           000649  1829 	Sstm8s_clk$CLK_GetFlagStatus$562 ==.
      008B3F 4B 00            [ 1] 1830 	push	#0x00
                           00064B  1831 	Sstm8s_clk$CLK_GetFlagStatus$563 ==.
      008B41 4B 00            [ 1] 1832 	push	#0x00
                           00064D  1833 	Sstm8s_clk$CLK_GetFlagStatus$564 ==.
      008B43 4B E2            [ 1] 1834 	push	#<(___str_0+0)
                           00064F  1835 	Sstm8s_clk$CLK_GetFlagStatus$565 ==.
      008B45 4B 80            [ 1] 1836 	push	#((___str_0+0) >> 8)
                           000651  1837 	Sstm8s_clk$CLK_GetFlagStatus$566 ==.
      008B47 CD 84 F3         [ 4] 1838 	call	_assert_failed
      008B4A 5B 06            [ 2] 1839 	addw	sp, #6
                           000656  1840 	Sstm8s_clk$CLK_GetFlagStatus$567 ==.
      008B4C 85               [ 2] 1841 	popw	x
                           000657  1842 	Sstm8s_clk$CLK_GetFlagStatus$568 ==.
      008B4D                       1843 00119$:
                           000657  1844 	Sstm8s_clk$CLK_GetFlagStatus$569 ==.
                                   1845 ;	drivers/src/stm8s_clk.c: 644: statusreg = (uint16_t)((uint16_t)CLK_FLAG & (uint16_t)0xFF00);
      008B4D 4F               [ 1] 1846 	clr	a
                           000658  1847 	Sstm8s_clk$CLK_GetFlagStatus$570 ==.
                                   1848 ;	drivers/src/stm8s_clk.c: 647: if (statusreg == 0x0100) /* The flag to check is in ICKRregister */
      008B4E 97               [ 1] 1849 	ld	xl, a
      008B4F A3 01 00         [ 2] 1850 	cpw	x, #0x0100
      008B52 26 05            [ 1] 1851 	jrne	00111$
                           00065E  1852 	Sstm8s_clk$CLK_GetFlagStatus$571 ==.
                           00065E  1853 	Sstm8s_clk$CLK_GetFlagStatus$572 ==.
                           00065E  1854 	Sstm8s_clk$CLK_GetFlagStatus$573 ==.
                                   1855 ;	drivers/src/stm8s_clk.c: 649: tmpreg = CLK->ICKR;
      008B54 C6 50 C0         [ 1] 1856 	ld	a, 0x50c0
                           000661  1857 	Sstm8s_clk$CLK_GetFlagStatus$574 ==.
      008B57 20 21            [ 2] 1858 	jra	00112$
      008B59                       1859 00111$:
                           000663  1860 	Sstm8s_clk$CLK_GetFlagStatus$575 ==.
                                   1861 ;	drivers/src/stm8s_clk.c: 651: else if (statusreg == 0x0200) /* The flag to check is in ECKRregister */
      008B59 A3 02 00         [ 2] 1862 	cpw	x, #0x0200
      008B5C 26 05            [ 1] 1863 	jrne	00108$
                           000668  1864 	Sstm8s_clk$CLK_GetFlagStatus$576 ==.
                           000668  1865 	Sstm8s_clk$CLK_GetFlagStatus$577 ==.
                           000668  1866 	Sstm8s_clk$CLK_GetFlagStatus$578 ==.
                                   1867 ;	drivers/src/stm8s_clk.c: 653: tmpreg = CLK->ECKR;
      008B5E C6 50 C1         [ 1] 1868 	ld	a, 0x50c1
                           00066B  1869 	Sstm8s_clk$CLK_GetFlagStatus$579 ==.
      008B61 20 17            [ 2] 1870 	jra	00112$
      008B63                       1871 00108$:
                           00066D  1872 	Sstm8s_clk$CLK_GetFlagStatus$580 ==.
                                   1873 ;	drivers/src/stm8s_clk.c: 655: else if (statusreg == 0x0300) /* The flag to check is in SWIC register */
      008B63 A3 03 00         [ 2] 1874 	cpw	x, #0x0300
      008B66 26 05            [ 1] 1875 	jrne	00105$
                           000672  1876 	Sstm8s_clk$CLK_GetFlagStatus$581 ==.
                           000672  1877 	Sstm8s_clk$CLK_GetFlagStatus$582 ==.
                           000672  1878 	Sstm8s_clk$CLK_GetFlagStatus$583 ==.
                                   1879 ;	drivers/src/stm8s_clk.c: 657: tmpreg = CLK->SWCR;
      008B68 C6 50 C5         [ 1] 1880 	ld	a, 0x50c5
                           000675  1881 	Sstm8s_clk$CLK_GetFlagStatus$584 ==.
      008B6B 20 0D            [ 2] 1882 	jra	00112$
      008B6D                       1883 00105$:
                           000677  1884 	Sstm8s_clk$CLK_GetFlagStatus$585 ==.
                                   1885 ;	drivers/src/stm8s_clk.c: 659: else if (statusreg == 0x0400) /* The flag to check is in CSS register */
      008B6D A3 04 00         [ 2] 1886 	cpw	x, #0x0400
      008B70 26 05            [ 1] 1887 	jrne	00102$
                           00067C  1888 	Sstm8s_clk$CLK_GetFlagStatus$586 ==.
                           00067C  1889 	Sstm8s_clk$CLK_GetFlagStatus$587 ==.
                           00067C  1890 	Sstm8s_clk$CLK_GetFlagStatus$588 ==.
                                   1891 ;	drivers/src/stm8s_clk.c: 661: tmpreg = CLK->CSSR;
      008B72 C6 50 C8         [ 1] 1892 	ld	a, 0x50c8
                           00067F  1893 	Sstm8s_clk$CLK_GetFlagStatus$589 ==.
      008B75 20 03            [ 2] 1894 	jra	00112$
      008B77                       1895 00102$:
                           000681  1896 	Sstm8s_clk$CLK_GetFlagStatus$590 ==.
                           000681  1897 	Sstm8s_clk$CLK_GetFlagStatus$591 ==.
                                   1898 ;	drivers/src/stm8s_clk.c: 665: tmpreg = CLK->CCOR;
      008B77 C6 50 C9         [ 1] 1899 	ld	a, 0x50c9
                           000684  1900 	Sstm8s_clk$CLK_GetFlagStatus$592 ==.
      008B7A                       1901 00112$:
                           000684  1902 	Sstm8s_clk$CLK_GetFlagStatus$593 ==.
                                   1903 ;	drivers/src/stm8s_clk.c: 668: if ((tmpreg & (uint8_t)CLK_FLAG) != (uint8_t)RESET)
      008B7A 88               [ 1] 1904 	push	a
                           000685  1905 	Sstm8s_clk$CLK_GetFlagStatus$594 ==.
      008B7B 7B 06            [ 1] 1906 	ld	a, (0x06, sp)
      008B7D 6B 02            [ 1] 1907 	ld	(0x02, sp), a
      008B7F 84               [ 1] 1908 	pop	a
                           00068A  1909 	Sstm8s_clk$CLK_GetFlagStatus$595 ==.
      008B80 14 01            [ 1] 1910 	and	a, (0x01, sp)
      008B82 27 04            [ 1] 1911 	jreq	00114$
                           00068E  1912 	Sstm8s_clk$CLK_GetFlagStatus$596 ==.
                           00068E  1913 	Sstm8s_clk$CLK_GetFlagStatus$597 ==.
                                   1914 ;	drivers/src/stm8s_clk.c: 670: bitstatus = SET;
      008B84 A6 01            [ 1] 1915 	ld	a, #0x01
                           000690  1916 	Sstm8s_clk$CLK_GetFlagStatus$598 ==.
      008B86 20 01            [ 2] 1917 	jra	00115$
      008B88                       1918 00114$:
                           000692  1919 	Sstm8s_clk$CLK_GetFlagStatus$599 ==.
                           000692  1920 	Sstm8s_clk$CLK_GetFlagStatus$600 ==.
                                   1921 ;	drivers/src/stm8s_clk.c: 674: bitstatus = RESET;
      008B88 4F               [ 1] 1922 	clr	a
                           000693  1923 	Sstm8s_clk$CLK_GetFlagStatus$601 ==.
      008B89                       1924 00115$:
                           000693  1925 	Sstm8s_clk$CLK_GetFlagStatus$602 ==.
                                   1926 ;	drivers/src/stm8s_clk.c: 678: return((FlagStatus)bitstatus);
                           000693  1927 	Sstm8s_clk$CLK_GetFlagStatus$603 ==.
                                   1928 ;	drivers/src/stm8s_clk.c: 679: }
      008B89 5B 01            [ 2] 1929 	addw	sp, #1
                           000695  1930 	Sstm8s_clk$CLK_GetFlagStatus$604 ==.
                           000695  1931 	Sstm8s_clk$CLK_GetFlagStatus$605 ==.
                           000695  1932 	XG$CLK_GetFlagStatus$0$0 ==.
      008B8B 81               [ 4] 1933 	ret
                           000696  1934 	Sstm8s_clk$CLK_GetFlagStatus$606 ==.
                           000696  1935 	Sstm8s_clk$CLK_GetITStatus$607 ==.
                                   1936 ;	drivers/src/stm8s_clk.c: 687: ITStatus CLK_GetITStatus(CLK_IT_TypeDef CLK_IT)
                                   1937 ;	-----------------------------------------
                                   1938 ;	 function CLK_GetITStatus
                                   1939 ;	-----------------------------------------
      008B8C                       1940 _CLK_GetITStatus:
                           000696  1941 	Sstm8s_clk$CLK_GetITStatus$608 ==.
                           000696  1942 	Sstm8s_clk$CLK_GetITStatus$609 ==.
                                   1943 ;	drivers/src/stm8s_clk.c: 692: assert_param(IS_CLK_IT_OK(CLK_IT));
      008B8C 7B 03            [ 1] 1944 	ld	a, (0x03, sp)
      008B8E A0 1C            [ 1] 1945 	sub	a, #0x1c
      008B90 26 02            [ 1] 1946 	jrne	00143$
      008B92 4C               [ 1] 1947 	inc	a
      008B93 21                    1948 	.byte 0x21
      008B94                       1949 00143$:
      008B94 4F               [ 1] 1950 	clr	a
      008B95                       1951 00144$:
                           00069F  1952 	Sstm8s_clk$CLK_GetITStatus$610 ==.
      008B95 88               [ 1] 1953 	push	a
                           0006A0  1954 	Sstm8s_clk$CLK_GetITStatus$611 ==.
      008B96 7B 04            [ 1] 1955 	ld	a, (0x04, sp)
      008B98 A1 0C            [ 1] 1956 	cp	a, #0x0c
      008B9A 84               [ 1] 1957 	pop	a
                           0006A5  1958 	Sstm8s_clk$CLK_GetITStatus$612 ==.
      008B9B 27 14            [ 1] 1959 	jreq	00113$
                           0006A7  1960 	Sstm8s_clk$CLK_GetITStatus$613 ==.
      008B9D 4D               [ 1] 1961 	tnz	a
      008B9E 26 11            [ 1] 1962 	jrne	00113$
      008BA0 88               [ 1] 1963 	push	a
                           0006AB  1964 	Sstm8s_clk$CLK_GetITStatus$614 ==.
      008BA1 4B B4            [ 1] 1965 	push	#0xb4
                           0006AD  1966 	Sstm8s_clk$CLK_GetITStatus$615 ==.
      008BA3 4B 02            [ 1] 1967 	push	#0x02
                           0006AF  1968 	Sstm8s_clk$CLK_GetITStatus$616 ==.
      008BA5 5F               [ 1] 1969 	clrw	x
      008BA6 89               [ 2] 1970 	pushw	x
                           0006B1  1971 	Sstm8s_clk$CLK_GetITStatus$617 ==.
      008BA7 4B E2            [ 1] 1972 	push	#<(___str_0+0)
                           0006B3  1973 	Sstm8s_clk$CLK_GetITStatus$618 ==.
      008BA9 4B 80            [ 1] 1974 	push	#((___str_0+0) >> 8)
                           0006B5  1975 	Sstm8s_clk$CLK_GetITStatus$619 ==.
      008BAB CD 84 F3         [ 4] 1976 	call	_assert_failed
      008BAE 5B 06            [ 2] 1977 	addw	sp, #6
                           0006BA  1978 	Sstm8s_clk$CLK_GetITStatus$620 ==.
      008BB0 84               [ 1] 1979 	pop	a
                           0006BB  1980 	Sstm8s_clk$CLK_GetITStatus$621 ==.
      008BB1                       1981 00113$:
                           0006BB  1982 	Sstm8s_clk$CLK_GetITStatus$622 ==.
                                   1983 ;	drivers/src/stm8s_clk.c: 694: if (CLK_IT == CLK_IT_SWIF)
      008BB1 4D               [ 1] 1984 	tnz	a
      008BB2 27 10            [ 1] 1985 	jreq	00108$
                           0006BE  1986 	Sstm8s_clk$CLK_GetITStatus$623 ==.
                           0006BE  1987 	Sstm8s_clk$CLK_GetITStatus$624 ==.
                                   1988 ;	drivers/src/stm8s_clk.c: 697: if ((CLK->SWCR & (uint8_t)CLK_IT) == (uint8_t)0x0C)
      008BB4 C6 50 C5         [ 1] 1989 	ld	a, 0x50c5
      008BB7 14 03            [ 1] 1990 	and	a, (0x03, sp)
      008BB9 A1 0C            [ 1] 1991 	cp	a, #0x0c
      008BBB 26 04            [ 1] 1992 	jrne	00102$
                           0006C7  1993 	Sstm8s_clk$CLK_GetITStatus$625 ==.
                           0006C7  1994 	Sstm8s_clk$CLK_GetITStatus$626 ==.
                           0006C7  1995 	Sstm8s_clk$CLK_GetITStatus$627 ==.
                                   1996 ;	drivers/src/stm8s_clk.c: 699: bitstatus = SET;
      008BBD A6 01            [ 1] 1997 	ld	a, #0x01
                           0006C9  1998 	Sstm8s_clk$CLK_GetITStatus$628 ==.
      008BBF 20 11            [ 2] 1999 	jra	00109$
      008BC1                       2000 00102$:
                           0006CB  2001 	Sstm8s_clk$CLK_GetITStatus$629 ==.
                           0006CB  2002 	Sstm8s_clk$CLK_GetITStatus$630 ==.
                                   2003 ;	drivers/src/stm8s_clk.c: 703: bitstatus = RESET;
      008BC1 4F               [ 1] 2004 	clr	a
                           0006CC  2005 	Sstm8s_clk$CLK_GetITStatus$631 ==.
      008BC2 20 0E            [ 2] 2006 	jra	00109$
      008BC4                       2007 00108$:
                           0006CE  2008 	Sstm8s_clk$CLK_GetITStatus$632 ==.
                           0006CE  2009 	Sstm8s_clk$CLK_GetITStatus$633 ==.
                                   2010 ;	drivers/src/stm8s_clk.c: 709: if ((CLK->CSSR & (uint8_t)CLK_IT) == (uint8_t)0x0C)
      008BC4 C6 50 C8         [ 1] 2011 	ld	a, 0x50c8
      008BC7 14 03            [ 1] 2012 	and	a, (0x03, sp)
      008BC9 A1 0C            [ 1] 2013 	cp	a, #0x0c
      008BCB 26 04            [ 1] 2014 	jrne	00105$
                           0006D7  2015 	Sstm8s_clk$CLK_GetITStatus$634 ==.
                           0006D7  2016 	Sstm8s_clk$CLK_GetITStatus$635 ==.
                           0006D7  2017 	Sstm8s_clk$CLK_GetITStatus$636 ==.
                                   2018 ;	drivers/src/stm8s_clk.c: 711: bitstatus = SET;
      008BCD A6 01            [ 1] 2019 	ld	a, #0x01
                           0006D9  2020 	Sstm8s_clk$CLK_GetITStatus$637 ==.
      008BCF 20 01            [ 2] 2021 	jra	00109$
      008BD1                       2022 00105$:
                           0006DB  2023 	Sstm8s_clk$CLK_GetITStatus$638 ==.
                           0006DB  2024 	Sstm8s_clk$CLK_GetITStatus$639 ==.
                                   2025 ;	drivers/src/stm8s_clk.c: 715: bitstatus = RESET;
      008BD1 4F               [ 1] 2026 	clr	a
                           0006DC  2027 	Sstm8s_clk$CLK_GetITStatus$640 ==.
      008BD2                       2028 00109$:
                           0006DC  2029 	Sstm8s_clk$CLK_GetITStatus$641 ==.
                                   2030 ;	drivers/src/stm8s_clk.c: 720: return bitstatus;
                           0006DC  2031 	Sstm8s_clk$CLK_GetITStatus$642 ==.
                                   2032 ;	drivers/src/stm8s_clk.c: 721: }
                           0006DC  2033 	Sstm8s_clk$CLK_GetITStatus$643 ==.
                           0006DC  2034 	XG$CLK_GetITStatus$0$0 ==.
      008BD2 81               [ 4] 2035 	ret
                           0006DD  2036 	Sstm8s_clk$CLK_GetITStatus$644 ==.
                           0006DD  2037 	Sstm8s_clk$CLK_ClearITPendingBit$645 ==.
                                   2038 ;	drivers/src/stm8s_clk.c: 729: void CLK_ClearITPendingBit(CLK_IT_TypeDef CLK_IT)
                                   2039 ;	-----------------------------------------
                                   2040 ;	 function CLK_ClearITPendingBit
                                   2041 ;	-----------------------------------------
      008BD3                       2042 _CLK_ClearITPendingBit:
                           0006DD  2043 	Sstm8s_clk$CLK_ClearITPendingBit$646 ==.
                           0006DD  2044 	Sstm8s_clk$CLK_ClearITPendingBit$647 ==.
                                   2045 ;	drivers/src/stm8s_clk.c: 732: assert_param(IS_CLK_IT_OK(CLK_IT));
      008BD3 7B 03            [ 1] 2046 	ld	a, (0x03, sp)
      008BD5 A0 0C            [ 1] 2047 	sub	a, #0x0c
      008BD7 26 02            [ 1] 2048 	jrne	00127$
      008BD9 4C               [ 1] 2049 	inc	a
      008BDA 21                    2050 	.byte 0x21
      008BDB                       2051 00127$:
      008BDB 4F               [ 1] 2052 	clr	a
      008BDC                       2053 00128$:
                           0006E6  2054 	Sstm8s_clk$CLK_ClearITPendingBit$648 ==.
      008BDC 4D               [ 1] 2055 	tnz	a
      008BDD 26 19            [ 1] 2056 	jrne	00107$
      008BDF 88               [ 1] 2057 	push	a
                           0006EA  2058 	Sstm8s_clk$CLK_ClearITPendingBit$649 ==.
      008BE0 7B 04            [ 1] 2059 	ld	a, (0x04, sp)
      008BE2 A1 1C            [ 1] 2060 	cp	a, #0x1c
      008BE4 84               [ 1] 2061 	pop	a
                           0006EF  2062 	Sstm8s_clk$CLK_ClearITPendingBit$650 ==.
      008BE5 27 11            [ 1] 2063 	jreq	00107$
                           0006F1  2064 	Sstm8s_clk$CLK_ClearITPendingBit$651 ==.
      008BE7 88               [ 1] 2065 	push	a
                           0006F2  2066 	Sstm8s_clk$CLK_ClearITPendingBit$652 ==.
      008BE8 4B DC            [ 1] 2067 	push	#0xdc
                           0006F4  2068 	Sstm8s_clk$CLK_ClearITPendingBit$653 ==.
      008BEA 4B 02            [ 1] 2069 	push	#0x02
                           0006F6  2070 	Sstm8s_clk$CLK_ClearITPendingBit$654 ==.
      008BEC 5F               [ 1] 2071 	clrw	x
      008BED 89               [ 2] 2072 	pushw	x
                           0006F8  2073 	Sstm8s_clk$CLK_ClearITPendingBit$655 ==.
      008BEE 4B E2            [ 1] 2074 	push	#<(___str_0+0)
                           0006FA  2075 	Sstm8s_clk$CLK_ClearITPendingBit$656 ==.
      008BF0 4B 80            [ 1] 2076 	push	#((___str_0+0) >> 8)
                           0006FC  2077 	Sstm8s_clk$CLK_ClearITPendingBit$657 ==.
      008BF2 CD 84 F3         [ 4] 2078 	call	_assert_failed
      008BF5 5B 06            [ 2] 2079 	addw	sp, #6
                           000701  2080 	Sstm8s_clk$CLK_ClearITPendingBit$658 ==.
      008BF7 84               [ 1] 2081 	pop	a
                           000702  2082 	Sstm8s_clk$CLK_ClearITPendingBit$659 ==.
      008BF8                       2083 00107$:
                           000702  2084 	Sstm8s_clk$CLK_ClearITPendingBit$660 ==.
                                   2085 ;	drivers/src/stm8s_clk.c: 734: if (CLK_IT == (uint8_t)CLK_IT_CSSD)
      008BF8 4D               [ 1] 2086 	tnz	a
      008BF9 27 06            [ 1] 2087 	jreq	00102$
                           000705  2088 	Sstm8s_clk$CLK_ClearITPendingBit$661 ==.
                           000705  2089 	Sstm8s_clk$CLK_ClearITPendingBit$662 ==.
                                   2090 ;	drivers/src/stm8s_clk.c: 737: CLK->CSSR &= (uint8_t)(~CLK_CSSR_CSSD);
      008BFB 72 17 50 C8      [ 1] 2091 	bres	20680, #3
                           000709  2092 	Sstm8s_clk$CLK_ClearITPendingBit$663 ==.
      008BFF 20 04            [ 2] 2093 	jra	00104$
      008C01                       2094 00102$:
                           00070B  2095 	Sstm8s_clk$CLK_ClearITPendingBit$664 ==.
                           00070B  2096 	Sstm8s_clk$CLK_ClearITPendingBit$665 ==.
                                   2097 ;	drivers/src/stm8s_clk.c: 742: CLK->SWCR &= (uint8_t)(~CLK_SWCR_SWIF);
      008C01 72 17 50 C5      [ 1] 2098 	bres	20677, #3
                           00070F  2099 	Sstm8s_clk$CLK_ClearITPendingBit$666 ==.
      008C05                       2100 00104$:
                           00070F  2101 	Sstm8s_clk$CLK_ClearITPendingBit$667 ==.
                                   2102 ;	drivers/src/stm8s_clk.c: 745: }
                           00070F  2103 	Sstm8s_clk$CLK_ClearITPendingBit$668 ==.
                           00070F  2104 	XG$CLK_ClearITPendingBit$0$0 ==.
      008C05 81               [ 4] 2105 	ret
                           000710  2106 	Sstm8s_clk$CLK_ClearITPendingBit$669 ==.
                                   2107 	.area CODE
                                   2108 	.area CONST
                           000000  2109 G$HSIDivFactor$0_0$0 == .
      0080D6                       2110 _HSIDivFactor:
      0080D6 01                    2111 	.db #0x01	; 1
      0080D7 02                    2112 	.db #0x02	; 2
      0080D8 04                    2113 	.db #0x04	; 4
      0080D9 08                    2114 	.db #0x08	; 8
                           000004  2115 G$CLKPrescTable$0_0$0 == .
      0080DA                       2116 _CLKPrescTable:
      0080DA 01                    2117 	.db #0x01	; 1
      0080DB 02                    2118 	.db #0x02	; 2
      0080DC 04                    2119 	.db #0x04	; 4
      0080DD 08                    2120 	.db #0x08	; 8
      0080DE 0A                    2121 	.db #0x0a	; 10
      0080DF 10                    2122 	.db #0x10	; 16
      0080E0 14                    2123 	.db #0x14	; 20
      0080E1 28                    2124 	.db #0x28	; 40
                           00000C  2125 Fstm8s_clk$__str_0$0_0$0 == .
                                   2126 	.area CONST
      0080E2                       2127 ___str_0:
      0080E2 64 72 69 76 65 72 73  2128 	.ascii "drivers/src/stm8s_clk.c"
             2F 73 72 63 2F 73 74
             6D 38 73 5F 63 6C 6B
             2E 63
      0080F9 00                    2129 	.db 0x00
                                   2130 	.area CODE
                                   2131 	.area INITIALIZER
                                   2132 	.area CABS (ABS)
                                   2133 
                                   2134 	.area .debug_line (NOLOAD)
      000814 00 00 06 A3           2135 	.dw	0,Ldebug_line_end-Ldebug_line_start
      000818                       2136 Ldebug_line_start:
      000818 00 02                 2137 	.dw	2
      00081A 00 00 00 78           2138 	.dw	0,Ldebug_line_stmt-6-Ldebug_line_start
      00081E 01                    2139 	.db	1
      00081F 01                    2140 	.db	1
      000820 FB                    2141 	.db	-5
      000821 0F                    2142 	.db	15
      000822 0A                    2143 	.db	10
      000823 00                    2144 	.db	0
      000824 01                    2145 	.db	1
      000825 01                    2146 	.db	1
      000826 01                    2147 	.db	1
      000827 01                    2148 	.db	1
      000828 00                    2149 	.db	0
      000829 00                    2150 	.db	0
      00082A 00                    2151 	.db	0
      00082B 01                    2152 	.db	1
      00082C 43 3A 5C 50 72 6F 67  2153 	.ascii "C:\Program Files\SDCC\bin\..\include\stm8"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
             5C 73 74 6D 38
      000854 00                    2154 	.db	0
      000855 43 3A 5C 50 72 6F 67  2155 	.ascii "C:\Program Files\SDCC\bin\..\include"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
      000878 00                    2156 	.db	0
      000879 00                    2157 	.db	0
      00087A 64 72 69 76 65 72 73  2158 	.ascii "drivers/src/stm8s_clk.c"
             2F 73 72 63 2F 73 74
             6D 38 73 5F 63 6C 6B
             2E 63
      000891 00                    2159 	.db	0
      000892 00                    2160 	.uleb128	0
      000893 00                    2161 	.uleb128	0
      000894 00                    2162 	.uleb128	0
      000895 00                    2163 	.db	0
      000896                       2164 Ldebug_line_stmt:
      000896 00                    2165 	.db	0
      000897 05                    2166 	.uleb128	5
      000898 02                    2167 	.db	2
      000899 00 00 84 F6           2168 	.dw	0,(Sstm8s_clk$CLK_DeInit$0)
      00089D 03                    2169 	.db	3
      00089E C7 00                 2170 	.sleb128	71
      0008A0 01                    2171 	.db	1
      0008A1 09                    2172 	.db	9
      0008A2 00 00                 2173 	.dw	Sstm8s_clk$CLK_DeInit$2-Sstm8s_clk$CLK_DeInit$0
      0008A4 03                    2174 	.db	3
      0008A5 02                    2175 	.sleb128	2
      0008A6 01                    2176 	.db	1
      0008A7 09                    2177 	.db	9
      0008A8 00 04                 2178 	.dw	Sstm8s_clk$CLK_DeInit$3-Sstm8s_clk$CLK_DeInit$2
      0008AA 03                    2179 	.db	3
      0008AB 01                    2180 	.sleb128	1
      0008AC 01                    2181 	.db	1
      0008AD 09                    2182 	.db	9
      0008AE 00 04                 2183 	.dw	Sstm8s_clk$CLK_DeInit$4-Sstm8s_clk$CLK_DeInit$3
      0008B0 03                    2184 	.db	3
      0008B1 01                    2185 	.sleb128	1
      0008B2 01                    2186 	.db	1
      0008B3 09                    2187 	.db	9
      0008B4 00 04                 2188 	.dw	Sstm8s_clk$CLK_DeInit$5-Sstm8s_clk$CLK_DeInit$4
      0008B6 03                    2189 	.db	3
      0008B7 01                    2190 	.sleb128	1
      0008B8 01                    2191 	.db	1
      0008B9 09                    2192 	.db	9
      0008BA 00 04                 2193 	.dw	Sstm8s_clk$CLK_DeInit$6-Sstm8s_clk$CLK_DeInit$5
      0008BC 03                    2194 	.db	3
      0008BD 01                    2195 	.sleb128	1
      0008BE 01                    2196 	.db	1
      0008BF 09                    2197 	.db	9
      0008C0 00 04                 2198 	.dw	Sstm8s_clk$CLK_DeInit$7-Sstm8s_clk$CLK_DeInit$6
      0008C2 03                    2199 	.db	3
      0008C3 01                    2200 	.sleb128	1
      0008C4 01                    2201 	.db	1
      0008C5 09                    2202 	.db	9
      0008C6 00 04                 2203 	.dw	Sstm8s_clk$CLK_DeInit$8-Sstm8s_clk$CLK_DeInit$7
      0008C8 03                    2204 	.db	3
      0008C9 01                    2205 	.sleb128	1
      0008CA 01                    2206 	.db	1
      0008CB 09                    2207 	.db	9
      0008CC 00 04                 2208 	.dw	Sstm8s_clk$CLK_DeInit$9-Sstm8s_clk$CLK_DeInit$8
      0008CE 03                    2209 	.db	3
      0008CF 01                    2210 	.sleb128	1
      0008D0 01                    2211 	.db	1
      0008D1 09                    2212 	.db	9
      0008D2 00 04                 2213 	.dw	Sstm8s_clk$CLK_DeInit$10-Sstm8s_clk$CLK_DeInit$9
      0008D4 03                    2214 	.db	3
      0008D5 01                    2215 	.sleb128	1
      0008D6 01                    2216 	.db	1
      0008D7 09                    2217 	.db	9
      0008D8 00 04                 2218 	.dw	Sstm8s_clk$CLK_DeInit$11-Sstm8s_clk$CLK_DeInit$10
      0008DA 03                    2219 	.db	3
      0008DB 01                    2220 	.sleb128	1
      0008DC 01                    2221 	.db	1
      0008DD 09                    2222 	.db	9
      0008DE 00 06                 2223 	.dw	Sstm8s_clk$CLK_DeInit$12-Sstm8s_clk$CLK_DeInit$11
      0008E0 03                    2224 	.db	3
      0008E1 02                    2225 	.sleb128	2
      0008E2 01                    2226 	.db	1
      0008E3 09                    2227 	.db	9
      0008E4 00 04                 2228 	.dw	Sstm8s_clk$CLK_DeInit$13-Sstm8s_clk$CLK_DeInit$12
      0008E6 03                    2229 	.db	3
      0008E7 01                    2230 	.sleb128	1
      0008E8 01                    2231 	.db	1
      0008E9 09                    2232 	.db	9
      0008EA 00 04                 2233 	.dw	Sstm8s_clk$CLK_DeInit$14-Sstm8s_clk$CLK_DeInit$13
      0008EC 03                    2234 	.db	3
      0008ED 01                    2235 	.sleb128	1
      0008EE 01                    2236 	.db	1
      0008EF 09                    2237 	.db	9
      0008F0 00 04                 2238 	.dw	Sstm8s_clk$CLK_DeInit$15-Sstm8s_clk$CLK_DeInit$14
      0008F2 03                    2239 	.db	3
      0008F3 01                    2240 	.sleb128	1
      0008F4 01                    2241 	.db	1
      0008F5 09                    2242 	.db	9
      0008F6 00 01                 2243 	.dw	1+Sstm8s_clk$CLK_DeInit$16-Sstm8s_clk$CLK_DeInit$15
      0008F8 00                    2244 	.db	0
      0008F9 01                    2245 	.uleb128	1
      0008FA 01                    2246 	.db	1
      0008FB 00                    2247 	.db	0
      0008FC 05                    2248 	.uleb128	5
      0008FD 02                    2249 	.db	2
      0008FE 00 00 85 2D           2250 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$18)
      000902 03                    2251 	.db	3
      000903 E2 00                 2252 	.sleb128	98
      000905 01                    2253 	.db	1
      000906 09                    2254 	.db	9
      000907 00 00                 2255 	.dw	Sstm8s_clk$CLK_FastHaltWakeUpCmd$20-Sstm8s_clk$CLK_FastHaltWakeUpCmd$18
      000909 03                    2256 	.db	3
      00090A 03                    2257 	.sleb128	3
      00090B 01                    2258 	.db	1
      00090C 09                    2259 	.db	9
      00090D 00 18                 2260 	.dw	Sstm8s_clk$CLK_FastHaltWakeUpCmd$28-Sstm8s_clk$CLK_FastHaltWakeUpCmd$20
      00090F 03                    2261 	.db	3
      000910 05                    2262 	.sleb128	5
      000911 01                    2263 	.db	1
      000912 09                    2264 	.db	9
      000913 00 03                 2265 	.dw	Sstm8s_clk$CLK_FastHaltWakeUpCmd$29-Sstm8s_clk$CLK_FastHaltWakeUpCmd$28
      000915 03                    2266 	.db	3
      000916 7D                    2267 	.sleb128	-3
      000917 01                    2268 	.db	1
      000918 09                    2269 	.db	9
      000919 00 04                 2270 	.dw	Sstm8s_clk$CLK_FastHaltWakeUpCmd$31-Sstm8s_clk$CLK_FastHaltWakeUpCmd$29
      00091B 03                    2271 	.db	3
      00091C 03                    2272 	.sleb128	3
      00091D 01                    2273 	.db	1
      00091E 09                    2274 	.db	9
      00091F 00 07                 2275 	.dw	Sstm8s_clk$CLK_FastHaltWakeUpCmd$34-Sstm8s_clk$CLK_FastHaltWakeUpCmd$31
      000921 03                    2276 	.db	3
      000922 05                    2277 	.sleb128	5
      000923 01                    2278 	.db	1
      000924 09                    2279 	.db	9
      000925 00 05                 2280 	.dw	Sstm8s_clk$CLK_FastHaltWakeUpCmd$36-Sstm8s_clk$CLK_FastHaltWakeUpCmd$34
      000927 03                    2281 	.db	3
      000928 02                    2282 	.sleb128	2
      000929 01                    2283 	.db	1
      00092A 09                    2284 	.db	9
      00092B 00 01                 2285 	.dw	1+Sstm8s_clk$CLK_FastHaltWakeUpCmd$37-Sstm8s_clk$CLK_FastHaltWakeUpCmd$36
      00092D 00                    2286 	.db	0
      00092E 01                    2287 	.uleb128	1
      00092F 01                    2288 	.db	1
      000930 00                    2289 	.db	0
      000931 05                    2290 	.uleb128	5
      000932 02                    2291 	.db	2
      000933 00 00 85 59           2292 	.dw	0,(Sstm8s_clk$CLK_HSECmd$39)
      000937 03                    2293 	.db	3
      000938 F8 00                 2294 	.sleb128	120
      00093A 01                    2295 	.db	1
      00093B 09                    2296 	.db	9
      00093C 00 00                 2297 	.dw	Sstm8s_clk$CLK_HSECmd$41-Sstm8s_clk$CLK_HSECmd$39
      00093E 03                    2298 	.db	3
      00093F 03                    2299 	.sleb128	3
      000940 01                    2300 	.db	1
      000941 09                    2301 	.db	9
      000942 00 18                 2302 	.dw	Sstm8s_clk$CLK_HSECmd$49-Sstm8s_clk$CLK_HSECmd$41
      000944 03                    2303 	.db	3
      000945 05                    2304 	.sleb128	5
      000946 01                    2305 	.db	1
      000947 09                    2306 	.db	9
      000948 00 03                 2307 	.dw	Sstm8s_clk$CLK_HSECmd$50-Sstm8s_clk$CLK_HSECmd$49
      00094A 03                    2308 	.db	3
      00094B 7D                    2309 	.sleb128	-3
      00094C 01                    2310 	.db	1
      00094D 09                    2311 	.db	9
      00094E 00 04                 2312 	.dw	Sstm8s_clk$CLK_HSECmd$52-Sstm8s_clk$CLK_HSECmd$50
      000950 03                    2313 	.db	3
      000951 03                    2314 	.sleb128	3
      000952 01                    2315 	.db	1
      000953 09                    2316 	.db	9
      000954 00 07                 2317 	.dw	Sstm8s_clk$CLK_HSECmd$55-Sstm8s_clk$CLK_HSECmd$52
      000956 03                    2318 	.db	3
      000957 05                    2319 	.sleb128	5
      000958 01                    2320 	.db	1
      000959 09                    2321 	.db	9
      00095A 00 05                 2322 	.dw	Sstm8s_clk$CLK_HSECmd$57-Sstm8s_clk$CLK_HSECmd$55
      00095C 03                    2323 	.db	3
      00095D 02                    2324 	.sleb128	2
      00095E 01                    2325 	.db	1
      00095F 09                    2326 	.db	9
      000960 00 01                 2327 	.dw	1+Sstm8s_clk$CLK_HSECmd$58-Sstm8s_clk$CLK_HSECmd$57
      000962 00                    2328 	.db	0
      000963 01                    2329 	.uleb128	1
      000964 01                    2330 	.db	1
      000965 00                    2331 	.db	0
      000966 05                    2332 	.uleb128	5
      000967 02                    2333 	.db	2
      000968 00 00 85 85           2334 	.dw	0,(Sstm8s_clk$CLK_HSICmd$60)
      00096C 03                    2335 	.db	3
      00096D 8E 01                 2336 	.sleb128	142
      00096F 01                    2337 	.db	1
      000970 09                    2338 	.db	9
      000971 00 00                 2339 	.dw	Sstm8s_clk$CLK_HSICmd$62-Sstm8s_clk$CLK_HSICmd$60
      000973 03                    2340 	.db	3
      000974 03                    2341 	.sleb128	3
      000975 01                    2342 	.db	1
      000976 09                    2343 	.db	9
      000977 00 18                 2344 	.dw	Sstm8s_clk$CLK_HSICmd$70-Sstm8s_clk$CLK_HSICmd$62
      000979 03                    2345 	.db	3
      00097A 05                    2346 	.sleb128	5
      00097B 01                    2347 	.db	1
      00097C 09                    2348 	.db	9
      00097D 00 03                 2349 	.dw	Sstm8s_clk$CLK_HSICmd$71-Sstm8s_clk$CLK_HSICmd$70
      00097F 03                    2350 	.db	3
      000980 7D                    2351 	.sleb128	-3
      000981 01                    2352 	.db	1
      000982 09                    2353 	.db	9
      000983 00 04                 2354 	.dw	Sstm8s_clk$CLK_HSICmd$73-Sstm8s_clk$CLK_HSICmd$71
      000985 03                    2355 	.db	3
      000986 03                    2356 	.sleb128	3
      000987 01                    2357 	.db	1
      000988 09                    2358 	.db	9
      000989 00 07                 2359 	.dw	Sstm8s_clk$CLK_HSICmd$76-Sstm8s_clk$CLK_HSICmd$73
      00098B 03                    2360 	.db	3
      00098C 05                    2361 	.sleb128	5
      00098D 01                    2362 	.db	1
      00098E 09                    2363 	.db	9
      00098F 00 05                 2364 	.dw	Sstm8s_clk$CLK_HSICmd$78-Sstm8s_clk$CLK_HSICmd$76
      000991 03                    2365 	.db	3
      000992 02                    2366 	.sleb128	2
      000993 01                    2367 	.db	1
      000994 09                    2368 	.db	9
      000995 00 01                 2369 	.dw	1+Sstm8s_clk$CLK_HSICmd$79-Sstm8s_clk$CLK_HSICmd$78
      000997 00                    2370 	.db	0
      000998 01                    2371 	.uleb128	1
      000999 01                    2372 	.db	1
      00099A 00                    2373 	.db	0
      00099B 05                    2374 	.uleb128	5
      00099C 02                    2375 	.db	2
      00099D 00 00 85 B1           2376 	.dw	0,(Sstm8s_clk$CLK_LSICmd$81)
      0009A1 03                    2377 	.db	3
      0009A2 A5 01                 2378 	.sleb128	165
      0009A4 01                    2379 	.db	1
      0009A5 09                    2380 	.db	9
      0009A6 00 00                 2381 	.dw	Sstm8s_clk$CLK_LSICmd$83-Sstm8s_clk$CLK_LSICmd$81
      0009A8 03                    2382 	.db	3
      0009A9 03                    2383 	.sleb128	3
      0009AA 01                    2384 	.db	1
      0009AB 09                    2385 	.db	9
      0009AC 00 18                 2386 	.dw	Sstm8s_clk$CLK_LSICmd$91-Sstm8s_clk$CLK_LSICmd$83
      0009AE 03                    2387 	.db	3
      0009AF 05                    2388 	.sleb128	5
      0009B0 01                    2389 	.db	1
      0009B1 09                    2390 	.db	9
      0009B2 00 03                 2391 	.dw	Sstm8s_clk$CLK_LSICmd$92-Sstm8s_clk$CLK_LSICmd$91
      0009B4 03                    2392 	.db	3
      0009B5 7D                    2393 	.sleb128	-3
      0009B6 01                    2394 	.db	1
      0009B7 09                    2395 	.db	9
      0009B8 00 04                 2396 	.dw	Sstm8s_clk$CLK_LSICmd$94-Sstm8s_clk$CLK_LSICmd$92
      0009BA 03                    2397 	.db	3
      0009BB 03                    2398 	.sleb128	3
      0009BC 01                    2399 	.db	1
      0009BD 09                    2400 	.db	9
      0009BE 00 07                 2401 	.dw	Sstm8s_clk$CLK_LSICmd$97-Sstm8s_clk$CLK_LSICmd$94
      0009C0 03                    2402 	.db	3
      0009C1 05                    2403 	.sleb128	5
      0009C2 01                    2404 	.db	1
      0009C3 09                    2405 	.db	9
      0009C4 00 05                 2406 	.dw	Sstm8s_clk$CLK_LSICmd$99-Sstm8s_clk$CLK_LSICmd$97
      0009C6 03                    2407 	.db	3
      0009C7 02                    2408 	.sleb128	2
      0009C8 01                    2409 	.db	1
      0009C9 09                    2410 	.db	9
      0009CA 00 01                 2411 	.dw	1+Sstm8s_clk$CLK_LSICmd$100-Sstm8s_clk$CLK_LSICmd$99
      0009CC 00                    2412 	.db	0
      0009CD 01                    2413 	.uleb128	1
      0009CE 01                    2414 	.db	1
      0009CF 00                    2415 	.db	0
      0009D0 05                    2416 	.uleb128	5
      0009D1 02                    2417 	.db	2
      0009D2 00 00 85 DD           2418 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$102)
      0009D6 03                    2419 	.db	3
      0009D7 BC 01                 2420 	.sleb128	188
      0009D9 01                    2421 	.db	1
      0009DA 09                    2422 	.db	9
      0009DB 00 00                 2423 	.dw	Sstm8s_clk$CLK_CCOCmd$104-Sstm8s_clk$CLK_CCOCmd$102
      0009DD 03                    2424 	.db	3
      0009DE 03                    2425 	.sleb128	3
      0009DF 01                    2426 	.db	1
      0009E0 09                    2427 	.db	9
      0009E1 00 18                 2428 	.dw	Sstm8s_clk$CLK_CCOCmd$112-Sstm8s_clk$CLK_CCOCmd$104
      0009E3 03                    2429 	.db	3
      0009E4 05                    2430 	.sleb128	5
      0009E5 01                    2431 	.db	1
      0009E6 09                    2432 	.db	9
      0009E7 00 03                 2433 	.dw	Sstm8s_clk$CLK_CCOCmd$113-Sstm8s_clk$CLK_CCOCmd$112
      0009E9 03                    2434 	.db	3
      0009EA 7D                    2435 	.sleb128	-3
      0009EB 01                    2436 	.db	1
      0009EC 09                    2437 	.db	9
      0009ED 00 04                 2438 	.dw	Sstm8s_clk$CLK_CCOCmd$115-Sstm8s_clk$CLK_CCOCmd$113
      0009EF 03                    2439 	.db	3
      0009F0 03                    2440 	.sleb128	3
      0009F1 01                    2441 	.db	1
      0009F2 09                    2442 	.db	9
      0009F3 00 07                 2443 	.dw	Sstm8s_clk$CLK_CCOCmd$118-Sstm8s_clk$CLK_CCOCmd$115
      0009F5 03                    2444 	.db	3
      0009F6 05                    2445 	.sleb128	5
      0009F7 01                    2446 	.db	1
      0009F8 09                    2447 	.db	9
      0009F9 00 05                 2448 	.dw	Sstm8s_clk$CLK_CCOCmd$120-Sstm8s_clk$CLK_CCOCmd$118
      0009FB 03                    2449 	.db	3
      0009FC 02                    2450 	.sleb128	2
      0009FD 01                    2451 	.db	1
      0009FE 09                    2452 	.db	9
      0009FF 00 01                 2453 	.dw	1+Sstm8s_clk$CLK_CCOCmd$121-Sstm8s_clk$CLK_CCOCmd$120
      000A01 00                    2454 	.db	0
      000A02 01                    2455 	.uleb128	1
      000A03 01                    2456 	.db	1
      000A04 00                    2457 	.db	0
      000A05 05                    2458 	.uleb128	5
      000A06 02                    2459 	.db	2
      000A07 00 00 86 09           2460 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$123)
      000A0B 03                    2461 	.db	3
      000A0C D4 01                 2462 	.sleb128	212
      000A0E 01                    2463 	.db	1
      000A0F 09                    2464 	.db	9
      000A10 00 00                 2465 	.dw	Sstm8s_clk$CLK_ClockSwitchCmd$125-Sstm8s_clk$CLK_ClockSwitchCmd$123
      000A12 03                    2466 	.db	3
      000A13 03                    2467 	.sleb128	3
      000A14 01                    2468 	.db	1
      000A15 09                    2469 	.db	9
      000A16 00 18                 2470 	.dw	Sstm8s_clk$CLK_ClockSwitchCmd$133-Sstm8s_clk$CLK_ClockSwitchCmd$125
      000A18 03                    2471 	.db	3
      000A19 05                    2472 	.sleb128	5
      000A1A 01                    2473 	.db	1
      000A1B 09                    2474 	.db	9
      000A1C 00 03                 2475 	.dw	Sstm8s_clk$CLK_ClockSwitchCmd$134-Sstm8s_clk$CLK_ClockSwitchCmd$133
      000A1E 03                    2476 	.db	3
      000A1F 7D                    2477 	.sleb128	-3
      000A20 01                    2478 	.db	1
      000A21 09                    2479 	.db	9
      000A22 00 04                 2480 	.dw	Sstm8s_clk$CLK_ClockSwitchCmd$136-Sstm8s_clk$CLK_ClockSwitchCmd$134
      000A24 03                    2481 	.db	3
      000A25 03                    2482 	.sleb128	3
      000A26 01                    2483 	.db	1
      000A27 09                    2484 	.db	9
      000A28 00 07                 2485 	.dw	Sstm8s_clk$CLK_ClockSwitchCmd$139-Sstm8s_clk$CLK_ClockSwitchCmd$136
      000A2A 03                    2486 	.db	3
      000A2B 05                    2487 	.sleb128	5
      000A2C 01                    2488 	.db	1
      000A2D 09                    2489 	.db	9
      000A2E 00 05                 2490 	.dw	Sstm8s_clk$CLK_ClockSwitchCmd$141-Sstm8s_clk$CLK_ClockSwitchCmd$139
      000A30 03                    2491 	.db	3
      000A31 02                    2492 	.sleb128	2
      000A32 01                    2493 	.db	1
      000A33 09                    2494 	.db	9
      000A34 00 01                 2495 	.dw	1+Sstm8s_clk$CLK_ClockSwitchCmd$142-Sstm8s_clk$CLK_ClockSwitchCmd$141
      000A36 00                    2496 	.db	0
      000A37 01                    2497 	.uleb128	1
      000A38 01                    2498 	.db	1
      000A39 00                    2499 	.db	0
      000A3A 05                    2500 	.uleb128	5
      000A3B 02                    2501 	.db	2
      000A3C 00 00 86 35           2502 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$144)
      000A40 03                    2503 	.db	3
      000A41 ED 01                 2504 	.sleb128	237
      000A43 01                    2505 	.db	1
      000A44 09                    2506 	.db	9
      000A45 00 00                 2507 	.dw	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$146-Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$144
      000A47 03                    2508 	.db	3
      000A48 03                    2509 	.sleb128	3
      000A49 01                    2510 	.db	1
      000A4A 09                    2511 	.db	9
      000A4B 00 18                 2512 	.dw	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$154-Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$146
      000A4D 03                    2513 	.db	3
      000A4E 05                    2514 	.sleb128	5
      000A4F 01                    2515 	.db	1
      000A50 09                    2516 	.db	9
      000A51 00 03                 2517 	.dw	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$155-Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$154
      000A53 03                    2518 	.db	3
      000A54 7D                    2519 	.sleb128	-3
      000A55 01                    2520 	.db	1
      000A56 09                    2521 	.db	9
      000A57 00 04                 2522 	.dw	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$157-Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$155
      000A59 03                    2523 	.db	3
      000A5A 03                    2524 	.sleb128	3
      000A5B 01                    2525 	.db	1
      000A5C 09                    2526 	.db	9
      000A5D 00 07                 2527 	.dw	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$160-Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$157
      000A5F 03                    2528 	.db	3
      000A60 05                    2529 	.sleb128	5
      000A61 01                    2530 	.db	1
      000A62 09                    2531 	.db	9
      000A63 00 05                 2532 	.dw	Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$162-Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$160
      000A65 03                    2533 	.db	3
      000A66 02                    2534 	.sleb128	2
      000A67 01                    2535 	.db	1
      000A68 09                    2536 	.db	9
      000A69 00 01                 2537 	.dw	1+Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$163-Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$162
      000A6B 00                    2538 	.db	0
      000A6C 01                    2539 	.uleb128	1
      000A6D 01                    2540 	.db	1
      000A6E 00                    2541 	.db	0
      000A6F 05                    2542 	.uleb128	5
      000A70 02                    2543 	.db	2
      000A71 00 00 86 61           2544 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$165)
      000A75 03                    2545 	.db	3
      000A76 86 02                 2546 	.sleb128	262
      000A78 01                    2547 	.db	1
      000A79 09                    2548 	.db	9
      000A7A 00 01                 2549 	.dw	Sstm8s_clk$CLK_PeripheralClockConfig$168-Sstm8s_clk$CLK_PeripheralClockConfig$165
      000A7C 03                    2550 	.db	3
      000A7D 03                    2551 	.sleb128	3
      000A7E 01                    2552 	.db	1
      000A7F 09                    2553 	.db	9
      000A80 00 18                 2554 	.dw	Sstm8s_clk$CLK_PeripheralClockConfig$176-Sstm8s_clk$CLK_PeripheralClockConfig$168
      000A82 03                    2555 	.db	3
      000A83 01                    2556 	.sleb128	1
      000A84 01                    2557 	.db	1
      000A85 09                    2558 	.db	9
      000A86 00 80                 2559 	.dw	Sstm8s_clk$CLK_PeripheralClockConfig$193-Sstm8s_clk$CLK_PeripheralClockConfig$176
      000A88 03                    2560 	.db	3
      000A89 07                    2561 	.sleb128	7
      000A8A 01                    2562 	.db	1
      000A8B 09                    2563 	.db	9
      000A8C 00 12                 2564 	.dw	Sstm8s_clk$CLK_PeripheralClockConfig$196-Sstm8s_clk$CLK_PeripheralClockConfig$193
      000A8E 03                    2565 	.db	3
      000A8F 05                    2566 	.sleb128	5
      000A90 01                    2567 	.db	1
      000A91 09                    2568 	.db	9
      000A92 00 05                 2569 	.dw	Sstm8s_clk$CLK_PeripheralClockConfig$197-Sstm8s_clk$CLK_PeripheralClockConfig$196
      000A94 03                    2570 	.db	3
      000A95 76                    2571 	.sleb128	-10
      000A96 01                    2572 	.db	1
      000A97 09                    2573 	.db	9
      000A98 00 06                 2574 	.dw	Sstm8s_clk$CLK_PeripheralClockConfig$198-Sstm8s_clk$CLK_PeripheralClockConfig$197
      000A9A 03                    2575 	.db	3
      000A9B 05                    2576 	.sleb128	5
      000A9C 01                    2577 	.db	1
      000A9D 09                    2578 	.db	9
      000A9E 00 03                 2579 	.dw	Sstm8s_clk$CLK_PeripheralClockConfig$200-Sstm8s_clk$CLK_PeripheralClockConfig$198
      000AA0 03                    2580 	.db	3
      000AA1 7D                    2581 	.sleb128	-3
      000AA2 01                    2582 	.db	1
      000AA3 09                    2583 	.db	9
      000AA4 00 04                 2584 	.dw	Sstm8s_clk$CLK_PeripheralClockConfig$202-Sstm8s_clk$CLK_PeripheralClockConfig$200
      000AA6 03                    2585 	.db	3
      000AA7 03                    2586 	.sleb128	3
      000AA8 01                    2587 	.db	1
      000AA9 09                    2588 	.db	9
      000AAA 00 07                 2589 	.dw	Sstm8s_clk$CLK_PeripheralClockConfig$205-Sstm8s_clk$CLK_PeripheralClockConfig$202
      000AAC 03                    2590 	.db	3
      000AAD 05                    2591 	.sleb128	5
      000AAE 01                    2592 	.db	1
      000AAF 09                    2593 	.db	9
      000AB0 00 07                 2594 	.dw	Sstm8s_clk$CLK_PeripheralClockConfig$207-Sstm8s_clk$CLK_PeripheralClockConfig$205
      000AB2 03                    2595 	.db	3
      000AB3 08                    2596 	.sleb128	8
      000AB4 01                    2597 	.db	1
      000AB5 09                    2598 	.db	9
      000AB6 00 03                 2599 	.dw	Sstm8s_clk$CLK_PeripheralClockConfig$209-Sstm8s_clk$CLK_PeripheralClockConfig$207
      000AB8 03                    2600 	.db	3
      000AB9 7D                    2601 	.sleb128	-3
      000ABA 01                    2602 	.db	1
      000ABB 09                    2603 	.db	9
      000ABC 00 04                 2604 	.dw	Sstm8s_clk$CLK_PeripheralClockConfig$211-Sstm8s_clk$CLK_PeripheralClockConfig$209
      000ABE 03                    2605 	.db	3
      000ABF 03                    2606 	.sleb128	3
      000AC0 01                    2607 	.db	1
      000AC1 09                    2608 	.db	9
      000AC2 00 07                 2609 	.dw	Sstm8s_clk$CLK_PeripheralClockConfig$214-Sstm8s_clk$CLK_PeripheralClockConfig$211
      000AC4 03                    2610 	.db	3
      000AC5 05                    2611 	.sleb128	5
      000AC6 01                    2612 	.db	1
      000AC7 09                    2613 	.db	9
      000AC8 00 05                 2614 	.dw	Sstm8s_clk$CLK_PeripheralClockConfig$216-Sstm8s_clk$CLK_PeripheralClockConfig$214
      000ACA 03                    2615 	.db	3
      000ACB 03                    2616 	.sleb128	3
      000ACC 01                    2617 	.db	1
      000ACD 09                    2618 	.db	9
      000ACE 00 02                 2619 	.dw	1+Sstm8s_clk$CLK_PeripheralClockConfig$218-Sstm8s_clk$CLK_PeripheralClockConfig$216
      000AD0 00                    2620 	.db	0
      000AD1 01                    2621 	.uleb128	1
      000AD2 01                    2622 	.db	1
      000AD3 00                    2623 	.db	0
      000AD4 05                    2624 	.uleb128	5
      000AD5 02                    2625 	.db	2
      000AD6 00 00 87 41           2626 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$220)
      000ADA 03                    2627 	.db	3
      000ADB B4 02                 2628 	.sleb128	308
      000ADD 01                    2629 	.db	1
      000ADE 09                    2630 	.db	9
      000ADF 00 01                 2631 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$223-Sstm8s_clk$CLK_ClockSwitchConfig$220
      000AE1 03                    2632 	.db	3
      000AE2 07                    2633 	.sleb128	7
      000AE3 01                    2634 	.db	1
      000AE4 09                    2635 	.db	9
      000AE5 00 21                 2636 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$233-Sstm8s_clk$CLK_ClockSwitchConfig$223
      000AE7 03                    2637 	.db	3
      000AE8 01                    2638 	.sleb128	1
      000AE9 01                    2639 	.db	1
      000AEA 09                    2640 	.db	9
      000AEB 00 23                 2641 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$241-Sstm8s_clk$CLK_ClockSwitchConfig$233
      000AED 03                    2642 	.db	3
      000AEE 01                    2643 	.sleb128	1
      000AEF 01                    2644 	.db	1
      000AF0 09                    2645 	.db	9
      000AF1 00 18                 2646 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$249-Sstm8s_clk$CLK_ClockSwitchConfig$241
      000AF3 03                    2647 	.db	3
      000AF4 01                    2648 	.sleb128	1
      000AF5 01                    2649 	.db	1
      000AF6 09                    2650 	.db	9
      000AF7 00 18                 2651 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$257-Sstm8s_clk$CLK_ClockSwitchConfig$249
      000AF9 03                    2652 	.db	3
      000AFA 03                    2653 	.sleb128	3
      000AFB 01                    2654 	.db	1
      000AFC 09                    2655 	.db	9
      000AFD 00 05                 2656 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$258-Sstm8s_clk$CLK_ClockSwitchConfig$257
      000AFF 03                    2657 	.db	3
      000B00 06                    2658 	.sleb128	6
      000B01 01                    2659 	.db	1
      000B02 09                    2660 	.db	9
      000B03 00 04                 2661 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$259-Sstm8s_clk$CLK_ClockSwitchConfig$258
      000B05 03                    2662 	.db	3
      000B06 7D                    2663 	.sleb128	-3
      000B07 01                    2664 	.db	1
      000B08 09                    2665 	.db	9
      000B09 00 07                 2666 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$261-Sstm8s_clk$CLK_ClockSwitchConfig$259
      000B0B 03                    2667 	.db	3
      000B0C 03                    2668 	.sleb128	3
      000B0D 01                    2669 	.db	1
      000B0E 09                    2670 	.db	9
      000B0F 00 09                 2671 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$263-Sstm8s_clk$CLK_ClockSwitchConfig$261
      000B11 03                    2672 	.db	3
      000B12 03                    2673 	.sleb128	3
      000B13 01                    2674 	.db	1
      000B14 09                    2675 	.db	9
      000B15 00 04                 2676 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$265-Sstm8s_clk$CLK_ClockSwitchConfig$263
      000B17 03                    2677 	.db	3
      000B18 02                    2678 	.sleb128	2
      000B19 01                    2679 	.db	1
      000B1A 09                    2680 	.db	9
      000B1B 00 07                 2681 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$268-Sstm8s_clk$CLK_ClockSwitchConfig$265
      000B1D 03                    2682 	.db	3
      000B1E 04                    2683 	.sleb128	4
      000B1F 01                    2684 	.db	1
      000B20 09                    2685 	.db	9
      000B21 00 05                 2686 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$270-Sstm8s_clk$CLK_ClockSwitchConfig$268
      000B23 03                    2687 	.db	3
      000B24 04                    2688 	.sleb128	4
      000B25 01                    2689 	.db	1
      000B26 09                    2690 	.db	9
      000B27 00 06                 2691 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$272-Sstm8s_clk$CLK_ClockSwitchConfig$270
      000B29 03                    2692 	.db	3
      000B2A 03                    2693 	.sleb128	3
      000B2B 01                    2694 	.db	1
      000B2C 09                    2695 	.db	9
      000B2D 00 0B                 2696 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$274-Sstm8s_clk$CLK_ClockSwitchConfig$272
      000B2F 03                    2697 	.db	3
      000B30 02                    2698 	.sleb128	2
      000B31 01                    2699 	.db	1
      000B32 09                    2700 	.db	9
      000B33 00 03                 2701 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$276-Sstm8s_clk$CLK_ClockSwitchConfig$274
      000B35 03                    2702 	.db	3
      000B36 03                    2703 	.sleb128	3
      000B37 01                    2704 	.db	1
      000B38 09                    2705 	.db	9
      000B39 00 03                 2706 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$278-Sstm8s_clk$CLK_ClockSwitchConfig$276
      000B3B 03                    2707 	.db	3
      000B3C 02                    2708 	.sleb128	2
      000B3D 01                    2709 	.db	1
      000B3E 09                    2710 	.db	9
      000B3F 00 06                 2711 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$281-Sstm8s_clk$CLK_ClockSwitchConfig$278
      000B41 03                    2712 	.db	3
      000B42 04                    2713 	.sleb128	4
      000B43 01                    2714 	.db	1
      000B44 09                    2715 	.db	9
      000B45 00 04                 2716 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$284-Sstm8s_clk$CLK_ClockSwitchConfig$281
      000B47 03                    2717 	.db	3
      000B48 06                    2718 	.sleb128	6
      000B49 01                    2719 	.db	1
      000B4A 09                    2720 	.db	9
      000B4B 00 04                 2721 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$286-Sstm8s_clk$CLK_ClockSwitchConfig$284
      000B4D 03                    2722 	.db	3
      000B4E 02                    2723 	.sleb128	2
      000B4F 01                    2724 	.db	1
      000B50 09                    2725 	.db	9
      000B51 00 08                 2726 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$289-Sstm8s_clk$CLK_ClockSwitchConfig$286
      000B53 03                    2727 	.db	3
      000B54 04                    2728 	.sleb128	4
      000B55 01                    2729 	.db	1
      000B56 09                    2730 	.db	9
      000B57 00 06                 2731 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$291-Sstm8s_clk$CLK_ClockSwitchConfig$289
      000B59 03                    2732 	.db	3
      000B5A 04                    2733 	.sleb128	4
      000B5B 01                    2734 	.db	1
      000B5C 09                    2735 	.db	9
      000B5D 00 06                 2736 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$293-Sstm8s_clk$CLK_ClockSwitchConfig$291
      000B5F 03                    2737 	.db	3
      000B60 03                    2738 	.sleb128	3
      000B61 01                    2739 	.db	1
      000B62 09                    2740 	.db	9
      000B63 00 0C                 2741 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$295-Sstm8s_clk$CLK_ClockSwitchConfig$293
      000B65 03                    2742 	.db	3
      000B66 02                    2743 	.sleb128	2
      000B67 01                    2744 	.db	1
      000B68 09                    2745 	.db	9
      000B69 00 03                 2746 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$297-Sstm8s_clk$CLK_ClockSwitchConfig$295
      000B6B 03                    2747 	.db	3
      000B6C 03                    2748 	.sleb128	3
      000B6D 01                    2749 	.db	1
      000B6E 09                    2750 	.db	9
      000B6F 00 03                 2751 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$299-Sstm8s_clk$CLK_ClockSwitchConfig$297
      000B71 03                    2752 	.db	3
      000B72 03                    2753 	.sleb128	3
      000B73 01                    2754 	.db	1
      000B74 09                    2755 	.db	9
      000B75 00 04                 2756 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$300-Sstm8s_clk$CLK_ClockSwitchConfig$299
      000B77 03                    2757 	.db	3
      000B78 01                    2758 	.sleb128	1
      000B79 01                    2759 	.db	1
      000B7A 09                    2760 	.db	9
      000B7B 00 05                 2761 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$303-Sstm8s_clk$CLK_ClockSwitchConfig$300
      000B7D 03                    2762 	.db	3
      000B7E 04                    2763 	.sleb128	4
      000B7F 01                    2764 	.db	1
      000B80 09                    2765 	.db	9
      000B81 00 01                 2766 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$305-Sstm8s_clk$CLK_ClockSwitchConfig$303
      000B83 03                    2767 	.db	3
      000B84 03                    2768 	.sleb128	3
      000B85 01                    2769 	.db	1
      000B86 09                    2770 	.db	9
      000B87 00 07                 2771 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$307-Sstm8s_clk$CLK_ClockSwitchConfig$305
      000B89 03                    2772 	.db	3
      000B8A 03                    2773 	.sleb128	3
      000B8B 01                    2774 	.db	1
      000B8C 09                    2775 	.db	9
      000B8D 00 0A                 2776 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$310-Sstm8s_clk$CLK_ClockSwitchConfig$307
      000B8F 03                    2777 	.db	3
      000B90 02                    2778 	.sleb128	2
      000B91 01                    2779 	.db	1
      000B92 09                    2780 	.db	9
      000B93 00 06                 2781 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$312-Sstm8s_clk$CLK_ClockSwitchConfig$310
      000B95 03                    2782 	.db	3
      000B96 02                    2783 	.sleb128	2
      000B97 01                    2784 	.db	1
      000B98 09                    2785 	.db	9
      000B99 00 0A                 2786 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$315-Sstm8s_clk$CLK_ClockSwitchConfig$312
      000B9B 03                    2787 	.db	3
      000B9C 02                    2788 	.sleb128	2
      000B9D 01                    2789 	.db	1
      000B9E 09                    2790 	.db	9
      000B9F 00 06                 2791 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$317-Sstm8s_clk$CLK_ClockSwitchConfig$315
      000BA1 03                    2792 	.db	3
      000BA2 02                    2793 	.sleb128	2
      000BA3 01                    2794 	.db	1
      000BA4 09                    2795 	.db	9
      000BA5 00 0A                 2796 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$320-Sstm8s_clk$CLK_ClockSwitchConfig$317
      000BA7 03                    2797 	.db	3
      000BA8 02                    2798 	.sleb128	2
      000BA9 01                    2799 	.db	1
      000BAA 09                    2800 	.db	9
      000BAB 00 04                 2801 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$322-Sstm8s_clk$CLK_ClockSwitchConfig$320
      000BAD 03                    2802 	.db	3
      000BAE 03                    2803 	.sleb128	3
      000BAF 01                    2804 	.db	1
      000BB0 09                    2805 	.db	9
      000BB1 00 01                 2806 	.dw	Sstm8s_clk$CLK_ClockSwitchConfig$323-Sstm8s_clk$CLK_ClockSwitchConfig$322
      000BB3 03                    2807 	.db	3
      000BB4 01                    2808 	.sleb128	1
      000BB5 01                    2809 	.db	1
      000BB6 09                    2810 	.db	9
      000BB7 00 03                 2811 	.dw	1+Sstm8s_clk$CLK_ClockSwitchConfig$325-Sstm8s_clk$CLK_ClockSwitchConfig$323
      000BB9 00                    2812 	.db	0
      000BBA 01                    2813 	.uleb128	1
      000BBB 01                    2814 	.db	1
      000BBC 00                    2815 	.db	0
      000BBD 05                    2816 	.uleb128	5
      000BBE 02                    2817 	.db	2
      000BBF 00 00 88 6D           2818 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$327)
      000BC3 03                    2819 	.db	3
      000BC4 9E 03                 2820 	.sleb128	414
      000BC6 01                    2821 	.db	1
      000BC7 09                    2822 	.db	9
      000BC8 00 00                 2823 	.dw	Sstm8s_clk$CLK_HSIPrescalerConfig$329-Sstm8s_clk$CLK_HSIPrescalerConfig$327
      000BCA 03                    2824 	.db	3
      000BCB 03                    2825 	.sleb128	3
      000BCC 01                    2826 	.db	1
      000BCD 09                    2827 	.db	9
      000BCE 00 25                 2828 	.dw	Sstm8s_clk$CLK_HSIPrescalerConfig$339-Sstm8s_clk$CLK_HSIPrescalerConfig$329
      000BD0 03                    2829 	.db	3
      000BD1 03                    2830 	.sleb128	3
      000BD2 01                    2831 	.db	1
      000BD3 09                    2832 	.db	9
      000BD4 00 08                 2833 	.dw	Sstm8s_clk$CLK_HSIPrescalerConfig$340-Sstm8s_clk$CLK_HSIPrescalerConfig$339
      000BD6 03                    2834 	.db	3
      000BD7 03                    2835 	.sleb128	3
      000BD8 01                    2836 	.db	1
      000BD9 09                    2837 	.db	9
      000BDA 00 08                 2838 	.dw	Sstm8s_clk$CLK_HSIPrescalerConfig$341-Sstm8s_clk$CLK_HSIPrescalerConfig$340
      000BDC 03                    2839 	.db	3
      000BDD 01                    2840 	.sleb128	1
      000BDE 01                    2841 	.db	1
      000BDF 09                    2842 	.db	9
      000BE0 00 01                 2843 	.dw	1+Sstm8s_clk$CLK_HSIPrescalerConfig$342-Sstm8s_clk$CLK_HSIPrescalerConfig$341
      000BE2 00                    2844 	.db	0
      000BE3 01                    2845 	.uleb128	1
      000BE4 01                    2846 	.db	1
      000BE5 00                    2847 	.db	0
      000BE6 05                    2848 	.uleb128	5
      000BE7 02                    2849 	.db	2
      000BE8 00 00 88 A3           2850 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$344)
      000BEC 03                    2851 	.db	3
      000BED B3 03                 2852 	.sleb128	435
      000BEF 01                    2853 	.db	1
      000BF0 09                    2854 	.db	9
      000BF1 00 00                 2855 	.dw	Sstm8s_clk$CLK_CCOConfig$346-Sstm8s_clk$CLK_CCOConfig$344
      000BF3 03                    2856 	.db	3
      000BF4 03                    2857 	.sleb128	3
      000BF5 01                    2858 	.db	1
      000BF6 09                    2859 	.db	9
      000BF7 00 6D                 2860 	.dw	Sstm8s_clk$CLK_CCOConfig$365-Sstm8s_clk$CLK_CCOConfig$346
      000BF9 03                    2861 	.db	3
      000BFA 03                    2862 	.sleb128	3
      000BFB 01                    2863 	.db	1
      000BFC 09                    2864 	.db	9
      000BFD 00 08                 2865 	.dw	Sstm8s_clk$CLK_CCOConfig$366-Sstm8s_clk$CLK_CCOConfig$365
      000BFF 03                    2866 	.db	3
      000C00 03                    2867 	.sleb128	3
      000C01 01                    2868 	.db	1
      000C02 09                    2869 	.db	9
      000C03 00 08                 2870 	.dw	Sstm8s_clk$CLK_CCOConfig$367-Sstm8s_clk$CLK_CCOConfig$366
      000C05 03                    2871 	.db	3
      000C06 03                    2872 	.sleb128	3
      000C07 01                    2873 	.db	1
      000C08 09                    2874 	.db	9
      000C09 00 04                 2875 	.dw	Sstm8s_clk$CLK_CCOConfig$368-Sstm8s_clk$CLK_CCOConfig$367
      000C0B 03                    2876 	.db	3
      000C0C 01                    2877 	.sleb128	1
      000C0D 01                    2878 	.db	1
      000C0E 09                    2879 	.db	9
      000C0F 00 01                 2880 	.dw	1+Sstm8s_clk$CLK_CCOConfig$369-Sstm8s_clk$CLK_CCOConfig$368
      000C11 00                    2881 	.db	0
      000C12 01                    2882 	.uleb128	1
      000C13 01                    2883 	.db	1
      000C14 00                    2884 	.db	0
      000C15 05                    2885 	.uleb128	5
      000C16 02                    2886 	.db	2
      000C17 00 00 89 25           2887 	.dw	0,(Sstm8s_clk$CLK_ITConfig$371)
      000C1B 03                    2888 	.db	3
      000C1C CA 03                 2889 	.sleb128	458
      000C1E 01                    2890 	.db	1
      000C1F 09                    2891 	.db	9
      000C20 00 01                 2892 	.dw	Sstm8s_clk$CLK_ITConfig$374-Sstm8s_clk$CLK_ITConfig$371
      000C22 03                    2893 	.db	3
      000C23 03                    2894 	.sleb128	3
      000C24 01                    2895 	.db	1
      000C25 09                    2896 	.db	9
      000C26 00 18                 2897 	.dw	Sstm8s_clk$CLK_ITConfig$382-Sstm8s_clk$CLK_ITConfig$374
      000C28 03                    2898 	.db	3
      000C29 01                    2899 	.sleb128	1
      000C2A 01                    2900 	.db	1
      000C2B 09                    2901 	.db	9
      000C2C 00 32                 2902 	.dw	Sstm8s_clk$CLK_ITConfig$397-Sstm8s_clk$CLK_ITConfig$382
      000C2E 03                    2903 	.db	3
      000C2F 02                    2904 	.sleb128	2
      000C30 01                    2905 	.db	1
      000C31 09                    2906 	.db	9
      000C32 00 04                 2907 	.dw	Sstm8s_clk$CLK_ITConfig$399-Sstm8s_clk$CLK_ITConfig$397
      000C34 03                    2908 	.db	3
      000C35 02                    2909 	.sleb128	2
      000C36 01                    2910 	.db	1
      000C37 09                    2911 	.db	9
      000C38 00 07                 2912 	.dw	Sstm8s_clk$CLK_ITConfig$401-Sstm8s_clk$CLK_ITConfig$399
      000C3A 03                    2913 	.db	3
      000C3B 03                    2914 	.sleb128	3
      000C3C 01                    2915 	.db	1
      000C3D 09                    2916 	.db	9
      000C3E 00 04                 2917 	.dw	Sstm8s_clk$CLK_ITConfig$402-Sstm8s_clk$CLK_ITConfig$401
      000C40 03                    2918 	.db	3
      000C41 01                    2919 	.sleb128	1
      000C42 01                    2920 	.db	1
      000C43 09                    2921 	.db	9
      000C44 00 02                 2922 	.dw	Sstm8s_clk$CLK_ITConfig$403-Sstm8s_clk$CLK_ITConfig$402
      000C46 03                    2923 	.db	3
      000C47 01                    2924 	.sleb128	1
      000C48 01                    2925 	.db	1
      000C49 09                    2926 	.db	9
      000C4A 00 00                 2927 	.dw	Sstm8s_clk$CLK_ITConfig$404-Sstm8s_clk$CLK_ITConfig$403
      000C4C 03                    2928 	.db	3
      000C4D 01                    2929 	.sleb128	1
      000C4E 01                    2930 	.db	1
      000C4F 09                    2931 	.db	9
      000C50 00 04                 2932 	.dw	Sstm8s_clk$CLK_ITConfig$405-Sstm8s_clk$CLK_ITConfig$404
      000C52 03                    2933 	.db	3
      000C53 01                    2934 	.sleb128	1
      000C54 01                    2935 	.db	1
      000C55 09                    2936 	.db	9
      000C56 00 02                 2937 	.dw	Sstm8s_clk$CLK_ITConfig$407-Sstm8s_clk$CLK_ITConfig$405
      000C58 03                    2938 	.db	3
      000C59 03                    2939 	.sleb128	3
      000C5A 01                    2940 	.db	1
      000C5B 09                    2941 	.db	9
      000C5C 00 00                 2942 	.dw	Sstm8s_clk$CLK_ITConfig$409-Sstm8s_clk$CLK_ITConfig$407
      000C5E 03                    2943 	.db	3
      000C5F 04                    2944 	.sleb128	4
      000C60 01                    2945 	.db	1
      000C61 09                    2946 	.db	9
      000C62 00 07                 2947 	.dw	Sstm8s_clk$CLK_ITConfig$411-Sstm8s_clk$CLK_ITConfig$409
      000C64 03                    2948 	.db	3
      000C65 03                    2949 	.sleb128	3
      000C66 01                    2950 	.db	1
      000C67 09                    2951 	.db	9
      000C68 00 04                 2952 	.dw	Sstm8s_clk$CLK_ITConfig$412-Sstm8s_clk$CLK_ITConfig$411
      000C6A 03                    2953 	.db	3
      000C6B 01                    2954 	.sleb128	1
      000C6C 01                    2955 	.db	1
      000C6D 09                    2956 	.db	9
      000C6E 00 02                 2957 	.dw	Sstm8s_clk$CLK_ITConfig$413-Sstm8s_clk$CLK_ITConfig$412
      000C70 03                    2958 	.db	3
      000C71 01                    2959 	.sleb128	1
      000C72 01                    2960 	.db	1
      000C73 09                    2961 	.db	9
      000C74 00 00                 2962 	.dw	Sstm8s_clk$CLK_ITConfig$414-Sstm8s_clk$CLK_ITConfig$413
      000C76 03                    2963 	.db	3
      000C77 01                    2964 	.sleb128	1
      000C78 01                    2965 	.db	1
      000C79 09                    2966 	.db	9
      000C7A 00 04                 2967 	.dw	Sstm8s_clk$CLK_ITConfig$416-Sstm8s_clk$CLK_ITConfig$414
      000C7C 03                    2968 	.db	3
      000C7D 04                    2969 	.sleb128	4
      000C7E 01                    2970 	.db	1
      000C7F 09                    2971 	.db	9
      000C80 00 00                 2972 	.dw	Sstm8s_clk$CLK_ITConfig$417-Sstm8s_clk$CLK_ITConfig$416
      000C82 03                    2973 	.db	3
      000C83 02                    2974 	.sleb128	2
      000C84 01                    2975 	.db	1
      000C85 09                    2976 	.db	9
      000C86 00 02                 2977 	.dw	1+Sstm8s_clk$CLK_ITConfig$419-Sstm8s_clk$CLK_ITConfig$417
      000C88 00                    2978 	.db	0
      000C89 01                    2979 	.uleb128	1
      000C8A 01                    2980 	.db	1
      000C8B 00                    2981 	.db	0
      000C8C 05                    2982 	.uleb128	5
      000C8D 02                    2983 	.db	2
      000C8E 00 00 89 9A           2984 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$421)
      000C92 03                    2985 	.db	3
      000C93 F3 03                 2986 	.sleb128	499
      000C95 01                    2987 	.db	1
      000C96 09                    2988 	.db	9
      000C97 00 01                 2989 	.dw	Sstm8s_clk$CLK_SYSCLKConfig$424-Sstm8s_clk$CLK_SYSCLKConfig$421
      000C99 03                    2990 	.db	3
      000C9A 03                    2991 	.sleb128	3
      000C9B 01                    2992 	.db	1
      000C9C 09                    2993 	.db	9
      000C9D 00 64                 2994 	.dw	Sstm8s_clk$CLK_SYSCLKConfig$442-Sstm8s_clk$CLK_SYSCLKConfig$424
      000C9F 03                    2995 	.db	3
      000CA0 04                    2996 	.sleb128	4
      000CA1 01                    2997 	.db	1
      000CA2 09                    2998 	.db	9
      000CA3 00 03                 2999 	.dw	Sstm8s_clk$CLK_SYSCLKConfig$443-Sstm8s_clk$CLK_SYSCLKConfig$442
      000CA5 03                    3000 	.db	3
      000CA6 7E                    3001 	.sleb128	-2
      000CA7 01                    3002 	.db	1
      000CA8 09                    3003 	.db	9
      000CA9 00 04                 3004 	.dw	Sstm8s_clk$CLK_SYSCLKConfig$445-Sstm8s_clk$CLK_SYSCLKConfig$443
      000CAB 03                    3005 	.db	3
      000CAC 02                    3006 	.sleb128	2
      000CAD 01                    3007 	.db	1
      000CAE 09                    3008 	.db	9
      000CAF 00 05                 3009 	.dw	Sstm8s_clk$CLK_SYSCLKConfig$446-Sstm8s_clk$CLK_SYSCLKConfig$445
      000CB1 03                    3010 	.db	3
      000CB2 01                    3011 	.sleb128	1
      000CB3 01                    3012 	.db	1
      000CB4 09                    3013 	.db	9
      000CB5 00 10                 3014 	.dw	Sstm8s_clk$CLK_SYSCLKConfig$449-Sstm8s_clk$CLK_SYSCLKConfig$446
      000CB7 03                    3015 	.db	3
      000CB8 04                    3016 	.sleb128	4
      000CB9 01                    3017 	.db	1
      000CBA 09                    3018 	.db	9
      000CBB 00 05                 3019 	.dw	Sstm8s_clk$CLK_SYSCLKConfig$450-Sstm8s_clk$CLK_SYSCLKConfig$449
      000CBD 03                    3020 	.db	3
      000CBE 01                    3021 	.sleb128	1
      000CBF 01                    3022 	.db	1
      000CC0 09                    3023 	.db	9
      000CC1 00 0E                 3024 	.dw	Sstm8s_clk$CLK_SYSCLKConfig$452-Sstm8s_clk$CLK_SYSCLKConfig$450
      000CC3 03                    3025 	.db	3
      000CC4 02                    3026 	.sleb128	2
      000CC5 01                    3027 	.db	1
      000CC6 09                    3028 	.db	9
      000CC7 00 02                 3029 	.dw	1+Sstm8s_clk$CLK_SYSCLKConfig$454-Sstm8s_clk$CLK_SYSCLKConfig$452
      000CC9 00                    3030 	.db	0
      000CCA 01                    3031 	.uleb128	1
      000CCB 01                    3032 	.db	1
      000CCC 00                    3033 	.db	0
      000CCD 05                    3034 	.uleb128	5
      000CCE 02                    3035 	.db	2
      000CCF 00 00 8A 30           3036 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$456)
      000CD3 03                    3037 	.db	3
      000CD4 8A 04                 3038 	.sleb128	522
      000CD6 01                    3039 	.db	1
      000CD7 09                    3040 	.db	9
      000CD8 00 00                 3041 	.dw	Sstm8s_clk$CLK_SWIMConfig$458-Sstm8s_clk$CLK_SWIMConfig$456
      000CDA 03                    3042 	.db	3
      000CDB 03                    3043 	.sleb128	3
      000CDC 01                    3044 	.db	1
      000CDD 09                    3045 	.db	9
      000CDE 00 18                 3046 	.dw	Sstm8s_clk$CLK_SWIMConfig$466-Sstm8s_clk$CLK_SWIMConfig$458
      000CE0 03                    3047 	.db	3
      000CE1 05                    3048 	.sleb128	5
      000CE2 01                    3049 	.db	1
      000CE3 09                    3050 	.db	9
      000CE4 00 03                 3051 	.dw	Sstm8s_clk$CLK_SWIMConfig$467-Sstm8s_clk$CLK_SWIMConfig$466
      000CE6 03                    3052 	.db	3
      000CE7 7D                    3053 	.sleb128	-3
      000CE8 01                    3054 	.db	1
      000CE9 09                    3055 	.db	9
      000CEA 00 04                 3056 	.dw	Sstm8s_clk$CLK_SWIMConfig$469-Sstm8s_clk$CLK_SWIMConfig$467
      000CEC 03                    3057 	.db	3
      000CED 03                    3058 	.sleb128	3
      000CEE 01                    3059 	.db	1
      000CEF 09                    3060 	.db	9
      000CF0 00 07                 3061 	.dw	Sstm8s_clk$CLK_SWIMConfig$472-Sstm8s_clk$CLK_SWIMConfig$469
      000CF2 03                    3062 	.db	3
      000CF3 05                    3063 	.sleb128	5
      000CF4 01                    3064 	.db	1
      000CF5 09                    3065 	.db	9
      000CF6 00 05                 3066 	.dw	Sstm8s_clk$CLK_SWIMConfig$474-Sstm8s_clk$CLK_SWIMConfig$472
      000CF8 03                    3067 	.db	3
      000CF9 02                    3068 	.sleb128	2
      000CFA 01                    3069 	.db	1
      000CFB 09                    3070 	.db	9
      000CFC 00 01                 3071 	.dw	1+Sstm8s_clk$CLK_SWIMConfig$475-Sstm8s_clk$CLK_SWIMConfig$474
      000CFE 00                    3072 	.db	0
      000CFF 01                    3073 	.uleb128	1
      000D00 01                    3074 	.db	1
      000D01 00                    3075 	.db	0
      000D02 05                    3076 	.uleb128	5
      000D03 02                    3077 	.db	2
      000D04 00 00 8A 5C           3078 	.dw	0,(Sstm8s_clk$CLK_ClockSecuritySystemEnable$477)
      000D08 03                    3079 	.db	3
      000D09 A2 04                 3080 	.sleb128	546
      000D0B 01                    3081 	.db	1
      000D0C 09                    3082 	.db	9
      000D0D 00 00                 3083 	.dw	Sstm8s_clk$CLK_ClockSecuritySystemEnable$479-Sstm8s_clk$CLK_ClockSecuritySystemEnable$477
      000D0F 03                    3084 	.db	3
      000D10 03                    3085 	.sleb128	3
      000D11 01                    3086 	.db	1
      000D12 09                    3087 	.db	9
      000D13 00 04                 3088 	.dw	Sstm8s_clk$CLK_ClockSecuritySystemEnable$480-Sstm8s_clk$CLK_ClockSecuritySystemEnable$479
      000D15 03                    3089 	.db	3
      000D16 01                    3090 	.sleb128	1
      000D17 01                    3091 	.db	1
      000D18 09                    3092 	.db	9
      000D19 00 01                 3093 	.dw	1+Sstm8s_clk$CLK_ClockSecuritySystemEnable$481-Sstm8s_clk$CLK_ClockSecuritySystemEnable$480
      000D1B 00                    3094 	.db	0
      000D1C 01                    3095 	.uleb128	1
      000D1D 01                    3096 	.db	1
      000D1E 00                    3097 	.db	0
      000D1F 05                    3098 	.uleb128	5
      000D20 02                    3099 	.db	2
      000D21 00 00 8A 61           3100 	.dw	0,(Sstm8s_clk$CLK_GetSYSCLKSource$483)
      000D25 03                    3101 	.db	3
      000D26 AE 04                 3102 	.sleb128	558
      000D28 01                    3103 	.db	1
      000D29 09                    3104 	.db	9
      000D2A 00 00                 3105 	.dw	Sstm8s_clk$CLK_GetSYSCLKSource$485-Sstm8s_clk$CLK_GetSYSCLKSource$483
      000D2C 03                    3106 	.db	3
      000D2D 02                    3107 	.sleb128	2
      000D2E 01                    3108 	.db	1
      000D2F 09                    3109 	.db	9
      000D30 00 03                 3110 	.dw	Sstm8s_clk$CLK_GetSYSCLKSource$486-Sstm8s_clk$CLK_GetSYSCLKSource$485
      000D32 03                    3111 	.db	3
      000D33 01                    3112 	.sleb128	1
      000D34 01                    3113 	.db	1
      000D35 09                    3114 	.db	9
      000D36 00 01                 3115 	.dw	1+Sstm8s_clk$CLK_GetSYSCLKSource$487-Sstm8s_clk$CLK_GetSYSCLKSource$486
      000D38 00                    3116 	.db	0
      000D39 01                    3117 	.uleb128	1
      000D3A 01                    3118 	.db	1
      000D3B 00                    3119 	.db	0
      000D3C 05                    3120 	.uleb128	5
      000D3D 02                    3121 	.db	2
      000D3E 00 00 8A 65           3122 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$489)
      000D42 03                    3123 	.db	3
      000D43 B8 04                 3124 	.sleb128	568
      000D45 01                    3125 	.db	1
      000D46 09                    3126 	.db	9
      000D47 00 02                 3127 	.dw	Sstm8s_clk$CLK_GetClockFreq$492-Sstm8s_clk$CLK_GetClockFreq$489
      000D49 03                    3128 	.db	3
      000D4A 07                    3129 	.sleb128	7
      000D4B 01                    3130 	.db	1
      000D4C 09                    3131 	.db	9
      000D4D 00 05                 3132 	.dw	Sstm8s_clk$CLK_GetClockFreq$493-Sstm8s_clk$CLK_GetClockFreq$492
      000D4F 03                    3133 	.db	3
      000D50 02                    3134 	.sleb128	2
      000D51 01                    3135 	.db	1
      000D52 09                    3136 	.db	9
      000D53 00 06                 3137 	.dw	Sstm8s_clk$CLK_GetClockFreq$496-Sstm8s_clk$CLK_GetClockFreq$493
      000D55 03                    3138 	.db	3
      000D56 02                    3139 	.sleb128	2
      000D57 01                    3140 	.db	1
      000D58 09                    3141 	.db	9
      000D59 00 05                 3142 	.dw	Sstm8s_clk$CLK_GetClockFreq$497-Sstm8s_clk$CLK_GetClockFreq$496
      000D5B 03                    3143 	.db	3
      000D5C 01                    3144 	.sleb128	1
      000D5D 01                    3145 	.db	1
      000D5E 09                    3146 	.db	9
      000D5F 00 03                 3147 	.dw	Sstm8s_clk$CLK_GetClockFreq$498-Sstm8s_clk$CLK_GetClockFreq$497
      000D61 03                    3148 	.db	3
      000D62 01                    3149 	.sleb128	1
      000D63 01                    3150 	.db	1
      000D64 09                    3151 	.db	9
      000D65 00 05                 3152 	.dw	Sstm8s_clk$CLK_GetClockFreq$499-Sstm8s_clk$CLK_GetClockFreq$498
      000D67 03                    3153 	.db	3
      000D68 01                    3154 	.sleb128	1
      000D69 01                    3155 	.db	1
      000D6A 09                    3156 	.db	9
      000D6B 00 19                 3157 	.dw	Sstm8s_clk$CLK_GetClockFreq$508-Sstm8s_clk$CLK_GetClockFreq$499
      000D6D 03                    3158 	.db	3
      000D6E 02                    3159 	.sleb128	2
      000D6F 01                    3160 	.db	1
      000D70 09                    3161 	.db	9
      000D71 00 06                 3162 	.dw	Sstm8s_clk$CLK_GetClockFreq$511-Sstm8s_clk$CLK_GetClockFreq$508
      000D73 03                    3163 	.db	3
      000D74 02                    3164 	.sleb128	2
      000D75 01                    3165 	.db	1
      000D76 09                    3166 	.db	9
      000D77 00 09                 3167 	.dw	Sstm8s_clk$CLK_GetClockFreq$514-Sstm8s_clk$CLK_GetClockFreq$511
      000D79 03                    3168 	.db	3
      000D7A 04                    3169 	.sleb128	4
      000D7B 01                    3170 	.db	1
      000D7C 09                    3171 	.db	9
      000D7D 00 08                 3172 	.dw	Sstm8s_clk$CLK_GetClockFreq$516-Sstm8s_clk$CLK_GetClockFreq$514
      000D7F 03                    3173 	.db	3
      000D80 03                    3174 	.sleb128	3
      000D81 01                    3175 	.db	1
      000D82 09                    3176 	.db	9
      000D83 00 03                 3177 	.dw	Sstm8s_clk$CLK_GetClockFreq$517-Sstm8s_clk$CLK_GetClockFreq$516
      000D85 03                    3178 	.db	3
      000D86 01                    3179 	.sleb128	1
      000D87 01                    3180 	.db	1
      000D88 09                    3181 	.db	9
      000D89 00 03                 3182 	.dw	1+Sstm8s_clk$CLK_GetClockFreq$519-Sstm8s_clk$CLK_GetClockFreq$517
      000D8B 00                    3183 	.db	0
      000D8C 01                    3184 	.uleb128	1
      000D8D 01                    3185 	.db	1
      000D8E 00                    3186 	.db	0
      000D8F 05                    3187 	.uleb128	5
      000D90 02                    3188 	.db	2
      000D91 00 00 8A B5           3189 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$521)
      000D95 03                    3190 	.db	3
      000D96 DB 04                 3191 	.sleb128	603
      000D98 01                    3192 	.db	1
      000D99 09                    3193 	.db	9
      000D9A 00 00                 3194 	.dw	Sstm8s_clk$CLK_AdjustHSICalibrationValue$523-Sstm8s_clk$CLK_AdjustHSICalibrationValue$521
      000D9C 03                    3195 	.db	3
      000D9D 03                    3196 	.sleb128	3
      000D9E 01                    3197 	.db	1
      000D9F 09                    3198 	.db	9
      000DA0 00 3C                 3199 	.dw	Sstm8s_clk$CLK_AdjustHSICalibrationValue$537-Sstm8s_clk$CLK_AdjustHSICalibrationValue$523
      000DA2 03                    3200 	.db	3
      000DA3 03                    3201 	.sleb128	3
      000DA4 01                    3202 	.db	1
      000DA5 09                    3203 	.db	9
      000DA6 00 0A                 3204 	.dw	Sstm8s_clk$CLK_AdjustHSICalibrationValue$538-Sstm8s_clk$CLK_AdjustHSICalibrationValue$537
      000DA8 03                    3205 	.db	3
      000DA9 01                    3206 	.sleb128	1
      000DAA 01                    3207 	.db	1
      000DAB 09                    3208 	.db	9
      000DAC 00 01                 3209 	.dw	1+Sstm8s_clk$CLK_AdjustHSICalibrationValue$539-Sstm8s_clk$CLK_AdjustHSICalibrationValue$538
      000DAE 00                    3210 	.db	0
      000DAF 01                    3211 	.uleb128	1
      000DB0 01                    3212 	.db	1
      000DB1 00                    3213 	.db	0
      000DB2 05                    3214 	.uleb128	5
      000DB3 02                    3215 	.db	2
      000DB4 00 00 8A FC           3216 	.dw	0,(Sstm8s_clk$CLK_SYSCLKEmergencyClear$541)
      000DB8 03                    3217 	.db	3
      000DB9 ED 04                 3218 	.sleb128	621
      000DBB 01                    3219 	.db	1
      000DBC 09                    3220 	.db	9
      000DBD 00 00                 3221 	.dw	Sstm8s_clk$CLK_SYSCLKEmergencyClear$543-Sstm8s_clk$CLK_SYSCLKEmergencyClear$541
      000DBF 03                    3222 	.db	3
      000DC0 02                    3223 	.sleb128	2
      000DC1 01                    3224 	.db	1
      000DC2 09                    3225 	.db	9
      000DC3 00 04                 3226 	.dw	Sstm8s_clk$CLK_SYSCLKEmergencyClear$544-Sstm8s_clk$CLK_SYSCLKEmergencyClear$543
      000DC5 03                    3227 	.db	3
      000DC6 01                    3228 	.sleb128	1
      000DC7 01                    3229 	.db	1
      000DC8 09                    3230 	.db	9
      000DC9 00 01                 3231 	.dw	1+Sstm8s_clk$CLK_SYSCLKEmergencyClear$545-Sstm8s_clk$CLK_SYSCLKEmergencyClear$544
      000DCB 00                    3232 	.db	0
      000DCC 01                    3233 	.uleb128	1
      000DCD 01                    3234 	.db	1
      000DCE 00                    3235 	.db	0
      000DCF 05                    3236 	.uleb128	5
      000DD0 02                    3237 	.db	2
      000DD1 00 00 8B 01           3238 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$547)
      000DD5 03                    3239 	.db	3
      000DD6 F9 04                 3240 	.sleb128	633
      000DD8 01                    3241 	.db	1
      000DD9 09                    3242 	.db	9
      000DDA 00 01                 3243 	.dw	Sstm8s_clk$CLK_GetFlagStatus$550-Sstm8s_clk$CLK_GetFlagStatus$547
      000DDC 03                    3244 	.db	3
      000DDD 07                    3245 	.sleb128	7
      000DDE 01                    3246 	.db	1
      000DDF 09                    3247 	.db	9
      000DE0 00 4B                 3248 	.dw	Sstm8s_clk$CLK_GetFlagStatus$569-Sstm8s_clk$CLK_GetFlagStatus$550
      000DE2 03                    3249 	.db	3
      000DE3 03                    3250 	.sleb128	3
      000DE4 01                    3251 	.db	1
      000DE5 09                    3252 	.db	9
      000DE6 00 01                 3253 	.dw	Sstm8s_clk$CLK_GetFlagStatus$570-Sstm8s_clk$CLK_GetFlagStatus$569
      000DE8 03                    3254 	.db	3
      000DE9 03                    3255 	.sleb128	3
      000DEA 01                    3256 	.db	1
      000DEB 09                    3257 	.db	9
      000DEC 00 06                 3258 	.dw	Sstm8s_clk$CLK_GetFlagStatus$573-Sstm8s_clk$CLK_GetFlagStatus$570
      000DEE 03                    3259 	.db	3
      000DEF 02                    3260 	.sleb128	2
      000DF0 01                    3261 	.db	1
      000DF1 09                    3262 	.db	9
      000DF2 00 05                 3263 	.dw	Sstm8s_clk$CLK_GetFlagStatus$575-Sstm8s_clk$CLK_GetFlagStatus$573
      000DF4 03                    3264 	.db	3
      000DF5 02                    3265 	.sleb128	2
      000DF6 01                    3266 	.db	1
      000DF7 09                    3267 	.db	9
      000DF8 00 05                 3268 	.dw	Sstm8s_clk$CLK_GetFlagStatus$578-Sstm8s_clk$CLK_GetFlagStatus$575
      000DFA 03                    3269 	.db	3
      000DFB 02                    3270 	.sleb128	2
      000DFC 01                    3271 	.db	1
      000DFD 09                    3272 	.db	9
      000DFE 00 05                 3273 	.dw	Sstm8s_clk$CLK_GetFlagStatus$580-Sstm8s_clk$CLK_GetFlagStatus$578
      000E00 03                    3274 	.db	3
      000E01 02                    3275 	.sleb128	2
      000E02 01                    3276 	.db	1
      000E03 09                    3277 	.db	9
      000E04 00 05                 3278 	.dw	Sstm8s_clk$CLK_GetFlagStatus$583-Sstm8s_clk$CLK_GetFlagStatus$580
      000E06 03                    3279 	.db	3
      000E07 02                    3280 	.sleb128	2
      000E08 01                    3281 	.db	1
      000E09 09                    3282 	.db	9
      000E0A 00 05                 3283 	.dw	Sstm8s_clk$CLK_GetFlagStatus$585-Sstm8s_clk$CLK_GetFlagStatus$583
      000E0C 03                    3284 	.db	3
      000E0D 02                    3285 	.sleb128	2
      000E0E 01                    3286 	.db	1
      000E0F 09                    3287 	.db	9
      000E10 00 05                 3288 	.dw	Sstm8s_clk$CLK_GetFlagStatus$588-Sstm8s_clk$CLK_GetFlagStatus$585
      000E12 03                    3289 	.db	3
      000E13 02                    3290 	.sleb128	2
      000E14 01                    3291 	.db	1
      000E15 09                    3292 	.db	9
      000E16 00 05                 3293 	.dw	Sstm8s_clk$CLK_GetFlagStatus$591-Sstm8s_clk$CLK_GetFlagStatus$588
      000E18 03                    3294 	.db	3
      000E19 04                    3295 	.sleb128	4
      000E1A 01                    3296 	.db	1
      000E1B 09                    3297 	.db	9
      000E1C 00 03                 3298 	.dw	Sstm8s_clk$CLK_GetFlagStatus$593-Sstm8s_clk$CLK_GetFlagStatus$591
      000E1E 03                    3299 	.db	3
      000E1F 03                    3300 	.sleb128	3
      000E20 01                    3301 	.db	1
      000E21 09                    3302 	.db	9
      000E22 00 0A                 3303 	.dw	Sstm8s_clk$CLK_GetFlagStatus$597-Sstm8s_clk$CLK_GetFlagStatus$593
      000E24 03                    3304 	.db	3
      000E25 02                    3305 	.sleb128	2
      000E26 01                    3306 	.db	1
      000E27 09                    3307 	.db	9
      000E28 00 04                 3308 	.dw	Sstm8s_clk$CLK_GetFlagStatus$600-Sstm8s_clk$CLK_GetFlagStatus$597
      000E2A 03                    3309 	.db	3
      000E2B 04                    3310 	.sleb128	4
      000E2C 01                    3311 	.db	1
      000E2D 09                    3312 	.db	9
      000E2E 00 01                 3313 	.dw	Sstm8s_clk$CLK_GetFlagStatus$602-Sstm8s_clk$CLK_GetFlagStatus$600
      000E30 03                    3314 	.db	3
      000E31 04                    3315 	.sleb128	4
      000E32 01                    3316 	.db	1
      000E33 09                    3317 	.db	9
      000E34 00 00                 3318 	.dw	Sstm8s_clk$CLK_GetFlagStatus$603-Sstm8s_clk$CLK_GetFlagStatus$602
      000E36 03                    3319 	.db	3
      000E37 01                    3320 	.sleb128	1
      000E38 01                    3321 	.db	1
      000E39 09                    3322 	.db	9
      000E3A 00 03                 3323 	.dw	1+Sstm8s_clk$CLK_GetFlagStatus$605-Sstm8s_clk$CLK_GetFlagStatus$603
      000E3C 00                    3324 	.db	0
      000E3D 01                    3325 	.uleb128	1
      000E3E 01                    3326 	.db	1
      000E3F 00                    3327 	.db	0
      000E40 05                    3328 	.uleb128	5
      000E41 02                    3329 	.db	2
      000E42 00 00 8B 8C           3330 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$607)
      000E46 03                    3331 	.db	3
      000E47 AE 05                 3332 	.sleb128	686
      000E49 01                    3333 	.db	1
      000E4A 09                    3334 	.db	9
      000E4B 00 00                 3335 	.dw	Sstm8s_clk$CLK_GetITStatus$609-Sstm8s_clk$CLK_GetITStatus$607
      000E4D 03                    3336 	.db	3
      000E4E 05                    3337 	.sleb128	5
      000E4F 01                    3338 	.db	1
      000E50 09                    3339 	.db	9
      000E51 00 25                 3340 	.dw	Sstm8s_clk$CLK_GetITStatus$622-Sstm8s_clk$CLK_GetITStatus$609
      000E53 03                    3341 	.db	3
      000E54 02                    3342 	.sleb128	2
      000E55 01                    3343 	.db	1
      000E56 09                    3344 	.db	9
      000E57 00 03                 3345 	.dw	Sstm8s_clk$CLK_GetITStatus$624-Sstm8s_clk$CLK_GetITStatus$622
      000E59 03                    3346 	.db	3
      000E5A 03                    3347 	.sleb128	3
      000E5B 01                    3348 	.db	1
      000E5C 09                    3349 	.db	9
      000E5D 00 09                 3350 	.dw	Sstm8s_clk$CLK_GetITStatus$627-Sstm8s_clk$CLK_GetITStatus$624
      000E5F 03                    3351 	.db	3
      000E60 02                    3352 	.sleb128	2
      000E61 01                    3353 	.db	1
      000E62 09                    3354 	.db	9
      000E63 00 04                 3355 	.dw	Sstm8s_clk$CLK_GetITStatus$630-Sstm8s_clk$CLK_GetITStatus$627
      000E65 03                    3356 	.db	3
      000E66 04                    3357 	.sleb128	4
      000E67 01                    3358 	.db	1
      000E68 09                    3359 	.db	9
      000E69 00 03                 3360 	.dw	Sstm8s_clk$CLK_GetITStatus$633-Sstm8s_clk$CLK_GetITStatus$630
      000E6B 03                    3361 	.db	3
      000E6C 06                    3362 	.sleb128	6
      000E6D 01                    3363 	.db	1
      000E6E 09                    3364 	.db	9
      000E6F 00 09                 3365 	.dw	Sstm8s_clk$CLK_GetITStatus$636-Sstm8s_clk$CLK_GetITStatus$633
      000E71 03                    3366 	.db	3
      000E72 02                    3367 	.sleb128	2
      000E73 01                    3368 	.db	1
      000E74 09                    3369 	.db	9
      000E75 00 04                 3370 	.dw	Sstm8s_clk$CLK_GetITStatus$639-Sstm8s_clk$CLK_GetITStatus$636
      000E77 03                    3371 	.db	3
      000E78 04                    3372 	.sleb128	4
      000E79 01                    3373 	.db	1
      000E7A 09                    3374 	.db	9
      000E7B 00 01                 3375 	.dw	Sstm8s_clk$CLK_GetITStatus$641-Sstm8s_clk$CLK_GetITStatus$639
      000E7D 03                    3376 	.db	3
      000E7E 05                    3377 	.sleb128	5
      000E7F 01                    3378 	.db	1
      000E80 09                    3379 	.db	9
      000E81 00 00                 3380 	.dw	Sstm8s_clk$CLK_GetITStatus$642-Sstm8s_clk$CLK_GetITStatus$641
      000E83 03                    3381 	.db	3
      000E84 01                    3382 	.sleb128	1
      000E85 01                    3383 	.db	1
      000E86 09                    3384 	.db	9
      000E87 00 01                 3385 	.dw	1+Sstm8s_clk$CLK_GetITStatus$643-Sstm8s_clk$CLK_GetITStatus$642
      000E89 00                    3386 	.db	0
      000E8A 01                    3387 	.uleb128	1
      000E8B 01                    3388 	.db	1
      000E8C 00                    3389 	.db	0
      000E8D 05                    3390 	.uleb128	5
      000E8E 02                    3391 	.db	2
      000E8F 00 00 8B D3           3392 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$645)
      000E93 03                    3393 	.db	3
      000E94 D8 05                 3394 	.sleb128	728
      000E96 01                    3395 	.db	1
      000E97 09                    3396 	.db	9
      000E98 00 00                 3397 	.dw	Sstm8s_clk$CLK_ClearITPendingBit$647-Sstm8s_clk$CLK_ClearITPendingBit$645
      000E9A 03                    3398 	.db	3
      000E9B 03                    3399 	.sleb128	3
      000E9C 01                    3400 	.db	1
      000E9D 09                    3401 	.db	9
      000E9E 00 25                 3402 	.dw	Sstm8s_clk$CLK_ClearITPendingBit$660-Sstm8s_clk$CLK_ClearITPendingBit$647
      000EA0 03                    3403 	.db	3
      000EA1 02                    3404 	.sleb128	2
      000EA2 01                    3405 	.db	1
      000EA3 09                    3406 	.db	9
      000EA4 00 03                 3407 	.dw	Sstm8s_clk$CLK_ClearITPendingBit$662-Sstm8s_clk$CLK_ClearITPendingBit$660
      000EA6 03                    3408 	.db	3
      000EA7 03                    3409 	.sleb128	3
      000EA8 01                    3410 	.db	1
      000EA9 09                    3411 	.db	9
      000EAA 00 06                 3412 	.dw	Sstm8s_clk$CLK_ClearITPendingBit$665-Sstm8s_clk$CLK_ClearITPendingBit$662
      000EAC 03                    3413 	.db	3
      000EAD 05                    3414 	.sleb128	5
      000EAE 01                    3415 	.db	1
      000EAF 09                    3416 	.db	9
      000EB0 00 04                 3417 	.dw	Sstm8s_clk$CLK_ClearITPendingBit$667-Sstm8s_clk$CLK_ClearITPendingBit$665
      000EB2 03                    3418 	.db	3
      000EB3 03                    3419 	.sleb128	3
      000EB4 01                    3420 	.db	1
      000EB5 09                    3421 	.db	9
      000EB6 00 01                 3422 	.dw	1+Sstm8s_clk$CLK_ClearITPendingBit$668-Sstm8s_clk$CLK_ClearITPendingBit$667
      000EB8 00                    3423 	.db	0
      000EB9 01                    3424 	.uleb128	1
      000EBA 01                    3425 	.db	1
      000EBB                       3426 Ldebug_line_end:
                                   3427 
                                   3428 	.area .debug_loc (NOLOAD)
      000C48                       3429 Ldebug_loc_start:
      000C48 00 00 8B F8           3430 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$659)
      000C4C 00 00 8C 06           3431 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$669)
      000C50 00 02                 3432 	.dw	2
      000C52 78                    3433 	.db	120
      000C53 01                    3434 	.sleb128	1
      000C54 00 00 8B F7           3435 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$658)
      000C58 00 00 8B F8           3436 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$659)
      000C5C 00 02                 3437 	.dw	2
      000C5E 78                    3438 	.db	120
      000C5F 02                    3439 	.sleb128	2
      000C60 00 00 8B F2           3440 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$657)
      000C64 00 00 8B F7           3441 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$658)
      000C68 00 02                 3442 	.dw	2
      000C6A 78                    3443 	.db	120
      000C6B 08                    3444 	.sleb128	8
      000C6C 00 00 8B F0           3445 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$656)
      000C70 00 00 8B F2           3446 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$657)
      000C74 00 02                 3447 	.dw	2
      000C76 78                    3448 	.db	120
      000C77 07                    3449 	.sleb128	7
      000C78 00 00 8B EE           3450 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$655)
      000C7C 00 00 8B F0           3451 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$656)
      000C80 00 02                 3452 	.dw	2
      000C82 78                    3453 	.db	120
      000C83 06                    3454 	.sleb128	6
      000C84 00 00 8B EC           3455 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$654)
      000C88 00 00 8B EE           3456 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$655)
      000C8C 00 02                 3457 	.dw	2
      000C8E 78                    3458 	.db	120
      000C8F 04                    3459 	.sleb128	4
      000C90 00 00 8B EA           3460 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$653)
      000C94 00 00 8B EC           3461 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$654)
      000C98 00 02                 3462 	.dw	2
      000C9A 78                    3463 	.db	120
      000C9B 03                    3464 	.sleb128	3
      000C9C 00 00 8B E8           3465 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$652)
      000CA0 00 00 8B EA           3466 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$653)
      000CA4 00 02                 3467 	.dw	2
      000CA6 78                    3468 	.db	120
      000CA7 02                    3469 	.sleb128	2
      000CA8 00 00 8B E7           3470 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$651)
      000CAC 00 00 8B E8           3471 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$652)
      000CB0 00 02                 3472 	.dw	2
      000CB2 78                    3473 	.db	120
      000CB3 01                    3474 	.sleb128	1
      000CB4 00 00 8B E5           3475 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$650)
      000CB8 00 00 8B E7           3476 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$651)
      000CBC 00 02                 3477 	.dw	2
      000CBE 78                    3478 	.db	120
      000CBF 01                    3479 	.sleb128	1
      000CC0 00 00 8B E0           3480 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$649)
      000CC4 00 00 8B E5           3481 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$650)
      000CC8 00 02                 3482 	.dw	2
      000CCA 78                    3483 	.db	120
      000CCB 02                    3484 	.sleb128	2
      000CCC 00 00 8B DC           3485 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$648)
      000CD0 00 00 8B E0           3486 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$649)
      000CD4 00 02                 3487 	.dw	2
      000CD6 78                    3488 	.db	120
      000CD7 01                    3489 	.sleb128	1
      000CD8 00 00 8B D3           3490 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$646)
      000CDC 00 00 8B DC           3491 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$648)
      000CE0 00 02                 3492 	.dw	2
      000CE2 78                    3493 	.db	120
      000CE3 01                    3494 	.sleb128	1
      000CE4 00 00 00 00           3495 	.dw	0,0
      000CE8 00 00 00 00           3496 	.dw	0,0
      000CEC 00 00 8B CD           3497 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$634)
      000CF0 00 00 8B D3           3498 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$644)
      000CF4 00 02                 3499 	.dw	2
      000CF6 78                    3500 	.db	120
      000CF7 01                    3501 	.sleb128	1
      000CF8 00 00 8B BD           3502 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$625)
      000CFC 00 00 8B CD           3503 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$634)
      000D00 00 02                 3504 	.dw	2
      000D02 78                    3505 	.db	120
      000D03 01                    3506 	.sleb128	1
      000D04 00 00 8B B1           3507 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$621)
      000D08 00 00 8B BD           3508 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$625)
      000D0C 00 02                 3509 	.dw	2
      000D0E 78                    3510 	.db	120
      000D0F 01                    3511 	.sleb128	1
      000D10 00 00 8B B0           3512 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$620)
      000D14 00 00 8B B1           3513 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$621)
      000D18 00 02                 3514 	.dw	2
      000D1A 78                    3515 	.db	120
      000D1B 02                    3516 	.sleb128	2
      000D1C 00 00 8B AB           3517 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$619)
      000D20 00 00 8B B0           3518 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$620)
      000D24 00 02                 3519 	.dw	2
      000D26 78                    3520 	.db	120
      000D27 08                    3521 	.sleb128	8
      000D28 00 00 8B A9           3522 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$618)
      000D2C 00 00 8B AB           3523 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$619)
      000D30 00 02                 3524 	.dw	2
      000D32 78                    3525 	.db	120
      000D33 07                    3526 	.sleb128	7
      000D34 00 00 8B A7           3527 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$617)
      000D38 00 00 8B A9           3528 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$618)
      000D3C 00 02                 3529 	.dw	2
      000D3E 78                    3530 	.db	120
      000D3F 06                    3531 	.sleb128	6
      000D40 00 00 8B A5           3532 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$616)
      000D44 00 00 8B A7           3533 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$617)
      000D48 00 02                 3534 	.dw	2
      000D4A 78                    3535 	.db	120
      000D4B 04                    3536 	.sleb128	4
      000D4C 00 00 8B A3           3537 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$615)
      000D50 00 00 8B A5           3538 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$616)
      000D54 00 02                 3539 	.dw	2
      000D56 78                    3540 	.db	120
      000D57 03                    3541 	.sleb128	3
      000D58 00 00 8B A1           3542 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$614)
      000D5C 00 00 8B A3           3543 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$615)
      000D60 00 02                 3544 	.dw	2
      000D62 78                    3545 	.db	120
      000D63 02                    3546 	.sleb128	2
      000D64 00 00 8B 9D           3547 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$613)
      000D68 00 00 8B A1           3548 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$614)
      000D6C 00 02                 3549 	.dw	2
      000D6E 78                    3550 	.db	120
      000D6F 01                    3551 	.sleb128	1
      000D70 00 00 8B 9B           3552 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$612)
      000D74 00 00 8B 9D           3553 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$613)
      000D78 00 02                 3554 	.dw	2
      000D7A 78                    3555 	.db	120
      000D7B 01                    3556 	.sleb128	1
      000D7C 00 00 8B 96           3557 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$611)
      000D80 00 00 8B 9B           3558 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$612)
      000D84 00 02                 3559 	.dw	2
      000D86 78                    3560 	.db	120
      000D87 02                    3561 	.sleb128	2
      000D88 00 00 8B 95           3562 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$610)
      000D8C 00 00 8B 96           3563 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$611)
      000D90 00 02                 3564 	.dw	2
      000D92 78                    3565 	.db	120
      000D93 01                    3566 	.sleb128	1
      000D94 00 00 8B 8C           3567 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$608)
      000D98 00 00 8B 95           3568 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$610)
      000D9C 00 02                 3569 	.dw	2
      000D9E 78                    3570 	.db	120
      000D9F 01                    3571 	.sleb128	1
      000DA0 00 00 00 00           3572 	.dw	0,0
      000DA4 00 00 00 00           3573 	.dw	0,0
      000DA8 00 00 8B 8B           3574 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$604)
      000DAC 00 00 8B 8C           3575 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$606)
      000DB0 00 02                 3576 	.dw	2
      000DB2 78                    3577 	.db	120
      000DB3 01                    3578 	.sleb128	1
      000DB4 00 00 8B 80           3579 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$595)
      000DB8 00 00 8B 8B           3580 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$604)
      000DBC 00 02                 3581 	.dw	2
      000DBE 78                    3582 	.db	120
      000DBF 02                    3583 	.sleb128	2
      000DC0 00 00 8B 7B           3584 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$594)
      000DC4 00 00 8B 80           3585 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$595)
      000DC8 00 02                 3586 	.dw	2
      000DCA 78                    3587 	.db	120
      000DCB 03                    3588 	.sleb128	3
      000DCC 00 00 8B 72           3589 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$586)
      000DD0 00 00 8B 7B           3590 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$594)
      000DD4 00 02                 3591 	.dw	2
      000DD6 78                    3592 	.db	120
      000DD7 02                    3593 	.sleb128	2
      000DD8 00 00 8B 68           3594 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$581)
      000DDC 00 00 8B 72           3595 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$586)
      000DE0 00 02                 3596 	.dw	2
      000DE2 78                    3597 	.db	120
      000DE3 02                    3598 	.sleb128	2
      000DE4 00 00 8B 5E           3599 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$576)
      000DE8 00 00 8B 68           3600 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$581)
      000DEC 00 02                 3601 	.dw	2
      000DEE 78                    3602 	.db	120
      000DEF 02                    3603 	.sleb128	2
      000DF0 00 00 8B 54           3604 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$571)
      000DF4 00 00 8B 5E           3605 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$576)
      000DF8 00 02                 3606 	.dw	2
      000DFA 78                    3607 	.db	120
      000DFB 02                    3608 	.sleb128	2
      000DFC 00 00 8B 4D           3609 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$568)
      000E00 00 00 8B 54           3610 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$571)
      000E04 00 02                 3611 	.dw	2
      000E06 78                    3612 	.db	120
      000E07 02                    3613 	.sleb128	2
      000E08 00 00 8B 4C           3614 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$567)
      000E0C 00 00 8B 4D           3615 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$568)
      000E10 00 02                 3616 	.dw	2
      000E12 78                    3617 	.db	120
      000E13 04                    3618 	.sleb128	4
      000E14 00 00 8B 47           3619 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$566)
      000E18 00 00 8B 4C           3620 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$567)
      000E1C 00 02                 3621 	.dw	2
      000E1E 78                    3622 	.db	120
      000E1F 0A                    3623 	.sleb128	10
      000E20 00 00 8B 45           3624 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$565)
      000E24 00 00 8B 47           3625 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$566)
      000E28 00 02                 3626 	.dw	2
      000E2A 78                    3627 	.db	120
      000E2B 09                    3628 	.sleb128	9
      000E2C 00 00 8B 43           3629 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$564)
      000E30 00 00 8B 45           3630 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$565)
      000E34 00 02                 3631 	.dw	2
      000E36 78                    3632 	.db	120
      000E37 08                    3633 	.sleb128	8
      000E38 00 00 8B 41           3634 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$563)
      000E3C 00 00 8B 43           3635 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$564)
      000E40 00 02                 3636 	.dw	2
      000E42 78                    3637 	.db	120
      000E43 07                    3638 	.sleb128	7
      000E44 00 00 8B 3F           3639 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$562)
      000E48 00 00 8B 41           3640 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$563)
      000E4C 00 02                 3641 	.dw	2
      000E4E 78                    3642 	.db	120
      000E4F 06                    3643 	.sleb128	6
      000E50 00 00 8B 3D           3644 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$561)
      000E54 00 00 8B 3F           3645 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$562)
      000E58 00 02                 3646 	.dw	2
      000E5A 78                    3647 	.db	120
      000E5B 05                    3648 	.sleb128	5
      000E5C 00 00 8B 3B           3649 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$560)
      000E60 00 00 8B 3D           3650 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$561)
      000E64 00 02                 3651 	.dw	2
      000E66 78                    3652 	.db	120
      000E67 04                    3653 	.sleb128	4
      000E68 00 00 8B 3A           3654 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$559)
      000E6C 00 00 8B 3B           3655 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$560)
      000E70 00 02                 3656 	.dw	2
      000E72 78                    3657 	.db	120
      000E73 02                    3658 	.sleb128	2
      000E74 00 00 8B 35           3659 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$558)
      000E78 00 00 8B 3A           3660 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$559)
      000E7C 00 02                 3661 	.dw	2
      000E7E 78                    3662 	.db	120
      000E7F 02                    3663 	.sleb128	2
      000E80 00 00 8B 30           3664 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$557)
      000E84 00 00 8B 35           3665 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$558)
      000E88 00 02                 3666 	.dw	2
      000E8A 78                    3667 	.db	120
      000E8B 02                    3668 	.sleb128	2
      000E8C 00 00 8B 2B           3669 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$556)
      000E90 00 00 8B 30           3670 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$557)
      000E94 00 02                 3671 	.dw	2
      000E96 78                    3672 	.db	120
      000E97 02                    3673 	.sleb128	2
      000E98 00 00 8B 26           3674 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$555)
      000E9C 00 00 8B 2B           3675 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$556)
      000EA0 00 02                 3676 	.dw	2
      000EA2 78                    3677 	.db	120
      000EA3 02                    3678 	.sleb128	2
      000EA4 00 00 8B 21           3679 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$554)
      000EA8 00 00 8B 26           3680 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$555)
      000EAC 00 02                 3681 	.dw	2
      000EAE 78                    3682 	.db	120
      000EAF 02                    3683 	.sleb128	2
      000EB0 00 00 8B 1C           3684 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$553)
      000EB4 00 00 8B 21           3685 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$554)
      000EB8 00 02                 3686 	.dw	2
      000EBA 78                    3687 	.db	120
      000EBB 02                    3688 	.sleb128	2
      000EBC 00 00 8B 14           3689 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$552)
      000EC0 00 00 8B 1C           3690 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$553)
      000EC4 00 02                 3691 	.dw	2
      000EC6 78                    3692 	.db	120
      000EC7 02                    3693 	.sleb128	2
      000EC8 00 00 8B 0C           3694 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$551)
      000ECC 00 00 8B 14           3695 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$552)
      000ED0 00 02                 3696 	.dw	2
      000ED2 78                    3697 	.db	120
      000ED3 02                    3698 	.sleb128	2
      000ED4 00 00 8B 02           3699 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$549)
      000ED8 00 00 8B 0C           3700 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$551)
      000EDC 00 02                 3701 	.dw	2
      000EDE 78                    3702 	.db	120
      000EDF 02                    3703 	.sleb128	2
      000EE0 00 00 8B 01           3704 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$548)
      000EE4 00 00 8B 02           3705 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$549)
      000EE8 00 02                 3706 	.dw	2
      000EEA 78                    3707 	.db	120
      000EEB 01                    3708 	.sleb128	1
      000EEC 00 00 00 00           3709 	.dw	0,0
      000EF0 00 00 00 00           3710 	.dw	0,0
      000EF4 00 00 8A FC           3711 	.dw	0,(Sstm8s_clk$CLK_SYSCLKEmergencyClear$542)
      000EF8 00 00 8B 01           3712 	.dw	0,(Sstm8s_clk$CLK_SYSCLKEmergencyClear$546)
      000EFC 00 02                 3713 	.dw	2
      000EFE 78                    3714 	.db	120
      000EFF 01                    3715 	.sleb128	1
      000F00 00 00 00 00           3716 	.dw	0,0
      000F04 00 00 00 00           3717 	.dw	0,0
      000F08 00 00 8A F1           3718 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$536)
      000F0C 00 00 8A FC           3719 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$540)
      000F10 00 02                 3720 	.dw	2
      000F12 78                    3721 	.db	120
      000F13 01                    3722 	.sleb128	1
      000F14 00 00 8A EC           3723 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$535)
      000F18 00 00 8A F1           3724 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$536)
      000F1C 00 02                 3725 	.dw	2
      000F1E 78                    3726 	.db	120
      000F1F 07                    3727 	.sleb128	7
      000F20 00 00 8A EA           3728 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$534)
      000F24 00 00 8A EC           3729 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$535)
      000F28 00 02                 3730 	.dw	2
      000F2A 78                    3731 	.db	120
      000F2B 06                    3732 	.sleb128	6
      000F2C 00 00 8A E8           3733 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$533)
      000F30 00 00 8A EA           3734 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$534)
      000F34 00 02                 3735 	.dw	2
      000F36 78                    3736 	.db	120
      000F37 05                    3737 	.sleb128	5
      000F38 00 00 8A E6           3738 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$532)
      000F3C 00 00 8A E8           3739 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$533)
      000F40 00 02                 3740 	.dw	2
      000F42 78                    3741 	.db	120
      000F43 03                    3742 	.sleb128	3
      000F44 00 00 8A E4           3743 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$531)
      000F48 00 00 8A E6           3744 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$532)
      000F4C 00 02                 3745 	.dw	2
      000F4E 78                    3746 	.db	120
      000F4F 02                    3747 	.sleb128	2
      000F50 00 00 8A E2           3748 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$530)
      000F54 00 00 8A E4           3749 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$531)
      000F58 00 02                 3750 	.dw	2
      000F5A 78                    3751 	.db	120
      000F5B 01                    3752 	.sleb128	1
      000F5C 00 00 8A DC           3753 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$529)
      000F60 00 00 8A E2           3754 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$530)
      000F64 00 02                 3755 	.dw	2
      000F66 78                    3756 	.db	120
      000F67 01                    3757 	.sleb128	1
      000F68 00 00 8A D6           3758 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$528)
      000F6C 00 00 8A DC           3759 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$529)
      000F70 00 02                 3760 	.dw	2
      000F72 78                    3761 	.db	120
      000F73 01                    3762 	.sleb128	1
      000F74 00 00 8A D0           3763 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$527)
      000F78 00 00 8A D6           3764 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$528)
      000F7C 00 02                 3765 	.dw	2
      000F7E 78                    3766 	.db	120
      000F7F 01                    3767 	.sleb128	1
      000F80 00 00 8A CA           3768 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$526)
      000F84 00 00 8A D0           3769 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$527)
      000F88 00 02                 3770 	.dw	2
      000F8A 78                    3771 	.db	120
      000F8B 01                    3772 	.sleb128	1
      000F8C 00 00 8A C4           3773 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$525)
      000F90 00 00 8A CA           3774 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$526)
      000F94 00 02                 3775 	.dw	2
      000F96 78                    3776 	.db	120
      000F97 01                    3777 	.sleb128	1
      000F98 00 00 8A BE           3778 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$524)
      000F9C 00 00 8A C4           3779 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$525)
      000FA0 00 02                 3780 	.dw	2
      000FA2 78                    3781 	.db	120
      000FA3 01                    3782 	.sleb128	1
      000FA4 00 00 8A B5           3783 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$522)
      000FA8 00 00 8A BE           3784 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$524)
      000FAC 00 02                 3785 	.dw	2
      000FAE 78                    3786 	.db	120
      000FAF 01                    3787 	.sleb128	1
      000FB0 00 00 00 00           3788 	.dw	0,0
      000FB4 00 00 00 00           3789 	.dw	0,0
      000FB8 00 00 8A B4           3790 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$518)
      000FBC 00 00 8A B5           3791 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$520)
      000FC0 00 02                 3792 	.dw	2
      000FC2 78                    3793 	.db	120
      000FC3 01                    3794 	.sleb128	1
      000FC4 00 00 8A 9E           3795 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$509)
      000FC8 00 00 8A B4           3796 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$518)
      000FCC 00 02                 3797 	.dw	2
      000FCE 78                    3798 	.db	120
      000FCF 05                    3799 	.sleb128	5
      000FD0 00 00 8A 93           3800 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$507)
      000FD4 00 00 8A 9E           3801 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$509)
      000FD8 00 02                 3802 	.dw	2
      000FDA 78                    3803 	.db	120
      000FDB 05                    3804 	.sleb128	5
      000FDC 00 00 8A 8E           3805 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$506)
      000FE0 00 00 8A 93           3806 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$507)
      000FE4 00 02                 3807 	.dw	2
      000FE6 78                    3808 	.db	120
      000FE7 0D                    3809 	.sleb128	13
      000FE8 00 00 8A 8C           3810 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$505)
      000FEC 00 00 8A 8E           3811 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$506)
      000FF0 00 02                 3812 	.dw	2
      000FF2 78                    3813 	.db	120
      000FF3 0C                    3814 	.sleb128	12
      000FF4 00 00 8A 8A           3815 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$504)
      000FF8 00 00 8A 8C           3816 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$505)
      000FFC 00 02                 3817 	.dw	2
      000FFE 78                    3818 	.db	120
      000FFF 0B                    3819 	.sleb128	11
      001000 00 00 8A 88           3820 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$503)
      001004 00 00 8A 8A           3821 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$504)
      001008 00 02                 3822 	.dw	2
      00100A 78                    3823 	.db	120
      00100B 0A                    3824 	.sleb128	10
      00100C 00 00 8A 86           3825 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$502)
      001010 00 00 8A 88           3826 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$503)
      001014 00 02                 3827 	.dw	2
      001016 78                    3828 	.db	120
      001017 09                    3829 	.sleb128	9
      001018 00 00 8A 84           3830 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$501)
      00101C 00 00 8A 86           3831 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$502)
      001020 00 02                 3832 	.dw	2
      001022 78                    3833 	.db	120
      001023 07                    3834 	.sleb128	7
      001024 00 00 8A 72           3835 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$494)
      001028 00 00 8A 84           3836 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$501)
      00102C 00 02                 3837 	.dw	2
      00102E 78                    3838 	.db	120
      00102F 05                    3839 	.sleb128	5
      001030 00 00 8A 67           3840 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$491)
      001034 00 00 8A 72           3841 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$494)
      001038 00 02                 3842 	.dw	2
      00103A 78                    3843 	.db	120
      00103B 05                    3844 	.sleb128	5
      00103C 00 00 8A 65           3845 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$490)
      001040 00 00 8A 67           3846 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$491)
      001044 00 02                 3847 	.dw	2
      001046 78                    3848 	.db	120
      001047 01                    3849 	.sleb128	1
      001048 00 00 00 00           3850 	.dw	0,0
      00104C 00 00 00 00           3851 	.dw	0,0
      001050 00 00 8A 61           3852 	.dw	0,(Sstm8s_clk$CLK_GetSYSCLKSource$484)
      001054 00 00 8A 65           3853 	.dw	0,(Sstm8s_clk$CLK_GetSYSCLKSource$488)
      001058 00 02                 3854 	.dw	2
      00105A 78                    3855 	.db	120
      00105B 01                    3856 	.sleb128	1
      00105C 00 00 00 00           3857 	.dw	0,0
      001060 00 00 00 00           3858 	.dw	0,0
      001064 00 00 8A 5C           3859 	.dw	0,(Sstm8s_clk$CLK_ClockSecuritySystemEnable$478)
      001068 00 00 8A 61           3860 	.dw	0,(Sstm8s_clk$CLK_ClockSecuritySystemEnable$482)
      00106C 00 02                 3861 	.dw	2
      00106E 78                    3862 	.db	120
      00106F 01                    3863 	.sleb128	1
      001070 00 00 00 00           3864 	.dw	0,0
      001074 00 00 00 00           3865 	.dw	0,0
      001078 00 00 8A 48           3866 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$465)
      00107C 00 00 8A 5C           3867 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$476)
      001080 00 02                 3868 	.dw	2
      001082 78                    3869 	.db	120
      001083 01                    3870 	.sleb128	1
      001084 00 00 8A 43           3871 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$464)
      001088 00 00 8A 48           3872 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$465)
      00108C 00 02                 3873 	.dw	2
      00108E 78                    3874 	.db	120
      00108F 07                    3875 	.sleb128	7
      001090 00 00 8A 41           3876 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$463)
      001094 00 00 8A 43           3877 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$464)
      001098 00 02                 3878 	.dw	2
      00109A 78                    3879 	.db	120
      00109B 06                    3880 	.sleb128	6
      00109C 00 00 8A 3F           3881 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$462)
      0010A0 00 00 8A 41           3882 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$463)
      0010A4 00 02                 3883 	.dw	2
      0010A6 78                    3884 	.db	120
      0010A7 05                    3885 	.sleb128	5
      0010A8 00 00 8A 3D           3886 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$461)
      0010AC 00 00 8A 3F           3887 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$462)
      0010B0 00 02                 3888 	.dw	2
      0010B2 78                    3889 	.db	120
      0010B3 03                    3890 	.sleb128	3
      0010B4 00 00 8A 3B           3891 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$460)
      0010B8 00 00 8A 3D           3892 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$461)
      0010BC 00 02                 3893 	.dw	2
      0010BE 78                    3894 	.db	120
      0010BF 02                    3895 	.sleb128	2
      0010C0 00 00 8A 39           3896 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$459)
      0010C4 00 00 8A 3B           3897 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$460)
      0010C8 00 02                 3898 	.dw	2
      0010CA 78                    3899 	.db	120
      0010CB 01                    3900 	.sleb128	1
      0010CC 00 00 8A 30           3901 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$457)
      0010D0 00 00 8A 39           3902 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$459)
      0010D4 00 02                 3903 	.dw	2
      0010D6 78                    3904 	.db	120
      0010D7 01                    3905 	.sleb128	1
      0010D8 00 00 00 00           3906 	.dw	0,0
      0010DC 00 00 00 00           3907 	.dw	0,0
      0010E0 00 00 8A 2F           3908 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$453)
      0010E4 00 00 8A 30           3909 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$455)
      0010E8 00 02                 3910 	.dw	2
      0010EA 78                    3911 	.db	120
      0010EB 01                    3912 	.sleb128	1
      0010EC 00 00 89 FF           3913 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$441)
      0010F0 00 00 8A 2F           3914 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$453)
      0010F4 00 02                 3915 	.dw	2
      0010F6 78                    3916 	.db	120
      0010F7 02                    3917 	.sleb128	2
      0010F8 00 00 89 FA           3918 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$440)
      0010FC 00 00 89 FF           3919 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$441)
      001100 00 02                 3920 	.dw	2
      001102 78                    3921 	.db	120
      001103 08                    3922 	.sleb128	8
      001104 00 00 89 F8           3923 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$439)
      001108 00 00 89 FA           3924 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$440)
      00110C 00 02                 3925 	.dw	2
      00110E 78                    3926 	.db	120
      00110F 07                    3927 	.sleb128	7
      001110 00 00 89 F6           3928 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$438)
      001114 00 00 89 F8           3929 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$439)
      001118 00 02                 3930 	.dw	2
      00111A 78                    3931 	.db	120
      00111B 06                    3932 	.sleb128	6
      00111C 00 00 89 F4           3933 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$437)
      001120 00 00 89 F6           3934 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$438)
      001124 00 02                 3935 	.dw	2
      001126 78                    3936 	.db	120
      001127 04                    3937 	.sleb128	4
      001128 00 00 89 F2           3938 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$436)
      00112C 00 00 89 F4           3939 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$437)
      001130 00 02                 3940 	.dw	2
      001132 78                    3941 	.db	120
      001133 03                    3942 	.sleb128	3
      001134 00 00 89 F0           3943 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$435)
      001138 00 00 89 F2           3944 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$436)
      00113C 00 02                 3945 	.dw	2
      00113E 78                    3946 	.db	120
      00113F 02                    3947 	.sleb128	2
      001140 00 00 89 EA           3948 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$434)
      001144 00 00 89 F0           3949 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$435)
      001148 00 02                 3950 	.dw	2
      00114A 78                    3951 	.db	120
      00114B 02                    3952 	.sleb128	2
      00114C 00 00 89 E4           3953 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$433)
      001150 00 00 89 EA           3954 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$434)
      001154 00 02                 3955 	.dw	2
      001156 78                    3956 	.db	120
      001157 02                    3957 	.sleb128	2
      001158 00 00 89 DE           3958 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$432)
      00115C 00 00 89 E4           3959 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$433)
      001160 00 02                 3960 	.dw	2
      001162 78                    3961 	.db	120
      001163 02                    3962 	.sleb128	2
      001164 00 00 89 D8           3963 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$431)
      001168 00 00 89 DE           3964 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$432)
      00116C 00 02                 3965 	.dw	2
      00116E 78                    3966 	.db	120
      00116F 02                    3967 	.sleb128	2
      001170 00 00 89 D2           3968 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$430)
      001174 00 00 89 D8           3969 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$431)
      001178 00 02                 3970 	.dw	2
      00117A 78                    3971 	.db	120
      00117B 02                    3972 	.sleb128	2
      00117C 00 00 89 CC           3973 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$429)
      001180 00 00 89 D2           3974 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$430)
      001184 00 02                 3975 	.dw	2
      001186 78                    3976 	.db	120
      001187 02                    3977 	.sleb128	2
      001188 00 00 89 C6           3978 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$428)
      00118C 00 00 89 CC           3979 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$429)
      001190 00 02                 3980 	.dw	2
      001192 78                    3981 	.db	120
      001193 02                    3982 	.sleb128	2
      001194 00 00 89 BD           3983 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$427)
      001198 00 00 89 C6           3984 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$428)
      00119C 00 02                 3985 	.dw	2
      00119E 78                    3986 	.db	120
      00119F 02                    3987 	.sleb128	2
      0011A0 00 00 89 B4           3988 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$426)
      0011A4 00 00 89 BD           3989 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$427)
      0011A8 00 02                 3990 	.dw	2
      0011AA 78                    3991 	.db	120
      0011AB 02                    3992 	.sleb128	2
      0011AC 00 00 89 AB           3993 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$425)
      0011B0 00 00 89 B4           3994 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$426)
      0011B4 00 02                 3995 	.dw	2
      0011B6 78                    3996 	.db	120
      0011B7 02                    3997 	.sleb128	2
      0011B8 00 00 89 9B           3998 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$423)
      0011BC 00 00 89 AB           3999 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$425)
      0011C0 00 02                 4000 	.dw	2
      0011C2 78                    4001 	.db	120
      0011C3 02                    4002 	.sleb128	2
      0011C4 00 00 89 9A           4003 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$422)
      0011C8 00 00 89 9B           4004 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$423)
      0011CC 00 02                 4005 	.dw	2
      0011CE 78                    4006 	.db	120
      0011CF 01                    4007 	.sleb128	1
      0011D0 00 00 00 00           4008 	.dw	0,0
      0011D4 00 00 00 00           4009 	.dw	0,0
      0011D8 00 00 89 99           4010 	.dw	0,(Sstm8s_clk$CLK_ITConfig$418)
      0011DC 00 00 89 9A           4011 	.dw	0,(Sstm8s_clk$CLK_ITConfig$420)
      0011E0 00 02                 4012 	.dw	2
      0011E2 78                    4013 	.db	120
      0011E3 01                    4014 	.sleb128	1
      0011E4 00 00 89 70           4015 	.dw	0,(Sstm8s_clk$CLK_ITConfig$396)
      0011E8 00 00 89 99           4016 	.dw	0,(Sstm8s_clk$CLK_ITConfig$418)
      0011EC 00 02                 4017 	.dw	2
      0011EE 78                    4018 	.db	120
      0011EF 02                    4019 	.sleb128	2
      0011F0 00 00 89 6F           4020 	.dw	0,(Sstm8s_clk$CLK_ITConfig$395)
      0011F4 00 00 89 70           4021 	.dw	0,(Sstm8s_clk$CLK_ITConfig$396)
      0011F8 00 02                 4022 	.dw	2
      0011FA 78                    4023 	.db	120
      0011FB 03                    4024 	.sleb128	3
      0011FC 00 00 89 6A           4025 	.dw	0,(Sstm8s_clk$CLK_ITConfig$394)
      001200 00 00 89 6F           4026 	.dw	0,(Sstm8s_clk$CLK_ITConfig$395)
      001204 00 02                 4027 	.dw	2
      001206 78                    4028 	.db	120
      001207 09                    4029 	.sleb128	9
      001208 00 00 89 68           4030 	.dw	0,(Sstm8s_clk$CLK_ITConfig$393)
      00120C 00 00 89 6A           4031 	.dw	0,(Sstm8s_clk$CLK_ITConfig$394)
      001210 00 02                 4032 	.dw	2
      001212 78                    4033 	.db	120
      001213 08                    4034 	.sleb128	8
      001214 00 00 89 66           4035 	.dw	0,(Sstm8s_clk$CLK_ITConfig$392)
      001218 00 00 89 68           4036 	.dw	0,(Sstm8s_clk$CLK_ITConfig$393)
      00121C 00 02                 4037 	.dw	2
      00121E 78                    4038 	.db	120
      00121F 07                    4039 	.sleb128	7
      001220 00 00 89 64           4040 	.dw	0,(Sstm8s_clk$CLK_ITConfig$391)
      001224 00 00 89 66           4041 	.dw	0,(Sstm8s_clk$CLK_ITConfig$392)
      001228 00 02                 4042 	.dw	2
      00122A 78                    4043 	.db	120
      00122B 05                    4044 	.sleb128	5
      00122C 00 00 89 62           4045 	.dw	0,(Sstm8s_clk$CLK_ITConfig$390)
      001230 00 00 89 64           4046 	.dw	0,(Sstm8s_clk$CLK_ITConfig$391)
      001234 00 02                 4047 	.dw	2
      001236 78                    4048 	.db	120
      001237 04                    4049 	.sleb128	4
      001238 00 00 89 60           4050 	.dw	0,(Sstm8s_clk$CLK_ITConfig$389)
      00123C 00 00 89 62           4051 	.dw	0,(Sstm8s_clk$CLK_ITConfig$390)
      001240 00 02                 4052 	.dw	2
      001242 78                    4053 	.db	120
      001243 03                    4054 	.sleb128	3
      001244 00 00 89 58           4055 	.dw	0,(Sstm8s_clk$CLK_ITConfig$388)
      001248 00 00 89 60           4056 	.dw	0,(Sstm8s_clk$CLK_ITConfig$389)
      00124C 00 02                 4057 	.dw	2
      00124E 78                    4058 	.db	120
      00124F 02                    4059 	.sleb128	2
      001250 00 00 89 55           4060 	.dw	0,(Sstm8s_clk$CLK_ITConfig$387)
      001254 00 00 89 58           4061 	.dw	0,(Sstm8s_clk$CLK_ITConfig$388)
      001258 00 02                 4062 	.dw	2
      00125A 78                    4063 	.db	120
      00125B 02                    4064 	.sleb128	2
      00125C 00 00 89 50           4065 	.dw	0,(Sstm8s_clk$CLK_ITConfig$386)
      001260 00 00 89 55           4066 	.dw	0,(Sstm8s_clk$CLK_ITConfig$387)
      001264 00 02                 4067 	.dw	2
      001266 78                    4068 	.db	120
      001267 03                    4069 	.sleb128	3
      001268 00 00 89 4D           4070 	.dw	0,(Sstm8s_clk$CLK_ITConfig$385)
      00126C 00 00 89 50           4071 	.dw	0,(Sstm8s_clk$CLK_ITConfig$386)
      001270 00 02                 4072 	.dw	2
      001272 78                    4073 	.db	120
      001273 02                    4074 	.sleb128	2
      001274 00 00 89 48           4075 	.dw	0,(Sstm8s_clk$CLK_ITConfig$384)
      001278 00 00 89 4D           4076 	.dw	0,(Sstm8s_clk$CLK_ITConfig$385)
      00127C 00 02                 4077 	.dw	2
      00127E 78                    4078 	.db	120
      00127F 03                    4079 	.sleb128	3
      001280 00 00 89 47           4080 	.dw	0,(Sstm8s_clk$CLK_ITConfig$383)
      001284 00 00 89 48           4081 	.dw	0,(Sstm8s_clk$CLK_ITConfig$384)
      001288 00 02                 4082 	.dw	2
      00128A 78                    4083 	.db	120
      00128B 02                    4084 	.sleb128	2
      00128C 00 00 89 3E           4085 	.dw	0,(Sstm8s_clk$CLK_ITConfig$381)
      001290 00 00 89 47           4086 	.dw	0,(Sstm8s_clk$CLK_ITConfig$383)
      001294 00 02                 4087 	.dw	2
      001296 78                    4088 	.db	120
      001297 02                    4089 	.sleb128	2
      001298 00 00 89 39           4090 	.dw	0,(Sstm8s_clk$CLK_ITConfig$380)
      00129C 00 00 89 3E           4091 	.dw	0,(Sstm8s_clk$CLK_ITConfig$381)
      0012A0 00 02                 4092 	.dw	2
      0012A2 78                    4093 	.db	120
      0012A3 08                    4094 	.sleb128	8
      0012A4 00 00 89 37           4095 	.dw	0,(Sstm8s_clk$CLK_ITConfig$379)
      0012A8 00 00 89 39           4096 	.dw	0,(Sstm8s_clk$CLK_ITConfig$380)
      0012AC 00 02                 4097 	.dw	2
      0012AE 78                    4098 	.db	120
      0012AF 07                    4099 	.sleb128	7
      0012B0 00 00 89 35           4100 	.dw	0,(Sstm8s_clk$CLK_ITConfig$378)
      0012B4 00 00 89 37           4101 	.dw	0,(Sstm8s_clk$CLK_ITConfig$379)
      0012B8 00 02                 4102 	.dw	2
      0012BA 78                    4103 	.db	120
      0012BB 06                    4104 	.sleb128	6
      0012BC 00 00 89 33           4105 	.dw	0,(Sstm8s_clk$CLK_ITConfig$377)
      0012C0 00 00 89 35           4106 	.dw	0,(Sstm8s_clk$CLK_ITConfig$378)
      0012C4 00 02                 4107 	.dw	2
      0012C6 78                    4108 	.db	120
      0012C7 04                    4109 	.sleb128	4
      0012C8 00 00 89 31           4110 	.dw	0,(Sstm8s_clk$CLK_ITConfig$376)
      0012CC 00 00 89 33           4111 	.dw	0,(Sstm8s_clk$CLK_ITConfig$377)
      0012D0 00 02                 4112 	.dw	2
      0012D2 78                    4113 	.db	120
      0012D3 03                    4114 	.sleb128	3
      0012D4 00 00 89 2F           4115 	.dw	0,(Sstm8s_clk$CLK_ITConfig$375)
      0012D8 00 00 89 31           4116 	.dw	0,(Sstm8s_clk$CLK_ITConfig$376)
      0012DC 00 02                 4117 	.dw	2
      0012DE 78                    4118 	.db	120
      0012DF 02                    4119 	.sleb128	2
      0012E0 00 00 89 26           4120 	.dw	0,(Sstm8s_clk$CLK_ITConfig$373)
      0012E4 00 00 89 2F           4121 	.dw	0,(Sstm8s_clk$CLK_ITConfig$375)
      0012E8 00 02                 4122 	.dw	2
      0012EA 78                    4123 	.db	120
      0012EB 02                    4124 	.sleb128	2
      0012EC 00 00 89 25           4125 	.dw	0,(Sstm8s_clk$CLK_ITConfig$372)
      0012F0 00 00 89 26           4126 	.dw	0,(Sstm8s_clk$CLK_ITConfig$373)
      0012F4 00 02                 4127 	.dw	2
      0012F6 78                    4128 	.db	120
      0012F7 01                    4129 	.sleb128	1
      0012F8 00 00 00 00           4130 	.dw	0,0
      0012FC 00 00 00 00           4131 	.dw	0,0
      001300 00 00 89 10           4132 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$364)
      001304 00 00 89 25           4133 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$370)
      001308 00 02                 4134 	.dw	2
      00130A 78                    4135 	.db	120
      00130B 01                    4136 	.sleb128	1
      00130C 00 00 89 0B           4137 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$363)
      001310 00 00 89 10           4138 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$364)
      001314 00 02                 4139 	.dw	2
      001316 78                    4140 	.db	120
      001317 07                    4141 	.sleb128	7
      001318 00 00 89 09           4142 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$362)
      00131C 00 00 89 0B           4143 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$363)
      001320 00 02                 4144 	.dw	2
      001322 78                    4145 	.db	120
      001323 06                    4146 	.sleb128	6
      001324 00 00 89 07           4147 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$361)
      001328 00 00 89 09           4148 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$362)
      00132C 00 02                 4149 	.dw	2
      00132E 78                    4150 	.db	120
      00132F 05                    4151 	.sleb128	5
      001330 00 00 89 05           4152 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$360)
      001334 00 00 89 07           4153 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$361)
      001338 00 02                 4154 	.dw	2
      00133A 78                    4155 	.db	120
      00133B 03                    4156 	.sleb128	3
      00133C 00 00 89 03           4157 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$359)
      001340 00 00 89 05           4158 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$360)
      001344 00 02                 4159 	.dw	2
      001346 78                    4160 	.db	120
      001347 02                    4161 	.sleb128	2
      001348 00 00 89 01           4162 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$358)
      00134C 00 00 89 03           4163 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$359)
      001350 00 02                 4164 	.dw	2
      001352 78                    4165 	.db	120
      001353 01                    4166 	.sleb128	1
      001354 00 00 88 FB           4167 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$357)
      001358 00 00 89 01           4168 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$358)
      00135C 00 02                 4169 	.dw	2
      00135E 78                    4170 	.db	120
      00135F 01                    4171 	.sleb128	1
      001360 00 00 88 F5           4172 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$356)
      001364 00 00 88 FB           4173 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$357)
      001368 00 02                 4174 	.dw	2
      00136A 78                    4175 	.db	120
      00136B 01                    4176 	.sleb128	1
      00136C 00 00 88 EF           4177 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$355)
      001370 00 00 88 F5           4178 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$356)
      001374 00 02                 4179 	.dw	2
      001376 78                    4180 	.db	120
      001377 01                    4181 	.sleb128	1
      001378 00 00 88 E9           4182 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$354)
      00137C 00 00 88 EF           4183 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$355)
      001380 00 02                 4184 	.dw	2
      001382 78                    4185 	.db	120
      001383 01                    4186 	.sleb128	1
      001384 00 00 88 E3           4187 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$353)
      001388 00 00 88 E9           4188 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$354)
      00138C 00 02                 4189 	.dw	2
      00138E 78                    4190 	.db	120
      00138F 01                    4191 	.sleb128	1
      001390 00 00 88 DD           4192 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$352)
      001394 00 00 88 E3           4193 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$353)
      001398 00 02                 4194 	.dw	2
      00139A 78                    4195 	.db	120
      00139B 01                    4196 	.sleb128	1
      00139C 00 00 88 D7           4197 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$351)
      0013A0 00 00 88 DD           4198 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$352)
      0013A4 00 02                 4199 	.dw	2
      0013A6 78                    4200 	.db	120
      0013A7 01                    4201 	.sleb128	1
      0013A8 00 00 88 CE           4202 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$350)
      0013AC 00 00 88 D7           4203 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$351)
      0013B0 00 02                 4204 	.dw	2
      0013B2 78                    4205 	.db	120
      0013B3 01                    4206 	.sleb128	1
      0013B4 00 00 88 C5           4207 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$349)
      0013B8 00 00 88 CE           4208 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$350)
      0013BC 00 02                 4209 	.dw	2
      0013BE 78                    4210 	.db	120
      0013BF 01                    4211 	.sleb128	1
      0013C0 00 00 88 BC           4212 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$348)
      0013C4 00 00 88 C5           4213 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$349)
      0013C8 00 02                 4214 	.dw	2
      0013CA 78                    4215 	.db	120
      0013CB 01                    4216 	.sleb128	1
      0013CC 00 00 88 B3           4217 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$347)
      0013D0 00 00 88 BC           4218 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$348)
      0013D4 00 02                 4219 	.dw	2
      0013D6 78                    4220 	.db	120
      0013D7 01                    4221 	.sleb128	1
      0013D8 00 00 88 A3           4222 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$345)
      0013DC 00 00 88 B3           4223 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$347)
      0013E0 00 02                 4224 	.dw	2
      0013E2 78                    4225 	.db	120
      0013E3 01                    4226 	.sleb128	1
      0013E4 00 00 00 00           4227 	.dw	0,0
      0013E8 00 00 00 00           4228 	.dw	0,0
      0013EC 00 00 88 92           4229 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$338)
      0013F0 00 00 88 A3           4230 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$343)
      0013F4 00 02                 4231 	.dw	2
      0013F6 78                    4232 	.db	120
      0013F7 01                    4233 	.sleb128	1
      0013F8 00 00 88 8D           4234 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$337)
      0013FC 00 00 88 92           4235 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$338)
      001400 00 02                 4236 	.dw	2
      001402 78                    4237 	.db	120
      001403 07                    4238 	.sleb128	7
      001404 00 00 88 8B           4239 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$336)
      001408 00 00 88 8D           4240 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$337)
      00140C 00 02                 4241 	.dw	2
      00140E 78                    4242 	.db	120
      00140F 06                    4243 	.sleb128	6
      001410 00 00 88 89           4244 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$335)
      001414 00 00 88 8B           4245 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$336)
      001418 00 02                 4246 	.dw	2
      00141A 78                    4247 	.db	120
      00141B 05                    4248 	.sleb128	5
      00141C 00 00 88 87           4249 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$334)
      001420 00 00 88 89           4250 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$335)
      001424 00 02                 4251 	.dw	2
      001426 78                    4252 	.db	120
      001427 03                    4253 	.sleb128	3
      001428 00 00 88 85           4254 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$333)
      00142C 00 00 88 87           4255 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$334)
      001430 00 02                 4256 	.dw	2
      001432 78                    4257 	.db	120
      001433 02                    4258 	.sleb128	2
      001434 00 00 88 83           4259 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$332)
      001438 00 00 88 85           4260 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$333)
      00143C 00 02                 4261 	.dw	2
      00143E 78                    4262 	.db	120
      00143F 01                    4263 	.sleb128	1
      001440 00 00 88 7D           4264 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$331)
      001444 00 00 88 83           4265 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$332)
      001448 00 02                 4266 	.dw	2
      00144A 78                    4267 	.db	120
      00144B 01                    4268 	.sleb128	1
      00144C 00 00 88 77           4269 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$330)
      001450 00 00 88 7D           4270 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$331)
      001454 00 02                 4271 	.dw	2
      001456 78                    4272 	.db	120
      001457 01                    4273 	.sleb128	1
      001458 00 00 88 6D           4274 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$328)
      00145C 00 00 88 77           4275 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$330)
      001460 00 02                 4276 	.dw	2
      001462 78                    4277 	.db	120
      001463 01                    4278 	.sleb128	1
      001464 00 00 00 00           4279 	.dw	0,0
      001468 00 00 00 00           4280 	.dw	0,0
      00146C 00 00 88 6C           4281 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$324)
      001470 00 00 88 6D           4282 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$326)
      001474 00 02                 4283 	.dw	2
      001476 78                    4284 	.db	120
      001477 01                    4285 	.sleb128	1
      001478 00 00 88 65           4286 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$318)
      00147C 00 00 88 6C           4287 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$324)
      001480 00 02                 4288 	.dw	2
      001482 78                    4289 	.db	120
      001483 02                    4290 	.sleb128	2
      001484 00 00 88 55           4291 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$313)
      001488 00 00 88 65           4292 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$318)
      00148C 00 02                 4293 	.dw	2
      00148E 78                    4294 	.db	120
      00148F 02                    4295 	.sleb128	2
      001490 00 00 88 45           4296 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$308)
      001494 00 00 88 55           4297 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$313)
      001498 00 02                 4298 	.dw	2
      00149A 78                    4299 	.db	120
      00149B 02                    4300 	.sleb128	2
      00149C 00 00 87 B6           4301 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$256)
      0014A0 00 00 88 45           4302 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$308)
      0014A4 00 02                 4303 	.dw	2
      0014A6 78                    4304 	.db	120
      0014A7 02                    4305 	.sleb128	2
      0014A8 00 00 87 B1           4306 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$255)
      0014AC 00 00 87 B6           4307 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$256)
      0014B0 00 02                 4308 	.dw	2
      0014B2 78                    4309 	.db	120
      0014B3 08                    4310 	.sleb128	8
      0014B4 00 00 87 AF           4311 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$254)
      0014B8 00 00 87 B1           4312 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$255)
      0014BC 00 02                 4313 	.dw	2
      0014BE 78                    4314 	.db	120
      0014BF 07                    4315 	.sleb128	7
      0014C0 00 00 87 AD           4316 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$253)
      0014C4 00 00 87 AF           4317 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$254)
      0014C8 00 02                 4318 	.dw	2
      0014CA 78                    4319 	.db	120
      0014CB 06                    4320 	.sleb128	6
      0014CC 00 00 87 AB           4321 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$252)
      0014D0 00 00 87 AD           4322 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$253)
      0014D4 00 02                 4323 	.dw	2
      0014D6 78                    4324 	.db	120
      0014D7 04                    4325 	.sleb128	4
      0014D8 00 00 87 A9           4326 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$251)
      0014DC 00 00 87 AB           4327 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$252)
      0014E0 00 02                 4328 	.dw	2
      0014E2 78                    4329 	.db	120
      0014E3 03                    4330 	.sleb128	3
      0014E4 00 00 87 A7           4331 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$250)
      0014E8 00 00 87 A9           4332 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$251)
      0014EC 00 02                 4333 	.dw	2
      0014EE 78                    4334 	.db	120
      0014EF 02                    4335 	.sleb128	2
      0014F0 00 00 87 9E           4336 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$248)
      0014F4 00 00 87 A7           4337 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$250)
      0014F8 00 02                 4338 	.dw	2
      0014FA 78                    4339 	.db	120
      0014FB 02                    4340 	.sleb128	2
      0014FC 00 00 87 99           4341 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$247)
      001500 00 00 87 9E           4342 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$248)
      001504 00 02                 4343 	.dw	2
      001506 78                    4344 	.db	120
      001507 08                    4345 	.sleb128	8
      001508 00 00 87 97           4346 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$246)
      00150C 00 00 87 99           4347 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$247)
      001510 00 02                 4348 	.dw	2
      001512 78                    4349 	.db	120
      001513 07                    4350 	.sleb128	7
      001514 00 00 87 95           4351 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$245)
      001518 00 00 87 97           4352 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$246)
      00151C 00 02                 4353 	.dw	2
      00151E 78                    4354 	.db	120
      00151F 06                    4355 	.sleb128	6
      001520 00 00 87 93           4356 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$244)
      001524 00 00 87 95           4357 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$245)
      001528 00 02                 4358 	.dw	2
      00152A 78                    4359 	.db	120
      00152B 04                    4360 	.sleb128	4
      00152C 00 00 87 91           4361 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$243)
      001530 00 00 87 93           4362 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$244)
      001534 00 02                 4363 	.dw	2
      001536 78                    4364 	.db	120
      001537 03                    4365 	.sleb128	3
      001538 00 00 87 8F           4366 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$242)
      00153C 00 00 87 91           4367 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$243)
      001540 00 02                 4368 	.dw	2
      001542 78                    4369 	.db	120
      001543 02                    4370 	.sleb128	2
      001544 00 00 87 86           4371 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$240)
      001548 00 00 87 8F           4372 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$242)
      00154C 00 02                 4373 	.dw	2
      00154E 78                    4374 	.db	120
      00154F 02                    4375 	.sleb128	2
      001550 00 00 87 81           4376 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$239)
      001554 00 00 87 86           4377 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$240)
      001558 00 02                 4378 	.dw	2
      00155A 78                    4379 	.db	120
      00155B 08                    4380 	.sleb128	8
      00155C 00 00 87 7F           4381 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$238)
      001560 00 00 87 81           4382 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$239)
      001564 00 02                 4383 	.dw	2
      001566 78                    4384 	.db	120
      001567 07                    4385 	.sleb128	7
      001568 00 00 87 7D           4386 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$237)
      00156C 00 00 87 7F           4387 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$238)
      001570 00 02                 4388 	.dw	2
      001572 78                    4389 	.db	120
      001573 06                    4390 	.sleb128	6
      001574 00 00 87 7B           4391 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$236)
      001578 00 00 87 7D           4392 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$237)
      00157C 00 02                 4393 	.dw	2
      00157E 78                    4394 	.db	120
      00157F 04                    4395 	.sleb128	4
      001580 00 00 87 79           4396 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$235)
      001584 00 00 87 7B           4397 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$236)
      001588 00 02                 4398 	.dw	2
      00158A 78                    4399 	.db	120
      00158B 03                    4400 	.sleb128	3
      00158C 00 00 87 6F           4401 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$234)
      001590 00 00 87 79           4402 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$235)
      001594 00 02                 4403 	.dw	2
      001596 78                    4404 	.db	120
      001597 02                    4405 	.sleb128	2
      001598 00 00 87 63           4406 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$232)
      00159C 00 00 87 6F           4407 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$234)
      0015A0 00 02                 4408 	.dw	2
      0015A2 78                    4409 	.db	120
      0015A3 02                    4410 	.sleb128	2
      0015A4 00 00 87 5E           4411 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$231)
      0015A8 00 00 87 63           4412 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$232)
      0015AC 00 02                 4413 	.dw	2
      0015AE 78                    4414 	.db	120
      0015AF 08                    4415 	.sleb128	8
      0015B0 00 00 87 5C           4416 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$230)
      0015B4 00 00 87 5E           4417 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$231)
      0015B8 00 02                 4418 	.dw	2
      0015BA 78                    4419 	.db	120
      0015BB 07                    4420 	.sleb128	7
      0015BC 00 00 87 5A           4421 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$229)
      0015C0 00 00 87 5C           4422 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$230)
      0015C4 00 02                 4423 	.dw	2
      0015C6 78                    4424 	.db	120
      0015C7 06                    4425 	.sleb128	6
      0015C8 00 00 87 58           4426 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$228)
      0015CC 00 00 87 5A           4427 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$229)
      0015D0 00 02                 4428 	.dw	2
      0015D2 78                    4429 	.db	120
      0015D3 04                    4430 	.sleb128	4
      0015D4 00 00 87 56           4431 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$227)
      0015D8 00 00 87 58           4432 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$228)
      0015DC 00 02                 4433 	.dw	2
      0015DE 78                    4434 	.db	120
      0015DF 03                    4435 	.sleb128	3
      0015E0 00 00 87 54           4436 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$226)
      0015E4 00 00 87 56           4437 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$227)
      0015E8 00 02                 4438 	.dw	2
      0015EA 78                    4439 	.db	120
      0015EB 02                    4440 	.sleb128	2
      0015EC 00 00 87 4E           4441 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$225)
      0015F0 00 00 87 54           4442 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$226)
      0015F4 00 02                 4443 	.dw	2
      0015F6 78                    4444 	.db	120
      0015F7 02                    4445 	.sleb128	2
      0015F8 00 00 87 48           4446 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$224)
      0015FC 00 00 87 4E           4447 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$225)
      001600 00 02                 4448 	.dw	2
      001602 78                    4449 	.db	120
      001603 02                    4450 	.sleb128	2
      001604 00 00 87 42           4451 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$222)
      001608 00 00 87 48           4452 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$224)
      00160C 00 02                 4453 	.dw	2
      00160E 78                    4454 	.db	120
      00160F 02                    4455 	.sleb128	2
      001610 00 00 87 41           4456 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$221)
      001614 00 00 87 42           4457 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$222)
      001618 00 02                 4458 	.dw	2
      00161A 78                    4459 	.db	120
      00161B 01                    4460 	.sleb128	1
      00161C 00 00 00 00           4461 	.dw	0,0
      001620 00 00 00 00           4462 	.dw	0,0
      001624 00 00 87 40           4463 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$217)
      001628 00 00 87 41           4464 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$219)
      00162C 00 02                 4465 	.dw	2
      00162E 78                    4466 	.db	120
      00162F 01                    4467 	.sleb128	1
      001630 00 00 87 04           4468 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$195)
      001634 00 00 87 40           4469 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$217)
      001638 00 02                 4470 	.dw	2
      00163A 78                    4471 	.db	120
      00163B 03                    4472 	.sleb128	3
      00163C 00 00 86 FF           4473 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$194)
      001640 00 00 87 04           4474 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$195)
      001644 00 02                 4475 	.dw	2
      001646 78                    4476 	.db	120
      001647 04                    4477 	.sleb128	4
      001648 00 00 86 FA           4478 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$192)
      00164C 00 00 86 FF           4479 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$194)
      001650 00 02                 4480 	.dw	2
      001652 78                    4481 	.db	120
      001653 03                    4482 	.sleb128	3
      001654 00 00 86 F5           4483 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$191)
      001658 00 00 86 FA           4484 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$192)
      00165C 00 02                 4485 	.dw	2
      00165E 78                    4486 	.db	120
      00165F 09                    4487 	.sleb128	9
      001660 00 00 86 F3           4488 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$190)
      001664 00 00 86 F5           4489 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$191)
      001668 00 02                 4490 	.dw	2
      00166A 78                    4491 	.db	120
      00166B 08                    4492 	.sleb128	8
      00166C 00 00 86 F1           4493 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$189)
      001670 00 00 86 F3           4494 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$190)
      001674 00 02                 4495 	.dw	2
      001676 78                    4496 	.db	120
      001677 07                    4497 	.sleb128	7
      001678 00 00 86 EF           4498 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$188)
      00167C 00 00 86 F1           4499 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$189)
      001680 00 02                 4500 	.dw	2
      001682 78                    4501 	.db	120
      001683 05                    4502 	.sleb128	5
      001684 00 00 86 ED           4503 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$187)
      001688 00 00 86 EF           4504 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$188)
      00168C 00 02                 4505 	.dw	2
      00168E 78                    4506 	.db	120
      00168F 04                    4507 	.sleb128	4
      001690 00 00 86 EB           4508 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$186)
      001694 00 00 86 ED           4509 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$187)
      001698 00 02                 4510 	.dw	2
      00169A 78                    4511 	.db	120
      00169B 03                    4512 	.sleb128	3
      00169C 00 00 86 E5           4513 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$185)
      0016A0 00 00 86 EB           4514 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$186)
      0016A4 00 02                 4515 	.dw	2
      0016A6 78                    4516 	.db	120
      0016A7 03                    4517 	.sleb128	3
      0016A8 00 00 86 DF           4518 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$184)
      0016AC 00 00 86 E5           4519 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$185)
      0016B0 00 02                 4520 	.dw	2
      0016B2 78                    4521 	.db	120
      0016B3 03                    4522 	.sleb128	3
      0016B4 00 00 86 D9           4523 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$183)
      0016B8 00 00 86 DF           4524 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$184)
      0016BC 00 02                 4525 	.dw	2
      0016BE 78                    4526 	.db	120
      0016BF 03                    4527 	.sleb128	3
      0016C0 00 00 86 D3           4528 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$182)
      0016C4 00 00 86 D9           4529 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$183)
      0016C8 00 02                 4530 	.dw	2
      0016CA 78                    4531 	.db	120
      0016CB 03                    4532 	.sleb128	3
      0016CC 00 00 86 C3           4533 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$181)
      0016D0 00 00 86 D3           4534 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$182)
      0016D4 00 02                 4535 	.dw	2
      0016D6 78                    4536 	.db	120
      0016D7 03                    4537 	.sleb128	3
      0016D8 00 00 86 B3           4538 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$180)
      0016DC 00 00 86 C3           4539 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$181)
      0016E0 00 02                 4540 	.dw	2
      0016E2 78                    4541 	.db	120
      0016E3 03                    4542 	.sleb128	3
      0016E4 00 00 86 A7           4543 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$179)
      0016E8 00 00 86 B3           4544 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$180)
      0016EC 00 02                 4545 	.dw	2
      0016EE 78                    4546 	.db	120
      0016EF 03                    4547 	.sleb128	3
      0016F0 00 00 86 92           4548 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$178)
      0016F4 00 00 86 A7           4549 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$179)
      0016F8 00 02                 4550 	.dw	2
      0016FA 78                    4551 	.db	120
      0016FB 03                    4552 	.sleb128	3
      0016FC 00 00 86 89           4553 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$177)
      001700 00 00 86 92           4554 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$178)
      001704 00 02                 4555 	.dw	2
      001706 78                    4556 	.db	120
      001707 03                    4557 	.sleb128	3
      001708 00 00 86 7A           4558 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$175)
      00170C 00 00 86 89           4559 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$177)
      001710 00 02                 4560 	.dw	2
      001712 78                    4561 	.db	120
      001713 03                    4562 	.sleb128	3
      001714 00 00 86 75           4563 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$174)
      001718 00 00 86 7A           4564 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$175)
      00171C 00 02                 4565 	.dw	2
      00171E 78                    4566 	.db	120
      00171F 09                    4567 	.sleb128	9
      001720 00 00 86 73           4568 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$173)
      001724 00 00 86 75           4569 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$174)
      001728 00 02                 4570 	.dw	2
      00172A 78                    4571 	.db	120
      00172B 08                    4572 	.sleb128	8
      00172C 00 00 86 71           4573 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$172)
      001730 00 00 86 73           4574 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$173)
      001734 00 02                 4575 	.dw	2
      001736 78                    4576 	.db	120
      001737 07                    4577 	.sleb128	7
      001738 00 00 86 6F           4578 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$171)
      00173C 00 00 86 71           4579 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$172)
      001740 00 02                 4580 	.dw	2
      001742 78                    4581 	.db	120
      001743 05                    4582 	.sleb128	5
      001744 00 00 86 6D           4583 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$170)
      001748 00 00 86 6F           4584 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$171)
      00174C 00 02                 4585 	.dw	2
      00174E 78                    4586 	.db	120
      00174F 04                    4587 	.sleb128	4
      001750 00 00 86 6B           4588 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$169)
      001754 00 00 86 6D           4589 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$170)
      001758 00 02                 4590 	.dw	2
      00175A 78                    4591 	.db	120
      00175B 03                    4592 	.sleb128	3
      00175C 00 00 86 62           4593 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$167)
      001760 00 00 86 6B           4594 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$169)
      001764 00 02                 4595 	.dw	2
      001766 78                    4596 	.db	120
      001767 03                    4597 	.sleb128	3
      001768 00 00 86 61           4598 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$166)
      00176C 00 00 86 62           4599 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$167)
      001770 00 02                 4600 	.dw	2
      001772 78                    4601 	.db	120
      001773 01                    4602 	.sleb128	1
      001774 00 00 00 00           4603 	.dw	0,0
      001778 00 00 00 00           4604 	.dw	0,0
      00177C 00 00 86 4D           4605 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$153)
      001780 00 00 86 61           4606 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$164)
      001784 00 02                 4607 	.dw	2
      001786 78                    4608 	.db	120
      001787 01                    4609 	.sleb128	1
      001788 00 00 86 48           4610 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$152)
      00178C 00 00 86 4D           4611 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$153)
      001790 00 02                 4612 	.dw	2
      001792 78                    4613 	.db	120
      001793 07                    4614 	.sleb128	7
      001794 00 00 86 46           4615 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$151)
      001798 00 00 86 48           4616 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$152)
      00179C 00 02                 4617 	.dw	2
      00179E 78                    4618 	.db	120
      00179F 06                    4619 	.sleb128	6
      0017A0 00 00 86 44           4620 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$150)
      0017A4 00 00 86 46           4621 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$151)
      0017A8 00 02                 4622 	.dw	2
      0017AA 78                    4623 	.db	120
      0017AB 05                    4624 	.sleb128	5
      0017AC 00 00 86 42           4625 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$149)
      0017B0 00 00 86 44           4626 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$150)
      0017B4 00 02                 4627 	.dw	2
      0017B6 78                    4628 	.db	120
      0017B7 04                    4629 	.sleb128	4
      0017B8 00 00 86 40           4630 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$148)
      0017BC 00 00 86 42           4631 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$149)
      0017C0 00 02                 4632 	.dw	2
      0017C2 78                    4633 	.db	120
      0017C3 02                    4634 	.sleb128	2
      0017C4 00 00 86 3E           4635 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$147)
      0017C8 00 00 86 40           4636 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$148)
      0017CC 00 02                 4637 	.dw	2
      0017CE 78                    4638 	.db	120
      0017CF 01                    4639 	.sleb128	1
      0017D0 00 00 86 35           4640 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$145)
      0017D4 00 00 86 3E           4641 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$147)
      0017D8 00 02                 4642 	.dw	2
      0017DA 78                    4643 	.db	120
      0017DB 01                    4644 	.sleb128	1
      0017DC 00 00 00 00           4645 	.dw	0,0
      0017E0 00 00 00 00           4646 	.dw	0,0
      0017E4 00 00 86 21           4647 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$132)
      0017E8 00 00 86 35           4648 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$143)
      0017EC 00 02                 4649 	.dw	2
      0017EE 78                    4650 	.db	120
      0017EF 01                    4651 	.sleb128	1
      0017F0 00 00 86 1C           4652 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$131)
      0017F4 00 00 86 21           4653 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$132)
      0017F8 00 02                 4654 	.dw	2
      0017FA 78                    4655 	.db	120
      0017FB 07                    4656 	.sleb128	7
      0017FC 00 00 86 1A           4657 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$130)
      001800 00 00 86 1C           4658 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$131)
      001804 00 02                 4659 	.dw	2
      001806 78                    4660 	.db	120
      001807 06                    4661 	.sleb128	6
      001808 00 00 86 18           4662 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$129)
      00180C 00 00 86 1A           4663 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$130)
      001810 00 02                 4664 	.dw	2
      001812 78                    4665 	.db	120
      001813 05                    4666 	.sleb128	5
      001814 00 00 86 16           4667 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$128)
      001818 00 00 86 18           4668 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$129)
      00181C 00 02                 4669 	.dw	2
      00181E 78                    4670 	.db	120
      00181F 04                    4671 	.sleb128	4
      001820 00 00 86 14           4672 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$127)
      001824 00 00 86 16           4673 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$128)
      001828 00 02                 4674 	.dw	2
      00182A 78                    4675 	.db	120
      00182B 02                    4676 	.sleb128	2
      00182C 00 00 86 12           4677 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$126)
      001830 00 00 86 14           4678 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$127)
      001834 00 02                 4679 	.dw	2
      001836 78                    4680 	.db	120
      001837 01                    4681 	.sleb128	1
      001838 00 00 86 09           4682 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$124)
      00183C 00 00 86 12           4683 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$126)
      001840 00 02                 4684 	.dw	2
      001842 78                    4685 	.db	120
      001843 01                    4686 	.sleb128	1
      001844 00 00 00 00           4687 	.dw	0,0
      001848 00 00 00 00           4688 	.dw	0,0
      00184C 00 00 85 F5           4689 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$111)
      001850 00 00 86 09           4690 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$122)
      001854 00 02                 4691 	.dw	2
      001856 78                    4692 	.db	120
      001857 01                    4693 	.sleb128	1
      001858 00 00 85 F0           4694 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$110)
      00185C 00 00 85 F5           4695 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$111)
      001860 00 02                 4696 	.dw	2
      001862 78                    4697 	.db	120
      001863 07                    4698 	.sleb128	7
      001864 00 00 85 EE           4699 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$109)
      001868 00 00 85 F0           4700 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$110)
      00186C 00 02                 4701 	.dw	2
      00186E 78                    4702 	.db	120
      00186F 06                    4703 	.sleb128	6
      001870 00 00 85 EC           4704 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$108)
      001874 00 00 85 EE           4705 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$109)
      001878 00 02                 4706 	.dw	2
      00187A 78                    4707 	.db	120
      00187B 05                    4708 	.sleb128	5
      00187C 00 00 85 EA           4709 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$107)
      001880 00 00 85 EC           4710 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$108)
      001884 00 02                 4711 	.dw	2
      001886 78                    4712 	.db	120
      001887 04                    4713 	.sleb128	4
      001888 00 00 85 E8           4714 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$106)
      00188C 00 00 85 EA           4715 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$107)
      001890 00 02                 4716 	.dw	2
      001892 78                    4717 	.db	120
      001893 02                    4718 	.sleb128	2
      001894 00 00 85 E6           4719 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$105)
      001898 00 00 85 E8           4720 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$106)
      00189C 00 02                 4721 	.dw	2
      00189E 78                    4722 	.db	120
      00189F 01                    4723 	.sleb128	1
      0018A0 00 00 85 DD           4724 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$103)
      0018A4 00 00 85 E6           4725 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$105)
      0018A8 00 02                 4726 	.dw	2
      0018AA 78                    4727 	.db	120
      0018AB 01                    4728 	.sleb128	1
      0018AC 00 00 00 00           4729 	.dw	0,0
      0018B0 00 00 00 00           4730 	.dw	0,0
      0018B4 00 00 85 C9           4731 	.dw	0,(Sstm8s_clk$CLK_LSICmd$90)
      0018B8 00 00 85 DD           4732 	.dw	0,(Sstm8s_clk$CLK_LSICmd$101)
      0018BC 00 02                 4733 	.dw	2
      0018BE 78                    4734 	.db	120
      0018BF 01                    4735 	.sleb128	1
      0018C0 00 00 85 C4           4736 	.dw	0,(Sstm8s_clk$CLK_LSICmd$89)
      0018C4 00 00 85 C9           4737 	.dw	0,(Sstm8s_clk$CLK_LSICmd$90)
      0018C8 00 02                 4738 	.dw	2
      0018CA 78                    4739 	.db	120
      0018CB 07                    4740 	.sleb128	7
      0018CC 00 00 85 C2           4741 	.dw	0,(Sstm8s_clk$CLK_LSICmd$88)
      0018D0 00 00 85 C4           4742 	.dw	0,(Sstm8s_clk$CLK_LSICmd$89)
      0018D4 00 02                 4743 	.dw	2
      0018D6 78                    4744 	.db	120
      0018D7 06                    4745 	.sleb128	6
      0018D8 00 00 85 C0           4746 	.dw	0,(Sstm8s_clk$CLK_LSICmd$87)
      0018DC 00 00 85 C2           4747 	.dw	0,(Sstm8s_clk$CLK_LSICmd$88)
      0018E0 00 02                 4748 	.dw	2
      0018E2 78                    4749 	.db	120
      0018E3 05                    4750 	.sleb128	5
      0018E4 00 00 85 BE           4751 	.dw	0,(Sstm8s_clk$CLK_LSICmd$86)
      0018E8 00 00 85 C0           4752 	.dw	0,(Sstm8s_clk$CLK_LSICmd$87)
      0018EC 00 02                 4753 	.dw	2
      0018EE 78                    4754 	.db	120
      0018EF 04                    4755 	.sleb128	4
      0018F0 00 00 85 BC           4756 	.dw	0,(Sstm8s_clk$CLK_LSICmd$85)
      0018F4 00 00 85 BE           4757 	.dw	0,(Sstm8s_clk$CLK_LSICmd$86)
      0018F8 00 02                 4758 	.dw	2
      0018FA 78                    4759 	.db	120
      0018FB 02                    4760 	.sleb128	2
      0018FC 00 00 85 BA           4761 	.dw	0,(Sstm8s_clk$CLK_LSICmd$84)
      001900 00 00 85 BC           4762 	.dw	0,(Sstm8s_clk$CLK_LSICmd$85)
      001904 00 02                 4763 	.dw	2
      001906 78                    4764 	.db	120
      001907 01                    4765 	.sleb128	1
      001908 00 00 85 B1           4766 	.dw	0,(Sstm8s_clk$CLK_LSICmd$82)
      00190C 00 00 85 BA           4767 	.dw	0,(Sstm8s_clk$CLK_LSICmd$84)
      001910 00 02                 4768 	.dw	2
      001912 78                    4769 	.db	120
      001913 01                    4770 	.sleb128	1
      001914 00 00 00 00           4771 	.dw	0,0
      001918 00 00 00 00           4772 	.dw	0,0
      00191C 00 00 85 9D           4773 	.dw	0,(Sstm8s_clk$CLK_HSICmd$69)
      001920 00 00 85 B1           4774 	.dw	0,(Sstm8s_clk$CLK_HSICmd$80)
      001924 00 02                 4775 	.dw	2
      001926 78                    4776 	.db	120
      001927 01                    4777 	.sleb128	1
      001928 00 00 85 98           4778 	.dw	0,(Sstm8s_clk$CLK_HSICmd$68)
      00192C 00 00 85 9D           4779 	.dw	0,(Sstm8s_clk$CLK_HSICmd$69)
      001930 00 02                 4780 	.dw	2
      001932 78                    4781 	.db	120
      001933 07                    4782 	.sleb128	7
      001934 00 00 85 96           4783 	.dw	0,(Sstm8s_clk$CLK_HSICmd$67)
      001938 00 00 85 98           4784 	.dw	0,(Sstm8s_clk$CLK_HSICmd$68)
      00193C 00 02                 4785 	.dw	2
      00193E 78                    4786 	.db	120
      00193F 06                    4787 	.sleb128	6
      001940 00 00 85 94           4788 	.dw	0,(Sstm8s_clk$CLK_HSICmd$66)
      001944 00 00 85 96           4789 	.dw	0,(Sstm8s_clk$CLK_HSICmd$67)
      001948 00 02                 4790 	.dw	2
      00194A 78                    4791 	.db	120
      00194B 05                    4792 	.sleb128	5
      00194C 00 00 85 92           4793 	.dw	0,(Sstm8s_clk$CLK_HSICmd$65)
      001950 00 00 85 94           4794 	.dw	0,(Sstm8s_clk$CLK_HSICmd$66)
      001954 00 02                 4795 	.dw	2
      001956 78                    4796 	.db	120
      001957 04                    4797 	.sleb128	4
      001958 00 00 85 90           4798 	.dw	0,(Sstm8s_clk$CLK_HSICmd$64)
      00195C 00 00 85 92           4799 	.dw	0,(Sstm8s_clk$CLK_HSICmd$65)
      001960 00 02                 4800 	.dw	2
      001962 78                    4801 	.db	120
      001963 02                    4802 	.sleb128	2
      001964 00 00 85 8E           4803 	.dw	0,(Sstm8s_clk$CLK_HSICmd$63)
      001968 00 00 85 90           4804 	.dw	0,(Sstm8s_clk$CLK_HSICmd$64)
      00196C 00 02                 4805 	.dw	2
      00196E 78                    4806 	.db	120
      00196F 01                    4807 	.sleb128	1
      001970 00 00 85 85           4808 	.dw	0,(Sstm8s_clk$CLK_HSICmd$61)
      001974 00 00 85 8E           4809 	.dw	0,(Sstm8s_clk$CLK_HSICmd$63)
      001978 00 02                 4810 	.dw	2
      00197A 78                    4811 	.db	120
      00197B 01                    4812 	.sleb128	1
      00197C 00 00 00 00           4813 	.dw	0,0
      001980 00 00 00 00           4814 	.dw	0,0
      001984 00 00 85 71           4815 	.dw	0,(Sstm8s_clk$CLK_HSECmd$48)
      001988 00 00 85 85           4816 	.dw	0,(Sstm8s_clk$CLK_HSECmd$59)
      00198C 00 02                 4817 	.dw	2
      00198E 78                    4818 	.db	120
      00198F 01                    4819 	.sleb128	1
      001990 00 00 85 6C           4820 	.dw	0,(Sstm8s_clk$CLK_HSECmd$47)
      001994 00 00 85 71           4821 	.dw	0,(Sstm8s_clk$CLK_HSECmd$48)
      001998 00 02                 4822 	.dw	2
      00199A 78                    4823 	.db	120
      00199B 07                    4824 	.sleb128	7
      00199C 00 00 85 6A           4825 	.dw	0,(Sstm8s_clk$CLK_HSECmd$46)
      0019A0 00 00 85 6C           4826 	.dw	0,(Sstm8s_clk$CLK_HSECmd$47)
      0019A4 00 02                 4827 	.dw	2
      0019A6 78                    4828 	.db	120
      0019A7 06                    4829 	.sleb128	6
      0019A8 00 00 85 68           4830 	.dw	0,(Sstm8s_clk$CLK_HSECmd$45)
      0019AC 00 00 85 6A           4831 	.dw	0,(Sstm8s_clk$CLK_HSECmd$46)
      0019B0 00 02                 4832 	.dw	2
      0019B2 78                    4833 	.db	120
      0019B3 05                    4834 	.sleb128	5
      0019B4 00 00 85 66           4835 	.dw	0,(Sstm8s_clk$CLK_HSECmd$44)
      0019B8 00 00 85 68           4836 	.dw	0,(Sstm8s_clk$CLK_HSECmd$45)
      0019BC 00 02                 4837 	.dw	2
      0019BE 78                    4838 	.db	120
      0019BF 04                    4839 	.sleb128	4
      0019C0 00 00 85 64           4840 	.dw	0,(Sstm8s_clk$CLK_HSECmd$43)
      0019C4 00 00 85 66           4841 	.dw	0,(Sstm8s_clk$CLK_HSECmd$44)
      0019C8 00 02                 4842 	.dw	2
      0019CA 78                    4843 	.db	120
      0019CB 02                    4844 	.sleb128	2
      0019CC 00 00 85 62           4845 	.dw	0,(Sstm8s_clk$CLK_HSECmd$42)
      0019D0 00 00 85 64           4846 	.dw	0,(Sstm8s_clk$CLK_HSECmd$43)
      0019D4 00 02                 4847 	.dw	2
      0019D6 78                    4848 	.db	120
      0019D7 01                    4849 	.sleb128	1
      0019D8 00 00 85 59           4850 	.dw	0,(Sstm8s_clk$CLK_HSECmd$40)
      0019DC 00 00 85 62           4851 	.dw	0,(Sstm8s_clk$CLK_HSECmd$42)
      0019E0 00 02                 4852 	.dw	2
      0019E2 78                    4853 	.db	120
      0019E3 01                    4854 	.sleb128	1
      0019E4 00 00 00 00           4855 	.dw	0,0
      0019E8 00 00 00 00           4856 	.dw	0,0
      0019EC 00 00 85 45           4857 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$27)
      0019F0 00 00 85 59           4858 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$38)
      0019F4 00 02                 4859 	.dw	2
      0019F6 78                    4860 	.db	120
      0019F7 01                    4861 	.sleb128	1
      0019F8 00 00 85 40           4862 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$26)
      0019FC 00 00 85 45           4863 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$27)
      001A00 00 02                 4864 	.dw	2
      001A02 78                    4865 	.db	120
      001A03 07                    4866 	.sleb128	7
      001A04 00 00 85 3E           4867 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$25)
      001A08 00 00 85 40           4868 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$26)
      001A0C 00 02                 4869 	.dw	2
      001A0E 78                    4870 	.db	120
      001A0F 06                    4871 	.sleb128	6
      001A10 00 00 85 3C           4872 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$24)
      001A14 00 00 85 3E           4873 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$25)
      001A18 00 02                 4874 	.dw	2
      001A1A 78                    4875 	.db	120
      001A1B 05                    4876 	.sleb128	5
      001A1C 00 00 85 3A           4877 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$23)
      001A20 00 00 85 3C           4878 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$24)
      001A24 00 02                 4879 	.dw	2
      001A26 78                    4880 	.db	120
      001A27 04                    4881 	.sleb128	4
      001A28 00 00 85 38           4882 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$22)
      001A2C 00 00 85 3A           4883 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$23)
      001A30 00 02                 4884 	.dw	2
      001A32 78                    4885 	.db	120
      001A33 02                    4886 	.sleb128	2
      001A34 00 00 85 36           4887 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$21)
      001A38 00 00 85 38           4888 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$22)
      001A3C 00 02                 4889 	.dw	2
      001A3E 78                    4890 	.db	120
      001A3F 01                    4891 	.sleb128	1
      001A40 00 00 85 2D           4892 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$19)
      001A44 00 00 85 36           4893 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$21)
      001A48 00 02                 4894 	.dw	2
      001A4A 78                    4895 	.db	120
      001A4B 01                    4896 	.sleb128	1
      001A4C 00 00 00 00           4897 	.dw	0,0
      001A50 00 00 00 00           4898 	.dw	0,0
      001A54 00 00 84 F6           4899 	.dw	0,(Sstm8s_clk$CLK_DeInit$1)
      001A58 00 00 85 2D           4900 	.dw	0,(Sstm8s_clk$CLK_DeInit$17)
      001A5C 00 02                 4901 	.dw	2
      001A5E 78                    4902 	.db	120
      001A5F 01                    4903 	.sleb128	1
      001A60 00 00 00 00           4904 	.dw	0,0
      001A64 00 00 00 00           4905 	.dw	0,0
                                   4906 
                                   4907 	.area .debug_abbrev (NOLOAD)
      000212                       4908 Ldebug_abbrev:
      000212 0C                    4909 	.uleb128	12
      000213 2E                    4910 	.uleb128	46
      000214 00                    4911 	.db	0
      000215 03                    4912 	.uleb128	3
      000216 08                    4913 	.uleb128	8
      000217 11                    4914 	.uleb128	17
      000218 01                    4915 	.uleb128	1
      000219 12                    4916 	.uleb128	18
      00021A 01                    4917 	.uleb128	1
      00021B 3F                    4918 	.uleb128	63
      00021C 0C                    4919 	.uleb128	12
      00021D 40                    4920 	.uleb128	64
      00021E 06                    4921 	.uleb128	6
      00021F 49                    4922 	.uleb128	73
      000220 13                    4923 	.uleb128	19
      000221 00                    4924 	.uleb128	0
      000222 00                    4925 	.uleb128	0
      000223 10                    4926 	.uleb128	16
      000224 34                    4927 	.uleb128	52
      000225 00                    4928 	.db	0
      000226 02                    4929 	.uleb128	2
      000227 0A                    4930 	.uleb128	10
      000228 03                    4931 	.uleb128	3
      000229 08                    4932 	.uleb128	8
      00022A 3F                    4933 	.uleb128	63
      00022B 0C                    4934 	.uleb128	12
      00022C 49                    4935 	.uleb128	73
      00022D 13                    4936 	.uleb128	19
      00022E 00                    4937 	.uleb128	0
      00022F 00                    4938 	.uleb128	0
      000230 04                    4939 	.uleb128	4
      000231 05                    4940 	.uleb128	5
      000232 00                    4941 	.db	0
      000233 02                    4942 	.uleb128	2
      000234 0A                    4943 	.uleb128	10
      000235 03                    4944 	.uleb128	3
      000236 08                    4945 	.uleb128	8
      000237 49                    4946 	.uleb128	73
      000238 13                    4947 	.uleb128	19
      000239 00                    4948 	.uleb128	0
      00023A 00                    4949 	.uleb128	0
      00023B 0E                    4950 	.uleb128	14
      00023C 01                    4951 	.uleb128	1
      00023D 01                    4952 	.db	1
      00023E 01                    4953 	.uleb128	1
      00023F 13                    4954 	.uleb128	19
      000240 0B                    4955 	.uleb128	11
      000241 0B                    4956 	.uleb128	11
      000242 49                    4957 	.uleb128	73
      000243 13                    4958 	.uleb128	19
      000244 00                    4959 	.uleb128	0
      000245 00                    4960 	.uleb128	0
      000246 03                    4961 	.uleb128	3
      000247 2E                    4962 	.uleb128	46
      000248 01                    4963 	.db	1
      000249 01                    4964 	.uleb128	1
      00024A 13                    4965 	.uleb128	19
      00024B 03                    4966 	.uleb128	3
      00024C 08                    4967 	.uleb128	8
      00024D 11                    4968 	.uleb128	17
      00024E 01                    4969 	.uleb128	1
      00024F 12                    4970 	.uleb128	18
      000250 01                    4971 	.uleb128	1
      000251 3F                    4972 	.uleb128	63
      000252 0C                    4973 	.uleb128	12
      000253 40                    4974 	.uleb128	64
      000254 06                    4975 	.uleb128	6
      000255 00                    4976 	.uleb128	0
      000256 00                    4977 	.uleb128	0
      000257 0B                    4978 	.uleb128	11
      000258 34                    4979 	.uleb128	52
      000259 00                    4980 	.db	0
      00025A 02                    4981 	.uleb128	2
      00025B 0A                    4982 	.uleb128	10
      00025C 03                    4983 	.uleb128	3
      00025D 08                    4984 	.uleb128	8
      00025E 49                    4985 	.uleb128	73
      00025F 13                    4986 	.uleb128	19
      000260 00                    4987 	.uleb128	0
      000261 00                    4988 	.uleb128	0
      000262 09                    4989 	.uleb128	9
      000263 2E                    4990 	.uleb128	46
      000264 01                    4991 	.db	1
      000265 01                    4992 	.uleb128	1
      000266 13                    4993 	.uleb128	19
      000267 03                    4994 	.uleb128	3
      000268 08                    4995 	.uleb128	8
      000269 11                    4996 	.uleb128	17
      00026A 01                    4997 	.uleb128	1
      00026B 12                    4998 	.uleb128	18
      00026C 01                    4999 	.uleb128	1
      00026D 3F                    5000 	.uleb128	63
      00026E 0C                    5001 	.uleb128	12
      00026F 40                    5002 	.uleb128	64
      000270 06                    5003 	.uleb128	6
      000271 49                    5004 	.uleb128	73
      000272 13                    5005 	.uleb128	19
      000273 00                    5006 	.uleb128	0
      000274 00                    5007 	.uleb128	0
      000275 0D                    5008 	.uleb128	13
      000276 26                    5009 	.uleb128	38
      000277 00                    5010 	.db	0
      000278 49                    5011 	.uleb128	73
      000279 13                    5012 	.uleb128	19
      00027A 00                    5013 	.uleb128	0
      00027B 00                    5014 	.uleb128	0
      00027C 08                    5015 	.uleb128	8
      00027D 0B                    5016 	.uleb128	11
      00027E 01                    5017 	.db	1
      00027F 11                    5018 	.uleb128	17
      000280 01                    5019 	.uleb128	1
      000281 00                    5020 	.uleb128	0
      000282 00                    5021 	.uleb128	0
      000283 01                    5022 	.uleb128	1
      000284 11                    5023 	.uleb128	17
      000285 01                    5024 	.db	1
      000286 03                    5025 	.uleb128	3
      000287 08                    5026 	.uleb128	8
      000288 10                    5027 	.uleb128	16
      000289 06                    5028 	.uleb128	6
      00028A 13                    5029 	.uleb128	19
      00028B 0B                    5030 	.uleb128	11
      00028C 25                    5031 	.uleb128	37
      00028D 08                    5032 	.uleb128	8
      00028E 00                    5033 	.uleb128	0
      00028F 00                    5034 	.uleb128	0
      000290 05                    5035 	.uleb128	5
      000291 0B                    5036 	.uleb128	11
      000292 00                    5037 	.db	0
      000293 11                    5038 	.uleb128	17
      000294 01                    5039 	.uleb128	1
      000295 12                    5040 	.uleb128	18
      000296 01                    5041 	.uleb128	1
      000297 00                    5042 	.uleb128	0
      000298 00                    5043 	.uleb128	0
      000299 07                    5044 	.uleb128	7
      00029A 0B                    5045 	.uleb128	11
      00029B 01                    5046 	.db	1
      00029C 01                    5047 	.uleb128	1
      00029D 13                    5048 	.uleb128	19
      00029E 11                    5049 	.uleb128	17
      00029F 01                    5050 	.uleb128	1
      0002A0 00                    5051 	.uleb128	0
      0002A1 00                    5052 	.uleb128	0
      0002A2 02                    5053 	.uleb128	2
      0002A3 2E                    5054 	.uleb128	46
      0002A4 00                    5055 	.db	0
      0002A5 03                    5056 	.uleb128	3
      0002A6 08                    5057 	.uleb128	8
      0002A7 11                    5058 	.uleb128	17
      0002A8 01                    5059 	.uleb128	1
      0002A9 12                    5060 	.uleb128	18
      0002AA 01                    5061 	.uleb128	1
      0002AB 3F                    5062 	.uleb128	63
      0002AC 0C                    5063 	.uleb128	12
      0002AD 40                    5064 	.uleb128	64
      0002AE 06                    5065 	.uleb128	6
      0002AF 00                    5066 	.uleb128	0
      0002B0 00                    5067 	.uleb128	0
      0002B1 0A                    5068 	.uleb128	10
      0002B2 0B                    5069 	.uleb128	11
      0002B3 01                    5070 	.db	1
      0002B4 01                    5071 	.uleb128	1
      0002B5 13                    5072 	.uleb128	19
      0002B6 11                    5073 	.uleb128	17
      0002B7 01                    5074 	.uleb128	1
      0002B8 12                    5075 	.uleb128	18
      0002B9 01                    5076 	.uleb128	1
      0002BA 00                    5077 	.uleb128	0
      0002BB 00                    5078 	.uleb128	0
      0002BC 0F                    5079 	.uleb128	15
      0002BD 21                    5080 	.uleb128	33
      0002BE 00                    5081 	.db	0
      0002BF 2F                    5082 	.uleb128	47
      0002C0 0B                    5083 	.uleb128	11
      0002C1 00                    5084 	.uleb128	0
      0002C2 00                    5085 	.uleb128	0
      0002C3 06                    5086 	.uleb128	6
      0002C4 24                    5087 	.uleb128	36
      0002C5 00                    5088 	.db	0
      0002C6 03                    5089 	.uleb128	3
      0002C7 08                    5090 	.uleb128	8
      0002C8 0B                    5091 	.uleb128	11
      0002C9 0B                    5092 	.uleb128	11
      0002CA 3E                    5093 	.uleb128	62
      0002CB 0B                    5094 	.uleb128	11
      0002CC 00                    5095 	.uleb128	0
      0002CD 00                    5096 	.uleb128	0
      0002CE 00                    5097 	.uleb128	0
                                   5098 
                                   5099 	.area .debug_info (NOLOAD)
      000905 00 00 09 1A           5100 	.dw	0,Ldebug_info_end-Ldebug_info_start
      000909                       5101 Ldebug_info_start:
      000909 00 02                 5102 	.dw	2
      00090B 00 00 02 12           5103 	.dw	0,(Ldebug_abbrev)
      00090F 04                    5104 	.db	4
      000910 01                    5105 	.uleb128	1
      000911 64 72 69 76 65 72 73  5106 	.ascii "drivers/src/stm8s_clk.c"
             2F 73 72 63 2F 73 74
             6D 38 73 5F 63 6C 6B
             2E 63
      000928 00                    5107 	.db	0
      000929 00 00 08 14           5108 	.dw	0,(Ldebug_line_start+-4)
      00092D 01                    5109 	.db	1
      00092E 53 44 43 43 20 76 65  5110 	.ascii "SDCC version 4.1.0 #12072"
             72 73 69 6F 6E 20 34
             2E 31 2E 30 20 23 31
             32 30 37 32
      000947 00                    5111 	.db	0
      000948 02                    5112 	.uleb128	2
      000949 43 4C 4B 5F 44 65 49  5113 	.ascii "CLK_DeInit"
             6E 69 74
      000953 00                    5114 	.db	0
      000954 00 00 84 F6           5115 	.dw	0,(_CLK_DeInit)
      000958 00 00 85 2D           5116 	.dw	0,(XG$CLK_DeInit$0$0+1)
      00095C 01                    5117 	.db	1
      00095D 00 00 1A 54           5118 	.dw	0,(Ldebug_loc_start+3596)
      000961 03                    5119 	.uleb128	3
      000962 00 00 00 A8           5120 	.dw	0,168
      000966 43 4C 4B 5F 46 61 73  5121 	.ascii "CLK_FastHaltWakeUpCmd"
             74 48 61 6C 74 57 61
             6B 65 55 70 43 6D 64
      00097B 00                    5122 	.db	0
      00097C 00 00 85 2D           5123 	.dw	0,(_CLK_FastHaltWakeUpCmd)
      000980 00 00 85 59           5124 	.dw	0,(XG$CLK_FastHaltWakeUpCmd$0$0+1)
      000984 01                    5125 	.db	1
      000985 00 00 19 EC           5126 	.dw	0,(Ldebug_loc_start+3492)
      000989 04                    5127 	.uleb128	4
      00098A 02                    5128 	.db	2
      00098B 91                    5129 	.db	145
      00098C 02                    5130 	.sleb128	2
      00098D 4E 65 77 53 74 61 74  5131 	.ascii "NewState"
             65
      000995 00                    5132 	.db	0
      000996 00 00 00 A8           5133 	.dw	0,168
      00099A 05                    5134 	.uleb128	5
      00099B 00 00 85 4C           5135 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$30)
      00099F 00 00 85 51           5136 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$32)
      0009A3 05                    5137 	.uleb128	5
      0009A4 00 00 85 53           5138 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$33)
      0009A8 00 00 85 58           5139 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$35)
      0009AC 00                    5140 	.uleb128	0
      0009AD 06                    5141 	.uleb128	6
      0009AE 75 6E 73 69 67 6E 65  5142 	.ascii "unsigned char"
             64 20 63 68 61 72
      0009BB 00                    5143 	.db	0
      0009BC 01                    5144 	.db	1
      0009BD 08                    5145 	.db	8
      0009BE 03                    5146 	.uleb128	3
      0009BF 00 00 00 FA           5147 	.dw	0,250
      0009C3 43 4C 4B 5F 48 53 45  5148 	.ascii "CLK_HSECmd"
             43 6D 64
      0009CD 00                    5149 	.db	0
      0009CE 00 00 85 59           5150 	.dw	0,(_CLK_HSECmd)
      0009D2 00 00 85 85           5151 	.dw	0,(XG$CLK_HSECmd$0$0+1)
      0009D6 01                    5152 	.db	1
      0009D7 00 00 19 84           5153 	.dw	0,(Ldebug_loc_start+3388)
      0009DB 04                    5154 	.uleb128	4
      0009DC 02                    5155 	.db	2
      0009DD 91                    5156 	.db	145
      0009DE 02                    5157 	.sleb128	2
      0009DF 4E 65 77 53 74 61 74  5158 	.ascii "NewState"
             65
      0009E7 00                    5159 	.db	0
      0009E8 00 00 00 A8           5160 	.dw	0,168
      0009EC 05                    5161 	.uleb128	5
      0009ED 00 00 85 78           5162 	.dw	0,(Sstm8s_clk$CLK_HSECmd$51)
      0009F1 00 00 85 7D           5163 	.dw	0,(Sstm8s_clk$CLK_HSECmd$53)
      0009F5 05                    5164 	.uleb128	5
      0009F6 00 00 85 7F           5165 	.dw	0,(Sstm8s_clk$CLK_HSECmd$54)
      0009FA 00 00 85 84           5166 	.dw	0,(Sstm8s_clk$CLK_HSECmd$56)
      0009FE 00                    5167 	.uleb128	0
      0009FF 03                    5168 	.uleb128	3
      000A00 00 00 01 3B           5169 	.dw	0,315
      000A04 43 4C 4B 5F 48 53 49  5170 	.ascii "CLK_HSICmd"
             43 6D 64
      000A0E 00                    5171 	.db	0
      000A0F 00 00 85 85           5172 	.dw	0,(_CLK_HSICmd)
      000A13 00 00 85 B1           5173 	.dw	0,(XG$CLK_HSICmd$0$0+1)
      000A17 01                    5174 	.db	1
      000A18 00 00 19 1C           5175 	.dw	0,(Ldebug_loc_start+3284)
      000A1C 04                    5176 	.uleb128	4
      000A1D 02                    5177 	.db	2
      000A1E 91                    5178 	.db	145
      000A1F 02                    5179 	.sleb128	2
      000A20 4E 65 77 53 74 61 74  5180 	.ascii "NewState"
             65
      000A28 00                    5181 	.db	0
      000A29 00 00 00 A8           5182 	.dw	0,168
      000A2D 05                    5183 	.uleb128	5
      000A2E 00 00 85 A4           5184 	.dw	0,(Sstm8s_clk$CLK_HSICmd$72)
      000A32 00 00 85 A9           5185 	.dw	0,(Sstm8s_clk$CLK_HSICmd$74)
      000A36 05                    5186 	.uleb128	5
      000A37 00 00 85 AB           5187 	.dw	0,(Sstm8s_clk$CLK_HSICmd$75)
      000A3B 00 00 85 B0           5188 	.dw	0,(Sstm8s_clk$CLK_HSICmd$77)
      000A3F 00                    5189 	.uleb128	0
      000A40 03                    5190 	.uleb128	3
      000A41 00 00 01 7C           5191 	.dw	0,380
      000A45 43 4C 4B 5F 4C 53 49  5192 	.ascii "CLK_LSICmd"
             43 6D 64
      000A4F 00                    5193 	.db	0
      000A50 00 00 85 B1           5194 	.dw	0,(_CLK_LSICmd)
      000A54 00 00 85 DD           5195 	.dw	0,(XG$CLK_LSICmd$0$0+1)
      000A58 01                    5196 	.db	1
      000A59 00 00 18 B4           5197 	.dw	0,(Ldebug_loc_start+3180)
      000A5D 04                    5198 	.uleb128	4
      000A5E 02                    5199 	.db	2
      000A5F 91                    5200 	.db	145
      000A60 02                    5201 	.sleb128	2
      000A61 4E 65 77 53 74 61 74  5202 	.ascii "NewState"
             65
      000A69 00                    5203 	.db	0
      000A6A 00 00 00 A8           5204 	.dw	0,168
      000A6E 05                    5205 	.uleb128	5
      000A6F 00 00 85 D0           5206 	.dw	0,(Sstm8s_clk$CLK_LSICmd$93)
      000A73 00 00 85 D5           5207 	.dw	0,(Sstm8s_clk$CLK_LSICmd$95)
      000A77 05                    5208 	.uleb128	5
      000A78 00 00 85 D7           5209 	.dw	0,(Sstm8s_clk$CLK_LSICmd$96)
      000A7C 00 00 85 DC           5210 	.dw	0,(Sstm8s_clk$CLK_LSICmd$98)
      000A80 00                    5211 	.uleb128	0
      000A81 03                    5212 	.uleb128	3
      000A82 00 00 01 BD           5213 	.dw	0,445
      000A86 43 4C 4B 5F 43 43 4F  5214 	.ascii "CLK_CCOCmd"
             43 6D 64
      000A90 00                    5215 	.db	0
      000A91 00 00 85 DD           5216 	.dw	0,(_CLK_CCOCmd)
      000A95 00 00 86 09           5217 	.dw	0,(XG$CLK_CCOCmd$0$0+1)
      000A99 01                    5218 	.db	1
      000A9A 00 00 18 4C           5219 	.dw	0,(Ldebug_loc_start+3076)
      000A9E 04                    5220 	.uleb128	4
      000A9F 02                    5221 	.db	2
      000AA0 91                    5222 	.db	145
      000AA1 02                    5223 	.sleb128	2
      000AA2 4E 65 77 53 74 61 74  5224 	.ascii "NewState"
             65
      000AAA 00                    5225 	.db	0
      000AAB 00 00 00 A8           5226 	.dw	0,168
      000AAF 05                    5227 	.uleb128	5
      000AB0 00 00 85 FC           5228 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$114)
      000AB4 00 00 86 01           5229 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$116)
      000AB8 05                    5230 	.uleb128	5
      000AB9 00 00 86 03           5231 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$117)
      000ABD 00 00 86 08           5232 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$119)
      000AC1 00                    5233 	.uleb128	0
      000AC2 03                    5234 	.uleb128	3
      000AC3 00 00 02 06           5235 	.dw	0,518
      000AC7 43 4C 4B 5F 43 6C 6F  5236 	.ascii "CLK_ClockSwitchCmd"
             63 6B 53 77 69 74 63
             68 43 6D 64
      000AD9 00                    5237 	.db	0
      000ADA 00 00 86 09           5238 	.dw	0,(_CLK_ClockSwitchCmd)
      000ADE 00 00 86 35           5239 	.dw	0,(XG$CLK_ClockSwitchCmd$0$0+1)
      000AE2 01                    5240 	.db	1
      000AE3 00 00 17 E4           5241 	.dw	0,(Ldebug_loc_start+2972)
      000AE7 04                    5242 	.uleb128	4
      000AE8 02                    5243 	.db	2
      000AE9 91                    5244 	.db	145
      000AEA 02                    5245 	.sleb128	2
      000AEB 4E 65 77 53 74 61 74  5246 	.ascii "NewState"
             65
      000AF3 00                    5247 	.db	0
      000AF4 00 00 00 A8           5248 	.dw	0,168
      000AF8 05                    5249 	.uleb128	5
      000AF9 00 00 86 28           5250 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$135)
      000AFD 00 00 86 2D           5251 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$137)
      000B01 05                    5252 	.uleb128	5
      000B02 00 00 86 2F           5253 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$138)
      000B06 00 00 86 34           5254 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$140)
      000B0A 00                    5255 	.uleb128	0
      000B0B 03                    5256 	.uleb128	3
      000B0C 00 00 02 58           5257 	.dw	0,600
      000B10 43 4C 4B 5F 53 6C 6F  5258 	.ascii "CLK_SlowActiveHaltWakeUpCmd"
             77 41 63 74 69 76 65
             48 61 6C 74 57 61 6B
             65 55 70 43 6D 64
      000B2B 00                    5259 	.db	0
      000B2C 00 00 86 35           5260 	.dw	0,(_CLK_SlowActiveHaltWakeUpCmd)
      000B30 00 00 86 61           5261 	.dw	0,(XG$CLK_SlowActiveHaltWakeUpCmd$0$0+1)
      000B34 01                    5262 	.db	1
      000B35 00 00 17 7C           5263 	.dw	0,(Ldebug_loc_start+2868)
      000B39 04                    5264 	.uleb128	4
      000B3A 02                    5265 	.db	2
      000B3B 91                    5266 	.db	145
      000B3C 02                    5267 	.sleb128	2
      000B3D 4E 65 77 53 74 61 74  5268 	.ascii "NewState"
             65
      000B45 00                    5269 	.db	0
      000B46 00 00 00 A8           5270 	.dw	0,168
      000B4A 05                    5271 	.uleb128	5
      000B4B 00 00 86 54           5272 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$156)
      000B4F 00 00 86 59           5273 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$158)
      000B53 05                    5274 	.uleb128	5
      000B54 00 00 86 5B           5275 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$159)
      000B58 00 00 86 60           5276 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$161)
      000B5C 00                    5277 	.uleb128	0
      000B5D 03                    5278 	.uleb128	3
      000B5E 00 00 02 E1           5279 	.dw	0,737
      000B62 43 4C 4B 5F 50 65 72  5280 	.ascii "CLK_PeripheralClockConfig"
             69 70 68 65 72 61 6C
             43 6C 6F 63 6B 43 6F
             6E 66 69 67
      000B7B 00                    5281 	.db	0
      000B7C 00 00 86 61           5282 	.dw	0,(_CLK_PeripheralClockConfig)
      000B80 00 00 87 41           5283 	.dw	0,(XG$CLK_PeripheralClockConfig$0$0+1)
      000B84 01                    5284 	.db	1
      000B85 00 00 16 24           5285 	.dw	0,(Ldebug_loc_start+2524)
      000B89 04                    5286 	.uleb128	4
      000B8A 02                    5287 	.db	2
      000B8B 91                    5288 	.db	145
      000B8C 02                    5289 	.sleb128	2
      000B8D 43 4C 4B 5F 50 65 72  5290 	.ascii "CLK_Peripheral"
             69 70 68 65 72 61 6C
      000B9B 00                    5291 	.db	0
      000B9C 00 00 00 A8           5292 	.dw	0,168
      000BA0 04                    5293 	.uleb128	4
      000BA1 02                    5294 	.db	2
      000BA2 91                    5295 	.db	145
      000BA3 03                    5296 	.sleb128	3
      000BA4 4E 65 77 53 74 61 74  5297 	.ascii "NewState"
             65
      000BAC 00                    5298 	.db	0
      000BAD 00 00 00 A8           5299 	.dw	0,168
      000BB1 07                    5300 	.uleb128	7
      000BB2 00 00 02 C8           5301 	.dw	0,712
      000BB6 00 00 87 1A           5302 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$199)
      000BBA 05                    5303 	.uleb128	5
      000BBB 00 00 87 1E           5304 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$201)
      000BBF 00 00 87 23           5305 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$203)
      000BC3 05                    5306 	.uleb128	5
      000BC4 00 00 87 25           5307 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$204)
      000BC8 00 00 87 2A           5308 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$206)
      000BCC 00                    5309 	.uleb128	0
      000BCD 08                    5310 	.uleb128	8
      000BCE 00 00 87 2F           5311 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$208)
      000BD2 05                    5312 	.uleb128	5
      000BD3 00 00 87 33           5313 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$210)
      000BD7 00 00 87 38           5314 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$212)
      000BDB 05                    5315 	.uleb128	5
      000BDC 00 00 87 3A           5316 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$213)
      000BE0 00 00 87 3F           5317 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$215)
      000BE4 00                    5318 	.uleb128	0
      000BE5 00                    5319 	.uleb128	0
      000BE6 09                    5320 	.uleb128	9
      000BE7 00 00 04 3B           5321 	.dw	0,1083
      000BEB 43 4C 4B 5F 43 6C 6F  5322 	.ascii "CLK_ClockSwitchConfig"
             63 6B 53 77 69 74 63
             68 43 6F 6E 66 69 67
      000C00 00                    5323 	.db	0
      000C01 00 00 87 41           5324 	.dw	0,(_CLK_ClockSwitchConfig)
      000C05 00 00 88 6D           5325 	.dw	0,(XG$CLK_ClockSwitchConfig$0$0+1)
      000C09 01                    5326 	.db	1
      000C0A 00 00 14 6C           5327 	.dw	0,(Ldebug_loc_start+2084)
      000C0E 00 00 00 A8           5328 	.dw	0,168
      000C12 04                    5329 	.uleb128	4
      000C13 02                    5330 	.db	2
      000C14 91                    5331 	.db	145
      000C15 02                    5332 	.sleb128	2
      000C16 43 4C 4B 5F 53 77 69  5333 	.ascii "CLK_SwitchMode"
             74 63 68 4D 6F 64 65
      000C24 00                    5334 	.db	0
      000C25 00 00 00 A8           5335 	.dw	0,168
      000C29 04                    5336 	.uleb128	4
      000C2A 02                    5337 	.db	2
      000C2B 91                    5338 	.db	145
      000C2C 03                    5339 	.sleb128	3
      000C2D 43 4C 4B 5F 4E 65 77  5340 	.ascii "CLK_NewClock"
             43 6C 6F 63 6B
      000C39 00                    5341 	.db	0
      000C3A 00 00 00 A8           5342 	.dw	0,168
      000C3E 04                    5343 	.uleb128	4
      000C3F 02                    5344 	.db	2
      000C40 91                    5345 	.db	145
      000C41 04                    5346 	.sleb128	4
      000C42 49 54 53 74 61 74 65  5347 	.ascii "ITState"
      000C49 00                    5348 	.db	0
      000C4A 00 00 00 A8           5349 	.dw	0,168
      000C4E 04                    5350 	.uleb128	4
      000C4F 02                    5351 	.db	2
      000C50 91                    5352 	.db	145
      000C51 05                    5353 	.sleb128	5
      000C52 43 4C 4B 5F 43 75 72  5354 	.ascii "CLK_CurrentClockState"
             72 65 6E 74 43 6C 6F
             63 6B 53 74 61 74 65
      000C67 00                    5355 	.db	0
      000C68 00 00 00 A8           5356 	.dw	0,168
      000C6C 0A                    5357 	.uleb128	10
      000C6D 00 00 03 A2           5358 	.dw	0,930
      000C71 00 00 87 C6           5359 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$260)
      000C75 00 00 87 E5           5360 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$271)
      000C79 05                    5361 	.uleb128	5
      000C7A 00 00 87 D3           5362 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$264)
      000C7E 00 00 87 D8           5363 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$266)
      000C82 05                    5364 	.uleb128	5
      000C83 00 00 87 DA           5365 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$267)
      000C87 00 00 87 DF           5366 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$269)
      000C8B 05                    5367 	.uleb128	5
      000C8C 00 00 87 F0           5368 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$273)
      000C90 00 00 87 F1           5369 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$275)
      000C94 05                    5370 	.uleb128	5
      000C95 00 00 87 F6           5371 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$277)
      000C99 00 00 87 F9           5372 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$279)
      000C9D 05                    5373 	.uleb128	5
      000C9E 00 00 87 FC           5374 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$280)
      000CA2 00 00 87 FD           5375 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$282)
      000CA6 00                    5376 	.uleb128	0
      000CA7 0A                    5377 	.uleb128	10
      000CA8 00 00 03 DD           5378 	.dw	0,989
      000CAC 00 00 88 00           5379 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$283)
      000CB0 00 00 88 18           5380 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$292)
      000CB4 05                    5381 	.uleb128	5
      000CB5 00 00 88 04           5382 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$285)
      000CB9 00 00 88 0A           5383 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$287)
      000CBD 05                    5384 	.uleb128	5
      000CBE 00 00 88 0C           5385 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$288)
      000CC2 00 00 88 12           5386 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$290)
      000CC6 05                    5387 	.uleb128	5
      000CC7 00 00 88 24           5388 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$294)
      000CCB 00 00 88 25           5389 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$296)
      000CCF 05                    5390 	.uleb128	5
      000CD0 00 00 88 2A           5391 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$298)
      000CD4 00 00 88 31           5392 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$301)
      000CD8 05                    5393 	.uleb128	5
      000CD9 00 00 88 33           5394 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$302)
      000CDD 00 00 88 34           5395 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$304)
      000CE1 00                    5396 	.uleb128	0
      000CE2 07                    5397 	.uleb128	7
      000CE3 00 00 04 02           5398 	.dw	0,1026
      000CE7 00 00 88 3B           5399 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$306)
      000CEB 05                    5400 	.uleb128	5
      000CEC 00 00 88 45           5401 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$309)
      000CF0 00 00 88 49           5402 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$311)
      000CF4 05                    5403 	.uleb128	5
      000CF5 00 00 88 55           5404 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$314)
      000CF9 00 00 88 59           5405 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$316)
      000CFD 05                    5406 	.uleb128	5
      000CFE 00 00 88 65           5407 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$319)
      000D02 00 00 88 69           5408 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$321)
      000D06 00                    5409 	.uleb128	0
      000D07 0B                    5410 	.uleb128	11
      000D08 01                    5411 	.db	1
      000D09 53                    5412 	.db	83
      000D0A 63 6C 6F 63 6B 5F 6D  5413 	.ascii "clock_master"
             61 73 74 65 72
      000D16 00                    5414 	.db	0
      000D17 00 00 00 A8           5415 	.dw	0,168
      000D1B 0B                    5416 	.uleb128	11
      000D1C 06                    5417 	.db	6
      000D1D 52                    5418 	.db	82
      000D1E 93                    5419 	.db	147
      000D1F 01                    5420 	.uleb128	1
      000D20 51                    5421 	.db	81
      000D21 93                    5422 	.db	147
      000D22 01                    5423 	.uleb128	1
      000D23 44 6F 77 6E 43 6F 75  5424 	.ascii "DownCounter"
             6E 74 65 72
      000D2E 00                    5425 	.db	0
      000D2F 00 00 04 3B           5426 	.dw	0,1083
      000D33 0B                    5427 	.uleb128	11
      000D34 01                    5428 	.db	1
      000D35 51                    5429 	.db	81
      000D36 53 77 69 66           5430 	.ascii "Swif"
      000D3A 00                    5431 	.db	0
      000D3B 00 00 00 A8           5432 	.dw	0,168
      000D3F 00                    5433 	.uleb128	0
      000D40 06                    5434 	.uleb128	6
      000D41 75 6E 73 69 67 6E 65  5435 	.ascii "unsigned int"
             64 20 69 6E 74
      000D4D 00                    5436 	.db	0
      000D4E 02                    5437 	.db	2
      000D4F 07                    5438 	.db	7
      000D50 03                    5439 	.uleb128	3
      000D51 00 00 04 8A           5440 	.dw	0,1162
      000D55 43 4C 4B 5F 48 53 49  5441 	.ascii "CLK_HSIPrescalerConfig"
             50 72 65 73 63 61 6C
             65 72 43 6F 6E 66 69
             67
      000D6B 00                    5442 	.db	0
      000D6C 00 00 88 6D           5443 	.dw	0,(_CLK_HSIPrescalerConfig)
      000D70 00 00 88 A3           5444 	.dw	0,(XG$CLK_HSIPrescalerConfig$0$0+1)
      000D74 01                    5445 	.db	1
      000D75 00 00 13 EC           5446 	.dw	0,(Ldebug_loc_start+1956)
      000D79 04                    5447 	.uleb128	4
      000D7A 02                    5448 	.db	2
      000D7B 91                    5449 	.db	145
      000D7C 02                    5450 	.sleb128	2
      000D7D 48 53 49 50 72 65 73  5451 	.ascii "HSIPrescaler"
             63 61 6C 65 72
      000D89 00                    5452 	.db	0
      000D8A 00 00 00 A8           5453 	.dw	0,168
      000D8E 00                    5454 	.uleb128	0
      000D8F 03                    5455 	.uleb128	3
      000D90 00 00 04 BB           5456 	.dw	0,1211
      000D94 43 4C 4B 5F 43 43 4F  5457 	.ascii "CLK_CCOConfig"
             43 6F 6E 66 69 67
      000DA1 00                    5458 	.db	0
      000DA2 00 00 88 A3           5459 	.dw	0,(_CLK_CCOConfig)
      000DA6 00 00 89 25           5460 	.dw	0,(XG$CLK_CCOConfig$0$0+1)
      000DAA 01                    5461 	.db	1
      000DAB 00 00 13 00           5462 	.dw	0,(Ldebug_loc_start+1720)
      000DAF 04                    5463 	.uleb128	4
      000DB0 02                    5464 	.db	2
      000DB1 91                    5465 	.db	145
      000DB2 02                    5466 	.sleb128	2
      000DB3 43 4C 4B 5F 43 43 4F  5467 	.ascii "CLK_CCO"
      000DBA 00                    5468 	.db	0
      000DBB 00 00 00 A8           5469 	.dw	0,168
      000DBF 00                    5470 	.uleb128	0
      000DC0 03                    5471 	.uleb128	3
      000DC1 00 00 05 1D           5472 	.dw	0,1309
      000DC5 43 4C 4B 5F 49 54 43  5473 	.ascii "CLK_ITConfig"
             6F 6E 66 69 67
      000DD1 00                    5474 	.db	0
      000DD2 00 00 89 25           5475 	.dw	0,(_CLK_ITConfig)
      000DD6 00 00 89 9A           5476 	.dw	0,(XG$CLK_ITConfig$0$0+1)
      000DDA 01                    5477 	.db	1
      000DDB 00 00 11 D8           5478 	.dw	0,(Ldebug_loc_start+1424)
      000DDF 04                    5479 	.uleb128	4
      000DE0 02                    5480 	.db	2
      000DE1 91                    5481 	.db	145
      000DE2 02                    5482 	.sleb128	2
      000DE3 43 4C 4B 5F 49 54     5483 	.ascii "CLK_IT"
      000DE9 00                    5484 	.db	0
      000DEA 00 00 00 A8           5485 	.dw	0,168
      000DEE 04                    5486 	.uleb128	4
      000DEF 02                    5487 	.db	2
      000DF0 91                    5488 	.db	145
      000DF1 03                    5489 	.sleb128	3
      000DF2 4E 65 77 53 74 61 74  5490 	.ascii "NewState"
             65
      000DFA 00                    5491 	.db	0
      000DFB 00 00 00 A8           5492 	.dw	0,168
      000DFF 07                    5493 	.uleb128	7
      000E00 00 00 05 0D           5494 	.dw	0,1293
      000E04 00 00 89 74           5495 	.dw	0,(Sstm8s_clk$CLK_ITConfig$398)
      000E08 05                    5496 	.uleb128	5
      000E09 00 00 89 7B           5497 	.dw	0,(Sstm8s_clk$CLK_ITConfig$400)
      000E0D 00 00 89 87           5498 	.dw	0,(Sstm8s_clk$CLK_ITConfig$406)
      000E11 00                    5499 	.uleb128	0
      000E12 08                    5500 	.uleb128	8
      000E13 00 00 89 87           5501 	.dw	0,(Sstm8s_clk$CLK_ITConfig$408)
      000E17 05                    5502 	.uleb128	5
      000E18 00 00 89 8E           5503 	.dw	0,(Sstm8s_clk$CLK_ITConfig$410)
      000E1C 00 00 89 98           5504 	.dw	0,(Sstm8s_clk$CLK_ITConfig$415)
      000E20 00                    5505 	.uleb128	0
      000E21 00                    5506 	.uleb128	0
      000E22 03                    5507 	.uleb128	3
      000E23 00 00 05 69           5508 	.dw	0,1385
      000E27 43 4C 4B 5F 53 59 53  5509 	.ascii "CLK_SYSCLKConfig"
             43 4C 4B 43 6F 6E 66
             69 67
      000E37 00                    5510 	.db	0
      000E38 00 00 89 9A           5511 	.dw	0,(_CLK_SYSCLKConfig)
      000E3C 00 00 8A 30           5512 	.dw	0,(XG$CLK_SYSCLKConfig$0$0+1)
      000E40 01                    5513 	.db	1
      000E41 00 00 10 E0           5514 	.dw	0,(Ldebug_loc_start+1176)
      000E45 04                    5515 	.uleb128	4
      000E46 02                    5516 	.db	2
      000E47 91                    5517 	.db	145
      000E48 02                    5518 	.sleb128	2
      000E49 43 4C 4B 5F 50 72 65  5519 	.ascii "CLK_Prescaler"
             73 63 61 6C 65 72
      000E56 00                    5520 	.db	0
      000E57 00 00 00 A8           5521 	.dw	0,168
      000E5B 05                    5522 	.uleb128	5
      000E5C 00 00 8A 06           5523 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$444)
      000E60 00 00 8A 19           5524 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$447)
      000E64 05                    5525 	.uleb128	5
      000E65 00 00 8A 1B           5526 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$448)
      000E69 00 00 8A 2E           5527 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$451)
      000E6D 00                    5528 	.uleb128	0
      000E6E 03                    5529 	.uleb128	3
      000E6F 00 00 05 B5           5530 	.dw	0,1461
      000E73 43 4C 4B 5F 53 57 49  5531 	.ascii "CLK_SWIMConfig"
             4D 43 6F 6E 66 69 67
      000E81 00                    5532 	.db	0
      000E82 00 00 8A 30           5533 	.dw	0,(_CLK_SWIMConfig)
      000E86 00 00 8A 5C           5534 	.dw	0,(XG$CLK_SWIMConfig$0$0+1)
      000E8A 01                    5535 	.db	1
      000E8B 00 00 10 78           5536 	.dw	0,(Ldebug_loc_start+1072)
      000E8F 04                    5537 	.uleb128	4
      000E90 02                    5538 	.db	2
      000E91 91                    5539 	.db	145
      000E92 02                    5540 	.sleb128	2
      000E93 43 4C 4B 5F 53 57 49  5541 	.ascii "CLK_SWIMDivider"
             4D 44 69 76 69 64 65
             72
      000EA2 00                    5542 	.db	0
      000EA3 00 00 00 A8           5543 	.dw	0,168
      000EA7 05                    5544 	.uleb128	5
      000EA8 00 00 8A 4F           5545 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$468)
      000EAC 00 00 8A 54           5546 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$470)
      000EB0 05                    5547 	.uleb128	5
      000EB1 00 00 8A 56           5548 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$471)
      000EB5 00 00 8A 5B           5549 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$473)
      000EB9 00                    5550 	.uleb128	0
      000EBA 02                    5551 	.uleb128	2
      000EBB 43 4C 4B 5F 43 6C 6F  5552 	.ascii "CLK_ClockSecuritySystemEnable"
             63 6B 53 65 63 75 72
             69 74 79 53 79 73 74
             65 6D 45 6E 61 62 6C
             65
      000ED8 00                    5553 	.db	0
      000ED9 00 00 8A 5C           5554 	.dw	0,(_CLK_ClockSecuritySystemEnable)
      000EDD 00 00 8A 61           5555 	.dw	0,(XG$CLK_ClockSecuritySystemEnable$0$0+1)
      000EE1 01                    5556 	.db	1
      000EE2 00 00 10 64           5557 	.dw	0,(Ldebug_loc_start+1052)
      000EE6 0C                    5558 	.uleb128	12
      000EE7 43 4C 4B 5F 47 65 74  5559 	.ascii "CLK_GetSYSCLKSource"
             53 59 53 43 4C 4B 53
             6F 75 72 63 65
      000EFA 00                    5560 	.db	0
      000EFB 00 00 8A 61           5561 	.dw	0,(_CLK_GetSYSCLKSource)
      000EFF 00 00 8A 65           5562 	.dw	0,(XG$CLK_GetSYSCLKSource$0$0+1)
      000F03 01                    5563 	.db	1
      000F04 00 00 10 50           5564 	.dw	0,(Ldebug_loc_start+1032)
      000F08 00 00 00 A8           5565 	.dw	0,168
      000F0C 06                    5566 	.uleb128	6
      000F0D 75 6E 73 69 67 6E 65  5567 	.ascii "unsigned long"
             64 20 6C 6F 6E 67
      000F1A 00                    5568 	.db	0
      000F1B 04                    5569 	.db	4
      000F1C 07                    5570 	.db	7
      000F1D 09                    5571 	.uleb128	9
      000F1E 00 00 06 AA           5572 	.dw	0,1706
      000F22 43 4C 4B 5F 47 65 74  5573 	.ascii "CLK_GetClockFreq"
             43 6C 6F 63 6B 46 72
             65 71
      000F32 00                    5574 	.db	0
      000F33 00 00 8A 65           5575 	.dw	0,(_CLK_GetClockFreq)
      000F37 00 00 8A B5           5576 	.dw	0,(XG$CLK_GetClockFreq$0$0+1)
      000F3B 01                    5577 	.db	1
      000F3C 00 00 0F B8           5578 	.dw	0,(Ldebug_loc_start+880)
      000F40 00 00 06 07           5579 	.dw	0,1543
      000F44 05                    5580 	.uleb128	5
      000F45 00 00 8A 72           5581 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$495)
      000F49 00 00 8A 83           5582 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$500)
      000F4D 05                    5583 	.uleb128	5
      000F4E 00 00 8A 9E           5584 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$510)
      000F52 00 00 8A A5           5585 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$512)
      000F56 05                    5586 	.uleb128	5
      000F57 00 00 8A A7           5587 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$513)
      000F5B 00 00 8A AF           5588 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$515)
      000F5F 0B                    5589 	.uleb128	11
      000F60 0E                    5590 	.db	14
      000F61 52                    5591 	.db	82
      000F62 93                    5592 	.db	147
      000F63 01                    5593 	.uleb128	1
      000F64 51                    5594 	.db	81
      000F65 93                    5595 	.db	147
      000F66 01                    5596 	.uleb128	1
      000F67 91                    5597 	.db	145
      000F68 7E                    5598 	.sleb128	-2
      000F69 93                    5599 	.db	147
      000F6A 01                    5600 	.uleb128	1
      000F6B 91                    5601 	.db	145
      000F6C 7F                    5602 	.sleb128	-1
      000F6D 93                    5603 	.db	147
      000F6E 01                    5604 	.uleb128	1
      000F6F 63 6C 6F 63 6B 66 72  5605 	.ascii "clockfrequency"
             65 71 75 65 6E 63 79
      000F7D 00                    5606 	.db	0
      000F7E 00 00 06 07           5607 	.dw	0,1543
      000F82 0B                    5608 	.uleb128	11
      000F83 02                    5609 	.db	2
      000F84 91                    5610 	.db	145
      000F85 7F                    5611 	.sleb128	-1
      000F86 63 6C 6F 63 6B 73 6F  5612 	.ascii "clocksource"
             75 72 63 65
      000F91 00                    5613 	.db	0
      000F92 00 00 00 A8           5614 	.dw	0,168
      000F96 0B                    5615 	.uleb128	11
      000F97 01                    5616 	.db	1
      000F98 50                    5617 	.db	80
      000F99 74 6D 70              5618 	.ascii "tmp"
      000F9C 00                    5619 	.db	0
      000F9D 00 00 00 A8           5620 	.dw	0,168
      000FA1 0B                    5621 	.uleb128	11
      000FA2 01                    5622 	.db	1
      000FA3 50                    5623 	.db	80
      000FA4 70 72 65 73 63        5624 	.ascii "presc"
      000FA9 00                    5625 	.db	0
      000FAA 00 00 00 A8           5626 	.dw	0,168
      000FAE 00                    5627 	.uleb128	0
      000FAF 03                    5628 	.uleb128	3
      000FB0 00 00 06 FB           5629 	.dw	0,1787
      000FB4 43 4C 4B 5F 41 64 6A  5630 	.ascii "CLK_AdjustHSICalibrationValue"
             75 73 74 48 53 49 43
             61 6C 69 62 72 61 74
             69 6F 6E 56 61 6C 75
             65
      000FD1 00                    5631 	.db	0
      000FD2 00 00 8A B5           5632 	.dw	0,(_CLK_AdjustHSICalibrationValue)
      000FD6 00 00 8A FC           5633 	.dw	0,(XG$CLK_AdjustHSICalibrationValue$0$0+1)
      000FDA 01                    5634 	.db	1
      000FDB 00 00 0F 08           5635 	.dw	0,(Ldebug_loc_start+704)
      000FDF 04                    5636 	.uleb128	4
      000FE0 02                    5637 	.db	2
      000FE1 91                    5638 	.db	145
      000FE2 02                    5639 	.sleb128	2
      000FE3 43 4C 4B 5F 48 53 49  5640 	.ascii "CLK_HSICalibrationValue"
             43 61 6C 69 62 72 61
             74 69 6F 6E 56 61 6C
             75 65
      000FFA 00                    5641 	.db	0
      000FFB 00 00 00 A8           5642 	.dw	0,168
      000FFF 00                    5643 	.uleb128	0
      001000 02                    5644 	.uleb128	2
      001001 43 4C 4B 5F 53 59 53  5645 	.ascii "CLK_SYSCLKEmergencyClear"
             43 4C 4B 45 6D 65 72
             67 65 6E 63 79 43 6C
             65 61 72
      001019 00                    5646 	.db	0
      00101A 00 00 8A FC           5647 	.dw	0,(_CLK_SYSCLKEmergencyClear)
      00101E 00 00 8B 01           5648 	.dw	0,(XG$CLK_SYSCLKEmergencyClear$0$0+1)
      001022 01                    5649 	.db	1
      001023 00 00 0E F4           5650 	.dw	0,(Ldebug_loc_start+684)
      001027 09                    5651 	.uleb128	9
      001028 00 00 07 D0           5652 	.dw	0,2000
      00102C 43 4C 4B 5F 47 65 74  5653 	.ascii "CLK_GetFlagStatus"
             46 6C 61 67 53 74 61
             74 75 73
      00103D 00                    5654 	.db	0
      00103E 00 00 8B 01           5655 	.dw	0,(_CLK_GetFlagStatus)
      001042 00 00 8B 8C           5656 	.dw	0,(XG$CLK_GetFlagStatus$0$0+1)
      001046 01                    5657 	.db	1
      001047 00 00 0D A8           5658 	.dw	0,(Ldebug_loc_start+352)
      00104B 00 00 00 A8           5659 	.dw	0,168
      00104F 04                    5660 	.uleb128	4
      001050 02                    5661 	.db	2
      001051 91                    5662 	.db	145
      001052 02                    5663 	.sleb128	2
      001053 43 4C 4B 5F 46 4C 41  5664 	.ascii "CLK_FLAG"
             47
      00105B 00                    5665 	.db	0
      00105C 00 00 07 D0           5666 	.dw	0,2000
      001060 05                    5667 	.uleb128	5
      001061 00 00 8B 54           5668 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$572)
      001065 00 00 8B 57           5669 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$574)
      001069 05                    5670 	.uleb128	5
      00106A 00 00 8B 5E           5671 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$577)
      00106E 00 00 8B 61           5672 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$579)
      001072 05                    5673 	.uleb128	5
      001073 00 00 8B 68           5674 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$582)
      001077 00 00 8B 6B           5675 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$584)
      00107B 05                    5676 	.uleb128	5
      00107C 00 00 8B 72           5677 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$587)
      001080 00 00 8B 75           5678 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$589)
      001084 05                    5679 	.uleb128	5
      001085 00 00 8B 77           5680 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$590)
      001089 00 00 8B 7A           5681 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$592)
      00108D 05                    5682 	.uleb128	5
      00108E 00 00 8B 84           5683 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$596)
      001092 00 00 8B 86           5684 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$598)
      001096 05                    5685 	.uleb128	5
      001097 00 00 8B 88           5686 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$599)
      00109B 00 00 8B 89           5687 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$601)
      00109F 0B                    5688 	.uleb128	11
      0010A0 06                    5689 	.db	6
      0010A1 52                    5690 	.db	82
      0010A2 93                    5691 	.db	147
      0010A3 01                    5692 	.uleb128	1
      0010A4 50                    5693 	.db	80
      0010A5 93                    5694 	.db	147
      0010A6 01                    5695 	.uleb128	1
      0010A7 73 74 61 74 75 73 72  5696 	.ascii "statusreg"
             65 67
      0010B0 00                    5697 	.db	0
      0010B1 00 00 04 3B           5698 	.dw	0,1083
      0010B5 0B                    5699 	.uleb128	11
      0010B6 01                    5700 	.db	1
      0010B7 50                    5701 	.db	80
      0010B8 74 6D 70 72 65 67     5702 	.ascii "tmpreg"
      0010BE 00                    5703 	.db	0
      0010BF 00 00 00 A8           5704 	.dw	0,168
      0010C3 0B                    5705 	.uleb128	11
      0010C4 01                    5706 	.db	1
      0010C5 50                    5707 	.db	80
      0010C6 62 69 74 73 74 61 74  5708 	.ascii "bitstatus"
             75 73
      0010CF 00                    5709 	.db	0
      0010D0 00 00 00 A8           5710 	.dw	0,168
      0010D4 00                    5711 	.uleb128	0
      0010D5 06                    5712 	.uleb128	6
      0010D6 75 6E 73 69 67 6E 65  5713 	.ascii "unsigned int"
             64 20 69 6E 74
      0010E2 00                    5714 	.db	0
      0010E3 02                    5715 	.db	2
      0010E4 07                    5716 	.db	7
      0010E5 09                    5717 	.uleb128	9
      0010E6 00 00 08 5F           5718 	.dw	0,2143
      0010EA 43 4C 4B 5F 47 65 74  5719 	.ascii "CLK_GetITStatus"
             49 54 53 74 61 74 75
             73
      0010F9 00                    5720 	.db	0
      0010FA 00 00 8B 8C           5721 	.dw	0,(_CLK_GetITStatus)
      0010FE 00 00 8B D3           5722 	.dw	0,(XG$CLK_GetITStatus$0$0+1)
      001102 01                    5723 	.db	1
      001103 00 00 0C EC           5724 	.dw	0,(Ldebug_loc_start+164)
      001107 00 00 00 A8           5725 	.dw	0,168
      00110B 04                    5726 	.uleb128	4
      00110C 02                    5727 	.db	2
      00110D 91                    5728 	.db	145
      00110E 02                    5729 	.sleb128	2
      00110F 43 4C 4B 5F 49 54     5730 	.ascii "CLK_IT"
      001115 00                    5731 	.db	0
      001116 00 00 00 A8           5732 	.dw	0,168
      00111A 07                    5733 	.uleb128	7
      00111B 00 00 08 31           5734 	.dw	0,2097
      00111F 00 00 8B B4           5735 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$623)
      001123 05                    5736 	.uleb128	5
      001124 00 00 8B BD           5737 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$626)
      001128 00 00 8B BF           5738 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$628)
      00112C 05                    5739 	.uleb128	5
      00112D 00 00 8B C1           5740 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$629)
      001131 00 00 8B C2           5741 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$631)
      001135 00                    5742 	.uleb128	0
      001136 07                    5743 	.uleb128	7
      001137 00 00 08 4D           5744 	.dw	0,2125
      00113B 00 00 8B C4           5745 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$632)
      00113F 05                    5746 	.uleb128	5
      001140 00 00 8B CD           5747 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$635)
      001144 00 00 8B CF           5748 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$637)
      001148 05                    5749 	.uleb128	5
      001149 00 00 8B D1           5750 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$638)
      00114D 00 00 8B D2           5751 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$640)
      001151 00                    5752 	.uleb128	0
      001152 0B                    5753 	.uleb128	11
      001153 01                    5754 	.db	1
      001154 50                    5755 	.db	80
      001155 62 69 74 73 74 61 74  5756 	.ascii "bitstatus"
             75 73
      00115E 00                    5757 	.db	0
      00115F 00 00 00 A8           5758 	.dw	0,168
      001163 00                    5759 	.uleb128	0
      001164 03                    5760 	.uleb128	3
      001165 00 00 08 A9           5761 	.dw	0,2217
      001169 43 4C 4B 5F 43 6C 65  5762 	.ascii "CLK_ClearITPendingBit"
             61 72 49 54 50 65 6E
             64 69 6E 67 42 69 74
      00117E 00                    5763 	.db	0
      00117F 00 00 8B D3           5764 	.dw	0,(_CLK_ClearITPendingBit)
      001183 00 00 8C 06           5765 	.dw	0,(XG$CLK_ClearITPendingBit$0$0+1)
      001187 01                    5766 	.db	1
      001188 00 00 0C 48           5767 	.dw	0,(Ldebug_loc_start)
      00118C 04                    5768 	.uleb128	4
      00118D 02                    5769 	.db	2
      00118E 91                    5770 	.db	145
      00118F 02                    5771 	.sleb128	2
      001190 43 4C 4B 5F 49 54     5772 	.ascii "CLK_IT"
      001196 00                    5773 	.db	0
      001197 00 00 00 A8           5774 	.dw	0,168
      00119B 05                    5775 	.uleb128	5
      00119C 00 00 8B FB           5776 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$661)
      0011A0 00 00 8B FF           5777 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$663)
      0011A4 05                    5778 	.uleb128	5
      0011A5 00 00 8C 01           5779 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$664)
      0011A9 00 00 8C 05           5780 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$666)
      0011AD 00                    5781 	.uleb128	0
      0011AE 0D                    5782 	.uleb128	13
      0011AF 00 00 00 A8           5783 	.dw	0,168
      0011B3 0E                    5784 	.uleb128	14
      0011B4 00 00 08 BB           5785 	.dw	0,2235
      0011B8 04                    5786 	.db	4
      0011B9 00 00 08 A9           5787 	.dw	0,2217
      0011BD 0F                    5788 	.uleb128	15
      0011BE 03                    5789 	.db	3
      0011BF 00                    5790 	.uleb128	0
      0011C0 10                    5791 	.uleb128	16
      0011C1 05                    5792 	.db	5
      0011C2 03                    5793 	.db	3
      0011C3 00 00 80 D6           5794 	.dw	0,(_HSIDivFactor)
      0011C7 48 53 49 44 69 76 46  5795 	.ascii "HSIDivFactor"
             61 63 74 6F 72
      0011D3 00                    5796 	.db	0
      0011D4 01                    5797 	.db	1
      0011D5 00 00 08 AE           5798 	.dw	0,2222
      0011D9 0E                    5799 	.uleb128	14
      0011DA 00 00 08 E1           5800 	.dw	0,2273
      0011DE 08                    5801 	.db	8
      0011DF 00 00 08 A9           5802 	.dw	0,2217
      0011E3 0F                    5803 	.uleb128	15
      0011E4 07                    5804 	.db	7
      0011E5 00                    5805 	.uleb128	0
      0011E6 10                    5806 	.uleb128	16
      0011E7 05                    5807 	.db	5
      0011E8 03                    5808 	.db	3
      0011E9 00 00 80 DA           5809 	.dw	0,(_CLKPrescTable)
      0011ED 43 4C 4B 50 72 65 73  5810 	.ascii "CLKPrescTable"
             63 54 61 62 6C 65
      0011FA 00                    5811 	.db	0
      0011FB 01                    5812 	.db	1
      0011FC 00 00 08 D4           5813 	.dw	0,2260
      001200 0E                    5814 	.uleb128	14
      001201 00 00 09 08           5815 	.dw	0,2312
      001205 18                    5816 	.db	24
      001206 00 00 08 A9           5817 	.dw	0,2217
      00120A 0F                    5818 	.uleb128	15
      00120B 17                    5819 	.db	23
      00120C 00                    5820 	.uleb128	0
      00120D 0B                    5821 	.uleb128	11
      00120E 05                    5822 	.db	5
      00120F 03                    5823 	.db	3
      001210 00 00 80 E2           5824 	.dw	0,(___str_0)
      001214 5F 5F 73 74 72 5F 30  5825 	.ascii "__str_0"
      00121B 00                    5826 	.db	0
      00121C 00 00 08 FB           5827 	.dw	0,2299
      001220 00                    5828 	.uleb128	0
      001221 00                    5829 	.uleb128	0
      001222 00                    5830 	.uleb128	0
      001223                       5831 Ldebug_info_end:
                                   5832 
                                   5833 	.area .debug_pubnames (NOLOAD)
      000393 00 00 02 3D           5834 	.dw	0,Ldebug_pubnames_end-Ldebug_pubnames_start
      000397                       5835 Ldebug_pubnames_start:
      000397 00 02                 5836 	.dw	2
      000399 00 00 09 05           5837 	.dw	0,(Ldebug_info_start-4)
      00039D 00 00 09 1E           5838 	.dw	0,4+Ldebug_info_end-Ldebug_info_start
      0003A1 00 00 00 43           5839 	.dw	0,67
      0003A5 43 4C 4B 5F 44 65 49  5840 	.ascii "CLK_DeInit"
             6E 69 74
      0003AF 00                    5841 	.db	0
      0003B0 00 00 00 5C           5842 	.dw	0,92
      0003B4 43 4C 4B 5F 46 61 73  5843 	.ascii "CLK_FastHaltWakeUpCmd"
             74 48 61 6C 74 57 61
             6B 65 55 70 43 6D 64
      0003C9 00                    5844 	.db	0
      0003CA 00 00 00 B9           5845 	.dw	0,185
      0003CE 43 4C 4B 5F 48 53 45  5846 	.ascii "CLK_HSECmd"
             43 6D 64
      0003D8 00                    5847 	.db	0
      0003D9 00 00 00 FA           5848 	.dw	0,250
      0003DD 43 4C 4B 5F 48 53 49  5849 	.ascii "CLK_HSICmd"
             43 6D 64
      0003E7 00                    5850 	.db	0
      0003E8 00 00 01 3B           5851 	.dw	0,315
      0003EC 43 4C 4B 5F 4C 53 49  5852 	.ascii "CLK_LSICmd"
             43 6D 64
      0003F6 00                    5853 	.db	0
      0003F7 00 00 01 7C           5854 	.dw	0,380
      0003FB 43 4C 4B 5F 43 43 4F  5855 	.ascii "CLK_CCOCmd"
             43 6D 64
      000405 00                    5856 	.db	0
      000406 00 00 01 BD           5857 	.dw	0,445
      00040A 43 4C 4B 5F 43 6C 6F  5858 	.ascii "CLK_ClockSwitchCmd"
             63 6B 53 77 69 74 63
             68 43 6D 64
      00041C 00                    5859 	.db	0
      00041D 00 00 02 06           5860 	.dw	0,518
      000421 43 4C 4B 5F 53 6C 6F  5861 	.ascii "CLK_SlowActiveHaltWakeUpCmd"
             77 41 63 74 69 76 65
             48 61 6C 74 57 61 6B
             65 55 70 43 6D 64
      00043C 00                    5862 	.db	0
      00043D 00 00 02 58           5863 	.dw	0,600
      000441 43 4C 4B 5F 50 65 72  5864 	.ascii "CLK_PeripheralClockConfig"
             69 70 68 65 72 61 6C
             43 6C 6F 63 6B 43 6F
             6E 66 69 67
      00045A 00                    5865 	.db	0
      00045B 00 00 02 E1           5866 	.dw	0,737
      00045F 43 4C 4B 5F 43 6C 6F  5867 	.ascii "CLK_ClockSwitchConfig"
             63 6B 53 77 69 74 63
             68 43 6F 6E 66 69 67
      000474 00                    5868 	.db	0
      000475 00 00 04 4B           5869 	.dw	0,1099
      000479 43 4C 4B 5F 48 53 49  5870 	.ascii "CLK_HSIPrescalerConfig"
             50 72 65 73 63 61 6C
             65 72 43 6F 6E 66 69
             67
      00048F 00                    5871 	.db	0
      000490 00 00 04 8A           5872 	.dw	0,1162
      000494 43 4C 4B 5F 43 43 4F  5873 	.ascii "CLK_CCOConfig"
             43 6F 6E 66 69 67
      0004A1 00                    5874 	.db	0
      0004A2 00 00 04 BB           5875 	.dw	0,1211
      0004A6 43 4C 4B 5F 49 54 43  5876 	.ascii "CLK_ITConfig"
             6F 6E 66 69 67
      0004B2 00                    5877 	.db	0
      0004B3 00 00 05 1D           5878 	.dw	0,1309
      0004B7 43 4C 4B 5F 53 59 53  5879 	.ascii "CLK_SYSCLKConfig"
             43 4C 4B 43 6F 6E 66
             69 67
      0004C7 00                    5880 	.db	0
      0004C8 00 00 05 69           5881 	.dw	0,1385
      0004CC 43 4C 4B 5F 53 57 49  5882 	.ascii "CLK_SWIMConfig"
             4D 43 6F 6E 66 69 67
      0004DA 00                    5883 	.db	0
      0004DB 00 00 05 B5           5884 	.dw	0,1461
      0004DF 43 4C 4B 5F 43 6C 6F  5885 	.ascii "CLK_ClockSecuritySystemEnable"
             63 6B 53 65 63 75 72
             69 74 79 53 79 73 74
             65 6D 45 6E 61 62 6C
             65
      0004FC 00                    5886 	.db	0
      0004FD 00 00 05 E1           5887 	.dw	0,1505
      000501 43 4C 4B 5F 47 65 74  5888 	.ascii "CLK_GetSYSCLKSource"
             53 59 53 43 4C 4B 53
             6F 75 72 63 65
      000514 00                    5889 	.db	0
      000515 00 00 06 18           5890 	.dw	0,1560
      000519 43 4C 4B 5F 47 65 74  5891 	.ascii "CLK_GetClockFreq"
             43 6C 6F 63 6B 46 72
             65 71
      000529 00                    5892 	.db	0
      00052A 00 00 06 AA           5893 	.dw	0,1706
      00052E 43 4C 4B 5F 41 64 6A  5894 	.ascii "CLK_AdjustHSICalibrationValue"
             75 73 74 48 53 49 43
             61 6C 69 62 72 61 74
             69 6F 6E 56 61 6C 75
             65
      00054B 00                    5895 	.db	0
      00054C 00 00 06 FB           5896 	.dw	0,1787
      000550 43 4C 4B 5F 53 59 53  5897 	.ascii "CLK_SYSCLKEmergencyClear"
             43 4C 4B 45 6D 65 72
             67 65 6E 63 79 43 6C
             65 61 72
      000568 00                    5898 	.db	0
      000569 00 00 07 22           5899 	.dw	0,1826
      00056D 43 4C 4B 5F 47 65 74  5900 	.ascii "CLK_GetFlagStatus"
             46 6C 61 67 53 74 61
             74 75 73
      00057E 00                    5901 	.db	0
      00057F 00 00 07 E0           5902 	.dw	0,2016
      000583 43 4C 4B 5F 47 65 74  5903 	.ascii "CLK_GetITStatus"
             49 54 53 74 61 74 75
             73
      000592 00                    5904 	.db	0
      000593 00 00 08 5F           5905 	.dw	0,2143
      000597 43 4C 4B 5F 43 6C 65  5906 	.ascii "CLK_ClearITPendingBit"
             61 72 49 54 50 65 6E
             64 69 6E 67 42 69 74
      0005AC 00                    5907 	.db	0
      0005AD 00 00 08 BB           5908 	.dw	0,2235
      0005B1 48 53 49 44 69 76 46  5909 	.ascii "HSIDivFactor"
             61 63 74 6F 72
      0005BD 00                    5910 	.db	0
      0005BE 00 00 08 E1           5911 	.dw	0,2273
      0005C2 43 4C 4B 50 72 65 73  5912 	.ascii "CLKPrescTable"
             63 54 61 62 6C 65
      0005CF 00                    5913 	.db	0
      0005D0 00 00 00 00           5914 	.dw	0,0
      0005D4                       5915 Ldebug_pubnames_end:
                                   5916 
                                   5917 	.area .debug_frame (NOLOAD)
      000B4A 00 00                 5918 	.dw	0
      000B4C 00 0E                 5919 	.dw	Ldebug_CIE0_end-Ldebug_CIE0_start
      000B4E                       5920 Ldebug_CIE0_start:
      000B4E FF FF                 5921 	.dw	0xffff
      000B50 FF FF                 5922 	.dw	0xffff
      000B52 01                    5923 	.db	1
      000B53 00                    5924 	.db	0
      000B54 01                    5925 	.uleb128	1
      000B55 7F                    5926 	.sleb128	-1
      000B56 09                    5927 	.db	9
      000B57 0C                    5928 	.db	12
      000B58 08                    5929 	.uleb128	8
      000B59 02                    5930 	.uleb128	2
      000B5A 89                    5931 	.db	137
      000B5B 01                    5932 	.uleb128	1
      000B5C                       5933 Ldebug_CIE0_end:
      000B5C 00 00 00 67           5934 	.dw	0,103
      000B60 00 00 0B 4A           5935 	.dw	0,(Ldebug_CIE0_start-4)
      000B64 00 00 8B D3           5936 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$646)	;initial loc
      000B68 00 00 00 33           5937 	.dw	0,Sstm8s_clk$CLK_ClearITPendingBit$669-Sstm8s_clk$CLK_ClearITPendingBit$646
      000B6C 01                    5938 	.db	1
      000B6D 00 00 8B D3           5939 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$646)
      000B71 0E                    5940 	.db	14
      000B72 02                    5941 	.uleb128	2
      000B73 01                    5942 	.db	1
      000B74 00 00 8B DC           5943 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$648)
      000B78 0E                    5944 	.db	14
      000B79 02                    5945 	.uleb128	2
      000B7A 01                    5946 	.db	1
      000B7B 00 00 8B E0           5947 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$649)
      000B7F 0E                    5948 	.db	14
      000B80 03                    5949 	.uleb128	3
      000B81 01                    5950 	.db	1
      000B82 00 00 8B E5           5951 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$650)
      000B86 0E                    5952 	.db	14
      000B87 02                    5953 	.uleb128	2
      000B88 01                    5954 	.db	1
      000B89 00 00 8B E7           5955 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$651)
      000B8D 0E                    5956 	.db	14
      000B8E 02                    5957 	.uleb128	2
      000B8F 01                    5958 	.db	1
      000B90 00 00 8B E8           5959 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$652)
      000B94 0E                    5960 	.db	14
      000B95 03                    5961 	.uleb128	3
      000B96 01                    5962 	.db	1
      000B97 00 00 8B EA           5963 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$653)
      000B9B 0E                    5964 	.db	14
      000B9C 04                    5965 	.uleb128	4
      000B9D 01                    5966 	.db	1
      000B9E 00 00 8B EC           5967 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$654)
      000BA2 0E                    5968 	.db	14
      000BA3 05                    5969 	.uleb128	5
      000BA4 01                    5970 	.db	1
      000BA5 00 00 8B EE           5971 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$655)
      000BA9 0E                    5972 	.db	14
      000BAA 07                    5973 	.uleb128	7
      000BAB 01                    5974 	.db	1
      000BAC 00 00 8B F0           5975 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$656)
      000BB0 0E                    5976 	.db	14
      000BB1 08                    5977 	.uleb128	8
      000BB2 01                    5978 	.db	1
      000BB3 00 00 8B F2           5979 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$657)
      000BB7 0E                    5980 	.db	14
      000BB8 09                    5981 	.uleb128	9
      000BB9 01                    5982 	.db	1
      000BBA 00 00 8B F7           5983 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$658)
      000BBE 0E                    5984 	.db	14
      000BBF 03                    5985 	.uleb128	3
      000BC0 01                    5986 	.db	1
      000BC1 00 00 8B F8           5987 	.dw	0,(Sstm8s_clk$CLK_ClearITPendingBit$659)
      000BC5 0E                    5988 	.db	14
      000BC6 02                    5989 	.uleb128	2
                                   5990 
                                   5991 	.area .debug_frame (NOLOAD)
      000BC7 00 00                 5992 	.dw	0
      000BC9 00 0E                 5993 	.dw	Ldebug_CIE1_end-Ldebug_CIE1_start
      000BCB                       5994 Ldebug_CIE1_start:
      000BCB FF FF                 5995 	.dw	0xffff
      000BCD FF FF                 5996 	.dw	0xffff
      000BCF 01                    5997 	.db	1
      000BD0 00                    5998 	.db	0
      000BD1 01                    5999 	.uleb128	1
      000BD2 7F                    6000 	.sleb128	-1
      000BD3 09                    6001 	.db	9
      000BD4 0C                    6002 	.db	12
      000BD5 08                    6003 	.uleb128	8
      000BD6 02                    6004 	.uleb128	2
      000BD7 89                    6005 	.db	137
      000BD8 01                    6006 	.uleb128	1
      000BD9                       6007 Ldebug_CIE1_end:
      000BD9 00 00 00 75           6008 	.dw	0,117
      000BDD 00 00 0B C7           6009 	.dw	0,(Ldebug_CIE1_start-4)
      000BE1 00 00 8B 8C           6010 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$608)	;initial loc
      000BE5 00 00 00 47           6011 	.dw	0,Sstm8s_clk$CLK_GetITStatus$644-Sstm8s_clk$CLK_GetITStatus$608
      000BE9 01                    6012 	.db	1
      000BEA 00 00 8B 8C           6013 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$608)
      000BEE 0E                    6014 	.db	14
      000BEF 02                    6015 	.uleb128	2
      000BF0 01                    6016 	.db	1
      000BF1 00 00 8B 95           6017 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$610)
      000BF5 0E                    6018 	.db	14
      000BF6 02                    6019 	.uleb128	2
      000BF7 01                    6020 	.db	1
      000BF8 00 00 8B 96           6021 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$611)
      000BFC 0E                    6022 	.db	14
      000BFD 03                    6023 	.uleb128	3
      000BFE 01                    6024 	.db	1
      000BFF 00 00 8B 9B           6025 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$612)
      000C03 0E                    6026 	.db	14
      000C04 02                    6027 	.uleb128	2
      000C05 01                    6028 	.db	1
      000C06 00 00 8B 9D           6029 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$613)
      000C0A 0E                    6030 	.db	14
      000C0B 02                    6031 	.uleb128	2
      000C0C 01                    6032 	.db	1
      000C0D 00 00 8B A1           6033 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$614)
      000C11 0E                    6034 	.db	14
      000C12 03                    6035 	.uleb128	3
      000C13 01                    6036 	.db	1
      000C14 00 00 8B A3           6037 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$615)
      000C18 0E                    6038 	.db	14
      000C19 04                    6039 	.uleb128	4
      000C1A 01                    6040 	.db	1
      000C1B 00 00 8B A5           6041 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$616)
      000C1F 0E                    6042 	.db	14
      000C20 05                    6043 	.uleb128	5
      000C21 01                    6044 	.db	1
      000C22 00 00 8B A7           6045 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$617)
      000C26 0E                    6046 	.db	14
      000C27 07                    6047 	.uleb128	7
      000C28 01                    6048 	.db	1
      000C29 00 00 8B A9           6049 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$618)
      000C2D 0E                    6050 	.db	14
      000C2E 08                    6051 	.uleb128	8
      000C2F 01                    6052 	.db	1
      000C30 00 00 8B AB           6053 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$619)
      000C34 0E                    6054 	.db	14
      000C35 09                    6055 	.uleb128	9
      000C36 01                    6056 	.db	1
      000C37 00 00 8B B0           6057 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$620)
      000C3B 0E                    6058 	.db	14
      000C3C 03                    6059 	.uleb128	3
      000C3D 01                    6060 	.db	1
      000C3E 00 00 8B B1           6061 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$621)
      000C42 0E                    6062 	.db	14
      000C43 02                    6063 	.uleb128	2
      000C44 01                    6064 	.db	1
      000C45 00 00 8B BD           6065 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$625)
      000C49 0E                    6066 	.db	14
      000C4A 02                    6067 	.uleb128	2
      000C4B 01                    6068 	.db	1
      000C4C 00 00 8B CD           6069 	.dw	0,(Sstm8s_clk$CLK_GetITStatus$634)
      000C50 0E                    6070 	.db	14
      000C51 02                    6071 	.uleb128	2
                                   6072 
                                   6073 	.area .debug_frame (NOLOAD)
      000C52 00 00                 6074 	.dw	0
      000C54 00 0E                 6075 	.dw	Ldebug_CIE2_end-Ldebug_CIE2_start
      000C56                       6076 Ldebug_CIE2_start:
      000C56 FF FF                 6077 	.dw	0xffff
      000C58 FF FF                 6078 	.dw	0xffff
      000C5A 01                    6079 	.db	1
      000C5B 00                    6080 	.db	0
      000C5C 01                    6081 	.uleb128	1
      000C5D 7F                    6082 	.sleb128	-1
      000C5E 09                    6083 	.db	9
      000C5F 0C                    6084 	.db	12
      000C60 08                    6085 	.uleb128	8
      000C61 02                    6086 	.uleb128	2
      000C62 89                    6087 	.db	137
      000C63 01                    6088 	.uleb128	1
      000C64                       6089 Ldebug_CIE2_end:
      000C64 00 00 00 C9           6090 	.dw	0,201
      000C68 00 00 0C 52           6091 	.dw	0,(Ldebug_CIE2_start-4)
      000C6C 00 00 8B 01           6092 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$548)	;initial loc
      000C70 00 00 00 8B           6093 	.dw	0,Sstm8s_clk$CLK_GetFlagStatus$606-Sstm8s_clk$CLK_GetFlagStatus$548
      000C74 01                    6094 	.db	1
      000C75 00 00 8B 01           6095 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$548)
      000C79 0E                    6096 	.db	14
      000C7A 02                    6097 	.uleb128	2
      000C7B 01                    6098 	.db	1
      000C7C 00 00 8B 02           6099 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$549)
      000C80 0E                    6100 	.db	14
      000C81 03                    6101 	.uleb128	3
      000C82 01                    6102 	.db	1
      000C83 00 00 8B 0C           6103 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$551)
      000C87 0E                    6104 	.db	14
      000C88 03                    6105 	.uleb128	3
      000C89 01                    6106 	.db	1
      000C8A 00 00 8B 14           6107 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$552)
      000C8E 0E                    6108 	.db	14
      000C8F 03                    6109 	.uleb128	3
      000C90 01                    6110 	.db	1
      000C91 00 00 8B 1C           6111 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$553)
      000C95 0E                    6112 	.db	14
      000C96 03                    6113 	.uleb128	3
      000C97 01                    6114 	.db	1
      000C98 00 00 8B 21           6115 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$554)
      000C9C 0E                    6116 	.db	14
      000C9D 03                    6117 	.uleb128	3
      000C9E 01                    6118 	.db	1
      000C9F 00 00 8B 26           6119 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$555)
      000CA3 0E                    6120 	.db	14
      000CA4 03                    6121 	.uleb128	3
      000CA5 01                    6122 	.db	1
      000CA6 00 00 8B 2B           6123 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$556)
      000CAA 0E                    6124 	.db	14
      000CAB 03                    6125 	.uleb128	3
      000CAC 01                    6126 	.db	1
      000CAD 00 00 8B 30           6127 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$557)
      000CB1 0E                    6128 	.db	14
      000CB2 03                    6129 	.uleb128	3
      000CB3 01                    6130 	.db	1
      000CB4 00 00 8B 35           6131 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$558)
      000CB8 0E                    6132 	.db	14
      000CB9 03                    6133 	.uleb128	3
      000CBA 01                    6134 	.db	1
      000CBB 00 00 8B 3A           6135 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$559)
      000CBF 0E                    6136 	.db	14
      000CC0 03                    6137 	.uleb128	3
      000CC1 01                    6138 	.db	1
      000CC2 00 00 8B 3B           6139 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$560)
      000CC6 0E                    6140 	.db	14
      000CC7 05                    6141 	.uleb128	5
      000CC8 01                    6142 	.db	1
      000CC9 00 00 8B 3D           6143 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$561)
      000CCD 0E                    6144 	.db	14
      000CCE 06                    6145 	.uleb128	6
      000CCF 01                    6146 	.db	1
      000CD0 00 00 8B 3F           6147 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$562)
      000CD4 0E                    6148 	.db	14
      000CD5 07                    6149 	.uleb128	7
      000CD6 01                    6150 	.db	1
      000CD7 00 00 8B 41           6151 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$563)
      000CDB 0E                    6152 	.db	14
      000CDC 08                    6153 	.uleb128	8
      000CDD 01                    6154 	.db	1
      000CDE 00 00 8B 43           6155 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$564)
      000CE2 0E                    6156 	.db	14
      000CE3 09                    6157 	.uleb128	9
      000CE4 01                    6158 	.db	1
      000CE5 00 00 8B 45           6159 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$565)
      000CE9 0E                    6160 	.db	14
      000CEA 0A                    6161 	.uleb128	10
      000CEB 01                    6162 	.db	1
      000CEC 00 00 8B 47           6163 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$566)
      000CF0 0E                    6164 	.db	14
      000CF1 0B                    6165 	.uleb128	11
      000CF2 01                    6166 	.db	1
      000CF3 00 00 8B 4C           6167 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$567)
      000CF7 0E                    6168 	.db	14
      000CF8 05                    6169 	.uleb128	5
      000CF9 01                    6170 	.db	1
      000CFA 00 00 8B 4D           6171 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$568)
      000CFE 0E                    6172 	.db	14
      000CFF 03                    6173 	.uleb128	3
      000D00 01                    6174 	.db	1
      000D01 00 00 8B 54           6175 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$571)
      000D05 0E                    6176 	.db	14
      000D06 03                    6177 	.uleb128	3
      000D07 01                    6178 	.db	1
      000D08 00 00 8B 5E           6179 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$576)
      000D0C 0E                    6180 	.db	14
      000D0D 03                    6181 	.uleb128	3
      000D0E 01                    6182 	.db	1
      000D0F 00 00 8B 68           6183 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$581)
      000D13 0E                    6184 	.db	14
      000D14 03                    6185 	.uleb128	3
      000D15 01                    6186 	.db	1
      000D16 00 00 8B 72           6187 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$586)
      000D1A 0E                    6188 	.db	14
      000D1B 03                    6189 	.uleb128	3
      000D1C 01                    6190 	.db	1
      000D1D 00 00 8B 7B           6191 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$594)
      000D21 0E                    6192 	.db	14
      000D22 04                    6193 	.uleb128	4
      000D23 01                    6194 	.db	1
      000D24 00 00 8B 80           6195 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$595)
      000D28 0E                    6196 	.db	14
      000D29 03                    6197 	.uleb128	3
      000D2A 01                    6198 	.db	1
      000D2B 00 00 8B 8B           6199 	.dw	0,(Sstm8s_clk$CLK_GetFlagStatus$604)
      000D2F 0E                    6200 	.db	14
      000D30 02                    6201 	.uleb128	2
                                   6202 
                                   6203 	.area .debug_frame (NOLOAD)
      000D31 00 00                 6204 	.dw	0
      000D33 00 0E                 6205 	.dw	Ldebug_CIE3_end-Ldebug_CIE3_start
      000D35                       6206 Ldebug_CIE3_start:
      000D35 FF FF                 6207 	.dw	0xffff
      000D37 FF FF                 6208 	.dw	0xffff
      000D39 01                    6209 	.db	1
      000D3A 00                    6210 	.db	0
      000D3B 01                    6211 	.uleb128	1
      000D3C 7F                    6212 	.sleb128	-1
      000D3D 09                    6213 	.db	9
      000D3E 0C                    6214 	.db	12
      000D3F 08                    6215 	.uleb128	8
      000D40 02                    6216 	.uleb128	2
      000D41 89                    6217 	.db	137
      000D42 01                    6218 	.uleb128	1
      000D43                       6219 Ldebug_CIE3_end:
      000D43 00 00 00 13           6220 	.dw	0,19
      000D47 00 00 0D 31           6221 	.dw	0,(Ldebug_CIE3_start-4)
      000D4B 00 00 8A FC           6222 	.dw	0,(Sstm8s_clk$CLK_SYSCLKEmergencyClear$542)	;initial loc
      000D4F 00 00 00 05           6223 	.dw	0,Sstm8s_clk$CLK_SYSCLKEmergencyClear$546-Sstm8s_clk$CLK_SYSCLKEmergencyClear$542
      000D53 01                    6224 	.db	1
      000D54 00 00 8A FC           6225 	.dw	0,(Sstm8s_clk$CLK_SYSCLKEmergencyClear$542)
      000D58 0E                    6226 	.db	14
      000D59 02                    6227 	.uleb128	2
                                   6228 
                                   6229 	.area .debug_frame (NOLOAD)
      000D5A 00 00                 6230 	.dw	0
      000D5C 00 0E                 6231 	.dw	Ldebug_CIE4_end-Ldebug_CIE4_start
      000D5E                       6232 Ldebug_CIE4_start:
      000D5E FF FF                 6233 	.dw	0xffff
      000D60 FF FF                 6234 	.dw	0xffff
      000D62 01                    6235 	.db	1
      000D63 00                    6236 	.db	0
      000D64 01                    6237 	.uleb128	1
      000D65 7F                    6238 	.sleb128	-1
      000D66 09                    6239 	.db	9
      000D67 0C                    6240 	.db	12
      000D68 08                    6241 	.uleb128	8
      000D69 02                    6242 	.uleb128	2
      000D6A 89                    6243 	.db	137
      000D6B 01                    6244 	.uleb128	1
      000D6C                       6245 Ldebug_CIE4_end:
      000D6C 00 00 00 6E           6246 	.dw	0,110
      000D70 00 00 0D 5A           6247 	.dw	0,(Ldebug_CIE4_start-4)
      000D74 00 00 8A B5           6248 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$522)	;initial loc
      000D78 00 00 00 47           6249 	.dw	0,Sstm8s_clk$CLK_AdjustHSICalibrationValue$540-Sstm8s_clk$CLK_AdjustHSICalibrationValue$522
      000D7C 01                    6250 	.db	1
      000D7D 00 00 8A B5           6251 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$522)
      000D81 0E                    6252 	.db	14
      000D82 02                    6253 	.uleb128	2
      000D83 01                    6254 	.db	1
      000D84 00 00 8A BE           6255 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$524)
      000D88 0E                    6256 	.db	14
      000D89 02                    6257 	.uleb128	2
      000D8A 01                    6258 	.db	1
      000D8B 00 00 8A C4           6259 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$525)
      000D8F 0E                    6260 	.db	14
      000D90 02                    6261 	.uleb128	2
      000D91 01                    6262 	.db	1
      000D92 00 00 8A CA           6263 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$526)
      000D96 0E                    6264 	.db	14
      000D97 02                    6265 	.uleb128	2
      000D98 01                    6266 	.db	1
      000D99 00 00 8A D0           6267 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$527)
      000D9D 0E                    6268 	.db	14
      000D9E 02                    6269 	.uleb128	2
      000D9F 01                    6270 	.db	1
      000DA0 00 00 8A D6           6271 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$528)
      000DA4 0E                    6272 	.db	14
      000DA5 02                    6273 	.uleb128	2
      000DA6 01                    6274 	.db	1
      000DA7 00 00 8A DC           6275 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$529)
      000DAB 0E                    6276 	.db	14
      000DAC 02                    6277 	.uleb128	2
      000DAD 01                    6278 	.db	1
      000DAE 00 00 8A E2           6279 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$530)
      000DB2 0E                    6280 	.db	14
      000DB3 02                    6281 	.uleb128	2
      000DB4 01                    6282 	.db	1
      000DB5 00 00 8A E4           6283 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$531)
      000DB9 0E                    6284 	.db	14
      000DBA 03                    6285 	.uleb128	3
      000DBB 01                    6286 	.db	1
      000DBC 00 00 8A E6           6287 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$532)
      000DC0 0E                    6288 	.db	14
      000DC1 04                    6289 	.uleb128	4
      000DC2 01                    6290 	.db	1
      000DC3 00 00 8A E8           6291 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$533)
      000DC7 0E                    6292 	.db	14
      000DC8 06                    6293 	.uleb128	6
      000DC9 01                    6294 	.db	1
      000DCA 00 00 8A EA           6295 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$534)
      000DCE 0E                    6296 	.db	14
      000DCF 07                    6297 	.uleb128	7
      000DD0 01                    6298 	.db	1
      000DD1 00 00 8A EC           6299 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$535)
      000DD5 0E                    6300 	.db	14
      000DD6 08                    6301 	.uleb128	8
      000DD7 01                    6302 	.db	1
      000DD8 00 00 8A F1           6303 	.dw	0,(Sstm8s_clk$CLK_AdjustHSICalibrationValue$536)
      000DDC 0E                    6304 	.db	14
      000DDD 02                    6305 	.uleb128	2
                                   6306 
                                   6307 	.area .debug_frame (NOLOAD)
      000DDE 00 00                 6308 	.dw	0
      000DE0 00 0E                 6309 	.dw	Ldebug_CIE5_end-Ldebug_CIE5_start
      000DE2                       6310 Ldebug_CIE5_start:
      000DE2 FF FF                 6311 	.dw	0xffff
      000DE4 FF FF                 6312 	.dw	0xffff
      000DE6 01                    6313 	.db	1
      000DE7 00                    6314 	.db	0
      000DE8 01                    6315 	.uleb128	1
      000DE9 7F                    6316 	.sleb128	-1
      000DEA 09                    6317 	.db	9
      000DEB 0C                    6318 	.db	12
      000DEC 08                    6319 	.uleb128	8
      000DED 02                    6320 	.uleb128	2
      000DEE 89                    6321 	.db	137
      000DEF 01                    6322 	.uleb128	1
      000DF0                       6323 Ldebug_CIE5_end:
      000DF0 00 00 00 60           6324 	.dw	0,96
      000DF4 00 00 0D DE           6325 	.dw	0,(Ldebug_CIE5_start-4)
      000DF8 00 00 8A 65           6326 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$490)	;initial loc
      000DFC 00 00 00 50           6327 	.dw	0,Sstm8s_clk$CLK_GetClockFreq$520-Sstm8s_clk$CLK_GetClockFreq$490
      000E00 01                    6328 	.db	1
      000E01 00 00 8A 65           6329 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$490)
      000E05 0E                    6330 	.db	14
      000E06 02                    6331 	.uleb128	2
      000E07 01                    6332 	.db	1
      000E08 00 00 8A 67           6333 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$491)
      000E0C 0E                    6334 	.db	14
      000E0D 06                    6335 	.uleb128	6
      000E0E 01                    6336 	.db	1
      000E0F 00 00 8A 72           6337 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$494)
      000E13 0E                    6338 	.db	14
      000E14 06                    6339 	.uleb128	6
      000E15 01                    6340 	.db	1
      000E16 00 00 8A 84           6341 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$501)
      000E1A 0E                    6342 	.db	14
      000E1B 08                    6343 	.uleb128	8
      000E1C 01                    6344 	.db	1
      000E1D 00 00 8A 86           6345 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$502)
      000E21 0E                    6346 	.db	14
      000E22 0A                    6347 	.uleb128	10
      000E23 01                    6348 	.db	1
      000E24 00 00 8A 88           6349 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$503)
      000E28 0E                    6350 	.db	14
      000E29 0B                    6351 	.uleb128	11
      000E2A 01                    6352 	.db	1
      000E2B 00 00 8A 8A           6353 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$504)
      000E2F 0E                    6354 	.db	14
      000E30 0C                    6355 	.uleb128	12
      000E31 01                    6356 	.db	1
      000E32 00 00 8A 8C           6357 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$505)
      000E36 0E                    6358 	.db	14
      000E37 0D                    6359 	.uleb128	13
      000E38 01                    6360 	.db	1
      000E39 00 00 8A 8E           6361 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$506)
      000E3D 0E                    6362 	.db	14
      000E3E 0E                    6363 	.uleb128	14
      000E3F 01                    6364 	.db	1
      000E40 00 00 8A 93           6365 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$507)
      000E44 0E                    6366 	.db	14
      000E45 06                    6367 	.uleb128	6
      000E46 01                    6368 	.db	1
      000E47 00 00 8A 9E           6369 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$509)
      000E4B 0E                    6370 	.db	14
      000E4C 06                    6371 	.uleb128	6
      000E4D 01                    6372 	.db	1
      000E4E 00 00 8A B4           6373 	.dw	0,(Sstm8s_clk$CLK_GetClockFreq$518)
      000E52 0E                    6374 	.db	14
      000E53 02                    6375 	.uleb128	2
                                   6376 
                                   6377 	.area .debug_frame (NOLOAD)
      000E54 00 00                 6378 	.dw	0
      000E56 00 0E                 6379 	.dw	Ldebug_CIE6_end-Ldebug_CIE6_start
      000E58                       6380 Ldebug_CIE6_start:
      000E58 FF FF                 6381 	.dw	0xffff
      000E5A FF FF                 6382 	.dw	0xffff
      000E5C 01                    6383 	.db	1
      000E5D 00                    6384 	.db	0
      000E5E 01                    6385 	.uleb128	1
      000E5F 7F                    6386 	.sleb128	-1
      000E60 09                    6387 	.db	9
      000E61 0C                    6388 	.db	12
      000E62 08                    6389 	.uleb128	8
      000E63 02                    6390 	.uleb128	2
      000E64 89                    6391 	.db	137
      000E65 01                    6392 	.uleb128	1
      000E66                       6393 Ldebug_CIE6_end:
      000E66 00 00 00 13           6394 	.dw	0,19
      000E6A 00 00 0E 54           6395 	.dw	0,(Ldebug_CIE6_start-4)
      000E6E 00 00 8A 61           6396 	.dw	0,(Sstm8s_clk$CLK_GetSYSCLKSource$484)	;initial loc
      000E72 00 00 00 04           6397 	.dw	0,Sstm8s_clk$CLK_GetSYSCLKSource$488-Sstm8s_clk$CLK_GetSYSCLKSource$484
      000E76 01                    6398 	.db	1
      000E77 00 00 8A 61           6399 	.dw	0,(Sstm8s_clk$CLK_GetSYSCLKSource$484)
      000E7B 0E                    6400 	.db	14
      000E7C 02                    6401 	.uleb128	2
                                   6402 
                                   6403 	.area .debug_frame (NOLOAD)
      000E7D 00 00                 6404 	.dw	0
      000E7F 00 0E                 6405 	.dw	Ldebug_CIE7_end-Ldebug_CIE7_start
      000E81                       6406 Ldebug_CIE7_start:
      000E81 FF FF                 6407 	.dw	0xffff
      000E83 FF FF                 6408 	.dw	0xffff
      000E85 01                    6409 	.db	1
      000E86 00                    6410 	.db	0
      000E87 01                    6411 	.uleb128	1
      000E88 7F                    6412 	.sleb128	-1
      000E89 09                    6413 	.db	9
      000E8A 0C                    6414 	.db	12
      000E8B 08                    6415 	.uleb128	8
      000E8C 02                    6416 	.uleb128	2
      000E8D 89                    6417 	.db	137
      000E8E 01                    6418 	.uleb128	1
      000E8F                       6419 Ldebug_CIE7_end:
      000E8F 00 00 00 13           6420 	.dw	0,19
      000E93 00 00 0E 7D           6421 	.dw	0,(Ldebug_CIE7_start-4)
      000E97 00 00 8A 5C           6422 	.dw	0,(Sstm8s_clk$CLK_ClockSecuritySystemEnable$478)	;initial loc
      000E9B 00 00 00 05           6423 	.dw	0,Sstm8s_clk$CLK_ClockSecuritySystemEnable$482-Sstm8s_clk$CLK_ClockSecuritySystemEnable$478
      000E9F 01                    6424 	.db	1
      000EA0 00 00 8A 5C           6425 	.dw	0,(Sstm8s_clk$CLK_ClockSecuritySystemEnable$478)
      000EA4 0E                    6426 	.db	14
      000EA5 02                    6427 	.uleb128	2
                                   6428 
                                   6429 	.area .debug_frame (NOLOAD)
      000EA6 00 00                 6430 	.dw	0
      000EA8 00 0E                 6431 	.dw	Ldebug_CIE8_end-Ldebug_CIE8_start
      000EAA                       6432 Ldebug_CIE8_start:
      000EAA FF FF                 6433 	.dw	0xffff
      000EAC FF FF                 6434 	.dw	0xffff
      000EAE 01                    6435 	.db	1
      000EAF 00                    6436 	.db	0
      000EB0 01                    6437 	.uleb128	1
      000EB1 7F                    6438 	.sleb128	-1
      000EB2 09                    6439 	.db	9
      000EB3 0C                    6440 	.db	12
      000EB4 08                    6441 	.uleb128	8
      000EB5 02                    6442 	.uleb128	2
      000EB6 89                    6443 	.db	137
      000EB7 01                    6444 	.uleb128	1
      000EB8                       6445 Ldebug_CIE8_end:
      000EB8 00 00 00 44           6446 	.dw	0,68
      000EBC 00 00 0E A6           6447 	.dw	0,(Ldebug_CIE8_start-4)
      000EC0 00 00 8A 30           6448 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$457)	;initial loc
      000EC4 00 00 00 2C           6449 	.dw	0,Sstm8s_clk$CLK_SWIMConfig$476-Sstm8s_clk$CLK_SWIMConfig$457
      000EC8 01                    6450 	.db	1
      000EC9 00 00 8A 30           6451 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$457)
      000ECD 0E                    6452 	.db	14
      000ECE 02                    6453 	.uleb128	2
      000ECF 01                    6454 	.db	1
      000ED0 00 00 8A 39           6455 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$459)
      000ED4 0E                    6456 	.db	14
      000ED5 02                    6457 	.uleb128	2
      000ED6 01                    6458 	.db	1
      000ED7 00 00 8A 3B           6459 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$460)
      000EDB 0E                    6460 	.db	14
      000EDC 03                    6461 	.uleb128	3
      000EDD 01                    6462 	.db	1
      000EDE 00 00 8A 3D           6463 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$461)
      000EE2 0E                    6464 	.db	14
      000EE3 04                    6465 	.uleb128	4
      000EE4 01                    6466 	.db	1
      000EE5 00 00 8A 3F           6467 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$462)
      000EE9 0E                    6468 	.db	14
      000EEA 06                    6469 	.uleb128	6
      000EEB 01                    6470 	.db	1
      000EEC 00 00 8A 41           6471 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$463)
      000EF0 0E                    6472 	.db	14
      000EF1 07                    6473 	.uleb128	7
      000EF2 01                    6474 	.db	1
      000EF3 00 00 8A 43           6475 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$464)
      000EF7 0E                    6476 	.db	14
      000EF8 08                    6477 	.uleb128	8
      000EF9 01                    6478 	.db	1
      000EFA 00 00 8A 48           6479 	.dw	0,(Sstm8s_clk$CLK_SWIMConfig$465)
      000EFE 0E                    6480 	.db	14
      000EFF 02                    6481 	.uleb128	2
                                   6482 
                                   6483 	.area .debug_frame (NOLOAD)
      000F00 00 00                 6484 	.dw	0
      000F02 00 0E                 6485 	.dw	Ldebug_CIE9_end-Ldebug_CIE9_start
      000F04                       6486 Ldebug_CIE9_start:
      000F04 FF FF                 6487 	.dw	0xffff
      000F06 FF FF                 6488 	.dw	0xffff
      000F08 01                    6489 	.db	1
      000F09 00                    6490 	.db	0
      000F0A 01                    6491 	.uleb128	1
      000F0B 7F                    6492 	.sleb128	-1
      000F0C 09                    6493 	.db	9
      000F0D 0C                    6494 	.db	12
      000F0E 08                    6495 	.uleb128	8
      000F0F 02                    6496 	.uleb128	2
      000F10 89                    6497 	.db	137
      000F11 01                    6498 	.uleb128	1
      000F12                       6499 Ldebug_CIE9_end:
      000F12 00 00 00 98           6500 	.dw	0,152
      000F16 00 00 0F 00           6501 	.dw	0,(Ldebug_CIE9_start-4)
      000F1A 00 00 89 9A           6502 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$422)	;initial loc
      000F1E 00 00 00 96           6503 	.dw	0,Sstm8s_clk$CLK_SYSCLKConfig$455-Sstm8s_clk$CLK_SYSCLKConfig$422
      000F22 01                    6504 	.db	1
      000F23 00 00 89 9A           6505 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$422)
      000F27 0E                    6506 	.db	14
      000F28 02                    6507 	.uleb128	2
      000F29 01                    6508 	.db	1
      000F2A 00 00 89 9B           6509 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$423)
      000F2E 0E                    6510 	.db	14
      000F2F 03                    6511 	.uleb128	3
      000F30 01                    6512 	.db	1
      000F31 00 00 89 AB           6513 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$425)
      000F35 0E                    6514 	.db	14
      000F36 03                    6515 	.uleb128	3
      000F37 01                    6516 	.db	1
      000F38 00 00 89 B4           6517 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$426)
      000F3C 0E                    6518 	.db	14
      000F3D 03                    6519 	.uleb128	3
      000F3E 01                    6520 	.db	1
      000F3F 00 00 89 BD           6521 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$427)
      000F43 0E                    6522 	.db	14
      000F44 03                    6523 	.uleb128	3
      000F45 01                    6524 	.db	1
      000F46 00 00 89 C6           6525 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$428)
      000F4A 0E                    6526 	.db	14
      000F4B 03                    6527 	.uleb128	3
      000F4C 01                    6528 	.db	1
      000F4D 00 00 89 CC           6529 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$429)
      000F51 0E                    6530 	.db	14
      000F52 03                    6531 	.uleb128	3
      000F53 01                    6532 	.db	1
      000F54 00 00 89 D2           6533 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$430)
      000F58 0E                    6534 	.db	14
      000F59 03                    6535 	.uleb128	3
      000F5A 01                    6536 	.db	1
      000F5B 00 00 89 D8           6537 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$431)
      000F5F 0E                    6538 	.db	14
      000F60 03                    6539 	.uleb128	3
      000F61 01                    6540 	.db	1
      000F62 00 00 89 DE           6541 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$432)
      000F66 0E                    6542 	.db	14
      000F67 03                    6543 	.uleb128	3
      000F68 01                    6544 	.db	1
      000F69 00 00 89 E4           6545 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$433)
      000F6D 0E                    6546 	.db	14
      000F6E 03                    6547 	.uleb128	3
      000F6F 01                    6548 	.db	1
      000F70 00 00 89 EA           6549 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$434)
      000F74 0E                    6550 	.db	14
      000F75 03                    6551 	.uleb128	3
      000F76 01                    6552 	.db	1
      000F77 00 00 89 F0           6553 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$435)
      000F7B 0E                    6554 	.db	14
      000F7C 03                    6555 	.uleb128	3
      000F7D 01                    6556 	.db	1
      000F7E 00 00 89 F2           6557 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$436)
      000F82 0E                    6558 	.db	14
      000F83 04                    6559 	.uleb128	4
      000F84 01                    6560 	.db	1
      000F85 00 00 89 F4           6561 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$437)
      000F89 0E                    6562 	.db	14
      000F8A 05                    6563 	.uleb128	5
      000F8B 01                    6564 	.db	1
      000F8C 00 00 89 F6           6565 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$438)
      000F90 0E                    6566 	.db	14
      000F91 07                    6567 	.uleb128	7
      000F92 01                    6568 	.db	1
      000F93 00 00 89 F8           6569 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$439)
      000F97 0E                    6570 	.db	14
      000F98 08                    6571 	.uleb128	8
      000F99 01                    6572 	.db	1
      000F9A 00 00 89 FA           6573 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$440)
      000F9E 0E                    6574 	.db	14
      000F9F 09                    6575 	.uleb128	9
      000FA0 01                    6576 	.db	1
      000FA1 00 00 89 FF           6577 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$441)
      000FA5 0E                    6578 	.db	14
      000FA6 03                    6579 	.uleb128	3
      000FA7 01                    6580 	.db	1
      000FA8 00 00 8A 2F           6581 	.dw	0,(Sstm8s_clk$CLK_SYSCLKConfig$453)
      000FAC 0E                    6582 	.db	14
      000FAD 02                    6583 	.uleb128	2
                                   6584 
                                   6585 	.area .debug_frame (NOLOAD)
      000FAE 00 00                 6586 	.dw	0
      000FB0 00 0E                 6587 	.dw	Ldebug_CIE10_end-Ldebug_CIE10_start
      000FB2                       6588 Ldebug_CIE10_start:
      000FB2 FF FF                 6589 	.dw	0xffff
      000FB4 FF FF                 6590 	.dw	0xffff
      000FB6 01                    6591 	.db	1
      000FB7 00                    6592 	.db	0
      000FB8 01                    6593 	.uleb128	1
      000FB9 7F                    6594 	.sleb128	-1
      000FBA 09                    6595 	.db	9
      000FBB 0C                    6596 	.db	12
      000FBC 08                    6597 	.uleb128	8
      000FBD 02                    6598 	.uleb128	2
      000FBE 89                    6599 	.db	137
      000FBF 01                    6600 	.uleb128	1
      000FC0                       6601 Ldebug_CIE10_end:
      000FC0 00 00 00 B4           6602 	.dw	0,180
      000FC4 00 00 0F AE           6603 	.dw	0,(Ldebug_CIE10_start-4)
      000FC8 00 00 89 25           6604 	.dw	0,(Sstm8s_clk$CLK_ITConfig$372)	;initial loc
      000FCC 00 00 00 75           6605 	.dw	0,Sstm8s_clk$CLK_ITConfig$420-Sstm8s_clk$CLK_ITConfig$372
      000FD0 01                    6606 	.db	1
      000FD1 00 00 89 25           6607 	.dw	0,(Sstm8s_clk$CLK_ITConfig$372)
      000FD5 0E                    6608 	.db	14
      000FD6 02                    6609 	.uleb128	2
      000FD7 01                    6610 	.db	1
      000FD8 00 00 89 26           6611 	.dw	0,(Sstm8s_clk$CLK_ITConfig$373)
      000FDC 0E                    6612 	.db	14
      000FDD 03                    6613 	.uleb128	3
      000FDE 01                    6614 	.db	1
      000FDF 00 00 89 2F           6615 	.dw	0,(Sstm8s_clk$CLK_ITConfig$375)
      000FE3 0E                    6616 	.db	14
      000FE4 03                    6617 	.uleb128	3
      000FE5 01                    6618 	.db	1
      000FE6 00 00 89 31           6619 	.dw	0,(Sstm8s_clk$CLK_ITConfig$376)
      000FEA 0E                    6620 	.db	14
      000FEB 04                    6621 	.uleb128	4
      000FEC 01                    6622 	.db	1
      000FED 00 00 89 33           6623 	.dw	0,(Sstm8s_clk$CLK_ITConfig$377)
      000FF1 0E                    6624 	.db	14
      000FF2 05                    6625 	.uleb128	5
      000FF3 01                    6626 	.db	1
      000FF4 00 00 89 35           6627 	.dw	0,(Sstm8s_clk$CLK_ITConfig$378)
      000FF8 0E                    6628 	.db	14
      000FF9 07                    6629 	.uleb128	7
      000FFA 01                    6630 	.db	1
      000FFB 00 00 89 37           6631 	.dw	0,(Sstm8s_clk$CLK_ITConfig$379)
      000FFF 0E                    6632 	.db	14
      001000 08                    6633 	.uleb128	8
      001001 01                    6634 	.db	1
      001002 00 00 89 39           6635 	.dw	0,(Sstm8s_clk$CLK_ITConfig$380)
      001006 0E                    6636 	.db	14
      001007 09                    6637 	.uleb128	9
      001008 01                    6638 	.db	1
      001009 00 00 89 3E           6639 	.dw	0,(Sstm8s_clk$CLK_ITConfig$381)
      00100D 0E                    6640 	.db	14
      00100E 03                    6641 	.uleb128	3
      00100F 01                    6642 	.db	1
      001010 00 00 89 47           6643 	.dw	0,(Sstm8s_clk$CLK_ITConfig$383)
      001014 0E                    6644 	.db	14
      001015 03                    6645 	.uleb128	3
      001016 01                    6646 	.db	1
      001017 00 00 89 48           6647 	.dw	0,(Sstm8s_clk$CLK_ITConfig$384)
      00101B 0E                    6648 	.db	14
      00101C 04                    6649 	.uleb128	4
      00101D 01                    6650 	.db	1
      00101E 00 00 89 4D           6651 	.dw	0,(Sstm8s_clk$CLK_ITConfig$385)
      001022 0E                    6652 	.db	14
      001023 03                    6653 	.uleb128	3
      001024 01                    6654 	.db	1
      001025 00 00 89 50           6655 	.dw	0,(Sstm8s_clk$CLK_ITConfig$386)
      001029 0E                    6656 	.db	14
      00102A 04                    6657 	.uleb128	4
      00102B 01                    6658 	.db	1
      00102C 00 00 89 55           6659 	.dw	0,(Sstm8s_clk$CLK_ITConfig$387)
      001030 0E                    6660 	.db	14
      001031 03                    6661 	.uleb128	3
      001032 01                    6662 	.db	1
      001033 00 00 89 58           6663 	.dw	0,(Sstm8s_clk$CLK_ITConfig$388)
      001037 0E                    6664 	.db	14
      001038 03                    6665 	.uleb128	3
      001039 01                    6666 	.db	1
      00103A 00 00 89 60           6667 	.dw	0,(Sstm8s_clk$CLK_ITConfig$389)
      00103E 0E                    6668 	.db	14
      00103F 04                    6669 	.uleb128	4
      001040 01                    6670 	.db	1
      001041 00 00 89 62           6671 	.dw	0,(Sstm8s_clk$CLK_ITConfig$390)
      001045 0E                    6672 	.db	14
      001046 05                    6673 	.uleb128	5
      001047 01                    6674 	.db	1
      001048 00 00 89 64           6675 	.dw	0,(Sstm8s_clk$CLK_ITConfig$391)
      00104C 0E                    6676 	.db	14
      00104D 06                    6677 	.uleb128	6
      00104E 01                    6678 	.db	1
      00104F 00 00 89 66           6679 	.dw	0,(Sstm8s_clk$CLK_ITConfig$392)
      001053 0E                    6680 	.db	14
      001054 08                    6681 	.uleb128	8
      001055 01                    6682 	.db	1
      001056 00 00 89 68           6683 	.dw	0,(Sstm8s_clk$CLK_ITConfig$393)
      00105A 0E                    6684 	.db	14
      00105B 09                    6685 	.uleb128	9
      00105C 01                    6686 	.db	1
      00105D 00 00 89 6A           6687 	.dw	0,(Sstm8s_clk$CLK_ITConfig$394)
      001061 0E                    6688 	.db	14
      001062 0A                    6689 	.uleb128	10
      001063 01                    6690 	.db	1
      001064 00 00 89 6F           6691 	.dw	0,(Sstm8s_clk$CLK_ITConfig$395)
      001068 0E                    6692 	.db	14
      001069 04                    6693 	.uleb128	4
      00106A 01                    6694 	.db	1
      00106B 00 00 89 70           6695 	.dw	0,(Sstm8s_clk$CLK_ITConfig$396)
      00106F 0E                    6696 	.db	14
      001070 03                    6697 	.uleb128	3
      001071 01                    6698 	.db	1
      001072 00 00 89 99           6699 	.dw	0,(Sstm8s_clk$CLK_ITConfig$418)
      001076 0E                    6700 	.db	14
      001077 02                    6701 	.uleb128	2
                                   6702 
                                   6703 	.area .debug_frame (NOLOAD)
      001078 00 00                 6704 	.dw	0
      00107A 00 0E                 6705 	.dw	Ldebug_CIE11_end-Ldebug_CIE11_start
      00107C                       6706 Ldebug_CIE11_start:
      00107C FF FF                 6707 	.dw	0xffff
      00107E FF FF                 6708 	.dw	0xffff
      001080 01                    6709 	.db	1
      001081 00                    6710 	.db	0
      001082 01                    6711 	.uleb128	1
      001083 7F                    6712 	.sleb128	-1
      001084 09                    6713 	.db	9
      001085 0C                    6714 	.db	12
      001086 08                    6715 	.uleb128	8
      001087 02                    6716 	.uleb128	2
      001088 89                    6717 	.db	137
      001089 01                    6718 	.uleb128	1
      00108A                       6719 Ldebug_CIE11_end:
      00108A 00 00 00 91           6720 	.dw	0,145
      00108E 00 00 10 78           6721 	.dw	0,(Ldebug_CIE11_start-4)
      001092 00 00 88 A3           6722 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$345)	;initial loc
      001096 00 00 00 82           6723 	.dw	0,Sstm8s_clk$CLK_CCOConfig$370-Sstm8s_clk$CLK_CCOConfig$345
      00109A 01                    6724 	.db	1
      00109B 00 00 88 A3           6725 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$345)
      00109F 0E                    6726 	.db	14
      0010A0 02                    6727 	.uleb128	2
      0010A1 01                    6728 	.db	1
      0010A2 00 00 88 B3           6729 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$347)
      0010A6 0E                    6730 	.db	14
      0010A7 02                    6731 	.uleb128	2
      0010A8 01                    6732 	.db	1
      0010A9 00 00 88 BC           6733 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$348)
      0010AD 0E                    6734 	.db	14
      0010AE 02                    6735 	.uleb128	2
      0010AF 01                    6736 	.db	1
      0010B0 00 00 88 C5           6737 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$349)
      0010B4 0E                    6738 	.db	14
      0010B5 02                    6739 	.uleb128	2
      0010B6 01                    6740 	.db	1
      0010B7 00 00 88 CE           6741 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$350)
      0010BB 0E                    6742 	.db	14
      0010BC 02                    6743 	.uleb128	2
      0010BD 01                    6744 	.db	1
      0010BE 00 00 88 D7           6745 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$351)
      0010C2 0E                    6746 	.db	14
      0010C3 02                    6747 	.uleb128	2
      0010C4 01                    6748 	.db	1
      0010C5 00 00 88 DD           6749 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$352)
      0010C9 0E                    6750 	.db	14
      0010CA 02                    6751 	.uleb128	2
      0010CB 01                    6752 	.db	1
      0010CC 00 00 88 E3           6753 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$353)
      0010D0 0E                    6754 	.db	14
      0010D1 02                    6755 	.uleb128	2
      0010D2 01                    6756 	.db	1
      0010D3 00 00 88 E9           6757 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$354)
      0010D7 0E                    6758 	.db	14
      0010D8 02                    6759 	.uleb128	2
      0010D9 01                    6760 	.db	1
      0010DA 00 00 88 EF           6761 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$355)
      0010DE 0E                    6762 	.db	14
      0010DF 02                    6763 	.uleb128	2
      0010E0 01                    6764 	.db	1
      0010E1 00 00 88 F5           6765 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$356)
      0010E5 0E                    6766 	.db	14
      0010E6 02                    6767 	.uleb128	2
      0010E7 01                    6768 	.db	1
      0010E8 00 00 88 FB           6769 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$357)
      0010EC 0E                    6770 	.db	14
      0010ED 02                    6771 	.uleb128	2
      0010EE 01                    6772 	.db	1
      0010EF 00 00 89 01           6773 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$358)
      0010F3 0E                    6774 	.db	14
      0010F4 02                    6775 	.uleb128	2
      0010F5 01                    6776 	.db	1
      0010F6 00 00 89 03           6777 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$359)
      0010FA 0E                    6778 	.db	14
      0010FB 03                    6779 	.uleb128	3
      0010FC 01                    6780 	.db	1
      0010FD 00 00 89 05           6781 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$360)
      001101 0E                    6782 	.db	14
      001102 04                    6783 	.uleb128	4
      001103 01                    6784 	.db	1
      001104 00 00 89 07           6785 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$361)
      001108 0E                    6786 	.db	14
      001109 06                    6787 	.uleb128	6
      00110A 01                    6788 	.db	1
      00110B 00 00 89 09           6789 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$362)
      00110F 0E                    6790 	.db	14
      001110 07                    6791 	.uleb128	7
      001111 01                    6792 	.db	1
      001112 00 00 89 0B           6793 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$363)
      001116 0E                    6794 	.db	14
      001117 08                    6795 	.uleb128	8
      001118 01                    6796 	.db	1
      001119 00 00 89 10           6797 	.dw	0,(Sstm8s_clk$CLK_CCOConfig$364)
      00111D 0E                    6798 	.db	14
      00111E 02                    6799 	.uleb128	2
                                   6800 
                                   6801 	.area .debug_frame (NOLOAD)
      00111F 00 00                 6802 	.dw	0
      001121 00 0E                 6803 	.dw	Ldebug_CIE12_end-Ldebug_CIE12_start
      001123                       6804 Ldebug_CIE12_start:
      001123 FF FF                 6805 	.dw	0xffff
      001125 FF FF                 6806 	.dw	0xffff
      001127 01                    6807 	.db	1
      001128 00                    6808 	.db	0
      001129 01                    6809 	.uleb128	1
      00112A 7F                    6810 	.sleb128	-1
      00112B 09                    6811 	.db	9
      00112C 0C                    6812 	.db	12
      00112D 08                    6813 	.uleb128	8
      00112E 02                    6814 	.uleb128	2
      00112F 89                    6815 	.db	137
      001130 01                    6816 	.uleb128	1
      001131                       6817 Ldebug_CIE12_end:
      001131 00 00 00 52           6818 	.dw	0,82
      001135 00 00 11 1F           6819 	.dw	0,(Ldebug_CIE12_start-4)
      001139 00 00 88 6D           6820 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$328)	;initial loc
      00113D 00 00 00 36           6821 	.dw	0,Sstm8s_clk$CLK_HSIPrescalerConfig$343-Sstm8s_clk$CLK_HSIPrescalerConfig$328
      001141 01                    6822 	.db	1
      001142 00 00 88 6D           6823 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$328)
      001146 0E                    6824 	.db	14
      001147 02                    6825 	.uleb128	2
      001148 01                    6826 	.db	1
      001149 00 00 88 77           6827 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$330)
      00114D 0E                    6828 	.db	14
      00114E 02                    6829 	.uleb128	2
      00114F 01                    6830 	.db	1
      001150 00 00 88 7D           6831 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$331)
      001154 0E                    6832 	.db	14
      001155 02                    6833 	.uleb128	2
      001156 01                    6834 	.db	1
      001157 00 00 88 83           6835 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$332)
      00115B 0E                    6836 	.db	14
      00115C 02                    6837 	.uleb128	2
      00115D 01                    6838 	.db	1
      00115E 00 00 88 85           6839 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$333)
      001162 0E                    6840 	.db	14
      001163 03                    6841 	.uleb128	3
      001164 01                    6842 	.db	1
      001165 00 00 88 87           6843 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$334)
      001169 0E                    6844 	.db	14
      00116A 04                    6845 	.uleb128	4
      00116B 01                    6846 	.db	1
      00116C 00 00 88 89           6847 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$335)
      001170 0E                    6848 	.db	14
      001171 06                    6849 	.uleb128	6
      001172 01                    6850 	.db	1
      001173 00 00 88 8B           6851 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$336)
      001177 0E                    6852 	.db	14
      001178 07                    6853 	.uleb128	7
      001179 01                    6854 	.db	1
      00117A 00 00 88 8D           6855 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$337)
      00117E 0E                    6856 	.db	14
      00117F 08                    6857 	.uleb128	8
      001180 01                    6858 	.db	1
      001181 00 00 88 92           6859 	.dw	0,(Sstm8s_clk$CLK_HSIPrescalerConfig$338)
      001185 0E                    6860 	.db	14
      001186 02                    6861 	.uleb128	2
                                   6862 
                                   6863 	.area .debug_frame (NOLOAD)
      001187 00 00                 6864 	.dw	0
      001189 00 0E                 6865 	.dw	Ldebug_CIE13_end-Ldebug_CIE13_start
      00118B                       6866 Ldebug_CIE13_start:
      00118B FF FF                 6867 	.dw	0xffff
      00118D FF FF                 6868 	.dw	0xffff
      00118F 01                    6869 	.db	1
      001190 00                    6870 	.db	0
      001191 01                    6871 	.uleb128	1
      001192 7F                    6872 	.sleb128	-1
      001193 09                    6873 	.db	9
      001194 0C                    6874 	.db	12
      001195 08                    6875 	.uleb128	8
      001196 02                    6876 	.uleb128	2
      001197 89                    6877 	.db	137
      001198 01                    6878 	.uleb128	1
      001199                       6879 Ldebug_CIE13_end:
      001199 00 00 01 08           6880 	.dw	0,264
      00119D 00 00 11 87           6881 	.dw	0,(Ldebug_CIE13_start-4)
      0011A1 00 00 87 41           6882 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$221)	;initial loc
      0011A5 00 00 01 2C           6883 	.dw	0,Sstm8s_clk$CLK_ClockSwitchConfig$326-Sstm8s_clk$CLK_ClockSwitchConfig$221
      0011A9 01                    6884 	.db	1
      0011AA 00 00 87 41           6885 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$221)
      0011AE 0E                    6886 	.db	14
      0011AF 02                    6887 	.uleb128	2
      0011B0 01                    6888 	.db	1
      0011B1 00 00 87 42           6889 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$222)
      0011B5 0E                    6890 	.db	14
      0011B6 03                    6891 	.uleb128	3
      0011B7 01                    6892 	.db	1
      0011B8 00 00 87 48           6893 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$224)
      0011BC 0E                    6894 	.db	14
      0011BD 03                    6895 	.uleb128	3
      0011BE 01                    6896 	.db	1
      0011BF 00 00 87 4E           6897 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$225)
      0011C3 0E                    6898 	.db	14
      0011C4 03                    6899 	.uleb128	3
      0011C5 01                    6900 	.db	1
      0011C6 00 00 87 54           6901 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$226)
      0011CA 0E                    6902 	.db	14
      0011CB 03                    6903 	.uleb128	3
      0011CC 01                    6904 	.db	1
      0011CD 00 00 87 56           6905 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$227)
      0011D1 0E                    6906 	.db	14
      0011D2 04                    6907 	.uleb128	4
      0011D3 01                    6908 	.db	1
      0011D4 00 00 87 58           6909 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$228)
      0011D8 0E                    6910 	.db	14
      0011D9 05                    6911 	.uleb128	5
      0011DA 01                    6912 	.db	1
      0011DB 00 00 87 5A           6913 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$229)
      0011DF 0E                    6914 	.db	14
      0011E0 07                    6915 	.uleb128	7
      0011E1 01                    6916 	.db	1
      0011E2 00 00 87 5C           6917 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$230)
      0011E6 0E                    6918 	.db	14
      0011E7 08                    6919 	.uleb128	8
      0011E8 01                    6920 	.db	1
      0011E9 00 00 87 5E           6921 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$231)
      0011ED 0E                    6922 	.db	14
      0011EE 09                    6923 	.uleb128	9
      0011EF 01                    6924 	.db	1
      0011F0 00 00 87 63           6925 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$232)
      0011F4 0E                    6926 	.db	14
      0011F5 03                    6927 	.uleb128	3
      0011F6 01                    6928 	.db	1
      0011F7 00 00 87 6F           6929 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$234)
      0011FB 0E                    6930 	.db	14
      0011FC 03                    6931 	.uleb128	3
      0011FD 01                    6932 	.db	1
      0011FE 00 00 87 79           6933 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$235)
      001202 0E                    6934 	.db	14
      001203 04                    6935 	.uleb128	4
      001204 01                    6936 	.db	1
      001205 00 00 87 7B           6937 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$236)
      001209 0E                    6938 	.db	14
      00120A 05                    6939 	.uleb128	5
      00120B 01                    6940 	.db	1
      00120C 00 00 87 7D           6941 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$237)
      001210 0E                    6942 	.db	14
      001211 07                    6943 	.uleb128	7
      001212 01                    6944 	.db	1
      001213 00 00 87 7F           6945 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$238)
      001217 0E                    6946 	.db	14
      001218 08                    6947 	.uleb128	8
      001219 01                    6948 	.db	1
      00121A 00 00 87 81           6949 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$239)
      00121E 0E                    6950 	.db	14
      00121F 09                    6951 	.uleb128	9
      001220 01                    6952 	.db	1
      001221 00 00 87 86           6953 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$240)
      001225 0E                    6954 	.db	14
      001226 03                    6955 	.uleb128	3
      001227 01                    6956 	.db	1
      001228 00 00 87 8F           6957 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$242)
      00122C 0E                    6958 	.db	14
      00122D 03                    6959 	.uleb128	3
      00122E 01                    6960 	.db	1
      00122F 00 00 87 91           6961 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$243)
      001233 0E                    6962 	.db	14
      001234 04                    6963 	.uleb128	4
      001235 01                    6964 	.db	1
      001236 00 00 87 93           6965 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$244)
      00123A 0E                    6966 	.db	14
      00123B 05                    6967 	.uleb128	5
      00123C 01                    6968 	.db	1
      00123D 00 00 87 95           6969 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$245)
      001241 0E                    6970 	.db	14
      001242 07                    6971 	.uleb128	7
      001243 01                    6972 	.db	1
      001244 00 00 87 97           6973 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$246)
      001248 0E                    6974 	.db	14
      001249 08                    6975 	.uleb128	8
      00124A 01                    6976 	.db	1
      00124B 00 00 87 99           6977 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$247)
      00124F 0E                    6978 	.db	14
      001250 09                    6979 	.uleb128	9
      001251 01                    6980 	.db	1
      001252 00 00 87 9E           6981 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$248)
      001256 0E                    6982 	.db	14
      001257 03                    6983 	.uleb128	3
      001258 01                    6984 	.db	1
      001259 00 00 87 A7           6985 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$250)
      00125D 0E                    6986 	.db	14
      00125E 03                    6987 	.uleb128	3
      00125F 01                    6988 	.db	1
      001260 00 00 87 A9           6989 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$251)
      001264 0E                    6990 	.db	14
      001265 04                    6991 	.uleb128	4
      001266 01                    6992 	.db	1
      001267 00 00 87 AB           6993 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$252)
      00126B 0E                    6994 	.db	14
      00126C 05                    6995 	.uleb128	5
      00126D 01                    6996 	.db	1
      00126E 00 00 87 AD           6997 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$253)
      001272 0E                    6998 	.db	14
      001273 07                    6999 	.uleb128	7
      001274 01                    7000 	.db	1
      001275 00 00 87 AF           7001 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$254)
      001279 0E                    7002 	.db	14
      00127A 08                    7003 	.uleb128	8
      00127B 01                    7004 	.db	1
      00127C 00 00 87 B1           7005 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$255)
      001280 0E                    7006 	.db	14
      001281 09                    7007 	.uleb128	9
      001282 01                    7008 	.db	1
      001283 00 00 87 B6           7009 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$256)
      001287 0E                    7010 	.db	14
      001288 03                    7011 	.uleb128	3
      001289 01                    7012 	.db	1
      00128A 00 00 88 45           7013 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$308)
      00128E 0E                    7014 	.db	14
      00128F 03                    7015 	.uleb128	3
      001290 01                    7016 	.db	1
      001291 00 00 88 55           7017 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$313)
      001295 0E                    7018 	.db	14
      001296 03                    7019 	.uleb128	3
      001297 01                    7020 	.db	1
      001298 00 00 88 65           7021 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$318)
      00129C 0E                    7022 	.db	14
      00129D 03                    7023 	.uleb128	3
      00129E 01                    7024 	.db	1
      00129F 00 00 88 6C           7025 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchConfig$324)
      0012A3 0E                    7026 	.db	14
      0012A4 02                    7027 	.uleb128	2
                                   7028 
                                   7029 	.area .debug_frame (NOLOAD)
      0012A5 00 00                 7030 	.dw	0
      0012A7 00 0E                 7031 	.dw	Ldebug_CIE14_end-Ldebug_CIE14_start
      0012A9                       7032 Ldebug_CIE14_start:
      0012A9 FF FF                 7033 	.dw	0xffff
      0012AB FF FF                 7034 	.dw	0xffff
      0012AD 01                    7035 	.db	1
      0012AE 00                    7036 	.db	0
      0012AF 01                    7037 	.uleb128	1
      0012B0 7F                    7038 	.sleb128	-1
      0012B1 09                    7039 	.db	9
      0012B2 0C                    7040 	.db	12
      0012B3 08                    7041 	.uleb128	8
      0012B4 02                    7042 	.uleb128	2
      0012B5 89                    7043 	.db	137
      0012B6 01                    7044 	.uleb128	1
      0012B7                       7045 Ldebug_CIE14_end:
      0012B7 00 00 00 D0           7046 	.dw	0,208
      0012BB 00 00 12 A5           7047 	.dw	0,(Ldebug_CIE14_start-4)
      0012BF 00 00 86 61           7048 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$166)	;initial loc
      0012C3 00 00 00 E0           7049 	.dw	0,Sstm8s_clk$CLK_PeripheralClockConfig$219-Sstm8s_clk$CLK_PeripheralClockConfig$166
      0012C7 01                    7050 	.db	1
      0012C8 00 00 86 61           7051 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$166)
      0012CC 0E                    7052 	.db	14
      0012CD 02                    7053 	.uleb128	2
      0012CE 01                    7054 	.db	1
      0012CF 00 00 86 62           7055 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$167)
      0012D3 0E                    7056 	.db	14
      0012D4 04                    7057 	.uleb128	4
      0012D5 01                    7058 	.db	1
      0012D6 00 00 86 6B           7059 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$169)
      0012DA 0E                    7060 	.db	14
      0012DB 04                    7061 	.uleb128	4
      0012DC 01                    7062 	.db	1
      0012DD 00 00 86 6D           7063 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$170)
      0012E1 0E                    7064 	.db	14
      0012E2 05                    7065 	.uleb128	5
      0012E3 01                    7066 	.db	1
      0012E4 00 00 86 6F           7067 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$171)
      0012E8 0E                    7068 	.db	14
      0012E9 06                    7069 	.uleb128	6
      0012EA 01                    7070 	.db	1
      0012EB 00 00 86 71           7071 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$172)
      0012EF 0E                    7072 	.db	14
      0012F0 08                    7073 	.uleb128	8
      0012F1 01                    7074 	.db	1
      0012F2 00 00 86 73           7075 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$173)
      0012F6 0E                    7076 	.db	14
      0012F7 09                    7077 	.uleb128	9
      0012F8 01                    7078 	.db	1
      0012F9 00 00 86 75           7079 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$174)
      0012FD 0E                    7080 	.db	14
      0012FE 0A                    7081 	.uleb128	10
      0012FF 01                    7082 	.db	1
      001300 00 00 86 7A           7083 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$175)
      001304 0E                    7084 	.db	14
      001305 04                    7085 	.uleb128	4
      001306 01                    7086 	.db	1
      001307 00 00 86 89           7087 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$177)
      00130B 0E                    7088 	.db	14
      00130C 04                    7089 	.uleb128	4
      00130D 01                    7090 	.db	1
      00130E 00 00 86 92           7091 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$178)
      001312 0E                    7092 	.db	14
      001313 04                    7093 	.uleb128	4
      001314 01                    7094 	.db	1
      001315 00 00 86 A7           7095 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$179)
      001319 0E                    7096 	.db	14
      00131A 04                    7097 	.uleb128	4
      00131B 01                    7098 	.db	1
      00131C 00 00 86 B3           7099 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$180)
      001320 0E                    7100 	.db	14
      001321 04                    7101 	.uleb128	4
      001322 01                    7102 	.db	1
      001323 00 00 86 C3           7103 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$181)
      001327 0E                    7104 	.db	14
      001328 04                    7105 	.uleb128	4
      001329 01                    7106 	.db	1
      00132A 00 00 86 D3           7107 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$182)
      00132E 0E                    7108 	.db	14
      00132F 04                    7109 	.uleb128	4
      001330 01                    7110 	.db	1
      001331 00 00 86 D9           7111 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$183)
      001335 0E                    7112 	.db	14
      001336 04                    7113 	.uleb128	4
      001337 01                    7114 	.db	1
      001338 00 00 86 DF           7115 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$184)
      00133C 0E                    7116 	.db	14
      00133D 04                    7117 	.uleb128	4
      00133E 01                    7118 	.db	1
      00133F 00 00 86 E5           7119 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$185)
      001343 0E                    7120 	.db	14
      001344 04                    7121 	.uleb128	4
      001345 01                    7122 	.db	1
      001346 00 00 86 EB           7123 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$186)
      00134A 0E                    7124 	.db	14
      00134B 04                    7125 	.uleb128	4
      00134C 01                    7126 	.db	1
      00134D 00 00 86 ED           7127 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$187)
      001351 0E                    7128 	.db	14
      001352 05                    7129 	.uleb128	5
      001353 01                    7130 	.db	1
      001354 00 00 86 EF           7131 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$188)
      001358 0E                    7132 	.db	14
      001359 06                    7133 	.uleb128	6
      00135A 01                    7134 	.db	1
      00135B 00 00 86 F1           7135 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$189)
      00135F 0E                    7136 	.db	14
      001360 08                    7137 	.uleb128	8
      001361 01                    7138 	.db	1
      001362 00 00 86 F3           7139 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$190)
      001366 0E                    7140 	.db	14
      001367 09                    7141 	.uleb128	9
      001368 01                    7142 	.db	1
      001369 00 00 86 F5           7143 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$191)
      00136D 0E                    7144 	.db	14
      00136E 0A                    7145 	.uleb128	10
      00136F 01                    7146 	.db	1
      001370 00 00 86 FA           7147 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$192)
      001374 0E                    7148 	.db	14
      001375 04                    7149 	.uleb128	4
      001376 01                    7150 	.db	1
      001377 00 00 86 FF           7151 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$194)
      00137B 0E                    7152 	.db	14
      00137C 05                    7153 	.uleb128	5
      00137D 01                    7154 	.db	1
      00137E 00 00 87 04           7155 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$195)
      001382 0E                    7156 	.db	14
      001383 04                    7157 	.uleb128	4
      001384 01                    7158 	.db	1
      001385 00 00 87 40           7159 	.dw	0,(Sstm8s_clk$CLK_PeripheralClockConfig$217)
      001389 0E                    7160 	.db	14
      00138A 02                    7161 	.uleb128	2
                                   7162 
                                   7163 	.area .debug_frame (NOLOAD)
      00138B 00 00                 7164 	.dw	0
      00138D 00 0E                 7165 	.dw	Ldebug_CIE15_end-Ldebug_CIE15_start
      00138F                       7166 Ldebug_CIE15_start:
      00138F FF FF                 7167 	.dw	0xffff
      001391 FF FF                 7168 	.dw	0xffff
      001393 01                    7169 	.db	1
      001394 00                    7170 	.db	0
      001395 01                    7171 	.uleb128	1
      001396 7F                    7172 	.sleb128	-1
      001397 09                    7173 	.db	9
      001398 0C                    7174 	.db	12
      001399 08                    7175 	.uleb128	8
      00139A 02                    7176 	.uleb128	2
      00139B 89                    7177 	.db	137
      00139C 01                    7178 	.uleb128	1
      00139D                       7179 Ldebug_CIE15_end:
      00139D 00 00 00 44           7180 	.dw	0,68
      0013A1 00 00 13 8B           7181 	.dw	0,(Ldebug_CIE15_start-4)
      0013A5 00 00 86 35           7182 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$145)	;initial loc
      0013A9 00 00 00 2C           7183 	.dw	0,Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$164-Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$145
      0013AD 01                    7184 	.db	1
      0013AE 00 00 86 35           7185 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$145)
      0013B2 0E                    7186 	.db	14
      0013B3 02                    7187 	.uleb128	2
      0013B4 01                    7188 	.db	1
      0013B5 00 00 86 3E           7189 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$147)
      0013B9 0E                    7190 	.db	14
      0013BA 02                    7191 	.uleb128	2
      0013BB 01                    7192 	.db	1
      0013BC 00 00 86 40           7193 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$148)
      0013C0 0E                    7194 	.db	14
      0013C1 03                    7195 	.uleb128	3
      0013C2 01                    7196 	.db	1
      0013C3 00 00 86 42           7197 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$149)
      0013C7 0E                    7198 	.db	14
      0013C8 05                    7199 	.uleb128	5
      0013C9 01                    7200 	.db	1
      0013CA 00 00 86 44           7201 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$150)
      0013CE 0E                    7202 	.db	14
      0013CF 06                    7203 	.uleb128	6
      0013D0 01                    7204 	.db	1
      0013D1 00 00 86 46           7205 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$151)
      0013D5 0E                    7206 	.db	14
      0013D6 07                    7207 	.uleb128	7
      0013D7 01                    7208 	.db	1
      0013D8 00 00 86 48           7209 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$152)
      0013DC 0E                    7210 	.db	14
      0013DD 08                    7211 	.uleb128	8
      0013DE 01                    7212 	.db	1
      0013DF 00 00 86 4D           7213 	.dw	0,(Sstm8s_clk$CLK_SlowActiveHaltWakeUpCmd$153)
      0013E3 0E                    7214 	.db	14
      0013E4 02                    7215 	.uleb128	2
                                   7216 
                                   7217 	.area .debug_frame (NOLOAD)
      0013E5 00 00                 7218 	.dw	0
      0013E7 00 0E                 7219 	.dw	Ldebug_CIE16_end-Ldebug_CIE16_start
      0013E9                       7220 Ldebug_CIE16_start:
      0013E9 FF FF                 7221 	.dw	0xffff
      0013EB FF FF                 7222 	.dw	0xffff
      0013ED 01                    7223 	.db	1
      0013EE 00                    7224 	.db	0
      0013EF 01                    7225 	.uleb128	1
      0013F0 7F                    7226 	.sleb128	-1
      0013F1 09                    7227 	.db	9
      0013F2 0C                    7228 	.db	12
      0013F3 08                    7229 	.uleb128	8
      0013F4 02                    7230 	.uleb128	2
      0013F5 89                    7231 	.db	137
      0013F6 01                    7232 	.uleb128	1
      0013F7                       7233 Ldebug_CIE16_end:
      0013F7 00 00 00 44           7234 	.dw	0,68
      0013FB 00 00 13 E5           7235 	.dw	0,(Ldebug_CIE16_start-4)
      0013FF 00 00 86 09           7236 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$124)	;initial loc
      001403 00 00 00 2C           7237 	.dw	0,Sstm8s_clk$CLK_ClockSwitchCmd$143-Sstm8s_clk$CLK_ClockSwitchCmd$124
      001407 01                    7238 	.db	1
      001408 00 00 86 09           7239 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$124)
      00140C 0E                    7240 	.db	14
      00140D 02                    7241 	.uleb128	2
      00140E 01                    7242 	.db	1
      00140F 00 00 86 12           7243 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$126)
      001413 0E                    7244 	.db	14
      001414 02                    7245 	.uleb128	2
      001415 01                    7246 	.db	1
      001416 00 00 86 14           7247 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$127)
      00141A 0E                    7248 	.db	14
      00141B 03                    7249 	.uleb128	3
      00141C 01                    7250 	.db	1
      00141D 00 00 86 16           7251 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$128)
      001421 0E                    7252 	.db	14
      001422 05                    7253 	.uleb128	5
      001423 01                    7254 	.db	1
      001424 00 00 86 18           7255 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$129)
      001428 0E                    7256 	.db	14
      001429 06                    7257 	.uleb128	6
      00142A 01                    7258 	.db	1
      00142B 00 00 86 1A           7259 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$130)
      00142F 0E                    7260 	.db	14
      001430 07                    7261 	.uleb128	7
      001431 01                    7262 	.db	1
      001432 00 00 86 1C           7263 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$131)
      001436 0E                    7264 	.db	14
      001437 08                    7265 	.uleb128	8
      001438 01                    7266 	.db	1
      001439 00 00 86 21           7267 	.dw	0,(Sstm8s_clk$CLK_ClockSwitchCmd$132)
      00143D 0E                    7268 	.db	14
      00143E 02                    7269 	.uleb128	2
                                   7270 
                                   7271 	.area .debug_frame (NOLOAD)
      00143F 00 00                 7272 	.dw	0
      001441 00 0E                 7273 	.dw	Ldebug_CIE17_end-Ldebug_CIE17_start
      001443                       7274 Ldebug_CIE17_start:
      001443 FF FF                 7275 	.dw	0xffff
      001445 FF FF                 7276 	.dw	0xffff
      001447 01                    7277 	.db	1
      001448 00                    7278 	.db	0
      001449 01                    7279 	.uleb128	1
      00144A 7F                    7280 	.sleb128	-1
      00144B 09                    7281 	.db	9
      00144C 0C                    7282 	.db	12
      00144D 08                    7283 	.uleb128	8
      00144E 02                    7284 	.uleb128	2
      00144F 89                    7285 	.db	137
      001450 01                    7286 	.uleb128	1
      001451                       7287 Ldebug_CIE17_end:
      001451 00 00 00 44           7288 	.dw	0,68
      001455 00 00 14 3F           7289 	.dw	0,(Ldebug_CIE17_start-4)
      001459 00 00 85 DD           7290 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$103)	;initial loc
      00145D 00 00 00 2C           7291 	.dw	0,Sstm8s_clk$CLK_CCOCmd$122-Sstm8s_clk$CLK_CCOCmd$103
      001461 01                    7292 	.db	1
      001462 00 00 85 DD           7293 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$103)
      001466 0E                    7294 	.db	14
      001467 02                    7295 	.uleb128	2
      001468 01                    7296 	.db	1
      001469 00 00 85 E6           7297 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$105)
      00146D 0E                    7298 	.db	14
      00146E 02                    7299 	.uleb128	2
      00146F 01                    7300 	.db	1
      001470 00 00 85 E8           7301 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$106)
      001474 0E                    7302 	.db	14
      001475 03                    7303 	.uleb128	3
      001476 01                    7304 	.db	1
      001477 00 00 85 EA           7305 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$107)
      00147B 0E                    7306 	.db	14
      00147C 05                    7307 	.uleb128	5
      00147D 01                    7308 	.db	1
      00147E 00 00 85 EC           7309 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$108)
      001482 0E                    7310 	.db	14
      001483 06                    7311 	.uleb128	6
      001484 01                    7312 	.db	1
      001485 00 00 85 EE           7313 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$109)
      001489 0E                    7314 	.db	14
      00148A 07                    7315 	.uleb128	7
      00148B 01                    7316 	.db	1
      00148C 00 00 85 F0           7317 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$110)
      001490 0E                    7318 	.db	14
      001491 08                    7319 	.uleb128	8
      001492 01                    7320 	.db	1
      001493 00 00 85 F5           7321 	.dw	0,(Sstm8s_clk$CLK_CCOCmd$111)
      001497 0E                    7322 	.db	14
      001498 02                    7323 	.uleb128	2
                                   7324 
                                   7325 	.area .debug_frame (NOLOAD)
      001499 00 00                 7326 	.dw	0
      00149B 00 0E                 7327 	.dw	Ldebug_CIE18_end-Ldebug_CIE18_start
      00149D                       7328 Ldebug_CIE18_start:
      00149D FF FF                 7329 	.dw	0xffff
      00149F FF FF                 7330 	.dw	0xffff
      0014A1 01                    7331 	.db	1
      0014A2 00                    7332 	.db	0
      0014A3 01                    7333 	.uleb128	1
      0014A4 7F                    7334 	.sleb128	-1
      0014A5 09                    7335 	.db	9
      0014A6 0C                    7336 	.db	12
      0014A7 08                    7337 	.uleb128	8
      0014A8 02                    7338 	.uleb128	2
      0014A9 89                    7339 	.db	137
      0014AA 01                    7340 	.uleb128	1
      0014AB                       7341 Ldebug_CIE18_end:
      0014AB 00 00 00 44           7342 	.dw	0,68
      0014AF 00 00 14 99           7343 	.dw	0,(Ldebug_CIE18_start-4)
      0014B3 00 00 85 B1           7344 	.dw	0,(Sstm8s_clk$CLK_LSICmd$82)	;initial loc
      0014B7 00 00 00 2C           7345 	.dw	0,Sstm8s_clk$CLK_LSICmd$101-Sstm8s_clk$CLK_LSICmd$82
      0014BB 01                    7346 	.db	1
      0014BC 00 00 85 B1           7347 	.dw	0,(Sstm8s_clk$CLK_LSICmd$82)
      0014C0 0E                    7348 	.db	14
      0014C1 02                    7349 	.uleb128	2
      0014C2 01                    7350 	.db	1
      0014C3 00 00 85 BA           7351 	.dw	0,(Sstm8s_clk$CLK_LSICmd$84)
      0014C7 0E                    7352 	.db	14
      0014C8 02                    7353 	.uleb128	2
      0014C9 01                    7354 	.db	1
      0014CA 00 00 85 BC           7355 	.dw	0,(Sstm8s_clk$CLK_LSICmd$85)
      0014CE 0E                    7356 	.db	14
      0014CF 03                    7357 	.uleb128	3
      0014D0 01                    7358 	.db	1
      0014D1 00 00 85 BE           7359 	.dw	0,(Sstm8s_clk$CLK_LSICmd$86)
      0014D5 0E                    7360 	.db	14
      0014D6 05                    7361 	.uleb128	5
      0014D7 01                    7362 	.db	1
      0014D8 00 00 85 C0           7363 	.dw	0,(Sstm8s_clk$CLK_LSICmd$87)
      0014DC 0E                    7364 	.db	14
      0014DD 06                    7365 	.uleb128	6
      0014DE 01                    7366 	.db	1
      0014DF 00 00 85 C2           7367 	.dw	0,(Sstm8s_clk$CLK_LSICmd$88)
      0014E3 0E                    7368 	.db	14
      0014E4 07                    7369 	.uleb128	7
      0014E5 01                    7370 	.db	1
      0014E6 00 00 85 C4           7371 	.dw	0,(Sstm8s_clk$CLK_LSICmd$89)
      0014EA 0E                    7372 	.db	14
      0014EB 08                    7373 	.uleb128	8
      0014EC 01                    7374 	.db	1
      0014ED 00 00 85 C9           7375 	.dw	0,(Sstm8s_clk$CLK_LSICmd$90)
      0014F1 0E                    7376 	.db	14
      0014F2 02                    7377 	.uleb128	2
                                   7378 
                                   7379 	.area .debug_frame (NOLOAD)
      0014F3 00 00                 7380 	.dw	0
      0014F5 00 0E                 7381 	.dw	Ldebug_CIE19_end-Ldebug_CIE19_start
      0014F7                       7382 Ldebug_CIE19_start:
      0014F7 FF FF                 7383 	.dw	0xffff
      0014F9 FF FF                 7384 	.dw	0xffff
      0014FB 01                    7385 	.db	1
      0014FC 00                    7386 	.db	0
      0014FD 01                    7387 	.uleb128	1
      0014FE 7F                    7388 	.sleb128	-1
      0014FF 09                    7389 	.db	9
      001500 0C                    7390 	.db	12
      001501 08                    7391 	.uleb128	8
      001502 02                    7392 	.uleb128	2
      001503 89                    7393 	.db	137
      001504 01                    7394 	.uleb128	1
      001505                       7395 Ldebug_CIE19_end:
      001505 00 00 00 44           7396 	.dw	0,68
      001509 00 00 14 F3           7397 	.dw	0,(Ldebug_CIE19_start-4)
      00150D 00 00 85 85           7398 	.dw	0,(Sstm8s_clk$CLK_HSICmd$61)	;initial loc
      001511 00 00 00 2C           7399 	.dw	0,Sstm8s_clk$CLK_HSICmd$80-Sstm8s_clk$CLK_HSICmd$61
      001515 01                    7400 	.db	1
      001516 00 00 85 85           7401 	.dw	0,(Sstm8s_clk$CLK_HSICmd$61)
      00151A 0E                    7402 	.db	14
      00151B 02                    7403 	.uleb128	2
      00151C 01                    7404 	.db	1
      00151D 00 00 85 8E           7405 	.dw	0,(Sstm8s_clk$CLK_HSICmd$63)
      001521 0E                    7406 	.db	14
      001522 02                    7407 	.uleb128	2
      001523 01                    7408 	.db	1
      001524 00 00 85 90           7409 	.dw	0,(Sstm8s_clk$CLK_HSICmd$64)
      001528 0E                    7410 	.db	14
      001529 03                    7411 	.uleb128	3
      00152A 01                    7412 	.db	1
      00152B 00 00 85 92           7413 	.dw	0,(Sstm8s_clk$CLK_HSICmd$65)
      00152F 0E                    7414 	.db	14
      001530 05                    7415 	.uleb128	5
      001531 01                    7416 	.db	1
      001532 00 00 85 94           7417 	.dw	0,(Sstm8s_clk$CLK_HSICmd$66)
      001536 0E                    7418 	.db	14
      001537 06                    7419 	.uleb128	6
      001538 01                    7420 	.db	1
      001539 00 00 85 96           7421 	.dw	0,(Sstm8s_clk$CLK_HSICmd$67)
      00153D 0E                    7422 	.db	14
      00153E 07                    7423 	.uleb128	7
      00153F 01                    7424 	.db	1
      001540 00 00 85 98           7425 	.dw	0,(Sstm8s_clk$CLK_HSICmd$68)
      001544 0E                    7426 	.db	14
      001545 08                    7427 	.uleb128	8
      001546 01                    7428 	.db	1
      001547 00 00 85 9D           7429 	.dw	0,(Sstm8s_clk$CLK_HSICmd$69)
      00154B 0E                    7430 	.db	14
      00154C 02                    7431 	.uleb128	2
                                   7432 
                                   7433 	.area .debug_frame (NOLOAD)
      00154D 00 00                 7434 	.dw	0
      00154F 00 0E                 7435 	.dw	Ldebug_CIE20_end-Ldebug_CIE20_start
      001551                       7436 Ldebug_CIE20_start:
      001551 FF FF                 7437 	.dw	0xffff
      001553 FF FF                 7438 	.dw	0xffff
      001555 01                    7439 	.db	1
      001556 00                    7440 	.db	0
      001557 01                    7441 	.uleb128	1
      001558 7F                    7442 	.sleb128	-1
      001559 09                    7443 	.db	9
      00155A 0C                    7444 	.db	12
      00155B 08                    7445 	.uleb128	8
      00155C 02                    7446 	.uleb128	2
      00155D 89                    7447 	.db	137
      00155E 01                    7448 	.uleb128	1
      00155F                       7449 Ldebug_CIE20_end:
      00155F 00 00 00 44           7450 	.dw	0,68
      001563 00 00 15 4D           7451 	.dw	0,(Ldebug_CIE20_start-4)
      001567 00 00 85 59           7452 	.dw	0,(Sstm8s_clk$CLK_HSECmd$40)	;initial loc
      00156B 00 00 00 2C           7453 	.dw	0,Sstm8s_clk$CLK_HSECmd$59-Sstm8s_clk$CLK_HSECmd$40
      00156F 01                    7454 	.db	1
      001570 00 00 85 59           7455 	.dw	0,(Sstm8s_clk$CLK_HSECmd$40)
      001574 0E                    7456 	.db	14
      001575 02                    7457 	.uleb128	2
      001576 01                    7458 	.db	1
      001577 00 00 85 62           7459 	.dw	0,(Sstm8s_clk$CLK_HSECmd$42)
      00157B 0E                    7460 	.db	14
      00157C 02                    7461 	.uleb128	2
      00157D 01                    7462 	.db	1
      00157E 00 00 85 64           7463 	.dw	0,(Sstm8s_clk$CLK_HSECmd$43)
      001582 0E                    7464 	.db	14
      001583 03                    7465 	.uleb128	3
      001584 01                    7466 	.db	1
      001585 00 00 85 66           7467 	.dw	0,(Sstm8s_clk$CLK_HSECmd$44)
      001589 0E                    7468 	.db	14
      00158A 05                    7469 	.uleb128	5
      00158B 01                    7470 	.db	1
      00158C 00 00 85 68           7471 	.dw	0,(Sstm8s_clk$CLK_HSECmd$45)
      001590 0E                    7472 	.db	14
      001591 06                    7473 	.uleb128	6
      001592 01                    7474 	.db	1
      001593 00 00 85 6A           7475 	.dw	0,(Sstm8s_clk$CLK_HSECmd$46)
      001597 0E                    7476 	.db	14
      001598 07                    7477 	.uleb128	7
      001599 01                    7478 	.db	1
      00159A 00 00 85 6C           7479 	.dw	0,(Sstm8s_clk$CLK_HSECmd$47)
      00159E 0E                    7480 	.db	14
      00159F 08                    7481 	.uleb128	8
      0015A0 01                    7482 	.db	1
      0015A1 00 00 85 71           7483 	.dw	0,(Sstm8s_clk$CLK_HSECmd$48)
      0015A5 0E                    7484 	.db	14
      0015A6 02                    7485 	.uleb128	2
                                   7486 
                                   7487 	.area .debug_frame (NOLOAD)
      0015A7 00 00                 7488 	.dw	0
      0015A9 00 0E                 7489 	.dw	Ldebug_CIE21_end-Ldebug_CIE21_start
      0015AB                       7490 Ldebug_CIE21_start:
      0015AB FF FF                 7491 	.dw	0xffff
      0015AD FF FF                 7492 	.dw	0xffff
      0015AF 01                    7493 	.db	1
      0015B0 00                    7494 	.db	0
      0015B1 01                    7495 	.uleb128	1
      0015B2 7F                    7496 	.sleb128	-1
      0015B3 09                    7497 	.db	9
      0015B4 0C                    7498 	.db	12
      0015B5 08                    7499 	.uleb128	8
      0015B6 02                    7500 	.uleb128	2
      0015B7 89                    7501 	.db	137
      0015B8 01                    7502 	.uleb128	1
      0015B9                       7503 Ldebug_CIE21_end:
      0015B9 00 00 00 44           7504 	.dw	0,68
      0015BD 00 00 15 A7           7505 	.dw	0,(Ldebug_CIE21_start-4)
      0015C1 00 00 85 2D           7506 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$19)	;initial loc
      0015C5 00 00 00 2C           7507 	.dw	0,Sstm8s_clk$CLK_FastHaltWakeUpCmd$38-Sstm8s_clk$CLK_FastHaltWakeUpCmd$19
      0015C9 01                    7508 	.db	1
      0015CA 00 00 85 2D           7509 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$19)
      0015CE 0E                    7510 	.db	14
      0015CF 02                    7511 	.uleb128	2
      0015D0 01                    7512 	.db	1
      0015D1 00 00 85 36           7513 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$21)
      0015D5 0E                    7514 	.db	14
      0015D6 02                    7515 	.uleb128	2
      0015D7 01                    7516 	.db	1
      0015D8 00 00 85 38           7517 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$22)
      0015DC 0E                    7518 	.db	14
      0015DD 03                    7519 	.uleb128	3
      0015DE 01                    7520 	.db	1
      0015DF 00 00 85 3A           7521 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$23)
      0015E3 0E                    7522 	.db	14
      0015E4 05                    7523 	.uleb128	5
      0015E5 01                    7524 	.db	1
      0015E6 00 00 85 3C           7525 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$24)
      0015EA 0E                    7526 	.db	14
      0015EB 06                    7527 	.uleb128	6
      0015EC 01                    7528 	.db	1
      0015ED 00 00 85 3E           7529 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$25)
      0015F1 0E                    7530 	.db	14
      0015F2 07                    7531 	.uleb128	7
      0015F3 01                    7532 	.db	1
      0015F4 00 00 85 40           7533 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$26)
      0015F8 0E                    7534 	.db	14
      0015F9 08                    7535 	.uleb128	8
      0015FA 01                    7536 	.db	1
      0015FB 00 00 85 45           7537 	.dw	0,(Sstm8s_clk$CLK_FastHaltWakeUpCmd$27)
      0015FF 0E                    7538 	.db	14
      001600 02                    7539 	.uleb128	2
                                   7540 
                                   7541 	.area .debug_frame (NOLOAD)
      001601 00 00                 7542 	.dw	0
      001603 00 0E                 7543 	.dw	Ldebug_CIE22_end-Ldebug_CIE22_start
      001605                       7544 Ldebug_CIE22_start:
      001605 FF FF                 7545 	.dw	0xffff
      001607 FF FF                 7546 	.dw	0xffff
      001609 01                    7547 	.db	1
      00160A 00                    7548 	.db	0
      00160B 01                    7549 	.uleb128	1
      00160C 7F                    7550 	.sleb128	-1
      00160D 09                    7551 	.db	9
      00160E 0C                    7552 	.db	12
      00160F 08                    7553 	.uleb128	8
      001610 02                    7554 	.uleb128	2
      001611 89                    7555 	.db	137
      001612 01                    7556 	.uleb128	1
      001613                       7557 Ldebug_CIE22_end:
      001613 00 00 00 13           7558 	.dw	0,19
      001617 00 00 16 01           7559 	.dw	0,(Ldebug_CIE22_start-4)
      00161B 00 00 84 F6           7560 	.dw	0,(Sstm8s_clk$CLK_DeInit$1)	;initial loc
      00161F 00 00 00 37           7561 	.dw	0,Sstm8s_clk$CLK_DeInit$17-Sstm8s_clk$CLK_DeInit$1
      001623 01                    7562 	.db	1
      001624 00 00 84 F6           7563 	.dw	0,(Sstm8s_clk$CLK_DeInit$1)
      001628 0E                    7564 	.db	14
      001629 02                    7565 	.uleb128	2
