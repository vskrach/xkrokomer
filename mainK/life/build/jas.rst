                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.1.0 #12072 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module jas
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _TIM2_SetCompare1
                                     12 	.globl _TIM2_OC1PreloadConfig
                                     13 	.globl _TIM2_Cmd
                                     14 	.globl _TIM2_OC1Init
                                     15 	.globl _TIM2_TimeBaseInit
                                     16 	.globl _jas_init
                                     17 	.globl _jas
                                     18 ;--------------------------------------------------------
                                     19 ; ram data
                                     20 ;--------------------------------------------------------
                                     21 	.area DATA
                                     22 ;--------------------------------------------------------
                                     23 ; ram data
                                     24 ;--------------------------------------------------------
                                     25 	.area INITIALIZED
                                     26 ;--------------------------------------------------------
                                     27 ; absolute external ram data
                                     28 ;--------------------------------------------------------
                                     29 	.area DABS (ABS)
                                     30 
                                     31 ; default segment ordering for linker
                                     32 	.area HOME
                                     33 	.area GSINIT
                                     34 	.area GSFINAL
                                     35 	.area CONST
                                     36 	.area INITIALIZER
                                     37 	.area CODE
                                     38 
                                     39 ;--------------------------------------------------------
                                     40 ; global & static initialisations
                                     41 ;--------------------------------------------------------
                                     42 	.area HOME
                                     43 	.area GSINIT
                                     44 	.area GSFINAL
                                     45 	.area GSINIT
                                     46 ;--------------------------------------------------------
                                     47 ; Home
                                     48 ;--------------------------------------------------------
                                     49 	.area HOME
                                     50 	.area HOME
                                     51 ;--------------------------------------------------------
                                     52 ; code
                                     53 ;--------------------------------------------------------
                                     54 	.area CODE
                           000000    55 	Sjas$jas_init$0 ==.
                                     56 ;	app/src/jas.c: 4: void jas_init(void) //azurova #00ffff zluta #ffff00 oranzova  #ff4000
                                     57 ;	-----------------------------------------
                                     58 ;	 function jas_init
                                     59 ;	-----------------------------------------
      00822E                         60 _jas_init:
                           000000    61 	Sjas$jas_init$1 ==.
                           000000    62 	Sjas$jas_init$2 ==.
                                     63 ;	app/src/jas.c: 6: TIM2_TimeBaseInit(TIM2_PRESCALER_128,255);
      00822E 4B FF            [ 1]   64 	push	#0xff
                           000002    65 	Sjas$jas_init$3 ==.
      008230 4B 00            [ 1]   66 	push	#0x00
                           000004    67 	Sjas$jas_init$4 ==.
      008232 4B 07            [ 1]   68 	push	#0x07
                           000006    69 	Sjas$jas_init$5 ==.
      008234 CD 8F 71         [ 4]   70 	call	_TIM2_TimeBaseInit
      008237 5B 03            [ 2]   71 	addw	sp, #3
                           00000B    72 	Sjas$jas_init$6 ==.
                           00000B    73 	Sjas$jas_init$7 ==.
                                     74 ;	app/src/jas.c: 8: TIM2_OC1Init(TIM2_OCMODE_PWM1,TIM2_OUTPUTSTATE_ENABLE,4000,TIM2_OCPOLARITY_HIGH);
      008239 4B 00            [ 1]   75 	push	#0x00
                           00000D    76 	Sjas$jas_init$8 ==.
      00823B 4B A0            [ 1]   77 	push	#0xa0
                           00000F    78 	Sjas$jas_init$9 ==.
      00823D 4B 0F            [ 1]   79 	push	#0x0f
                           000011    80 	Sjas$jas_init$10 ==.
      00823F 4B 11            [ 1]   81 	push	#0x11
                           000013    82 	Sjas$jas_init$11 ==.
      008241 4B 60            [ 1]   83 	push	#0x60
                           000015    84 	Sjas$jas_init$12 ==.
      008243 CD 8F 82         [ 4]   85 	call	_TIM2_OC1Init
      008246 5B 05            [ 2]   86 	addw	sp, #5
                           00001A    87 	Sjas$jas_init$13 ==.
                           00001A    88 	Sjas$jas_init$14 ==.
                                     89 ;	app/src/jas.c: 9: TIM2_OC1PreloadConfig(ENABLE);
      008248 4B 01            [ 1]   90 	push	#0x01
                           00001C    91 	Sjas$jas_init$15 ==.
      00824A CD 95 83         [ 4]   92 	call	_TIM2_OC1PreloadConfig
      00824D 84               [ 1]   93 	pop	a
                           000020    94 	Sjas$jas_init$16 ==.
                           000020    95 	Sjas$jas_init$17 ==.
                                     96 ;	app/src/jas.c: 11: TIM2_Cmd(ENABLE);
      00824E 4B 01            [ 1]   97 	push	#0x01
                           000022    98 	Sjas$jas_init$18 ==.
      008250 CD 93 3B         [ 4]   99 	call	_TIM2_Cmd
      008253 84               [ 1]  100 	pop	a
                           000026   101 	Sjas$jas_init$19 ==.
                           000026   102 	Sjas$jas_init$20 ==.
                                    103 ;	app/src/jas.c: 12: }
                           000026   104 	Sjas$jas_init$21 ==.
                           000026   105 	XG$jas_init$0$0 ==.
      008254 81               [ 4]  106 	ret
                           000027   107 	Sjas$jas_init$22 ==.
                           000027   108 	Sjas$jas$23 ==.
                                    109 ;	app/src/jas.c: 13: void jas(uint8_t j){
                                    110 ;	-----------------------------------------
                                    111 ;	 function jas
                                    112 ;	-----------------------------------------
      008255                        113 _jas:
                           000027   114 	Sjas$jas$24 ==.
                           000027   115 	Sjas$jas$25 ==.
                                    116 ;	app/src/jas.c: 14: TIM2_SetCompare1(j);
      008255 5F               [ 1]  117 	clrw	x
      008256 7B 03            [ 1]  118 	ld	a, (0x03, sp)
      008258 97               [ 1]  119 	ld	xl, a
      008259 89               [ 2]  120 	pushw	x
                           00002C   121 	Sjas$jas$26 ==.
      00825A CD 97 E8         [ 4]  122 	call	_TIM2_SetCompare1
      00825D 85               [ 2]  123 	popw	x
                           000030   124 	Sjas$jas$27 ==.
                           000030   125 	Sjas$jas$28 ==.
                                    126 ;	app/src/jas.c: 17: }
                           000030   127 	Sjas$jas$29 ==.
                           000030   128 	XG$jas$0$0 ==.
      00825E 81               [ 4]  129 	ret
                           000031   130 	Sjas$jas$30 ==.
                                    131 	.area CODE
                                    132 	.area CONST
                                    133 	.area INITIALIZER
                                    134 	.area CABS (ABS)
                                    135 
                                    136 	.area .debug_line (NOLOAD)
      0001D3 00 00 00 BE            137 	.dw	0,Ldebug_line_end-Ldebug_line_start
      0001D7                        138 Ldebug_line_start:
      0001D7 00 02                  139 	.dw	2
      0001D9 00 00 00 6E            140 	.dw	0,Ldebug_line_stmt-6-Ldebug_line_start
      0001DD 01                     141 	.db	1
      0001DE 01                     142 	.db	1
      0001DF FB                     143 	.db	-5
      0001E0 0F                     144 	.db	15
      0001E1 0A                     145 	.db	10
      0001E2 00                     146 	.db	0
      0001E3 01                     147 	.db	1
      0001E4 01                     148 	.db	1
      0001E5 01                     149 	.db	1
      0001E6 01                     150 	.db	1
      0001E7 00                     151 	.db	0
      0001E8 00                     152 	.db	0
      0001E9 00                     153 	.db	0
      0001EA 01                     154 	.db	1
      0001EB 43 3A 5C 50 72 6F 67   155 	.ascii "C:\Program Files\SDCC\bin\..\include\stm8"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
             5C 73 74 6D 38
      000213 00                     156 	.db	0
      000214 43 3A 5C 50 72 6F 67   157 	.ascii "C:\Program Files\SDCC\bin\..\include"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
      000237 00                     158 	.db	0
      000238 00                     159 	.db	0
      000239 61 70 70 2F 73 72 63   160 	.ascii "app/src/jas.c"
             2F 6A 61 73 2E 63
      000246 00                     161 	.db	0
      000247 00                     162 	.uleb128	0
      000248 00                     163 	.uleb128	0
      000249 00                     164 	.uleb128	0
      00024A 00                     165 	.db	0
      00024B                        166 Ldebug_line_stmt:
      00024B 00                     167 	.db	0
      00024C 05                     168 	.uleb128	5
      00024D 02                     169 	.db	2
      00024E 00 00 82 2E            170 	.dw	0,(Sjas$jas_init$0)
      000252 03                     171 	.db	3
      000253 03                     172 	.sleb128	3
      000254 01                     173 	.db	1
      000255 09                     174 	.db	9
      000256 00 00                  175 	.dw	Sjas$jas_init$2-Sjas$jas_init$0
      000258 03                     176 	.db	3
      000259 02                     177 	.sleb128	2
      00025A 01                     178 	.db	1
      00025B 09                     179 	.db	9
      00025C 00 0B                  180 	.dw	Sjas$jas_init$7-Sjas$jas_init$2
      00025E 03                     181 	.db	3
      00025F 02                     182 	.sleb128	2
      000260 01                     183 	.db	1
      000261 09                     184 	.db	9
      000262 00 0F                  185 	.dw	Sjas$jas_init$14-Sjas$jas_init$7
      000264 03                     186 	.db	3
      000265 01                     187 	.sleb128	1
      000266 01                     188 	.db	1
      000267 09                     189 	.db	9
      000268 00 06                  190 	.dw	Sjas$jas_init$17-Sjas$jas_init$14
      00026A 03                     191 	.db	3
      00026B 02                     192 	.sleb128	2
      00026C 01                     193 	.db	1
      00026D 09                     194 	.db	9
      00026E 00 06                  195 	.dw	Sjas$jas_init$20-Sjas$jas_init$17
      000270 03                     196 	.db	3
      000271 01                     197 	.sleb128	1
      000272 01                     198 	.db	1
      000273 09                     199 	.db	9
      000274 00 01                  200 	.dw	1+Sjas$jas_init$21-Sjas$jas_init$20
      000276 00                     201 	.db	0
      000277 01                     202 	.uleb128	1
      000278 01                     203 	.db	1
      000279 00                     204 	.db	0
      00027A 05                     205 	.uleb128	5
      00027B 02                     206 	.db	2
      00027C 00 00 82 55            207 	.dw	0,(Sjas$jas$23)
      000280 03                     208 	.db	3
      000281 0C                     209 	.sleb128	12
      000282 01                     210 	.db	1
      000283 09                     211 	.db	9
      000284 00 00                  212 	.dw	Sjas$jas$25-Sjas$jas$23
      000286 03                     213 	.db	3
      000287 01                     214 	.sleb128	1
      000288 01                     215 	.db	1
      000289 09                     216 	.db	9
      00028A 00 09                  217 	.dw	Sjas$jas$28-Sjas$jas$25
      00028C 03                     218 	.db	3
      00028D 03                     219 	.sleb128	3
      00028E 01                     220 	.db	1
      00028F 09                     221 	.db	9
      000290 00 01                  222 	.dw	1+Sjas$jas$29-Sjas$jas$28
      000292 00                     223 	.db	0
      000293 01                     224 	.uleb128	1
      000294 01                     225 	.db	1
      000295                        226 Ldebug_line_end:
                                    227 
                                    228 	.area .debug_loc (NOLOAD)
      000258                        229 Ldebug_loc_start:
      000258 00 00 82 5E            230 	.dw	0,(Sjas$jas$27)
      00025C 00 00 82 5F            231 	.dw	0,(Sjas$jas$30)
      000260 00 02                  232 	.dw	2
      000262 78                     233 	.db	120
      000263 01                     234 	.sleb128	1
      000264 00 00 82 5A            235 	.dw	0,(Sjas$jas$26)
      000268 00 00 82 5E            236 	.dw	0,(Sjas$jas$27)
      00026C 00 02                  237 	.dw	2
      00026E 78                     238 	.db	120
      00026F 03                     239 	.sleb128	3
      000270 00 00 82 55            240 	.dw	0,(Sjas$jas$24)
      000274 00 00 82 5A            241 	.dw	0,(Sjas$jas$26)
      000278 00 02                  242 	.dw	2
      00027A 78                     243 	.db	120
      00027B 01                     244 	.sleb128	1
      00027C 00 00 00 00            245 	.dw	0,0
      000280 00 00 00 00            246 	.dw	0,0
      000284 00 00 82 54            247 	.dw	0,(Sjas$jas_init$19)
      000288 00 00 82 55            248 	.dw	0,(Sjas$jas_init$22)
      00028C 00 02                  249 	.dw	2
      00028E 78                     250 	.db	120
      00028F 01                     251 	.sleb128	1
      000290 00 00 82 50            252 	.dw	0,(Sjas$jas_init$18)
      000294 00 00 82 54            253 	.dw	0,(Sjas$jas_init$19)
      000298 00 02                  254 	.dw	2
      00029A 78                     255 	.db	120
      00029B 02                     256 	.sleb128	2
      00029C 00 00 82 4E            257 	.dw	0,(Sjas$jas_init$16)
      0002A0 00 00 82 50            258 	.dw	0,(Sjas$jas_init$18)
      0002A4 00 02                  259 	.dw	2
      0002A6 78                     260 	.db	120
      0002A7 01                     261 	.sleb128	1
      0002A8 00 00 82 4A            262 	.dw	0,(Sjas$jas_init$15)
      0002AC 00 00 82 4E            263 	.dw	0,(Sjas$jas_init$16)
      0002B0 00 02                  264 	.dw	2
      0002B2 78                     265 	.db	120
      0002B3 02                     266 	.sleb128	2
      0002B4 00 00 82 48            267 	.dw	0,(Sjas$jas_init$13)
      0002B8 00 00 82 4A            268 	.dw	0,(Sjas$jas_init$15)
      0002BC 00 02                  269 	.dw	2
      0002BE 78                     270 	.db	120
      0002BF 01                     271 	.sleb128	1
      0002C0 00 00 82 43            272 	.dw	0,(Sjas$jas_init$12)
      0002C4 00 00 82 48            273 	.dw	0,(Sjas$jas_init$13)
      0002C8 00 02                  274 	.dw	2
      0002CA 78                     275 	.db	120
      0002CB 06                     276 	.sleb128	6
      0002CC 00 00 82 41            277 	.dw	0,(Sjas$jas_init$11)
      0002D0 00 00 82 43            278 	.dw	0,(Sjas$jas_init$12)
      0002D4 00 02                  279 	.dw	2
      0002D6 78                     280 	.db	120
      0002D7 05                     281 	.sleb128	5
      0002D8 00 00 82 3F            282 	.dw	0,(Sjas$jas_init$10)
      0002DC 00 00 82 41            283 	.dw	0,(Sjas$jas_init$11)
      0002E0 00 02                  284 	.dw	2
      0002E2 78                     285 	.db	120
      0002E3 04                     286 	.sleb128	4
      0002E4 00 00 82 3D            287 	.dw	0,(Sjas$jas_init$9)
      0002E8 00 00 82 3F            288 	.dw	0,(Sjas$jas_init$10)
      0002EC 00 02                  289 	.dw	2
      0002EE 78                     290 	.db	120
      0002EF 03                     291 	.sleb128	3
      0002F0 00 00 82 3B            292 	.dw	0,(Sjas$jas_init$8)
      0002F4 00 00 82 3D            293 	.dw	0,(Sjas$jas_init$9)
      0002F8 00 02                  294 	.dw	2
      0002FA 78                     295 	.db	120
      0002FB 02                     296 	.sleb128	2
      0002FC 00 00 82 39            297 	.dw	0,(Sjas$jas_init$6)
      000300 00 00 82 3B            298 	.dw	0,(Sjas$jas_init$8)
      000304 00 02                  299 	.dw	2
      000306 78                     300 	.db	120
      000307 01                     301 	.sleb128	1
      000308 00 00 82 34            302 	.dw	0,(Sjas$jas_init$5)
      00030C 00 00 82 39            303 	.dw	0,(Sjas$jas_init$6)
      000310 00 02                  304 	.dw	2
      000312 78                     305 	.db	120
      000313 04                     306 	.sleb128	4
      000314 00 00 82 32            307 	.dw	0,(Sjas$jas_init$4)
      000318 00 00 82 34            308 	.dw	0,(Sjas$jas_init$5)
      00031C 00 02                  309 	.dw	2
      00031E 78                     310 	.db	120
      00031F 03                     311 	.sleb128	3
      000320 00 00 82 30            312 	.dw	0,(Sjas$jas_init$3)
      000324 00 00 82 32            313 	.dw	0,(Sjas$jas_init$4)
      000328 00 02                  314 	.dw	2
      00032A 78                     315 	.db	120
      00032B 02                     316 	.sleb128	2
      00032C 00 00 82 2E            317 	.dw	0,(Sjas$jas_init$1)
      000330 00 00 82 30            318 	.dw	0,(Sjas$jas_init$3)
      000334 00 02                  319 	.dw	2
      000336 78                     320 	.db	120
      000337 01                     321 	.sleb128	1
      000338 00 00 00 00            322 	.dw	0,0
      00033C 00 00 00 00            323 	.dw	0,0
                                    324 
                                    325 	.area .debug_abbrev (NOLOAD)
      0000DA                        326 Ldebug_abbrev:
      0000DA 04                     327 	.uleb128	4
      0000DB 05                     328 	.uleb128	5
      0000DC 00                     329 	.db	0
      0000DD 02                     330 	.uleb128	2
      0000DE 0A                     331 	.uleb128	10
      0000DF 03                     332 	.uleb128	3
      0000E0 08                     333 	.uleb128	8
      0000E1 49                     334 	.uleb128	73
      0000E2 13                     335 	.uleb128	19
      0000E3 00                     336 	.uleb128	0
      0000E4 00                     337 	.uleb128	0
      0000E5 03                     338 	.uleb128	3
      0000E6 2E                     339 	.uleb128	46
      0000E7 01                     340 	.db	1
      0000E8 01                     341 	.uleb128	1
      0000E9 13                     342 	.uleb128	19
      0000EA 03                     343 	.uleb128	3
      0000EB 08                     344 	.uleb128	8
      0000EC 11                     345 	.uleb128	17
      0000ED 01                     346 	.uleb128	1
      0000EE 12                     347 	.uleb128	18
      0000EF 01                     348 	.uleb128	1
      0000F0 3F                     349 	.uleb128	63
      0000F1 0C                     350 	.uleb128	12
      0000F2 40                     351 	.uleb128	64
      0000F3 06                     352 	.uleb128	6
      0000F4 00                     353 	.uleb128	0
      0000F5 00                     354 	.uleb128	0
      0000F6 01                     355 	.uleb128	1
      0000F7 11                     356 	.uleb128	17
      0000F8 01                     357 	.db	1
      0000F9 03                     358 	.uleb128	3
      0000FA 08                     359 	.uleb128	8
      0000FB 10                     360 	.uleb128	16
      0000FC 06                     361 	.uleb128	6
      0000FD 13                     362 	.uleb128	19
      0000FE 0B                     363 	.uleb128	11
      0000FF 25                     364 	.uleb128	37
      000100 08                     365 	.uleb128	8
      000101 00                     366 	.uleb128	0
      000102 00                     367 	.uleb128	0
      000103 02                     368 	.uleb128	2
      000104 2E                     369 	.uleb128	46
      000105 00                     370 	.db	0
      000106 03                     371 	.uleb128	3
      000107 08                     372 	.uleb128	8
      000108 11                     373 	.uleb128	17
      000109 01                     374 	.uleb128	1
      00010A 12                     375 	.uleb128	18
      00010B 01                     376 	.uleb128	1
      00010C 3F                     377 	.uleb128	63
      00010D 0C                     378 	.uleb128	12
      00010E 40                     379 	.uleb128	64
      00010F 06                     380 	.uleb128	6
      000110 00                     381 	.uleb128	0
      000111 00                     382 	.uleb128	0
      000112 05                     383 	.uleb128	5
      000113 24                     384 	.uleb128	36
      000114 00                     385 	.db	0
      000115 03                     386 	.uleb128	3
      000116 08                     387 	.uleb128	8
      000117 0B                     388 	.uleb128	11
      000118 0B                     389 	.uleb128	11
      000119 3E                     390 	.uleb128	62
      00011A 0B                     391 	.uleb128	11
      00011B 00                     392 	.uleb128	0
      00011C 00                     393 	.uleb128	0
      00011D 00                     394 	.uleb128	0
                                    395 
                                    396 	.area .debug_info (NOLOAD)
      0001D0 00 00 00 81            397 	.dw	0,Ldebug_info_end-Ldebug_info_start
      0001D4                        398 Ldebug_info_start:
      0001D4 00 02                  399 	.dw	2
      0001D6 00 00 00 DA            400 	.dw	0,(Ldebug_abbrev)
      0001DA 04                     401 	.db	4
      0001DB 01                     402 	.uleb128	1
      0001DC 61 70 70 2F 73 72 63   403 	.ascii "app/src/jas.c"
             2F 6A 61 73 2E 63
      0001E9 00                     404 	.db	0
      0001EA 00 00 01 D3            405 	.dw	0,(Ldebug_line_start+-4)
      0001EE 01                     406 	.db	1
      0001EF 53 44 43 43 20 76 65   407 	.ascii "SDCC version 4.1.0 #12072"
             72 73 69 6F 6E 20 34
             2E 31 2E 30 20 23 31
             32 30 37 32
      000208 00                     408 	.db	0
      000209 02                     409 	.uleb128	2
      00020A 6A 61 73 5F 69 6E 69   410 	.ascii "jas_init"
             74
      000212 00                     411 	.db	0
      000213 00 00 82 2E            412 	.dw	0,(_jas_init)
      000217 00 00 82 55            413 	.dw	0,(XG$jas_init$0$0+1)
      00021B 01                     414 	.db	1
      00021C 00 00 02 84            415 	.dw	0,(Ldebug_loc_start+44)
      000220 03                     416 	.uleb128	3
      000221 00 00 00 71            417 	.dw	0,113
      000225 6A 61 73               418 	.ascii "jas"
      000228 00                     419 	.db	0
      000229 00 00 82 55            420 	.dw	0,(_jas)
      00022D 00 00 82 5F            421 	.dw	0,(XG$jas$0$0+1)
      000231 01                     422 	.db	1
      000232 00 00 02 58            423 	.dw	0,(Ldebug_loc_start)
      000236 04                     424 	.uleb128	4
      000237 02                     425 	.db	2
      000238 91                     426 	.db	145
      000239 02                     427 	.sleb128	2
      00023A 6A                     428 	.ascii "j"
      00023B 00                     429 	.db	0
      00023C 00 00 00 71            430 	.dw	0,113
      000240 00                     431 	.uleb128	0
      000241 05                     432 	.uleb128	5
      000242 75 6E 73 69 67 6E 65   433 	.ascii "unsigned char"
             64 20 63 68 61 72
      00024F 00                     434 	.db	0
      000250 01                     435 	.db	1
      000251 08                     436 	.db	8
      000252 00                     437 	.uleb128	0
      000253 00                     438 	.uleb128	0
      000254 00                     439 	.uleb128	0
      000255                        440 Ldebug_info_end:
                                    441 
                                    442 	.area .debug_pubnames (NOLOAD)
      000088 00 00 00 23            443 	.dw	0,Ldebug_pubnames_end-Ldebug_pubnames_start
      00008C                        444 Ldebug_pubnames_start:
      00008C 00 02                  445 	.dw	2
      00008E 00 00 01 D0            446 	.dw	0,(Ldebug_info_start-4)
      000092 00 00 00 85            447 	.dw	0,4+Ldebug_info_end-Ldebug_info_start
      000096 00 00 00 39            448 	.dw	0,57
      00009A 6A 61 73 5F 69 6E 69   449 	.ascii "jas_init"
             74
      0000A2 00                     450 	.db	0
      0000A3 00 00 00 50            451 	.dw	0,80
      0000A7 6A 61 73               452 	.ascii "jas"
      0000AA 00                     453 	.db	0
      0000AB 00 00 00 00            454 	.dw	0,0
      0000AF                        455 Ldebug_pubnames_end:
                                    456 
                                    457 	.area .debug_frame (NOLOAD)
      00020E 00 00                  458 	.dw	0
      000210 00 0E                  459 	.dw	Ldebug_CIE0_end-Ldebug_CIE0_start
      000212                        460 Ldebug_CIE0_start:
      000212 FF FF                  461 	.dw	0xffff
      000214 FF FF                  462 	.dw	0xffff
      000216 01                     463 	.db	1
      000217 00                     464 	.db	0
      000218 01                     465 	.uleb128	1
      000219 7F                     466 	.sleb128	-1
      00021A 09                     467 	.db	9
      00021B 0C                     468 	.db	12
      00021C 08                     469 	.uleb128	8
      00021D 02                     470 	.uleb128	2
      00021E 89                     471 	.db	137
      00021F 01                     472 	.uleb128	1
      000220                        473 Ldebug_CIE0_end:
      000220 00 00 00 21            474 	.dw	0,33
      000224 00 00 02 0E            475 	.dw	0,(Ldebug_CIE0_start-4)
      000228 00 00 82 55            476 	.dw	0,(Sjas$jas$24)	;initial loc
      00022C 00 00 00 0A            477 	.dw	0,Sjas$jas$30-Sjas$jas$24
      000230 01                     478 	.db	1
      000231 00 00 82 55            479 	.dw	0,(Sjas$jas$24)
      000235 0E                     480 	.db	14
      000236 02                     481 	.uleb128	2
      000237 01                     482 	.db	1
      000238 00 00 82 5A            483 	.dw	0,(Sjas$jas$26)
      00023C 0E                     484 	.db	14
      00023D 04                     485 	.uleb128	4
      00023E 01                     486 	.db	1
      00023F 00 00 82 5E            487 	.dw	0,(Sjas$jas$27)
      000243 0E                     488 	.db	14
      000244 02                     489 	.uleb128	2
                                    490 
                                    491 	.area .debug_frame (NOLOAD)
      000245 00 00                  492 	.dw	0
      000247 00 0E                  493 	.dw	Ldebug_CIE1_end-Ldebug_CIE1_start
      000249                        494 Ldebug_CIE1_start:
      000249 FF FF                  495 	.dw	0xffff
      00024B FF FF                  496 	.dw	0xffff
      00024D 01                     497 	.db	1
      00024E 00                     498 	.db	0
      00024F 01                     499 	.uleb128	1
      000250 7F                     500 	.sleb128	-1
      000251 09                     501 	.db	9
      000252 0C                     502 	.db	12
      000253 08                     503 	.uleb128	8
      000254 02                     504 	.uleb128	2
      000255 89                     505 	.db	137
      000256 01                     506 	.uleb128	1
      000257                        507 Ldebug_CIE1_end:
      000257 00 00 00 75            508 	.dw	0,117
      00025B 00 00 02 45            509 	.dw	0,(Ldebug_CIE1_start-4)
      00025F 00 00 82 2E            510 	.dw	0,(Sjas$jas_init$1)	;initial loc
      000263 00 00 00 27            511 	.dw	0,Sjas$jas_init$22-Sjas$jas_init$1
      000267 01                     512 	.db	1
      000268 00 00 82 2E            513 	.dw	0,(Sjas$jas_init$1)
      00026C 0E                     514 	.db	14
      00026D 02                     515 	.uleb128	2
      00026E 01                     516 	.db	1
      00026F 00 00 82 30            517 	.dw	0,(Sjas$jas_init$3)
      000273 0E                     518 	.db	14
      000274 03                     519 	.uleb128	3
      000275 01                     520 	.db	1
      000276 00 00 82 32            521 	.dw	0,(Sjas$jas_init$4)
      00027A 0E                     522 	.db	14
      00027B 04                     523 	.uleb128	4
      00027C 01                     524 	.db	1
      00027D 00 00 82 34            525 	.dw	0,(Sjas$jas_init$5)
      000281 0E                     526 	.db	14
      000282 05                     527 	.uleb128	5
      000283 01                     528 	.db	1
      000284 00 00 82 39            529 	.dw	0,(Sjas$jas_init$6)
      000288 0E                     530 	.db	14
      000289 02                     531 	.uleb128	2
      00028A 01                     532 	.db	1
      00028B 00 00 82 3B            533 	.dw	0,(Sjas$jas_init$8)
      00028F 0E                     534 	.db	14
      000290 03                     535 	.uleb128	3
      000291 01                     536 	.db	1
      000292 00 00 82 3D            537 	.dw	0,(Sjas$jas_init$9)
      000296 0E                     538 	.db	14
      000297 04                     539 	.uleb128	4
      000298 01                     540 	.db	1
      000299 00 00 82 3F            541 	.dw	0,(Sjas$jas_init$10)
      00029D 0E                     542 	.db	14
      00029E 05                     543 	.uleb128	5
      00029F 01                     544 	.db	1
      0002A0 00 00 82 41            545 	.dw	0,(Sjas$jas_init$11)
      0002A4 0E                     546 	.db	14
      0002A5 06                     547 	.uleb128	6
      0002A6 01                     548 	.db	1
      0002A7 00 00 82 43            549 	.dw	0,(Sjas$jas_init$12)
      0002AB 0E                     550 	.db	14
      0002AC 07                     551 	.uleb128	7
      0002AD 01                     552 	.db	1
      0002AE 00 00 82 48            553 	.dw	0,(Sjas$jas_init$13)
      0002B2 0E                     554 	.db	14
      0002B3 02                     555 	.uleb128	2
      0002B4 01                     556 	.db	1
      0002B5 00 00 82 4A            557 	.dw	0,(Sjas$jas_init$15)
      0002B9 0E                     558 	.db	14
      0002BA 03                     559 	.uleb128	3
      0002BB 01                     560 	.db	1
      0002BC 00 00 82 4E            561 	.dw	0,(Sjas$jas_init$16)
      0002C0 0E                     562 	.db	14
      0002C1 02                     563 	.uleb128	2
      0002C2 01                     564 	.db	1
      0002C3 00 00 82 50            565 	.dw	0,(Sjas$jas_init$18)
      0002C7 0E                     566 	.db	14
      0002C8 03                     567 	.uleb128	3
      0002C9 01                     568 	.db	1
      0002CA 00 00 82 54            569 	.dw	0,(Sjas$jas_init$19)
      0002CE 0E                     570 	.db	14
      0002CF 02                     571 	.uleb128	2
