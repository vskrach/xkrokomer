;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.1.0 #12072 (MINGW64)
;--------------------------------------------------------
	.module main
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl _prepocet
	.globl _jas
	.globl _jas_init
	.globl _delay_05s
	.globl _delay_05s_init
	.globl _pc_write_string
	.globl _pc_init
	.globl _UART1_GetFlagStatus
	.globl _UART1_ReceiveData8
	.globl _GPIO_ReadInputPin
	.globl _GPIO_WriteReverse
	.globl _GPIO_WriteLow
	.globl _GPIO_WriteHigh
	.globl _GPIO_Write
	.globl _GPIO_Init
	.globl _CLK_HSIPrescalerConfig
	.globl _kroky
	.globl _js
	.globl _b
	.globl _a
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
G$a$0_0$0==.
_a::
	.ds 1
G$b$0_0$0==.
_b::
	.ds 1
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
G$js$0_0$0==.
_js::
	.ds 4
G$kroky$0_0$0==.
_kroky::
	.ds 4
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG
__start__stack:
	.ds	1

;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)

; default segment ordering for linker
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area CONST
	.area INITIALIZER
	.area CODE

;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME
__interrupt_vect:
	int s_GSINIT ; reset
	int _TRAP_IRQHandler ; trap
	int _TLI_IRQHandler ; int0
	int _AWU_IRQHandler ; int1
	int _CLK_IRQHandler ; int2
	int _EXTI_PORTA_IRQHandler ; int3
	int _EXTI_PORTB_IRQHandler ; int4
	int _EXTI_PORTC_IRQHandler ; int5
	int _EXTI_PORTD_IRQHandler ; int6
	int 0x000000 ; int7
	int _CAN_RX_IRQHandler ; int8
	int _CAN_TX_IRQHandler ; int9
	int _SPI_IRQHandler ; int10
	int _TIM1_UPD_OVF_TRG_BRK_IRQHandler ; int11
	int _TIM1_CAP_COM_IRQHandler ; int12
	int _TIM2_UPD_OVF_BRK_IRQHandler ; int13
	int _TIM2_CAP_COM_IRQHandler ; int14
	int _TIM3_UPD_OVF_BRK_IRQHandler ; int15
	int _TIM3_CAP_COM_IRQHandler ; int16
	int _UART1_TX_IRQHandler ; int17
	int _UART1_RX_IRQHandler ; int18
	int _I2C_IRQHandler ; int19
	int _UART3_TX_IRQHandler ; int20
	int _UART3_RX_IRQHandler ; int21
	int _ADC2_IRQHandler ; int22
	int _TIM4_UPD_OVF_IRQHandler ; int23
	int _EEPROM_EEC_IRQHandler ; int24
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
__sdcc_init_data:
; stm8_genXINIT() start
	ldw x, #l_DATA
	jreq	00002$
00001$:
	clr (s_DATA - 1, x)
	decw x
	jrne	00001$
00002$:
	ldw	x, #l_INITIALIZER
	jreq	00004$
00003$:
	ld	a, (s_INITIALIZER - 1, x)
	ld	(s_INITIALIZED - 1, x), a
	decw	x
	jrne	00003$
00004$:
; stm8_genXINIT() end
	.area GSFINAL
	jp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
__sdcc_program_startup:
	jp	_main
;	return from main will return to caller
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
	Smain$prepocet$0 ==.
;	app/src/main.c: 14: void prepocet(void){
;	-----------------------------------------
;	 function prepocet
;	-----------------------------------------
_prepocet:
	Smain$prepocet$1 ==.
	Smain$prepocet$2 ==.
;	app/src/main.c: 16: a = kroky % 10;
	push	#0x0a
	Smain$prepocet$3 ==.
	clrw	x
	pushw	x
	Smain$prepocet$4 ==.
	push	#0x00
	Smain$prepocet$5 ==.
	ldw	x, _kroky+2
	pushw	x
	Smain$prepocet$6 ==.
	ldw	x, _kroky+0
	pushw	x
	Smain$prepocet$7 ==.
	call	__modulong
	addw	sp, #8
	Smain$prepocet$8 ==.
	ld	a, xl
	ld	_a+0, a
	Smain$prepocet$9 ==.
;	app/src/main.c: 17: b = kroky - a;
	ld	a, _kroky+3
	sub	a, _a+0
	ld	_b+0, a
	Smain$prepocet$10 ==.
;	app/src/main.c: 18: b = b/10;
	clrw	x
	ld	a, _b+0
	ld	xl, a
	push	#0x0a
	Smain$prepocet$11 ==.
	push	#0x00
	Smain$prepocet$12 ==.
	pushw	x
	Smain$prepocet$13 ==.
	call	__divsint
	addw	sp, #4
	Smain$prepocet$14 ==.
	ld	a, xl
	ld	_b+0, a
	Smain$prepocet$15 ==.
;	app/src/main.c: 19: GPIO_Write(GPIOB,a);
	push	_a+0
	Smain$prepocet$16 ==.
	push	#0x05
	Smain$prepocet$17 ==.
	push	#0x50
	Smain$prepocet$18 ==.
	call	_GPIO_Write
	addw	sp, #3
	Smain$prepocet$19 ==.
	Smain$prepocet$20 ==.
;	app/src/main.c: 20: GPIO_Write(GPIOG,b);
	push	_b+0
	Smain$prepocet$21 ==.
	push	#0x1e
	Smain$prepocet$22 ==.
	push	#0x50
	Smain$prepocet$23 ==.
	call	_GPIO_Write
	addw	sp, #3
	Smain$prepocet$24 ==.
	Smain$prepocet$25 ==.
;	app/src/main.c: 21: GPIO_WriteLow(GPIOG, GPIO_PIN_3);
	push	#0x08
	Smain$prepocet$26 ==.
	push	#0x1e
	Smain$prepocet$27 ==.
	push	#0x50
	Smain$prepocet$28 ==.
	call	_GPIO_WriteLow
	addw	sp, #3
	Smain$prepocet$29 ==.
	Smain$prepocet$30 ==.
;	app/src/main.c: 22: }
	Smain$prepocet$31 ==.
	XG$prepocet$0$0 ==.
	ret
	Smain$prepocet$32 ==.
	Smain$main$33 ==.
;	app/src/main.c: 35: void main(void)
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
	Smain$main$34 ==.
	pushw	x
	Smain$main$35 ==.
	Smain$main$36 ==.
;	app/src/main.c: 38: CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // FREQ MCU 16MHz
	push	#0x00
	Smain$main$37 ==.
	call	_CLK_HSIPrescalerConfig
	pop	a
	Smain$main$38 ==.
	Smain$main$39 ==.
;	app/src/main.c: 40: GPIO_Init(GPIOG, GPIO_PIN_0, GPIO_MODE_OUT_PP_LOW_SLOW);    //inicializace GPIOA = 1. SEGMENT
	push	#0xc0
	Smain$main$40 ==.
	push	#0x01
	Smain$main$41 ==.
	push	#0x1e
	Smain$main$42 ==.
	push	#0x50
	Smain$main$43 ==.
	call	_GPIO_Init
	addw	sp, #4
	Smain$main$44 ==.
	Smain$main$45 ==.
;	app/src/main.c: 41: GPIO_Init(GPIOG, GPIO_PIN_1, GPIO_MODE_OUT_PP_LOW_SLOW);
	push	#0xc0
	Smain$main$46 ==.
	push	#0x02
	Smain$main$47 ==.
	push	#0x1e
	Smain$main$48 ==.
	push	#0x50
	Smain$main$49 ==.
	call	_GPIO_Init
	addw	sp, #4
	Smain$main$50 ==.
	Smain$main$51 ==.
;	app/src/main.c: 42: GPIO_Init(GPIOG, GPIO_PIN_2, GPIO_MODE_OUT_PP_LOW_SLOW);
	push	#0xc0
	Smain$main$52 ==.
	push	#0x04
	Smain$main$53 ==.
	push	#0x1e
	Smain$main$54 ==.
	push	#0x50
	Smain$main$55 ==.
	call	_GPIO_Init
	addw	sp, #4
	Smain$main$56 ==.
	Smain$main$57 ==.
;	app/src/main.c: 43: GPIO_Init(GPIOG, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);
	push	#0xc0
	Smain$main$58 ==.
	push	#0x08
	Smain$main$59 ==.
	push	#0x1e
	Smain$main$60 ==.
	push	#0x50
	Smain$main$61 ==.
	call	_GPIO_Init
	addw	sp, #4
	Smain$main$62 ==.
	Smain$main$63 ==.
;	app/src/main.c: 45: GPIO_Init(GPIOB, GPIO_PIN_0, GPIO_MODE_OUT_PP_LOW_SLOW);     //inicializace GPIOB = 2. SEGMENT
	push	#0xc0
	Smain$main$64 ==.
	push	#0x01
	Smain$main$65 ==.
	push	#0x05
	Smain$main$66 ==.
	push	#0x50
	Smain$main$67 ==.
	call	_GPIO_Init
	addw	sp, #4
	Smain$main$68 ==.
	Smain$main$69 ==.
;	app/src/main.c: 46: GPIO_Init(GPIOB, GPIO_PIN_1, GPIO_MODE_OUT_PP_LOW_SLOW);
	push	#0xc0
	Smain$main$70 ==.
	push	#0x02
	Smain$main$71 ==.
	push	#0x05
	Smain$main$72 ==.
	push	#0x50
	Smain$main$73 ==.
	call	_GPIO_Init
	addw	sp, #4
	Smain$main$74 ==.
	Smain$main$75 ==.
;	app/src/main.c: 47: GPIO_Init(GPIOB, GPIO_PIN_2, GPIO_MODE_OUT_PP_LOW_SLOW);
	push	#0xc0
	Smain$main$76 ==.
	push	#0x04
	Smain$main$77 ==.
	push	#0x05
	Smain$main$78 ==.
	push	#0x50
	Smain$main$79 ==.
	call	_GPIO_Init
	addw	sp, #4
	Smain$main$80 ==.
	Smain$main$81 ==.
;	app/src/main.c: 48: GPIO_Init(GPIOB, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);
	push	#0xc0
	Smain$main$82 ==.
	push	#0x08
	Smain$main$83 ==.
	push	#0x05
	Smain$main$84 ==.
	push	#0x50
	Smain$main$85 ==.
	call	_GPIO_Init
	addw	sp, #4
	Smain$main$86 ==.
	Smain$main$87 ==.
;	app/src/main.c: 50: GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);
	push	#0xc0
	Smain$main$88 ==.
	push	#0x20
	Smain$main$89 ==.
	push	#0x0a
	Smain$main$90 ==.
	push	#0x50
	Smain$main$91 ==.
	call	_GPIO_Init
	addw	sp, #4
	Smain$main$92 ==.
	Smain$main$93 ==.
;	app/src/main.c: 51: GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
	push	#0x20
	Smain$main$94 ==.
	push	#0x0a
	Smain$main$95 ==.
	push	#0x50
	Smain$main$96 ==.
	call	_GPIO_WriteHigh
	addw	sp, #3
	Smain$main$97 ==.
	Smain$main$98 ==.
;	app/src/main.c: 53: GPIO_Init(GPIOE, GPIO_PIN_4, GPIO_MODE_IN_FL_IT);        //inicializace Senzoru
	push	#0x20
	Smain$main$99 ==.
	push	#0x10
	Smain$main$100 ==.
	push	#0x14
	Smain$main$101 ==.
	push	#0x50
	Smain$main$102 ==.
	call	_GPIO_Init
	addw	sp, #4
	Smain$main$103 ==.
	Smain$main$104 ==.
;	app/src/main.c: 57: jas_init();
	call	_jas_init
	Smain$main$105 ==.
;	app/src/main.c: 58: pc_init();
	call	_pc_init
	Smain$main$106 ==.
;	app/src/main.c: 59: delay_05s_init();
	call	_delay_05s_init
	Smain$main$107 ==.
	Smain$main$108 ==.
;	app/src/main.c: 62: uint8_t beta = 0;
	clr	(0x01, sp)
	Smain$main$109 ==.
	Smain$main$110 ==.
;	app/src/main.c: 64: pc_write_string("\n6 >>> +krok/n4 >>> -krok");
	push	#<(___str_0+0)
	Smain$main$111 ==.
	push	#((___str_0+0) >> 8)
	Smain$main$112 ==.
	call	_pc_write_string
	popw	x
	Smain$main$113 ==.
	Smain$main$114 ==.
;	app/src/main.c: 65: while (1)
00127$:
	Smain$main$115 ==.
	Smain$main$116 ==.
;	app/src/main.c: 69: if(UART1_GetFlagStatus(UART1_FLAG_RXNE))
	push	#0x20
	Smain$main$117 ==.
	push	#0x00
	Smain$main$118 ==.
	call	_UART1_GetFlagStatus
	popw	x
	Smain$main$119 ==.
	tnz	a
	jrne	00186$
	jp	00117$
00186$:
	Smain$main$120 ==.
	Smain$main$121 ==.
;	app/src/main.c: 71: key = UART1_ReceiveData8();
	call	_UART1_ReceiveData8
	Smain$main$122 ==.
;	app/src/main.c: 72: if(key == ('5'))
	cp	a, #0x35
	jreq	00189$
	jp	00114$
00189$:
	Smain$main$123 ==.
	Smain$main$124 ==.
	Smain$main$125 ==.
;	app/src/main.c: 74: kroky = 99;
	ldw	x, #0x0063
	ldw	_kroky+2, x
	clrw	x
	ldw	_kroky+0, x
	Smain$main$126 ==.
;	app/src/main.c: 75: delay_05s(1);
	push	#0x01
	Smain$main$127 ==.
	clrw	x
	pushw	x
	Smain$main$128 ==.
	push	#0x00
	Smain$main$129 ==.
	call	_delay_05s
	addw	sp, #4
	Smain$main$130 ==.
	Smain$main$131 ==.
;	app/src/main.c: 76: kroky = 0;
	clrw	x
	ldw	_kroky+2, x
	ldw	_kroky+0, x
	Smain$main$132 ==.
;	app/src/main.c: 77: delay_05s(1);
	push	#0x01
	Smain$main$133 ==.
	clrw	x
	pushw	x
	Smain$main$134 ==.
	push	#0x00
	Smain$main$135 ==.
	call	_delay_05s
	addw	sp, #4
	Smain$main$136 ==.
	Smain$main$137 ==.
;	app/src/main.c: 78: kroky = 99;
	ldw	x, #0x0063
	ldw	_kroky+2, x
	clrw	x
	ldw	_kroky+0, x
	Smain$main$138 ==.
;	app/src/main.c: 79: delay_05s(1);
	push	#0x01
	Smain$main$139 ==.
	clrw	x
	pushw	x
	Smain$main$140 ==.
	push	#0x00
	Smain$main$141 ==.
	call	_delay_05s
	addw	sp, #4
	Smain$main$142 ==.
	Smain$main$143 ==.
;	app/src/main.c: 80: kroky = 0;
	clrw	x
	ldw	_kroky+2, x
	ldw	_kroky+0, x
	Smain$main$144 ==.
;	app/src/main.c: 81: pc_write_string("Vynulování");
	push	#<(___str_1+0)
	Smain$main$145 ==.
	push	#((___str_1+0) >> 8)
	Smain$main$146 ==.
	call	_pc_write_string
	popw	x
	Smain$main$147 ==.
	Smain$main$148 ==.
	jp	00117$
00114$:
	Smain$main$149 ==.
;	app/src/main.c: 83: else if(key == ('6'))
	cp	a, #0x36
	jrne	00111$
	Smain$main$150 ==.
	Smain$main$151 ==.
	Smain$main$152 ==.
;	app/src/main.c: 85: kroky = kroky + 1;
	ldw	x, _kroky+2
	addw	x, #0x0001
	ldw	y, _kroky+0
	jrnc	00193$
	incw	y
00193$:
	ldw	_kroky+2, x
	ldw	_kroky+0, y
	Smain$main$153 ==.
;	app/src/main.c: 86: pc_write_string("+krok");
	push	#<(___str_2+0)
	Smain$main$154 ==.
	push	#((___str_2+0) >> 8)
	Smain$main$155 ==.
	call	_pc_write_string
	popw	x
	Smain$main$156 ==.
	Smain$main$157 ==.
	jp	00117$
00111$:
	Smain$main$158 ==.
;	app/src/main.c: 88: else if(key == ('4'))
	cp	a, #0x34
	jrne	00108$
	Smain$main$159 ==.
	Smain$main$160 ==.
	Smain$main$161 ==.
;	app/src/main.c: 90: kroky = kroky - 1;
	ldw	x, _kroky+2
	subw	x, #0x0001
	ldw	y, _kroky+0
	jrnc	00197$
	decw	y
00197$:
	ldw	_kroky+2, x
	ldw	_kroky+0, y
	Smain$main$162 ==.
;	app/src/main.c: 91: pc_write_string("-krok");
	push	#<(___str_3+0)
	Smain$main$163 ==.
	push	#((___str_3+0) >> 8)
	Smain$main$164 ==.
	call	_pc_write_string
	popw	x
	Smain$main$165 ==.
	Smain$main$166 ==.
	jp	00117$
00108$:
	Smain$main$167 ==.
;	app/src/main.c: 93: else if(key == ('2'))
	cp	a, #0x32
	jrne	00105$
	Smain$main$168 ==.
	Smain$main$169 ==.
	Smain$main$170 ==.
;	app/src/main.c: 95: js = js - 10;
	ldw	x, _js+2
	subw	x, #0x000a
	ldw	y, _js+0
	jrnc	00201$
	decw	y
00201$:
	ldw	_js+2, x
	ldw	_js+0, y
	Smain$main$171 ==.
;	app/src/main.c: 96: pc_write_string("-jas");
	push	#<(___str_4+0)
	Smain$main$172 ==.
	push	#((___str_4+0) >> 8)
	Smain$main$173 ==.
	call	_pc_write_string
	popw	x
	Smain$main$174 ==.
	Smain$main$175 ==.
	jra	00117$
00105$:
	Smain$main$176 ==.
;	app/src/main.c: 98: else if(key == ('8'))
	cp	a, #0x38
	jrne	00102$
	Smain$main$177 ==.
	Smain$main$178 ==.
	Smain$main$179 ==.
;	app/src/main.c: 100: js = js + 10;
	ldw	x, _js+2
	addw	x, #0x000a
	ldw	y, _js+0
	jrnc	00205$
	incw	y
00205$:
	ldw	_js+2, x
	ldw	_js+0, y
	Smain$main$180 ==.
;	app/src/main.c: 101: pc_write_string("+jas");
	push	#<(___str_5+0)
	Smain$main$181 ==.
	push	#((___str_5+0) >> 8)
	Smain$main$182 ==.
	call	_pc_write_string
	popw	x
	Smain$main$183 ==.
	Smain$main$184 ==.
	jra	00117$
00102$:
	Smain$main$185 ==.
	Smain$main$186 ==.
;	app/src/main.c: 105: pc_write_string("UNKNOWN");
	push	#<(___str_6+0)
	Smain$main$187 ==.
	push	#((___str_6+0) >> 8)
	Smain$main$188 ==.
	call	_pc_write_string
	popw	x
	Smain$main$189 ==.
	Smain$main$190 ==.
00117$:
	Smain$main$191 ==.
;	app/src/main.c: 108: alpha = GPIO_ReadInputPin(GPIOE, GPIO_PIN_4);
	push	#0x10
	Smain$main$192 ==.
	push	#0x14
	Smain$main$193 ==.
	push	#0x50
	Smain$main$194 ==.
	call	_GPIO_ReadInputPin
	addw	sp, #3
	Smain$main$195 ==.
	ld	(0x02, sp), a
	Smain$main$196 ==.
;	app/src/main.c: 109: if(alpha==0)
	tnz	a
	jreq	00206$
	jp	00121$
00206$:
	Smain$main$197 ==.
	Smain$main$198 ==.
;	app/src/main.c: 111: if(beta == 1)
	ld	a, (0x01, sp)
	dec	a
	jrne	00121$
	Smain$main$199 ==.
	Smain$main$200 ==.
	Smain$main$201 ==.
;	app/src/main.c: 114: beta = 0;
	clr	(0x01, sp)
	Smain$main$202 ==.
;	app/src/main.c: 115: GPIO_WriteReverse(GPIOC,GPIO_PIN_5);
	push	#0x20
	Smain$main$203 ==.
	push	#0x0a
	Smain$main$204 ==.
	push	#0x50
	Smain$main$205 ==.
	call	_GPIO_WriteReverse
	addw	sp, #3
	Smain$main$206 ==.
	Smain$main$207 ==.
;	app/src/main.c: 116: pc_write_string("+krok");
	push	#<(___str_2+0)
	Smain$main$208 ==.
	push	#((___str_2+0) >> 8)
	Smain$main$209 ==.
	call	_pc_write_string
	popw	x
	Smain$main$210 ==.
	Smain$main$211 ==.
;	app/src/main.c: 117: delay_05s(1);
	push	#0x01
	Smain$main$212 ==.
	clrw	x
	pushw	x
	Smain$main$213 ==.
	push	#0x00
	Smain$main$214 ==.
	call	_delay_05s
	addw	sp, #4
	Smain$main$215 ==.
	Smain$main$216 ==.
00121$:
	Smain$main$217 ==.
;	app/src/main.c: 121: if(alpha>1)
	ld	a, (0x02, sp)
	cp	a, #0x01
	jrugt	00210$
	jp	00125$
00210$:
	Smain$main$218 ==.
	Smain$main$219 ==.
;	app/src/main.c: 123: if(beta == 0)
	tnz	(0x01, sp)
	jrne	00125$
	Smain$main$220 ==.
	Smain$main$221 ==.
;	app/src/main.c: 125: beta = 1;
	ld	a, #0x01
	ld	(0x01, sp), a
	Smain$main$222 ==.
;	app/src/main.c: 126: kroky = kroky + 1;
	ldw	x, _kroky+2
	addw	x, #0x0001
	ldw	y, _kroky+0
	jrnc	00212$
	incw	y
00212$:
	ldw	_kroky+2, x
	ldw	_kroky+0, y
	Smain$main$223 ==.
;	app/src/main.c: 127: pc_write_string("beta");
	push	#<(___str_7+0)
	Smain$main$224 ==.
	push	#((___str_7+0) >> 8)
	Smain$main$225 ==.
	call	_pc_write_string
	popw	x
	Smain$main$226 ==.
	Smain$main$227 ==.
;	app/src/main.c: 128: delay_05s(1);
	push	#0x01
	Smain$main$228 ==.
	clrw	x
	pushw	x
	Smain$main$229 ==.
	push	#0x00
	Smain$main$230 ==.
	call	_delay_05s
	addw	sp, #4
	Smain$main$231 ==.
	Smain$main$232 ==.
00125$:
	Smain$main$233 ==.
;	app/src/main.c: 133: jas(js);
	ld	a, _js+3
	push	a
	Smain$main$234 ==.
	call	_jas
	pop	a
	Smain$main$235 ==.
	Smain$main$236 ==.
;	app/src/main.c: 134: prepocet();
	call	_prepocet
	Smain$main$237 ==.
	jp	00127$
	Smain$main$238 ==.
	Smain$main$239 ==.
;	app/src/main.c: 136: }
	popw	x
	Smain$main$240 ==.
	Smain$main$241 ==.
	XG$main$0$0 ==.
	ret
	Smain$main$242 ==.
	.area CODE
	.area CONST
Fmain$__str_0$0_0$0 == .
	.area CONST
___str_0:
	.db 0x0a
	.ascii "6 >>> +krok/n4 >>> -krok"
	.db 0x00
	.area CODE
Fmain$__str_1$0_0$0 == .
	.area CONST
___str_1:
	.ascii "Vynulov"
	.db 0xc3
	.db 0xa1
	.ascii "n"
	.db 0xc3
	.db 0xad
	.db 0x00
	.area CODE
Fmain$__str_2$0_0$0 == .
	.area CONST
___str_2:
	.ascii "+krok"
	.db 0x00
	.area CODE
Fmain$__str_3$0_0$0 == .
	.area CONST
___str_3:
	.ascii "-krok"
	.db 0x00
	.area CODE
Fmain$__str_4$0_0$0 == .
	.area CONST
___str_4:
	.ascii "-jas"
	.db 0x00
	.area CODE
Fmain$__str_5$0_0$0 == .
	.area CONST
___str_5:
	.ascii "+jas"
	.db 0x00
	.area CODE
Fmain$__str_6$0_0$0 == .
	.area CONST
___str_6:
	.ascii "UNKNOWN"
	.db 0x00
	.area CODE
Fmain$__str_7$0_0$0 == .
	.area CONST
___str_7:
	.ascii "beta"
	.db 0x00
	.area CODE
	.area INITIALIZER
Fmain$__xinit_js$0_0$0 == .
__xinit__js:
	.byte #0x00, #0x00, #0x00, #0xfa	; 250
Fmain$__xinit_kroky$0_0$0 == .
__xinit__kroky:
	.byte #0x00, #0x00, #0x00, #0x00	; 0
	.area CABS (ABS)

	.area .debug_line (NOLOAD)
	.dw	0,Ldebug_line_end-Ldebug_line_start
Ldebug_line_start:
	.dw	2
	.dw	0,Ldebug_line_stmt-6-Ldebug_line_start
	.db	1
	.db	1
	.db	-5
	.db	15
	.db	10
	.db	0
	.db	1
	.db	1
	.db	1
	.db	1
	.db	0
	.db	0
	.db	0
	.db	1
	.ascii "C:\Program Files\SDCC\bin\..\include\stm8"
	.db	0
	.ascii "C:\Program Files\SDCC\bin\..\include"
	.db	0
	.db	0
	.ascii "app/src/main.c"
	.db	0
	.uleb128	0
	.uleb128	0
	.uleb128	0
	.db	0
Ldebug_line_stmt:
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Smain$prepocet$0)
	.db	3
	.sleb128	13
	.db	1
	.db	9
	.dw	Smain$prepocet$2-Smain$prepocet$0
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$prepocet$9-Smain$prepocet$2
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$prepocet$10-Smain$prepocet$9
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$prepocet$15-Smain$prepocet$10
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$prepocet$20-Smain$prepocet$15
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$prepocet$25-Smain$prepocet$20
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$prepocet$30-Smain$prepocet$25
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Smain$prepocet$31-Smain$prepocet$30
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Smain$main$33)
	.db	3
	.sleb128	34
	.db	1
	.db	9
	.dw	Smain$main$36-Smain$main$33
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Smain$main$39-Smain$main$36
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$45-Smain$main$39
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$51-Smain$main$45
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$57-Smain$main$51
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$63-Smain$main$57
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$69-Smain$main$63
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$75-Smain$main$69
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$81-Smain$main$75
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$87-Smain$main$81
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$93-Smain$main$87
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$98-Smain$main$93
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$104-Smain$main$98
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Smain$main$105-Smain$main$104
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$106-Smain$main$105
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$108-Smain$main$106
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Smain$main$110-Smain$main$108
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$114-Smain$main$110
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$116-Smain$main$114
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Smain$main$121-Smain$main$116
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$122-Smain$main$121
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$125-Smain$main$122
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$126-Smain$main$125
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$131-Smain$main$126
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$132-Smain$main$131
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$137-Smain$main$132
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$138-Smain$main$137
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$143-Smain$main$138
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$144-Smain$main$143
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$149-Smain$main$144
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$152-Smain$main$149
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$153-Smain$main$152
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$158-Smain$main$153
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$161-Smain$main$158
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$162-Smain$main$161
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$167-Smain$main$162
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$170-Smain$main$167
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$171-Smain$main$170
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$176-Smain$main$171
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$179-Smain$main$176
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$180-Smain$main$179
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$186-Smain$main$180
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Smain$main$191-Smain$main$186
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Smain$main$196-Smain$main$191
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$198-Smain$main$196
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$201-Smain$main$198
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Smain$main$202-Smain$main$201
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$207-Smain$main$202
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$211-Smain$main$207
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$217-Smain$main$211
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Smain$main$219-Smain$main$217
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$221-Smain$main$219
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Smain$main$222-Smain$main$221
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$223-Smain$main$222
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$227-Smain$main$223
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$233-Smain$main$227
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Smain$main$236-Smain$main$233
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Smain$main$239-Smain$main$236
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Smain$main$241-Smain$main$239
	.db	0
	.uleb128	1
	.db	1
Ldebug_line_end:

	.area .debug_loc (NOLOAD)
Ldebug_loc_start:
	.dw	0,(Smain$main$240)
	.dw	0,(Smain$main$242)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Smain$main$235)
	.dw	0,(Smain$main$240)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$234)
	.dw	0,(Smain$main$235)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$231)
	.dw	0,(Smain$main$234)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$230)
	.dw	0,(Smain$main$231)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Smain$main$229)
	.dw	0,(Smain$main$230)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$228)
	.dw	0,(Smain$main$229)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$226)
	.dw	0,(Smain$main$228)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$225)
	.dw	0,(Smain$main$226)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$224)
	.dw	0,(Smain$main$225)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$215)
	.dw	0,(Smain$main$224)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$214)
	.dw	0,(Smain$main$215)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Smain$main$213)
	.dw	0,(Smain$main$214)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$212)
	.dw	0,(Smain$main$213)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$210)
	.dw	0,(Smain$main$212)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$209)
	.dw	0,(Smain$main$210)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$208)
	.dw	0,(Smain$main$209)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$206)
	.dw	0,(Smain$main$208)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$205)
	.dw	0,(Smain$main$206)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$204)
	.dw	0,(Smain$main$205)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$203)
	.dw	0,(Smain$main$204)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$199)
	.dw	0,(Smain$main$203)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$195)
	.dw	0,(Smain$main$199)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$194)
	.dw	0,(Smain$main$195)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$193)
	.dw	0,(Smain$main$194)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$192)
	.dw	0,(Smain$main$193)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$189)
	.dw	0,(Smain$main$192)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$188)
	.dw	0,(Smain$main$189)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$187)
	.dw	0,(Smain$main$188)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$183)
	.dw	0,(Smain$main$187)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$182)
	.dw	0,(Smain$main$183)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$181)
	.dw	0,(Smain$main$182)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$177)
	.dw	0,(Smain$main$181)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$174)
	.dw	0,(Smain$main$177)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$173)
	.dw	0,(Smain$main$174)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$172)
	.dw	0,(Smain$main$173)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$168)
	.dw	0,(Smain$main$172)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$165)
	.dw	0,(Smain$main$168)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$164)
	.dw	0,(Smain$main$165)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$163)
	.dw	0,(Smain$main$164)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$159)
	.dw	0,(Smain$main$163)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$156)
	.dw	0,(Smain$main$159)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$155)
	.dw	0,(Smain$main$156)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$154)
	.dw	0,(Smain$main$155)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$150)
	.dw	0,(Smain$main$154)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$147)
	.dw	0,(Smain$main$150)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$146)
	.dw	0,(Smain$main$147)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$145)
	.dw	0,(Smain$main$146)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$142)
	.dw	0,(Smain$main$145)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$141)
	.dw	0,(Smain$main$142)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Smain$main$140)
	.dw	0,(Smain$main$141)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$139)
	.dw	0,(Smain$main$140)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$136)
	.dw	0,(Smain$main$139)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$135)
	.dw	0,(Smain$main$136)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Smain$main$134)
	.dw	0,(Smain$main$135)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$133)
	.dw	0,(Smain$main$134)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$130)
	.dw	0,(Smain$main$133)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$129)
	.dw	0,(Smain$main$130)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Smain$main$128)
	.dw	0,(Smain$main$129)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$127)
	.dw	0,(Smain$main$128)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$123)
	.dw	0,(Smain$main$127)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$119)
	.dw	0,(Smain$main$123)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$118)
	.dw	0,(Smain$main$119)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$117)
	.dw	0,(Smain$main$118)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$113)
	.dw	0,(Smain$main$117)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$112)
	.dw	0,(Smain$main$113)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$111)
	.dw	0,(Smain$main$112)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$103)
	.dw	0,(Smain$main$111)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$102)
	.dw	0,(Smain$main$103)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Smain$main$101)
	.dw	0,(Smain$main$102)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$100)
	.dw	0,(Smain$main$101)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$99)
	.dw	0,(Smain$main$100)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$97)
	.dw	0,(Smain$main$99)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$96)
	.dw	0,(Smain$main$97)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$95)
	.dw	0,(Smain$main$96)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$94)
	.dw	0,(Smain$main$95)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$92)
	.dw	0,(Smain$main$94)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$91)
	.dw	0,(Smain$main$92)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Smain$main$90)
	.dw	0,(Smain$main$91)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$89)
	.dw	0,(Smain$main$90)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$88)
	.dw	0,(Smain$main$89)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$86)
	.dw	0,(Smain$main$88)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$85)
	.dw	0,(Smain$main$86)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Smain$main$84)
	.dw	0,(Smain$main$85)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$83)
	.dw	0,(Smain$main$84)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$82)
	.dw	0,(Smain$main$83)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$80)
	.dw	0,(Smain$main$82)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$79)
	.dw	0,(Smain$main$80)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Smain$main$78)
	.dw	0,(Smain$main$79)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$77)
	.dw	0,(Smain$main$78)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$76)
	.dw	0,(Smain$main$77)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$74)
	.dw	0,(Smain$main$76)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$73)
	.dw	0,(Smain$main$74)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Smain$main$72)
	.dw	0,(Smain$main$73)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$71)
	.dw	0,(Smain$main$72)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$70)
	.dw	0,(Smain$main$71)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$68)
	.dw	0,(Smain$main$70)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$67)
	.dw	0,(Smain$main$68)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Smain$main$66)
	.dw	0,(Smain$main$67)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$65)
	.dw	0,(Smain$main$66)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$64)
	.dw	0,(Smain$main$65)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$62)
	.dw	0,(Smain$main$64)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$61)
	.dw	0,(Smain$main$62)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Smain$main$60)
	.dw	0,(Smain$main$61)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$59)
	.dw	0,(Smain$main$60)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$58)
	.dw	0,(Smain$main$59)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$56)
	.dw	0,(Smain$main$58)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$55)
	.dw	0,(Smain$main$56)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Smain$main$54)
	.dw	0,(Smain$main$55)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$53)
	.dw	0,(Smain$main$54)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$52)
	.dw	0,(Smain$main$53)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$50)
	.dw	0,(Smain$main$52)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$49)
	.dw	0,(Smain$main$50)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Smain$main$48)
	.dw	0,(Smain$main$49)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$47)
	.dw	0,(Smain$main$48)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$46)
	.dw	0,(Smain$main$47)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$44)
	.dw	0,(Smain$main$46)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$43)
	.dw	0,(Smain$main$44)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Smain$main$42)
	.dw	0,(Smain$main$43)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Smain$main$41)
	.dw	0,(Smain$main$42)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$main$40)
	.dw	0,(Smain$main$41)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$38)
	.dw	0,(Smain$main$40)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$37)
	.dw	0,(Smain$main$38)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$main$35)
	.dw	0,(Smain$main$37)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$main$34)
	.dw	0,(Smain$main$35)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Smain$prepocet$29)
	.dw	0,(Smain$prepocet$32)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Smain$prepocet$28)
	.dw	0,(Smain$prepocet$29)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$prepocet$27)
	.dw	0,(Smain$prepocet$28)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$prepocet$26)
	.dw	0,(Smain$prepocet$27)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Smain$prepocet$24)
	.dw	0,(Smain$prepocet$26)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Smain$prepocet$23)
	.dw	0,(Smain$prepocet$24)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$prepocet$22)
	.dw	0,(Smain$prepocet$23)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$prepocet$21)
	.dw	0,(Smain$prepocet$22)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Smain$prepocet$19)
	.dw	0,(Smain$prepocet$21)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Smain$prepocet$18)
	.dw	0,(Smain$prepocet$19)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$prepocet$17)
	.dw	0,(Smain$prepocet$18)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$prepocet$16)
	.dw	0,(Smain$prepocet$17)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Smain$prepocet$14)
	.dw	0,(Smain$prepocet$16)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Smain$prepocet$13)
	.dw	0,(Smain$prepocet$14)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$prepocet$12)
	.dw	0,(Smain$prepocet$13)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Smain$prepocet$11)
	.dw	0,(Smain$prepocet$12)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Smain$prepocet$8)
	.dw	0,(Smain$prepocet$11)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Smain$prepocet$7)
	.dw	0,(Smain$prepocet$8)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Smain$prepocet$6)
	.dw	0,(Smain$prepocet$7)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Smain$prepocet$5)
	.dw	0,(Smain$prepocet$6)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Smain$prepocet$4)
	.dw	0,(Smain$prepocet$5)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Smain$prepocet$3)
	.dw	0,(Smain$prepocet$4)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Smain$prepocet$1)
	.dw	0,(Smain$prepocet$3)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0

	.area .debug_abbrev (NOLOAD)
Ldebug_abbrev:
	.uleb128	11
	.uleb128	52
	.db	0
	.uleb128	2
	.uleb128	10
	.uleb128	3
	.uleb128	8
	.uleb128	63
	.uleb128	12
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	13
	.uleb128	1
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	11
	.uleb128	11
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	3
	.uleb128	46
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	0
	.uleb128	0
	.uleb128	9
	.uleb128	52
	.db	0
	.uleb128	2
	.uleb128	10
	.uleb128	3
	.uleb128	8
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	12
	.uleb128	38
	.db	0
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	8
	.uleb128	11
	.db	1
	.uleb128	17
	.uleb128	1
	.uleb128	0
	.uleb128	0
	.uleb128	1
	.uleb128	17
	.db	1
	.uleb128	3
	.uleb128	8
	.uleb128	16
	.uleb128	6
	.uleb128	19
	.uleb128	11
	.uleb128	37
	.uleb128	8
	.uleb128	0
	.uleb128	0
	.uleb128	4
	.uleb128	11
	.db	1
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	0
	.uleb128	0
	.uleb128	7
	.uleb128	11
	.db	0
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	0
	.uleb128	0
	.uleb128	6
	.uleb128	11
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	17
	.uleb128	1
	.uleb128	0
	.uleb128	0
	.uleb128	2
	.uleb128	46
	.db	0
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	0
	.uleb128	0
	.uleb128	5
	.uleb128	11
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	0
	.uleb128	0
	.uleb128	14
	.uleb128	33
	.db	0
	.uleb128	47
	.uleb128	11
	.uleb128	0
	.uleb128	0
	.uleb128	10
	.uleb128	36
	.db	0
	.uleb128	3
	.uleb128	8
	.uleb128	11
	.uleb128	11
	.uleb128	62
	.uleb128	11
	.uleb128	0
	.uleb128	0
	.uleb128	0

	.area .debug_info (NOLOAD)
	.dw	0,Ldebug_info_end-Ldebug_info_start
Ldebug_info_start:
	.dw	2
	.dw	0,(Ldebug_abbrev)
	.db	4
	.uleb128	1
	.ascii "app/src/main.c"
	.db	0
	.dw	0,(Ldebug_line_start+-4)
	.db	1
	.ascii "SDCC version 4.1.0 #12072"
	.db	0
	.uleb128	2
	.ascii "prepocet"
	.db	0
	.dw	0,(_prepocet)
	.dw	0,(XG$prepocet$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1508)
	.uleb128	3
	.dw	0,265
	.ascii "main"
	.db	0
	.dw	0,(_main)
	.dw	0,(XG$main$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start)
	.uleb128	4
	.dw	0,(Smain$main$107)
	.dw	0,(Smain$main$238)
	.uleb128	5
	.dw	0,225
	.dw	0,(Smain$main$115)
	.dw	0,(Smain$main$237)
	.uleb128	6
	.dw	0,190
	.dw	0,(Smain$main$120)
	.uleb128	7
	.dw	0,(Smain$main$124)
	.dw	0,(Smain$main$148)
	.uleb128	7
	.dw	0,(Smain$main$151)
	.dw	0,(Smain$main$157)
	.uleb128	7
	.dw	0,(Smain$main$160)
	.dw	0,(Smain$main$166)
	.uleb128	7
	.dw	0,(Smain$main$169)
	.dw	0,(Smain$main$175)
	.uleb128	7
	.dw	0,(Smain$main$178)
	.dw	0,(Smain$main$184)
	.uleb128	7
	.dw	0,(Smain$main$185)
	.dw	0,(Smain$main$190)
	.uleb128	0
	.uleb128	6
	.dw	0,209
	.dw	0,(Smain$main$197)
	.uleb128	7
	.dw	0,(Smain$main$200)
	.dw	0,(Smain$main$216)
	.uleb128	0
	.uleb128	8
	.dw	0,(Smain$main$218)
	.uleb128	7
	.dw	0,(Smain$main$220)
	.dw	0,(Smain$main$232)
	.uleb128	0
	.uleb128	0
	.uleb128	9
	.db	1
	.db	80
	.ascii "key"
	.db	0
	.dw	0,265
	.uleb128	9
	.db	2
	.db	145
	.sleb128	-1
	.ascii "alpha"
	.db	0
	.dw	0,265
	.uleb128	9
	.db	2
	.db	145
	.sleb128	-2
	.ascii "beta"
	.db	0
	.dw	0,265
	.uleb128	0
	.uleb128	0
	.uleb128	10
	.ascii "unsigned char"
	.db	0
	.db	1
	.db	8
	.uleb128	10
	.ascii "unsigned long"
	.db	0
	.db	4
	.db	7
	.uleb128	11
	.db	5
	.db	3
	.dw	0,(_js)
	.ascii "js"
	.db	0
	.db	1
	.dw	0,282
	.uleb128	11
	.db	5
	.db	3
	.dw	0,(_kroky)
	.ascii "kroky"
	.db	0
	.db	1
	.dw	0,282
	.uleb128	11
	.db	5
	.db	3
	.dw	0,(_a)
	.ascii "a"
	.db	0
	.db	1
	.dw	0,265
	.uleb128	11
	.db	5
	.db	3
	.dw	0,(_b)
	.ascii "b"
	.db	0
	.db	1
	.dw	0,265
	.uleb128	12
	.dw	0,265
	.uleb128	13
	.dw	0,378
	.db	26
	.dw	0,360
	.uleb128	14
	.db	25
	.uleb128	0
	.uleb128	9
	.db	5
	.db	3
	.dw	0,(___str_0)
	.ascii "__str_0"
	.db	0
	.dw	0,365
	.uleb128	13
	.dw	0,410
	.db	13
	.dw	0,360
	.uleb128	14
	.db	12
	.uleb128	0
	.uleb128	9
	.db	5
	.db	3
	.dw	0,(___str_1)
	.ascii "__str_1"
	.db	0
	.dw	0,397
	.uleb128	13
	.dw	0,442
	.db	6
	.dw	0,360
	.uleb128	14
	.db	5
	.uleb128	0
	.uleb128	9
	.db	5
	.db	3
	.dw	0,(___str_2)
	.ascii "__str_2"
	.db	0
	.dw	0,429
	.uleb128	9
	.db	5
	.db	3
	.dw	0,(___str_3)
	.ascii "__str_3"
	.db	0
	.dw	0,429
	.uleb128	13
	.dw	0,493
	.db	5
	.dw	0,360
	.uleb128	14
	.db	4
	.uleb128	0
	.uleb128	9
	.db	5
	.db	3
	.dw	0,(___str_4)
	.ascii "__str_4"
	.db	0
	.dw	0,480
	.uleb128	9
	.db	5
	.db	3
	.dw	0,(___str_5)
	.ascii "__str_5"
	.db	0
	.dw	0,480
	.uleb128	13
	.dw	0,544
	.db	8
	.dw	0,360
	.uleb128	14
	.db	7
	.uleb128	0
	.uleb128	9
	.db	5
	.db	3
	.dw	0,(___str_6)
	.ascii "__str_6"
	.db	0
	.dw	0,531
	.uleb128	9
	.db	5
	.db	3
	.dw	0,(___str_7)
	.ascii "__str_7"
	.db	0
	.dw	0,480
	.uleb128	0
	.uleb128	0
	.uleb128	0
Ldebug_info_end:

	.area .debug_pubnames (NOLOAD)
	.dw	0,Ldebug_pubnames_end-Ldebug_pubnames_start
Ldebug_pubnames_start:
	.dw	2
	.dw	0,(Ldebug_info_start-4)
	.dw	0,4+Ldebug_info_end-Ldebug_info_start
	.dw	0,58
	.ascii "prepocet"
	.db	0
	.dw	0,81
	.ascii "main"
	.db	0
	.dw	0,299
	.ascii "js"
	.db	0
	.dw	0,314
	.ascii "kroky"
	.db	0
	.dw	0,332
	.ascii "a"
	.db	0
	.dw	0,346
	.ascii "b"
	.db	0
	.dw	0,0
Ldebug_pubnames_end:

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE0_end-Ldebug_CIE0_start
Ldebug_CIE0_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE0_end:
	.dw	0,887
	.dw	0,(Ldebug_CIE0_start-4)
	.dw	0,(Smain$main$34)	;initial loc
	.dw	0,Smain$main$242-Smain$main$34
	.db	1
	.dw	0,(Smain$main$34)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Smain$main$35)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$37)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$38)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$40)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$41)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$42)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$43)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Smain$main$44)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$46)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$47)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$48)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$49)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Smain$main$50)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$52)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$53)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$54)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$55)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Smain$main$56)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$58)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$59)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$60)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$61)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Smain$main$62)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$64)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$65)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$66)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$67)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Smain$main$68)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$70)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$71)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$72)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$73)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Smain$main$74)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$76)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$77)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$78)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$79)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Smain$main$80)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$82)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$83)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$84)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$85)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Smain$main$86)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$88)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$89)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$90)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$91)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Smain$main$92)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$94)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$95)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$96)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$97)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$99)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$100)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$101)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$102)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Smain$main$103)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$111)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$112)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$113)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$117)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$118)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$119)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$123)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$127)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$128)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$129)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Smain$main$130)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$133)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$134)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$135)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Smain$main$136)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$139)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$140)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$141)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Smain$main$142)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$145)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$146)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$147)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$150)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$154)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$155)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$156)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$159)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$163)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$164)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$165)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$168)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$172)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$173)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$174)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$177)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$181)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$182)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$183)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$187)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$188)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$189)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$192)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$193)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$194)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$195)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$199)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$203)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$204)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$205)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$206)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$208)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$209)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$210)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$212)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$213)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$214)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Smain$main$215)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$224)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$225)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$main$226)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$228)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$229)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Smain$main$230)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Smain$main$231)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$234)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$main$235)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$main$240)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE1_end-Ldebug_CIE1_start
Ldebug_CIE1_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE1_end:
	.dw	0,173
	.dw	0,(Ldebug_CIE1_start-4)
	.dw	0,(Smain$prepocet$1)	;initial loc
	.dw	0,Smain$prepocet$32-Smain$prepocet$1
	.db	1
	.dw	0,(Smain$prepocet$1)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Smain$prepocet$3)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Smain$prepocet$4)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$prepocet$5)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$prepocet$6)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Smain$prepocet$7)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Smain$prepocet$8)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Smain$prepocet$11)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Smain$prepocet$12)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$prepocet$13)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Smain$prepocet$14)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Smain$prepocet$16)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Smain$prepocet$17)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$prepocet$18)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$prepocet$19)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Smain$prepocet$21)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Smain$prepocet$22)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$prepocet$23)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$prepocet$24)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Smain$prepocet$26)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Smain$prepocet$27)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Smain$prepocet$28)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Smain$prepocet$29)
	.db	14
	.uleb128	2
