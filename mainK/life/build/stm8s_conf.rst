                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.1.0 #12072 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module stm8s_conf
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _assert_failed
                                     12 ;--------------------------------------------------------
                                     13 ; ram data
                                     14 ;--------------------------------------------------------
                                     15 	.area DATA
                                     16 ;--------------------------------------------------------
                                     17 ; ram data
                                     18 ;--------------------------------------------------------
                                     19 	.area INITIALIZED
                                     20 ;--------------------------------------------------------
                                     21 ; absolute external ram data
                                     22 ;--------------------------------------------------------
                                     23 	.area DABS (ABS)
                                     24 
                                     25 ; default segment ordering for linker
                                     26 	.area HOME
                                     27 	.area GSINIT
                                     28 	.area GSFINAL
                                     29 	.area CONST
                                     30 	.area INITIALIZER
                                     31 	.area CODE
                                     32 
                                     33 ;--------------------------------------------------------
                                     34 ; global & static initialisations
                                     35 ;--------------------------------------------------------
                                     36 	.area HOME
                                     37 	.area GSINIT
                                     38 	.area GSFINAL
                                     39 	.area GSINIT
                                     40 ;--------------------------------------------------------
                                     41 ; Home
                                     42 ;--------------------------------------------------------
                                     43 	.area HOME
                                     44 	.area HOME
                                     45 ;--------------------------------------------------------
                                     46 ; code
                                     47 ;--------------------------------------------------------
                                     48 	.area CODE
                           000000    49 	Sstm8s_conf$assert_failed$0 ==.
                                     50 ;	drivers/src/stm8s_conf.c: 12: void assert_failed(uint8_t* file, uint32_t line)
                                     51 ;	-----------------------------------------
                                     52 ;	 function assert_failed
                                     53 ;	-----------------------------------------
      0084F3                         54 _assert_failed:
                           000000    55 	Sstm8s_conf$assert_failed$1 ==.
                           000000    56 	Sstm8s_conf$assert_failed$2 ==.
                                     57 ;	drivers/src/stm8s_conf.c: 21: while (1)
      0084F3                         58 00102$:
      0084F3 20 FE            [ 2]   59 	jra	00102$
                           000002    60 	Sstm8s_conf$assert_failed$3 ==.
                                     61 ;	drivers/src/stm8s_conf.c: 24: }
                           000002    62 	Sstm8s_conf$assert_failed$4 ==.
                           000002    63 	XG$assert_failed$0$0 ==.
      0084F5 81               [ 4]   64 	ret
                           000003    65 	Sstm8s_conf$assert_failed$5 ==.
                                     66 	.area CODE
                                     67 	.area CONST
                                     68 	.area INITIALIZER
                                     69 	.area CABS (ABS)
                                     70 
                                     71 	.area .debug_line (NOLOAD)
      000775 00 00 00 9B             72 	.dw	0,Ldebug_line_end-Ldebug_line_start
      000779                         73 Ldebug_line_start:
      000779 00 02                   74 	.dw	2
      00077B 00 00 00 79             75 	.dw	0,Ldebug_line_stmt-6-Ldebug_line_start
      00077F 01                      76 	.db	1
      000780 01                      77 	.db	1
      000781 FB                      78 	.db	-5
      000782 0F                      79 	.db	15
      000783 0A                      80 	.db	10
      000784 00                      81 	.db	0
      000785 01                      82 	.db	1
      000786 01                      83 	.db	1
      000787 01                      84 	.db	1
      000788 01                      85 	.db	1
      000789 00                      86 	.db	0
      00078A 00                      87 	.db	0
      00078B 00                      88 	.db	0
      00078C 01                      89 	.db	1
      00078D 43 3A 5C 50 72 6F 67    90 	.ascii "C:\Program Files\SDCC\bin\..\include\stm8"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
             5C 73 74 6D 38
      0007B5 00                      91 	.db	0
      0007B6 43 3A 5C 50 72 6F 67    92 	.ascii "C:\Program Files\SDCC\bin\..\include"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
      0007D9 00                      93 	.db	0
      0007DA 00                      94 	.db	0
      0007DB 64 72 69 76 65 72 73    95 	.ascii "drivers/src/stm8s_conf.c"
             2F 73 72 63 2F 73 74
             6D 38 73 5F 63 6F 6E
             66 2E 63
      0007F3 00                      96 	.db	0
      0007F4 00                      97 	.uleb128	0
      0007F5 00                      98 	.uleb128	0
      0007F6 00                      99 	.uleb128	0
      0007F7 00                     100 	.db	0
      0007F8                        101 Ldebug_line_stmt:
      0007F8 00                     102 	.db	0
      0007F9 05                     103 	.uleb128	5
      0007FA 02                     104 	.db	2
      0007FB 00 00 84 F3            105 	.dw	0,(Sstm8s_conf$assert_failed$0)
      0007FF 03                     106 	.db	3
      000800 0B                     107 	.sleb128	11
      000801 01                     108 	.db	1
      000802 09                     109 	.db	9
      000803 00 00                  110 	.dw	Sstm8s_conf$assert_failed$2-Sstm8s_conf$assert_failed$0
      000805 03                     111 	.db	3
      000806 09                     112 	.sleb128	9
      000807 01                     113 	.db	1
      000808 09                     114 	.db	9
      000809 00 02                  115 	.dw	Sstm8s_conf$assert_failed$3-Sstm8s_conf$assert_failed$2
      00080B 03                     116 	.db	3
      00080C 03                     117 	.sleb128	3
      00080D 01                     118 	.db	1
      00080E 09                     119 	.db	9
      00080F 00 01                  120 	.dw	1+Sstm8s_conf$assert_failed$4-Sstm8s_conf$assert_failed$3
      000811 00                     121 	.db	0
      000812 01                     122 	.uleb128	1
      000813 01                     123 	.db	1
      000814                        124 Ldebug_line_end:
                                    125 
                                    126 	.area .debug_loc (NOLOAD)
      000C34                        127 Ldebug_loc_start:
      000C34 00 00 84 F3            128 	.dw	0,(Sstm8s_conf$assert_failed$1)
      000C38 00 00 84 F6            129 	.dw	0,(Sstm8s_conf$assert_failed$5)
      000C3C 00 02                  130 	.dw	2
      000C3E 78                     131 	.db	120
      000C3F 01                     132 	.sleb128	1
      000C40 00 00 00 00            133 	.dw	0,0
      000C44 00 00 00 00            134 	.dw	0,0
                                    135 
                                    136 	.area .debug_abbrev (NOLOAD)
      0001D4                        137 Ldebug_abbrev:
      0001D4 03                     138 	.uleb128	3
      0001D5 0F                     139 	.uleb128	15
      0001D6 00                     140 	.db	0
      0001D7 0B                     141 	.uleb128	11
      0001D8 0B                     142 	.uleb128	11
      0001D9 49                     143 	.uleb128	73
      0001DA 13                     144 	.uleb128	19
      0001DB 00                     145 	.uleb128	0
      0001DC 00                     146 	.uleb128	0
      0001DD 04                     147 	.uleb128	4
      0001DE 05                     148 	.uleb128	5
      0001DF 00                     149 	.db	0
      0001E0 02                     150 	.uleb128	2
      0001E1 0A                     151 	.uleb128	10
      0001E2 03                     152 	.uleb128	3
      0001E3 08                     153 	.uleb128	8
      0001E4 49                     154 	.uleb128	73
      0001E5 13                     155 	.uleb128	19
      0001E6 00                     156 	.uleb128	0
      0001E7 00                     157 	.uleb128	0
      0001E8 02                     158 	.uleb128	2
      0001E9 2E                     159 	.uleb128	46
      0001EA 01                     160 	.db	1
      0001EB 01                     161 	.uleb128	1
      0001EC 13                     162 	.uleb128	19
      0001ED 03                     163 	.uleb128	3
      0001EE 08                     164 	.uleb128	8
      0001EF 11                     165 	.uleb128	17
      0001F0 01                     166 	.uleb128	1
      0001F1 12                     167 	.uleb128	18
      0001F2 01                     168 	.uleb128	1
      0001F3 3F                     169 	.uleb128	63
      0001F4 0C                     170 	.uleb128	12
      0001F5 40                     171 	.uleb128	64
      0001F6 06                     172 	.uleb128	6
      0001F7 00                     173 	.uleb128	0
      0001F8 00                     174 	.uleb128	0
      0001F9 01                     175 	.uleb128	1
      0001FA 11                     176 	.uleb128	17
      0001FB 01                     177 	.db	1
      0001FC 03                     178 	.uleb128	3
      0001FD 08                     179 	.uleb128	8
      0001FE 10                     180 	.uleb128	16
      0001FF 06                     181 	.uleb128	6
      000200 13                     182 	.uleb128	19
      000201 0B                     183 	.uleb128	11
      000202 25                     184 	.uleb128	37
      000203 08                     185 	.uleb128	8
      000204 00                     186 	.uleb128	0
      000205 00                     187 	.uleb128	0
      000206 05                     188 	.uleb128	5
      000207 24                     189 	.uleb128	36
      000208 00                     190 	.db	0
      000209 03                     191 	.uleb128	3
      00020A 08                     192 	.uleb128	8
      00020B 0B                     193 	.uleb128	11
      00020C 0B                     194 	.uleb128	11
      00020D 3E                     195 	.uleb128	62
      00020E 0B                     196 	.uleb128	11
      00020F 00                     197 	.uleb128	0
      000210 00                     198 	.uleb128	0
      000211 00                     199 	.uleb128	0
                                    200 
                                    201 	.area .debug_info (NOLOAD)
      00085B 00 00 00 A6            202 	.dw	0,Ldebug_info_end-Ldebug_info_start
      00085F                        203 Ldebug_info_start:
      00085F 00 02                  204 	.dw	2
      000861 00 00 01 D4            205 	.dw	0,(Ldebug_abbrev)
      000865 04                     206 	.db	4
      000866 01                     207 	.uleb128	1
      000867 64 72 69 76 65 72 73   208 	.ascii "drivers/src/stm8s_conf.c"
             2F 73 72 63 2F 73 74
             6D 38 73 5F 63 6F 6E
             66 2E 63
      00087F 00                     209 	.db	0
      000880 00 00 07 75            210 	.dw	0,(Ldebug_line_start+-4)
      000884 01                     211 	.db	1
      000885 53 44 43 43 20 76 65   212 	.ascii "SDCC version 4.1.0 #12072"
             72 73 69 6F 6E 20 34
             2E 31 2E 30 20 23 31
             32 30 37 32
      00089E 00                     213 	.db	0
      00089F 02                     214 	.uleb128	2
      0008A0 00 00 00 85            215 	.dw	0,133
      0008A4 61 73 73 65 72 74 5F   216 	.ascii "assert_failed"
             66 61 69 6C 65 64
      0008B1 00                     217 	.db	0
      0008B2 00 00 84 F3            218 	.dw	0,(_assert_failed)
      0008B6 00 00 84 F6            219 	.dw	0,(XG$assert_failed$0$0+1)
      0008BA 01                     220 	.db	1
      0008BB 00 00 0C 34            221 	.dw	0,(Ldebug_loc_start)
      0008BF 03                     222 	.uleb128	3
      0008C0 02                     223 	.db	2
      0008C1 00 00 00 85            224 	.dw	0,133
      0008C5 04                     225 	.uleb128	4
      0008C6 02                     226 	.db	2
      0008C7 91                     227 	.db	145
      0008C8 02                     228 	.sleb128	2
      0008C9 66 69 6C 65            229 	.ascii "file"
      0008CD 00                     230 	.db	0
      0008CE 00 00 00 64            231 	.dw	0,100
      0008D2 04                     232 	.uleb128	4
      0008D3 02                     233 	.db	2
      0008D4 91                     234 	.db	145
      0008D5 04                     235 	.sleb128	4
      0008D6 6C 69 6E 65            236 	.ascii "line"
      0008DA 00                     237 	.db	0
      0008DB 00 00 00 96            238 	.dw	0,150
      0008DF 00                     239 	.uleb128	0
      0008E0 05                     240 	.uleb128	5
      0008E1 75 6E 73 69 67 6E 65   241 	.ascii "unsigned char"
             64 20 63 68 61 72
      0008EE 00                     242 	.db	0
      0008EF 01                     243 	.db	1
      0008F0 08                     244 	.db	8
      0008F1 05                     245 	.uleb128	5
      0008F2 75 6E 73 69 67 6E 65   246 	.ascii "unsigned long"
             64 20 6C 6F 6E 67
      0008FF 00                     247 	.db	0
      000900 04                     248 	.db	4
      000901 07                     249 	.db	7
      000902 00                     250 	.uleb128	0
      000903 00                     251 	.uleb128	0
      000904 00                     252 	.uleb128	0
      000905                        253 Ldebug_info_end:
                                    254 
                                    255 	.area .debug_pubnames (NOLOAD)
      00036F 00 00 00 20            256 	.dw	0,Ldebug_pubnames_end-Ldebug_pubnames_start
      000373                        257 Ldebug_pubnames_start:
      000373 00 02                  258 	.dw	2
      000375 00 00 08 5B            259 	.dw	0,(Ldebug_info_start-4)
      000379 00 00 00 AA            260 	.dw	0,4+Ldebug_info_end-Ldebug_info_start
      00037D 00 00 00 44            261 	.dw	0,68
      000381 61 73 73 65 72 74 5F   262 	.ascii "assert_failed"
             66 61 69 6C 65 64
      00038E 00                     263 	.db	0
      00038F 00 00 00 00            264 	.dw	0,0
      000393                        265 Ldebug_pubnames_end:
                                    266 
                                    267 	.area .debug_frame (NOLOAD)
      000B21 00 00                  268 	.dw	0
      000B23 00 0E                  269 	.dw	Ldebug_CIE0_end-Ldebug_CIE0_start
      000B25                        270 Ldebug_CIE0_start:
      000B25 FF FF                  271 	.dw	0xffff
      000B27 FF FF                  272 	.dw	0xffff
      000B29 01                     273 	.db	1
      000B2A 00                     274 	.db	0
      000B2B 01                     275 	.uleb128	1
      000B2C 7F                     276 	.sleb128	-1
      000B2D 09                     277 	.db	9
      000B2E 0C                     278 	.db	12
      000B2F 08                     279 	.uleb128	8
      000B30 02                     280 	.uleb128	2
      000B31 89                     281 	.db	137
      000B32 01                     282 	.uleb128	1
      000B33                        283 Ldebug_CIE0_end:
      000B33 00 00 00 13            284 	.dw	0,19
      000B37 00 00 0B 21            285 	.dw	0,(Ldebug_CIE0_start-4)
      000B3B 00 00 84 F3            286 	.dw	0,(Sstm8s_conf$assert_failed$1)	;initial loc
      000B3F 00 00 00 03            287 	.dw	0,Sstm8s_conf$assert_failed$5-Sstm8s_conf$assert_failed$1
      000B43 01                     288 	.db	1
      000B44 00 00 84 F3            289 	.dw	0,(Sstm8s_conf$assert_failed$1)
      000B48 0E                     290 	.db	14
      000B49 02                     291 	.uleb128	2
