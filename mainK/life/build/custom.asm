;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.1.0 #12072 (MINGW64)
;--------------------------------------------------------
	.module custom
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _UART1_GetFlagStatus
	.globl _UART1_SendData8
	.globl _UART1_ReceiveData8
	.globl _UART1_Init
	.globl _pc_init
	.globl _pc_write_char
	.globl _pc_write_string
	.globl _pc_read_char
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)

; default segment ordering for linker
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area CONST
	.area INITIALIZER
	.area CODE

;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
	Scustom$pc_init$0 ==.
;	app/src/custom.c: 4: void pc_init(void)
;	-----------------------------------------
;	 function pc_init
;	-----------------------------------------
_pc_init:
	Scustom$pc_init$1 ==.
	Scustom$pc_init$2 ==.
;	app/src/custom.c: 6: UART1_Init(9600,UART1_WORDLENGTH_8D,UART1_STOPBITS_1,UART1_PARITY_NO,UART1_SYNCMODE_CLOCK_DISABLE,UART1_MODE_TXRX_ENABLE);
	push	#0x0c
	Scustom$pc_init$3 ==.
	push	#0x80
	Scustom$pc_init$4 ==.
	push	#0x00
	Scustom$pc_init$5 ==.
	push	#0x00
	Scustom$pc_init$6 ==.
	push	#0x00
	Scustom$pc_init$7 ==.
	push	#0x80
	Scustom$pc_init$8 ==.
	push	#0x25
	Scustom$pc_init$9 ==.
	clrw	x
	pushw	x
	Scustom$pc_init$10 ==.
	call	_UART1_Init
	addw	sp, #9
	Scustom$pc_init$11 ==.
	Scustom$pc_init$12 ==.
;	app/src/custom.c: 7: }
	Scustom$pc_init$13 ==.
	XG$pc_init$0$0 ==.
	ret
	Scustom$pc_init$14 ==.
	Scustom$pc_write_char$15 ==.
;	app/src/custom.c: 9: void pc_write_char(char c)
;	-----------------------------------------
;	 function pc_write_char
;	-----------------------------------------
_pc_write_char:
	Scustom$pc_write_char$16 ==.
	Scustom$pc_write_char$17 ==.
;	app/src/custom.c: 11: while (!(UART1_GetFlagStatus(UART1_FLAG_TXE))){;}
00101$:
	push	#0x80
	Scustom$pc_write_char$18 ==.
	push	#0x00
	Scustom$pc_write_char$19 ==.
	call	_UART1_GetFlagStatus
	popw	x
	Scustom$pc_write_char$20 ==.
	tnz	a
	jreq	00101$
	Scustom$pc_write_char$21 ==.
;	app/src/custom.c: 12: UART1_SendData8(c);
	ld	a, (0x03, sp)
	push	a
	Scustom$pc_write_char$22 ==.
	call	_UART1_SendData8
	pop	a
	Scustom$pc_write_char$23 ==.
	Scustom$pc_write_char$24 ==.
;	app/src/custom.c: 13: }
	Scustom$pc_write_char$25 ==.
	XG$pc_write_char$0$0 ==.
	ret
	Scustom$pc_write_char$26 ==.
	Scustom$pc_write_string$27 ==.
	Scustom$pc_write_string$28 ==.
;	app/src/custom.c: 14: void pc_write_string(char str[])
;	-----------------------------------------
;	 function pc_write_string
;	-----------------------------------------
_pc_write_string:
	Scustom$pc_write_string$29 ==.
	sub	sp, #4
	Scustom$pc_write_string$30 ==.
	Scustom$pc_write_string$31 ==.
;	app/src/custom.c: 16: for(uint32_t i=0;str[i];i++)
	clrw	y
	clrw	x
	ldw	(0x01, sp), x
00103$:
	ldw	x, y
	addw	x, (0x07, sp)
	ld	a, (x)
	jreq	00105$
	Scustom$pc_write_string$32 ==.
	Scustom$pc_write_string$33 ==.
;	app/src/custom.c: 17: {pc_write_char(str[i]);}
	pushw	y
	Scustom$pc_write_string$34 ==.
	push	a
	Scustom$pc_write_string$35 ==.
	call	_pc_write_char
	pop	a
	Scustom$pc_write_string$36 ==.
	popw	y
	Scustom$pc_write_string$37 ==.
	Scustom$pc_write_string$38 ==.
	Scustom$pc_write_string$39 ==.
;	app/src/custom.c: 16: for(uint32_t i=0;str[i];i++)
	incw	y
	jrne	00103$
	ldw	x, (0x01, sp)
	incw	x
	ldw	(0x01, sp), x
	jra	00103$
00105$:
	Scustom$pc_write_string$40 ==.
;	app/src/custom.c: 18: }
	addw	sp, #4
	Scustom$pc_write_string$41 ==.
	Scustom$pc_write_string$42 ==.
	XG$pc_write_string$0$0 ==.
	ret
	Scustom$pc_write_string$43 ==.
	Scustom$pc_read_char$44 ==.
;	app/src/custom.c: 19: char pc_read_char(void)
;	-----------------------------------------
;	 function pc_read_char
;	-----------------------------------------
_pc_read_char:
	Scustom$pc_read_char$45 ==.
	Scustom$pc_read_char$46 ==.
;	app/src/custom.c: 21: while (!(UART1_GetFlagStatus(UART1_FLAG_RXNE))){;}
00101$:
	push	#0x20
	Scustom$pc_read_char$47 ==.
	push	#0x00
	Scustom$pc_read_char$48 ==.
	call	_UART1_GetFlagStatus
	popw	x
	Scustom$pc_read_char$49 ==.
	tnz	a
	jreq	00101$
	Scustom$pc_read_char$50 ==.
;	app/src/custom.c: 22: return UART1_ReceiveData8();
	jp	_UART1_ReceiveData8
	Scustom$pc_read_char$51 ==.
;	app/src/custom.c: 23: }
	Scustom$pc_read_char$52 ==.
	XG$pc_read_char$0$0 ==.
	ret
	Scustom$pc_read_char$53 ==.
	.area CODE
	.area CONST
	.area INITIALIZER
	.area CABS (ABS)

	.area .debug_line (NOLOAD)
	.dw	0,Ldebug_line_end-Ldebug_line_start
Ldebug_line_start:
	.dw	2
	.dw	0,Ldebug_line_stmt-6-Ldebug_line_start
	.db	1
	.db	1
	.db	-5
	.db	15
	.db	10
	.db	0
	.db	1
	.db	1
	.db	1
	.db	1
	.db	0
	.db	0
	.db	0
	.db	1
	.ascii "C:\Program Files\SDCC\bin\..\include\stm8"
	.db	0
	.ascii "C:\Program Files\SDCC\bin\..\include"
	.db	0
	.db	0
	.ascii "app/src/custom.c"
	.db	0
	.uleb128	0
	.uleb128	0
	.uleb128	0
	.db	0
Ldebug_line_stmt:
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Scustom$pc_init$0)
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Scustom$pc_init$2-Scustom$pc_init$0
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Scustom$pc_init$12-Scustom$pc_init$2
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Scustom$pc_init$13-Scustom$pc_init$12
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Scustom$pc_write_char$15)
	.db	3
	.sleb128	8
	.db	1
	.db	9
	.dw	Scustom$pc_write_char$17-Scustom$pc_write_char$15
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Scustom$pc_write_char$21-Scustom$pc_write_char$17
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Scustom$pc_write_char$24-Scustom$pc_write_char$21
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Scustom$pc_write_char$25-Scustom$pc_write_char$24
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Scustom$pc_write_string$28)
	.db	3
	.sleb128	13
	.db	1
	.db	9
	.dw	Scustom$pc_write_string$31-Scustom$pc_write_string$28
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Scustom$pc_write_string$33-Scustom$pc_write_string$31
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Scustom$pc_write_string$39-Scustom$pc_write_string$33
	.db	3
	.sleb128	-1
	.db	1
	.db	9
	.dw	Scustom$pc_write_string$40-Scustom$pc_write_string$39
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Scustom$pc_write_string$42-Scustom$pc_write_string$40
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Scustom$pc_read_char$44)
	.db	3
	.sleb128	18
	.db	1
	.db	9
	.dw	Scustom$pc_read_char$46-Scustom$pc_read_char$44
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Scustom$pc_read_char$50-Scustom$pc_read_char$46
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Scustom$pc_read_char$51-Scustom$pc_read_char$50
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Scustom$pc_read_char$52-Scustom$pc_read_char$51
	.db	0
	.uleb128	1
	.db	1
Ldebug_line_end:

	.area .debug_loc (NOLOAD)
Ldebug_loc_start:
	.dw	0,(Scustom$pc_read_char$49)
	.dw	0,(Scustom$pc_read_char$53)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Scustom$pc_read_char$48)
	.dw	0,(Scustom$pc_read_char$49)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Scustom$pc_read_char$47)
	.dw	0,(Scustom$pc_read_char$48)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Scustom$pc_read_char$45)
	.dw	0,(Scustom$pc_read_char$47)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Scustom$pc_write_string$41)
	.dw	0,(Scustom$pc_write_string$43)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Scustom$pc_write_string$37)
	.dw	0,(Scustom$pc_write_string$41)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Scustom$pc_write_string$36)
	.dw	0,(Scustom$pc_write_string$37)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Scustom$pc_write_string$35)
	.dw	0,(Scustom$pc_write_string$36)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Scustom$pc_write_string$34)
	.dw	0,(Scustom$pc_write_string$35)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Scustom$pc_write_string$30)
	.dw	0,(Scustom$pc_write_string$34)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Scustom$pc_write_string$29)
	.dw	0,(Scustom$pc_write_string$30)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Scustom$pc_write_char$23)
	.dw	0,(Scustom$pc_write_char$26)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Scustom$pc_write_char$22)
	.dw	0,(Scustom$pc_write_char$23)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Scustom$pc_write_char$20)
	.dw	0,(Scustom$pc_write_char$22)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Scustom$pc_write_char$19)
	.dw	0,(Scustom$pc_write_char$20)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Scustom$pc_write_char$18)
	.dw	0,(Scustom$pc_write_char$19)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Scustom$pc_write_char$16)
	.dw	0,(Scustom$pc_write_char$18)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Scustom$pc_init$11)
	.dw	0,(Scustom$pc_init$14)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Scustom$pc_init$10)
	.dw	0,(Scustom$pc_init$11)
	.dw	2
	.db	120
	.sleb128	10
	.dw	0,(Scustom$pc_init$9)
	.dw	0,(Scustom$pc_init$10)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Scustom$pc_init$8)
	.dw	0,(Scustom$pc_init$9)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Scustom$pc_init$7)
	.dw	0,(Scustom$pc_init$8)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Scustom$pc_init$6)
	.dw	0,(Scustom$pc_init$7)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Scustom$pc_init$5)
	.dw	0,(Scustom$pc_init$6)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Scustom$pc_init$4)
	.dw	0,(Scustom$pc_init$5)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Scustom$pc_init$3)
	.dw	0,(Scustom$pc_init$4)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Scustom$pc_init$1)
	.dw	0,(Scustom$pc_init$3)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0

	.area .debug_abbrev (NOLOAD)
Ldebug_abbrev:
	.uleb128	6
	.uleb128	15
	.db	0
	.uleb128	11
	.uleb128	11
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	10
	.uleb128	46
	.db	0
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	4
	.uleb128	5
	.db	0
	.uleb128	2
	.uleb128	10
	.uleb128	3
	.uleb128	8
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	3
	.uleb128	46
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	0
	.uleb128	0
	.uleb128	9
	.uleb128	52
	.db	0
	.uleb128	2
	.uleb128	10
	.uleb128	3
	.uleb128	8
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	7
	.uleb128	11
	.db	1
	.uleb128	17
	.uleb128	1
	.uleb128	0
	.uleb128	0
	.uleb128	1
	.uleb128	17
	.db	1
	.uleb128	3
	.uleb128	8
	.uleb128	16
	.uleb128	6
	.uleb128	19
	.uleb128	11
	.uleb128	37
	.uleb128	8
	.uleb128	0
	.uleb128	0
	.uleb128	8
	.uleb128	11
	.db	0
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	0
	.uleb128	0
	.uleb128	2
	.uleb128	46
	.db	0
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	0
	.uleb128	0
	.uleb128	5
	.uleb128	36
	.db	0
	.uleb128	3
	.uleb128	8
	.uleb128	11
	.uleb128	11
	.uleb128	62
	.uleb128	11
	.uleb128	0
	.uleb128	0
	.uleb128	0

	.area .debug_info (NOLOAD)
	.dw	0,Ldebug_info_end-Ldebug_info_start
Ldebug_info_start:
	.dw	2
	.dw	0,(Ldebug_abbrev)
	.db	4
	.uleb128	1
	.ascii "app/src/custom.c"
	.db	0
	.dw	0,(Ldebug_line_start+-4)
	.db	1
	.ascii "SDCC version 4.1.0 #12072"
	.db	0
	.uleb128	2
	.ascii "pc_init"
	.db	0
	.dw	0,(_pc_init)
	.dw	0,(XG$pc_init$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+228)
	.uleb128	3
	.dw	0,125
	.ascii "pc_write_char"
	.db	0
	.dw	0,(_pc_write_char)
	.dw	0,(XG$pc_write_char$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+148)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "c"
	.db	0
	.dw	0,125
	.uleb128	0
	.uleb128	5
	.ascii "unsigned char"
	.db	0
	.db	1
	.db	8
	.uleb128	3
	.dw	0,232
	.ascii "pc_write_string"
	.db	0
	.dw	0,(_pc_write_string)
	.dw	0,(XG$pc_write_string$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+56)
	.uleb128	6
	.db	2
	.dw	0,125
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "str"
	.db	0
	.dw	0,176
	.uleb128	7
	.dw	0,(Scustom$pc_write_string$27)
	.uleb128	8
	.dw	0,(Scustom$pc_write_string$32)
	.dw	0,(Scustom$pc_write_string$38)
	.uleb128	9
	.db	14
	.db	145
	.sleb128	-4
	.db	147
	.uleb128	1
	.db	145
	.sleb128	-3
	.db	147
	.uleb128	1
	.db	84
	.db	147
	.uleb128	1
	.db	83
	.db	147
	.uleb128	1
	.ascii "i"
	.db	0
	.dw	0,232
	.uleb128	0
	.uleb128	0
	.uleb128	5
	.ascii "unsigned long"
	.db	0
	.db	4
	.db	7
	.uleb128	10
	.ascii "pc_read_char"
	.db	0
	.dw	0,(_pc_read_char)
	.dw	0,(XG$pc_read_char$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start)
	.dw	0,125
	.uleb128	0
	.uleb128	0
	.uleb128	0
Ldebug_info_end:

	.area .debug_pubnames (NOLOAD)
	.dw	0,Ldebug_pubnames_end-Ldebug_pubnames_start
Ldebug_pubnames_start:
	.dw	2
	.dw	0,(Ldebug_info_start-4)
	.dw	0,4+Ldebug_info_end-Ldebug_info_start
	.dw	0,60
	.ascii "pc_init"
	.db	0
	.dw	0,82
	.ascii "pc_write_char"
	.db	0
	.dw	0,142
	.ascii "pc_write_string"
	.db	0
	.dw	0,249
	.ascii "pc_read_char"
	.db	0
	.dw	0,0
Ldebug_pubnames_end:

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE0_end-Ldebug_CIE0_start
Ldebug_CIE0_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE0_end:
	.dw	0,40
	.dw	0,(Ldebug_CIE0_start-4)
	.dw	0,(Scustom$pc_read_char$45)	;initial loc
	.dw	0,Scustom$pc_read_char$53-Scustom$pc_read_char$45
	.db	1
	.dw	0,(Scustom$pc_read_char$45)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Scustom$pc_read_char$47)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Scustom$pc_read_char$48)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Scustom$pc_read_char$49)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE1_end-Ldebug_CIE1_start
Ldebug_CIE1_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE1_end:
	.dw	0,61
	.dw	0,(Ldebug_CIE1_start-4)
	.dw	0,(Scustom$pc_write_string$29)	;initial loc
	.dw	0,Scustom$pc_write_string$43-Scustom$pc_write_string$29
	.db	1
	.dw	0,(Scustom$pc_write_string$29)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Scustom$pc_write_string$30)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Scustom$pc_write_string$34)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Scustom$pc_write_string$35)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Scustom$pc_write_string$36)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Scustom$pc_write_string$37)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Scustom$pc_write_string$41)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE2_end-Ldebug_CIE2_start
Ldebug_CIE2_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE2_end:
	.dw	0,54
	.dw	0,(Ldebug_CIE2_start-4)
	.dw	0,(Scustom$pc_write_char$16)	;initial loc
	.dw	0,Scustom$pc_write_char$26-Scustom$pc_write_char$16
	.db	1
	.dw	0,(Scustom$pc_write_char$16)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Scustom$pc_write_char$18)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Scustom$pc_write_char$19)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Scustom$pc_write_char$20)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Scustom$pc_write_char$22)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Scustom$pc_write_char$23)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE3_end-Ldebug_CIE3_start
Ldebug_CIE3_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE3_end:
	.dw	0,82
	.dw	0,(Ldebug_CIE3_start-4)
	.dw	0,(Scustom$pc_init$1)	;initial loc
	.dw	0,Scustom$pc_init$14-Scustom$pc_init$1
	.db	1
	.dw	0,(Scustom$pc_init$1)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Scustom$pc_init$3)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Scustom$pc_init$4)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Scustom$pc_init$5)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Scustom$pc_init$6)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Scustom$pc_init$7)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Scustom$pc_init$8)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Scustom$pc_init$9)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Scustom$pc_init$10)
	.db	14
	.uleb128	11
	.db	1
	.dw	0,(Scustom$pc_init$11)
	.db	14
	.uleb128	2
