;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.1.0 #12072 (MINGW64)
;--------------------------------------------------------
	.module stm8s_exti
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _assert_failed
	.globl _EXTI_DeInit
	.globl _EXTI_SetExtIntSensitivity
	.globl _EXTI_SetTLISensitivity
	.globl _EXTI_GetExtIntSensitivity
	.globl _EXTI_GetTLISensitivity
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)

; default segment ordering for linker
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area CONST
	.area INITIALIZER
	.area CODE

;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
	Sstm8s_exti$EXTI_DeInit$0 ==.
;	drivers/src/stm8s_exti.c: 53: void EXTI_DeInit(void)
;	-----------------------------------------
;	 function EXTI_DeInit
;	-----------------------------------------
_EXTI_DeInit:
	Sstm8s_exti$EXTI_DeInit$1 ==.
	Sstm8s_exti$EXTI_DeInit$2 ==.
;	drivers/src/stm8s_exti.c: 55: EXTI->CR1 = EXTI_CR1_RESET_VALUE;
	mov	0x50a0+0, #0x00
	Sstm8s_exti$EXTI_DeInit$3 ==.
;	drivers/src/stm8s_exti.c: 56: EXTI->CR2 = EXTI_CR2_RESET_VALUE;
	mov	0x50a1+0, #0x00
	Sstm8s_exti$EXTI_DeInit$4 ==.
;	drivers/src/stm8s_exti.c: 57: }
	Sstm8s_exti$EXTI_DeInit$5 ==.
	XG$EXTI_DeInit$0$0 ==.
	ret
	Sstm8s_exti$EXTI_DeInit$6 ==.
	Sstm8s_exti$EXTI_SetExtIntSensitivity$7 ==.
;	drivers/src/stm8s_exti.c: 70: void EXTI_SetExtIntSensitivity(EXTI_Port_TypeDef Port, EXTI_Sensitivity_TypeDef SensitivityValue)
;	-----------------------------------------
;	 function EXTI_SetExtIntSensitivity
;	-----------------------------------------
_EXTI_SetExtIntSensitivity:
	Sstm8s_exti$EXTI_SetExtIntSensitivity$8 ==.
	push	a
	Sstm8s_exti$EXTI_SetExtIntSensitivity$9 ==.
	Sstm8s_exti$EXTI_SetExtIntSensitivity$10 ==.
;	drivers/src/stm8s_exti.c: 73: assert_param(IS_EXTI_PORT_OK(Port));
	tnz	(0x04, sp)
	jreq	00111$
	ld	a, (0x04, sp)
	dec	a
	jreq	00111$
	Sstm8s_exti$EXTI_SetExtIntSensitivity$11 ==.
	ld	a, (0x04, sp)
	cp	a, #0x02
	jreq	00111$
	Sstm8s_exti$EXTI_SetExtIntSensitivity$12 ==.
	ld	a, (0x04, sp)
	cp	a, #0x03
	jreq	00111$
	Sstm8s_exti$EXTI_SetExtIntSensitivity$13 ==.
	ld	a, (0x04, sp)
	cp	a, #0x04
	jreq	00111$
	Sstm8s_exti$EXTI_SetExtIntSensitivity$14 ==.
	push	#0x49
	Sstm8s_exti$EXTI_SetExtIntSensitivity$15 ==.
	clrw	x
	pushw	x
	Sstm8s_exti$EXTI_SetExtIntSensitivity$16 ==.
	push	#0x00
	Sstm8s_exti$EXTI_SetExtIntSensitivity$17 ==.
	push	#<(___str_0+0)
	Sstm8s_exti$EXTI_SetExtIntSensitivity$18 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_exti$EXTI_SetExtIntSensitivity$19 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_exti$EXTI_SetExtIntSensitivity$20 ==.
00111$:
	Sstm8s_exti$EXTI_SetExtIntSensitivity$21 ==.
;	drivers/src/stm8s_exti.c: 74: assert_param(IS_EXTI_SENSITIVITY_OK(SensitivityValue));
	tnz	(0x05, sp)
	jreq	00125$
	ld	a, (0x05, sp)
	dec	a
	jreq	00125$
	Sstm8s_exti$EXTI_SetExtIntSensitivity$22 ==.
	ld	a, (0x05, sp)
	cp	a, #0x02
	jreq	00125$
	Sstm8s_exti$EXTI_SetExtIntSensitivity$23 ==.
	ld	a, (0x05, sp)
	cp	a, #0x03
	jreq	00125$
	Sstm8s_exti$EXTI_SetExtIntSensitivity$24 ==.
	push	#0x4a
	Sstm8s_exti$EXTI_SetExtIntSensitivity$25 ==.
	clrw	x
	pushw	x
	Sstm8s_exti$EXTI_SetExtIntSensitivity$26 ==.
	push	#0x00
	Sstm8s_exti$EXTI_SetExtIntSensitivity$27 ==.
	push	#<(___str_0+0)
	Sstm8s_exti$EXTI_SetExtIntSensitivity$28 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_exti$EXTI_SetExtIntSensitivity$29 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_exti$EXTI_SetExtIntSensitivity$30 ==.
00125$:
	Sstm8s_exti$EXTI_SetExtIntSensitivity$31 ==.
;	drivers/src/stm8s_exti.c: 77: switch (Port)
	ld	a, (0x04, sp)
	cp	a, #0x04
	jrule	00208$
	jp	00108$
00208$:
	Sstm8s_exti$EXTI_SetExtIntSensitivity$32 ==.
;	drivers/src/stm8s_exti.c: 85: EXTI->CR1 |= (uint8_t)((uint8_t)(SensitivityValue) << 2);
	ld	a, (0x05, sp)
	ld	yl, a
	Sstm8s_exti$EXTI_SetExtIntSensitivity$33 ==.
;	drivers/src/stm8s_exti.c: 77: switch (Port)
	clrw	x
	ld	a, (0x04, sp)
	ld	xl, a
	sllw	x
	ldw	x, (#00209$, x)
	jp	(x)
00209$:
	.dw	#00101$
	.dw	#00102$
	.dw	#00103$
	.dw	#00104$
	.dw	#00105$
	Sstm8s_exti$EXTI_SetExtIntSensitivity$34 ==.
	Sstm8s_exti$EXTI_SetExtIntSensitivity$35 ==.
;	drivers/src/stm8s_exti.c: 79: case EXTI_PORT_GPIOA:
00101$:
	Sstm8s_exti$EXTI_SetExtIntSensitivity$36 ==.
;	drivers/src/stm8s_exti.c: 80: EXTI->CR1 &= (uint8_t)(~EXTI_CR1_PAIS);
	ld	a, 0x50a0
	and	a, #0xfc
	ld	0x50a0, a
	Sstm8s_exti$EXTI_SetExtIntSensitivity$37 ==.
;	drivers/src/stm8s_exti.c: 81: EXTI->CR1 |= (uint8_t)(SensitivityValue);
	ld	a, 0x50a0
	or	a, (0x05, sp)
	ld	0x50a0, a
	Sstm8s_exti$EXTI_SetExtIntSensitivity$38 ==.
;	drivers/src/stm8s_exti.c: 82: break;
	jp	00108$
	Sstm8s_exti$EXTI_SetExtIntSensitivity$39 ==.
;	drivers/src/stm8s_exti.c: 83: case EXTI_PORT_GPIOB:
00102$:
	Sstm8s_exti$EXTI_SetExtIntSensitivity$40 ==.
;	drivers/src/stm8s_exti.c: 84: EXTI->CR1 &= (uint8_t)(~EXTI_CR1_PBIS);
	ld	a, 0x50a0
	and	a, #0xf3
	ld	0x50a0, a
	Sstm8s_exti$EXTI_SetExtIntSensitivity$41 ==.
;	drivers/src/stm8s_exti.c: 85: EXTI->CR1 |= (uint8_t)((uint8_t)(SensitivityValue) << 2);
	ld	a, 0x50a0
	ldw	x, y
	sllw	x
	sllw	x
	pushw	x
	Sstm8s_exti$EXTI_SetExtIntSensitivity$42 ==.
	or	a, (2, sp)
	popw	x
	Sstm8s_exti$EXTI_SetExtIntSensitivity$43 ==.
	ld	0x50a0, a
	Sstm8s_exti$EXTI_SetExtIntSensitivity$44 ==.
;	drivers/src/stm8s_exti.c: 86: break;
	jp	00108$
	Sstm8s_exti$EXTI_SetExtIntSensitivity$45 ==.
;	drivers/src/stm8s_exti.c: 87: case EXTI_PORT_GPIOC:
00103$:
	Sstm8s_exti$EXTI_SetExtIntSensitivity$46 ==.
;	drivers/src/stm8s_exti.c: 88: EXTI->CR1 &= (uint8_t)(~EXTI_CR1_PCIS);
	ld	a, 0x50a0
	and	a, #0xcf
	ld	0x50a0, a
	Sstm8s_exti$EXTI_SetExtIntSensitivity$47 ==.
;	drivers/src/stm8s_exti.c: 89: EXTI->CR1 |= (uint8_t)((uint8_t)(SensitivityValue) << 4);
	ld	a, 0x50a0
	ld	(0x01, sp), a
	ld	a, yl
	swap	a
	and	a, #0xf0
	or	a, (0x01, sp)
	ld	0x50a0, a
	Sstm8s_exti$EXTI_SetExtIntSensitivity$48 ==.
;	drivers/src/stm8s_exti.c: 90: break;
	jra	00108$
	Sstm8s_exti$EXTI_SetExtIntSensitivity$49 ==.
;	drivers/src/stm8s_exti.c: 91: case EXTI_PORT_GPIOD:
00104$:
	Sstm8s_exti$EXTI_SetExtIntSensitivity$50 ==.
;	drivers/src/stm8s_exti.c: 92: EXTI->CR1 &= (uint8_t)(~EXTI_CR1_PDIS);
	ld	a, 0x50a0
	and	a, #0x3f
	ld	0x50a0, a
	Sstm8s_exti$EXTI_SetExtIntSensitivity$51 ==.
;	drivers/src/stm8s_exti.c: 93: EXTI->CR1 |= (uint8_t)((uint8_t)(SensitivityValue) << 6);
	ld	a, 0x50a0
	ld	(0x01, sp), a
	ld	a, yl
	swap	a
	and	a, #0xf0
	sll	a
	sll	a
	or	a, (0x01, sp)
	ld	0x50a0, a
	Sstm8s_exti$EXTI_SetExtIntSensitivity$52 ==.
;	drivers/src/stm8s_exti.c: 94: break;
	jra	00108$
	Sstm8s_exti$EXTI_SetExtIntSensitivity$53 ==.
;	drivers/src/stm8s_exti.c: 95: case EXTI_PORT_GPIOE:
00105$:
	Sstm8s_exti$EXTI_SetExtIntSensitivity$54 ==.
;	drivers/src/stm8s_exti.c: 96: EXTI->CR2 &= (uint8_t)(~EXTI_CR2_PEIS);
	ld	a, 0x50a1
	and	a, #0xfc
	ld	0x50a1, a
	Sstm8s_exti$EXTI_SetExtIntSensitivity$55 ==.
;	drivers/src/stm8s_exti.c: 97: EXTI->CR2 |= (uint8_t)(SensitivityValue);
	ld	a, 0x50a1
	or	a, (0x05, sp)
	ld	0x50a1, a
	Sstm8s_exti$EXTI_SetExtIntSensitivity$56 ==.
	Sstm8s_exti$EXTI_SetExtIntSensitivity$57 ==.
;	drivers/src/stm8s_exti.c: 101: }
00108$:
	Sstm8s_exti$EXTI_SetExtIntSensitivity$58 ==.
;	drivers/src/stm8s_exti.c: 102: }
	pop	a
	Sstm8s_exti$EXTI_SetExtIntSensitivity$59 ==.
	Sstm8s_exti$EXTI_SetExtIntSensitivity$60 ==.
	XG$EXTI_SetExtIntSensitivity$0$0 ==.
	ret
	Sstm8s_exti$EXTI_SetExtIntSensitivity$61 ==.
	Sstm8s_exti$EXTI_SetTLISensitivity$62 ==.
;	drivers/src/stm8s_exti.c: 111: void EXTI_SetTLISensitivity(EXTI_TLISensitivity_TypeDef SensitivityValue)
;	-----------------------------------------
;	 function EXTI_SetTLISensitivity
;	-----------------------------------------
_EXTI_SetTLISensitivity:
	Sstm8s_exti$EXTI_SetTLISensitivity$63 ==.
	Sstm8s_exti$EXTI_SetTLISensitivity$64 ==.
;	drivers/src/stm8s_exti.c: 114: assert_param(IS_EXTI_TLISENSITIVITY_OK(SensitivityValue));
	tnz	(0x03, sp)
	jreq	00104$
	ld	a, (0x03, sp)
	cp	a, #0x04
	jreq	00104$
	Sstm8s_exti$EXTI_SetTLISensitivity$65 ==.
	push	#0x72
	Sstm8s_exti$EXTI_SetTLISensitivity$66 ==.
	clrw	x
	pushw	x
	Sstm8s_exti$EXTI_SetTLISensitivity$67 ==.
	push	#0x00
	Sstm8s_exti$EXTI_SetTLISensitivity$68 ==.
	push	#<(___str_0+0)
	Sstm8s_exti$EXTI_SetTLISensitivity$69 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_exti$EXTI_SetTLISensitivity$70 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_exti$EXTI_SetTLISensitivity$71 ==.
00104$:
	Sstm8s_exti$EXTI_SetTLISensitivity$72 ==.
;	drivers/src/stm8s_exti.c: 117: EXTI->CR2 &= (uint8_t)(~EXTI_CR2_TLIS);
	ld	a, 0x50a1
	and	a, #0xfb
	ld	0x50a1, a
	Sstm8s_exti$EXTI_SetTLISensitivity$73 ==.
;	drivers/src/stm8s_exti.c: 118: EXTI->CR2 |= (uint8_t)(SensitivityValue);
	ld	a, 0x50a1
	or	a, (0x03, sp)
	ld	0x50a1, a
	Sstm8s_exti$EXTI_SetTLISensitivity$74 ==.
;	drivers/src/stm8s_exti.c: 119: }
	Sstm8s_exti$EXTI_SetTLISensitivity$75 ==.
	XG$EXTI_SetTLISensitivity$0$0 ==.
	ret
	Sstm8s_exti$EXTI_SetTLISensitivity$76 ==.
	Sstm8s_exti$EXTI_GetExtIntSensitivity$77 ==.
;	drivers/src/stm8s_exti.c: 126: EXTI_Sensitivity_TypeDef EXTI_GetExtIntSensitivity(EXTI_Port_TypeDef Port)
;	-----------------------------------------
;	 function EXTI_GetExtIntSensitivity
;	-----------------------------------------
_EXTI_GetExtIntSensitivity:
	Sstm8s_exti$EXTI_GetExtIntSensitivity$78 ==.
	Sstm8s_exti$EXTI_GetExtIntSensitivity$79 ==.
;	drivers/src/stm8s_exti.c: 128: uint8_t value = 0;
	clr	a
	Sstm8s_exti$EXTI_GetExtIntSensitivity$80 ==.
;	drivers/src/stm8s_exti.c: 131: assert_param(IS_EXTI_PORT_OK(Port));
	tnz	(0x03, sp)
	jrne	00154$
	jp	00111$
00154$:
	push	a
	Sstm8s_exti$EXTI_GetExtIntSensitivity$81 ==.
	ld	a, (0x04, sp)
	dec	a
	pop	a
	Sstm8s_exti$EXTI_GetExtIntSensitivity$82 ==.
	jrne	00156$
	jp	00111$
00156$:
	Sstm8s_exti$EXTI_GetExtIntSensitivity$83 ==.
	push	a
	Sstm8s_exti$EXTI_GetExtIntSensitivity$84 ==.
	ld	a, (0x04, sp)
	cp	a, #0x02
	pop	a
	Sstm8s_exti$EXTI_GetExtIntSensitivity$85 ==.
	jreq	00111$
	Sstm8s_exti$EXTI_GetExtIntSensitivity$86 ==.
	push	a
	Sstm8s_exti$EXTI_GetExtIntSensitivity$87 ==.
	ld	a, (0x04, sp)
	cp	a, #0x03
	pop	a
	Sstm8s_exti$EXTI_GetExtIntSensitivity$88 ==.
	jreq	00111$
	Sstm8s_exti$EXTI_GetExtIntSensitivity$89 ==.
	push	a
	Sstm8s_exti$EXTI_GetExtIntSensitivity$90 ==.
	ld	a, (0x04, sp)
	cp	a, #0x04
	pop	a
	Sstm8s_exti$EXTI_GetExtIntSensitivity$91 ==.
	jreq	00111$
	Sstm8s_exti$EXTI_GetExtIntSensitivity$92 ==.
	push	a
	Sstm8s_exti$EXTI_GetExtIntSensitivity$93 ==.
	push	#0x83
	Sstm8s_exti$EXTI_GetExtIntSensitivity$94 ==.
	clrw	x
	pushw	x
	Sstm8s_exti$EXTI_GetExtIntSensitivity$95 ==.
	push	#0x00
	Sstm8s_exti$EXTI_GetExtIntSensitivity$96 ==.
	push	#<(___str_0+0)
	Sstm8s_exti$EXTI_GetExtIntSensitivity$97 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_exti$EXTI_GetExtIntSensitivity$98 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_exti$EXTI_GetExtIntSensitivity$99 ==.
	pop	a
	Sstm8s_exti$EXTI_GetExtIntSensitivity$100 ==.
00111$:
	Sstm8s_exti$EXTI_GetExtIntSensitivity$101 ==.
;	drivers/src/stm8s_exti.c: 133: switch (Port)
	push	a
	Sstm8s_exti$EXTI_GetExtIntSensitivity$102 ==.
	ld	a, (0x04, sp)
	cp	a, #0x04
	pop	a
	Sstm8s_exti$EXTI_GetExtIntSensitivity$103 ==.
	jrule	00167$
	jp	00107$
00167$:
	clrw	x
	ld	a, (0x03, sp)
	ld	xl, a
	sllw	x
	ldw	x, (#00168$, x)
	jp	(x)
00168$:
	.dw	#00101$
	.dw	#00102$
	.dw	#00103$
	.dw	#00104$
	.dw	#00105$
	Sstm8s_exti$EXTI_GetExtIntSensitivity$104 ==.
	Sstm8s_exti$EXTI_GetExtIntSensitivity$105 ==.
;	drivers/src/stm8s_exti.c: 135: case EXTI_PORT_GPIOA:
00101$:
	Sstm8s_exti$EXTI_GetExtIntSensitivity$106 ==.
;	drivers/src/stm8s_exti.c: 136: value = (uint8_t)(EXTI->CR1 & EXTI_CR1_PAIS);
	ld	a, 0x50a0
	and	a, #0x03
	Sstm8s_exti$EXTI_GetExtIntSensitivity$107 ==.
;	drivers/src/stm8s_exti.c: 137: break;
	jra	00107$
	Sstm8s_exti$EXTI_GetExtIntSensitivity$108 ==.
;	drivers/src/stm8s_exti.c: 138: case EXTI_PORT_GPIOB:
00102$:
	Sstm8s_exti$EXTI_GetExtIntSensitivity$109 ==.
;	drivers/src/stm8s_exti.c: 139: value = (uint8_t)((uint8_t)(EXTI->CR1 & EXTI_CR1_PBIS) >> 2);
	ld	a, 0x50a0
	and	a, #0x0c
	srl	a
	srl	a
	Sstm8s_exti$EXTI_GetExtIntSensitivity$110 ==.
;	drivers/src/stm8s_exti.c: 140: break;
	jra	00107$
	Sstm8s_exti$EXTI_GetExtIntSensitivity$111 ==.
;	drivers/src/stm8s_exti.c: 141: case EXTI_PORT_GPIOC:
00103$:
	Sstm8s_exti$EXTI_GetExtIntSensitivity$112 ==.
;	drivers/src/stm8s_exti.c: 142: value = (uint8_t)((uint8_t)(EXTI->CR1 & EXTI_CR1_PCIS) >> 4);
	ld	a, 0x50a0
	and	a, #0x30
	swap	a
	and	a, #0x0f
	Sstm8s_exti$EXTI_GetExtIntSensitivity$113 ==.
;	drivers/src/stm8s_exti.c: 143: break;
	jra	00107$
	Sstm8s_exti$EXTI_GetExtIntSensitivity$114 ==.
;	drivers/src/stm8s_exti.c: 144: case EXTI_PORT_GPIOD:
00104$:
	Sstm8s_exti$EXTI_GetExtIntSensitivity$115 ==.
;	drivers/src/stm8s_exti.c: 145: value = (uint8_t)((uint8_t)(EXTI->CR1 & EXTI_CR1_PDIS) >> 6);
	ld	a, 0x50a0
	and	a, #0xc0
	swap	a
	and	a, #0x0f
	srl	a
	srl	a
	Sstm8s_exti$EXTI_GetExtIntSensitivity$116 ==.
;	drivers/src/stm8s_exti.c: 146: break;
	jra	00107$
	Sstm8s_exti$EXTI_GetExtIntSensitivity$117 ==.
;	drivers/src/stm8s_exti.c: 147: case EXTI_PORT_GPIOE:
00105$:
	Sstm8s_exti$EXTI_GetExtIntSensitivity$118 ==.
;	drivers/src/stm8s_exti.c: 148: value = (uint8_t)(EXTI->CR2 & EXTI_CR2_PEIS);
	ld	a, 0x50a1
	and	a, #0x03
	Sstm8s_exti$EXTI_GetExtIntSensitivity$119 ==.
	Sstm8s_exti$EXTI_GetExtIntSensitivity$120 ==.
;	drivers/src/stm8s_exti.c: 152: }
00107$:
	Sstm8s_exti$EXTI_GetExtIntSensitivity$121 ==.
;	drivers/src/stm8s_exti.c: 154: return((EXTI_Sensitivity_TypeDef)value);
	Sstm8s_exti$EXTI_GetExtIntSensitivity$122 ==.
;	drivers/src/stm8s_exti.c: 155: }
	Sstm8s_exti$EXTI_GetExtIntSensitivity$123 ==.
	XG$EXTI_GetExtIntSensitivity$0$0 ==.
	ret
	Sstm8s_exti$EXTI_GetExtIntSensitivity$124 ==.
	Sstm8s_exti$EXTI_GetTLISensitivity$125 ==.
;	drivers/src/stm8s_exti.c: 162: EXTI_TLISensitivity_TypeDef EXTI_GetTLISensitivity(void)
;	-----------------------------------------
;	 function EXTI_GetTLISensitivity
;	-----------------------------------------
_EXTI_GetTLISensitivity:
	Sstm8s_exti$EXTI_GetTLISensitivity$126 ==.
	Sstm8s_exti$EXTI_GetTLISensitivity$127 ==.
;	drivers/src/stm8s_exti.c: 167: value = (uint8_t)(EXTI->CR2 & EXTI_CR2_TLIS);
	ld	a, 0x50a1
	and	a, #0x04
	Sstm8s_exti$EXTI_GetTLISensitivity$128 ==.
;	drivers/src/stm8s_exti.c: 169: return((EXTI_TLISensitivity_TypeDef)value);
	Sstm8s_exti$EXTI_GetTLISensitivity$129 ==.
;	drivers/src/stm8s_exti.c: 170: }
	Sstm8s_exti$EXTI_GetTLISensitivity$130 ==.
	XG$EXTI_GetTLISensitivity$0$0 ==.
	ret
	Sstm8s_exti$EXTI_GetTLISensitivity$131 ==.
	.area CODE
	.area CONST
Fstm8s_exti$__str_0$0_0$0 == .
	.area CONST
___str_0:
	.ascii "drivers/src/stm8s_exti.c"
	.db 0x00
	.area CODE
	.area INITIALIZER
	.area CABS (ABS)

	.area .debug_line (NOLOAD)
	.dw	0,Ldebug_line_end-Ldebug_line_start
Ldebug_line_start:
	.dw	2
	.dw	0,Ldebug_line_stmt-6-Ldebug_line_start
	.db	1
	.db	1
	.db	-5
	.db	15
	.db	10
	.db	0
	.db	1
	.db	1
	.db	1
	.db	1
	.db	0
	.db	0
	.db	0
	.db	1
	.ascii "C:\Program Files\SDCC\bin\..\include\stm8"
	.db	0
	.ascii "C:\Program Files\SDCC\bin\..\include"
	.db	0
	.db	0
	.ascii "drivers/src/stm8s_exti.c"
	.db	0
	.uleb128	0
	.uleb128	0
	.uleb128	0
	.db	0
Ldebug_line_stmt:
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_exti$EXTI_DeInit$0)
	.db	3
	.sleb128	52
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_DeInit$2-Sstm8s_exti$EXTI_DeInit$0
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_DeInit$3-Sstm8s_exti$EXTI_DeInit$2
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_DeInit$4-Sstm8s_exti$EXTI_DeInit$3
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_exti$EXTI_DeInit$5-Sstm8s_exti$EXTI_DeInit$4
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$7)
	.db	3
	.sleb128	69
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$10-Sstm8s_exti$EXTI_SetExtIntSensitivity$7
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$21-Sstm8s_exti$EXTI_SetExtIntSensitivity$10
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$31-Sstm8s_exti$EXTI_SetExtIntSensitivity$21
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$32-Sstm8s_exti$EXTI_SetExtIntSensitivity$31
	.db	3
	.sleb128	8
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$33-Sstm8s_exti$EXTI_SetExtIntSensitivity$32
	.db	3
	.sleb128	-8
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$35-Sstm8s_exti$EXTI_SetExtIntSensitivity$33
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$36-Sstm8s_exti$EXTI_SetExtIntSensitivity$35
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$37-Sstm8s_exti$EXTI_SetExtIntSensitivity$36
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$38-Sstm8s_exti$EXTI_SetExtIntSensitivity$37
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$39-Sstm8s_exti$EXTI_SetExtIntSensitivity$38
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$40-Sstm8s_exti$EXTI_SetExtIntSensitivity$39
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$41-Sstm8s_exti$EXTI_SetExtIntSensitivity$40
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$44-Sstm8s_exti$EXTI_SetExtIntSensitivity$41
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$45-Sstm8s_exti$EXTI_SetExtIntSensitivity$44
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$46-Sstm8s_exti$EXTI_SetExtIntSensitivity$45
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$47-Sstm8s_exti$EXTI_SetExtIntSensitivity$46
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$48-Sstm8s_exti$EXTI_SetExtIntSensitivity$47
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$49-Sstm8s_exti$EXTI_SetExtIntSensitivity$48
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$50-Sstm8s_exti$EXTI_SetExtIntSensitivity$49
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$51-Sstm8s_exti$EXTI_SetExtIntSensitivity$50
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$52-Sstm8s_exti$EXTI_SetExtIntSensitivity$51
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$53-Sstm8s_exti$EXTI_SetExtIntSensitivity$52
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$54-Sstm8s_exti$EXTI_SetExtIntSensitivity$53
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$55-Sstm8s_exti$EXTI_SetExtIntSensitivity$54
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$57-Sstm8s_exti$EXTI_SetExtIntSensitivity$55
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetExtIntSensitivity$58-Sstm8s_exti$EXTI_SetExtIntSensitivity$57
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_exti$EXTI_SetExtIntSensitivity$60-Sstm8s_exti$EXTI_SetExtIntSensitivity$58
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$62)
	.db	3
	.sleb128	110
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetTLISensitivity$64-Sstm8s_exti$EXTI_SetTLISensitivity$62
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetTLISensitivity$72-Sstm8s_exti$EXTI_SetTLISensitivity$64
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetTLISensitivity$73-Sstm8s_exti$EXTI_SetTLISensitivity$72
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_SetTLISensitivity$74-Sstm8s_exti$EXTI_SetTLISensitivity$73
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_exti$EXTI_SetTLISensitivity$75-Sstm8s_exti$EXTI_SetTLISensitivity$74
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$77)
	.db	3
	.sleb128	125
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$79-Sstm8s_exti$EXTI_GetExtIntSensitivity$77
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$80-Sstm8s_exti$EXTI_GetExtIntSensitivity$79
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$101-Sstm8s_exti$EXTI_GetExtIntSensitivity$80
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$105-Sstm8s_exti$EXTI_GetExtIntSensitivity$101
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$106-Sstm8s_exti$EXTI_GetExtIntSensitivity$105
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$107-Sstm8s_exti$EXTI_GetExtIntSensitivity$106
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$108-Sstm8s_exti$EXTI_GetExtIntSensitivity$107
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$109-Sstm8s_exti$EXTI_GetExtIntSensitivity$108
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$110-Sstm8s_exti$EXTI_GetExtIntSensitivity$109
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$111-Sstm8s_exti$EXTI_GetExtIntSensitivity$110
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$112-Sstm8s_exti$EXTI_GetExtIntSensitivity$111
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$113-Sstm8s_exti$EXTI_GetExtIntSensitivity$112
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$114-Sstm8s_exti$EXTI_GetExtIntSensitivity$113
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$115-Sstm8s_exti$EXTI_GetExtIntSensitivity$114
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$116-Sstm8s_exti$EXTI_GetExtIntSensitivity$115
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$117-Sstm8s_exti$EXTI_GetExtIntSensitivity$116
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$118-Sstm8s_exti$EXTI_GetExtIntSensitivity$117
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$120-Sstm8s_exti$EXTI_GetExtIntSensitivity$118
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$121-Sstm8s_exti$EXTI_GetExtIntSensitivity$120
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetExtIntSensitivity$122-Sstm8s_exti$EXTI_GetExtIntSensitivity$121
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_exti$EXTI_GetExtIntSensitivity$123-Sstm8s_exti$EXTI_GetExtIntSensitivity$122
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_exti$EXTI_GetTLISensitivity$125)
	.db	3
	.sleb128	161
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetTLISensitivity$127-Sstm8s_exti$EXTI_GetTLISensitivity$125
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetTLISensitivity$128-Sstm8s_exti$EXTI_GetTLISensitivity$127
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_exti$EXTI_GetTLISensitivity$129-Sstm8s_exti$EXTI_GetTLISensitivity$128
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_exti$EXTI_GetTLISensitivity$130-Sstm8s_exti$EXTI_GetTLISensitivity$129
	.db	0
	.uleb128	1
	.db	1
Ldebug_line_end:

	.area .debug_loc (NOLOAD)
Ldebug_loc_start:
	.dw	0,(Sstm8s_exti$EXTI_GetTLISensitivity$126)
	.dw	0,(Sstm8s_exti$EXTI_GetTLISensitivity$131)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$103)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$124)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$102)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$103)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$100)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$102)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$99)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$100)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$98)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$99)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$97)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$98)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$96)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$97)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$95)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$96)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$94)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$95)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$93)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$94)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$92)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$93)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$91)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$92)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$90)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$91)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$89)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$90)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$88)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$89)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$87)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$88)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$86)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$87)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$85)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$86)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$84)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$85)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$83)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$84)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$82)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$83)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$81)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$82)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$78)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$81)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$71)
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$76)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$70)
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$71)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$69)
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$70)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$68)
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$69)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$67)
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$68)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$66)
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$67)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$65)
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$66)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$63)
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$65)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$59)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$61)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$43)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$59)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$42)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$43)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$30)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$42)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$29)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$30)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$28)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$29)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$27)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$28)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$26)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$27)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$25)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$26)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$24)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$25)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$23)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$24)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$22)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$23)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$20)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$22)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$19)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$20)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$18)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$19)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$17)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$18)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$16)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$17)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$15)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$16)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$14)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$15)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$13)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$14)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$12)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$13)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$11)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$12)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$9)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$11)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$8)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$9)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_exti$EXTI_DeInit$1)
	.dw	0,(Sstm8s_exti$EXTI_DeInit$6)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0

	.area .debug_abbrev (NOLOAD)
Ldebug_abbrev:
	.uleb128	4
	.uleb128	5
	.db	0
	.uleb128	2
	.uleb128	10
	.uleb128	3
	.uleb128	8
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	10
	.uleb128	1
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	11
	.uleb128	11
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	3
	.uleb128	46
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	0
	.uleb128	0
	.uleb128	8
	.uleb128	52
	.db	0
	.uleb128	2
	.uleb128	10
	.uleb128	3
	.uleb128	8
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	7
	.uleb128	46
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	9
	.uleb128	38
	.db	0
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	1
	.uleb128	17
	.db	1
	.uleb128	3
	.uleb128	8
	.uleb128	16
	.uleb128	6
	.uleb128	19
	.uleb128	11
	.uleb128	37
	.uleb128	8
	.uleb128	0
	.uleb128	0
	.uleb128	5
	.uleb128	11
	.db	0
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	0
	.uleb128	0
	.uleb128	2
	.uleb128	46
	.db	0
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	0
	.uleb128	0
	.uleb128	11
	.uleb128	33
	.db	0
	.uleb128	47
	.uleb128	11
	.uleb128	0
	.uleb128	0
	.uleb128	6
	.uleb128	36
	.db	0
	.uleb128	3
	.uleb128	8
	.uleb128	11
	.uleb128	11
	.uleb128	62
	.uleb128	11
	.uleb128	0
	.uleb128	0
	.uleb128	0

	.area .debug_info (NOLOAD)
	.dw	0,Ldebug_info_end-Ldebug_info_start
Ldebug_info_start:
	.dw	2
	.dw	0,(Ldebug_abbrev)
	.db	4
	.uleb128	1
	.ascii "drivers/src/stm8s_exti.c"
	.db	0
	.dw	0,(Ldebug_line_start+-4)
	.db	1
	.ascii "SDCC version 4.1.0 #12072"
	.db	0
	.uleb128	2
	.ascii "EXTI_DeInit"
	.db	0
	.dw	0,(_EXTI_DeInit)
	.dw	0,(XG$EXTI_DeInit$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+704)
	.uleb128	3
	.dw	0,186
	.ascii "EXTI_SetExtIntSensitivity"
	.db	0
	.dw	0,(_EXTI_SetExtIntSensitivity)
	.dw	0,(XG$EXTI_SetExtIntSensitivity$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+408)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "Port"
	.db	0
	.dw	0,186
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "SensitivityValue"
	.db	0
	.dw	0,186
	.uleb128	5
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$34)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$56)
	.uleb128	0
	.uleb128	6
	.ascii "unsigned char"
	.db	0
	.db	1
	.db	8
	.uleb128	3
	.dw	0,270
	.ascii "EXTI_SetTLISensitivity"
	.db	0
	.dw	0,(_EXTI_SetTLISensitivity)
	.dw	0,(XG$EXTI_SetTLISensitivity$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+304)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "SensitivityValue"
	.db	0
	.dw	0,186
	.uleb128	0
	.uleb128	7
	.dw	0,354
	.ascii "EXTI_GetExtIntSensitivity"
	.db	0
	.dw	0,(_EXTI_GetExtIntSensitivity)
	.dw	0,(XG$EXTI_GetExtIntSensitivity$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+20)
	.dw	0,186
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "Port"
	.db	0
	.dw	0,186
	.uleb128	5
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$104)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$119)
	.uleb128	8
	.db	1
	.db	80
	.ascii "value"
	.db	0
	.dw	0,186
	.uleb128	0
	.uleb128	7
	.dw	0,413
	.ascii "EXTI_GetTLISensitivity"
	.db	0
	.dw	0,(_EXTI_GetTLISensitivity)
	.dw	0,(XG$EXTI_GetTLISensitivity$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start)
	.dw	0,186
	.uleb128	8
	.db	1
	.db	80
	.ascii "value"
	.db	0
	.dw	0,186
	.uleb128	0
	.uleb128	9
	.dw	0,186
	.uleb128	10
	.dw	0,431
	.db	25
	.dw	0,413
	.uleb128	11
	.db	24
	.uleb128	0
	.uleb128	8
	.db	5
	.db	3
	.dw	0,(___str_0)
	.ascii "__str_0"
	.db	0
	.dw	0,418
	.uleb128	0
	.uleb128	0
	.uleb128	0
Ldebug_info_end:

	.area .debug_pubnames (NOLOAD)
	.dw	0,Ldebug_pubnames_end-Ldebug_pubnames_start
Ldebug_pubnames_start:
	.dw	2
	.dw	0,(Ldebug_info_start-4)
	.dw	0,4+Ldebug_info_end-Ldebug_info_start
	.dw	0,68
	.ascii "EXTI_DeInit"
	.db	0
	.dw	0,94
	.ascii "EXTI_SetExtIntSensitivity"
	.db	0
	.dw	0,203
	.ascii "EXTI_SetTLISensitivity"
	.db	0
	.dw	0,270
	.ascii "EXTI_GetExtIntSensitivity"
	.db	0
	.dw	0,354
	.ascii "EXTI_GetTLISensitivity"
	.db	0
	.dw	0,0
Ldebug_pubnames_end:

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE0_end-Ldebug_CIE0_start
Ldebug_CIE0_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE0_end:
	.dw	0,19
	.dw	0,(Ldebug_CIE0_start-4)
	.dw	0,(Sstm8s_exti$EXTI_GetTLISensitivity$126)	;initial loc
	.dw	0,Sstm8s_exti$EXTI_GetTLISensitivity$131-Sstm8s_exti$EXTI_GetTLISensitivity$126
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetTLISensitivity$126)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE1_end-Ldebug_CIE1_start
Ldebug_CIE1_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE1_end:
	.dw	0,173
	.dw	0,(Ldebug_CIE1_start-4)
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$78)	;initial loc
	.dw	0,Sstm8s_exti$EXTI_GetExtIntSensitivity$124-Sstm8s_exti$EXTI_GetExtIntSensitivity$78
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$78)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$81)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$82)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$83)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$84)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$85)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$86)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$87)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$88)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$89)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$90)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$91)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$92)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$93)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$94)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$95)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$96)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$97)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$98)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$99)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$100)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$102)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_GetExtIntSensitivity$103)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE2_end-Ldebug_CIE2_start
Ldebug_CIE2_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE2_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE2_start-4)
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$63)	;initial loc
	.dw	0,Sstm8s_exti$EXTI_SetTLISensitivity$76-Sstm8s_exti$EXTI_SetTLISensitivity$63
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$63)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$65)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$66)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$67)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$68)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$69)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$70)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetTLISensitivity$71)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE3_end-Ldebug_CIE3_start
Ldebug_CIE3_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE3_end:
	.dw	0,180
	.dw	0,(Ldebug_CIE3_start-4)
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$8)	;initial loc
	.dw	0,Sstm8s_exti$EXTI_SetExtIntSensitivity$61-Sstm8s_exti$EXTI_SetExtIntSensitivity$8
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$8)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$9)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$11)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$12)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$13)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$14)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$15)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$16)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$17)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$18)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$19)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$20)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$22)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$23)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$24)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$25)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$26)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$27)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$28)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$29)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$30)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$42)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$43)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_SetExtIntSensitivity$59)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE4_end-Ldebug_CIE4_start
Ldebug_CIE4_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE4_end:
	.dw	0,19
	.dw	0,(Ldebug_CIE4_start-4)
	.dw	0,(Sstm8s_exti$EXTI_DeInit$1)	;initial loc
	.dw	0,Sstm8s_exti$EXTI_DeInit$6-Sstm8s_exti$EXTI_DeInit$1
	.db	1
	.dw	0,(Sstm8s_exti$EXTI_DeInit$1)
	.db	14
	.uleb128	2
