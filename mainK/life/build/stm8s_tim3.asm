;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.1.0 #12072 (MINGW64)
;--------------------------------------------------------
	.module stm8s_tim3
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _assert_failed
	.globl _TIM3_DeInit
	.globl _TIM3_TimeBaseInit
	.globl _TIM3_OC1Init
	.globl _TIM3_OC2Init
	.globl _TIM3_ICInit
	.globl _TIM3_PWMIConfig
	.globl _TIM3_Cmd
	.globl _TIM3_ITConfig
	.globl _TIM3_UpdateDisableConfig
	.globl _TIM3_UpdateRequestConfig
	.globl _TIM3_SelectOnePulseMode
	.globl _TIM3_PrescalerConfig
	.globl _TIM3_ForcedOC1Config
	.globl _TIM3_ForcedOC2Config
	.globl _TIM3_ARRPreloadConfig
	.globl _TIM3_OC1PreloadConfig
	.globl _TIM3_OC2PreloadConfig
	.globl _TIM3_GenerateEvent
	.globl _TIM3_OC1PolarityConfig
	.globl _TIM3_OC2PolarityConfig
	.globl _TIM3_CCxCmd
	.globl _TIM3_SelectOCxM
	.globl _TIM3_SetCounter
	.globl _TIM3_SetAutoreload
	.globl _TIM3_SetCompare1
	.globl _TIM3_SetCompare2
	.globl _TIM3_SetIC1Prescaler
	.globl _TIM3_SetIC2Prescaler
	.globl _TIM3_GetCapture1
	.globl _TIM3_GetCapture2
	.globl _TIM3_GetCounter
	.globl _TIM3_GetPrescaler
	.globl _TIM3_GetFlagStatus
	.globl _TIM3_ClearFlag
	.globl _TIM3_GetITStatus
	.globl _TIM3_ClearITPendingBit
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)

; default segment ordering for linker
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area CONST
	.area INITIALIZER
	.area CODE

;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
	Sstm8s_tim3$TIM3_DeInit$0 ==.
;	drivers/src/stm8s_tim3.c: 51: void TIM3_DeInit(void)
;	-----------------------------------------
;	 function TIM3_DeInit
;	-----------------------------------------
_TIM3_DeInit:
	Sstm8s_tim3$TIM3_DeInit$1 ==.
	Sstm8s_tim3$TIM3_DeInit$2 ==.
;	drivers/src/stm8s_tim3.c: 53: TIM3->CR1 = (uint8_t)TIM3_CR1_RESET_VALUE;
	mov	0x5320+0, #0x00
	Sstm8s_tim3$TIM3_DeInit$3 ==.
;	drivers/src/stm8s_tim3.c: 54: TIM3->IER = (uint8_t)TIM3_IER_RESET_VALUE;
	mov	0x5321+0, #0x00
	Sstm8s_tim3$TIM3_DeInit$4 ==.
;	drivers/src/stm8s_tim3.c: 55: TIM3->SR2 = (uint8_t)TIM3_SR2_RESET_VALUE;
	mov	0x5323+0, #0x00
	Sstm8s_tim3$TIM3_DeInit$5 ==.
;	drivers/src/stm8s_tim3.c: 58: TIM3->CCER1 = (uint8_t)TIM3_CCER1_RESET_VALUE;
	mov	0x5327+0, #0x00
	Sstm8s_tim3$TIM3_DeInit$6 ==.
;	drivers/src/stm8s_tim3.c: 61: TIM3->CCER1 = (uint8_t)TIM3_CCER1_RESET_VALUE;
	mov	0x5327+0, #0x00
	Sstm8s_tim3$TIM3_DeInit$7 ==.
;	drivers/src/stm8s_tim3.c: 62: TIM3->CCMR1 = (uint8_t)TIM3_CCMR1_RESET_VALUE;
	mov	0x5325+0, #0x00
	Sstm8s_tim3$TIM3_DeInit$8 ==.
;	drivers/src/stm8s_tim3.c: 63: TIM3->CCMR2 = (uint8_t)TIM3_CCMR2_RESET_VALUE;
	mov	0x5326+0, #0x00
	Sstm8s_tim3$TIM3_DeInit$9 ==.
;	drivers/src/stm8s_tim3.c: 64: TIM3->CNTRH = (uint8_t)TIM3_CNTRH_RESET_VALUE;
	mov	0x5328+0, #0x00
	Sstm8s_tim3$TIM3_DeInit$10 ==.
;	drivers/src/stm8s_tim3.c: 65: TIM3->CNTRL = (uint8_t)TIM3_CNTRL_RESET_VALUE;
	mov	0x5329+0, #0x00
	Sstm8s_tim3$TIM3_DeInit$11 ==.
;	drivers/src/stm8s_tim3.c: 66: TIM3->PSCR = (uint8_t)TIM3_PSCR_RESET_VALUE;
	mov	0x532a+0, #0x00
	Sstm8s_tim3$TIM3_DeInit$12 ==.
;	drivers/src/stm8s_tim3.c: 67: TIM3->ARRH  = (uint8_t)TIM3_ARRH_RESET_VALUE;
	mov	0x532b+0, #0xff
	Sstm8s_tim3$TIM3_DeInit$13 ==.
;	drivers/src/stm8s_tim3.c: 68: TIM3->ARRL  = (uint8_t)TIM3_ARRL_RESET_VALUE;
	mov	0x532c+0, #0xff
	Sstm8s_tim3$TIM3_DeInit$14 ==.
;	drivers/src/stm8s_tim3.c: 69: TIM3->CCR1H = (uint8_t)TIM3_CCR1H_RESET_VALUE;
	mov	0x532d+0, #0x00
	Sstm8s_tim3$TIM3_DeInit$15 ==.
;	drivers/src/stm8s_tim3.c: 70: TIM3->CCR1L = (uint8_t)TIM3_CCR1L_RESET_VALUE;
	mov	0x532e+0, #0x00
	Sstm8s_tim3$TIM3_DeInit$16 ==.
;	drivers/src/stm8s_tim3.c: 71: TIM3->CCR2H = (uint8_t)TIM3_CCR2H_RESET_VALUE;
	mov	0x532f+0, #0x00
	Sstm8s_tim3$TIM3_DeInit$17 ==.
;	drivers/src/stm8s_tim3.c: 72: TIM3->CCR2L = (uint8_t)TIM3_CCR2L_RESET_VALUE;
	mov	0x5330+0, #0x00
	Sstm8s_tim3$TIM3_DeInit$18 ==.
;	drivers/src/stm8s_tim3.c: 73: TIM3->SR1 = (uint8_t)TIM3_SR1_RESET_VALUE;
	mov	0x5322+0, #0x00
	Sstm8s_tim3$TIM3_DeInit$19 ==.
;	drivers/src/stm8s_tim3.c: 74: }
	Sstm8s_tim3$TIM3_DeInit$20 ==.
	XG$TIM3_DeInit$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_DeInit$21 ==.
	Sstm8s_tim3$TIM3_TimeBaseInit$22 ==.
;	drivers/src/stm8s_tim3.c: 82: void TIM3_TimeBaseInit( TIM3_Prescaler_TypeDef TIM3_Prescaler,
;	-----------------------------------------
;	 function TIM3_TimeBaseInit
;	-----------------------------------------
_TIM3_TimeBaseInit:
	Sstm8s_tim3$TIM3_TimeBaseInit$23 ==.
	Sstm8s_tim3$TIM3_TimeBaseInit$24 ==.
;	drivers/src/stm8s_tim3.c: 86: TIM3->PSCR = (uint8_t)(TIM3_Prescaler);
	ldw	x, #0x532a
	ld	a, (0x03, sp)
	ld	(x), a
	Sstm8s_tim3$TIM3_TimeBaseInit$25 ==.
;	drivers/src/stm8s_tim3.c: 88: TIM3->ARRH = (uint8_t)(TIM3_Period >> 8);
	ld	a, (0x04, sp)
	ld	0x532b, a
	Sstm8s_tim3$TIM3_TimeBaseInit$26 ==.
;	drivers/src/stm8s_tim3.c: 89: TIM3->ARRL = (uint8_t)(TIM3_Period);
	ld	a, (0x05, sp)
	ld	0x532c, a
	Sstm8s_tim3$TIM3_TimeBaseInit$27 ==.
;	drivers/src/stm8s_tim3.c: 90: }
	Sstm8s_tim3$TIM3_TimeBaseInit$28 ==.
	XG$TIM3_TimeBaseInit$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_TimeBaseInit$29 ==.
	Sstm8s_tim3$TIM3_OC1Init$30 ==.
;	drivers/src/stm8s_tim3.c: 100: void TIM3_OC1Init(TIM3_OCMode_TypeDef TIM3_OCMode,
;	-----------------------------------------
;	 function TIM3_OC1Init
;	-----------------------------------------
_TIM3_OC1Init:
	Sstm8s_tim3$TIM3_OC1Init$31 ==.
	pushw	x
	Sstm8s_tim3$TIM3_OC1Init$32 ==.
	Sstm8s_tim3$TIM3_OC1Init$33 ==.
;	drivers/src/stm8s_tim3.c: 106: assert_param(IS_TIM3_OC_MODE_OK(TIM3_OCMode));
	tnz	(0x05, sp)
	jreq	00104$
	ld	a, (0x05, sp)
	cp	a, #0x10
	jreq	00104$
	Sstm8s_tim3$TIM3_OC1Init$34 ==.
	ld	a, (0x05, sp)
	cp	a, #0x20
	jreq	00104$
	Sstm8s_tim3$TIM3_OC1Init$35 ==.
	ld	a, (0x05, sp)
	cp	a, #0x30
	jreq	00104$
	Sstm8s_tim3$TIM3_OC1Init$36 ==.
	ld	a, (0x05, sp)
	cp	a, #0x60
	jreq	00104$
	Sstm8s_tim3$TIM3_OC1Init$37 ==.
	ld	a, (0x05, sp)
	cp	a, #0x70
	jreq	00104$
	Sstm8s_tim3$TIM3_OC1Init$38 ==.
	push	#0x6a
	Sstm8s_tim3$TIM3_OC1Init$39 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_OC1Init$40 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_OC1Init$41 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_OC1Init$42 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_OC1Init$43 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_OC1Init$44 ==.
00104$:
	Sstm8s_tim3$TIM3_OC1Init$45 ==.
;	drivers/src/stm8s_tim3.c: 107: assert_param(IS_TIM3_OUTPUT_STATE_OK(TIM3_OutputState));
	tnz	(0x06, sp)
	jreq	00121$
	ld	a, (0x06, sp)
	cp	a, #0x11
	jreq	00121$
	Sstm8s_tim3$TIM3_OC1Init$46 ==.
	push	#0x6b
	Sstm8s_tim3$TIM3_OC1Init$47 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_OC1Init$48 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_OC1Init$49 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_OC1Init$50 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_OC1Init$51 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_OC1Init$52 ==.
00121$:
	Sstm8s_tim3$TIM3_OC1Init$53 ==.
;	drivers/src/stm8s_tim3.c: 108: assert_param(IS_TIM3_OC_POLARITY_OK(TIM3_OCPolarity));
	tnz	(0x09, sp)
	jreq	00126$
	ld	a, (0x09, sp)
	cp	a, #0x22
	jreq	00126$
	Sstm8s_tim3$TIM3_OC1Init$54 ==.
	push	#0x6c
	Sstm8s_tim3$TIM3_OC1Init$55 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_OC1Init$56 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_OC1Init$57 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_OC1Init$58 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_OC1Init$59 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_OC1Init$60 ==.
00126$:
	Sstm8s_tim3$TIM3_OC1Init$61 ==.
;	drivers/src/stm8s_tim3.c: 111: TIM3->CCER1 &= (uint8_t)(~( TIM3_CCER1_CC1E | TIM3_CCER1_CC1P));
	ld	a, 0x5327
	and	a, #0xfc
	ld	0x5327, a
	Sstm8s_tim3$TIM3_OC1Init$62 ==.
;	drivers/src/stm8s_tim3.c: 113: TIM3->CCER1 |= (uint8_t)((uint8_t)(TIM3_OutputState  & TIM3_CCER1_CC1E   ) | (uint8_t)(TIM3_OCPolarity   & TIM3_CCER1_CC1P   ));
	ld	a, 0x5327
	ld	(0x01, sp), a
	ld	a, (0x06, sp)
	and	a, #0x01
	ld	(0x02, sp), a
	ld	a, (0x09, sp)
	and	a, #0x02
	or	a, (0x02, sp)
	or	a, (0x01, sp)
	ld	0x5327, a
	Sstm8s_tim3$TIM3_OC1Init$63 ==.
;	drivers/src/stm8s_tim3.c: 116: TIM3->CCMR1 = (uint8_t)((uint8_t)(TIM3->CCMR1 & (uint8_t)(~TIM3_CCMR_OCM)) | (uint8_t)TIM3_OCMode);
	ld	a, 0x5325
	and	a, #0x8f
	or	a, (0x05, sp)
	ld	0x5325, a
	Sstm8s_tim3$TIM3_OC1Init$64 ==.
;	drivers/src/stm8s_tim3.c: 119: TIM3->CCR1H = (uint8_t)(TIM3_Pulse >> 8);
	ld	a, (0x07, sp)
	ld	0x532d, a
	Sstm8s_tim3$TIM3_OC1Init$65 ==.
;	drivers/src/stm8s_tim3.c: 120: TIM3->CCR1L = (uint8_t)(TIM3_Pulse);
	ld	a, (0x08, sp)
	ld	0x532e, a
	Sstm8s_tim3$TIM3_OC1Init$66 ==.
;	drivers/src/stm8s_tim3.c: 121: }
	popw	x
	Sstm8s_tim3$TIM3_OC1Init$67 ==.
	Sstm8s_tim3$TIM3_OC1Init$68 ==.
	XG$TIM3_OC1Init$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_OC1Init$69 ==.
	Sstm8s_tim3$TIM3_OC2Init$70 ==.
;	drivers/src/stm8s_tim3.c: 131: void TIM3_OC2Init(TIM3_OCMode_TypeDef TIM3_OCMode,
;	-----------------------------------------
;	 function TIM3_OC2Init
;	-----------------------------------------
_TIM3_OC2Init:
	Sstm8s_tim3$TIM3_OC2Init$71 ==.
	pushw	x
	Sstm8s_tim3$TIM3_OC2Init$72 ==.
	Sstm8s_tim3$TIM3_OC2Init$73 ==.
;	drivers/src/stm8s_tim3.c: 137: assert_param(IS_TIM3_OC_MODE_OK(TIM3_OCMode));
	tnz	(0x05, sp)
	jreq	00104$
	ld	a, (0x05, sp)
	cp	a, #0x10
	jreq	00104$
	Sstm8s_tim3$TIM3_OC2Init$74 ==.
	ld	a, (0x05, sp)
	cp	a, #0x20
	jreq	00104$
	Sstm8s_tim3$TIM3_OC2Init$75 ==.
	ld	a, (0x05, sp)
	cp	a, #0x30
	jreq	00104$
	Sstm8s_tim3$TIM3_OC2Init$76 ==.
	ld	a, (0x05, sp)
	cp	a, #0x60
	jreq	00104$
	Sstm8s_tim3$TIM3_OC2Init$77 ==.
	ld	a, (0x05, sp)
	cp	a, #0x70
	jreq	00104$
	Sstm8s_tim3$TIM3_OC2Init$78 ==.
	push	#0x89
	Sstm8s_tim3$TIM3_OC2Init$79 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_OC2Init$80 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_OC2Init$81 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_OC2Init$82 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_OC2Init$83 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_OC2Init$84 ==.
00104$:
	Sstm8s_tim3$TIM3_OC2Init$85 ==.
;	drivers/src/stm8s_tim3.c: 138: assert_param(IS_TIM3_OUTPUT_STATE_OK(TIM3_OutputState));
	tnz	(0x06, sp)
	jreq	00121$
	ld	a, (0x06, sp)
	cp	a, #0x11
	jreq	00121$
	Sstm8s_tim3$TIM3_OC2Init$86 ==.
	push	#0x8a
	Sstm8s_tim3$TIM3_OC2Init$87 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_OC2Init$88 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_OC2Init$89 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_OC2Init$90 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_OC2Init$91 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_OC2Init$92 ==.
00121$:
	Sstm8s_tim3$TIM3_OC2Init$93 ==.
;	drivers/src/stm8s_tim3.c: 139: assert_param(IS_TIM3_OC_POLARITY_OK(TIM3_OCPolarity));
	tnz	(0x09, sp)
	jreq	00126$
	ld	a, (0x09, sp)
	cp	a, #0x22
	jreq	00126$
	Sstm8s_tim3$TIM3_OC2Init$94 ==.
	push	#0x8b
	Sstm8s_tim3$TIM3_OC2Init$95 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_OC2Init$96 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_OC2Init$97 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_OC2Init$98 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_OC2Init$99 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_OC2Init$100 ==.
00126$:
	Sstm8s_tim3$TIM3_OC2Init$101 ==.
;	drivers/src/stm8s_tim3.c: 143: TIM3->CCER1 &= (uint8_t)(~( TIM3_CCER1_CC2E |  TIM3_CCER1_CC2P ));
	ld	a, 0x5327
	and	a, #0xcf
	ld	0x5327, a
	Sstm8s_tim3$TIM3_OC2Init$102 ==.
;	drivers/src/stm8s_tim3.c: 145: TIM3->CCER1 |= (uint8_t)((uint8_t)(TIM3_OutputState  & TIM3_CCER1_CC2E   ) | (uint8_t)(TIM3_OCPolarity   & TIM3_CCER1_CC2P ));
	ld	a, 0x5327
	ld	(0x01, sp), a
	ld	a, (0x06, sp)
	and	a, #0x10
	ld	(0x02, sp), a
	ld	a, (0x09, sp)
	and	a, #0x20
	or	a, (0x02, sp)
	or	a, (0x01, sp)
	ld	0x5327, a
	Sstm8s_tim3$TIM3_OC2Init$103 ==.
;	drivers/src/stm8s_tim3.c: 149: TIM3->CCMR2 = (uint8_t)((uint8_t)(TIM3->CCMR2 & (uint8_t)(~TIM3_CCMR_OCM)) | (uint8_t)TIM3_OCMode);
	ld	a, 0x5326
	and	a, #0x8f
	or	a, (0x05, sp)
	ld	0x5326, a
	Sstm8s_tim3$TIM3_OC2Init$104 ==.
;	drivers/src/stm8s_tim3.c: 153: TIM3->CCR2H = (uint8_t)(TIM3_Pulse >> 8);
	ld	a, (0x07, sp)
	ld	0x532f, a
	Sstm8s_tim3$TIM3_OC2Init$105 ==.
;	drivers/src/stm8s_tim3.c: 154: TIM3->CCR2L = (uint8_t)(TIM3_Pulse);
	ld	a, (0x08, sp)
	ld	0x5330, a
	Sstm8s_tim3$TIM3_OC2Init$106 ==.
;	drivers/src/stm8s_tim3.c: 155: }
	popw	x
	Sstm8s_tim3$TIM3_OC2Init$107 ==.
	Sstm8s_tim3$TIM3_OC2Init$108 ==.
	XG$TIM3_OC2Init$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_OC2Init$109 ==.
	Sstm8s_tim3$TIM3_ICInit$110 ==.
;	drivers/src/stm8s_tim3.c: 166: void TIM3_ICInit(TIM3_Channel_TypeDef TIM3_Channel,
;	-----------------------------------------
;	 function TIM3_ICInit
;	-----------------------------------------
_TIM3_ICInit:
	Sstm8s_tim3$TIM3_ICInit$111 ==.
	push	a
	Sstm8s_tim3$TIM3_ICInit$112 ==.
	Sstm8s_tim3$TIM3_ICInit$113 ==.
;	drivers/src/stm8s_tim3.c: 173: assert_param(IS_TIM3_CHANNEL_OK(TIM3_Channel));
	ld	a, (0x04, sp)
	dec	a
	jrne	00203$
	ld	a, #0x01
	ld	(0x01, sp), a
	.byte 0xc5
00203$:
	clr	(0x01, sp)
00204$:
	Sstm8s_tim3$TIM3_ICInit$114 ==.
	tnz	(0x04, sp)
	jreq	00107$
	tnz	(0x01, sp)
	jrne	00107$
	push	#0xad
	Sstm8s_tim3$TIM3_ICInit$115 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_ICInit$116 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_ICInit$117 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_ICInit$118 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_ICInit$119 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_ICInit$120 ==.
00107$:
	Sstm8s_tim3$TIM3_ICInit$121 ==.
;	drivers/src/stm8s_tim3.c: 174: assert_param(IS_TIM3_IC_POLARITY_OK(TIM3_ICPolarity));
	tnz	(0x05, sp)
	jreq	00112$
	ld	a, (0x05, sp)
	cp	a, #0x44
	jreq	00112$
	Sstm8s_tim3$TIM3_ICInit$122 ==.
	push	#0xae
	Sstm8s_tim3$TIM3_ICInit$123 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_ICInit$124 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_ICInit$125 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_ICInit$126 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_ICInit$127 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_ICInit$128 ==.
00112$:
	Sstm8s_tim3$TIM3_ICInit$129 ==.
;	drivers/src/stm8s_tim3.c: 175: assert_param(IS_TIM3_IC_SELECTION_OK(TIM3_ICSelection));
	ld	a, (0x06, sp)
	dec	a
	jreq	00117$
	Sstm8s_tim3$TIM3_ICInit$130 ==.
	ld	a, (0x06, sp)
	cp	a, #0x02
	jreq	00117$
	Sstm8s_tim3$TIM3_ICInit$131 ==.
	ld	a, (0x06, sp)
	cp	a, #0x03
	jreq	00117$
	Sstm8s_tim3$TIM3_ICInit$132 ==.
	push	#0xaf
	Sstm8s_tim3$TIM3_ICInit$133 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_ICInit$134 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_ICInit$135 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_ICInit$136 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_ICInit$137 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_ICInit$138 ==.
00117$:
	Sstm8s_tim3$TIM3_ICInit$139 ==.
;	drivers/src/stm8s_tim3.c: 176: assert_param(IS_TIM3_IC_PRESCALER_OK(TIM3_ICPrescaler));
	tnz	(0x07, sp)
	jreq	00125$
	ld	a, (0x07, sp)
	cp	a, #0x04
	jreq	00125$
	Sstm8s_tim3$TIM3_ICInit$140 ==.
	ld	a, (0x07, sp)
	cp	a, #0x08
	jreq	00125$
	Sstm8s_tim3$TIM3_ICInit$141 ==.
	ld	a, (0x07, sp)
	cp	a, #0x0c
	jreq	00125$
	Sstm8s_tim3$TIM3_ICInit$142 ==.
	push	#0xb0
	Sstm8s_tim3$TIM3_ICInit$143 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_ICInit$144 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_ICInit$145 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_ICInit$146 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_ICInit$147 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_ICInit$148 ==.
00125$:
	Sstm8s_tim3$TIM3_ICInit$149 ==.
;	drivers/src/stm8s_tim3.c: 177: assert_param(IS_TIM3_IC_FILTER_OK(TIM3_ICFilter));
	ld	a, (0x08, sp)
	cp	a, #0x0f
	jrule	00136$
	push	#0xb1
	Sstm8s_tim3$TIM3_ICInit$150 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_ICInit$151 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_ICInit$152 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_ICInit$153 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_ICInit$154 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_ICInit$155 ==.
00136$:
	Sstm8s_tim3$TIM3_ICInit$156 ==.
;	drivers/src/stm8s_tim3.c: 179: if (TIM3_Channel != TIM3_CHANNEL_2)
	tnz	(0x01, sp)
	jrne	00102$
	Sstm8s_tim3$TIM3_ICInit$157 ==.
	Sstm8s_tim3$TIM3_ICInit$158 ==.
;	drivers/src/stm8s_tim3.c: 182: TI1_Config((uint8_t)TIM3_ICPolarity,
	ld	a, (0x08, sp)
	push	a
	Sstm8s_tim3$TIM3_ICInit$159 ==.
	ld	a, (0x07, sp)
	push	a
	Sstm8s_tim3$TIM3_ICInit$160 ==.
	ld	a, (0x07, sp)
	push	a
	Sstm8s_tim3$TIM3_ICInit$161 ==.
	call	_TI1_Config
	addw	sp, #3
	Sstm8s_tim3$TIM3_ICInit$162 ==.
	Sstm8s_tim3$TIM3_ICInit$163 ==.
;	drivers/src/stm8s_tim3.c: 187: TIM3_SetIC1Prescaler(TIM3_ICPrescaler);
	ld	a, (0x07, sp)
	push	a
	Sstm8s_tim3$TIM3_ICInit$164 ==.
	call	_TIM3_SetIC1Prescaler
	pop	a
	Sstm8s_tim3$TIM3_ICInit$165 ==.
	Sstm8s_tim3$TIM3_ICInit$166 ==.
	jra	00104$
00102$:
	Sstm8s_tim3$TIM3_ICInit$167 ==.
	Sstm8s_tim3$TIM3_ICInit$168 ==.
;	drivers/src/stm8s_tim3.c: 192: TI2_Config((uint8_t)TIM3_ICPolarity,
	ld	a, (0x08, sp)
	push	a
	Sstm8s_tim3$TIM3_ICInit$169 ==.
	ld	a, (0x07, sp)
	push	a
	Sstm8s_tim3$TIM3_ICInit$170 ==.
	ld	a, (0x07, sp)
	push	a
	Sstm8s_tim3$TIM3_ICInit$171 ==.
	call	_TI2_Config
	addw	sp, #3
	Sstm8s_tim3$TIM3_ICInit$172 ==.
	Sstm8s_tim3$TIM3_ICInit$173 ==.
;	drivers/src/stm8s_tim3.c: 197: TIM3_SetIC2Prescaler(TIM3_ICPrescaler);
	ld	a, (0x07, sp)
	push	a
	Sstm8s_tim3$TIM3_ICInit$174 ==.
	call	_TIM3_SetIC2Prescaler
	pop	a
	Sstm8s_tim3$TIM3_ICInit$175 ==.
	Sstm8s_tim3$TIM3_ICInit$176 ==.
00104$:
	Sstm8s_tim3$TIM3_ICInit$177 ==.
;	drivers/src/stm8s_tim3.c: 199: }
	pop	a
	Sstm8s_tim3$TIM3_ICInit$178 ==.
	Sstm8s_tim3$TIM3_ICInit$179 ==.
	XG$TIM3_ICInit$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_ICInit$180 ==.
	Sstm8s_tim3$TIM3_PWMIConfig$181 ==.
;	drivers/src/stm8s_tim3.c: 210: void TIM3_PWMIConfig(TIM3_Channel_TypeDef TIM3_Channel,
;	-----------------------------------------
;	 function TIM3_PWMIConfig
;	-----------------------------------------
_TIM3_PWMIConfig:
	Sstm8s_tim3$TIM3_PWMIConfig$182 ==.
	sub	sp, #3
	Sstm8s_tim3$TIM3_PWMIConfig$183 ==.
	Sstm8s_tim3$TIM3_PWMIConfig$184 ==.
;	drivers/src/stm8s_tim3.c: 220: assert_param(IS_TIM3_PWMI_CHANNEL_OK(TIM3_Channel));
	ld	a, (0x06, sp)
	dec	a
	jrne	00212$
	ld	a, #0x01
	ld	(0x01, sp), a
	.byte 0xc5
00212$:
	clr	(0x01, sp)
00213$:
	Sstm8s_tim3$TIM3_PWMIConfig$185 ==.
	tnz	(0x06, sp)
	jreq	00113$
	tnz	(0x01, sp)
	jrne	00113$
	push	#0xdc
	Sstm8s_tim3$TIM3_PWMIConfig$186 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_PWMIConfig$187 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_PWMIConfig$188 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_PWMIConfig$189 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_PWMIConfig$190 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_PWMIConfig$191 ==.
00113$:
	Sstm8s_tim3$TIM3_PWMIConfig$192 ==.
;	drivers/src/stm8s_tim3.c: 221: assert_param(IS_TIM3_IC_POLARITY_OK(TIM3_ICPolarity));
	ld	a, (0x07, sp)
	sub	a, #0x44
	jrne	00217$
	inc	a
	ld	(0x02, sp), a
	.byte 0xc5
00217$:
	clr	(0x02, sp)
00218$:
	Sstm8s_tim3$TIM3_PWMIConfig$193 ==.
	tnz	(0x07, sp)
	jreq	00118$
	tnz	(0x02, sp)
	jrne	00118$
	push	#0xdd
	Sstm8s_tim3$TIM3_PWMIConfig$194 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_PWMIConfig$195 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_PWMIConfig$196 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_PWMIConfig$197 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_PWMIConfig$198 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_PWMIConfig$199 ==.
00118$:
	Sstm8s_tim3$TIM3_PWMIConfig$200 ==.
;	drivers/src/stm8s_tim3.c: 222: assert_param(IS_TIM3_IC_SELECTION_OK(TIM3_ICSelection));
	ld	a, (0x08, sp)
	dec	a
	jrne	00222$
	ld	a, #0x01
	ld	(0x03, sp), a
	.byte 0xc5
00222$:
	clr	(0x03, sp)
00223$:
	Sstm8s_tim3$TIM3_PWMIConfig$201 ==.
	tnz	(0x03, sp)
	jrne	00123$
	ld	a, (0x08, sp)
	cp	a, #0x02
	jreq	00123$
	Sstm8s_tim3$TIM3_PWMIConfig$202 ==.
	ld	a, (0x08, sp)
	cp	a, #0x03
	jreq	00123$
	Sstm8s_tim3$TIM3_PWMIConfig$203 ==.
	push	#0xde
	Sstm8s_tim3$TIM3_PWMIConfig$204 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_PWMIConfig$205 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_PWMIConfig$206 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_PWMIConfig$207 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_PWMIConfig$208 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_PWMIConfig$209 ==.
00123$:
	Sstm8s_tim3$TIM3_PWMIConfig$210 ==.
;	drivers/src/stm8s_tim3.c: 223: assert_param(IS_TIM3_IC_PRESCALER_OK(TIM3_ICPrescaler));
	tnz	(0x09, sp)
	jreq	00131$
	ld	a, (0x09, sp)
	cp	a, #0x04
	jreq	00131$
	Sstm8s_tim3$TIM3_PWMIConfig$211 ==.
	ld	a, (0x09, sp)
	cp	a, #0x08
	jreq	00131$
	Sstm8s_tim3$TIM3_PWMIConfig$212 ==.
	ld	a, (0x09, sp)
	cp	a, #0x0c
	jreq	00131$
	Sstm8s_tim3$TIM3_PWMIConfig$213 ==.
	push	#0xdf
	Sstm8s_tim3$TIM3_PWMIConfig$214 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_PWMIConfig$215 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_PWMIConfig$216 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_PWMIConfig$217 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_PWMIConfig$218 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_PWMIConfig$219 ==.
00131$:
	Sstm8s_tim3$TIM3_PWMIConfig$220 ==.
;	drivers/src/stm8s_tim3.c: 226: if (TIM3_ICPolarity != TIM3_ICPOLARITY_FALLING)
	tnz	(0x02, sp)
	jrne	00102$
	Sstm8s_tim3$TIM3_PWMIConfig$221 ==.
	Sstm8s_tim3$TIM3_PWMIConfig$222 ==.
;	drivers/src/stm8s_tim3.c: 228: icpolarity = (uint8_t)TIM3_ICPOLARITY_FALLING;
	ld	a, #0x44
	ld	(0x02, sp), a
	Sstm8s_tim3$TIM3_PWMIConfig$223 ==.
	jra	00103$
00102$:
	Sstm8s_tim3$TIM3_PWMIConfig$224 ==.
	Sstm8s_tim3$TIM3_PWMIConfig$225 ==.
;	drivers/src/stm8s_tim3.c: 232: icpolarity = (uint8_t)TIM3_ICPOLARITY_RISING;
	clr	(0x02, sp)
	Sstm8s_tim3$TIM3_PWMIConfig$226 ==.
00103$:
	Sstm8s_tim3$TIM3_PWMIConfig$227 ==.
;	drivers/src/stm8s_tim3.c: 236: if (TIM3_ICSelection == TIM3_ICSELECTION_DIRECTTI)
	ld	a, (0x03, sp)
	jreq	00105$
	Sstm8s_tim3$TIM3_PWMIConfig$228 ==.
	Sstm8s_tim3$TIM3_PWMIConfig$229 ==.
;	drivers/src/stm8s_tim3.c: 238: icselection = (uint8_t)TIM3_ICSELECTION_INDIRECTTI;
	ld	a, #0x02
	ld	(0x03, sp), a
	Sstm8s_tim3$TIM3_PWMIConfig$230 ==.
	jra	00106$
00105$:
	Sstm8s_tim3$TIM3_PWMIConfig$231 ==.
	Sstm8s_tim3$TIM3_PWMIConfig$232 ==.
;	drivers/src/stm8s_tim3.c: 242: icselection = (uint8_t)TIM3_ICSELECTION_DIRECTTI;
	ld	a, #0x01
	ld	(0x03, sp), a
	Sstm8s_tim3$TIM3_PWMIConfig$233 ==.
00106$:
	Sstm8s_tim3$TIM3_PWMIConfig$234 ==.
;	drivers/src/stm8s_tim3.c: 245: if (TIM3_Channel != TIM3_CHANNEL_2)
	tnz	(0x01, sp)
	jreq	00243$
	jp	00108$
00243$:
	Sstm8s_tim3$TIM3_PWMIConfig$235 ==.
	Sstm8s_tim3$TIM3_PWMIConfig$236 ==.
;	drivers/src/stm8s_tim3.c: 248: TI1_Config((uint8_t)TIM3_ICPolarity, (uint8_t)TIM3_ICSelection,
	ld	a, (0x0a, sp)
	push	a
	Sstm8s_tim3$TIM3_PWMIConfig$237 ==.
	ld	a, (0x09, sp)
	push	a
	Sstm8s_tim3$TIM3_PWMIConfig$238 ==.
	ld	a, (0x09, sp)
	push	a
	Sstm8s_tim3$TIM3_PWMIConfig$239 ==.
	call	_TI1_Config
	addw	sp, #3
	Sstm8s_tim3$TIM3_PWMIConfig$240 ==.
	Sstm8s_tim3$TIM3_PWMIConfig$241 ==.
;	drivers/src/stm8s_tim3.c: 252: TIM3_SetIC1Prescaler(TIM3_ICPrescaler);
	ld	a, (0x09, sp)
	push	a
	Sstm8s_tim3$TIM3_PWMIConfig$242 ==.
	call	_TIM3_SetIC1Prescaler
	pop	a
	Sstm8s_tim3$TIM3_PWMIConfig$243 ==.
	Sstm8s_tim3$TIM3_PWMIConfig$244 ==.
;	drivers/src/stm8s_tim3.c: 255: TI2_Config(icpolarity, icselection, TIM3_ICFilter);
	ld	a, (0x0a, sp)
	push	a
	Sstm8s_tim3$TIM3_PWMIConfig$245 ==.
	ld	a, (0x04, sp)
	push	a
	Sstm8s_tim3$TIM3_PWMIConfig$246 ==.
	ld	a, (0x04, sp)
	push	a
	Sstm8s_tim3$TIM3_PWMIConfig$247 ==.
	call	_TI2_Config
	addw	sp, #3
	Sstm8s_tim3$TIM3_PWMIConfig$248 ==.
	Sstm8s_tim3$TIM3_PWMIConfig$249 ==.
;	drivers/src/stm8s_tim3.c: 258: TIM3_SetIC2Prescaler(TIM3_ICPrescaler);
	ld	a, (0x09, sp)
	push	a
	Sstm8s_tim3$TIM3_PWMIConfig$250 ==.
	call	_TIM3_SetIC2Prescaler
	pop	a
	Sstm8s_tim3$TIM3_PWMIConfig$251 ==.
	Sstm8s_tim3$TIM3_PWMIConfig$252 ==.
	jp	00110$
00108$:
	Sstm8s_tim3$TIM3_PWMIConfig$253 ==.
	Sstm8s_tim3$TIM3_PWMIConfig$254 ==.
;	drivers/src/stm8s_tim3.c: 263: TI2_Config((uint8_t)TIM3_ICPolarity, (uint8_t)TIM3_ICSelection,
	ld	a, (0x0a, sp)
	push	a
	Sstm8s_tim3$TIM3_PWMIConfig$255 ==.
	ld	a, (0x09, sp)
	push	a
	Sstm8s_tim3$TIM3_PWMIConfig$256 ==.
	ld	a, (0x09, sp)
	push	a
	Sstm8s_tim3$TIM3_PWMIConfig$257 ==.
	call	_TI2_Config
	addw	sp, #3
	Sstm8s_tim3$TIM3_PWMIConfig$258 ==.
	Sstm8s_tim3$TIM3_PWMIConfig$259 ==.
;	drivers/src/stm8s_tim3.c: 267: TIM3_SetIC2Prescaler(TIM3_ICPrescaler);
	ld	a, (0x09, sp)
	push	a
	Sstm8s_tim3$TIM3_PWMIConfig$260 ==.
	call	_TIM3_SetIC2Prescaler
	pop	a
	Sstm8s_tim3$TIM3_PWMIConfig$261 ==.
	Sstm8s_tim3$TIM3_PWMIConfig$262 ==.
;	drivers/src/stm8s_tim3.c: 270: TI1_Config(icpolarity, icselection, TIM3_ICFilter);
	ld	a, (0x0a, sp)
	push	a
	Sstm8s_tim3$TIM3_PWMIConfig$263 ==.
	ld	a, (0x04, sp)
	push	a
	Sstm8s_tim3$TIM3_PWMIConfig$264 ==.
	ld	a, (0x04, sp)
	push	a
	Sstm8s_tim3$TIM3_PWMIConfig$265 ==.
	call	_TI1_Config
	addw	sp, #3
	Sstm8s_tim3$TIM3_PWMIConfig$266 ==.
	Sstm8s_tim3$TIM3_PWMIConfig$267 ==.
;	drivers/src/stm8s_tim3.c: 273: TIM3_SetIC1Prescaler(TIM3_ICPrescaler);
	ld	a, (0x09, sp)
	push	a
	Sstm8s_tim3$TIM3_PWMIConfig$268 ==.
	call	_TIM3_SetIC1Prescaler
	pop	a
	Sstm8s_tim3$TIM3_PWMIConfig$269 ==.
	Sstm8s_tim3$TIM3_PWMIConfig$270 ==.
00110$:
	Sstm8s_tim3$TIM3_PWMIConfig$271 ==.
;	drivers/src/stm8s_tim3.c: 275: }
	addw	sp, #3
	Sstm8s_tim3$TIM3_PWMIConfig$272 ==.
	Sstm8s_tim3$TIM3_PWMIConfig$273 ==.
	XG$TIM3_PWMIConfig$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_PWMIConfig$274 ==.
	Sstm8s_tim3$TIM3_Cmd$275 ==.
;	drivers/src/stm8s_tim3.c: 283: void TIM3_Cmd(FunctionalState NewState)
;	-----------------------------------------
;	 function TIM3_Cmd
;	-----------------------------------------
_TIM3_Cmd:
	Sstm8s_tim3$TIM3_Cmd$276 ==.
	Sstm8s_tim3$TIM3_Cmd$277 ==.
;	drivers/src/stm8s_tim3.c: 286: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	dec	a
	jreq	00107$
	Sstm8s_tim3$TIM3_Cmd$278 ==.
	push	#0x1e
	Sstm8s_tim3$TIM3_Cmd$279 ==.
	push	#0x01
	Sstm8s_tim3$TIM3_Cmd$280 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_Cmd$281 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_Cmd$282 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_Cmd$283 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_Cmd$284 ==.
00107$:
	Sstm8s_tim3$TIM3_Cmd$285 ==.
;	drivers/src/stm8s_tim3.c: 291: TIM3->CR1 |= (uint8_t)TIM3_CR1_CEN;
	ld	a, 0x5320
	Sstm8s_tim3$TIM3_Cmd$286 ==.
;	drivers/src/stm8s_tim3.c: 289: if (NewState != DISABLE)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim3$TIM3_Cmd$287 ==.
	Sstm8s_tim3$TIM3_Cmd$288 ==.
;	drivers/src/stm8s_tim3.c: 291: TIM3->CR1 |= (uint8_t)TIM3_CR1_CEN;
	or	a, #0x01
	ld	0x5320, a
	Sstm8s_tim3$TIM3_Cmd$289 ==.
	jra	00104$
00102$:
	Sstm8s_tim3$TIM3_Cmd$290 ==.
	Sstm8s_tim3$TIM3_Cmd$291 ==.
;	drivers/src/stm8s_tim3.c: 295: TIM3->CR1 &= (uint8_t)(~TIM3_CR1_CEN);
	and	a, #0xfe
	ld	0x5320, a
	Sstm8s_tim3$TIM3_Cmd$292 ==.
00104$:
	Sstm8s_tim3$TIM3_Cmd$293 ==.
;	drivers/src/stm8s_tim3.c: 297: }
	Sstm8s_tim3$TIM3_Cmd$294 ==.
	XG$TIM3_Cmd$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_Cmd$295 ==.
	Sstm8s_tim3$TIM3_ITConfig$296 ==.
;	drivers/src/stm8s_tim3.c: 311: void TIM3_ITConfig(TIM3_IT_TypeDef TIM3_IT, FunctionalState NewState)
;	-----------------------------------------
;	 function TIM3_ITConfig
;	-----------------------------------------
_TIM3_ITConfig:
	Sstm8s_tim3$TIM3_ITConfig$297 ==.
	push	a
	Sstm8s_tim3$TIM3_ITConfig$298 ==.
	Sstm8s_tim3$TIM3_ITConfig$299 ==.
;	drivers/src/stm8s_tim3.c: 314: assert_param(IS_TIM3_IT_OK(TIM3_IT));
	tnz	(0x04, sp)
	jreq	00106$
	ld	a, (0x04, sp)
	cp	a, #0x07
	jrule	00107$
00106$:
	push	#0x3a
	Sstm8s_tim3$TIM3_ITConfig$300 ==.
	push	#0x01
	Sstm8s_tim3$TIM3_ITConfig$301 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_ITConfig$302 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_ITConfig$303 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_ITConfig$304 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_ITConfig$305 ==.
00107$:
	Sstm8s_tim3$TIM3_ITConfig$306 ==.
;	drivers/src/stm8s_tim3.c: 315: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
	tnz	(0x05, sp)
	jreq	00112$
	ld	a, (0x05, sp)
	dec	a
	jreq	00112$
	Sstm8s_tim3$TIM3_ITConfig$307 ==.
	push	#0x3b
	Sstm8s_tim3$TIM3_ITConfig$308 ==.
	push	#0x01
	Sstm8s_tim3$TIM3_ITConfig$309 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_ITConfig$310 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_ITConfig$311 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_ITConfig$312 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_ITConfig$313 ==.
00112$:
	Sstm8s_tim3$TIM3_ITConfig$314 ==.
;	drivers/src/stm8s_tim3.c: 320: TIM3->IER |= (uint8_t)TIM3_IT;
	ld	a, 0x5321
	Sstm8s_tim3$TIM3_ITConfig$315 ==.
;	drivers/src/stm8s_tim3.c: 317: if (NewState != DISABLE)
	tnz	(0x05, sp)
	jreq	00102$
	Sstm8s_tim3$TIM3_ITConfig$316 ==.
	Sstm8s_tim3$TIM3_ITConfig$317 ==.
;	drivers/src/stm8s_tim3.c: 320: TIM3->IER |= (uint8_t)TIM3_IT;
	or	a, (0x04, sp)
	ld	0x5321, a
	Sstm8s_tim3$TIM3_ITConfig$318 ==.
	jra	00104$
00102$:
	Sstm8s_tim3$TIM3_ITConfig$319 ==.
	Sstm8s_tim3$TIM3_ITConfig$320 ==.
;	drivers/src/stm8s_tim3.c: 325: TIM3->IER &= (uint8_t)(~TIM3_IT);
	push	a
	Sstm8s_tim3$TIM3_ITConfig$321 ==.
	ld	a, (0x05, sp)
	cpl	a
	ld	(0x02, sp), a
	pop	a
	Sstm8s_tim3$TIM3_ITConfig$322 ==.
	and	a, (0x01, sp)
	ld	0x5321, a
	Sstm8s_tim3$TIM3_ITConfig$323 ==.
00104$:
	Sstm8s_tim3$TIM3_ITConfig$324 ==.
;	drivers/src/stm8s_tim3.c: 327: }
	pop	a
	Sstm8s_tim3$TIM3_ITConfig$325 ==.
	Sstm8s_tim3$TIM3_ITConfig$326 ==.
	XG$TIM3_ITConfig$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_ITConfig$327 ==.
	Sstm8s_tim3$TIM3_UpdateDisableConfig$328 ==.
;	drivers/src/stm8s_tim3.c: 335: void TIM3_UpdateDisableConfig(FunctionalState NewState)
;	-----------------------------------------
;	 function TIM3_UpdateDisableConfig
;	-----------------------------------------
_TIM3_UpdateDisableConfig:
	Sstm8s_tim3$TIM3_UpdateDisableConfig$329 ==.
	Sstm8s_tim3$TIM3_UpdateDisableConfig$330 ==.
;	drivers/src/stm8s_tim3.c: 338: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	dec	a
	jreq	00107$
	Sstm8s_tim3$TIM3_UpdateDisableConfig$331 ==.
	push	#0x52
	Sstm8s_tim3$TIM3_UpdateDisableConfig$332 ==.
	push	#0x01
	Sstm8s_tim3$TIM3_UpdateDisableConfig$333 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_UpdateDisableConfig$334 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_UpdateDisableConfig$335 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_UpdateDisableConfig$336 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_UpdateDisableConfig$337 ==.
00107$:
	Sstm8s_tim3$TIM3_UpdateDisableConfig$338 ==.
;	drivers/src/stm8s_tim3.c: 343: TIM3->CR1 |= TIM3_CR1_UDIS;
	ld	a, 0x5320
	Sstm8s_tim3$TIM3_UpdateDisableConfig$339 ==.
;	drivers/src/stm8s_tim3.c: 341: if (NewState != DISABLE)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim3$TIM3_UpdateDisableConfig$340 ==.
	Sstm8s_tim3$TIM3_UpdateDisableConfig$341 ==.
;	drivers/src/stm8s_tim3.c: 343: TIM3->CR1 |= TIM3_CR1_UDIS;
	or	a, #0x02
	ld	0x5320, a
	Sstm8s_tim3$TIM3_UpdateDisableConfig$342 ==.
	jra	00104$
00102$:
	Sstm8s_tim3$TIM3_UpdateDisableConfig$343 ==.
	Sstm8s_tim3$TIM3_UpdateDisableConfig$344 ==.
;	drivers/src/stm8s_tim3.c: 347: TIM3->CR1 &= (uint8_t)(~TIM3_CR1_UDIS);
	and	a, #0xfd
	ld	0x5320, a
	Sstm8s_tim3$TIM3_UpdateDisableConfig$345 ==.
00104$:
	Sstm8s_tim3$TIM3_UpdateDisableConfig$346 ==.
;	drivers/src/stm8s_tim3.c: 349: }
	Sstm8s_tim3$TIM3_UpdateDisableConfig$347 ==.
	XG$TIM3_UpdateDisableConfig$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_UpdateDisableConfig$348 ==.
	Sstm8s_tim3$TIM3_UpdateRequestConfig$349 ==.
;	drivers/src/stm8s_tim3.c: 359: void TIM3_UpdateRequestConfig(TIM3_UpdateSource_TypeDef TIM3_UpdateSource)
;	-----------------------------------------
;	 function TIM3_UpdateRequestConfig
;	-----------------------------------------
_TIM3_UpdateRequestConfig:
	Sstm8s_tim3$TIM3_UpdateRequestConfig$350 ==.
	Sstm8s_tim3$TIM3_UpdateRequestConfig$351 ==.
;	drivers/src/stm8s_tim3.c: 362: assert_param(IS_TIM3_UPDATE_SOURCE_OK(TIM3_UpdateSource));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	dec	a
	jreq	00107$
	Sstm8s_tim3$TIM3_UpdateRequestConfig$352 ==.
	push	#0x6a
	Sstm8s_tim3$TIM3_UpdateRequestConfig$353 ==.
	push	#0x01
	Sstm8s_tim3$TIM3_UpdateRequestConfig$354 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_UpdateRequestConfig$355 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_UpdateRequestConfig$356 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_UpdateRequestConfig$357 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_UpdateRequestConfig$358 ==.
00107$:
	Sstm8s_tim3$TIM3_UpdateRequestConfig$359 ==.
;	drivers/src/stm8s_tim3.c: 367: TIM3->CR1 |= TIM3_CR1_URS;
	ld	a, 0x5320
	Sstm8s_tim3$TIM3_UpdateRequestConfig$360 ==.
;	drivers/src/stm8s_tim3.c: 365: if (TIM3_UpdateSource != TIM3_UPDATESOURCE_GLOBAL)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim3$TIM3_UpdateRequestConfig$361 ==.
	Sstm8s_tim3$TIM3_UpdateRequestConfig$362 ==.
;	drivers/src/stm8s_tim3.c: 367: TIM3->CR1 |= TIM3_CR1_URS;
	or	a, #0x04
	ld	0x5320, a
	Sstm8s_tim3$TIM3_UpdateRequestConfig$363 ==.
	jra	00104$
00102$:
	Sstm8s_tim3$TIM3_UpdateRequestConfig$364 ==.
	Sstm8s_tim3$TIM3_UpdateRequestConfig$365 ==.
;	drivers/src/stm8s_tim3.c: 371: TIM3->CR1 &= (uint8_t)(~TIM3_CR1_URS);
	and	a, #0xfb
	ld	0x5320, a
	Sstm8s_tim3$TIM3_UpdateRequestConfig$366 ==.
00104$:
	Sstm8s_tim3$TIM3_UpdateRequestConfig$367 ==.
;	drivers/src/stm8s_tim3.c: 373: }
	Sstm8s_tim3$TIM3_UpdateRequestConfig$368 ==.
	XG$TIM3_UpdateRequestConfig$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_UpdateRequestConfig$369 ==.
	Sstm8s_tim3$TIM3_SelectOnePulseMode$370 ==.
;	drivers/src/stm8s_tim3.c: 383: void TIM3_SelectOnePulseMode(TIM3_OPMode_TypeDef TIM3_OPMode)
;	-----------------------------------------
;	 function TIM3_SelectOnePulseMode
;	-----------------------------------------
_TIM3_SelectOnePulseMode:
	Sstm8s_tim3$TIM3_SelectOnePulseMode$371 ==.
	Sstm8s_tim3$TIM3_SelectOnePulseMode$372 ==.
;	drivers/src/stm8s_tim3.c: 386: assert_param(IS_TIM3_OPM_MODE_OK(TIM3_OPMode));
	ld	a, (0x03, sp)
	dec	a
	jreq	00107$
	Sstm8s_tim3$TIM3_SelectOnePulseMode$373 ==.
	tnz	(0x03, sp)
	jreq	00107$
	push	#0x82
	Sstm8s_tim3$TIM3_SelectOnePulseMode$374 ==.
	push	#0x01
	Sstm8s_tim3$TIM3_SelectOnePulseMode$375 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_SelectOnePulseMode$376 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_SelectOnePulseMode$377 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_SelectOnePulseMode$378 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_SelectOnePulseMode$379 ==.
00107$:
	Sstm8s_tim3$TIM3_SelectOnePulseMode$380 ==.
;	drivers/src/stm8s_tim3.c: 391: TIM3->CR1 |= TIM3_CR1_OPM;
	ld	a, 0x5320
	Sstm8s_tim3$TIM3_SelectOnePulseMode$381 ==.
;	drivers/src/stm8s_tim3.c: 389: if (TIM3_OPMode != TIM3_OPMODE_REPETITIVE)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim3$TIM3_SelectOnePulseMode$382 ==.
	Sstm8s_tim3$TIM3_SelectOnePulseMode$383 ==.
;	drivers/src/stm8s_tim3.c: 391: TIM3->CR1 |= TIM3_CR1_OPM;
	or	a, #0x08
	ld	0x5320, a
	Sstm8s_tim3$TIM3_SelectOnePulseMode$384 ==.
	jra	00104$
00102$:
	Sstm8s_tim3$TIM3_SelectOnePulseMode$385 ==.
	Sstm8s_tim3$TIM3_SelectOnePulseMode$386 ==.
;	drivers/src/stm8s_tim3.c: 395: TIM3->CR1 &= (uint8_t)(~TIM3_CR1_OPM);
	and	a, #0xf7
	ld	0x5320, a
	Sstm8s_tim3$TIM3_SelectOnePulseMode$387 ==.
00104$:
	Sstm8s_tim3$TIM3_SelectOnePulseMode$388 ==.
;	drivers/src/stm8s_tim3.c: 397: }
	Sstm8s_tim3$TIM3_SelectOnePulseMode$389 ==.
	XG$TIM3_SelectOnePulseMode$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_SelectOnePulseMode$390 ==.
	Sstm8s_tim3$TIM3_PrescalerConfig$391 ==.
;	drivers/src/stm8s_tim3.c: 427: void TIM3_PrescalerConfig(TIM3_Prescaler_TypeDef Prescaler,
;	-----------------------------------------
;	 function TIM3_PrescalerConfig
;	-----------------------------------------
_TIM3_PrescalerConfig:
	Sstm8s_tim3$TIM3_PrescalerConfig$392 ==.
	Sstm8s_tim3$TIM3_PrescalerConfig$393 ==.
;	drivers/src/stm8s_tim3.c: 431: assert_param(IS_TIM3_PRESCALER_RELOAD_OK(TIM3_PSCReloadMode));
	tnz	(0x04, sp)
	jreq	00104$
	ld	a, (0x04, sp)
	dec	a
	jreq	00104$
	Sstm8s_tim3$TIM3_PrescalerConfig$394 ==.
	push	#0xaf
	Sstm8s_tim3$TIM3_PrescalerConfig$395 ==.
	push	#0x01
	Sstm8s_tim3$TIM3_PrescalerConfig$396 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_PrescalerConfig$397 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_PrescalerConfig$398 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_PrescalerConfig$399 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_PrescalerConfig$400 ==.
00104$:
	Sstm8s_tim3$TIM3_PrescalerConfig$401 ==.
;	drivers/src/stm8s_tim3.c: 432: assert_param(IS_TIM3_PRESCALER_OK(Prescaler));
	tnz	(0x03, sp)
	jrne	00249$
	jp	00109$
00249$:
	ld	a, (0x03, sp)
	dec	a
	jrne	00251$
	jp	00109$
00251$:
	Sstm8s_tim3$TIM3_PrescalerConfig$402 ==.
	ld	a, (0x03, sp)
	cp	a, #0x02
	jrne	00254$
	jp	00109$
00254$:
	Sstm8s_tim3$TIM3_PrescalerConfig$403 ==.
	ld	a, (0x03, sp)
	cp	a, #0x03
	jrne	00257$
	jp	00109$
00257$:
	Sstm8s_tim3$TIM3_PrescalerConfig$404 ==.
	ld	a, (0x03, sp)
	cp	a, #0x04
	jrne	00260$
	jp	00109$
00260$:
	Sstm8s_tim3$TIM3_PrescalerConfig$405 ==.
	ld	a, (0x03, sp)
	cp	a, #0x05
	jrne	00263$
	jp	00109$
00263$:
	Sstm8s_tim3$TIM3_PrescalerConfig$406 ==.
	ld	a, (0x03, sp)
	cp	a, #0x06
	jrne	00266$
	jp	00109$
00266$:
	Sstm8s_tim3$TIM3_PrescalerConfig$407 ==.
	ld	a, (0x03, sp)
	cp	a, #0x07
	jrne	00269$
	jp	00109$
00269$:
	Sstm8s_tim3$TIM3_PrescalerConfig$408 ==.
	ld	a, (0x03, sp)
	cp	a, #0x08
	jrne	00272$
	jp	00109$
00272$:
	Sstm8s_tim3$TIM3_PrescalerConfig$409 ==.
	ld	a, (0x03, sp)
	cp	a, #0x09
	jreq	00109$
	Sstm8s_tim3$TIM3_PrescalerConfig$410 ==.
	ld	a, (0x03, sp)
	cp	a, #0x0a
	jreq	00109$
	Sstm8s_tim3$TIM3_PrescalerConfig$411 ==.
	ld	a, (0x03, sp)
	cp	a, #0x0b
	jreq	00109$
	Sstm8s_tim3$TIM3_PrescalerConfig$412 ==.
	ld	a, (0x03, sp)
	cp	a, #0x0c
	jreq	00109$
	Sstm8s_tim3$TIM3_PrescalerConfig$413 ==.
	ld	a, (0x03, sp)
	cp	a, #0x0d
	jreq	00109$
	Sstm8s_tim3$TIM3_PrescalerConfig$414 ==.
	ld	a, (0x03, sp)
	cp	a, #0x0e
	jreq	00109$
	Sstm8s_tim3$TIM3_PrescalerConfig$415 ==.
	ld	a, (0x03, sp)
	cp	a, #0x0f
	jreq	00109$
	Sstm8s_tim3$TIM3_PrescalerConfig$416 ==.
	push	#0xb0
	Sstm8s_tim3$TIM3_PrescalerConfig$417 ==.
	push	#0x01
	Sstm8s_tim3$TIM3_PrescalerConfig$418 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_PrescalerConfig$419 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_PrescalerConfig$420 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_PrescalerConfig$421 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_PrescalerConfig$422 ==.
00109$:
	Sstm8s_tim3$TIM3_PrescalerConfig$423 ==.
;	drivers/src/stm8s_tim3.c: 435: TIM3->PSCR = (uint8_t)Prescaler;
	ldw	x, #0x532a
	ld	a, (0x03, sp)
	ld	(x), a
	Sstm8s_tim3$TIM3_PrescalerConfig$424 ==.
;	drivers/src/stm8s_tim3.c: 438: TIM3->EGR = (uint8_t)TIM3_PSCReloadMode;
	ldw	x, #0x5324
	ld	a, (0x04, sp)
	ld	(x), a
	Sstm8s_tim3$TIM3_PrescalerConfig$425 ==.
;	drivers/src/stm8s_tim3.c: 439: }
	Sstm8s_tim3$TIM3_PrescalerConfig$426 ==.
	XG$TIM3_PrescalerConfig$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_PrescalerConfig$427 ==.
	Sstm8s_tim3$TIM3_ForcedOC1Config$428 ==.
;	drivers/src/stm8s_tim3.c: 450: void TIM3_ForcedOC1Config(TIM3_ForcedAction_TypeDef TIM3_ForcedAction)
;	-----------------------------------------
;	 function TIM3_ForcedOC1Config
;	-----------------------------------------
_TIM3_ForcedOC1Config:
	Sstm8s_tim3$TIM3_ForcedOC1Config$429 ==.
	Sstm8s_tim3$TIM3_ForcedOC1Config$430 ==.
;	drivers/src/stm8s_tim3.c: 453: assert_param(IS_TIM3_FORCED_ACTION_OK(TIM3_ForcedAction));
	ld	a, (0x03, sp)
	cp	a, #0x50
	jreq	00104$
	Sstm8s_tim3$TIM3_ForcedOC1Config$431 ==.
	ld	a, (0x03, sp)
	cp	a, #0x40
	jreq	00104$
	Sstm8s_tim3$TIM3_ForcedOC1Config$432 ==.
	push	#0xc5
	Sstm8s_tim3$TIM3_ForcedOC1Config$433 ==.
	push	#0x01
	Sstm8s_tim3$TIM3_ForcedOC1Config$434 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_ForcedOC1Config$435 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_ForcedOC1Config$436 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_ForcedOC1Config$437 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_ForcedOC1Config$438 ==.
00104$:
	Sstm8s_tim3$TIM3_ForcedOC1Config$439 ==.
;	drivers/src/stm8s_tim3.c: 456: TIM3->CCMR1 =  (uint8_t)((uint8_t)(TIM3->CCMR1 & (uint8_t)(~TIM3_CCMR_OCM))  | (uint8_t)TIM3_ForcedAction);
	ld	a, 0x5325
	and	a, #0x8f
	or	a, (0x03, sp)
	ld	0x5325, a
	Sstm8s_tim3$TIM3_ForcedOC1Config$440 ==.
;	drivers/src/stm8s_tim3.c: 457: }
	Sstm8s_tim3$TIM3_ForcedOC1Config$441 ==.
	XG$TIM3_ForcedOC1Config$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_ForcedOC1Config$442 ==.
	Sstm8s_tim3$TIM3_ForcedOC2Config$443 ==.
;	drivers/src/stm8s_tim3.c: 468: void TIM3_ForcedOC2Config(TIM3_ForcedAction_TypeDef TIM3_ForcedAction)
;	-----------------------------------------
;	 function TIM3_ForcedOC2Config
;	-----------------------------------------
_TIM3_ForcedOC2Config:
	Sstm8s_tim3$TIM3_ForcedOC2Config$444 ==.
	Sstm8s_tim3$TIM3_ForcedOC2Config$445 ==.
;	drivers/src/stm8s_tim3.c: 471: assert_param(IS_TIM3_FORCED_ACTION_OK(TIM3_ForcedAction));
	ld	a, (0x03, sp)
	cp	a, #0x50
	jreq	00104$
	Sstm8s_tim3$TIM3_ForcedOC2Config$446 ==.
	ld	a, (0x03, sp)
	cp	a, #0x40
	jreq	00104$
	Sstm8s_tim3$TIM3_ForcedOC2Config$447 ==.
	push	#0xd7
	Sstm8s_tim3$TIM3_ForcedOC2Config$448 ==.
	push	#0x01
	Sstm8s_tim3$TIM3_ForcedOC2Config$449 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_ForcedOC2Config$450 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_ForcedOC2Config$451 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_ForcedOC2Config$452 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_ForcedOC2Config$453 ==.
00104$:
	Sstm8s_tim3$TIM3_ForcedOC2Config$454 ==.
;	drivers/src/stm8s_tim3.c: 474: TIM3->CCMR2 =  (uint8_t)((uint8_t)(TIM3->CCMR2 & (uint8_t)(~TIM3_CCMR_OCM)) | (uint8_t)TIM3_ForcedAction);
	ld	a, 0x5326
	and	a, #0x8f
	or	a, (0x03, sp)
	ld	0x5326, a
	Sstm8s_tim3$TIM3_ForcedOC2Config$455 ==.
;	drivers/src/stm8s_tim3.c: 475: }
	Sstm8s_tim3$TIM3_ForcedOC2Config$456 ==.
	XG$TIM3_ForcedOC2Config$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_ForcedOC2Config$457 ==.
	Sstm8s_tim3$TIM3_ARRPreloadConfig$458 ==.
;	drivers/src/stm8s_tim3.c: 483: void TIM3_ARRPreloadConfig(FunctionalState NewState)
;	-----------------------------------------
;	 function TIM3_ARRPreloadConfig
;	-----------------------------------------
_TIM3_ARRPreloadConfig:
	Sstm8s_tim3$TIM3_ARRPreloadConfig$459 ==.
	Sstm8s_tim3$TIM3_ARRPreloadConfig$460 ==.
;	drivers/src/stm8s_tim3.c: 486: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	dec	a
	jreq	00107$
	Sstm8s_tim3$TIM3_ARRPreloadConfig$461 ==.
	push	#0xe6
	Sstm8s_tim3$TIM3_ARRPreloadConfig$462 ==.
	push	#0x01
	Sstm8s_tim3$TIM3_ARRPreloadConfig$463 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_ARRPreloadConfig$464 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_ARRPreloadConfig$465 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_ARRPreloadConfig$466 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_ARRPreloadConfig$467 ==.
00107$:
	Sstm8s_tim3$TIM3_ARRPreloadConfig$468 ==.
;	drivers/src/stm8s_tim3.c: 491: TIM3->CR1 |= TIM3_CR1_ARPE;
	ld	a, 0x5320
	Sstm8s_tim3$TIM3_ARRPreloadConfig$469 ==.
;	drivers/src/stm8s_tim3.c: 489: if (NewState != DISABLE)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim3$TIM3_ARRPreloadConfig$470 ==.
	Sstm8s_tim3$TIM3_ARRPreloadConfig$471 ==.
;	drivers/src/stm8s_tim3.c: 491: TIM3->CR1 |= TIM3_CR1_ARPE;
	or	a, #0x80
	ld	0x5320, a
	Sstm8s_tim3$TIM3_ARRPreloadConfig$472 ==.
	jra	00104$
00102$:
	Sstm8s_tim3$TIM3_ARRPreloadConfig$473 ==.
	Sstm8s_tim3$TIM3_ARRPreloadConfig$474 ==.
;	drivers/src/stm8s_tim3.c: 495: TIM3->CR1 &= (uint8_t)(~TIM3_CR1_ARPE);
	and	a, #0x7f
	ld	0x5320, a
	Sstm8s_tim3$TIM3_ARRPreloadConfig$475 ==.
00104$:
	Sstm8s_tim3$TIM3_ARRPreloadConfig$476 ==.
;	drivers/src/stm8s_tim3.c: 497: }
	Sstm8s_tim3$TIM3_ARRPreloadConfig$477 ==.
	XG$TIM3_ARRPreloadConfig$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_ARRPreloadConfig$478 ==.
	Sstm8s_tim3$TIM3_OC1PreloadConfig$479 ==.
;	drivers/src/stm8s_tim3.c: 505: void TIM3_OC1PreloadConfig(FunctionalState NewState)
;	-----------------------------------------
;	 function TIM3_OC1PreloadConfig
;	-----------------------------------------
_TIM3_OC1PreloadConfig:
	Sstm8s_tim3$TIM3_OC1PreloadConfig$480 ==.
	Sstm8s_tim3$TIM3_OC1PreloadConfig$481 ==.
;	drivers/src/stm8s_tim3.c: 508: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	dec	a
	jreq	00107$
	Sstm8s_tim3$TIM3_OC1PreloadConfig$482 ==.
	push	#0xfc
	Sstm8s_tim3$TIM3_OC1PreloadConfig$483 ==.
	push	#0x01
	Sstm8s_tim3$TIM3_OC1PreloadConfig$484 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_OC1PreloadConfig$485 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_OC1PreloadConfig$486 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_OC1PreloadConfig$487 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_OC1PreloadConfig$488 ==.
00107$:
	Sstm8s_tim3$TIM3_OC1PreloadConfig$489 ==.
;	drivers/src/stm8s_tim3.c: 513: TIM3->CCMR1 |= TIM3_CCMR_OCxPE;
	ld	a, 0x5325
	Sstm8s_tim3$TIM3_OC1PreloadConfig$490 ==.
;	drivers/src/stm8s_tim3.c: 511: if (NewState != DISABLE)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim3$TIM3_OC1PreloadConfig$491 ==.
	Sstm8s_tim3$TIM3_OC1PreloadConfig$492 ==.
;	drivers/src/stm8s_tim3.c: 513: TIM3->CCMR1 |= TIM3_CCMR_OCxPE;
	or	a, #0x08
	ld	0x5325, a
	Sstm8s_tim3$TIM3_OC1PreloadConfig$493 ==.
	jra	00104$
00102$:
	Sstm8s_tim3$TIM3_OC1PreloadConfig$494 ==.
	Sstm8s_tim3$TIM3_OC1PreloadConfig$495 ==.
;	drivers/src/stm8s_tim3.c: 517: TIM3->CCMR1 &= (uint8_t)(~TIM3_CCMR_OCxPE);
	and	a, #0xf7
	ld	0x5325, a
	Sstm8s_tim3$TIM3_OC1PreloadConfig$496 ==.
00104$:
	Sstm8s_tim3$TIM3_OC1PreloadConfig$497 ==.
;	drivers/src/stm8s_tim3.c: 519: }
	Sstm8s_tim3$TIM3_OC1PreloadConfig$498 ==.
	XG$TIM3_OC1PreloadConfig$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_OC1PreloadConfig$499 ==.
	Sstm8s_tim3$TIM3_OC2PreloadConfig$500 ==.
;	drivers/src/stm8s_tim3.c: 527: void TIM3_OC2PreloadConfig(FunctionalState NewState)
;	-----------------------------------------
;	 function TIM3_OC2PreloadConfig
;	-----------------------------------------
_TIM3_OC2PreloadConfig:
	Sstm8s_tim3$TIM3_OC2PreloadConfig$501 ==.
	Sstm8s_tim3$TIM3_OC2PreloadConfig$502 ==.
;	drivers/src/stm8s_tim3.c: 530: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	dec	a
	jreq	00107$
	Sstm8s_tim3$TIM3_OC2PreloadConfig$503 ==.
	push	#0x12
	Sstm8s_tim3$TIM3_OC2PreloadConfig$504 ==.
	push	#0x02
	Sstm8s_tim3$TIM3_OC2PreloadConfig$505 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_OC2PreloadConfig$506 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_OC2PreloadConfig$507 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_OC2PreloadConfig$508 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_OC2PreloadConfig$509 ==.
00107$:
	Sstm8s_tim3$TIM3_OC2PreloadConfig$510 ==.
;	drivers/src/stm8s_tim3.c: 535: TIM3->CCMR2 |= TIM3_CCMR_OCxPE;
	ld	a, 0x5326
	Sstm8s_tim3$TIM3_OC2PreloadConfig$511 ==.
;	drivers/src/stm8s_tim3.c: 533: if (NewState != DISABLE)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim3$TIM3_OC2PreloadConfig$512 ==.
	Sstm8s_tim3$TIM3_OC2PreloadConfig$513 ==.
;	drivers/src/stm8s_tim3.c: 535: TIM3->CCMR2 |= TIM3_CCMR_OCxPE;
	or	a, #0x08
	ld	0x5326, a
	Sstm8s_tim3$TIM3_OC2PreloadConfig$514 ==.
	jra	00104$
00102$:
	Sstm8s_tim3$TIM3_OC2PreloadConfig$515 ==.
	Sstm8s_tim3$TIM3_OC2PreloadConfig$516 ==.
;	drivers/src/stm8s_tim3.c: 539: TIM3->CCMR2 &= (uint8_t)(~TIM3_CCMR_OCxPE);
	and	a, #0xf7
	ld	0x5326, a
	Sstm8s_tim3$TIM3_OC2PreloadConfig$517 ==.
00104$:
	Sstm8s_tim3$TIM3_OC2PreloadConfig$518 ==.
;	drivers/src/stm8s_tim3.c: 541: }
	Sstm8s_tim3$TIM3_OC2PreloadConfig$519 ==.
	XG$TIM3_OC2PreloadConfig$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_OC2PreloadConfig$520 ==.
	Sstm8s_tim3$TIM3_GenerateEvent$521 ==.
;	drivers/src/stm8s_tim3.c: 552: void TIM3_GenerateEvent(TIM3_EventSource_TypeDef TIM3_EventSource)
;	-----------------------------------------
;	 function TIM3_GenerateEvent
;	-----------------------------------------
_TIM3_GenerateEvent:
	Sstm8s_tim3$TIM3_GenerateEvent$522 ==.
	Sstm8s_tim3$TIM3_GenerateEvent$523 ==.
;	drivers/src/stm8s_tim3.c: 555: assert_param(IS_TIM3_EVENT_SOURCE_OK(TIM3_EventSource));
	tnz	(0x03, sp)
	jrne	00104$
	push	#0x2b
	Sstm8s_tim3$TIM3_GenerateEvent$524 ==.
	push	#0x02
	Sstm8s_tim3$TIM3_GenerateEvent$525 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_GenerateEvent$526 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_GenerateEvent$527 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_GenerateEvent$528 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_GenerateEvent$529 ==.
00104$:
	Sstm8s_tim3$TIM3_GenerateEvent$530 ==.
;	drivers/src/stm8s_tim3.c: 558: TIM3->EGR = (uint8_t)TIM3_EventSource;
	ldw	x, #0x5324
	ld	a, (0x03, sp)
	ld	(x), a
	Sstm8s_tim3$TIM3_GenerateEvent$531 ==.
;	drivers/src/stm8s_tim3.c: 559: }
	Sstm8s_tim3$TIM3_GenerateEvent$532 ==.
	XG$TIM3_GenerateEvent$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_GenerateEvent$533 ==.
	Sstm8s_tim3$TIM3_OC1PolarityConfig$534 ==.
;	drivers/src/stm8s_tim3.c: 569: void TIM3_OC1PolarityConfig(TIM3_OCPolarity_TypeDef TIM3_OCPolarity)
;	-----------------------------------------
;	 function TIM3_OC1PolarityConfig
;	-----------------------------------------
_TIM3_OC1PolarityConfig:
	Sstm8s_tim3$TIM3_OC1PolarityConfig$535 ==.
	Sstm8s_tim3$TIM3_OC1PolarityConfig$536 ==.
;	drivers/src/stm8s_tim3.c: 572: assert_param(IS_TIM3_OC_POLARITY_OK(TIM3_OCPolarity));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	cp	a, #0x22
	jreq	00107$
	Sstm8s_tim3$TIM3_OC1PolarityConfig$537 ==.
	push	#0x3c
	Sstm8s_tim3$TIM3_OC1PolarityConfig$538 ==.
	push	#0x02
	Sstm8s_tim3$TIM3_OC1PolarityConfig$539 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_OC1PolarityConfig$540 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_OC1PolarityConfig$541 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_OC1PolarityConfig$542 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_OC1PolarityConfig$543 ==.
00107$:
	Sstm8s_tim3$TIM3_OC1PolarityConfig$544 ==.
;	drivers/src/stm8s_tim3.c: 577: TIM3->CCER1 |= TIM3_CCER1_CC1P;
	ld	a, 0x5327
	Sstm8s_tim3$TIM3_OC1PolarityConfig$545 ==.
;	drivers/src/stm8s_tim3.c: 575: if (TIM3_OCPolarity != TIM3_OCPOLARITY_HIGH)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim3$TIM3_OC1PolarityConfig$546 ==.
	Sstm8s_tim3$TIM3_OC1PolarityConfig$547 ==.
;	drivers/src/stm8s_tim3.c: 577: TIM3->CCER1 |= TIM3_CCER1_CC1P;
	or	a, #0x02
	ld	0x5327, a
	Sstm8s_tim3$TIM3_OC1PolarityConfig$548 ==.
	jra	00104$
00102$:
	Sstm8s_tim3$TIM3_OC1PolarityConfig$549 ==.
	Sstm8s_tim3$TIM3_OC1PolarityConfig$550 ==.
;	drivers/src/stm8s_tim3.c: 581: TIM3->CCER1 &= (uint8_t)(~TIM3_CCER1_CC1P);
	and	a, #0xfd
	ld	0x5327, a
	Sstm8s_tim3$TIM3_OC1PolarityConfig$551 ==.
00104$:
	Sstm8s_tim3$TIM3_OC1PolarityConfig$552 ==.
;	drivers/src/stm8s_tim3.c: 583: }
	Sstm8s_tim3$TIM3_OC1PolarityConfig$553 ==.
	XG$TIM3_OC1PolarityConfig$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_OC1PolarityConfig$554 ==.
	Sstm8s_tim3$TIM3_OC2PolarityConfig$555 ==.
;	drivers/src/stm8s_tim3.c: 593: void TIM3_OC2PolarityConfig(TIM3_OCPolarity_TypeDef TIM3_OCPolarity)
;	-----------------------------------------
;	 function TIM3_OC2PolarityConfig
;	-----------------------------------------
_TIM3_OC2PolarityConfig:
	Sstm8s_tim3$TIM3_OC2PolarityConfig$556 ==.
	Sstm8s_tim3$TIM3_OC2PolarityConfig$557 ==.
;	drivers/src/stm8s_tim3.c: 596: assert_param(IS_TIM3_OC_POLARITY_OK(TIM3_OCPolarity));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	cp	a, #0x22
	jreq	00107$
	Sstm8s_tim3$TIM3_OC2PolarityConfig$558 ==.
	push	#0x54
	Sstm8s_tim3$TIM3_OC2PolarityConfig$559 ==.
	push	#0x02
	Sstm8s_tim3$TIM3_OC2PolarityConfig$560 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_OC2PolarityConfig$561 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_OC2PolarityConfig$562 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_OC2PolarityConfig$563 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_OC2PolarityConfig$564 ==.
00107$:
	Sstm8s_tim3$TIM3_OC2PolarityConfig$565 ==.
;	drivers/src/stm8s_tim3.c: 601: TIM3->CCER1 |= TIM3_CCER1_CC2P;
	ld	a, 0x5327
	Sstm8s_tim3$TIM3_OC2PolarityConfig$566 ==.
;	drivers/src/stm8s_tim3.c: 599: if (TIM3_OCPolarity != TIM3_OCPOLARITY_HIGH)
	tnz	(0x03, sp)
	jreq	00102$
	Sstm8s_tim3$TIM3_OC2PolarityConfig$567 ==.
	Sstm8s_tim3$TIM3_OC2PolarityConfig$568 ==.
;	drivers/src/stm8s_tim3.c: 601: TIM3->CCER1 |= TIM3_CCER1_CC2P;
	or	a, #0x20
	ld	0x5327, a
	Sstm8s_tim3$TIM3_OC2PolarityConfig$569 ==.
	jra	00104$
00102$:
	Sstm8s_tim3$TIM3_OC2PolarityConfig$570 ==.
	Sstm8s_tim3$TIM3_OC2PolarityConfig$571 ==.
;	drivers/src/stm8s_tim3.c: 605: TIM3->CCER1 &= (uint8_t)(~TIM3_CCER1_CC2P);
	and	a, #0xdf
	ld	0x5327, a
	Sstm8s_tim3$TIM3_OC2PolarityConfig$572 ==.
00104$:
	Sstm8s_tim3$TIM3_OC2PolarityConfig$573 ==.
;	drivers/src/stm8s_tim3.c: 607: }
	Sstm8s_tim3$TIM3_OC2PolarityConfig$574 ==.
	XG$TIM3_OC2PolarityConfig$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_OC2PolarityConfig$575 ==.
	Sstm8s_tim3$TIM3_CCxCmd$576 ==.
;	drivers/src/stm8s_tim3.c: 619: void TIM3_CCxCmd(TIM3_Channel_TypeDef TIM3_Channel, FunctionalState NewState)
;	-----------------------------------------
;	 function TIM3_CCxCmd
;	-----------------------------------------
_TIM3_CCxCmd:
	Sstm8s_tim3$TIM3_CCxCmd$577 ==.
	Sstm8s_tim3$TIM3_CCxCmd$578 ==.
;	drivers/src/stm8s_tim3.c: 622: assert_param(IS_TIM3_CHANNEL_OK(TIM3_Channel));
	tnz	(0x03, sp)
	jreq	00113$
	ld	a, (0x03, sp)
	dec	a
	jreq	00113$
	Sstm8s_tim3$TIM3_CCxCmd$579 ==.
	push	#0x6e
	Sstm8s_tim3$TIM3_CCxCmd$580 ==.
	push	#0x02
	Sstm8s_tim3$TIM3_CCxCmd$581 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_CCxCmd$582 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_CCxCmd$583 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_CCxCmd$584 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_CCxCmd$585 ==.
00113$:
	Sstm8s_tim3$TIM3_CCxCmd$586 ==.
;	drivers/src/stm8s_tim3.c: 623: assert_param(IS_FUNCTIONALSTATE_OK(NewState));
	tnz	(0x04, sp)
	jreq	00118$
	ld	a, (0x04, sp)
	dec	a
	jreq	00118$
	Sstm8s_tim3$TIM3_CCxCmd$587 ==.
	push	#0x6f
	Sstm8s_tim3$TIM3_CCxCmd$588 ==.
	push	#0x02
	Sstm8s_tim3$TIM3_CCxCmd$589 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_CCxCmd$590 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_CCxCmd$591 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_CCxCmd$592 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_CCxCmd$593 ==.
00118$:
	Sstm8s_tim3$TIM3_CCxCmd$594 ==.
;	drivers/src/stm8s_tim3.c: 630: TIM3->CCER1 |= TIM3_CCER1_CC1E;
	ld	a, 0x5327
	Sstm8s_tim3$TIM3_CCxCmd$595 ==.
;	drivers/src/stm8s_tim3.c: 625: if (TIM3_Channel == TIM3_CHANNEL_1)
	tnz	(0x03, sp)
	jrne	00108$
	Sstm8s_tim3$TIM3_CCxCmd$596 ==.
	Sstm8s_tim3$TIM3_CCxCmd$597 ==.
;	drivers/src/stm8s_tim3.c: 628: if (NewState != DISABLE)
	tnz	(0x04, sp)
	jreq	00102$
	Sstm8s_tim3$TIM3_CCxCmd$598 ==.
	Sstm8s_tim3$TIM3_CCxCmd$599 ==.
;	drivers/src/stm8s_tim3.c: 630: TIM3->CCER1 |= TIM3_CCER1_CC1E;
	or	a, #0x01
	ld	0x5327, a
	Sstm8s_tim3$TIM3_CCxCmd$600 ==.
	jra	00110$
00102$:
	Sstm8s_tim3$TIM3_CCxCmd$601 ==.
	Sstm8s_tim3$TIM3_CCxCmd$602 ==.
;	drivers/src/stm8s_tim3.c: 634: TIM3->CCER1 &= (uint8_t)(~TIM3_CCER1_CC1E);
	and	a, #0xfe
	ld	0x5327, a
	Sstm8s_tim3$TIM3_CCxCmd$603 ==.
	jra	00110$
00108$:
	Sstm8s_tim3$TIM3_CCxCmd$604 ==.
	Sstm8s_tim3$TIM3_CCxCmd$605 ==.
;	drivers/src/stm8s_tim3.c: 641: if (NewState != DISABLE)
	tnz	(0x04, sp)
	jreq	00105$
	Sstm8s_tim3$TIM3_CCxCmd$606 ==.
	Sstm8s_tim3$TIM3_CCxCmd$607 ==.
;	drivers/src/stm8s_tim3.c: 643: TIM3->CCER1 |= TIM3_CCER1_CC2E;
	or	a, #0x10
	ld	0x5327, a
	Sstm8s_tim3$TIM3_CCxCmd$608 ==.
	jra	00110$
00105$:
	Sstm8s_tim3$TIM3_CCxCmd$609 ==.
	Sstm8s_tim3$TIM3_CCxCmd$610 ==.
;	drivers/src/stm8s_tim3.c: 647: TIM3->CCER1 &= (uint8_t)(~TIM3_CCER1_CC2E);
	and	a, #0xef
	ld	0x5327, a
	Sstm8s_tim3$TIM3_CCxCmd$611 ==.
00110$:
	Sstm8s_tim3$TIM3_CCxCmd$612 ==.
;	drivers/src/stm8s_tim3.c: 650: }
	Sstm8s_tim3$TIM3_CCxCmd$613 ==.
	XG$TIM3_CCxCmd$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_CCxCmd$614 ==.
	Sstm8s_tim3$TIM3_SelectOCxM$615 ==.
;	drivers/src/stm8s_tim3.c: 671: void TIM3_SelectOCxM(TIM3_Channel_TypeDef TIM3_Channel, TIM3_OCMode_TypeDef TIM3_OCMode)
;	-----------------------------------------
;	 function TIM3_SelectOCxM
;	-----------------------------------------
_TIM3_SelectOCxM:
	Sstm8s_tim3$TIM3_SelectOCxM$616 ==.
	Sstm8s_tim3$TIM3_SelectOCxM$617 ==.
;	drivers/src/stm8s_tim3.c: 674: assert_param(IS_TIM3_CHANNEL_OK(TIM3_Channel));
	tnz	(0x03, sp)
	jreq	00107$
	ld	a, (0x03, sp)
	dec	a
	jreq	00107$
	Sstm8s_tim3$TIM3_SelectOCxM$618 ==.
	push	#0xa2
	Sstm8s_tim3$TIM3_SelectOCxM$619 ==.
	push	#0x02
	Sstm8s_tim3$TIM3_SelectOCxM$620 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_SelectOCxM$621 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_SelectOCxM$622 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_SelectOCxM$623 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_SelectOCxM$624 ==.
00107$:
	Sstm8s_tim3$TIM3_SelectOCxM$625 ==.
;	drivers/src/stm8s_tim3.c: 675: assert_param(IS_TIM3_OCM_OK(TIM3_OCMode));
	tnz	(0x04, sp)
	jreq	00112$
	ld	a, (0x04, sp)
	cp	a, #0x10
	jreq	00112$
	Sstm8s_tim3$TIM3_SelectOCxM$626 ==.
	ld	a, (0x04, sp)
	cp	a, #0x20
	jreq	00112$
	Sstm8s_tim3$TIM3_SelectOCxM$627 ==.
	ld	a, (0x04, sp)
	cp	a, #0x30
	jreq	00112$
	Sstm8s_tim3$TIM3_SelectOCxM$628 ==.
	ld	a, (0x04, sp)
	cp	a, #0x60
	jreq	00112$
	Sstm8s_tim3$TIM3_SelectOCxM$629 ==.
	ld	a, (0x04, sp)
	cp	a, #0x70
	jreq	00112$
	Sstm8s_tim3$TIM3_SelectOCxM$630 ==.
	ld	a, (0x04, sp)
	cp	a, #0x50
	jreq	00112$
	Sstm8s_tim3$TIM3_SelectOCxM$631 ==.
	ld	a, (0x04, sp)
	cp	a, #0x40
	jreq	00112$
	Sstm8s_tim3$TIM3_SelectOCxM$632 ==.
	push	#0xa3
	Sstm8s_tim3$TIM3_SelectOCxM$633 ==.
	push	#0x02
	Sstm8s_tim3$TIM3_SelectOCxM$634 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_SelectOCxM$635 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_SelectOCxM$636 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_SelectOCxM$637 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_SelectOCxM$638 ==.
00112$:
	Sstm8s_tim3$TIM3_SelectOCxM$639 ==.
;	drivers/src/stm8s_tim3.c: 680: TIM3->CCER1 &= (uint8_t)(~TIM3_CCER1_CC1E);
	ld	a, 0x5327
	Sstm8s_tim3$TIM3_SelectOCxM$640 ==.
;	drivers/src/stm8s_tim3.c: 677: if (TIM3_Channel == TIM3_CHANNEL_1)
	tnz	(0x03, sp)
	jrne	00102$
	Sstm8s_tim3$TIM3_SelectOCxM$641 ==.
	Sstm8s_tim3$TIM3_SelectOCxM$642 ==.
;	drivers/src/stm8s_tim3.c: 680: TIM3->CCER1 &= (uint8_t)(~TIM3_CCER1_CC1E);
	and	a, #0xfe
	ld	0x5327, a
	Sstm8s_tim3$TIM3_SelectOCxM$643 ==.
;	drivers/src/stm8s_tim3.c: 683: TIM3->CCMR1 = (uint8_t)((uint8_t)(TIM3->CCMR1 & (uint8_t)(~TIM3_CCMR_OCM)) | (uint8_t)TIM3_OCMode);
	ld	a, 0x5325
	and	a, #0x8f
	or	a, (0x04, sp)
	ld	0x5325, a
	Sstm8s_tim3$TIM3_SelectOCxM$644 ==.
	jra	00104$
00102$:
	Sstm8s_tim3$TIM3_SelectOCxM$645 ==.
	Sstm8s_tim3$TIM3_SelectOCxM$646 ==.
;	drivers/src/stm8s_tim3.c: 688: TIM3->CCER1 &= (uint8_t)(~TIM3_CCER1_CC2E);
	and	a, #0xef
	ld	0x5327, a
	Sstm8s_tim3$TIM3_SelectOCxM$647 ==.
;	drivers/src/stm8s_tim3.c: 691: TIM3->CCMR2 = (uint8_t)((uint8_t)(TIM3->CCMR2 & (uint8_t)(~TIM3_CCMR_OCM)) | (uint8_t)TIM3_OCMode);
	ld	a, 0x5326
	and	a, #0x8f
	or	a, (0x04, sp)
	ld	0x5326, a
	Sstm8s_tim3$TIM3_SelectOCxM$648 ==.
00104$:
	Sstm8s_tim3$TIM3_SelectOCxM$649 ==.
;	drivers/src/stm8s_tim3.c: 693: }
	Sstm8s_tim3$TIM3_SelectOCxM$650 ==.
	XG$TIM3_SelectOCxM$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_SelectOCxM$651 ==.
	Sstm8s_tim3$TIM3_SetCounter$652 ==.
;	drivers/src/stm8s_tim3.c: 701: void TIM3_SetCounter(uint16_t Counter)
;	-----------------------------------------
;	 function TIM3_SetCounter
;	-----------------------------------------
_TIM3_SetCounter:
	Sstm8s_tim3$TIM3_SetCounter$653 ==.
	Sstm8s_tim3$TIM3_SetCounter$654 ==.
;	drivers/src/stm8s_tim3.c: 704: TIM3->CNTRH = (uint8_t)(Counter >> 8);
	ld	a, (0x03, sp)
	ld	0x5328, a
	Sstm8s_tim3$TIM3_SetCounter$655 ==.
;	drivers/src/stm8s_tim3.c: 705: TIM3->CNTRL = (uint8_t)(Counter);
	ld	a, (0x04, sp)
	ld	0x5329, a
	Sstm8s_tim3$TIM3_SetCounter$656 ==.
;	drivers/src/stm8s_tim3.c: 706: }
	Sstm8s_tim3$TIM3_SetCounter$657 ==.
	XG$TIM3_SetCounter$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_SetCounter$658 ==.
	Sstm8s_tim3$TIM3_SetAutoreload$659 ==.
;	drivers/src/stm8s_tim3.c: 714: void TIM3_SetAutoreload(uint16_t Autoreload)
;	-----------------------------------------
;	 function TIM3_SetAutoreload
;	-----------------------------------------
_TIM3_SetAutoreload:
	Sstm8s_tim3$TIM3_SetAutoreload$660 ==.
	Sstm8s_tim3$TIM3_SetAutoreload$661 ==.
;	drivers/src/stm8s_tim3.c: 717: TIM3->ARRH = (uint8_t)(Autoreload >> 8);
	ld	a, (0x03, sp)
	ld	0x532b, a
	Sstm8s_tim3$TIM3_SetAutoreload$662 ==.
;	drivers/src/stm8s_tim3.c: 718: TIM3->ARRL = (uint8_t)(Autoreload);
	ld	a, (0x04, sp)
	ld	0x532c, a
	Sstm8s_tim3$TIM3_SetAutoreload$663 ==.
;	drivers/src/stm8s_tim3.c: 719: }
	Sstm8s_tim3$TIM3_SetAutoreload$664 ==.
	XG$TIM3_SetAutoreload$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_SetAutoreload$665 ==.
	Sstm8s_tim3$TIM3_SetCompare1$666 ==.
;	drivers/src/stm8s_tim3.c: 727: void TIM3_SetCompare1(uint16_t Compare1)
;	-----------------------------------------
;	 function TIM3_SetCompare1
;	-----------------------------------------
_TIM3_SetCompare1:
	Sstm8s_tim3$TIM3_SetCompare1$667 ==.
	Sstm8s_tim3$TIM3_SetCompare1$668 ==.
;	drivers/src/stm8s_tim3.c: 730: TIM3->CCR1H = (uint8_t)(Compare1 >> 8);
	ld	a, (0x03, sp)
	ld	0x532d, a
	Sstm8s_tim3$TIM3_SetCompare1$669 ==.
;	drivers/src/stm8s_tim3.c: 731: TIM3->CCR1L = (uint8_t)(Compare1);
	ld	a, (0x04, sp)
	ld	0x532e, a
	Sstm8s_tim3$TIM3_SetCompare1$670 ==.
;	drivers/src/stm8s_tim3.c: 732: }
	Sstm8s_tim3$TIM3_SetCompare1$671 ==.
	XG$TIM3_SetCompare1$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_SetCompare1$672 ==.
	Sstm8s_tim3$TIM3_SetCompare2$673 ==.
;	drivers/src/stm8s_tim3.c: 740: void TIM3_SetCompare2(uint16_t Compare2)
;	-----------------------------------------
;	 function TIM3_SetCompare2
;	-----------------------------------------
_TIM3_SetCompare2:
	Sstm8s_tim3$TIM3_SetCompare2$674 ==.
	Sstm8s_tim3$TIM3_SetCompare2$675 ==.
;	drivers/src/stm8s_tim3.c: 743: TIM3->CCR2H = (uint8_t)(Compare2 >> 8);
	ld	a, (0x03, sp)
	ld	0x532f, a
	Sstm8s_tim3$TIM3_SetCompare2$676 ==.
;	drivers/src/stm8s_tim3.c: 744: TIM3->CCR2L = (uint8_t)(Compare2);
	ld	a, (0x04, sp)
	ld	0x5330, a
	Sstm8s_tim3$TIM3_SetCompare2$677 ==.
;	drivers/src/stm8s_tim3.c: 745: }
	Sstm8s_tim3$TIM3_SetCompare2$678 ==.
	XG$TIM3_SetCompare2$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_SetCompare2$679 ==.
	Sstm8s_tim3$TIM3_SetIC1Prescaler$680 ==.
;	drivers/src/stm8s_tim3.c: 757: void TIM3_SetIC1Prescaler(TIM3_ICPSC_TypeDef TIM3_IC1Prescaler)
;	-----------------------------------------
;	 function TIM3_SetIC1Prescaler
;	-----------------------------------------
_TIM3_SetIC1Prescaler:
	Sstm8s_tim3$TIM3_SetIC1Prescaler$681 ==.
	Sstm8s_tim3$TIM3_SetIC1Prescaler$682 ==.
;	drivers/src/stm8s_tim3.c: 760: assert_param(IS_TIM3_IC_PRESCALER_OK(TIM3_IC1Prescaler));
	tnz	(0x03, sp)
	jreq	00104$
	ld	a, (0x03, sp)
	cp	a, #0x04
	jreq	00104$
	Sstm8s_tim3$TIM3_SetIC1Prescaler$683 ==.
	ld	a, (0x03, sp)
	cp	a, #0x08
	jreq	00104$
	Sstm8s_tim3$TIM3_SetIC1Prescaler$684 ==.
	ld	a, (0x03, sp)
	cp	a, #0x0c
	jreq	00104$
	Sstm8s_tim3$TIM3_SetIC1Prescaler$685 ==.
	push	#0xf8
	Sstm8s_tim3$TIM3_SetIC1Prescaler$686 ==.
	push	#0x02
	Sstm8s_tim3$TIM3_SetIC1Prescaler$687 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_SetIC1Prescaler$688 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_SetIC1Prescaler$689 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_SetIC1Prescaler$690 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_SetIC1Prescaler$691 ==.
00104$:
	Sstm8s_tim3$TIM3_SetIC1Prescaler$692 ==.
;	drivers/src/stm8s_tim3.c: 763: TIM3->CCMR1 = (uint8_t)((uint8_t)(TIM3->CCMR1 & (uint8_t)(~TIM3_CCMR_ICxPSC)) | (uint8_t)TIM3_IC1Prescaler);
	ld	a, 0x5325
	and	a, #0xf3
	or	a, (0x03, sp)
	ld	0x5325, a
	Sstm8s_tim3$TIM3_SetIC1Prescaler$693 ==.
;	drivers/src/stm8s_tim3.c: 764: }
	Sstm8s_tim3$TIM3_SetIC1Prescaler$694 ==.
	XG$TIM3_SetIC1Prescaler$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_SetIC1Prescaler$695 ==.
	Sstm8s_tim3$TIM3_SetIC2Prescaler$696 ==.
;	drivers/src/stm8s_tim3.c: 776: void TIM3_SetIC2Prescaler(TIM3_ICPSC_TypeDef TIM3_IC2Prescaler)
;	-----------------------------------------
;	 function TIM3_SetIC2Prescaler
;	-----------------------------------------
_TIM3_SetIC2Prescaler:
	Sstm8s_tim3$TIM3_SetIC2Prescaler$697 ==.
	Sstm8s_tim3$TIM3_SetIC2Prescaler$698 ==.
;	drivers/src/stm8s_tim3.c: 779: assert_param(IS_TIM3_IC_PRESCALER_OK(TIM3_IC2Prescaler));
	tnz	(0x03, sp)
	jreq	00104$
	ld	a, (0x03, sp)
	cp	a, #0x04
	jreq	00104$
	Sstm8s_tim3$TIM3_SetIC2Prescaler$699 ==.
	ld	a, (0x03, sp)
	cp	a, #0x08
	jreq	00104$
	Sstm8s_tim3$TIM3_SetIC2Prescaler$700 ==.
	ld	a, (0x03, sp)
	cp	a, #0x0c
	jreq	00104$
	Sstm8s_tim3$TIM3_SetIC2Prescaler$701 ==.
	push	#0x0b
	Sstm8s_tim3$TIM3_SetIC2Prescaler$702 ==.
	push	#0x03
	Sstm8s_tim3$TIM3_SetIC2Prescaler$703 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_SetIC2Prescaler$704 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_SetIC2Prescaler$705 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_SetIC2Prescaler$706 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_SetIC2Prescaler$707 ==.
00104$:
	Sstm8s_tim3$TIM3_SetIC2Prescaler$708 ==.
;	drivers/src/stm8s_tim3.c: 782: TIM3->CCMR2 = (uint8_t)((uint8_t)(TIM3->CCMR2 & (uint8_t)(~TIM3_CCMR_ICxPSC)) | (uint8_t)TIM3_IC2Prescaler);
	ld	a, 0x5326
	and	a, #0xf3
	or	a, (0x03, sp)
	ld	0x5326, a
	Sstm8s_tim3$TIM3_SetIC2Prescaler$709 ==.
;	drivers/src/stm8s_tim3.c: 783: }
	Sstm8s_tim3$TIM3_SetIC2Prescaler$710 ==.
	XG$TIM3_SetIC2Prescaler$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_SetIC2Prescaler$711 ==.
	Sstm8s_tim3$TIM3_GetCapture1$712 ==.
;	drivers/src/stm8s_tim3.c: 790: uint16_t TIM3_GetCapture1(void)
;	-----------------------------------------
;	 function TIM3_GetCapture1
;	-----------------------------------------
_TIM3_GetCapture1:
	Sstm8s_tim3$TIM3_GetCapture1$713 ==.
	pushw	x
	Sstm8s_tim3$TIM3_GetCapture1$714 ==.
	Sstm8s_tim3$TIM3_GetCapture1$715 ==.
;	drivers/src/stm8s_tim3.c: 796: tmpccr1h = TIM3->CCR1H;
	ld	a, 0x532d
	ld	xh, a
	Sstm8s_tim3$TIM3_GetCapture1$716 ==.
;	drivers/src/stm8s_tim3.c: 797: tmpccr1l = TIM3->CCR1L;
	ld	a, 0x532e
	Sstm8s_tim3$TIM3_GetCapture1$717 ==.
;	drivers/src/stm8s_tim3.c: 799: tmpccr1 = (uint16_t)(tmpccr1l);
	ld	xl, a
	clr	a
	Sstm8s_tim3$TIM3_GetCapture1$718 ==.
;	drivers/src/stm8s_tim3.c: 800: tmpccr1 |= (uint16_t)((uint16_t)tmpccr1h << 8);
	clr	(0x02, sp)
	pushw	x
	Sstm8s_tim3$TIM3_GetCapture1$719 ==.
	or	a, (1, sp)
	popw	x
	Sstm8s_tim3$TIM3_GetCapture1$720 ==.
	rrwa	x
	or	a, (0x02, sp)
	ld	xl, a
	Sstm8s_tim3$TIM3_GetCapture1$721 ==.
;	drivers/src/stm8s_tim3.c: 802: return (uint16_t)tmpccr1;
	Sstm8s_tim3$TIM3_GetCapture1$722 ==.
;	drivers/src/stm8s_tim3.c: 803: }
	addw	sp, #2
	Sstm8s_tim3$TIM3_GetCapture1$723 ==.
	Sstm8s_tim3$TIM3_GetCapture1$724 ==.
	XG$TIM3_GetCapture1$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_GetCapture1$725 ==.
	Sstm8s_tim3$TIM3_GetCapture2$726 ==.
;	drivers/src/stm8s_tim3.c: 810: uint16_t TIM3_GetCapture2(void)
;	-----------------------------------------
;	 function TIM3_GetCapture2
;	-----------------------------------------
_TIM3_GetCapture2:
	Sstm8s_tim3$TIM3_GetCapture2$727 ==.
	pushw	x
	Sstm8s_tim3$TIM3_GetCapture2$728 ==.
	Sstm8s_tim3$TIM3_GetCapture2$729 ==.
;	drivers/src/stm8s_tim3.c: 816: tmpccr2h = TIM3->CCR2H;
	ld	a, 0x532f
	ld	xh, a
	Sstm8s_tim3$TIM3_GetCapture2$730 ==.
;	drivers/src/stm8s_tim3.c: 817: tmpccr2l = TIM3->CCR2L;
	ld	a, 0x5330
	Sstm8s_tim3$TIM3_GetCapture2$731 ==.
;	drivers/src/stm8s_tim3.c: 819: tmpccr2 = (uint16_t)(tmpccr2l);
	ld	xl, a
	clr	a
	Sstm8s_tim3$TIM3_GetCapture2$732 ==.
;	drivers/src/stm8s_tim3.c: 820: tmpccr2 |= (uint16_t)((uint16_t)tmpccr2h << 8);
	clr	(0x02, sp)
	pushw	x
	Sstm8s_tim3$TIM3_GetCapture2$733 ==.
	or	a, (1, sp)
	popw	x
	Sstm8s_tim3$TIM3_GetCapture2$734 ==.
	rrwa	x
	or	a, (0x02, sp)
	ld	xl, a
	Sstm8s_tim3$TIM3_GetCapture2$735 ==.
;	drivers/src/stm8s_tim3.c: 822: return (uint16_t)tmpccr2;
	Sstm8s_tim3$TIM3_GetCapture2$736 ==.
;	drivers/src/stm8s_tim3.c: 823: }
	addw	sp, #2
	Sstm8s_tim3$TIM3_GetCapture2$737 ==.
	Sstm8s_tim3$TIM3_GetCapture2$738 ==.
	XG$TIM3_GetCapture2$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_GetCapture2$739 ==.
	Sstm8s_tim3$TIM3_GetCounter$740 ==.
;	drivers/src/stm8s_tim3.c: 830: uint16_t TIM3_GetCounter(void)
;	-----------------------------------------
;	 function TIM3_GetCounter
;	-----------------------------------------
_TIM3_GetCounter:
	Sstm8s_tim3$TIM3_GetCounter$741 ==.
	sub	sp, #4
	Sstm8s_tim3$TIM3_GetCounter$742 ==.
	Sstm8s_tim3$TIM3_GetCounter$743 ==.
;	drivers/src/stm8s_tim3.c: 834: tmpcntr = ((uint16_t)TIM3->CNTRH << 8);
	ld	a, 0x5328
	clrw	x
	ld	xh, a
	clr	a
	ld	(0x02, sp), a
	Sstm8s_tim3$TIM3_GetCounter$744 ==.
;	drivers/src/stm8s_tim3.c: 836: return (uint16_t)( tmpcntr| (uint16_t)(TIM3->CNTRL));
	ld	a, 0x5329
	clr	(0x03, sp)
	or	a, (0x02, sp)
	rlwa	x
	or	a, (0x03, sp)
	ld	xh, a
	Sstm8s_tim3$TIM3_GetCounter$745 ==.
;	drivers/src/stm8s_tim3.c: 837: }
	addw	sp, #4
	Sstm8s_tim3$TIM3_GetCounter$746 ==.
	Sstm8s_tim3$TIM3_GetCounter$747 ==.
	XG$TIM3_GetCounter$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_GetCounter$748 ==.
	Sstm8s_tim3$TIM3_GetPrescaler$749 ==.
;	drivers/src/stm8s_tim3.c: 844: TIM3_Prescaler_TypeDef TIM3_GetPrescaler(void)
;	-----------------------------------------
;	 function TIM3_GetPrescaler
;	-----------------------------------------
_TIM3_GetPrescaler:
	Sstm8s_tim3$TIM3_GetPrescaler$750 ==.
	Sstm8s_tim3$TIM3_GetPrescaler$751 ==.
;	drivers/src/stm8s_tim3.c: 847: return (TIM3_Prescaler_TypeDef)(TIM3->PSCR);
	ld	a, 0x532a
	Sstm8s_tim3$TIM3_GetPrescaler$752 ==.
;	drivers/src/stm8s_tim3.c: 848: }
	Sstm8s_tim3$TIM3_GetPrescaler$753 ==.
	XG$TIM3_GetPrescaler$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_GetPrescaler$754 ==.
	Sstm8s_tim3$TIM3_GetFlagStatus$755 ==.
;	drivers/src/stm8s_tim3.c: 861: FlagStatus TIM3_GetFlagStatus(TIM3_FLAG_TypeDef TIM3_FLAG)
;	-----------------------------------------
;	 function TIM3_GetFlagStatus
;	-----------------------------------------
_TIM3_GetFlagStatus:
	Sstm8s_tim3$TIM3_GetFlagStatus$756 ==.
	push	a
	Sstm8s_tim3$TIM3_GetFlagStatus$757 ==.
	Sstm8s_tim3$TIM3_GetFlagStatus$758 ==.
;	drivers/src/stm8s_tim3.c: 867: assert_param(IS_TIM3_GET_FLAG_OK(TIM3_FLAG));
	ldw	x, (0x04, sp)
	cpw	x, #0x0001
	jreq	00107$
	Sstm8s_tim3$TIM3_GetFlagStatus$759 ==.
	cpw	x, #0x0002
	jreq	00107$
	Sstm8s_tim3$TIM3_GetFlagStatus$760 ==.
	cpw	x, #0x0004
	jreq	00107$
	Sstm8s_tim3$TIM3_GetFlagStatus$761 ==.
	cpw	x, #0x0200
	jreq	00107$
	Sstm8s_tim3$TIM3_GetFlagStatus$762 ==.
	cpw	x, #0x0400
	jreq	00107$
	Sstm8s_tim3$TIM3_GetFlagStatus$763 ==.
	pushw	x
	Sstm8s_tim3$TIM3_GetFlagStatus$764 ==.
	push	#0x63
	Sstm8s_tim3$TIM3_GetFlagStatus$765 ==.
	push	#0x03
	Sstm8s_tim3$TIM3_GetFlagStatus$766 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_GetFlagStatus$767 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_GetFlagStatus$768 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_GetFlagStatus$769 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_GetFlagStatus$770 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_GetFlagStatus$771 ==.
	popw	x
	Sstm8s_tim3$TIM3_GetFlagStatus$772 ==.
00107$:
	Sstm8s_tim3$TIM3_GetFlagStatus$773 ==.
;	drivers/src/stm8s_tim3.c: 869: tim3_flag_l = (uint8_t)(TIM3->SR1 & (uint8_t)TIM3_FLAG);
	ld	a, 0x5322
	ld	(0x01, sp), a
	ld	a, (0x05, sp)
	and	a, (0x01, sp)
	ld	(0x01, sp), a
	Sstm8s_tim3$TIM3_GetFlagStatus$774 ==.
;	drivers/src/stm8s_tim3.c: 870: tim3_flag_h = (uint8_t)((uint16_t)TIM3_FLAG >> 8);
	Sstm8s_tim3$TIM3_GetFlagStatus$775 ==.
;	drivers/src/stm8s_tim3.c: 872: if (((tim3_flag_l) | (uint8_t)(TIM3->SR2 & tim3_flag_h)) != (uint8_t)RESET )
	ld	a, 0x5323
	pushw	x
	Sstm8s_tim3$TIM3_GetFlagStatus$776 ==.
	and	a, (1, sp)
	popw	x
	Sstm8s_tim3$TIM3_GetFlagStatus$777 ==.
	or	a, (0x01, sp)
	jreq	00102$
	Sstm8s_tim3$TIM3_GetFlagStatus$778 ==.
	Sstm8s_tim3$TIM3_GetFlagStatus$779 ==.
;	drivers/src/stm8s_tim3.c: 874: bitstatus = SET;
	ld	a, #0x01
	Sstm8s_tim3$TIM3_GetFlagStatus$780 ==.
	jra	00103$
00102$:
	Sstm8s_tim3$TIM3_GetFlagStatus$781 ==.
	Sstm8s_tim3$TIM3_GetFlagStatus$782 ==.
;	drivers/src/stm8s_tim3.c: 878: bitstatus = RESET;
	clr	a
	Sstm8s_tim3$TIM3_GetFlagStatus$783 ==.
00103$:
	Sstm8s_tim3$TIM3_GetFlagStatus$784 ==.
;	drivers/src/stm8s_tim3.c: 880: return (FlagStatus)bitstatus;
	Sstm8s_tim3$TIM3_GetFlagStatus$785 ==.
;	drivers/src/stm8s_tim3.c: 881: }
	addw	sp, #1
	Sstm8s_tim3$TIM3_GetFlagStatus$786 ==.
	Sstm8s_tim3$TIM3_GetFlagStatus$787 ==.
	XG$TIM3_GetFlagStatus$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_GetFlagStatus$788 ==.
	Sstm8s_tim3$TIM3_ClearFlag$789 ==.
;	drivers/src/stm8s_tim3.c: 894: void TIM3_ClearFlag(TIM3_FLAG_TypeDef TIM3_FLAG)
;	-----------------------------------------
;	 function TIM3_ClearFlag
;	-----------------------------------------
_TIM3_ClearFlag:
	Sstm8s_tim3$TIM3_ClearFlag$790 ==.
	sub	sp, #4
	Sstm8s_tim3$TIM3_ClearFlag$791 ==.
	Sstm8s_tim3$TIM3_ClearFlag$792 ==.
;	drivers/src/stm8s_tim3.c: 897: assert_param(IS_TIM3_CLEAR_FLAG_OK(TIM3_FLAG));
	ldw	x, (0x07, sp)
	ldw	(0x01, sp), x
	ld	a, xl
	and	a, #0xf8
	ld	(0x04, sp), a
	ld	a, xh
	and	a, #0xf9
	ld	(0x03, sp), a
	ldw	y, (0x03, sp)
	jrne	00103$
	ldw	y, (0x01, sp)
	jrne	00104$
00103$:
	pushw	x
	Sstm8s_tim3$TIM3_ClearFlag$793 ==.
	push	#0x81
	Sstm8s_tim3$TIM3_ClearFlag$794 ==.
	push	#0x03
	Sstm8s_tim3$TIM3_ClearFlag$795 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_ClearFlag$796 ==.
	push	#0x00
	Sstm8s_tim3$TIM3_ClearFlag$797 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_ClearFlag$798 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_ClearFlag$799 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_ClearFlag$800 ==.
	popw	x
	Sstm8s_tim3$TIM3_ClearFlag$801 ==.
00104$:
	Sstm8s_tim3$TIM3_ClearFlag$802 ==.
;	drivers/src/stm8s_tim3.c: 900: TIM3->SR1 = (uint8_t)(~((uint8_t)(TIM3_FLAG)));
	ld	a, xl
	cpl	a
	ld	0x5322, a
	Sstm8s_tim3$TIM3_ClearFlag$803 ==.
;	drivers/src/stm8s_tim3.c: 901: TIM3->SR2 = (uint8_t)(~((uint8_t)((uint16_t)TIM3_FLAG >> 8)));
	ld	a, (0x01, sp)
	cpl	a
	ld	0x5323, a
	Sstm8s_tim3$TIM3_ClearFlag$804 ==.
;	drivers/src/stm8s_tim3.c: 902: }
	addw	sp, #4
	Sstm8s_tim3$TIM3_ClearFlag$805 ==.
	Sstm8s_tim3$TIM3_ClearFlag$806 ==.
	XG$TIM3_ClearFlag$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_ClearFlag$807 ==.
	Sstm8s_tim3$TIM3_GetITStatus$808 ==.
;	drivers/src/stm8s_tim3.c: 913: ITStatus TIM3_GetITStatus(TIM3_IT_TypeDef TIM3_IT)
;	-----------------------------------------
;	 function TIM3_GetITStatus
;	-----------------------------------------
_TIM3_GetITStatus:
	Sstm8s_tim3$TIM3_GetITStatus$809 ==.
	push	a
	Sstm8s_tim3$TIM3_GetITStatus$810 ==.
	Sstm8s_tim3$TIM3_GetITStatus$811 ==.
;	drivers/src/stm8s_tim3.c: 919: assert_param(IS_TIM3_GET_IT_OK(TIM3_IT));
	ld	a, (0x04, sp)
	dec	a
	jreq	00108$
	Sstm8s_tim3$TIM3_GetITStatus$812 ==.
	ld	a, (0x04, sp)
	cp	a, #0x02
	jreq	00108$
	Sstm8s_tim3$TIM3_GetITStatus$813 ==.
	ld	a, (0x04, sp)
	cp	a, #0x04
	jreq	00108$
	Sstm8s_tim3$TIM3_GetITStatus$814 ==.
	push	#0x97
	Sstm8s_tim3$TIM3_GetITStatus$815 ==.
	push	#0x03
	Sstm8s_tim3$TIM3_GetITStatus$816 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_GetITStatus$817 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_GetITStatus$818 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_GetITStatus$819 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_GetITStatus$820 ==.
00108$:
	Sstm8s_tim3$TIM3_GetITStatus$821 ==.
;	drivers/src/stm8s_tim3.c: 921: TIM3_itStatus = (uint8_t)(TIM3->SR1 & TIM3_IT);
	ld	a, 0x5322
	and	a, (0x04, sp)
	ld	(0x01, sp), a
	Sstm8s_tim3$TIM3_GetITStatus$822 ==.
;	drivers/src/stm8s_tim3.c: 923: TIM3_itEnable = (uint8_t)(TIM3->IER & TIM3_IT);
	ld	a, 0x5321
	and	a, (0x04, sp)
	Sstm8s_tim3$TIM3_GetITStatus$823 ==.
;	drivers/src/stm8s_tim3.c: 925: if ((TIM3_itStatus != (uint8_t)RESET ) && (TIM3_itEnable != (uint8_t)RESET ))
	tnz	(0x01, sp)
	jreq	00102$
	tnz	a
	jreq	00102$
	Sstm8s_tim3$TIM3_GetITStatus$824 ==.
	Sstm8s_tim3$TIM3_GetITStatus$825 ==.
;	drivers/src/stm8s_tim3.c: 927: bitstatus = SET;
	ld	a, #0x01
	Sstm8s_tim3$TIM3_GetITStatus$826 ==.
	jra	00103$
00102$:
	Sstm8s_tim3$TIM3_GetITStatus$827 ==.
	Sstm8s_tim3$TIM3_GetITStatus$828 ==.
;	drivers/src/stm8s_tim3.c: 931: bitstatus = RESET;
	clr	a
	Sstm8s_tim3$TIM3_GetITStatus$829 ==.
00103$:
	Sstm8s_tim3$TIM3_GetITStatus$830 ==.
;	drivers/src/stm8s_tim3.c: 933: return (ITStatus)(bitstatus);
	Sstm8s_tim3$TIM3_GetITStatus$831 ==.
;	drivers/src/stm8s_tim3.c: 934: }
	addw	sp, #1
	Sstm8s_tim3$TIM3_GetITStatus$832 ==.
	Sstm8s_tim3$TIM3_GetITStatus$833 ==.
	XG$TIM3_GetITStatus$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_GetITStatus$834 ==.
	Sstm8s_tim3$TIM3_ClearITPendingBit$835 ==.
;	drivers/src/stm8s_tim3.c: 945: void TIM3_ClearITPendingBit(TIM3_IT_TypeDef TIM3_IT)
;	-----------------------------------------
;	 function TIM3_ClearITPendingBit
;	-----------------------------------------
_TIM3_ClearITPendingBit:
	Sstm8s_tim3$TIM3_ClearITPendingBit$836 ==.
	Sstm8s_tim3$TIM3_ClearITPendingBit$837 ==.
;	drivers/src/stm8s_tim3.c: 948: assert_param(IS_TIM3_IT_OK(TIM3_IT));
	tnz	(0x03, sp)
	jreq	00103$
	ld	a, (0x03, sp)
	cp	a, #0x07
	jrule	00104$
00103$:
	push	#0xb4
	Sstm8s_tim3$TIM3_ClearITPendingBit$838 ==.
	push	#0x03
	Sstm8s_tim3$TIM3_ClearITPendingBit$839 ==.
	clrw	x
	pushw	x
	Sstm8s_tim3$TIM3_ClearITPendingBit$840 ==.
	push	#<(___str_0+0)
	Sstm8s_tim3$TIM3_ClearITPendingBit$841 ==.
	push	#((___str_0+0) >> 8)
	Sstm8s_tim3$TIM3_ClearITPendingBit$842 ==.
	call	_assert_failed
	addw	sp, #6
	Sstm8s_tim3$TIM3_ClearITPendingBit$843 ==.
00104$:
	Sstm8s_tim3$TIM3_ClearITPendingBit$844 ==.
;	drivers/src/stm8s_tim3.c: 951: TIM3->SR1 = (uint8_t)(~TIM3_IT);
	ld	a, (0x03, sp)
	cpl	a
	ld	0x5322, a
	Sstm8s_tim3$TIM3_ClearITPendingBit$845 ==.
;	drivers/src/stm8s_tim3.c: 952: }
	Sstm8s_tim3$TIM3_ClearITPendingBit$846 ==.
	XG$TIM3_ClearITPendingBit$0$0 ==.
	ret
	Sstm8s_tim3$TIM3_ClearITPendingBit$847 ==.
	Sstm8s_tim3$TI1_Config$848 ==.
;	drivers/src/stm8s_tim3.c: 970: static void TI1_Config(uint8_t TIM3_ICPolarity,
;	-----------------------------------------
;	 function TI1_Config
;	-----------------------------------------
_TI1_Config:
	Sstm8s_tim3$TI1_Config$849 ==.
	push	a
	Sstm8s_tim3$TI1_Config$850 ==.
	Sstm8s_tim3$TI1_Config$851 ==.
;	drivers/src/stm8s_tim3.c: 975: TIM3->CCER1 &= (uint8_t)(~TIM3_CCER1_CC1E);
	bres	21287, #0
	Sstm8s_tim3$TI1_Config$852 ==.
;	drivers/src/stm8s_tim3.c: 978: TIM3->CCMR1 = (uint8_t)((uint8_t)(TIM3->CCMR1 & (uint8_t)(~( TIM3_CCMR_CCxS | TIM3_CCMR_ICxF))) | (uint8_t)(( (TIM3_ICSelection)) | ((uint8_t)( TIM3_ICFilter << 4))));
	ld	a, 0x5325
	and	a, #0x0c
	ld	(0x01, sp), a
	ld	a, (0x06, sp)
	swap	a
	and	a, #0xf0
	or	a, (0x05, sp)
	or	a, (0x01, sp)
	ld	0x5325, a
	Sstm8s_tim3$TI1_Config$853 ==.
;	drivers/src/stm8s_tim3.c: 975: TIM3->CCER1 &= (uint8_t)(~TIM3_CCER1_CC1E);
	ld	a, 0x5327
	Sstm8s_tim3$TI1_Config$854 ==.
;	drivers/src/stm8s_tim3.c: 981: if (TIM3_ICPolarity != TIM3_ICPOLARITY_RISING)
	tnz	(0x04, sp)
	jreq	00102$
	Sstm8s_tim3$TI1_Config$855 ==.
	Sstm8s_tim3$TI1_Config$856 ==.
;	drivers/src/stm8s_tim3.c: 983: TIM3->CCER1 |= TIM3_CCER1_CC1P;
	or	a, #0x02
	ld	0x5327, a
	Sstm8s_tim3$TI1_Config$857 ==.
	jra	00103$
00102$:
	Sstm8s_tim3$TI1_Config$858 ==.
	Sstm8s_tim3$TI1_Config$859 ==.
;	drivers/src/stm8s_tim3.c: 987: TIM3->CCER1 &= (uint8_t)(~TIM3_CCER1_CC1P);
	and	a, #0xfd
	ld	0x5327, a
	Sstm8s_tim3$TI1_Config$860 ==.
00103$:
	Sstm8s_tim3$TI1_Config$861 ==.
;	drivers/src/stm8s_tim3.c: 990: TIM3->CCER1 |= TIM3_CCER1_CC1E;
	bset	21287, #0
	Sstm8s_tim3$TI1_Config$862 ==.
;	drivers/src/stm8s_tim3.c: 991: }
	pop	a
	Sstm8s_tim3$TI1_Config$863 ==.
	Sstm8s_tim3$TI1_Config$864 ==.
	XFstm8s_tim3$TI1_Config$0$0 ==.
	ret
	Sstm8s_tim3$TI1_Config$865 ==.
	Sstm8s_tim3$TI2_Config$866 ==.
;	drivers/src/stm8s_tim3.c: 1009: static void TI2_Config(uint8_t TIM3_ICPolarity,
;	-----------------------------------------
;	 function TI2_Config
;	-----------------------------------------
_TI2_Config:
	Sstm8s_tim3$TI2_Config$867 ==.
	push	a
	Sstm8s_tim3$TI2_Config$868 ==.
	Sstm8s_tim3$TI2_Config$869 ==.
;	drivers/src/stm8s_tim3.c: 1014: TIM3->CCER1 &=  (uint8_t)(~TIM3_CCER1_CC2E);
	bres	21287, #4
	Sstm8s_tim3$TI2_Config$870 ==.
;	drivers/src/stm8s_tim3.c: 1017: TIM3->CCMR2 = (uint8_t)((uint8_t)(TIM3->CCMR2 & (uint8_t)(~( TIM3_CCMR_CCxS |
	ld	a, 0x5326
	and	a, #0x0c
	ld	(0x01, sp), a
	Sstm8s_tim3$TI2_Config$871 ==.
;	drivers/src/stm8s_tim3.c: 1019: ((uint8_t)( TIM3_ICFilter << 4))));
	ld	a, (0x06, sp)
	swap	a
	and	a, #0xf0
	or	a, (0x05, sp)
	or	a, (0x01, sp)
	ld	0x5326, a
	Sstm8s_tim3$TI2_Config$872 ==.
;	drivers/src/stm8s_tim3.c: 1014: TIM3->CCER1 &=  (uint8_t)(~TIM3_CCER1_CC2E);
	ld	a, 0x5327
	Sstm8s_tim3$TI2_Config$873 ==.
;	drivers/src/stm8s_tim3.c: 1022: if (TIM3_ICPolarity != TIM3_ICPOLARITY_RISING)
	tnz	(0x04, sp)
	jreq	00102$
	Sstm8s_tim3$TI2_Config$874 ==.
	Sstm8s_tim3$TI2_Config$875 ==.
;	drivers/src/stm8s_tim3.c: 1024: TIM3->CCER1 |= TIM3_CCER1_CC2P;
	or	a, #0x20
	ld	0x5327, a
	Sstm8s_tim3$TI2_Config$876 ==.
	jra	00103$
00102$:
	Sstm8s_tim3$TI2_Config$877 ==.
	Sstm8s_tim3$TI2_Config$878 ==.
;	drivers/src/stm8s_tim3.c: 1028: TIM3->CCER1 &= (uint8_t)(~TIM3_CCER1_CC2P);
	and	a, #0xdf
	ld	0x5327, a
	Sstm8s_tim3$TI2_Config$879 ==.
00103$:
	Sstm8s_tim3$TI2_Config$880 ==.
;	drivers/src/stm8s_tim3.c: 1032: TIM3->CCER1 |= TIM3_CCER1_CC2E;
	bset	21287, #4
	Sstm8s_tim3$TI2_Config$881 ==.
;	drivers/src/stm8s_tim3.c: 1033: }
	pop	a
	Sstm8s_tim3$TI2_Config$882 ==.
	Sstm8s_tim3$TI2_Config$883 ==.
	XFstm8s_tim3$TI2_Config$0$0 ==.
	ret
	Sstm8s_tim3$TI2_Config$884 ==.
	.area CODE
	.area CONST
Fstm8s_tim3$__str_0$0_0$0 == .
	.area CONST
___str_0:
	.ascii "drivers/src/stm8s_tim3.c"
	.db 0x00
	.area CODE
	.area INITIALIZER
	.area CABS (ABS)

	.area .debug_line (NOLOAD)
	.dw	0,Ldebug_line_end-Ldebug_line_start
Ldebug_line_start:
	.dw	2
	.dw	0,Ldebug_line_stmt-6-Ldebug_line_start
	.db	1
	.db	1
	.db	-5
	.db	15
	.db	10
	.db	0
	.db	1
	.db	1
	.db	1
	.db	1
	.db	0
	.db	0
	.db	0
	.db	1
	.ascii "C:\Program Files\SDCC\bin\..\include\stm8"
	.db	0
	.ascii "C:\Program Files\SDCC\bin\..\include"
	.db	0
	.db	0
	.ascii "drivers/src/stm8s_tim3.c"
	.db	0
	.uleb128	0
	.uleb128	0
	.uleb128	0
	.db	0
Ldebug_line_stmt:
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_DeInit$0)
	.db	3
	.sleb128	50
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$2-Sstm8s_tim3$TIM3_DeInit$0
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$3-Sstm8s_tim3$TIM3_DeInit$2
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$4-Sstm8s_tim3$TIM3_DeInit$3
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$5-Sstm8s_tim3$TIM3_DeInit$4
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$6-Sstm8s_tim3$TIM3_DeInit$5
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$7-Sstm8s_tim3$TIM3_DeInit$6
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$8-Sstm8s_tim3$TIM3_DeInit$7
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$9-Sstm8s_tim3$TIM3_DeInit$8
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$10-Sstm8s_tim3$TIM3_DeInit$9
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$11-Sstm8s_tim3$TIM3_DeInit$10
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$12-Sstm8s_tim3$TIM3_DeInit$11
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$13-Sstm8s_tim3$TIM3_DeInit$12
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$14-Sstm8s_tim3$TIM3_DeInit$13
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$15-Sstm8s_tim3$TIM3_DeInit$14
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$16-Sstm8s_tim3$TIM3_DeInit$15
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$17-Sstm8s_tim3$TIM3_DeInit$16
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$18-Sstm8s_tim3$TIM3_DeInit$17
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_DeInit$19-Sstm8s_tim3$TIM3_DeInit$18
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_DeInit$20-Sstm8s_tim3$TIM3_DeInit$19
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_TimeBaseInit$22)
	.db	3
	.sleb128	81
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_TimeBaseInit$24-Sstm8s_tim3$TIM3_TimeBaseInit$22
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_TimeBaseInit$25-Sstm8s_tim3$TIM3_TimeBaseInit$24
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_TimeBaseInit$26-Sstm8s_tim3$TIM3_TimeBaseInit$25
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_TimeBaseInit$27-Sstm8s_tim3$TIM3_TimeBaseInit$26
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_TimeBaseInit$28-Sstm8s_tim3$TIM3_TimeBaseInit$27
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$30)
	.db	3
	.sleb128	99
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1Init$33-Sstm8s_tim3$TIM3_OC1Init$30
	.db	3
	.sleb128	6
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1Init$45-Sstm8s_tim3$TIM3_OC1Init$33
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1Init$53-Sstm8s_tim3$TIM3_OC1Init$45
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1Init$61-Sstm8s_tim3$TIM3_OC1Init$53
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1Init$62-Sstm8s_tim3$TIM3_OC1Init$61
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1Init$63-Sstm8s_tim3$TIM3_OC1Init$62
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1Init$64-Sstm8s_tim3$TIM3_OC1Init$63
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1Init$65-Sstm8s_tim3$TIM3_OC1Init$64
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1Init$66-Sstm8s_tim3$TIM3_OC1Init$65
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_OC1Init$68-Sstm8s_tim3$TIM3_OC1Init$66
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$70)
	.db	3
	.sleb128	130
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2Init$73-Sstm8s_tim3$TIM3_OC2Init$70
	.db	3
	.sleb128	6
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2Init$85-Sstm8s_tim3$TIM3_OC2Init$73
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2Init$93-Sstm8s_tim3$TIM3_OC2Init$85
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2Init$101-Sstm8s_tim3$TIM3_OC2Init$93
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2Init$102-Sstm8s_tim3$TIM3_OC2Init$101
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2Init$103-Sstm8s_tim3$TIM3_OC2Init$102
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2Init$104-Sstm8s_tim3$TIM3_OC2Init$103
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2Init$105-Sstm8s_tim3$TIM3_OC2Init$104
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2Init$106-Sstm8s_tim3$TIM3_OC2Init$105
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_OC2Init$108-Sstm8s_tim3$TIM3_OC2Init$106
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$110)
	.db	3
	.sleb128	165
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ICInit$113-Sstm8s_tim3$TIM3_ICInit$110
	.db	3
	.sleb128	7
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ICInit$121-Sstm8s_tim3$TIM3_ICInit$113
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ICInit$129-Sstm8s_tim3$TIM3_ICInit$121
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ICInit$139-Sstm8s_tim3$TIM3_ICInit$129
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ICInit$149-Sstm8s_tim3$TIM3_ICInit$139
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ICInit$156-Sstm8s_tim3$TIM3_ICInit$149
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ICInit$158-Sstm8s_tim3$TIM3_ICInit$156
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ICInit$163-Sstm8s_tim3$TIM3_ICInit$158
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ICInit$168-Sstm8s_tim3$TIM3_ICInit$163
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ICInit$173-Sstm8s_tim3$TIM3_ICInit$168
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ICInit$177-Sstm8s_tim3$TIM3_ICInit$173
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_ICInit$179-Sstm8s_tim3$TIM3_ICInit$177
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$181)
	.db	3
	.sleb128	209
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$184-Sstm8s_tim3$TIM3_PWMIConfig$181
	.db	3
	.sleb128	10
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$192-Sstm8s_tim3$TIM3_PWMIConfig$184
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$200-Sstm8s_tim3$TIM3_PWMIConfig$192
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$210-Sstm8s_tim3$TIM3_PWMIConfig$200
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$220-Sstm8s_tim3$TIM3_PWMIConfig$210
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$222-Sstm8s_tim3$TIM3_PWMIConfig$220
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$225-Sstm8s_tim3$TIM3_PWMIConfig$222
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$227-Sstm8s_tim3$TIM3_PWMIConfig$225
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$229-Sstm8s_tim3$TIM3_PWMIConfig$227
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$232-Sstm8s_tim3$TIM3_PWMIConfig$229
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$234-Sstm8s_tim3$TIM3_PWMIConfig$232
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$236-Sstm8s_tim3$TIM3_PWMIConfig$234
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$241-Sstm8s_tim3$TIM3_PWMIConfig$236
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$244-Sstm8s_tim3$TIM3_PWMIConfig$241
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$249-Sstm8s_tim3$TIM3_PWMIConfig$244
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$254-Sstm8s_tim3$TIM3_PWMIConfig$249
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$259-Sstm8s_tim3$TIM3_PWMIConfig$254
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$262-Sstm8s_tim3$TIM3_PWMIConfig$259
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$267-Sstm8s_tim3$TIM3_PWMIConfig$262
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PWMIConfig$271-Sstm8s_tim3$TIM3_PWMIConfig$267
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_PWMIConfig$273-Sstm8s_tim3$TIM3_PWMIConfig$271
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$275)
	.db	3
	.sleb128	282
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_Cmd$277-Sstm8s_tim3$TIM3_Cmd$275
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_Cmd$285-Sstm8s_tim3$TIM3_Cmd$277
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_Cmd$286-Sstm8s_tim3$TIM3_Cmd$285
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_Cmd$288-Sstm8s_tim3$TIM3_Cmd$286
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_Cmd$291-Sstm8s_tim3$TIM3_Cmd$288
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_Cmd$293-Sstm8s_tim3$TIM3_Cmd$291
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_Cmd$294-Sstm8s_tim3$TIM3_Cmd$293
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$296)
	.db	3
	.sleb128	310
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ITConfig$299-Sstm8s_tim3$TIM3_ITConfig$296
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ITConfig$306-Sstm8s_tim3$TIM3_ITConfig$299
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ITConfig$314-Sstm8s_tim3$TIM3_ITConfig$306
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ITConfig$315-Sstm8s_tim3$TIM3_ITConfig$314
	.db	3
	.sleb128	-3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ITConfig$317-Sstm8s_tim3$TIM3_ITConfig$315
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ITConfig$320-Sstm8s_tim3$TIM3_ITConfig$317
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ITConfig$324-Sstm8s_tim3$TIM3_ITConfig$320
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_ITConfig$326-Sstm8s_tim3$TIM3_ITConfig$324
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$328)
	.db	3
	.sleb128	334
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_UpdateDisableConfig$330-Sstm8s_tim3$TIM3_UpdateDisableConfig$328
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_UpdateDisableConfig$338-Sstm8s_tim3$TIM3_UpdateDisableConfig$330
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_UpdateDisableConfig$339-Sstm8s_tim3$TIM3_UpdateDisableConfig$338
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_UpdateDisableConfig$341-Sstm8s_tim3$TIM3_UpdateDisableConfig$339
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_UpdateDisableConfig$344-Sstm8s_tim3$TIM3_UpdateDisableConfig$341
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_UpdateDisableConfig$346-Sstm8s_tim3$TIM3_UpdateDisableConfig$344
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_UpdateDisableConfig$347-Sstm8s_tim3$TIM3_UpdateDisableConfig$346
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$349)
	.db	3
	.sleb128	358
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_UpdateRequestConfig$351-Sstm8s_tim3$TIM3_UpdateRequestConfig$349
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_UpdateRequestConfig$359-Sstm8s_tim3$TIM3_UpdateRequestConfig$351
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_UpdateRequestConfig$360-Sstm8s_tim3$TIM3_UpdateRequestConfig$359
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_UpdateRequestConfig$362-Sstm8s_tim3$TIM3_UpdateRequestConfig$360
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_UpdateRequestConfig$365-Sstm8s_tim3$TIM3_UpdateRequestConfig$362
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_UpdateRequestConfig$367-Sstm8s_tim3$TIM3_UpdateRequestConfig$365
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_UpdateRequestConfig$368-Sstm8s_tim3$TIM3_UpdateRequestConfig$367
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$370)
	.db	3
	.sleb128	382
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SelectOnePulseMode$372-Sstm8s_tim3$TIM3_SelectOnePulseMode$370
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SelectOnePulseMode$380-Sstm8s_tim3$TIM3_SelectOnePulseMode$372
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SelectOnePulseMode$381-Sstm8s_tim3$TIM3_SelectOnePulseMode$380
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SelectOnePulseMode$383-Sstm8s_tim3$TIM3_SelectOnePulseMode$381
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SelectOnePulseMode$386-Sstm8s_tim3$TIM3_SelectOnePulseMode$383
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SelectOnePulseMode$388-Sstm8s_tim3$TIM3_SelectOnePulseMode$386
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_SelectOnePulseMode$389-Sstm8s_tim3$TIM3_SelectOnePulseMode$388
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$391)
	.db	3
	.sleb128	426
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PrescalerConfig$393-Sstm8s_tim3$TIM3_PrescalerConfig$391
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PrescalerConfig$401-Sstm8s_tim3$TIM3_PrescalerConfig$393
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PrescalerConfig$423-Sstm8s_tim3$TIM3_PrescalerConfig$401
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PrescalerConfig$424-Sstm8s_tim3$TIM3_PrescalerConfig$423
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_PrescalerConfig$425-Sstm8s_tim3$TIM3_PrescalerConfig$424
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_PrescalerConfig$426-Sstm8s_tim3$TIM3_PrescalerConfig$425
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$428)
	.db	3
	.sleb128	449
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ForcedOC1Config$430-Sstm8s_tim3$TIM3_ForcedOC1Config$428
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ForcedOC1Config$439-Sstm8s_tim3$TIM3_ForcedOC1Config$430
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ForcedOC1Config$440-Sstm8s_tim3$TIM3_ForcedOC1Config$439
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_ForcedOC1Config$441-Sstm8s_tim3$TIM3_ForcedOC1Config$440
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$443)
	.db	3
	.sleb128	467
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ForcedOC2Config$445-Sstm8s_tim3$TIM3_ForcedOC2Config$443
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ForcedOC2Config$454-Sstm8s_tim3$TIM3_ForcedOC2Config$445
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ForcedOC2Config$455-Sstm8s_tim3$TIM3_ForcedOC2Config$454
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_ForcedOC2Config$456-Sstm8s_tim3$TIM3_ForcedOC2Config$455
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$458)
	.db	3
	.sleb128	482
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ARRPreloadConfig$460-Sstm8s_tim3$TIM3_ARRPreloadConfig$458
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ARRPreloadConfig$468-Sstm8s_tim3$TIM3_ARRPreloadConfig$460
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ARRPreloadConfig$469-Sstm8s_tim3$TIM3_ARRPreloadConfig$468
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ARRPreloadConfig$471-Sstm8s_tim3$TIM3_ARRPreloadConfig$469
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ARRPreloadConfig$474-Sstm8s_tim3$TIM3_ARRPreloadConfig$471
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ARRPreloadConfig$476-Sstm8s_tim3$TIM3_ARRPreloadConfig$474
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_ARRPreloadConfig$477-Sstm8s_tim3$TIM3_ARRPreloadConfig$476
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$479)
	.db	3
	.sleb128	504
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1PreloadConfig$481-Sstm8s_tim3$TIM3_OC1PreloadConfig$479
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1PreloadConfig$489-Sstm8s_tim3$TIM3_OC1PreloadConfig$481
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1PreloadConfig$490-Sstm8s_tim3$TIM3_OC1PreloadConfig$489
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1PreloadConfig$492-Sstm8s_tim3$TIM3_OC1PreloadConfig$490
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1PreloadConfig$495-Sstm8s_tim3$TIM3_OC1PreloadConfig$492
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1PreloadConfig$497-Sstm8s_tim3$TIM3_OC1PreloadConfig$495
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_OC1PreloadConfig$498-Sstm8s_tim3$TIM3_OC1PreloadConfig$497
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$500)
	.db	3
	.sleb128	526
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2PreloadConfig$502-Sstm8s_tim3$TIM3_OC2PreloadConfig$500
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2PreloadConfig$510-Sstm8s_tim3$TIM3_OC2PreloadConfig$502
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2PreloadConfig$511-Sstm8s_tim3$TIM3_OC2PreloadConfig$510
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2PreloadConfig$513-Sstm8s_tim3$TIM3_OC2PreloadConfig$511
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2PreloadConfig$516-Sstm8s_tim3$TIM3_OC2PreloadConfig$513
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2PreloadConfig$518-Sstm8s_tim3$TIM3_OC2PreloadConfig$516
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_OC2PreloadConfig$519-Sstm8s_tim3$TIM3_OC2PreloadConfig$518
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$521)
	.db	3
	.sleb128	551
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GenerateEvent$523-Sstm8s_tim3$TIM3_GenerateEvent$521
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GenerateEvent$530-Sstm8s_tim3$TIM3_GenerateEvent$523
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GenerateEvent$531-Sstm8s_tim3$TIM3_GenerateEvent$530
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_GenerateEvent$532-Sstm8s_tim3$TIM3_GenerateEvent$531
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$534)
	.db	3
	.sleb128	568
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1PolarityConfig$536-Sstm8s_tim3$TIM3_OC1PolarityConfig$534
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1PolarityConfig$544-Sstm8s_tim3$TIM3_OC1PolarityConfig$536
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1PolarityConfig$545-Sstm8s_tim3$TIM3_OC1PolarityConfig$544
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1PolarityConfig$547-Sstm8s_tim3$TIM3_OC1PolarityConfig$545
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1PolarityConfig$550-Sstm8s_tim3$TIM3_OC1PolarityConfig$547
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC1PolarityConfig$552-Sstm8s_tim3$TIM3_OC1PolarityConfig$550
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_OC1PolarityConfig$553-Sstm8s_tim3$TIM3_OC1PolarityConfig$552
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$555)
	.db	3
	.sleb128	592
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2PolarityConfig$557-Sstm8s_tim3$TIM3_OC2PolarityConfig$555
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2PolarityConfig$565-Sstm8s_tim3$TIM3_OC2PolarityConfig$557
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2PolarityConfig$566-Sstm8s_tim3$TIM3_OC2PolarityConfig$565
	.db	3
	.sleb128	-2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2PolarityConfig$568-Sstm8s_tim3$TIM3_OC2PolarityConfig$566
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2PolarityConfig$571-Sstm8s_tim3$TIM3_OC2PolarityConfig$568
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_OC2PolarityConfig$573-Sstm8s_tim3$TIM3_OC2PolarityConfig$571
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_OC2PolarityConfig$574-Sstm8s_tim3$TIM3_OC2PolarityConfig$573
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$576)
	.db	3
	.sleb128	618
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_CCxCmd$578-Sstm8s_tim3$TIM3_CCxCmd$576
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_CCxCmd$586-Sstm8s_tim3$TIM3_CCxCmd$578
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_CCxCmd$594-Sstm8s_tim3$TIM3_CCxCmd$586
	.db	3
	.sleb128	7
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_CCxCmd$595-Sstm8s_tim3$TIM3_CCxCmd$594
	.db	3
	.sleb128	-5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_CCxCmd$597-Sstm8s_tim3$TIM3_CCxCmd$595
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_CCxCmd$599-Sstm8s_tim3$TIM3_CCxCmd$597
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_CCxCmd$602-Sstm8s_tim3$TIM3_CCxCmd$599
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_CCxCmd$605-Sstm8s_tim3$TIM3_CCxCmd$602
	.db	3
	.sleb128	7
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_CCxCmd$607-Sstm8s_tim3$TIM3_CCxCmd$605
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_CCxCmd$610-Sstm8s_tim3$TIM3_CCxCmd$607
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_CCxCmd$612-Sstm8s_tim3$TIM3_CCxCmd$610
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_CCxCmd$613-Sstm8s_tim3$TIM3_CCxCmd$612
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$615)
	.db	3
	.sleb128	670
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SelectOCxM$617-Sstm8s_tim3$TIM3_SelectOCxM$615
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SelectOCxM$625-Sstm8s_tim3$TIM3_SelectOCxM$617
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SelectOCxM$639-Sstm8s_tim3$TIM3_SelectOCxM$625
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SelectOCxM$640-Sstm8s_tim3$TIM3_SelectOCxM$639
	.db	3
	.sleb128	-3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SelectOCxM$642-Sstm8s_tim3$TIM3_SelectOCxM$640
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SelectOCxM$643-Sstm8s_tim3$TIM3_SelectOCxM$642
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SelectOCxM$646-Sstm8s_tim3$TIM3_SelectOCxM$643
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SelectOCxM$647-Sstm8s_tim3$TIM3_SelectOCxM$646
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SelectOCxM$649-Sstm8s_tim3$TIM3_SelectOCxM$647
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_SelectOCxM$650-Sstm8s_tim3$TIM3_SelectOCxM$649
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_SetCounter$652)
	.db	3
	.sleb128	700
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetCounter$654-Sstm8s_tim3$TIM3_SetCounter$652
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetCounter$655-Sstm8s_tim3$TIM3_SetCounter$654
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetCounter$656-Sstm8s_tim3$TIM3_SetCounter$655
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_SetCounter$657-Sstm8s_tim3$TIM3_SetCounter$656
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_SetAutoreload$659)
	.db	3
	.sleb128	713
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetAutoreload$661-Sstm8s_tim3$TIM3_SetAutoreload$659
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetAutoreload$662-Sstm8s_tim3$TIM3_SetAutoreload$661
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetAutoreload$663-Sstm8s_tim3$TIM3_SetAutoreload$662
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_SetAutoreload$664-Sstm8s_tim3$TIM3_SetAutoreload$663
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_SetCompare1$666)
	.db	3
	.sleb128	726
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetCompare1$668-Sstm8s_tim3$TIM3_SetCompare1$666
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetCompare1$669-Sstm8s_tim3$TIM3_SetCompare1$668
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetCompare1$670-Sstm8s_tim3$TIM3_SetCompare1$669
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_SetCompare1$671-Sstm8s_tim3$TIM3_SetCompare1$670
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_SetCompare2$673)
	.db	3
	.sleb128	739
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetCompare2$675-Sstm8s_tim3$TIM3_SetCompare2$673
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetCompare2$676-Sstm8s_tim3$TIM3_SetCompare2$675
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetCompare2$677-Sstm8s_tim3$TIM3_SetCompare2$676
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_SetCompare2$678-Sstm8s_tim3$TIM3_SetCompare2$677
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$680)
	.db	3
	.sleb128	756
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetIC1Prescaler$682-Sstm8s_tim3$TIM3_SetIC1Prescaler$680
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetIC1Prescaler$692-Sstm8s_tim3$TIM3_SetIC1Prescaler$682
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetIC1Prescaler$693-Sstm8s_tim3$TIM3_SetIC1Prescaler$692
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_SetIC1Prescaler$694-Sstm8s_tim3$TIM3_SetIC1Prescaler$693
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$696)
	.db	3
	.sleb128	775
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetIC2Prescaler$698-Sstm8s_tim3$TIM3_SetIC2Prescaler$696
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetIC2Prescaler$708-Sstm8s_tim3$TIM3_SetIC2Prescaler$698
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_SetIC2Prescaler$709-Sstm8s_tim3$TIM3_SetIC2Prescaler$708
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_SetIC2Prescaler$710-Sstm8s_tim3$TIM3_SetIC2Prescaler$709
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture1$712)
	.db	3
	.sleb128	789
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetCapture1$715-Sstm8s_tim3$TIM3_GetCapture1$712
	.db	3
	.sleb128	6
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetCapture1$716-Sstm8s_tim3$TIM3_GetCapture1$715
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetCapture1$717-Sstm8s_tim3$TIM3_GetCapture1$716
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetCapture1$718-Sstm8s_tim3$TIM3_GetCapture1$717
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetCapture1$721-Sstm8s_tim3$TIM3_GetCapture1$718
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetCapture1$722-Sstm8s_tim3$TIM3_GetCapture1$721
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_GetCapture1$724-Sstm8s_tim3$TIM3_GetCapture1$722
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture2$726)
	.db	3
	.sleb128	809
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetCapture2$729-Sstm8s_tim3$TIM3_GetCapture2$726
	.db	3
	.sleb128	6
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetCapture2$730-Sstm8s_tim3$TIM3_GetCapture2$729
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetCapture2$731-Sstm8s_tim3$TIM3_GetCapture2$730
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetCapture2$732-Sstm8s_tim3$TIM3_GetCapture2$731
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetCapture2$735-Sstm8s_tim3$TIM3_GetCapture2$732
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetCapture2$736-Sstm8s_tim3$TIM3_GetCapture2$735
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_GetCapture2$738-Sstm8s_tim3$TIM3_GetCapture2$736
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_GetCounter$740)
	.db	3
	.sleb128	829
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetCounter$743-Sstm8s_tim3$TIM3_GetCounter$740
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetCounter$744-Sstm8s_tim3$TIM3_GetCounter$743
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetCounter$745-Sstm8s_tim3$TIM3_GetCounter$744
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_GetCounter$747-Sstm8s_tim3$TIM3_GetCounter$745
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_GetPrescaler$749)
	.db	3
	.sleb128	843
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetPrescaler$751-Sstm8s_tim3$TIM3_GetPrescaler$749
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetPrescaler$752-Sstm8s_tim3$TIM3_GetPrescaler$751
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_GetPrescaler$753-Sstm8s_tim3$TIM3_GetPrescaler$752
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$755)
	.db	3
	.sleb128	860
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetFlagStatus$758-Sstm8s_tim3$TIM3_GetFlagStatus$755
	.db	3
	.sleb128	6
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetFlagStatus$773-Sstm8s_tim3$TIM3_GetFlagStatus$758
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetFlagStatus$774-Sstm8s_tim3$TIM3_GetFlagStatus$773
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetFlagStatus$775-Sstm8s_tim3$TIM3_GetFlagStatus$774
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetFlagStatus$779-Sstm8s_tim3$TIM3_GetFlagStatus$775
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetFlagStatus$782-Sstm8s_tim3$TIM3_GetFlagStatus$779
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetFlagStatus$784-Sstm8s_tim3$TIM3_GetFlagStatus$782
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetFlagStatus$785-Sstm8s_tim3$TIM3_GetFlagStatus$784
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_GetFlagStatus$787-Sstm8s_tim3$TIM3_GetFlagStatus$785
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$789)
	.db	3
	.sleb128	893
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ClearFlag$792-Sstm8s_tim3$TIM3_ClearFlag$789
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ClearFlag$802-Sstm8s_tim3$TIM3_ClearFlag$792
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ClearFlag$803-Sstm8s_tim3$TIM3_ClearFlag$802
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ClearFlag$804-Sstm8s_tim3$TIM3_ClearFlag$803
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_ClearFlag$806-Sstm8s_tim3$TIM3_ClearFlag$804
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$808)
	.db	3
	.sleb128	912
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetITStatus$811-Sstm8s_tim3$TIM3_GetITStatus$808
	.db	3
	.sleb128	6
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetITStatus$821-Sstm8s_tim3$TIM3_GetITStatus$811
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetITStatus$822-Sstm8s_tim3$TIM3_GetITStatus$821
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetITStatus$823-Sstm8s_tim3$TIM3_GetITStatus$822
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetITStatus$825-Sstm8s_tim3$TIM3_GetITStatus$823
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetITStatus$828-Sstm8s_tim3$TIM3_GetITStatus$825
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetITStatus$830-Sstm8s_tim3$TIM3_GetITStatus$828
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_GetITStatus$831-Sstm8s_tim3$TIM3_GetITStatus$830
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_GetITStatus$833-Sstm8s_tim3$TIM3_GetITStatus$831
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$835)
	.db	3
	.sleb128	944
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ClearITPendingBit$837-Sstm8s_tim3$TIM3_ClearITPendingBit$835
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ClearITPendingBit$844-Sstm8s_tim3$TIM3_ClearITPendingBit$837
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TIM3_ClearITPendingBit$845-Sstm8s_tim3$TIM3_ClearITPendingBit$844
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TIM3_ClearITPendingBit$846-Sstm8s_tim3$TIM3_ClearITPendingBit$845
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TI1_Config$848)
	.db	3
	.sleb128	969
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TI1_Config$851-Sstm8s_tim3$TI1_Config$848
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TI1_Config$852-Sstm8s_tim3$TI1_Config$851
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TI1_Config$853-Sstm8s_tim3$TI1_Config$852
	.db	3
	.sleb128	-3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TI1_Config$854-Sstm8s_tim3$TI1_Config$853
	.db	3
	.sleb128	6
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TI1_Config$856-Sstm8s_tim3$TI1_Config$854
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TI1_Config$859-Sstm8s_tim3$TI1_Config$856
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TI1_Config$861-Sstm8s_tim3$TI1_Config$859
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TI1_Config$862-Sstm8s_tim3$TI1_Config$861
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TI1_Config$864-Sstm8s_tim3$TI1_Config$862
	.db	0
	.uleb128	1
	.db	1
	.db	0
	.uleb128	5
	.db	2
	.dw	0,(Sstm8s_tim3$TI2_Config$866)
	.db	3
	.sleb128	1008
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TI2_Config$869-Sstm8s_tim3$TI2_Config$866
	.db	3
	.sleb128	5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TI2_Config$870-Sstm8s_tim3$TI2_Config$869
	.db	3
	.sleb128	3
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TI2_Config$871-Sstm8s_tim3$TI2_Config$870
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TI2_Config$872-Sstm8s_tim3$TI2_Config$871
	.db	3
	.sleb128	-5
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TI2_Config$873-Sstm8s_tim3$TI2_Config$872
	.db	3
	.sleb128	8
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TI2_Config$875-Sstm8s_tim3$TI2_Config$873
	.db	3
	.sleb128	2
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TI2_Config$878-Sstm8s_tim3$TI2_Config$875
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TI2_Config$880-Sstm8s_tim3$TI2_Config$878
	.db	3
	.sleb128	4
	.db	1
	.db	9
	.dw	Sstm8s_tim3$TI2_Config$881-Sstm8s_tim3$TI2_Config$880
	.db	3
	.sleb128	1
	.db	1
	.db	9
	.dw	1+Sstm8s_tim3$TI2_Config$883-Sstm8s_tim3$TI2_Config$881
	.db	0
	.uleb128	1
	.db	1
Ldebug_line_end:

	.area .debug_loc (NOLOAD)
Ldebug_loc_start:
	.dw	0,(Sstm8s_tim3$TI2_Config$882)
	.dw	0,(Sstm8s_tim3$TI2_Config$884)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TI2_Config$868)
	.dw	0,(Sstm8s_tim3$TI2_Config$882)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TI2_Config$867)
	.dw	0,(Sstm8s_tim3$TI2_Config$868)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TI1_Config$863)
	.dw	0,(Sstm8s_tim3$TI1_Config$865)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TI1_Config$850)
	.dw	0,(Sstm8s_tim3$TI1_Config$863)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TI1_Config$849)
	.dw	0,(Sstm8s_tim3$TI1_Config$850)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$843)
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$847)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$842)
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$843)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$841)
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$842)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$840)
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$841)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$839)
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$840)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$838)
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$839)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$836)
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$838)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$832)
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$834)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$820)
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$832)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$819)
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$820)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$818)
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$819)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$817)
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$818)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$816)
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$817)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$815)
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$816)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$814)
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$815)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$813)
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$814)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$812)
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$813)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$810)
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$812)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$809)
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$810)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$805)
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$807)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$801)
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$805)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$800)
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$801)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$799)
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$800)
	.dw	2
	.db	120
	.sleb128	13
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$798)
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$799)
	.dw	2
	.db	120
	.sleb128	12
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$797)
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$798)
	.dw	2
	.db	120
	.sleb128	11
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$796)
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$797)
	.dw	2
	.db	120
	.sleb128	10
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$795)
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$796)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$794)
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$795)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$793)
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$794)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$791)
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$793)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$790)
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$791)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$786)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$788)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$777)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$786)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$776)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$777)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$772)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$776)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$771)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$772)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$770)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$771)
	.dw	2
	.db	120
	.sleb128	10
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$769)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$770)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$768)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$769)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$767)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$768)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$766)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$767)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$765)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$766)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$764)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$765)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$763)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$764)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$762)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$763)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$761)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$762)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$760)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$761)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$759)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$760)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$757)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$759)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$756)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$757)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_GetPrescaler$750)
	.dw	0,(Sstm8s_tim3$TIM3_GetPrescaler$754)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_GetCounter$746)
	.dw	0,(Sstm8s_tim3$TIM3_GetCounter$748)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_GetCounter$742)
	.dw	0,(Sstm8s_tim3$TIM3_GetCounter$746)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_GetCounter$741)
	.dw	0,(Sstm8s_tim3$TIM3_GetCounter$742)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture2$737)
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture2$739)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture2$734)
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture2$737)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture2$733)
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture2$734)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture2$728)
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture2$733)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture2$727)
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture2$728)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture1$723)
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture1$725)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture1$720)
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture1$723)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture1$719)
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture1$720)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture1$714)
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture1$719)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture1$713)
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture1$714)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$707)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$711)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$706)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$707)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$705)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$706)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$704)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$705)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$703)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$704)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$702)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$703)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$701)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$702)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$700)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$701)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$699)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$700)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$697)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$699)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$691)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$695)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$690)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$691)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$689)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$690)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$688)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$689)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$687)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$688)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$686)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$687)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$685)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$686)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$684)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$685)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$683)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$684)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$681)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$683)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_SetCompare2$674)
	.dw	0,(Sstm8s_tim3$TIM3_SetCompare2$679)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_SetCompare1$667)
	.dw	0,(Sstm8s_tim3$TIM3_SetCompare1$672)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_SetAutoreload$660)
	.dw	0,(Sstm8s_tim3$TIM3_SetAutoreload$665)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_SetCounter$653)
	.dw	0,(Sstm8s_tim3$TIM3_SetCounter$658)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$638)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$651)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$637)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$638)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$636)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$637)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$635)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$636)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$634)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$635)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$633)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$634)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$632)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$633)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$631)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$632)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$630)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$631)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$629)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$630)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$628)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$629)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$627)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$628)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$626)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$627)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$624)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$626)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$623)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$624)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$622)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$623)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$621)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$622)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$620)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$621)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$619)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$620)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$618)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$619)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$616)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$618)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$593)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$614)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$592)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$593)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$591)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$592)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$590)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$591)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$589)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$590)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$588)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$589)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$587)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$588)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$585)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$587)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$584)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$585)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$583)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$584)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$582)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$583)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$581)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$582)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$580)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$581)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$579)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$580)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$577)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$579)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$564)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$575)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$563)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$564)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$562)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$563)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$561)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$562)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$560)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$561)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$559)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$560)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$558)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$559)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$556)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$558)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$543)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$554)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$542)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$543)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$541)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$542)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$540)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$541)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$539)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$540)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$538)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$539)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$537)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$538)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$535)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$537)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$529)
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$533)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$528)
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$529)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$527)
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$528)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$526)
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$527)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$525)
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$526)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$524)
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$525)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$522)
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$524)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$509)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$520)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$508)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$509)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$507)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$508)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$506)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$507)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$505)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$506)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$504)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$505)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$503)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$504)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$501)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$503)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$488)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$499)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$487)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$488)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$486)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$487)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$485)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$486)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$484)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$485)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$483)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$484)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$482)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$483)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$480)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$482)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$467)
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$478)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$466)
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$467)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$465)
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$466)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$464)
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$465)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$463)
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$464)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$462)
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$463)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$461)
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$462)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$459)
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$461)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$453)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$457)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$452)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$453)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$451)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$452)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$450)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$451)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$449)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$450)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$448)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$449)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$447)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$448)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$446)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$447)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$444)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$446)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$438)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$442)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$437)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$438)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$436)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$437)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$435)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$436)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$434)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$435)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$433)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$434)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$432)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$433)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$431)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$432)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$429)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$431)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$422)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$427)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$421)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$422)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$420)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$421)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$419)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$420)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$418)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$419)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$417)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$418)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$416)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$417)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$415)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$416)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$414)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$415)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$413)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$414)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$412)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$413)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$411)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$412)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$410)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$411)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$409)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$410)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$408)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$409)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$407)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$408)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$406)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$407)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$405)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$406)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$404)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$405)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$403)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$404)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$402)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$403)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$400)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$402)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$399)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$400)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$398)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$399)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$397)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$398)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$396)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$397)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$395)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$396)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$394)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$395)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$392)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$394)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$379)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$390)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$378)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$379)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$377)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$378)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$376)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$377)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$375)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$376)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$374)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$375)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$373)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$374)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$371)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$373)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$358)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$369)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$357)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$358)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$356)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$357)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$355)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$356)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$354)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$355)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$353)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$354)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$352)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$353)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$350)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$352)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$337)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$348)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$336)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$337)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$335)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$336)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$334)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$335)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$333)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$334)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$332)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$333)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$331)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$332)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$329)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$331)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$325)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$327)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$322)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$325)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$321)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$322)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$313)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$321)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$312)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$313)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$311)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$312)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$310)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$311)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$309)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$310)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$308)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$309)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$307)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$308)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$305)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$307)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$304)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$305)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$303)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$304)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$302)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$303)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$301)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$302)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$300)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$301)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$298)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$300)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$297)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$298)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$284)
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$295)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$283)
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$284)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$282)
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$283)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$281)
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$282)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$280)
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$281)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$279)
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$280)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$278)
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$279)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$276)
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$278)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$272)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$274)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$269)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$272)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$268)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$269)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$266)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$268)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$265)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$266)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$264)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$265)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$263)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$264)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$261)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$263)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$260)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$261)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$258)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$260)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$257)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$258)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$256)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$257)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$255)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$256)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$251)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$255)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$250)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$251)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$248)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$250)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$247)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$248)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$246)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$247)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$245)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$246)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$243)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$245)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$242)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$243)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$240)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$242)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$239)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$240)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$238)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$239)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$237)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$238)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$219)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$237)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$218)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$219)
	.dw	2
	.db	120
	.sleb128	10
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$217)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$218)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$216)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$217)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$215)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$216)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$214)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$215)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$213)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$214)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$212)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$213)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$211)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$212)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$209)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$211)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$208)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$209)
	.dw	2
	.db	120
	.sleb128	10
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$207)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$208)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$206)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$207)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$205)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$206)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$204)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$205)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$203)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$204)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$202)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$203)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$201)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$202)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$199)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$201)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$198)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$199)
	.dw	2
	.db	120
	.sleb128	10
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$197)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$198)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$196)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$197)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$195)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$196)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$194)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$195)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$193)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$194)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$191)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$193)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$190)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$191)
	.dw	2
	.db	120
	.sleb128	10
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$189)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$190)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$188)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$189)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$187)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$188)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$186)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$187)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$185)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$186)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$183)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$185)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$182)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$183)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$178)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$180)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$175)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$178)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$174)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$175)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$172)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$174)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$171)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$172)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$170)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$171)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$169)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$170)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$165)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$169)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$164)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$165)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$162)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$164)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$161)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$162)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$160)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$161)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$159)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$160)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$155)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$159)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$154)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$155)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$153)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$154)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$152)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$153)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$151)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$152)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$150)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$151)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$148)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$150)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$147)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$148)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$146)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$147)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$145)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$146)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$144)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$145)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$143)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$144)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$142)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$143)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$141)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$142)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$140)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$141)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$138)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$140)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$137)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$138)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$136)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$137)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$135)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$136)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$134)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$135)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$133)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$134)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$132)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$133)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$131)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$132)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$130)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$131)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$128)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$130)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$127)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$128)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$126)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$127)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$125)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$126)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$124)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$125)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$123)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$124)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$122)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$123)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$120)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$122)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$119)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$120)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$118)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$119)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$117)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$118)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$116)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$117)
	.dw	2
	.db	120
	.sleb128	5
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$115)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$116)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$114)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$115)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$112)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$114)
	.dw	2
	.db	120
	.sleb128	2
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$111)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$112)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$107)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$109)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$100)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$107)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$99)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$100)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$98)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$99)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$97)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$98)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$96)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$97)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$95)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$96)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$94)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$95)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$92)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$94)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$91)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$92)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$90)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$91)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$89)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$90)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$88)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$89)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$87)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$88)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$86)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$87)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$84)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$86)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$83)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$84)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$82)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$83)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$81)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$82)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$80)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$81)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$79)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$80)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$78)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$79)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$77)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$78)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$76)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$77)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$75)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$76)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$74)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$75)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$72)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$74)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$71)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$72)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$67)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$69)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$60)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$67)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$59)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$60)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$58)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$59)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$57)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$58)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$56)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$57)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$55)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$56)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$54)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$55)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$52)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$54)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$51)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$52)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$50)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$51)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$49)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$50)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$48)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$49)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$47)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$48)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$46)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$47)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$44)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$46)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$43)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$44)
	.dw	2
	.db	120
	.sleb128	9
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$42)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$43)
	.dw	2
	.db	120
	.sleb128	8
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$41)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$42)
	.dw	2
	.db	120
	.sleb128	7
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$40)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$41)
	.dw	2
	.db	120
	.sleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$39)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$40)
	.dw	2
	.db	120
	.sleb128	4
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$38)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$39)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$37)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$38)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$36)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$37)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$35)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$36)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$34)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$35)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$32)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$34)
	.dw	2
	.db	120
	.sleb128	3
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$31)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$32)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_TimeBaseInit$23)
	.dw	0,(Sstm8s_tim3$TIM3_TimeBaseInit$29)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0
	.dw	0,(Sstm8s_tim3$TIM3_DeInit$1)
	.dw	0,(Sstm8s_tim3$TIM3_DeInit$21)
	.dw	2
	.db	120
	.sleb128	1
	.dw	0,0
	.dw	0,0

	.area .debug_abbrev (NOLOAD)
Ldebug_abbrev:
	.uleb128	11
	.uleb128	46
	.db	0
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	4
	.uleb128	5
	.db	0
	.uleb128	2
	.uleb128	10
	.uleb128	3
	.uleb128	8
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	13
	.uleb128	1
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	11
	.uleb128	11
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	3
	.uleb128	46
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	0
	.uleb128	0
	.uleb128	7
	.uleb128	52
	.db	0
	.uleb128	2
	.uleb128	10
	.uleb128	3
	.uleb128	8
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	10
	.uleb128	46
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	12
	.uleb128	38
	.db	0
	.uleb128	73
	.uleb128	19
	.uleb128	0
	.uleb128	0
	.uleb128	9
	.uleb128	11
	.db	1
	.uleb128	17
	.uleb128	1
	.uleb128	0
	.uleb128	0
	.uleb128	1
	.uleb128	17
	.db	1
	.uleb128	3
	.uleb128	8
	.uleb128	16
	.uleb128	6
	.uleb128	19
	.uleb128	11
	.uleb128	37
	.uleb128	8
	.uleb128	0
	.uleb128	0
	.uleb128	6
	.uleb128	11
	.db	0
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	0
	.uleb128	0
	.uleb128	8
	.uleb128	11
	.db	1
	.uleb128	1
	.uleb128	19
	.uleb128	17
	.uleb128	1
	.uleb128	0
	.uleb128	0
	.uleb128	2
	.uleb128	46
	.db	0
	.uleb128	3
	.uleb128	8
	.uleb128	17
	.uleb128	1
	.uleb128	18
	.uleb128	1
	.uleb128	63
	.uleb128	12
	.uleb128	64
	.uleb128	6
	.uleb128	0
	.uleb128	0
	.uleb128	14
	.uleb128	33
	.db	0
	.uleb128	47
	.uleb128	11
	.uleb128	0
	.uleb128	0
	.uleb128	5
	.uleb128	36
	.db	0
	.uleb128	3
	.uleb128	8
	.uleb128	11
	.uleb128	11
	.uleb128	62
	.uleb128	11
	.uleb128	0
	.uleb128	0
	.uleb128	0

	.area .debug_info (NOLOAD)
	.dw	0,Ldebug_info_end-Ldebug_info_start
Ldebug_info_start:
	.dw	2
	.dw	0,(Ldebug_abbrev)
	.db	4
	.uleb128	1
	.ascii "drivers/src/stm8s_tim3.c"
	.db	0
	.dw	0,(Ldebug_line_start+-4)
	.db	1
	.ascii "SDCC version 4.1.0 #12072"
	.db	0
	.uleb128	2
	.ascii "TIM3_DeInit"
	.db	0
	.dw	0,(_TIM3_DeInit)
	.dw	0,(XG$TIM3_DeInit$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+5612)
	.uleb128	3
	.dw	0,174
	.ascii "TIM3_TimeBaseInit"
	.db	0
	.dw	0,(_TIM3_TimeBaseInit)
	.dw	0,(XG$TIM3_TimeBaseInit$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+5592)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_Prescaler"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM3_Period"
	.db	0
	.dw	0,191
	.uleb128	0
	.uleb128	5
	.ascii "unsigned char"
	.db	0
	.db	1
	.db	8
	.uleb128	5
	.ascii "unsigned int"
	.db	0
	.db	2
	.db	7
	.uleb128	3
	.dw	0,327
	.ascii "TIM3_OC1Init"
	.db	0
	.dw	0,(_TIM3_OC1Init)
	.dw	0,(XG$TIM3_OC1Init$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+5248)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_OCMode"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM3_OutputState"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	4
	.ascii "TIM3_Pulse"
	.db	0
	.dw	0,191
	.uleb128	4
	.db	2
	.db	145
	.sleb128	6
	.ascii "TIM3_OCPolarity"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,447
	.ascii "TIM3_OC2Init"
	.db	0
	.dw	0,(_TIM3_OC2Init)
	.dw	0,(XG$TIM3_OC2Init$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+4904)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_OCMode"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM3_OutputState"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	4
	.ascii "TIM3_Pulse"
	.db	0
	.dw	0,191
	.uleb128	4
	.db	2
	.db	145
	.sleb128	6
	.ascii "TIM3_OCPolarity"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,613
	.ascii "TIM3_ICInit"
	.db	0
	.dw	0,(_TIM3_ICInit)
	.dw	0,(XG$TIM3_ICInit$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+4260)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_Channel"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM3_ICPolarity"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	4
	.ascii "TIM3_ICSelection"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	5
	.ascii "TIM3_ICPrescaler"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	6
	.ascii "TIM3_ICFilter"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$157)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$166)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$167)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$176)
	.uleb128	0
	.uleb128	3
	.dw	0,858
	.ascii "TIM3_PWMIConfig"
	.db	0
	.dw	0,(_TIM3_PWMIConfig)
	.dw	0,(XG$TIM3_PWMIConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+3544)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_Channel"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM3_ICPolarity"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	4
	.ascii "TIM3_ICSelection"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	5
	.ascii "TIM3_ICPrescaler"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	6
	.ascii "TIM3_ICFilter"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$221)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$223)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$224)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$226)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$228)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$230)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$231)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$233)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$235)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$252)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$253)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$270)
	.uleb128	7
	.db	2
	.db	145
	.sleb128	-2
	.ascii "icpolarity"
	.db	0
	.dw	0,174
	.uleb128	7
	.db	2
	.db	145
	.sleb128	-1
	.ascii "icselection"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,921
	.ascii "TIM3_Cmd"
	.db	0
	.dw	0,(_TIM3_Cmd)
	.dw	0,(XG$TIM3_Cmd$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+3440)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "NewState"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$287)
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$289)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$290)
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$292)
	.uleb128	0
	.uleb128	3
	.dw	0,1005
	.ascii "TIM3_ITConfig"
	.db	0
	.dw	0,(_TIM3_ITConfig)
	.dw	0,(XG$TIM3_ITConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+3216)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_IT"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "NewState"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$316)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$318)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$319)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$323)
	.uleb128	0
	.uleb128	3
	.dw	0,1084
	.ascii "TIM3_UpdateDisableConfig"
	.db	0
	.dw	0,(_TIM3_UpdateDisableConfig)
	.dw	0,(XG$TIM3_UpdateDisableConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+3112)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "NewState"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$340)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$342)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$343)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$345)
	.uleb128	0
	.uleb128	3
	.dw	0,1172
	.ascii "TIM3_UpdateRequestConfig"
	.db	0
	.dw	0,(_TIM3_UpdateRequestConfig)
	.dw	0,(XG$TIM3_UpdateRequestConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+3008)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_UpdateSource"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$361)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$363)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$364)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$366)
	.uleb128	0
	.uleb128	3
	.dw	0,1253
	.ascii "TIM3_SelectOnePulseMode"
	.db	0
	.dw	0,(_TIM3_SelectOnePulseMode)
	.dw	0,(XG$TIM3_SelectOnePulseMode$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+2904)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_OPMode"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$382)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$384)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$385)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$387)
	.uleb128	0
	.uleb128	3
	.dw	0,1338
	.ascii "TIM3_PrescalerConfig"
	.db	0
	.dw	0,(_TIM3_PrescalerConfig)
	.dw	0,(XG$TIM3_PrescalerConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+2548)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "Prescaler"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM3_PSCReloadMode"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,1404
	.ascii "TIM3_ForcedOC1Config"
	.db	0
	.dw	0,(_TIM3_ForcedOC1Config)
	.dw	0,(XG$TIM3_ForcedOC1Config$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+2432)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_ForcedAction"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,1470
	.ascii "TIM3_ForcedOC2Config"
	.db	0
	.dw	0,(_TIM3_ForcedOC2Config)
	.dw	0,(XG$TIM3_ForcedOC2Config$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+2316)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_ForcedAction"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,1546
	.ascii "TIM3_ARRPreloadConfig"
	.db	0
	.dw	0,(_TIM3_ARRPreloadConfig)
	.dw	0,(XG$TIM3_ARRPreloadConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+2212)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "NewState"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$470)
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$472)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$473)
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$475)
	.uleb128	0
	.uleb128	3
	.dw	0,1622
	.ascii "TIM3_OC1PreloadConfig"
	.db	0
	.dw	0,(_TIM3_OC1PreloadConfig)
	.dw	0,(XG$TIM3_OC1PreloadConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+2108)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "NewState"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$491)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$493)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$494)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$496)
	.uleb128	0
	.uleb128	3
	.dw	0,1698
	.ascii "TIM3_OC2PreloadConfig"
	.db	0
	.dw	0,(_TIM3_OC2PreloadConfig)
	.dw	0,(XG$TIM3_OC2PreloadConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+2004)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "NewState"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$512)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$514)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$515)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$517)
	.uleb128	0
	.uleb128	3
	.dw	0,1761
	.ascii "TIM3_GenerateEvent"
	.db	0
	.dw	0,(_TIM3_GenerateEvent)
	.dw	0,(XG$TIM3_GenerateEvent$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1912)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_EventSource"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,1845
	.ascii "TIM3_OC1PolarityConfig"
	.db	0
	.dw	0,(_TIM3_OC1PolarityConfig)
	.dw	0,(XG$TIM3_OC1PolarityConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1808)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_OCPolarity"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$546)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$548)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$549)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$551)
	.uleb128	0
	.uleb128	3
	.dw	0,1929
	.ascii "TIM3_OC2PolarityConfig"
	.db	0
	.dw	0,(_TIM3_OC2PolarityConfig)
	.dw	0,(XG$TIM3_OC2PolarityConfig$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1704)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_OCPolarity"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$567)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$569)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$570)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$572)
	.uleb128	0
	.uleb128	3
	.dw	0,2050
	.ascii "TIM3_CCxCmd"
	.db	0
	.dw	0,(_TIM3_CCxCmd)
	.dw	0,(XG$TIM3_CCxCmd$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1516)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_Channel"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "NewState"
	.db	0
	.dw	0,174
	.uleb128	8
	.dw	0,2025
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$596)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$598)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$600)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$601)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$603)
	.uleb128	0
	.uleb128	9
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$604)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$606)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$608)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$609)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$611)
	.uleb128	0
	.uleb128	0
	.uleb128	3
	.dw	0,2144
	.ascii "TIM3_SelectOCxM"
	.db	0
	.dw	0,(_TIM3_SelectOCxM)
	.dw	0,(XG$TIM3_SelectOCxM$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1256)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_Channel"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM3_OCMode"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$641)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$644)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$645)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$648)
	.uleb128	0
	.uleb128	3
	.dw	0,2195
	.ascii "TIM3_SetCounter"
	.db	0
	.dw	0,(_TIM3_SetCounter)
	.dw	0,(XG$TIM3_SetCounter$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1236)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "Counter"
	.db	0
	.dw	0,191
	.uleb128	0
	.uleb128	3
	.dw	0,2252
	.ascii "TIM3_SetAutoreload"
	.db	0
	.dw	0,(_TIM3_SetAutoreload)
	.dw	0,(XG$TIM3_SetAutoreload$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1216)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "Autoreload"
	.db	0
	.dw	0,191
	.uleb128	0
	.uleb128	3
	.dw	0,2305
	.ascii "TIM3_SetCompare1"
	.db	0
	.dw	0,(_TIM3_SetCompare1)
	.dw	0,(XG$TIM3_SetCompare1$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1196)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "Compare1"
	.db	0
	.dw	0,191
	.uleb128	0
	.uleb128	3
	.dw	0,2358
	.ascii "TIM3_SetCompare2"
	.db	0
	.dw	0,(_TIM3_SetCompare2)
	.dw	0,(XG$TIM3_SetCompare2$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1176)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "Compare2"
	.db	0
	.dw	0,191
	.uleb128	0
	.uleb128	3
	.dw	0,2424
	.ascii "TIM3_SetIC1Prescaler"
	.db	0
	.dw	0,(_TIM3_SetIC1Prescaler)
	.dw	0,(XG$TIM3_SetIC1Prescaler$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+1048)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_IC1Prescaler"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,2490
	.ascii "TIM3_SetIC2Prescaler"
	.db	0
	.dw	0,(_TIM3_SetIC2Prescaler)
	.dw	0,(XG$TIM3_SetIC2Prescaler$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+920)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_IC2Prescaler"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	10
	.dw	0,2582
	.ascii "TIM3_GetCapture1"
	.db	0
	.dw	0,(_TIM3_GetCapture1)
	.dw	0,(XG$TIM3_GetCapture1$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+852)
	.dw	0,191
	.uleb128	7
	.db	6
	.db	82
	.db	147
	.uleb128	1
	.db	81
	.db	147
	.uleb128	1
	.ascii "tmpccr1"
	.db	0
	.dw	0,191
	.uleb128	7
	.db	1
	.db	80
	.ascii "tmpccr1l"
	.db	0
	.dw	0,174
	.uleb128	7
	.db	1
	.db	82
	.ascii "tmpccr1h"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	10
	.dw	0,2674
	.ascii "TIM3_GetCapture2"
	.db	0
	.dw	0,(_TIM3_GetCapture2)
	.dw	0,(XG$TIM3_GetCapture2$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+784)
	.dw	0,191
	.uleb128	7
	.db	6
	.db	82
	.db	147
	.uleb128	1
	.db	81
	.db	147
	.uleb128	1
	.ascii "tmpccr2"
	.db	0
	.dw	0,191
	.uleb128	7
	.db	1
	.db	80
	.ascii "tmpccr2l"
	.db	0
	.dw	0,174
	.uleb128	7
	.db	1
	.db	82
	.ascii "tmpccr2h"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	10
	.dw	0,2734
	.ascii "TIM3_GetCounter"
	.db	0
	.dw	0,(_TIM3_GetCounter)
	.dw	0,(XG$TIM3_GetCounter$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+740)
	.dw	0,191
	.uleb128	7
	.db	7
	.db	82
	.db	147
	.uleb128	1
	.db	145
	.sleb128	-3
	.db	147
	.uleb128	1
	.ascii "tmpcntr"
	.db	0
	.dw	0,191
	.uleb128	0
	.uleb128	11
	.ascii "TIM3_GetPrescaler"
	.db	0
	.dw	0,(_TIM3_GetPrescaler)
	.dw	0,(XG$TIM3_GetPrescaler$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+720)
	.dw	0,174
	.uleb128	10
	.dw	0,2904
	.ascii "TIM3_GetFlagStatus"
	.db	0
	.dw	0,(_TIM3_GetFlagStatus)
	.dw	0,(XG$TIM3_GetFlagStatus$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+484)
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_FLAG"
	.db	0
	.dw	0,2904
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$778)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$780)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$781)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$783)
	.uleb128	7
	.db	1
	.db	80
	.ascii "bitstatus"
	.db	0
	.dw	0,174
	.uleb128	7
	.db	2
	.db	145
	.sleb128	-1
	.ascii "tim3_flag_l"
	.db	0
	.dw	0,174
	.uleb128	7
	.db	1
	.db	82
	.ascii "tim3_flag_h"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	5
	.ascii "unsigned int"
	.db	0
	.db	2
	.db	7
	.uleb128	3
	.dw	0,2972
	.ascii "TIM3_ClearFlag"
	.db	0
	.dw	0,(_TIM3_ClearFlag)
	.dw	0,(XG$TIM3_ClearFlag$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+332)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_FLAG"
	.db	0
	.dw	0,2904
	.uleb128	0
	.uleb128	10
	.dw	0,3106
	.ascii "TIM3_GetITStatus"
	.db	0
	.dw	0,(_TIM3_GetITStatus)
	.dw	0,(XG$TIM3_GetITStatus$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+180)
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_IT"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$824)
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$826)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$827)
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$829)
	.uleb128	7
	.db	1
	.db	80
	.ascii "bitstatus"
	.db	0
	.dw	0,174
	.uleb128	7
	.db	2
	.db	145
	.sleb128	-1
	.ascii "TIM3_itStatus"
	.db	0
	.dw	0,174
	.uleb128	7
	.db	1
	.db	80
	.ascii "TIM3_itEnable"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,3164
	.ascii "TIM3_ClearITPendingBit"
	.db	0
	.dw	0,(_TIM3_ClearITPendingBit)
	.dw	0,(XG$TIM3_ClearITPendingBit$0$0+1)
	.db	1
	.dw	0,(Ldebug_loc_start+88)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_IT"
	.db	0
	.dw	0,174
	.uleb128	0
	.uleb128	3
	.dw	0,3283
	.ascii "TI1_Config"
	.db	0
	.dw	0,(_TI1_Config)
	.dw	0,(XFstm8s_tim3$TI1_Config$0$0+1)
	.db	0
	.dw	0,(Ldebug_loc_start+44)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_ICPolarity"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM3_ICSelection"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	4
	.ascii "TIM3_ICFilter"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TI1_Config$855)
	.dw	0,(Sstm8s_tim3$TI1_Config$857)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TI1_Config$858)
	.dw	0,(Sstm8s_tim3$TI1_Config$860)
	.uleb128	0
	.uleb128	3
	.dw	0,3402
	.ascii "TI2_Config"
	.db	0
	.dw	0,(_TI2_Config)
	.dw	0,(XFstm8s_tim3$TI2_Config$0$0+1)
	.db	0
	.dw	0,(Ldebug_loc_start)
	.uleb128	4
	.db	2
	.db	145
	.sleb128	2
	.ascii "TIM3_ICPolarity"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	3
	.ascii "TIM3_ICSelection"
	.db	0
	.dw	0,174
	.uleb128	4
	.db	2
	.db	145
	.sleb128	4
	.ascii "TIM3_ICFilter"
	.db	0
	.dw	0,174
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TI2_Config$874)
	.dw	0,(Sstm8s_tim3$TI2_Config$876)
	.uleb128	6
	.dw	0,(Sstm8s_tim3$TI2_Config$877)
	.dw	0,(Sstm8s_tim3$TI2_Config$879)
	.uleb128	0
	.uleb128	12
	.dw	0,174
	.uleb128	13
	.dw	0,3420
	.db	25
	.dw	0,3402
	.uleb128	14
	.db	24
	.uleb128	0
	.uleb128	7
	.db	5
	.db	3
	.dw	0,(___str_0)
	.ascii "__str_0"
	.db	0
	.dw	0,3407
	.uleb128	0
	.uleb128	0
	.uleb128	0
Ldebug_info_end:

	.area .debug_pubnames (NOLOAD)
	.dw	0,Ldebug_pubnames_end-Ldebug_pubnames_start
Ldebug_pubnames_start:
	.dw	2
	.dw	0,(Ldebug_info_start-4)
	.dw	0,4+Ldebug_info_end-Ldebug_info_start
	.dw	0,68
	.ascii "TIM3_DeInit"
	.db	0
	.dw	0,94
	.ascii "TIM3_TimeBaseInit"
	.db	0
	.dw	0,207
	.ascii "TIM3_OC1Init"
	.db	0
	.dw	0,327
	.ascii "TIM3_OC2Init"
	.db	0
	.dw	0,447
	.ascii "TIM3_ICInit"
	.db	0
	.dw	0,613
	.ascii "TIM3_PWMIConfig"
	.db	0
	.dw	0,858
	.ascii "TIM3_Cmd"
	.db	0
	.dw	0,921
	.ascii "TIM3_ITConfig"
	.db	0
	.dw	0,1005
	.ascii "TIM3_UpdateDisableConfig"
	.db	0
	.dw	0,1084
	.ascii "TIM3_UpdateRequestConfig"
	.db	0
	.dw	0,1172
	.ascii "TIM3_SelectOnePulseMode"
	.db	0
	.dw	0,1253
	.ascii "TIM3_PrescalerConfig"
	.db	0
	.dw	0,1338
	.ascii "TIM3_ForcedOC1Config"
	.db	0
	.dw	0,1404
	.ascii "TIM3_ForcedOC2Config"
	.db	0
	.dw	0,1470
	.ascii "TIM3_ARRPreloadConfig"
	.db	0
	.dw	0,1546
	.ascii "TIM3_OC1PreloadConfig"
	.db	0
	.dw	0,1622
	.ascii "TIM3_OC2PreloadConfig"
	.db	0
	.dw	0,1698
	.ascii "TIM3_GenerateEvent"
	.db	0
	.dw	0,1761
	.ascii "TIM3_OC1PolarityConfig"
	.db	0
	.dw	0,1845
	.ascii "TIM3_OC2PolarityConfig"
	.db	0
	.dw	0,1929
	.ascii "TIM3_CCxCmd"
	.db	0
	.dw	0,2050
	.ascii "TIM3_SelectOCxM"
	.db	0
	.dw	0,2144
	.ascii "TIM3_SetCounter"
	.db	0
	.dw	0,2195
	.ascii "TIM3_SetAutoreload"
	.db	0
	.dw	0,2252
	.ascii "TIM3_SetCompare1"
	.db	0
	.dw	0,2305
	.ascii "TIM3_SetCompare2"
	.db	0
	.dw	0,2358
	.ascii "TIM3_SetIC1Prescaler"
	.db	0
	.dw	0,2424
	.ascii "TIM3_SetIC2Prescaler"
	.db	0
	.dw	0,2490
	.ascii "TIM3_GetCapture1"
	.db	0
	.dw	0,2582
	.ascii "TIM3_GetCapture2"
	.db	0
	.dw	0,2674
	.ascii "TIM3_GetCounter"
	.db	0
	.dw	0,2734
	.ascii "TIM3_GetPrescaler"
	.db	0
	.dw	0,2770
	.ascii "TIM3_GetFlagStatus"
	.db	0
	.dw	0,2920
	.ascii "TIM3_ClearFlag"
	.db	0
	.dw	0,2972
	.ascii "TIM3_GetITStatus"
	.db	0
	.dw	0,3106
	.ascii "TIM3_ClearITPendingBit"
	.db	0
	.dw	0,0
Ldebug_pubnames_end:

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE0_end-Ldebug_CIE0_start
Ldebug_CIE0_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE0_end:
	.dw	0,33
	.dw	0,(Ldebug_CIE0_start-4)
	.dw	0,(Sstm8s_tim3$TI2_Config$867)	;initial loc
	.dw	0,Sstm8s_tim3$TI2_Config$884-Sstm8s_tim3$TI2_Config$867
	.db	1
	.dw	0,(Sstm8s_tim3$TI2_Config$867)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TI2_Config$868)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TI2_Config$882)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE1_end-Ldebug_CIE1_start
Ldebug_CIE1_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE1_end:
	.dw	0,33
	.dw	0,(Ldebug_CIE1_start-4)
	.dw	0,(Sstm8s_tim3$TI1_Config$849)	;initial loc
	.dw	0,Sstm8s_tim3$TI1_Config$865-Sstm8s_tim3$TI1_Config$849
	.db	1
	.dw	0,(Sstm8s_tim3$TI1_Config$849)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TI1_Config$850)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TI1_Config$863)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE2_end-Ldebug_CIE2_start
Ldebug_CIE2_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE2_end:
	.dw	0,61
	.dw	0,(Ldebug_CIE2_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$836)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_ClearITPendingBit$847-Sstm8s_tim3$TIM3_ClearITPendingBit$836
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$836)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$838)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$839)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$840)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$841)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$842)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearITPendingBit$843)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE3_end-Ldebug_CIE3_start
Ldebug_CIE3_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE3_end:
	.dw	0,96
	.dw	0,(Ldebug_CIE3_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$809)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_GetITStatus$834-Sstm8s_tim3$TIM3_GetITStatus$809
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$809)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$810)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$812)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$813)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$814)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$815)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$816)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$817)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$818)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$819)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$820)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetITStatus$832)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE4_end-Ldebug_CIE4_start
Ldebug_CIE4_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE4_end:
	.dw	0,96
	.dw	0,(Ldebug_CIE4_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$790)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_ClearFlag$807-Sstm8s_tim3$TIM3_ClearFlag$790
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$790)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$791)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$793)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$794)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$795)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$796)
	.db	14
	.uleb128	11
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$797)
	.db	14
	.uleb128	12
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$798)
	.db	14
	.uleb128	13
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$799)
	.db	14
	.uleb128	14
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$800)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$801)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ClearFlag$805)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE5_end-Ldebug_CIE5_start
Ldebug_CIE5_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE5_end:
	.dw	0,145
	.dw	0,(Ldebug_CIE5_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$756)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_GetFlagStatus$788-Sstm8s_tim3$TIM3_GetFlagStatus$756
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$756)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$757)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$759)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$760)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$761)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$762)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$763)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$764)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$765)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$766)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$767)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$768)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$769)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$770)
	.db	14
	.uleb128	11
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$771)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$772)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$776)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$777)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetFlagStatus$786)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE6_end-Ldebug_CIE6_start
Ldebug_CIE6_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE6_end:
	.dw	0,19
	.dw	0,(Ldebug_CIE6_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_GetPrescaler$750)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_GetPrescaler$754-Sstm8s_tim3$TIM3_GetPrescaler$750
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetPrescaler$750)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE7_end-Ldebug_CIE7_start
Ldebug_CIE7_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE7_end:
	.dw	0,33
	.dw	0,(Ldebug_CIE7_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_GetCounter$741)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_GetCounter$748-Sstm8s_tim3$TIM3_GetCounter$741
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetCounter$741)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetCounter$742)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetCounter$746)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE8_end-Ldebug_CIE8_start
Ldebug_CIE8_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE8_end:
	.dw	0,47
	.dw	0,(Ldebug_CIE8_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture2$727)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_GetCapture2$739-Sstm8s_tim3$TIM3_GetCapture2$727
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture2$727)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture2$728)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture2$733)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture2$734)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture2$737)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE9_end-Ldebug_CIE9_start
Ldebug_CIE9_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE9_end:
	.dw	0,47
	.dw	0,(Ldebug_CIE9_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture1$713)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_GetCapture1$725-Sstm8s_tim3$TIM3_GetCapture1$713
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture1$713)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture1$714)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture1$719)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture1$720)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GetCapture1$723)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE10_end-Ldebug_CIE10_start
Ldebug_CIE10_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE10_end:
	.dw	0,82
	.dw	0,(Ldebug_CIE10_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$697)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_SetIC2Prescaler$711-Sstm8s_tim3$TIM3_SetIC2Prescaler$697
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$697)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$699)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$700)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$701)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$702)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$703)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$704)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$705)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$706)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC2Prescaler$707)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE11_end-Ldebug_CIE11_start
Ldebug_CIE11_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE11_end:
	.dw	0,82
	.dw	0,(Ldebug_CIE11_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$681)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_SetIC1Prescaler$695-Sstm8s_tim3$TIM3_SetIC1Prescaler$681
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$681)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$683)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$684)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$685)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$686)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$687)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$688)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$689)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$690)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetIC1Prescaler$691)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE12_end-Ldebug_CIE12_start
Ldebug_CIE12_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE12_end:
	.dw	0,19
	.dw	0,(Ldebug_CIE12_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_SetCompare2$674)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_SetCompare2$679-Sstm8s_tim3$TIM3_SetCompare2$674
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetCompare2$674)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE13_end-Ldebug_CIE13_start
Ldebug_CIE13_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE13_end:
	.dw	0,19
	.dw	0,(Ldebug_CIE13_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_SetCompare1$667)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_SetCompare1$672-Sstm8s_tim3$TIM3_SetCompare1$667
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetCompare1$667)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE14_end-Ldebug_CIE14_start
Ldebug_CIE14_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE14_end:
	.dw	0,19
	.dw	0,(Ldebug_CIE14_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_SetAutoreload$660)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_SetAutoreload$665-Sstm8s_tim3$TIM3_SetAutoreload$660
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetAutoreload$660)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE15_end-Ldebug_CIE15_start
Ldebug_CIE15_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE15_end:
	.dw	0,19
	.dw	0,(Ldebug_CIE15_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_SetCounter$653)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_SetCounter$658-Sstm8s_tim3$TIM3_SetCounter$653
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SetCounter$653)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE16_end-Ldebug_CIE16_start
Ldebug_CIE16_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE16_end:
	.dw	0,159
	.dw	0,(Ldebug_CIE16_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$616)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_SelectOCxM$651-Sstm8s_tim3$TIM3_SelectOCxM$616
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$616)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$618)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$619)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$620)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$621)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$622)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$623)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$624)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$626)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$627)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$628)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$629)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$630)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$631)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$632)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$633)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$634)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$635)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$636)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$637)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOCxM$638)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE17_end-Ldebug_CIE17_start
Ldebug_CIE17_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE17_end:
	.dw	0,117
	.dw	0,(Ldebug_CIE17_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$577)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_CCxCmd$614-Sstm8s_tim3$TIM3_CCxCmd$577
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$577)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$579)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$580)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$581)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$582)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$583)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$584)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$585)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$587)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$588)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$589)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$590)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$591)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$592)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_CCxCmd$593)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE18_end-Ldebug_CIE18_start
Ldebug_CIE18_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE18_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE18_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$556)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_OC2PolarityConfig$575-Sstm8s_tim3$TIM3_OC2PolarityConfig$556
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$556)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$558)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$559)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$560)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$561)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$562)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$563)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PolarityConfig$564)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE19_end-Ldebug_CIE19_start
Ldebug_CIE19_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE19_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE19_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$535)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_OC1PolarityConfig$554-Sstm8s_tim3$TIM3_OC1PolarityConfig$535
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$535)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$537)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$538)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$539)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$540)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$541)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$542)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PolarityConfig$543)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE20_end-Ldebug_CIE20_start
Ldebug_CIE20_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE20_end:
	.dw	0,61
	.dw	0,(Ldebug_CIE20_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$522)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_GenerateEvent$533-Sstm8s_tim3$TIM3_GenerateEvent$522
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$522)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$524)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$525)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$526)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$527)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$528)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_GenerateEvent$529)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE21_end-Ldebug_CIE21_start
Ldebug_CIE21_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE21_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE21_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$501)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_OC2PreloadConfig$520-Sstm8s_tim3$TIM3_OC2PreloadConfig$501
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$501)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$503)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$504)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$505)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$506)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$507)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$508)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2PreloadConfig$509)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE22_end-Ldebug_CIE22_start
Ldebug_CIE22_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE22_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE22_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$480)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_OC1PreloadConfig$499-Sstm8s_tim3$TIM3_OC1PreloadConfig$480
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$480)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$482)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$483)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$484)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$485)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$486)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$487)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1PreloadConfig$488)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE23_end-Ldebug_CIE23_start
Ldebug_CIE23_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE23_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE23_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$459)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_ARRPreloadConfig$478-Sstm8s_tim3$TIM3_ARRPreloadConfig$459
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$459)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$461)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$462)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$463)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$464)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$465)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$466)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ARRPreloadConfig$467)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE24_end-Ldebug_CIE24_start
Ldebug_CIE24_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE24_end:
	.dw	0,75
	.dw	0,(Ldebug_CIE24_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$444)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_ForcedOC2Config$457-Sstm8s_tim3$TIM3_ForcedOC2Config$444
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$444)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$446)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$447)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$448)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$449)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$450)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$451)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$452)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC2Config$453)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE25_end-Ldebug_CIE25_start
Ldebug_CIE25_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE25_end:
	.dw	0,75
	.dw	0,(Ldebug_CIE25_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$429)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_ForcedOC1Config$442-Sstm8s_tim3$TIM3_ForcedOC1Config$429
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$429)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$431)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$432)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$433)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$434)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$435)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$436)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$437)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ForcedOC1Config$438)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE26_end-Ldebug_CIE26_start
Ldebug_CIE26_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE26_end:
	.dw	0,215
	.dw	0,(Ldebug_CIE26_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$392)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_PrescalerConfig$427-Sstm8s_tim3$TIM3_PrescalerConfig$392
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$392)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$394)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$395)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$396)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$397)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$398)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$399)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$400)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$402)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$403)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$404)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$405)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$406)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$407)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$408)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$409)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$410)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$411)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$412)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$413)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$414)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$415)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$416)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$417)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$418)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$419)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$420)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$421)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PrescalerConfig$422)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE27_end-Ldebug_CIE27_start
Ldebug_CIE27_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE27_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE27_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$371)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_SelectOnePulseMode$390-Sstm8s_tim3$TIM3_SelectOnePulseMode$371
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$371)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$373)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$374)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$375)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$376)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$377)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$378)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_SelectOnePulseMode$379)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE28_end-Ldebug_CIE28_start
Ldebug_CIE28_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE28_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE28_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$350)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_UpdateRequestConfig$369-Sstm8s_tim3$TIM3_UpdateRequestConfig$350
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$350)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$352)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$353)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$354)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$355)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$356)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$357)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateRequestConfig$358)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE29_end-Ldebug_CIE29_start
Ldebug_CIE29_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE29_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE29_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$329)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_UpdateDisableConfig$348-Sstm8s_tim3$TIM3_UpdateDisableConfig$329
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$329)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$331)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$332)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$333)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$334)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$335)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$336)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_UpdateDisableConfig$337)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE30_end-Ldebug_CIE30_start
Ldebug_CIE30_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE30_end:
	.dw	0,138
	.dw	0,(Ldebug_CIE30_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$297)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_ITConfig$327-Sstm8s_tim3$TIM3_ITConfig$297
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$297)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$298)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$300)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$301)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$302)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$303)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$304)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$305)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$307)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$308)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$309)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$310)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$311)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$312)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$313)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$321)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$322)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ITConfig$325)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE31_end-Ldebug_CIE31_start
Ldebug_CIE31_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE31_end:
	.dw	0,68
	.dw	0,(Ldebug_CIE31_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$276)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_Cmd$295-Sstm8s_tim3$TIM3_Cmd$276
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$276)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$278)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$279)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$280)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$281)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$282)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$283)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_Cmd$284)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE32_end-Ldebug_CIE32_start
Ldebug_CIE32_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE32_end:
	.dw	0,425
	.dw	0,(Ldebug_CIE32_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$182)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_PWMIConfig$274-Sstm8s_tim3$TIM3_PWMIConfig$182
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$182)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$183)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$185)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$186)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$187)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$188)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$189)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$190)
	.db	14
	.uleb128	11
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$191)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$193)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$194)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$195)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$196)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$197)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$198)
	.db	14
	.uleb128	11
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$199)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$201)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$202)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$203)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$204)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$205)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$206)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$207)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$208)
	.db	14
	.uleb128	11
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$209)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$211)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$212)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$213)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$214)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$215)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$216)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$217)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$218)
	.db	14
	.uleb128	11
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$219)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$237)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$238)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$239)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$240)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$242)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$243)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$245)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$246)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$247)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$248)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$250)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$251)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$255)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$256)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$257)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$258)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$260)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$261)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$263)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$264)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$265)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$266)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$268)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$269)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_PWMIConfig$272)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE33_end-Ldebug_CIE33_start
Ldebug_CIE33_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE33_end:
	.dw	0,383
	.dw	0,(Ldebug_CIE33_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$111)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_ICInit$180-Sstm8s_tim3$TIM3_ICInit$111
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$111)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$112)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$114)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$115)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$116)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$117)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$118)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$119)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$120)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$122)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$123)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$124)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$125)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$126)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$127)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$128)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$130)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$131)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$132)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$133)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$134)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$135)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$136)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$137)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$138)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$140)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$141)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$142)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$143)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$144)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$145)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$146)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$147)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$148)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$150)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$151)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$152)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$153)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$154)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$155)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$159)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$160)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$161)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$162)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$164)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$165)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$169)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$170)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$171)
	.db	14
	.uleb128	6
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$172)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$174)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$175)
	.db	14
	.uleb128	3
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_ICInit$178)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE34_end-Ldebug_CIE34_start
Ldebug_CIE34_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE34_end:
	.dw	0,208
	.dw	0,(Ldebug_CIE34_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$71)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_OC2Init$109-Sstm8s_tim3$TIM3_OC2Init$71
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$71)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$72)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$74)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$75)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$76)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$77)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$78)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$79)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$80)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$81)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$82)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$83)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$84)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$86)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$87)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$88)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$89)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$90)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$91)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$92)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$94)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$95)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$96)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$97)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$98)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$99)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$100)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC2Init$107)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE35_end-Ldebug_CIE35_start
Ldebug_CIE35_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE35_end:
	.dw	0,208
	.dw	0,(Ldebug_CIE35_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$31)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_OC1Init$69-Sstm8s_tim3$TIM3_OC1Init$31
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$31)
	.db	14
	.uleb128	2
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$32)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$34)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$35)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$36)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$37)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$38)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$39)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$40)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$41)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$42)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$43)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$44)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$46)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$47)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$48)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$49)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$50)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$51)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$52)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$54)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$55)
	.db	14
	.uleb128	5
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$56)
	.db	14
	.uleb128	7
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$57)
	.db	14
	.uleb128	8
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$58)
	.db	14
	.uleb128	9
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$59)
	.db	14
	.uleb128	10
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$60)
	.db	14
	.uleb128	4
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_OC1Init$67)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE36_end-Ldebug_CIE36_start
Ldebug_CIE36_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE36_end:
	.dw	0,19
	.dw	0,(Ldebug_CIE36_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_TimeBaseInit$23)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_TimeBaseInit$29-Sstm8s_tim3$TIM3_TimeBaseInit$23
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_TimeBaseInit$23)
	.db	14
	.uleb128	2

	.area .debug_frame (NOLOAD)
	.dw	0
	.dw	Ldebug_CIE37_end-Ldebug_CIE37_start
Ldebug_CIE37_start:
	.dw	0xffff
	.dw	0xffff
	.db	1
	.db	0
	.uleb128	1
	.sleb128	-1
	.db	9
	.db	12
	.uleb128	8
	.uleb128	2
	.db	137
	.uleb128	1
Ldebug_CIE37_end:
	.dw	0,19
	.dw	0,(Ldebug_CIE37_start-4)
	.dw	0,(Sstm8s_tim3$TIM3_DeInit$1)	;initial loc
	.dw	0,Sstm8s_tim3$TIM3_DeInit$21-Sstm8s_tim3$TIM3_DeInit$1
	.db	1
	.dw	0,(Sstm8s_tim3$TIM3_DeInit$1)
	.db	14
	.uleb128	2
