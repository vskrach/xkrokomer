                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.1.0 #12072 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _main
                                     12 	.globl _prepocet
                                     13 	.globl _jas
                                     14 	.globl _jas_init
                                     15 	.globl _delay_05s
                                     16 	.globl _delay_05s_init
                                     17 	.globl _pc_write_string
                                     18 	.globl _pc_init
                                     19 	.globl _UART1_GetFlagStatus
                                     20 	.globl _UART1_ReceiveData8
                                     21 	.globl _GPIO_ReadInputPin
                                     22 	.globl _GPIO_WriteReverse
                                     23 	.globl _GPIO_WriteLow
                                     24 	.globl _GPIO_WriteHigh
                                     25 	.globl _GPIO_Write
                                     26 	.globl _GPIO_Init
                                     27 	.globl _CLK_HSIPrescalerConfig
                                     28 	.globl _kroky
                                     29 	.globl _js
                                     30 	.globl _b
                                     31 	.globl _a
                                     32 ;--------------------------------------------------------
                                     33 ; ram data
                                     34 ;--------------------------------------------------------
                                     35 	.area DATA
                           000000    36 G$a$0_0$0==.
      000001                         37 _a::
      000001                         38 	.ds 1
                           000001    39 G$b$0_0$0==.
      000002                         40 _b::
      000002                         41 	.ds 1
                                     42 ;--------------------------------------------------------
                                     43 ; ram data
                                     44 ;--------------------------------------------------------
                                     45 	.area INITIALIZED
                           000000    46 G$js$0_0$0==.
      000003                         47 _js::
      000003                         48 	.ds 4
                           000004    49 G$kroky$0_0$0==.
      000007                         50 _kroky::
      000007                         51 	.ds 4
                                     52 ;--------------------------------------------------------
                                     53 ; Stack segment in internal ram 
                                     54 ;--------------------------------------------------------
                                     55 	.area	SSEG
      00AC3F                         56 __start__stack:
      00AC3F                         57 	.ds	1
                                     58 
                                     59 ;--------------------------------------------------------
                                     60 ; absolute external ram data
                                     61 ;--------------------------------------------------------
                                     62 	.area DABS (ABS)
                                     63 
                                     64 ; default segment ordering for linker
                                     65 	.area HOME
                                     66 	.area GSINIT
                                     67 	.area GSFINAL
                                     68 	.area CONST
                                     69 	.area INITIALIZER
                                     70 	.area CODE
                                     71 
                                     72 ;--------------------------------------------------------
                                     73 ; interrupt vector 
                                     74 ;--------------------------------------------------------
                                     75 	.area HOME
      008000                         76 __interrupt_vect:
      008000 82 00 80 6F             77 	int s_GSINIT ; reset
      008004 82 00 84 DA             78 	int _TRAP_IRQHandler ; trap
      008008 82 00 84 DB             79 	int _TLI_IRQHandler ; int0
      00800C 82 00 84 DC             80 	int _AWU_IRQHandler ; int1
      008010 82 00 84 DD             81 	int _CLK_IRQHandler ; int2
      008014 82 00 84 DE             82 	int _EXTI_PORTA_IRQHandler ; int3
      008018 82 00 84 DF             83 	int _EXTI_PORTB_IRQHandler ; int4
      00801C 82 00 84 E0             84 	int _EXTI_PORTC_IRQHandler ; int5
      008020 82 00 84 E1             85 	int _EXTI_PORTD_IRQHandler ; int6
      008024 82 00 00 00             86 	int 0x000000 ; int7
      008028 82 00 84 E2             87 	int _CAN_RX_IRQHandler ; int8
      00802C 82 00 84 E3             88 	int _CAN_TX_IRQHandler ; int9
      008030 82 00 84 E4             89 	int _SPI_IRQHandler ; int10
      008034 82 00 84 E5             90 	int _TIM1_UPD_OVF_TRG_BRK_IRQHandler ; int11
      008038 82 00 84 E6             91 	int _TIM1_CAP_COM_IRQHandler ; int12
      00803C 82 00 84 E7             92 	int _TIM2_UPD_OVF_BRK_IRQHandler ; int13
      008040 82 00 84 E8             93 	int _TIM2_CAP_COM_IRQHandler ; int14
      008044 82 00 84 E9             94 	int _TIM3_UPD_OVF_BRK_IRQHandler ; int15
      008048 82 00 84 EA             95 	int _TIM3_CAP_COM_IRQHandler ; int16
      00804C 82 00 84 EB             96 	int _UART1_TX_IRQHandler ; int17
      008050 82 00 84 EC             97 	int _UART1_RX_IRQHandler ; int18
      008054 82 00 84 ED             98 	int _I2C_IRQHandler ; int19
      008058 82 00 84 EE             99 	int _UART3_TX_IRQHandler ; int20
      00805C 82 00 84 EF            100 	int _UART3_RX_IRQHandler ; int21
      008060 82 00 84 F0            101 	int _ADC2_IRQHandler ; int22
      008064 82 00 84 F1            102 	int _TIM4_UPD_OVF_IRQHandler ; int23
      008068 82 00 84 F2            103 	int _EEPROM_EEC_IRQHandler ; int24
                                    104 ;--------------------------------------------------------
                                    105 ; global & static initialisations
                                    106 ;--------------------------------------------------------
                                    107 	.area HOME
                                    108 	.area GSINIT
                                    109 	.area GSFINAL
                                    110 	.area GSINIT
      00806F                        111 __sdcc_init_data:
                                    112 ; stm8_genXINIT() start
      00806F AE 00 02         [ 2]  113 	ldw x, #l_DATA
      008072 27 07            [ 1]  114 	jreq	00002$
      008074                        115 00001$:
      008074 72 4F 00 00      [ 1]  116 	clr (s_DATA - 1, x)
      008078 5A               [ 2]  117 	decw x
      008079 26 F9            [ 1]  118 	jrne	00001$
      00807B                        119 00002$:
      00807B AE 00 08         [ 2]  120 	ldw	x, #l_INITIALIZER
      00807E 27 09            [ 1]  121 	jreq	00004$
      008080                        122 00003$:
      008080 D6 81 77         [ 1]  123 	ld	a, (s_INITIALIZER - 1, x)
      008083 D7 00 02         [ 1]  124 	ld	(s_INITIALIZED - 1, x), a
      008086 5A               [ 2]  125 	decw	x
      008087 26 F7            [ 1]  126 	jrne	00003$
      008089                        127 00004$:
                                    128 ; stm8_genXINIT() end
                                    129 	.area GSFINAL
      008089 CC 80 6C         [ 2]  130 	jp	__sdcc_program_startup
                                    131 ;--------------------------------------------------------
                                    132 ; Home
                                    133 ;--------------------------------------------------------
                                    134 	.area HOME
                                    135 	.area HOME
      00806C                        136 __sdcc_program_startup:
      00806C CC 82 B6         [ 2]  137 	jp	_main
                                    138 ;	return from main will return to caller
                                    139 ;--------------------------------------------------------
                                    140 ; code
                                    141 ;--------------------------------------------------------
                                    142 	.area CODE
                           000000   143 	Smain$prepocet$0 ==.
                                    144 ;	app/src/main.c: 14: void prepocet(void){
                                    145 ;	-----------------------------------------
                                    146 ;	 function prepocet
                                    147 ;	-----------------------------------------
      00825F                        148 _prepocet:
                           000000   149 	Smain$prepocet$1 ==.
                           000000   150 	Smain$prepocet$2 ==.
                                    151 ;	app/src/main.c: 16: a = kroky % 10;
      00825F 4B 0A            [ 1]  152 	push	#0x0a
                           000002   153 	Smain$prepocet$3 ==.
      008261 5F               [ 1]  154 	clrw	x
      008262 89               [ 2]  155 	pushw	x
                           000004   156 	Smain$prepocet$4 ==.
      008263 4B 00            [ 1]  157 	push	#0x00
                           000006   158 	Smain$prepocet$5 ==.
      008265 CE 00 09         [ 2]  159 	ldw	x, _kroky+2
      008268 89               [ 2]  160 	pushw	x
                           00000A   161 	Smain$prepocet$6 ==.
      008269 CE 00 07         [ 2]  162 	ldw	x, _kroky+0
      00826C 89               [ 2]  163 	pushw	x
                           00000E   164 	Smain$prepocet$7 ==.
      00826D CD AA E5         [ 4]  165 	call	__modulong
      008270 5B 08            [ 2]  166 	addw	sp, #8
                           000013   167 	Smain$prepocet$8 ==.
      008272 9F               [ 1]  168 	ld	a, xl
      008273 C7 00 01         [ 1]  169 	ld	_a+0, a
                           000017   170 	Smain$prepocet$9 ==.
                                    171 ;	app/src/main.c: 17: b = kroky - a;
      008276 C6 00 0A         [ 1]  172 	ld	a, _kroky+3
      008279 C0 00 01         [ 1]  173 	sub	a, _a+0
      00827C C7 00 02         [ 1]  174 	ld	_b+0, a
                           000020   175 	Smain$prepocet$10 ==.
                                    176 ;	app/src/main.c: 18: b = b/10;
      00827F 5F               [ 1]  177 	clrw	x
      008280 C6 00 02         [ 1]  178 	ld	a, _b+0
      008283 97               [ 1]  179 	ld	xl, a
      008284 4B 0A            [ 1]  180 	push	#0x0a
                           000027   181 	Smain$prepocet$11 ==.
      008286 4B 00            [ 1]  182 	push	#0x00
                           000029   183 	Smain$prepocet$12 ==.
      008288 89               [ 2]  184 	pushw	x
                           00002A   185 	Smain$prepocet$13 ==.
      008289 CD AC 2B         [ 4]  186 	call	__divsint
      00828C 5B 04            [ 2]  187 	addw	sp, #4
                           00002F   188 	Smain$prepocet$14 ==.
      00828E 9F               [ 1]  189 	ld	a, xl
      00828F C7 00 02         [ 1]  190 	ld	_b+0, a
                           000033   191 	Smain$prepocet$15 ==.
                                    192 ;	app/src/main.c: 19: GPIO_Write(GPIOB,a);
      008292 3B 00 01         [ 1]  193 	push	_a+0
                           000036   194 	Smain$prepocet$16 ==.
      008295 4B 05            [ 1]  195 	push	#0x05
                           000038   196 	Smain$prepocet$17 ==.
      008297 4B 50            [ 1]  197 	push	#0x50
                           00003A   198 	Smain$prepocet$18 ==.
      008299 CD 8E 9F         [ 4]  199 	call	_GPIO_Write
      00829C 5B 03            [ 2]  200 	addw	sp, #3
                           00003F   201 	Smain$prepocet$19 ==.
                           00003F   202 	Smain$prepocet$20 ==.
                                    203 ;	app/src/main.c: 20: GPIO_Write(GPIOG,b);
      00829E 3B 00 02         [ 1]  204 	push	_b+0
                           000042   205 	Smain$prepocet$21 ==.
      0082A1 4B 1E            [ 1]  206 	push	#0x1e
                           000044   207 	Smain$prepocet$22 ==.
      0082A3 4B 50            [ 1]  208 	push	#0x50
                           000046   209 	Smain$prepocet$23 ==.
      0082A5 CD 8E 9F         [ 4]  210 	call	_GPIO_Write
      0082A8 5B 03            [ 2]  211 	addw	sp, #3
                           00004B   212 	Smain$prepocet$24 ==.
                           00004B   213 	Smain$prepocet$25 ==.
                                    214 ;	app/src/main.c: 21: GPIO_WriteLow(GPIOG, GPIO_PIN_3);
      0082AA 4B 08            [ 1]  215 	push	#0x08
                           00004D   216 	Smain$prepocet$26 ==.
      0082AC 4B 1E            [ 1]  217 	push	#0x1e
                           00004F   218 	Smain$prepocet$27 ==.
      0082AE 4B 50            [ 1]  219 	push	#0x50
                           000051   220 	Smain$prepocet$28 ==.
      0082B0 CD 8E AC         [ 4]  221 	call	_GPIO_WriteLow
      0082B3 5B 03            [ 2]  222 	addw	sp, #3
                           000056   223 	Smain$prepocet$29 ==.
                           000056   224 	Smain$prepocet$30 ==.
                                    225 ;	app/src/main.c: 22: }
                           000056   226 	Smain$prepocet$31 ==.
                           000056   227 	XG$prepocet$0$0 ==.
      0082B5 81               [ 4]  228 	ret
                           000057   229 	Smain$prepocet$32 ==.
                           000057   230 	Smain$main$33 ==.
                                    231 ;	app/src/main.c: 35: void main(void)
                                    232 ;	-----------------------------------------
                                    233 ;	 function main
                                    234 ;	-----------------------------------------
      0082B6                        235 _main:
                           000057   236 	Smain$main$34 ==.
      0082B6 89               [ 2]  237 	pushw	x
                           000058   238 	Smain$main$35 ==.
                           000058   239 	Smain$main$36 ==.
                                    240 ;	app/src/main.c: 38: CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // FREQ MCU 16MHz
      0082B7 4B 00            [ 1]  241 	push	#0x00
                           00005A   242 	Smain$main$37 ==.
      0082B9 CD 88 6D         [ 4]  243 	call	_CLK_HSIPrescalerConfig
      0082BC 84               [ 1]  244 	pop	a
                           00005E   245 	Smain$main$38 ==.
                           00005E   246 	Smain$main$39 ==.
                                    247 ;	app/src/main.c: 40: GPIO_Init(GPIOG, GPIO_PIN_0, GPIO_MODE_OUT_PP_LOW_SLOW);    //inicializace GPIOA = 1. SEGMENT
      0082BD 4B C0            [ 1]  248 	push	#0xc0
                           000060   249 	Smain$main$40 ==.
      0082BF 4B 01            [ 1]  250 	push	#0x01
                           000062   251 	Smain$main$41 ==.
      0082C1 4B 1E            [ 1]  252 	push	#0x1e
                           000064   253 	Smain$main$42 ==.
      0082C3 4B 50            [ 1]  254 	push	#0x50
                           000066   255 	Smain$main$43 ==.
      0082C5 CD 8D B3         [ 4]  256 	call	_GPIO_Init
      0082C8 5B 04            [ 2]  257 	addw	sp, #4
                           00006B   258 	Smain$main$44 ==.
                           00006B   259 	Smain$main$45 ==.
                                    260 ;	app/src/main.c: 41: GPIO_Init(GPIOG, GPIO_PIN_1, GPIO_MODE_OUT_PP_LOW_SLOW);
      0082CA 4B C0            [ 1]  261 	push	#0xc0
                           00006D   262 	Smain$main$46 ==.
      0082CC 4B 02            [ 1]  263 	push	#0x02
                           00006F   264 	Smain$main$47 ==.
      0082CE 4B 1E            [ 1]  265 	push	#0x1e
                           000071   266 	Smain$main$48 ==.
      0082D0 4B 50            [ 1]  267 	push	#0x50
                           000073   268 	Smain$main$49 ==.
      0082D2 CD 8D B3         [ 4]  269 	call	_GPIO_Init
      0082D5 5B 04            [ 2]  270 	addw	sp, #4
                           000078   271 	Smain$main$50 ==.
                           000078   272 	Smain$main$51 ==.
                                    273 ;	app/src/main.c: 42: GPIO_Init(GPIOG, GPIO_PIN_2, GPIO_MODE_OUT_PP_LOW_SLOW);
      0082D7 4B C0            [ 1]  274 	push	#0xc0
                           00007A   275 	Smain$main$52 ==.
      0082D9 4B 04            [ 1]  276 	push	#0x04
                           00007C   277 	Smain$main$53 ==.
      0082DB 4B 1E            [ 1]  278 	push	#0x1e
                           00007E   279 	Smain$main$54 ==.
      0082DD 4B 50            [ 1]  280 	push	#0x50
                           000080   281 	Smain$main$55 ==.
      0082DF CD 8D B3         [ 4]  282 	call	_GPIO_Init
      0082E2 5B 04            [ 2]  283 	addw	sp, #4
                           000085   284 	Smain$main$56 ==.
                           000085   285 	Smain$main$57 ==.
                                    286 ;	app/src/main.c: 43: GPIO_Init(GPIOG, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);
      0082E4 4B C0            [ 1]  287 	push	#0xc0
                           000087   288 	Smain$main$58 ==.
      0082E6 4B 08            [ 1]  289 	push	#0x08
                           000089   290 	Smain$main$59 ==.
      0082E8 4B 1E            [ 1]  291 	push	#0x1e
                           00008B   292 	Smain$main$60 ==.
      0082EA 4B 50            [ 1]  293 	push	#0x50
                           00008D   294 	Smain$main$61 ==.
      0082EC CD 8D B3         [ 4]  295 	call	_GPIO_Init
      0082EF 5B 04            [ 2]  296 	addw	sp, #4
                           000092   297 	Smain$main$62 ==.
                           000092   298 	Smain$main$63 ==.
                                    299 ;	app/src/main.c: 45: GPIO_Init(GPIOB, GPIO_PIN_0, GPIO_MODE_OUT_PP_LOW_SLOW);     //inicializace GPIOB = 2. SEGMENT
      0082F1 4B C0            [ 1]  300 	push	#0xc0
                           000094   301 	Smain$main$64 ==.
      0082F3 4B 01            [ 1]  302 	push	#0x01
                           000096   303 	Smain$main$65 ==.
      0082F5 4B 05            [ 1]  304 	push	#0x05
                           000098   305 	Smain$main$66 ==.
      0082F7 4B 50            [ 1]  306 	push	#0x50
                           00009A   307 	Smain$main$67 ==.
      0082F9 CD 8D B3         [ 4]  308 	call	_GPIO_Init
      0082FC 5B 04            [ 2]  309 	addw	sp, #4
                           00009F   310 	Smain$main$68 ==.
                           00009F   311 	Smain$main$69 ==.
                                    312 ;	app/src/main.c: 46: GPIO_Init(GPIOB, GPIO_PIN_1, GPIO_MODE_OUT_PP_LOW_SLOW);
      0082FE 4B C0            [ 1]  313 	push	#0xc0
                           0000A1   314 	Smain$main$70 ==.
      008300 4B 02            [ 1]  315 	push	#0x02
                           0000A3   316 	Smain$main$71 ==.
      008302 4B 05            [ 1]  317 	push	#0x05
                           0000A5   318 	Smain$main$72 ==.
      008304 4B 50            [ 1]  319 	push	#0x50
                           0000A7   320 	Smain$main$73 ==.
      008306 CD 8D B3         [ 4]  321 	call	_GPIO_Init
      008309 5B 04            [ 2]  322 	addw	sp, #4
                           0000AC   323 	Smain$main$74 ==.
                           0000AC   324 	Smain$main$75 ==.
                                    325 ;	app/src/main.c: 47: GPIO_Init(GPIOB, GPIO_PIN_2, GPIO_MODE_OUT_PP_LOW_SLOW);
      00830B 4B C0            [ 1]  326 	push	#0xc0
                           0000AE   327 	Smain$main$76 ==.
      00830D 4B 04            [ 1]  328 	push	#0x04
                           0000B0   329 	Smain$main$77 ==.
      00830F 4B 05            [ 1]  330 	push	#0x05
                           0000B2   331 	Smain$main$78 ==.
      008311 4B 50            [ 1]  332 	push	#0x50
                           0000B4   333 	Smain$main$79 ==.
      008313 CD 8D B3         [ 4]  334 	call	_GPIO_Init
      008316 5B 04            [ 2]  335 	addw	sp, #4
                           0000B9   336 	Smain$main$80 ==.
                           0000B9   337 	Smain$main$81 ==.
                                    338 ;	app/src/main.c: 48: GPIO_Init(GPIOB, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);
      008318 4B C0            [ 1]  339 	push	#0xc0
                           0000BB   340 	Smain$main$82 ==.
      00831A 4B 08            [ 1]  341 	push	#0x08
                           0000BD   342 	Smain$main$83 ==.
      00831C 4B 05            [ 1]  343 	push	#0x05
                           0000BF   344 	Smain$main$84 ==.
      00831E 4B 50            [ 1]  345 	push	#0x50
                           0000C1   346 	Smain$main$85 ==.
      008320 CD 8D B3         [ 4]  347 	call	_GPIO_Init
      008323 5B 04            [ 2]  348 	addw	sp, #4
                           0000C6   349 	Smain$main$86 ==.
                           0000C6   350 	Smain$main$87 ==.
                                    351 ;	app/src/main.c: 50: GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);
      008325 4B C0            [ 1]  352 	push	#0xc0
                           0000C8   353 	Smain$main$88 ==.
      008327 4B 20            [ 1]  354 	push	#0x20
                           0000CA   355 	Smain$main$89 ==.
      008329 4B 0A            [ 1]  356 	push	#0x0a
                           0000CC   357 	Smain$main$90 ==.
      00832B 4B 50            [ 1]  358 	push	#0x50
                           0000CE   359 	Smain$main$91 ==.
      00832D CD 8D B3         [ 4]  360 	call	_GPIO_Init
      008330 5B 04            [ 2]  361 	addw	sp, #4
                           0000D3   362 	Smain$main$92 ==.
                           0000D3   363 	Smain$main$93 ==.
                                    364 ;	app/src/main.c: 51: GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
      008332 4B 20            [ 1]  365 	push	#0x20
                           0000D5   366 	Smain$main$94 ==.
      008334 4B 0A            [ 1]  367 	push	#0x0a
                           0000D7   368 	Smain$main$95 ==.
      008336 4B 50            [ 1]  369 	push	#0x50
                           0000D9   370 	Smain$main$96 ==.
      008338 CD 8E A5         [ 4]  371 	call	_GPIO_WriteHigh
      00833B 5B 03            [ 2]  372 	addw	sp, #3
                           0000DE   373 	Smain$main$97 ==.
                           0000DE   374 	Smain$main$98 ==.
                                    375 ;	app/src/main.c: 53: GPIO_Init(GPIOE, GPIO_PIN_4, GPIO_MODE_IN_FL_IT);        //inicializace Senzoru
      00833D 4B 20            [ 1]  376 	push	#0x20
                           0000E0   377 	Smain$main$99 ==.
      00833F 4B 10            [ 1]  378 	push	#0x10
                           0000E2   379 	Smain$main$100 ==.
      008341 4B 14            [ 1]  380 	push	#0x14
                           0000E4   381 	Smain$main$101 ==.
      008343 4B 50            [ 1]  382 	push	#0x50
                           0000E6   383 	Smain$main$102 ==.
      008345 CD 8D B3         [ 4]  384 	call	_GPIO_Init
      008348 5B 04            [ 2]  385 	addw	sp, #4
                           0000EB   386 	Smain$main$103 ==.
                           0000EB   387 	Smain$main$104 ==.
                                    388 ;	app/src/main.c: 57: jas_init();
      00834A CD 82 2E         [ 4]  389 	call	_jas_init
                           0000EE   390 	Smain$main$105 ==.
                                    391 ;	app/src/main.c: 58: pc_init();
      00834D CD 81 80         [ 4]  392 	call	_pc_init
                           0000F1   393 	Smain$main$106 ==.
                                    394 ;	app/src/main.c: 59: delay_05s_init();
      008350 CD 81 DD         [ 4]  395 	call	_delay_05s_init
                           0000F4   396 	Smain$main$107 ==.
                           0000F4   397 	Smain$main$108 ==.
                                    398 ;	app/src/main.c: 62: uint8_t beta = 0;
      008353 0F 01            [ 1]  399 	clr	(0x01, sp)
                           0000F6   400 	Smain$main$109 ==.
                           0000F6   401 	Smain$main$110 ==.
                                    402 ;	app/src/main.c: 64: pc_write_string("\n6 >>> +krok/n4 >>> -krok");
      008355 4B 8C            [ 1]  403 	push	#<(___str_0+0)
                           0000F8   404 	Smain$main$111 ==.
      008357 4B 80            [ 1]  405 	push	#((___str_0+0) >> 8)
                           0000FA   406 	Smain$main$112 ==.
      008359 CD 81 A9         [ 4]  407 	call	_pc_write_string
      00835C 85               [ 2]  408 	popw	x
                           0000FE   409 	Smain$main$113 ==.
                           0000FE   410 	Smain$main$114 ==.
                                    411 ;	app/src/main.c: 65: while (1)
      00835D                        412 00127$:
                           0000FE   413 	Smain$main$115 ==.
                           0000FE   414 	Smain$main$116 ==.
                                    415 ;	app/src/main.c: 69: if(UART1_GetFlagStatus(UART1_FLAG_RXNE))
      00835D 4B 20            [ 1]  416 	push	#0x20
                           000100   417 	Smain$main$117 ==.
      00835F 4B 00            [ 1]  418 	push	#0x00
                           000102   419 	Smain$main$118 ==.
      008361 CD A9 29         [ 4]  420 	call	_UART1_GetFlagStatus
      008364 85               [ 2]  421 	popw	x
                           000106   422 	Smain$main$119 ==.
      008365 4D               [ 1]  423 	tnz	a
      008366 26 03            [ 1]  424 	jrne	00186$
      008368 CC 84 59         [ 2]  425 	jp	00117$
      00836B                        426 00186$:
                           00010C   427 	Smain$main$120 ==.
                           00010C   428 	Smain$main$121 ==.
                                    429 ;	app/src/main.c: 71: key = UART1_ReceiveData8();
      00836B CD A8 AD         [ 4]  430 	call	_UART1_ReceiveData8
                           00010F   431 	Smain$main$122 ==.
                                    432 ;	app/src/main.c: 72: if(key == ('5'))
      00836E A1 35            [ 1]  433 	cp	a, #0x35
      008370 27 03            [ 1]  434 	jreq	00189$
      008372 CC 83 C3         [ 2]  435 	jp	00114$
      008375                        436 00189$:
                           000116   437 	Smain$main$123 ==.
                           000116   438 	Smain$main$124 ==.
                           000116   439 	Smain$main$125 ==.
                                    440 ;	app/src/main.c: 74: kroky = 99;
      008375 AE 00 63         [ 2]  441 	ldw	x, #0x0063
      008378 CF 00 09         [ 2]  442 	ldw	_kroky+2, x
      00837B 5F               [ 1]  443 	clrw	x
      00837C CF 00 07         [ 2]  444 	ldw	_kroky+0, x
                           000120   445 	Smain$main$126 ==.
                                    446 ;	app/src/main.c: 75: delay_05s(1);
      00837F 4B 01            [ 1]  447 	push	#0x01
                           000122   448 	Smain$main$127 ==.
      008381 5F               [ 1]  449 	clrw	x
      008382 89               [ 2]  450 	pushw	x
                           000124   451 	Smain$main$128 ==.
      008383 4B 00            [ 1]  452 	push	#0x00
                           000126   453 	Smain$main$129 ==.
      008385 CD 81 EF         [ 4]  454 	call	_delay_05s
      008388 5B 04            [ 2]  455 	addw	sp, #4
                           00012B   456 	Smain$main$130 ==.
                           00012B   457 	Smain$main$131 ==.
                                    458 ;	app/src/main.c: 76: kroky = 0;
      00838A 5F               [ 1]  459 	clrw	x
      00838B CF 00 09         [ 2]  460 	ldw	_kroky+2, x
      00838E CF 00 07         [ 2]  461 	ldw	_kroky+0, x
                           000132   462 	Smain$main$132 ==.
                                    463 ;	app/src/main.c: 77: delay_05s(1);
      008391 4B 01            [ 1]  464 	push	#0x01
                           000134   465 	Smain$main$133 ==.
      008393 5F               [ 1]  466 	clrw	x
      008394 89               [ 2]  467 	pushw	x
                           000136   468 	Smain$main$134 ==.
      008395 4B 00            [ 1]  469 	push	#0x00
                           000138   470 	Smain$main$135 ==.
      008397 CD 81 EF         [ 4]  471 	call	_delay_05s
      00839A 5B 04            [ 2]  472 	addw	sp, #4
                           00013D   473 	Smain$main$136 ==.
                           00013D   474 	Smain$main$137 ==.
                                    475 ;	app/src/main.c: 78: kroky = 99;
      00839C AE 00 63         [ 2]  476 	ldw	x, #0x0063
      00839F CF 00 09         [ 2]  477 	ldw	_kroky+2, x
      0083A2 5F               [ 1]  478 	clrw	x
      0083A3 CF 00 07         [ 2]  479 	ldw	_kroky+0, x
                           000147   480 	Smain$main$138 ==.
                                    481 ;	app/src/main.c: 79: delay_05s(1);
      0083A6 4B 01            [ 1]  482 	push	#0x01
                           000149   483 	Smain$main$139 ==.
      0083A8 5F               [ 1]  484 	clrw	x
      0083A9 89               [ 2]  485 	pushw	x
                           00014B   486 	Smain$main$140 ==.
      0083AA 4B 00            [ 1]  487 	push	#0x00
                           00014D   488 	Smain$main$141 ==.
      0083AC CD 81 EF         [ 4]  489 	call	_delay_05s
      0083AF 5B 04            [ 2]  490 	addw	sp, #4
                           000152   491 	Smain$main$142 ==.
                           000152   492 	Smain$main$143 ==.
                                    493 ;	app/src/main.c: 80: kroky = 0;
      0083B1 5F               [ 1]  494 	clrw	x
      0083B2 CF 00 09         [ 2]  495 	ldw	_kroky+2, x
      0083B5 CF 00 07         [ 2]  496 	ldw	_kroky+0, x
                           000159   497 	Smain$main$144 ==.
                                    498 ;	app/src/main.c: 81: pc_write_string("Vynulování");
      0083B8 4B A6            [ 1]  499 	push	#<(___str_1+0)
                           00015B   500 	Smain$main$145 ==.
      0083BA 4B 80            [ 1]  501 	push	#((___str_1+0) >> 8)
                           00015D   502 	Smain$main$146 ==.
      0083BC CD 81 A9         [ 4]  503 	call	_pc_write_string
      0083BF 85               [ 2]  504 	popw	x
                           000161   505 	Smain$main$147 ==.
                           000161   506 	Smain$main$148 ==.
      0083C0 CC 84 59         [ 2]  507 	jp	00117$
      0083C3                        508 00114$:
                           000164   509 	Smain$main$149 ==.
                                    510 ;	app/src/main.c: 83: else if(key == ('6'))
      0083C3 A1 36            [ 1]  511 	cp	a, #0x36
      0083C5 26 20            [ 1]  512 	jrne	00111$
                           000168   513 	Smain$main$150 ==.
                           000168   514 	Smain$main$151 ==.
                           000168   515 	Smain$main$152 ==.
                                    516 ;	app/src/main.c: 85: kroky = kroky + 1;
      0083C7 CE 00 09         [ 2]  517 	ldw	x, _kroky+2
      0083CA 1C 00 01         [ 2]  518 	addw	x, #0x0001
      0083CD 90 CE 00 07      [ 2]  519 	ldw	y, _kroky+0
      0083D1 24 02            [ 1]  520 	jrnc	00193$
      0083D3 90 5C            [ 1]  521 	incw	y
      0083D5                        522 00193$:
      0083D5 CF 00 09         [ 2]  523 	ldw	_kroky+2, x
      0083D8 90 CF 00 07      [ 2]  524 	ldw	_kroky+0, y
                           00017D   525 	Smain$main$153 ==.
                                    526 ;	app/src/main.c: 86: pc_write_string("+krok");
      0083DC 4B B3            [ 1]  527 	push	#<(___str_2+0)
                           00017F   528 	Smain$main$154 ==.
      0083DE 4B 80            [ 1]  529 	push	#((___str_2+0) >> 8)
                           000181   530 	Smain$main$155 ==.
      0083E0 CD 81 A9         [ 4]  531 	call	_pc_write_string
      0083E3 85               [ 2]  532 	popw	x
                           000185   533 	Smain$main$156 ==.
                           000185   534 	Smain$main$157 ==.
      0083E4 CC 84 59         [ 2]  535 	jp	00117$
      0083E7                        536 00111$:
                           000188   537 	Smain$main$158 ==.
                                    538 ;	app/src/main.c: 88: else if(key == ('4'))
      0083E7 A1 34            [ 1]  539 	cp	a, #0x34
      0083E9 26 20            [ 1]  540 	jrne	00108$
                           00018C   541 	Smain$main$159 ==.
                           00018C   542 	Smain$main$160 ==.
                           00018C   543 	Smain$main$161 ==.
                                    544 ;	app/src/main.c: 90: kroky = kroky - 1;
      0083EB CE 00 09         [ 2]  545 	ldw	x, _kroky+2
      0083EE 1D 00 01         [ 2]  546 	subw	x, #0x0001
      0083F1 90 CE 00 07      [ 2]  547 	ldw	y, _kroky+0
      0083F5 24 02            [ 1]  548 	jrnc	00197$
      0083F7 90 5A            [ 2]  549 	decw	y
      0083F9                        550 00197$:
      0083F9 CF 00 09         [ 2]  551 	ldw	_kroky+2, x
      0083FC 90 CF 00 07      [ 2]  552 	ldw	_kroky+0, y
                           0001A1   553 	Smain$main$162 ==.
                                    554 ;	app/src/main.c: 91: pc_write_string("-krok");
      008400 4B B9            [ 1]  555 	push	#<(___str_3+0)
                           0001A3   556 	Smain$main$163 ==.
      008402 4B 80            [ 1]  557 	push	#((___str_3+0) >> 8)
                           0001A5   558 	Smain$main$164 ==.
      008404 CD 81 A9         [ 4]  559 	call	_pc_write_string
      008407 85               [ 2]  560 	popw	x
                           0001A9   561 	Smain$main$165 ==.
                           0001A9   562 	Smain$main$166 ==.
      008408 CC 84 59         [ 2]  563 	jp	00117$
      00840B                        564 00108$:
                           0001AC   565 	Smain$main$167 ==.
                                    566 ;	app/src/main.c: 93: else if(key == ('2'))
      00840B A1 32            [ 1]  567 	cp	a, #0x32
      00840D 26 1F            [ 1]  568 	jrne	00105$
                           0001B0   569 	Smain$main$168 ==.
                           0001B0   570 	Smain$main$169 ==.
                           0001B0   571 	Smain$main$170 ==.
                                    572 ;	app/src/main.c: 95: js = js - 10;
      00840F CE 00 05         [ 2]  573 	ldw	x, _js+2
      008412 1D 00 0A         [ 2]  574 	subw	x, #0x000a
      008415 90 CE 00 03      [ 2]  575 	ldw	y, _js+0
      008419 24 02            [ 1]  576 	jrnc	00201$
      00841B 90 5A            [ 2]  577 	decw	y
      00841D                        578 00201$:
      00841D CF 00 05         [ 2]  579 	ldw	_js+2, x
      008420 90 CF 00 03      [ 2]  580 	ldw	_js+0, y
                           0001C5   581 	Smain$main$171 ==.
                                    582 ;	app/src/main.c: 96: pc_write_string("-jas");
      008424 4B BF            [ 1]  583 	push	#<(___str_4+0)
                           0001C7   584 	Smain$main$172 ==.
      008426 4B 80            [ 1]  585 	push	#((___str_4+0) >> 8)
                           0001C9   586 	Smain$main$173 ==.
      008428 CD 81 A9         [ 4]  587 	call	_pc_write_string
      00842B 85               [ 2]  588 	popw	x
                           0001CD   589 	Smain$main$174 ==.
                           0001CD   590 	Smain$main$175 ==.
      00842C 20 2B            [ 2]  591 	jra	00117$
      00842E                        592 00105$:
                           0001CF   593 	Smain$main$176 ==.
                                    594 ;	app/src/main.c: 98: else if(key == ('8'))
      00842E A1 38            [ 1]  595 	cp	a, #0x38
      008430 26 1F            [ 1]  596 	jrne	00102$
                           0001D3   597 	Smain$main$177 ==.
                           0001D3   598 	Smain$main$178 ==.
                           0001D3   599 	Smain$main$179 ==.
                                    600 ;	app/src/main.c: 100: js = js + 10;
      008432 CE 00 05         [ 2]  601 	ldw	x, _js+2
      008435 1C 00 0A         [ 2]  602 	addw	x, #0x000a
      008438 90 CE 00 03      [ 2]  603 	ldw	y, _js+0
      00843C 24 02            [ 1]  604 	jrnc	00205$
      00843E 90 5C            [ 1]  605 	incw	y
      008440                        606 00205$:
      008440 CF 00 05         [ 2]  607 	ldw	_js+2, x
      008443 90 CF 00 03      [ 2]  608 	ldw	_js+0, y
                           0001E8   609 	Smain$main$180 ==.
                                    610 ;	app/src/main.c: 101: pc_write_string("+jas");
      008447 4B C4            [ 1]  611 	push	#<(___str_5+0)
                           0001EA   612 	Smain$main$181 ==.
      008449 4B 80            [ 1]  613 	push	#((___str_5+0) >> 8)
                           0001EC   614 	Smain$main$182 ==.
      00844B CD 81 A9         [ 4]  615 	call	_pc_write_string
      00844E 85               [ 2]  616 	popw	x
                           0001F0   617 	Smain$main$183 ==.
                           0001F0   618 	Smain$main$184 ==.
      00844F 20 08            [ 2]  619 	jra	00117$
      008451                        620 00102$:
                           0001F2   621 	Smain$main$185 ==.
                           0001F2   622 	Smain$main$186 ==.
                                    623 ;	app/src/main.c: 105: pc_write_string("UNKNOWN");
      008451 4B C9            [ 1]  624 	push	#<(___str_6+0)
                           0001F4   625 	Smain$main$187 ==.
      008453 4B 80            [ 1]  626 	push	#((___str_6+0) >> 8)
                           0001F6   627 	Smain$main$188 ==.
      008455 CD 81 A9         [ 4]  628 	call	_pc_write_string
      008458 85               [ 2]  629 	popw	x
                           0001FA   630 	Smain$main$189 ==.
                           0001FA   631 	Smain$main$190 ==.
      008459                        632 00117$:
                           0001FA   633 	Smain$main$191 ==.
                                    634 ;	app/src/main.c: 108: alpha = GPIO_ReadInputPin(GPIOE, GPIO_PIN_4);
      008459 4B 10            [ 1]  635 	push	#0x10
                           0001FC   636 	Smain$main$192 ==.
      00845B 4B 14            [ 1]  637 	push	#0x14
                           0001FE   638 	Smain$main$193 ==.
      00845D 4B 50            [ 1]  639 	push	#0x50
                           000200   640 	Smain$main$194 ==.
      00845F CD 8E CA         [ 4]  641 	call	_GPIO_ReadInputPin
      008462 5B 03            [ 2]  642 	addw	sp, #3
                           000205   643 	Smain$main$195 ==.
      008464 6B 02            [ 1]  644 	ld	(0x02, sp), a
                           000207   645 	Smain$main$196 ==.
                                    646 ;	app/src/main.c: 109: if(alpha==0)
      008466 4D               [ 1]  647 	tnz	a
      008467 27 03            [ 1]  648 	jreq	00206$
      008469 CC 84 91         [ 2]  649 	jp	00121$
      00846C                        650 00206$:
                           00020D   651 	Smain$main$197 ==.
                           00020D   652 	Smain$main$198 ==.
                                    653 ;	app/src/main.c: 111: if(beta == 1)
      00846C 7B 01            [ 1]  654 	ld	a, (0x01, sp)
      00846E 4A               [ 1]  655 	dec	a
      00846F 26 20            [ 1]  656 	jrne	00121$
                           000212   657 	Smain$main$199 ==.
                           000212   658 	Smain$main$200 ==.
                           000212   659 	Smain$main$201 ==.
                                    660 ;	app/src/main.c: 114: beta = 0;
      008471 0F 01            [ 1]  661 	clr	(0x01, sp)
                           000214   662 	Smain$main$202 ==.
                                    663 ;	app/src/main.c: 115: GPIO_WriteReverse(GPIOC,GPIO_PIN_5);
      008473 4B 20            [ 1]  664 	push	#0x20
                           000216   665 	Smain$main$203 ==.
      008475 4B 0A            [ 1]  666 	push	#0x0a
                           000218   667 	Smain$main$204 ==.
      008477 4B 50            [ 1]  668 	push	#0x50
                           00021A   669 	Smain$main$205 ==.
      008479 CD 8E BA         [ 4]  670 	call	_GPIO_WriteReverse
      00847C 5B 03            [ 2]  671 	addw	sp, #3
                           00021F   672 	Smain$main$206 ==.
                           00021F   673 	Smain$main$207 ==.
                                    674 ;	app/src/main.c: 116: pc_write_string("+krok");
      00847E 4B B3            [ 1]  675 	push	#<(___str_2+0)
                           000221   676 	Smain$main$208 ==.
      008480 4B 80            [ 1]  677 	push	#((___str_2+0) >> 8)
                           000223   678 	Smain$main$209 ==.
      008482 CD 81 A9         [ 4]  679 	call	_pc_write_string
      008485 85               [ 2]  680 	popw	x
                           000227   681 	Smain$main$210 ==.
                           000227   682 	Smain$main$211 ==.
                                    683 ;	app/src/main.c: 117: delay_05s(1);
      008486 4B 01            [ 1]  684 	push	#0x01
                           000229   685 	Smain$main$212 ==.
      008488 5F               [ 1]  686 	clrw	x
      008489 89               [ 2]  687 	pushw	x
                           00022B   688 	Smain$main$213 ==.
      00848A 4B 00            [ 1]  689 	push	#0x00
                           00022D   690 	Smain$main$214 ==.
      00848C CD 81 EF         [ 4]  691 	call	_delay_05s
      00848F 5B 04            [ 2]  692 	addw	sp, #4
                           000232   693 	Smain$main$215 ==.
                           000232   694 	Smain$main$216 ==.
      008491                        695 00121$:
                           000232   696 	Smain$main$217 ==.
                                    697 ;	app/src/main.c: 121: if(alpha>1)
      008491 7B 02            [ 1]  698 	ld	a, (0x02, sp)
      008493 A1 01            [ 1]  699 	cp	a, #0x01
      008495 22 03            [ 1]  700 	jrugt	00210$
      008497 CC 84 CA         [ 2]  701 	jp	00125$
      00849A                        702 00210$:
                           00023B   703 	Smain$main$218 ==.
                           00023B   704 	Smain$main$219 ==.
                                    705 ;	app/src/main.c: 123: if(beta == 0)
      00849A 0D 01            [ 1]  706 	tnz	(0x01, sp)
      00849C 26 2C            [ 1]  707 	jrne	00125$
                           00023F   708 	Smain$main$220 ==.
                           00023F   709 	Smain$main$221 ==.
                                    710 ;	app/src/main.c: 125: beta = 1;
      00849E A6 01            [ 1]  711 	ld	a, #0x01
      0084A0 6B 01            [ 1]  712 	ld	(0x01, sp), a
                           000243   713 	Smain$main$222 ==.
                                    714 ;	app/src/main.c: 126: kroky = kroky + 1;
      0084A2 CE 00 09         [ 2]  715 	ldw	x, _kroky+2
      0084A5 1C 00 01         [ 2]  716 	addw	x, #0x0001
      0084A8 90 CE 00 07      [ 2]  717 	ldw	y, _kroky+0
      0084AC 24 02            [ 1]  718 	jrnc	00212$
      0084AE 90 5C            [ 1]  719 	incw	y
      0084B0                        720 00212$:
      0084B0 CF 00 09         [ 2]  721 	ldw	_kroky+2, x
      0084B3 90 CF 00 07      [ 2]  722 	ldw	_kroky+0, y
                           000258   723 	Smain$main$223 ==.
                                    724 ;	app/src/main.c: 127: pc_write_string("beta");
      0084B7 4B D1            [ 1]  725 	push	#<(___str_7+0)
                           00025A   726 	Smain$main$224 ==.
      0084B9 4B 80            [ 1]  727 	push	#((___str_7+0) >> 8)
                           00025C   728 	Smain$main$225 ==.
      0084BB CD 81 A9         [ 4]  729 	call	_pc_write_string
      0084BE 85               [ 2]  730 	popw	x
                           000260   731 	Smain$main$226 ==.
                           000260   732 	Smain$main$227 ==.
                                    733 ;	app/src/main.c: 128: delay_05s(1);
      0084BF 4B 01            [ 1]  734 	push	#0x01
                           000262   735 	Smain$main$228 ==.
      0084C1 5F               [ 1]  736 	clrw	x
      0084C2 89               [ 2]  737 	pushw	x
                           000264   738 	Smain$main$229 ==.
      0084C3 4B 00            [ 1]  739 	push	#0x00
                           000266   740 	Smain$main$230 ==.
      0084C5 CD 81 EF         [ 4]  741 	call	_delay_05s
      0084C8 5B 04            [ 2]  742 	addw	sp, #4
                           00026B   743 	Smain$main$231 ==.
                           00026B   744 	Smain$main$232 ==.
      0084CA                        745 00125$:
                           00026B   746 	Smain$main$233 ==.
                                    747 ;	app/src/main.c: 133: jas(js);
      0084CA C6 00 06         [ 1]  748 	ld	a, _js+3
      0084CD 88               [ 1]  749 	push	a
                           00026F   750 	Smain$main$234 ==.
      0084CE CD 82 55         [ 4]  751 	call	_jas
      0084D1 84               [ 1]  752 	pop	a
                           000273   753 	Smain$main$235 ==.
                           000273   754 	Smain$main$236 ==.
                                    755 ;	app/src/main.c: 134: prepocet();
      0084D2 CD 82 5F         [ 4]  756 	call	_prepocet
                           000276   757 	Smain$main$237 ==.
      0084D5 CC 83 5D         [ 2]  758 	jp	00127$
                           000279   759 	Smain$main$238 ==.
                           000279   760 	Smain$main$239 ==.
                                    761 ;	app/src/main.c: 136: }
      0084D8 85               [ 2]  762 	popw	x
                           00027A   763 	Smain$main$240 ==.
                           00027A   764 	Smain$main$241 ==.
                           00027A   765 	XG$main$0$0 ==.
      0084D9 81               [ 4]  766 	ret
                           00027B   767 	Smain$main$242 ==.
                                    768 	.area CODE
                                    769 	.area CONST
                           000000   770 Fmain$__str_0$0_0$0 == .
                                    771 	.area CONST
      00808C                        772 ___str_0:
      00808C 0A                     773 	.db 0x0a
      00808D 36 20 3E 3E 3E 20 2B   774 	.ascii "6 >>> +krok/n4 >>> -krok"
             6B 72 6F 6B 2F 6E 34
             20 3E 3E 3E 20 2D 6B
             72 6F 6B
      0080A5 00                     775 	.db 0x00
                                    776 	.area CODE
                           00027B   777 Fmain$__str_1$0_0$0 == .
                                    778 	.area CONST
      0080A6                        779 ___str_1:
      0080A6 56 79 6E 75 6C 6F 76   780 	.ascii "Vynulov"
      0080AD C3                     781 	.db 0xc3
      0080AE A1                     782 	.db 0xa1
      0080AF 6E                     783 	.ascii "n"
      0080B0 C3                     784 	.db 0xc3
      0080B1 AD                     785 	.db 0xad
      0080B2 00                     786 	.db 0x00
                                    787 	.area CODE
                           00027B   788 Fmain$__str_2$0_0$0 == .
                                    789 	.area CONST
      0080B3                        790 ___str_2:
      0080B3 2B 6B 72 6F 6B         791 	.ascii "+krok"
      0080B8 00                     792 	.db 0x00
                                    793 	.area CODE
                           00027B   794 Fmain$__str_3$0_0$0 == .
                                    795 	.area CONST
      0080B9                        796 ___str_3:
      0080B9 2D 6B 72 6F 6B         797 	.ascii "-krok"
      0080BE 00                     798 	.db 0x00
                                    799 	.area CODE
                           00027B   800 Fmain$__str_4$0_0$0 == .
                                    801 	.area CONST
      0080BF                        802 ___str_4:
      0080BF 2D 6A 61 73            803 	.ascii "-jas"
      0080C3 00                     804 	.db 0x00
                                    805 	.area CODE
                           00027B   806 Fmain$__str_5$0_0$0 == .
                                    807 	.area CONST
      0080C4                        808 ___str_5:
      0080C4 2B 6A 61 73            809 	.ascii "+jas"
      0080C8 00                     810 	.db 0x00
                                    811 	.area CODE
                           00027B   812 Fmain$__str_6$0_0$0 == .
                                    813 	.area CONST
      0080C9                        814 ___str_6:
      0080C9 55 4E 4B 4E 4F 57 4E   815 	.ascii "UNKNOWN"
      0080D0 00                     816 	.db 0x00
                                    817 	.area CODE
                           00027B   818 Fmain$__str_7$0_0$0 == .
                                    819 	.area CONST
      0080D1                        820 ___str_7:
      0080D1 62 65 74 61            821 	.ascii "beta"
      0080D5 00                     822 	.db 0x00
                                    823 	.area CODE
                                    824 	.area INITIALIZER
                           000000   825 Fmain$__xinit_js$0_0$0 == .
      008178                        826 __xinit__js:
      008178 00 00 00 FA            827 	.byte #0x00, #0x00, #0x00, #0xfa	; 250
                           000004   828 Fmain$__xinit_kroky$0_0$0 == .
      00817C                        829 __xinit__kroky:
      00817C 00 00 00 00            830 	.byte #0x00, #0x00, #0x00, #0x00	; 0
                                    831 	.area CABS (ABS)
                                    832 
                                    833 	.area .debug_line (NOLOAD)
      000295 00 00 02 1B            834 	.dw	0,Ldebug_line_end-Ldebug_line_start
      000299                        835 Ldebug_line_start:
      000299 00 02                  836 	.dw	2
      00029B 00 00 00 6F            837 	.dw	0,Ldebug_line_stmt-6-Ldebug_line_start
      00029F 01                     838 	.db	1
      0002A0 01                     839 	.db	1
      0002A1 FB                     840 	.db	-5
      0002A2 0F                     841 	.db	15
      0002A3 0A                     842 	.db	10
      0002A4 00                     843 	.db	0
      0002A5 01                     844 	.db	1
      0002A6 01                     845 	.db	1
      0002A7 01                     846 	.db	1
      0002A8 01                     847 	.db	1
      0002A9 00                     848 	.db	0
      0002AA 00                     849 	.db	0
      0002AB 00                     850 	.db	0
      0002AC 01                     851 	.db	1
      0002AD 43 3A 5C 50 72 6F 67   852 	.ascii "C:\Program Files\SDCC\bin\..\include\stm8"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
             5C 73 74 6D 38
      0002D5 00                     853 	.db	0
      0002D6 43 3A 5C 50 72 6F 67   854 	.ascii "C:\Program Files\SDCC\bin\..\include"
             72 61 6D 20 46 69 6C
             65 73 5C 53 44 43 43
             08 69 6E 5C 2E 2E 5C
             69 6E 63 6C 75 64 65
      0002F9 00                     855 	.db	0
      0002FA 00                     856 	.db	0
      0002FB 61 70 70 2F 73 72 63   857 	.ascii "app/src/main.c"
             2F 6D 61 69 6E 2E 63
      000309 00                     858 	.db	0
      00030A 00                     859 	.uleb128	0
      00030B 00                     860 	.uleb128	0
      00030C 00                     861 	.uleb128	0
      00030D 00                     862 	.db	0
      00030E                        863 Ldebug_line_stmt:
      00030E 00                     864 	.db	0
      00030F 05                     865 	.uleb128	5
      000310 02                     866 	.db	2
      000311 00 00 82 5F            867 	.dw	0,(Smain$prepocet$0)
      000315 03                     868 	.db	3
      000316 0D                     869 	.sleb128	13
      000317 01                     870 	.db	1
      000318 09                     871 	.db	9
      000319 00 00                  872 	.dw	Smain$prepocet$2-Smain$prepocet$0
      00031B 03                     873 	.db	3
      00031C 02                     874 	.sleb128	2
      00031D 01                     875 	.db	1
      00031E 09                     876 	.db	9
      00031F 00 17                  877 	.dw	Smain$prepocet$9-Smain$prepocet$2
      000321 03                     878 	.db	3
      000322 01                     879 	.sleb128	1
      000323 01                     880 	.db	1
      000324 09                     881 	.db	9
      000325 00 09                  882 	.dw	Smain$prepocet$10-Smain$prepocet$9
      000327 03                     883 	.db	3
      000328 01                     884 	.sleb128	1
      000329 01                     885 	.db	1
      00032A 09                     886 	.db	9
      00032B 00 13                  887 	.dw	Smain$prepocet$15-Smain$prepocet$10
      00032D 03                     888 	.db	3
      00032E 01                     889 	.sleb128	1
      00032F 01                     890 	.db	1
      000330 09                     891 	.db	9
      000331 00 0C                  892 	.dw	Smain$prepocet$20-Smain$prepocet$15
      000333 03                     893 	.db	3
      000334 01                     894 	.sleb128	1
      000335 01                     895 	.db	1
      000336 09                     896 	.db	9
      000337 00 0C                  897 	.dw	Smain$prepocet$25-Smain$prepocet$20
      000339 03                     898 	.db	3
      00033A 01                     899 	.sleb128	1
      00033B 01                     900 	.db	1
      00033C 09                     901 	.db	9
      00033D 00 0B                  902 	.dw	Smain$prepocet$30-Smain$prepocet$25
      00033F 03                     903 	.db	3
      000340 01                     904 	.sleb128	1
      000341 01                     905 	.db	1
      000342 09                     906 	.db	9
      000343 00 01                  907 	.dw	1+Smain$prepocet$31-Smain$prepocet$30
      000345 00                     908 	.db	0
      000346 01                     909 	.uleb128	1
      000347 01                     910 	.db	1
      000348 00                     911 	.db	0
      000349 05                     912 	.uleb128	5
      00034A 02                     913 	.db	2
      00034B 00 00 82 B6            914 	.dw	0,(Smain$main$33)
      00034F 03                     915 	.db	3
      000350 22                     916 	.sleb128	34
      000351 01                     917 	.db	1
      000352 09                     918 	.db	9
      000353 00 01                  919 	.dw	Smain$main$36-Smain$main$33
      000355 03                     920 	.db	3
      000356 03                     921 	.sleb128	3
      000357 01                     922 	.db	1
      000358 09                     923 	.db	9
      000359 00 06                  924 	.dw	Smain$main$39-Smain$main$36
      00035B 03                     925 	.db	3
      00035C 02                     926 	.sleb128	2
      00035D 01                     927 	.db	1
      00035E 09                     928 	.db	9
      00035F 00 0D                  929 	.dw	Smain$main$45-Smain$main$39
      000361 03                     930 	.db	3
      000362 01                     931 	.sleb128	1
      000363 01                     932 	.db	1
      000364 09                     933 	.db	9
      000365 00 0D                  934 	.dw	Smain$main$51-Smain$main$45
      000367 03                     935 	.db	3
      000368 01                     936 	.sleb128	1
      000369 01                     937 	.db	1
      00036A 09                     938 	.db	9
      00036B 00 0D                  939 	.dw	Smain$main$57-Smain$main$51
      00036D 03                     940 	.db	3
      00036E 01                     941 	.sleb128	1
      00036F 01                     942 	.db	1
      000370 09                     943 	.db	9
      000371 00 0D                  944 	.dw	Smain$main$63-Smain$main$57
      000373 03                     945 	.db	3
      000374 02                     946 	.sleb128	2
      000375 01                     947 	.db	1
      000376 09                     948 	.db	9
      000377 00 0D                  949 	.dw	Smain$main$69-Smain$main$63
      000379 03                     950 	.db	3
      00037A 01                     951 	.sleb128	1
      00037B 01                     952 	.db	1
      00037C 09                     953 	.db	9
      00037D 00 0D                  954 	.dw	Smain$main$75-Smain$main$69
      00037F 03                     955 	.db	3
      000380 01                     956 	.sleb128	1
      000381 01                     957 	.db	1
      000382 09                     958 	.db	9
      000383 00 0D                  959 	.dw	Smain$main$81-Smain$main$75
      000385 03                     960 	.db	3
      000386 01                     961 	.sleb128	1
      000387 01                     962 	.db	1
      000388 09                     963 	.db	9
      000389 00 0D                  964 	.dw	Smain$main$87-Smain$main$81
      00038B 03                     965 	.db	3
      00038C 02                     966 	.sleb128	2
      00038D 01                     967 	.db	1
      00038E 09                     968 	.db	9
      00038F 00 0D                  969 	.dw	Smain$main$93-Smain$main$87
      000391 03                     970 	.db	3
      000392 01                     971 	.sleb128	1
      000393 01                     972 	.db	1
      000394 09                     973 	.db	9
      000395 00 0B                  974 	.dw	Smain$main$98-Smain$main$93
      000397 03                     975 	.db	3
      000398 02                     976 	.sleb128	2
      000399 01                     977 	.db	1
      00039A 09                     978 	.db	9
      00039B 00 0D                  979 	.dw	Smain$main$104-Smain$main$98
      00039D 03                     980 	.db	3
      00039E 04                     981 	.sleb128	4
      00039F 01                     982 	.db	1
      0003A0 09                     983 	.db	9
      0003A1 00 03                  984 	.dw	Smain$main$105-Smain$main$104
      0003A3 03                     985 	.db	3
      0003A4 01                     986 	.sleb128	1
      0003A5 01                     987 	.db	1
      0003A6 09                     988 	.db	9
      0003A7 00 03                  989 	.dw	Smain$main$106-Smain$main$105
      0003A9 03                     990 	.db	3
      0003AA 01                     991 	.sleb128	1
      0003AB 01                     992 	.db	1
      0003AC 09                     993 	.db	9
      0003AD 00 03                  994 	.dw	Smain$main$108-Smain$main$106
      0003AF 03                     995 	.db	3
      0003B0 03                     996 	.sleb128	3
      0003B1 01                     997 	.db	1
      0003B2 09                     998 	.db	9
      0003B3 00 02                  999 	.dw	Smain$main$110-Smain$main$108
      0003B5 03                    1000 	.db	3
      0003B6 02                    1001 	.sleb128	2
      0003B7 01                    1002 	.db	1
      0003B8 09                    1003 	.db	9
      0003B9 00 08                 1004 	.dw	Smain$main$114-Smain$main$110
      0003BB 03                    1005 	.db	3
      0003BC 01                    1006 	.sleb128	1
      0003BD 01                    1007 	.db	1
      0003BE 09                    1008 	.db	9
      0003BF 00 00                 1009 	.dw	Smain$main$116-Smain$main$114
      0003C1 03                    1010 	.db	3
      0003C2 04                    1011 	.sleb128	4
      0003C3 01                    1012 	.db	1
      0003C4 09                    1013 	.db	9
      0003C5 00 0E                 1014 	.dw	Smain$main$121-Smain$main$116
      0003C7 03                    1015 	.db	3
      0003C8 02                    1016 	.sleb128	2
      0003C9 01                    1017 	.db	1
      0003CA 09                    1018 	.db	9
      0003CB 00 03                 1019 	.dw	Smain$main$122-Smain$main$121
      0003CD 03                    1020 	.db	3
      0003CE 01                    1021 	.sleb128	1
      0003CF 01                    1022 	.db	1
      0003D0 09                    1023 	.db	9
      0003D1 00 07                 1024 	.dw	Smain$main$125-Smain$main$122
      0003D3 03                    1025 	.db	3
      0003D4 02                    1026 	.sleb128	2
      0003D5 01                    1027 	.db	1
      0003D6 09                    1028 	.db	9
      0003D7 00 0A                 1029 	.dw	Smain$main$126-Smain$main$125
      0003D9 03                    1030 	.db	3
      0003DA 01                    1031 	.sleb128	1
      0003DB 01                    1032 	.db	1
      0003DC 09                    1033 	.db	9
      0003DD 00 0B                 1034 	.dw	Smain$main$131-Smain$main$126
      0003DF 03                    1035 	.db	3
      0003E0 01                    1036 	.sleb128	1
      0003E1 01                    1037 	.db	1
      0003E2 09                    1038 	.db	9
      0003E3 00 07                 1039 	.dw	Smain$main$132-Smain$main$131
      0003E5 03                    1040 	.db	3
      0003E6 01                    1041 	.sleb128	1
      0003E7 01                    1042 	.db	1
      0003E8 09                    1043 	.db	9
      0003E9 00 0B                 1044 	.dw	Smain$main$137-Smain$main$132
      0003EB 03                    1045 	.db	3
      0003EC 01                    1046 	.sleb128	1
      0003ED 01                    1047 	.db	1
      0003EE 09                    1048 	.db	9
      0003EF 00 0A                 1049 	.dw	Smain$main$138-Smain$main$137
      0003F1 03                    1050 	.db	3
      0003F2 01                    1051 	.sleb128	1
      0003F3 01                    1052 	.db	1
      0003F4 09                    1053 	.db	9
      0003F5 00 0B                 1054 	.dw	Smain$main$143-Smain$main$138
      0003F7 03                    1055 	.db	3
      0003F8 01                    1056 	.sleb128	1
      0003F9 01                    1057 	.db	1
      0003FA 09                    1058 	.db	9
      0003FB 00 07                 1059 	.dw	Smain$main$144-Smain$main$143
      0003FD 03                    1060 	.db	3
      0003FE 01                    1061 	.sleb128	1
      0003FF 01                    1062 	.db	1
      000400 09                    1063 	.db	9
      000401 00 0B                 1064 	.dw	Smain$main$149-Smain$main$144
      000403 03                    1065 	.db	3
      000404 02                    1066 	.sleb128	2
      000405 01                    1067 	.db	1
      000406 09                    1068 	.db	9
      000407 00 04                 1069 	.dw	Smain$main$152-Smain$main$149
      000409 03                    1070 	.db	3
      00040A 02                    1071 	.sleb128	2
      00040B 01                    1072 	.db	1
      00040C 09                    1073 	.db	9
      00040D 00 15                 1074 	.dw	Smain$main$153-Smain$main$152
      00040F 03                    1075 	.db	3
      000410 01                    1076 	.sleb128	1
      000411 01                    1077 	.db	1
      000412 09                    1078 	.db	9
      000413 00 0B                 1079 	.dw	Smain$main$158-Smain$main$153
      000415 03                    1080 	.db	3
      000416 02                    1081 	.sleb128	2
      000417 01                    1082 	.db	1
      000418 09                    1083 	.db	9
      000419 00 04                 1084 	.dw	Smain$main$161-Smain$main$158
      00041B 03                    1085 	.db	3
      00041C 02                    1086 	.sleb128	2
      00041D 01                    1087 	.db	1
      00041E 09                    1088 	.db	9
      00041F 00 15                 1089 	.dw	Smain$main$162-Smain$main$161
      000421 03                    1090 	.db	3
      000422 01                    1091 	.sleb128	1
      000423 01                    1092 	.db	1
      000424 09                    1093 	.db	9
      000425 00 0B                 1094 	.dw	Smain$main$167-Smain$main$162
      000427 03                    1095 	.db	3
      000428 02                    1096 	.sleb128	2
      000429 01                    1097 	.db	1
      00042A 09                    1098 	.db	9
      00042B 00 04                 1099 	.dw	Smain$main$170-Smain$main$167
      00042D 03                    1100 	.db	3
      00042E 02                    1101 	.sleb128	2
      00042F 01                    1102 	.db	1
      000430 09                    1103 	.db	9
      000431 00 15                 1104 	.dw	Smain$main$171-Smain$main$170
      000433 03                    1105 	.db	3
      000434 01                    1106 	.sleb128	1
      000435 01                    1107 	.db	1
      000436 09                    1108 	.db	9
      000437 00 0A                 1109 	.dw	Smain$main$176-Smain$main$171
      000439 03                    1110 	.db	3
      00043A 02                    1111 	.sleb128	2
      00043B 01                    1112 	.db	1
      00043C 09                    1113 	.db	9
      00043D 00 04                 1114 	.dw	Smain$main$179-Smain$main$176
      00043F 03                    1115 	.db	3
      000440 02                    1116 	.sleb128	2
      000441 01                    1117 	.db	1
      000442 09                    1118 	.db	9
      000443 00 15                 1119 	.dw	Smain$main$180-Smain$main$179
      000445 03                    1120 	.db	3
      000446 01                    1121 	.sleb128	1
      000447 01                    1122 	.db	1
      000448 09                    1123 	.db	9
      000449 00 0A                 1124 	.dw	Smain$main$186-Smain$main$180
      00044B 03                    1125 	.db	3
      00044C 04                    1126 	.sleb128	4
      00044D 01                    1127 	.db	1
      00044E 09                    1128 	.db	9
      00044F 00 08                 1129 	.dw	Smain$main$191-Smain$main$186
      000451 03                    1130 	.db	3
      000452 03                    1131 	.sleb128	3
      000453 01                    1132 	.db	1
      000454 09                    1133 	.db	9
      000455 00 0D                 1134 	.dw	Smain$main$196-Smain$main$191
      000457 03                    1135 	.db	3
      000458 01                    1136 	.sleb128	1
      000459 01                    1137 	.db	1
      00045A 09                    1138 	.db	9
      00045B 00 06                 1139 	.dw	Smain$main$198-Smain$main$196
      00045D 03                    1140 	.db	3
      00045E 02                    1141 	.sleb128	2
      00045F 01                    1142 	.db	1
      000460 09                    1143 	.db	9
      000461 00 05                 1144 	.dw	Smain$main$201-Smain$main$198
      000463 03                    1145 	.db	3
      000464 03                    1146 	.sleb128	3
      000465 01                    1147 	.db	1
      000466 09                    1148 	.db	9
      000467 00 02                 1149 	.dw	Smain$main$202-Smain$main$201
      000469 03                    1150 	.db	3
      00046A 01                    1151 	.sleb128	1
      00046B 01                    1152 	.db	1
      00046C 09                    1153 	.db	9
      00046D 00 0B                 1154 	.dw	Smain$main$207-Smain$main$202
      00046F 03                    1155 	.db	3
      000470 01                    1156 	.sleb128	1
      000471 01                    1157 	.db	1
      000472 09                    1158 	.db	9
      000473 00 08                 1159 	.dw	Smain$main$211-Smain$main$207
      000475 03                    1160 	.db	3
      000476 01                    1161 	.sleb128	1
      000477 01                    1162 	.db	1
      000478 09                    1163 	.db	9
      000479 00 0B                 1164 	.dw	Smain$main$217-Smain$main$211
      00047B 03                    1165 	.db	3
      00047C 04                    1166 	.sleb128	4
      00047D 01                    1167 	.db	1
      00047E 09                    1168 	.db	9
      00047F 00 09                 1169 	.dw	Smain$main$219-Smain$main$217
      000481 03                    1170 	.db	3
      000482 02                    1171 	.sleb128	2
      000483 01                    1172 	.db	1
      000484 09                    1173 	.db	9
      000485 00 04                 1174 	.dw	Smain$main$221-Smain$main$219
      000487 03                    1175 	.db	3
      000488 02                    1176 	.sleb128	2
      000489 01                    1177 	.db	1
      00048A 09                    1178 	.db	9
      00048B 00 04                 1179 	.dw	Smain$main$222-Smain$main$221
      00048D 03                    1180 	.db	3
      00048E 01                    1181 	.sleb128	1
      00048F 01                    1182 	.db	1
      000490 09                    1183 	.db	9
      000491 00 15                 1184 	.dw	Smain$main$223-Smain$main$222
      000493 03                    1185 	.db	3
      000494 01                    1186 	.sleb128	1
      000495 01                    1187 	.db	1
      000496 09                    1188 	.db	9
      000497 00 08                 1189 	.dw	Smain$main$227-Smain$main$223
      000499 03                    1190 	.db	3
      00049A 01                    1191 	.sleb128	1
      00049B 01                    1192 	.db	1
      00049C 09                    1193 	.db	9
      00049D 00 0B                 1194 	.dw	Smain$main$233-Smain$main$227
      00049F 03                    1195 	.db	3
      0004A0 05                    1196 	.sleb128	5
      0004A1 01                    1197 	.db	1
      0004A2 09                    1198 	.db	9
      0004A3 00 08                 1199 	.dw	Smain$main$236-Smain$main$233
      0004A5 03                    1200 	.db	3
      0004A6 01                    1201 	.sleb128	1
      0004A7 01                    1202 	.db	1
      0004A8 09                    1203 	.db	9
      0004A9 00 06                 1204 	.dw	Smain$main$239-Smain$main$236
      0004AB 03                    1205 	.db	3
      0004AC 02                    1206 	.sleb128	2
      0004AD 01                    1207 	.db	1
      0004AE 09                    1208 	.db	9
      0004AF 00 02                 1209 	.dw	1+Smain$main$241-Smain$main$239
      0004B1 00                    1210 	.db	0
      0004B2 01                    1211 	.uleb128	1
      0004B3 01                    1212 	.db	1
      0004B4                       1213 Ldebug_line_end:
                                   1214 
                                   1215 	.area .debug_loc (NOLOAD)
      000340                       1216 Ldebug_loc_start:
      000340 00 00 84 D9           1217 	.dw	0,(Smain$main$240)
      000344 00 00 84 DA           1218 	.dw	0,(Smain$main$242)
      000348 00 02                 1219 	.dw	2
      00034A 78                    1220 	.db	120
      00034B 01                    1221 	.sleb128	1
      00034C 00 00 84 D2           1222 	.dw	0,(Smain$main$235)
      000350 00 00 84 D9           1223 	.dw	0,(Smain$main$240)
      000354 00 02                 1224 	.dw	2
      000356 78                    1225 	.db	120
      000357 03                    1226 	.sleb128	3
      000358 00 00 84 CE           1227 	.dw	0,(Smain$main$234)
      00035C 00 00 84 D2           1228 	.dw	0,(Smain$main$235)
      000360 00 02                 1229 	.dw	2
      000362 78                    1230 	.db	120
      000363 04                    1231 	.sleb128	4
      000364 00 00 84 CA           1232 	.dw	0,(Smain$main$231)
      000368 00 00 84 CE           1233 	.dw	0,(Smain$main$234)
      00036C 00 02                 1234 	.dw	2
      00036E 78                    1235 	.db	120
      00036F 03                    1236 	.sleb128	3
      000370 00 00 84 C5           1237 	.dw	0,(Smain$main$230)
      000374 00 00 84 CA           1238 	.dw	0,(Smain$main$231)
      000378 00 02                 1239 	.dw	2
      00037A 78                    1240 	.db	120
      00037B 07                    1241 	.sleb128	7
      00037C 00 00 84 C3           1242 	.dw	0,(Smain$main$229)
      000380 00 00 84 C5           1243 	.dw	0,(Smain$main$230)
      000384 00 02                 1244 	.dw	2
      000386 78                    1245 	.db	120
      000387 06                    1246 	.sleb128	6
      000388 00 00 84 C1           1247 	.dw	0,(Smain$main$228)
      00038C 00 00 84 C3           1248 	.dw	0,(Smain$main$229)
      000390 00 02                 1249 	.dw	2
      000392 78                    1250 	.db	120
      000393 04                    1251 	.sleb128	4
      000394 00 00 84 BF           1252 	.dw	0,(Smain$main$226)
      000398 00 00 84 C1           1253 	.dw	0,(Smain$main$228)
      00039C 00 02                 1254 	.dw	2
      00039E 78                    1255 	.db	120
      00039F 03                    1256 	.sleb128	3
      0003A0 00 00 84 BB           1257 	.dw	0,(Smain$main$225)
      0003A4 00 00 84 BF           1258 	.dw	0,(Smain$main$226)
      0003A8 00 02                 1259 	.dw	2
      0003AA 78                    1260 	.db	120
      0003AB 05                    1261 	.sleb128	5
      0003AC 00 00 84 B9           1262 	.dw	0,(Smain$main$224)
      0003B0 00 00 84 BB           1263 	.dw	0,(Smain$main$225)
      0003B4 00 02                 1264 	.dw	2
      0003B6 78                    1265 	.db	120
      0003B7 04                    1266 	.sleb128	4
      0003B8 00 00 84 91           1267 	.dw	0,(Smain$main$215)
      0003BC 00 00 84 B9           1268 	.dw	0,(Smain$main$224)
      0003C0 00 02                 1269 	.dw	2
      0003C2 78                    1270 	.db	120
      0003C3 03                    1271 	.sleb128	3
      0003C4 00 00 84 8C           1272 	.dw	0,(Smain$main$214)
      0003C8 00 00 84 91           1273 	.dw	0,(Smain$main$215)
      0003CC 00 02                 1274 	.dw	2
      0003CE 78                    1275 	.db	120
      0003CF 07                    1276 	.sleb128	7
      0003D0 00 00 84 8A           1277 	.dw	0,(Smain$main$213)
      0003D4 00 00 84 8C           1278 	.dw	0,(Smain$main$214)
      0003D8 00 02                 1279 	.dw	2
      0003DA 78                    1280 	.db	120
      0003DB 06                    1281 	.sleb128	6
      0003DC 00 00 84 88           1282 	.dw	0,(Smain$main$212)
      0003E0 00 00 84 8A           1283 	.dw	0,(Smain$main$213)
      0003E4 00 02                 1284 	.dw	2
      0003E6 78                    1285 	.db	120
      0003E7 04                    1286 	.sleb128	4
      0003E8 00 00 84 86           1287 	.dw	0,(Smain$main$210)
      0003EC 00 00 84 88           1288 	.dw	0,(Smain$main$212)
      0003F0 00 02                 1289 	.dw	2
      0003F2 78                    1290 	.db	120
      0003F3 03                    1291 	.sleb128	3
      0003F4 00 00 84 82           1292 	.dw	0,(Smain$main$209)
      0003F8 00 00 84 86           1293 	.dw	0,(Smain$main$210)
      0003FC 00 02                 1294 	.dw	2
      0003FE 78                    1295 	.db	120
      0003FF 05                    1296 	.sleb128	5
      000400 00 00 84 80           1297 	.dw	0,(Smain$main$208)
      000404 00 00 84 82           1298 	.dw	0,(Smain$main$209)
      000408 00 02                 1299 	.dw	2
      00040A 78                    1300 	.db	120
      00040B 04                    1301 	.sleb128	4
      00040C 00 00 84 7E           1302 	.dw	0,(Smain$main$206)
      000410 00 00 84 80           1303 	.dw	0,(Smain$main$208)
      000414 00 02                 1304 	.dw	2
      000416 78                    1305 	.db	120
      000417 03                    1306 	.sleb128	3
      000418 00 00 84 79           1307 	.dw	0,(Smain$main$205)
      00041C 00 00 84 7E           1308 	.dw	0,(Smain$main$206)
      000420 00 02                 1309 	.dw	2
      000422 78                    1310 	.db	120
      000423 06                    1311 	.sleb128	6
      000424 00 00 84 77           1312 	.dw	0,(Smain$main$204)
      000428 00 00 84 79           1313 	.dw	0,(Smain$main$205)
      00042C 00 02                 1314 	.dw	2
      00042E 78                    1315 	.db	120
      00042F 05                    1316 	.sleb128	5
      000430 00 00 84 75           1317 	.dw	0,(Smain$main$203)
      000434 00 00 84 77           1318 	.dw	0,(Smain$main$204)
      000438 00 02                 1319 	.dw	2
      00043A 78                    1320 	.db	120
      00043B 04                    1321 	.sleb128	4
      00043C 00 00 84 71           1322 	.dw	0,(Smain$main$199)
      000440 00 00 84 75           1323 	.dw	0,(Smain$main$203)
      000444 00 02                 1324 	.dw	2
      000446 78                    1325 	.db	120
      000447 03                    1326 	.sleb128	3
      000448 00 00 84 64           1327 	.dw	0,(Smain$main$195)
      00044C 00 00 84 71           1328 	.dw	0,(Smain$main$199)
      000450 00 02                 1329 	.dw	2
      000452 78                    1330 	.db	120
      000453 03                    1331 	.sleb128	3
      000454 00 00 84 5F           1332 	.dw	0,(Smain$main$194)
      000458 00 00 84 64           1333 	.dw	0,(Smain$main$195)
      00045C 00 02                 1334 	.dw	2
      00045E 78                    1335 	.db	120
      00045F 06                    1336 	.sleb128	6
      000460 00 00 84 5D           1337 	.dw	0,(Smain$main$193)
      000464 00 00 84 5F           1338 	.dw	0,(Smain$main$194)
      000468 00 02                 1339 	.dw	2
      00046A 78                    1340 	.db	120
      00046B 05                    1341 	.sleb128	5
      00046C 00 00 84 5B           1342 	.dw	0,(Smain$main$192)
      000470 00 00 84 5D           1343 	.dw	0,(Smain$main$193)
      000474 00 02                 1344 	.dw	2
      000476 78                    1345 	.db	120
      000477 04                    1346 	.sleb128	4
      000478 00 00 84 59           1347 	.dw	0,(Smain$main$189)
      00047C 00 00 84 5B           1348 	.dw	0,(Smain$main$192)
      000480 00 02                 1349 	.dw	2
      000482 78                    1350 	.db	120
      000483 03                    1351 	.sleb128	3
      000484 00 00 84 55           1352 	.dw	0,(Smain$main$188)
      000488 00 00 84 59           1353 	.dw	0,(Smain$main$189)
      00048C 00 02                 1354 	.dw	2
      00048E 78                    1355 	.db	120
      00048F 05                    1356 	.sleb128	5
      000490 00 00 84 53           1357 	.dw	0,(Smain$main$187)
      000494 00 00 84 55           1358 	.dw	0,(Smain$main$188)
      000498 00 02                 1359 	.dw	2
      00049A 78                    1360 	.db	120
      00049B 04                    1361 	.sleb128	4
      00049C 00 00 84 4F           1362 	.dw	0,(Smain$main$183)
      0004A0 00 00 84 53           1363 	.dw	0,(Smain$main$187)
      0004A4 00 02                 1364 	.dw	2
      0004A6 78                    1365 	.db	120
      0004A7 03                    1366 	.sleb128	3
      0004A8 00 00 84 4B           1367 	.dw	0,(Smain$main$182)
      0004AC 00 00 84 4F           1368 	.dw	0,(Smain$main$183)
      0004B0 00 02                 1369 	.dw	2
      0004B2 78                    1370 	.db	120
      0004B3 05                    1371 	.sleb128	5
      0004B4 00 00 84 49           1372 	.dw	0,(Smain$main$181)
      0004B8 00 00 84 4B           1373 	.dw	0,(Smain$main$182)
      0004BC 00 02                 1374 	.dw	2
      0004BE 78                    1375 	.db	120
      0004BF 04                    1376 	.sleb128	4
      0004C0 00 00 84 32           1377 	.dw	0,(Smain$main$177)
      0004C4 00 00 84 49           1378 	.dw	0,(Smain$main$181)
      0004C8 00 02                 1379 	.dw	2
      0004CA 78                    1380 	.db	120
      0004CB 03                    1381 	.sleb128	3
      0004CC 00 00 84 2C           1382 	.dw	0,(Smain$main$174)
      0004D0 00 00 84 32           1383 	.dw	0,(Smain$main$177)
      0004D4 00 02                 1384 	.dw	2
      0004D6 78                    1385 	.db	120
      0004D7 03                    1386 	.sleb128	3
      0004D8 00 00 84 28           1387 	.dw	0,(Smain$main$173)
      0004DC 00 00 84 2C           1388 	.dw	0,(Smain$main$174)
      0004E0 00 02                 1389 	.dw	2
      0004E2 78                    1390 	.db	120
      0004E3 05                    1391 	.sleb128	5
      0004E4 00 00 84 26           1392 	.dw	0,(Smain$main$172)
      0004E8 00 00 84 28           1393 	.dw	0,(Smain$main$173)
      0004EC 00 02                 1394 	.dw	2
      0004EE 78                    1395 	.db	120
      0004EF 04                    1396 	.sleb128	4
      0004F0 00 00 84 0F           1397 	.dw	0,(Smain$main$168)
      0004F4 00 00 84 26           1398 	.dw	0,(Smain$main$172)
      0004F8 00 02                 1399 	.dw	2
      0004FA 78                    1400 	.db	120
      0004FB 03                    1401 	.sleb128	3
      0004FC 00 00 84 08           1402 	.dw	0,(Smain$main$165)
      000500 00 00 84 0F           1403 	.dw	0,(Smain$main$168)
      000504 00 02                 1404 	.dw	2
      000506 78                    1405 	.db	120
      000507 03                    1406 	.sleb128	3
      000508 00 00 84 04           1407 	.dw	0,(Smain$main$164)
      00050C 00 00 84 08           1408 	.dw	0,(Smain$main$165)
      000510 00 02                 1409 	.dw	2
      000512 78                    1410 	.db	120
      000513 05                    1411 	.sleb128	5
      000514 00 00 84 02           1412 	.dw	0,(Smain$main$163)
      000518 00 00 84 04           1413 	.dw	0,(Smain$main$164)
      00051C 00 02                 1414 	.dw	2
      00051E 78                    1415 	.db	120
      00051F 04                    1416 	.sleb128	4
      000520 00 00 83 EB           1417 	.dw	0,(Smain$main$159)
      000524 00 00 84 02           1418 	.dw	0,(Smain$main$163)
      000528 00 02                 1419 	.dw	2
      00052A 78                    1420 	.db	120
      00052B 03                    1421 	.sleb128	3
      00052C 00 00 83 E4           1422 	.dw	0,(Smain$main$156)
      000530 00 00 83 EB           1423 	.dw	0,(Smain$main$159)
      000534 00 02                 1424 	.dw	2
      000536 78                    1425 	.db	120
      000537 03                    1426 	.sleb128	3
      000538 00 00 83 E0           1427 	.dw	0,(Smain$main$155)
      00053C 00 00 83 E4           1428 	.dw	0,(Smain$main$156)
      000540 00 02                 1429 	.dw	2
      000542 78                    1430 	.db	120
      000543 05                    1431 	.sleb128	5
      000544 00 00 83 DE           1432 	.dw	0,(Smain$main$154)
      000548 00 00 83 E0           1433 	.dw	0,(Smain$main$155)
      00054C 00 02                 1434 	.dw	2
      00054E 78                    1435 	.db	120
      00054F 04                    1436 	.sleb128	4
      000550 00 00 83 C7           1437 	.dw	0,(Smain$main$150)
      000554 00 00 83 DE           1438 	.dw	0,(Smain$main$154)
      000558 00 02                 1439 	.dw	2
      00055A 78                    1440 	.db	120
      00055B 03                    1441 	.sleb128	3
      00055C 00 00 83 C0           1442 	.dw	0,(Smain$main$147)
      000560 00 00 83 C7           1443 	.dw	0,(Smain$main$150)
      000564 00 02                 1444 	.dw	2
      000566 78                    1445 	.db	120
      000567 03                    1446 	.sleb128	3
      000568 00 00 83 BC           1447 	.dw	0,(Smain$main$146)
      00056C 00 00 83 C0           1448 	.dw	0,(Smain$main$147)
      000570 00 02                 1449 	.dw	2
      000572 78                    1450 	.db	120
      000573 05                    1451 	.sleb128	5
      000574 00 00 83 BA           1452 	.dw	0,(Smain$main$145)
      000578 00 00 83 BC           1453 	.dw	0,(Smain$main$146)
      00057C 00 02                 1454 	.dw	2
      00057E 78                    1455 	.db	120
      00057F 04                    1456 	.sleb128	4
      000580 00 00 83 B1           1457 	.dw	0,(Smain$main$142)
      000584 00 00 83 BA           1458 	.dw	0,(Smain$main$145)
      000588 00 02                 1459 	.dw	2
      00058A 78                    1460 	.db	120
      00058B 03                    1461 	.sleb128	3
      00058C 00 00 83 AC           1462 	.dw	0,(Smain$main$141)
      000590 00 00 83 B1           1463 	.dw	0,(Smain$main$142)
      000594 00 02                 1464 	.dw	2
      000596 78                    1465 	.db	120
      000597 07                    1466 	.sleb128	7
      000598 00 00 83 AA           1467 	.dw	0,(Smain$main$140)
      00059C 00 00 83 AC           1468 	.dw	0,(Smain$main$141)
      0005A0 00 02                 1469 	.dw	2
      0005A2 78                    1470 	.db	120
      0005A3 06                    1471 	.sleb128	6
      0005A4 00 00 83 A8           1472 	.dw	0,(Smain$main$139)
      0005A8 00 00 83 AA           1473 	.dw	0,(Smain$main$140)
      0005AC 00 02                 1474 	.dw	2
      0005AE 78                    1475 	.db	120
      0005AF 04                    1476 	.sleb128	4
      0005B0 00 00 83 9C           1477 	.dw	0,(Smain$main$136)
      0005B4 00 00 83 A8           1478 	.dw	0,(Smain$main$139)
      0005B8 00 02                 1479 	.dw	2
      0005BA 78                    1480 	.db	120
      0005BB 03                    1481 	.sleb128	3
      0005BC 00 00 83 97           1482 	.dw	0,(Smain$main$135)
      0005C0 00 00 83 9C           1483 	.dw	0,(Smain$main$136)
      0005C4 00 02                 1484 	.dw	2
      0005C6 78                    1485 	.db	120
      0005C7 07                    1486 	.sleb128	7
      0005C8 00 00 83 95           1487 	.dw	0,(Smain$main$134)
      0005CC 00 00 83 97           1488 	.dw	0,(Smain$main$135)
      0005D0 00 02                 1489 	.dw	2
      0005D2 78                    1490 	.db	120
      0005D3 06                    1491 	.sleb128	6
      0005D4 00 00 83 93           1492 	.dw	0,(Smain$main$133)
      0005D8 00 00 83 95           1493 	.dw	0,(Smain$main$134)
      0005DC 00 02                 1494 	.dw	2
      0005DE 78                    1495 	.db	120
      0005DF 04                    1496 	.sleb128	4
      0005E0 00 00 83 8A           1497 	.dw	0,(Smain$main$130)
      0005E4 00 00 83 93           1498 	.dw	0,(Smain$main$133)
      0005E8 00 02                 1499 	.dw	2
      0005EA 78                    1500 	.db	120
      0005EB 03                    1501 	.sleb128	3
      0005EC 00 00 83 85           1502 	.dw	0,(Smain$main$129)
      0005F0 00 00 83 8A           1503 	.dw	0,(Smain$main$130)
      0005F4 00 02                 1504 	.dw	2
      0005F6 78                    1505 	.db	120
      0005F7 07                    1506 	.sleb128	7
      0005F8 00 00 83 83           1507 	.dw	0,(Smain$main$128)
      0005FC 00 00 83 85           1508 	.dw	0,(Smain$main$129)
      000600 00 02                 1509 	.dw	2
      000602 78                    1510 	.db	120
      000603 06                    1511 	.sleb128	6
      000604 00 00 83 81           1512 	.dw	0,(Smain$main$127)
      000608 00 00 83 83           1513 	.dw	0,(Smain$main$128)
      00060C 00 02                 1514 	.dw	2
      00060E 78                    1515 	.db	120
      00060F 04                    1516 	.sleb128	4
      000610 00 00 83 75           1517 	.dw	0,(Smain$main$123)
      000614 00 00 83 81           1518 	.dw	0,(Smain$main$127)
      000618 00 02                 1519 	.dw	2
      00061A 78                    1520 	.db	120
      00061B 03                    1521 	.sleb128	3
      00061C 00 00 83 65           1522 	.dw	0,(Smain$main$119)
      000620 00 00 83 75           1523 	.dw	0,(Smain$main$123)
      000624 00 02                 1524 	.dw	2
      000626 78                    1525 	.db	120
      000627 03                    1526 	.sleb128	3
      000628 00 00 83 61           1527 	.dw	0,(Smain$main$118)
      00062C 00 00 83 65           1528 	.dw	0,(Smain$main$119)
      000630 00 02                 1529 	.dw	2
      000632 78                    1530 	.db	120
      000633 05                    1531 	.sleb128	5
      000634 00 00 83 5F           1532 	.dw	0,(Smain$main$117)
      000638 00 00 83 61           1533 	.dw	0,(Smain$main$118)
      00063C 00 02                 1534 	.dw	2
      00063E 78                    1535 	.db	120
      00063F 04                    1536 	.sleb128	4
      000640 00 00 83 5D           1537 	.dw	0,(Smain$main$113)
      000644 00 00 83 5F           1538 	.dw	0,(Smain$main$117)
      000648 00 02                 1539 	.dw	2
      00064A 78                    1540 	.db	120
      00064B 03                    1541 	.sleb128	3
      00064C 00 00 83 59           1542 	.dw	0,(Smain$main$112)
      000650 00 00 83 5D           1543 	.dw	0,(Smain$main$113)
      000654 00 02                 1544 	.dw	2
      000656 78                    1545 	.db	120
      000657 05                    1546 	.sleb128	5
      000658 00 00 83 57           1547 	.dw	0,(Smain$main$111)
      00065C 00 00 83 59           1548 	.dw	0,(Smain$main$112)
      000660 00 02                 1549 	.dw	2
      000662 78                    1550 	.db	120
      000663 04                    1551 	.sleb128	4
      000664 00 00 83 4A           1552 	.dw	0,(Smain$main$103)
      000668 00 00 83 57           1553 	.dw	0,(Smain$main$111)
      00066C 00 02                 1554 	.dw	2
      00066E 78                    1555 	.db	120
      00066F 03                    1556 	.sleb128	3
      000670 00 00 83 45           1557 	.dw	0,(Smain$main$102)
      000674 00 00 83 4A           1558 	.dw	0,(Smain$main$103)
      000678 00 02                 1559 	.dw	2
      00067A 78                    1560 	.db	120
      00067B 07                    1561 	.sleb128	7
      00067C 00 00 83 43           1562 	.dw	0,(Smain$main$101)
      000680 00 00 83 45           1563 	.dw	0,(Smain$main$102)
      000684 00 02                 1564 	.dw	2
      000686 78                    1565 	.db	120
      000687 06                    1566 	.sleb128	6
      000688 00 00 83 41           1567 	.dw	0,(Smain$main$100)
      00068C 00 00 83 43           1568 	.dw	0,(Smain$main$101)
      000690 00 02                 1569 	.dw	2
      000692 78                    1570 	.db	120
      000693 05                    1571 	.sleb128	5
      000694 00 00 83 3F           1572 	.dw	0,(Smain$main$99)
      000698 00 00 83 41           1573 	.dw	0,(Smain$main$100)
      00069C 00 02                 1574 	.dw	2
      00069E 78                    1575 	.db	120
      00069F 04                    1576 	.sleb128	4
      0006A0 00 00 83 3D           1577 	.dw	0,(Smain$main$97)
      0006A4 00 00 83 3F           1578 	.dw	0,(Smain$main$99)
      0006A8 00 02                 1579 	.dw	2
      0006AA 78                    1580 	.db	120
      0006AB 03                    1581 	.sleb128	3
      0006AC 00 00 83 38           1582 	.dw	0,(Smain$main$96)
      0006B0 00 00 83 3D           1583 	.dw	0,(Smain$main$97)
      0006B4 00 02                 1584 	.dw	2
      0006B6 78                    1585 	.db	120
      0006B7 06                    1586 	.sleb128	6
      0006B8 00 00 83 36           1587 	.dw	0,(Smain$main$95)
      0006BC 00 00 83 38           1588 	.dw	0,(Smain$main$96)
      0006C0 00 02                 1589 	.dw	2
      0006C2 78                    1590 	.db	120
      0006C3 05                    1591 	.sleb128	5
      0006C4 00 00 83 34           1592 	.dw	0,(Smain$main$94)
      0006C8 00 00 83 36           1593 	.dw	0,(Smain$main$95)
      0006CC 00 02                 1594 	.dw	2
      0006CE 78                    1595 	.db	120
      0006CF 04                    1596 	.sleb128	4
      0006D0 00 00 83 32           1597 	.dw	0,(Smain$main$92)
      0006D4 00 00 83 34           1598 	.dw	0,(Smain$main$94)
      0006D8 00 02                 1599 	.dw	2
      0006DA 78                    1600 	.db	120
      0006DB 03                    1601 	.sleb128	3
      0006DC 00 00 83 2D           1602 	.dw	0,(Smain$main$91)
      0006E0 00 00 83 32           1603 	.dw	0,(Smain$main$92)
      0006E4 00 02                 1604 	.dw	2
      0006E6 78                    1605 	.db	120
      0006E7 07                    1606 	.sleb128	7
      0006E8 00 00 83 2B           1607 	.dw	0,(Smain$main$90)
      0006EC 00 00 83 2D           1608 	.dw	0,(Smain$main$91)
      0006F0 00 02                 1609 	.dw	2
      0006F2 78                    1610 	.db	120
      0006F3 06                    1611 	.sleb128	6
      0006F4 00 00 83 29           1612 	.dw	0,(Smain$main$89)
      0006F8 00 00 83 2B           1613 	.dw	0,(Smain$main$90)
      0006FC 00 02                 1614 	.dw	2
      0006FE 78                    1615 	.db	120
      0006FF 05                    1616 	.sleb128	5
      000700 00 00 83 27           1617 	.dw	0,(Smain$main$88)
      000704 00 00 83 29           1618 	.dw	0,(Smain$main$89)
      000708 00 02                 1619 	.dw	2
      00070A 78                    1620 	.db	120
      00070B 04                    1621 	.sleb128	4
      00070C 00 00 83 25           1622 	.dw	0,(Smain$main$86)
      000710 00 00 83 27           1623 	.dw	0,(Smain$main$88)
      000714 00 02                 1624 	.dw	2
      000716 78                    1625 	.db	120
      000717 03                    1626 	.sleb128	3
      000718 00 00 83 20           1627 	.dw	0,(Smain$main$85)
      00071C 00 00 83 25           1628 	.dw	0,(Smain$main$86)
      000720 00 02                 1629 	.dw	2
      000722 78                    1630 	.db	120
      000723 07                    1631 	.sleb128	7
      000724 00 00 83 1E           1632 	.dw	0,(Smain$main$84)
      000728 00 00 83 20           1633 	.dw	0,(Smain$main$85)
      00072C 00 02                 1634 	.dw	2
      00072E 78                    1635 	.db	120
      00072F 06                    1636 	.sleb128	6
      000730 00 00 83 1C           1637 	.dw	0,(Smain$main$83)
      000734 00 00 83 1E           1638 	.dw	0,(Smain$main$84)
      000738 00 02                 1639 	.dw	2
      00073A 78                    1640 	.db	120
      00073B 05                    1641 	.sleb128	5
      00073C 00 00 83 1A           1642 	.dw	0,(Smain$main$82)
      000740 00 00 83 1C           1643 	.dw	0,(Smain$main$83)
      000744 00 02                 1644 	.dw	2
      000746 78                    1645 	.db	120
      000747 04                    1646 	.sleb128	4
      000748 00 00 83 18           1647 	.dw	0,(Smain$main$80)
      00074C 00 00 83 1A           1648 	.dw	0,(Smain$main$82)
      000750 00 02                 1649 	.dw	2
      000752 78                    1650 	.db	120
      000753 03                    1651 	.sleb128	3
      000754 00 00 83 13           1652 	.dw	0,(Smain$main$79)
      000758 00 00 83 18           1653 	.dw	0,(Smain$main$80)
      00075C 00 02                 1654 	.dw	2
      00075E 78                    1655 	.db	120
      00075F 07                    1656 	.sleb128	7
      000760 00 00 83 11           1657 	.dw	0,(Smain$main$78)
      000764 00 00 83 13           1658 	.dw	0,(Smain$main$79)
      000768 00 02                 1659 	.dw	2
      00076A 78                    1660 	.db	120
      00076B 06                    1661 	.sleb128	6
      00076C 00 00 83 0F           1662 	.dw	0,(Smain$main$77)
      000770 00 00 83 11           1663 	.dw	0,(Smain$main$78)
      000774 00 02                 1664 	.dw	2
      000776 78                    1665 	.db	120
      000777 05                    1666 	.sleb128	5
      000778 00 00 83 0D           1667 	.dw	0,(Smain$main$76)
      00077C 00 00 83 0F           1668 	.dw	0,(Smain$main$77)
      000780 00 02                 1669 	.dw	2
      000782 78                    1670 	.db	120
      000783 04                    1671 	.sleb128	4
      000784 00 00 83 0B           1672 	.dw	0,(Smain$main$74)
      000788 00 00 83 0D           1673 	.dw	0,(Smain$main$76)
      00078C 00 02                 1674 	.dw	2
      00078E 78                    1675 	.db	120
      00078F 03                    1676 	.sleb128	3
      000790 00 00 83 06           1677 	.dw	0,(Smain$main$73)
      000794 00 00 83 0B           1678 	.dw	0,(Smain$main$74)
      000798 00 02                 1679 	.dw	2
      00079A 78                    1680 	.db	120
      00079B 07                    1681 	.sleb128	7
      00079C 00 00 83 04           1682 	.dw	0,(Smain$main$72)
      0007A0 00 00 83 06           1683 	.dw	0,(Smain$main$73)
      0007A4 00 02                 1684 	.dw	2
      0007A6 78                    1685 	.db	120
      0007A7 06                    1686 	.sleb128	6
      0007A8 00 00 83 02           1687 	.dw	0,(Smain$main$71)
      0007AC 00 00 83 04           1688 	.dw	0,(Smain$main$72)
      0007B0 00 02                 1689 	.dw	2
      0007B2 78                    1690 	.db	120
      0007B3 05                    1691 	.sleb128	5
      0007B4 00 00 83 00           1692 	.dw	0,(Smain$main$70)
      0007B8 00 00 83 02           1693 	.dw	0,(Smain$main$71)
      0007BC 00 02                 1694 	.dw	2
      0007BE 78                    1695 	.db	120
      0007BF 04                    1696 	.sleb128	4
      0007C0 00 00 82 FE           1697 	.dw	0,(Smain$main$68)
      0007C4 00 00 83 00           1698 	.dw	0,(Smain$main$70)
      0007C8 00 02                 1699 	.dw	2
      0007CA 78                    1700 	.db	120
      0007CB 03                    1701 	.sleb128	3
      0007CC 00 00 82 F9           1702 	.dw	0,(Smain$main$67)
      0007D0 00 00 82 FE           1703 	.dw	0,(Smain$main$68)
      0007D4 00 02                 1704 	.dw	2
      0007D6 78                    1705 	.db	120
      0007D7 07                    1706 	.sleb128	7
      0007D8 00 00 82 F7           1707 	.dw	0,(Smain$main$66)
      0007DC 00 00 82 F9           1708 	.dw	0,(Smain$main$67)
      0007E0 00 02                 1709 	.dw	2
      0007E2 78                    1710 	.db	120
      0007E3 06                    1711 	.sleb128	6
      0007E4 00 00 82 F5           1712 	.dw	0,(Smain$main$65)
      0007E8 00 00 82 F7           1713 	.dw	0,(Smain$main$66)
      0007EC 00 02                 1714 	.dw	2
      0007EE 78                    1715 	.db	120
      0007EF 05                    1716 	.sleb128	5
      0007F0 00 00 82 F3           1717 	.dw	0,(Smain$main$64)
      0007F4 00 00 82 F5           1718 	.dw	0,(Smain$main$65)
      0007F8 00 02                 1719 	.dw	2
      0007FA 78                    1720 	.db	120
      0007FB 04                    1721 	.sleb128	4
      0007FC 00 00 82 F1           1722 	.dw	0,(Smain$main$62)
      000800 00 00 82 F3           1723 	.dw	0,(Smain$main$64)
      000804 00 02                 1724 	.dw	2
      000806 78                    1725 	.db	120
      000807 03                    1726 	.sleb128	3
      000808 00 00 82 EC           1727 	.dw	0,(Smain$main$61)
      00080C 00 00 82 F1           1728 	.dw	0,(Smain$main$62)
      000810 00 02                 1729 	.dw	2
      000812 78                    1730 	.db	120
      000813 07                    1731 	.sleb128	7
      000814 00 00 82 EA           1732 	.dw	0,(Smain$main$60)
      000818 00 00 82 EC           1733 	.dw	0,(Smain$main$61)
      00081C 00 02                 1734 	.dw	2
      00081E 78                    1735 	.db	120
      00081F 06                    1736 	.sleb128	6
      000820 00 00 82 E8           1737 	.dw	0,(Smain$main$59)
      000824 00 00 82 EA           1738 	.dw	0,(Smain$main$60)
      000828 00 02                 1739 	.dw	2
      00082A 78                    1740 	.db	120
      00082B 05                    1741 	.sleb128	5
      00082C 00 00 82 E6           1742 	.dw	0,(Smain$main$58)
      000830 00 00 82 E8           1743 	.dw	0,(Smain$main$59)
      000834 00 02                 1744 	.dw	2
      000836 78                    1745 	.db	120
      000837 04                    1746 	.sleb128	4
      000838 00 00 82 E4           1747 	.dw	0,(Smain$main$56)
      00083C 00 00 82 E6           1748 	.dw	0,(Smain$main$58)
      000840 00 02                 1749 	.dw	2
      000842 78                    1750 	.db	120
      000843 03                    1751 	.sleb128	3
      000844 00 00 82 DF           1752 	.dw	0,(Smain$main$55)
      000848 00 00 82 E4           1753 	.dw	0,(Smain$main$56)
      00084C 00 02                 1754 	.dw	2
      00084E 78                    1755 	.db	120
      00084F 07                    1756 	.sleb128	7
      000850 00 00 82 DD           1757 	.dw	0,(Smain$main$54)
      000854 00 00 82 DF           1758 	.dw	0,(Smain$main$55)
      000858 00 02                 1759 	.dw	2
      00085A 78                    1760 	.db	120
      00085B 06                    1761 	.sleb128	6
      00085C 00 00 82 DB           1762 	.dw	0,(Smain$main$53)
      000860 00 00 82 DD           1763 	.dw	0,(Smain$main$54)
      000864 00 02                 1764 	.dw	2
      000866 78                    1765 	.db	120
      000867 05                    1766 	.sleb128	5
      000868 00 00 82 D9           1767 	.dw	0,(Smain$main$52)
      00086C 00 00 82 DB           1768 	.dw	0,(Smain$main$53)
      000870 00 02                 1769 	.dw	2
      000872 78                    1770 	.db	120
      000873 04                    1771 	.sleb128	4
      000874 00 00 82 D7           1772 	.dw	0,(Smain$main$50)
      000878 00 00 82 D9           1773 	.dw	0,(Smain$main$52)
      00087C 00 02                 1774 	.dw	2
      00087E 78                    1775 	.db	120
      00087F 03                    1776 	.sleb128	3
      000880 00 00 82 D2           1777 	.dw	0,(Smain$main$49)
      000884 00 00 82 D7           1778 	.dw	0,(Smain$main$50)
      000888 00 02                 1779 	.dw	2
      00088A 78                    1780 	.db	120
      00088B 07                    1781 	.sleb128	7
      00088C 00 00 82 D0           1782 	.dw	0,(Smain$main$48)
      000890 00 00 82 D2           1783 	.dw	0,(Smain$main$49)
      000894 00 02                 1784 	.dw	2
      000896 78                    1785 	.db	120
      000897 06                    1786 	.sleb128	6
      000898 00 00 82 CE           1787 	.dw	0,(Smain$main$47)
      00089C 00 00 82 D0           1788 	.dw	0,(Smain$main$48)
      0008A0 00 02                 1789 	.dw	2
      0008A2 78                    1790 	.db	120
      0008A3 05                    1791 	.sleb128	5
      0008A4 00 00 82 CC           1792 	.dw	0,(Smain$main$46)
      0008A8 00 00 82 CE           1793 	.dw	0,(Smain$main$47)
      0008AC 00 02                 1794 	.dw	2
      0008AE 78                    1795 	.db	120
      0008AF 04                    1796 	.sleb128	4
      0008B0 00 00 82 CA           1797 	.dw	0,(Smain$main$44)
      0008B4 00 00 82 CC           1798 	.dw	0,(Smain$main$46)
      0008B8 00 02                 1799 	.dw	2
      0008BA 78                    1800 	.db	120
      0008BB 03                    1801 	.sleb128	3
      0008BC 00 00 82 C5           1802 	.dw	0,(Smain$main$43)
      0008C0 00 00 82 CA           1803 	.dw	0,(Smain$main$44)
      0008C4 00 02                 1804 	.dw	2
      0008C6 78                    1805 	.db	120
      0008C7 07                    1806 	.sleb128	7
      0008C8 00 00 82 C3           1807 	.dw	0,(Smain$main$42)
      0008CC 00 00 82 C5           1808 	.dw	0,(Smain$main$43)
      0008D0 00 02                 1809 	.dw	2
      0008D2 78                    1810 	.db	120
      0008D3 06                    1811 	.sleb128	6
      0008D4 00 00 82 C1           1812 	.dw	0,(Smain$main$41)
      0008D8 00 00 82 C3           1813 	.dw	0,(Smain$main$42)
      0008DC 00 02                 1814 	.dw	2
      0008DE 78                    1815 	.db	120
      0008DF 05                    1816 	.sleb128	5
      0008E0 00 00 82 BF           1817 	.dw	0,(Smain$main$40)
      0008E4 00 00 82 C1           1818 	.dw	0,(Smain$main$41)
      0008E8 00 02                 1819 	.dw	2
      0008EA 78                    1820 	.db	120
      0008EB 04                    1821 	.sleb128	4
      0008EC 00 00 82 BD           1822 	.dw	0,(Smain$main$38)
      0008F0 00 00 82 BF           1823 	.dw	0,(Smain$main$40)
      0008F4 00 02                 1824 	.dw	2
      0008F6 78                    1825 	.db	120
      0008F7 03                    1826 	.sleb128	3
      0008F8 00 00 82 B9           1827 	.dw	0,(Smain$main$37)
      0008FC 00 00 82 BD           1828 	.dw	0,(Smain$main$38)
      000900 00 02                 1829 	.dw	2
      000902 78                    1830 	.db	120
      000903 04                    1831 	.sleb128	4
      000904 00 00 82 B7           1832 	.dw	0,(Smain$main$35)
      000908 00 00 82 B9           1833 	.dw	0,(Smain$main$37)
      00090C 00 02                 1834 	.dw	2
      00090E 78                    1835 	.db	120
      00090F 03                    1836 	.sleb128	3
      000910 00 00 82 B6           1837 	.dw	0,(Smain$main$34)
      000914 00 00 82 B7           1838 	.dw	0,(Smain$main$35)
      000918 00 02                 1839 	.dw	2
      00091A 78                    1840 	.db	120
      00091B 01                    1841 	.sleb128	1
      00091C 00 00 00 00           1842 	.dw	0,0
      000920 00 00 00 00           1843 	.dw	0,0
      000924 00 00 82 B5           1844 	.dw	0,(Smain$prepocet$29)
      000928 00 00 82 B6           1845 	.dw	0,(Smain$prepocet$32)
      00092C 00 02                 1846 	.dw	2
      00092E 78                    1847 	.db	120
      00092F 01                    1848 	.sleb128	1
      000930 00 00 82 B0           1849 	.dw	0,(Smain$prepocet$28)
      000934 00 00 82 B5           1850 	.dw	0,(Smain$prepocet$29)
      000938 00 02                 1851 	.dw	2
      00093A 78                    1852 	.db	120
      00093B 04                    1853 	.sleb128	4
      00093C 00 00 82 AE           1854 	.dw	0,(Smain$prepocet$27)
      000940 00 00 82 B0           1855 	.dw	0,(Smain$prepocet$28)
      000944 00 02                 1856 	.dw	2
      000946 78                    1857 	.db	120
      000947 03                    1858 	.sleb128	3
      000948 00 00 82 AC           1859 	.dw	0,(Smain$prepocet$26)
      00094C 00 00 82 AE           1860 	.dw	0,(Smain$prepocet$27)
      000950 00 02                 1861 	.dw	2
      000952 78                    1862 	.db	120
      000953 02                    1863 	.sleb128	2
      000954 00 00 82 AA           1864 	.dw	0,(Smain$prepocet$24)
      000958 00 00 82 AC           1865 	.dw	0,(Smain$prepocet$26)
      00095C 00 02                 1866 	.dw	2
      00095E 78                    1867 	.db	120
      00095F 01                    1868 	.sleb128	1
      000960 00 00 82 A5           1869 	.dw	0,(Smain$prepocet$23)
      000964 00 00 82 AA           1870 	.dw	0,(Smain$prepocet$24)
      000968 00 02                 1871 	.dw	2
      00096A 78                    1872 	.db	120
      00096B 04                    1873 	.sleb128	4
      00096C 00 00 82 A3           1874 	.dw	0,(Smain$prepocet$22)
      000970 00 00 82 A5           1875 	.dw	0,(Smain$prepocet$23)
      000974 00 02                 1876 	.dw	2
      000976 78                    1877 	.db	120
      000977 03                    1878 	.sleb128	3
      000978 00 00 82 A1           1879 	.dw	0,(Smain$prepocet$21)
      00097C 00 00 82 A3           1880 	.dw	0,(Smain$prepocet$22)
      000980 00 02                 1881 	.dw	2
      000982 78                    1882 	.db	120
      000983 02                    1883 	.sleb128	2
      000984 00 00 82 9E           1884 	.dw	0,(Smain$prepocet$19)
      000988 00 00 82 A1           1885 	.dw	0,(Smain$prepocet$21)
      00098C 00 02                 1886 	.dw	2
      00098E 78                    1887 	.db	120
      00098F 01                    1888 	.sleb128	1
      000990 00 00 82 99           1889 	.dw	0,(Smain$prepocet$18)
      000994 00 00 82 9E           1890 	.dw	0,(Smain$prepocet$19)
      000998 00 02                 1891 	.dw	2
      00099A 78                    1892 	.db	120
      00099B 04                    1893 	.sleb128	4
      00099C 00 00 82 97           1894 	.dw	0,(Smain$prepocet$17)
      0009A0 00 00 82 99           1895 	.dw	0,(Smain$prepocet$18)
      0009A4 00 02                 1896 	.dw	2
      0009A6 78                    1897 	.db	120
      0009A7 03                    1898 	.sleb128	3
      0009A8 00 00 82 95           1899 	.dw	0,(Smain$prepocet$16)
      0009AC 00 00 82 97           1900 	.dw	0,(Smain$prepocet$17)
      0009B0 00 02                 1901 	.dw	2
      0009B2 78                    1902 	.db	120
      0009B3 02                    1903 	.sleb128	2
      0009B4 00 00 82 8E           1904 	.dw	0,(Smain$prepocet$14)
      0009B8 00 00 82 95           1905 	.dw	0,(Smain$prepocet$16)
      0009BC 00 02                 1906 	.dw	2
      0009BE 78                    1907 	.db	120
      0009BF 01                    1908 	.sleb128	1
      0009C0 00 00 82 89           1909 	.dw	0,(Smain$prepocet$13)
      0009C4 00 00 82 8E           1910 	.dw	0,(Smain$prepocet$14)
      0009C8 00 02                 1911 	.dw	2
      0009CA 78                    1912 	.db	120
      0009CB 05                    1913 	.sleb128	5
      0009CC 00 00 82 88           1914 	.dw	0,(Smain$prepocet$12)
      0009D0 00 00 82 89           1915 	.dw	0,(Smain$prepocet$13)
      0009D4 00 02                 1916 	.dw	2
      0009D6 78                    1917 	.db	120
      0009D7 03                    1918 	.sleb128	3
      0009D8 00 00 82 86           1919 	.dw	0,(Smain$prepocet$11)
      0009DC 00 00 82 88           1920 	.dw	0,(Smain$prepocet$12)
      0009E0 00 02                 1921 	.dw	2
      0009E2 78                    1922 	.db	120
      0009E3 02                    1923 	.sleb128	2
      0009E4 00 00 82 72           1924 	.dw	0,(Smain$prepocet$8)
      0009E8 00 00 82 86           1925 	.dw	0,(Smain$prepocet$11)
      0009EC 00 02                 1926 	.dw	2
      0009EE 78                    1927 	.db	120
      0009EF 01                    1928 	.sleb128	1
      0009F0 00 00 82 6D           1929 	.dw	0,(Smain$prepocet$7)
      0009F4 00 00 82 72           1930 	.dw	0,(Smain$prepocet$8)
      0009F8 00 02                 1931 	.dw	2
      0009FA 78                    1932 	.db	120
      0009FB 09                    1933 	.sleb128	9
      0009FC 00 00 82 69           1934 	.dw	0,(Smain$prepocet$6)
      000A00 00 00 82 6D           1935 	.dw	0,(Smain$prepocet$7)
      000A04 00 02                 1936 	.dw	2
      000A06 78                    1937 	.db	120
      000A07 07                    1938 	.sleb128	7
      000A08 00 00 82 65           1939 	.dw	0,(Smain$prepocet$5)
      000A0C 00 00 82 69           1940 	.dw	0,(Smain$prepocet$6)
      000A10 00 02                 1941 	.dw	2
      000A12 78                    1942 	.db	120
      000A13 05                    1943 	.sleb128	5
      000A14 00 00 82 63           1944 	.dw	0,(Smain$prepocet$4)
      000A18 00 00 82 65           1945 	.dw	0,(Smain$prepocet$5)
      000A1C 00 02                 1946 	.dw	2
      000A1E 78                    1947 	.db	120
      000A1F 04                    1948 	.sleb128	4
      000A20 00 00 82 61           1949 	.dw	0,(Smain$prepocet$3)
      000A24 00 00 82 63           1950 	.dw	0,(Smain$prepocet$4)
      000A28 00 02                 1951 	.dw	2
      000A2A 78                    1952 	.db	120
      000A2B 02                    1953 	.sleb128	2
      000A2C 00 00 82 5F           1954 	.dw	0,(Smain$prepocet$1)
      000A30 00 00 82 61           1955 	.dw	0,(Smain$prepocet$3)
      000A34 00 02                 1956 	.dw	2
      000A36 78                    1957 	.db	120
      000A37 01                    1958 	.sleb128	1
      000A38 00 00 00 00           1959 	.dw	0,0
      000A3C 00 00 00 00           1960 	.dw	0,0
                                   1961 
                                   1962 	.area .debug_abbrev (NOLOAD)
      00011E                       1963 Ldebug_abbrev:
      00011E 0B                    1964 	.uleb128	11
      00011F 34                    1965 	.uleb128	52
      000120 00                    1966 	.db	0
      000121 02                    1967 	.uleb128	2
      000122 0A                    1968 	.uleb128	10
      000123 03                    1969 	.uleb128	3
      000124 08                    1970 	.uleb128	8
      000125 3F                    1971 	.uleb128	63
      000126 0C                    1972 	.uleb128	12
      000127 49                    1973 	.uleb128	73
      000128 13                    1974 	.uleb128	19
      000129 00                    1975 	.uleb128	0
      00012A 00                    1976 	.uleb128	0
      00012B 0D                    1977 	.uleb128	13
      00012C 01                    1978 	.uleb128	1
      00012D 01                    1979 	.db	1
      00012E 01                    1980 	.uleb128	1
      00012F 13                    1981 	.uleb128	19
      000130 0B                    1982 	.uleb128	11
      000131 0B                    1983 	.uleb128	11
      000132 49                    1984 	.uleb128	73
      000133 13                    1985 	.uleb128	19
      000134 00                    1986 	.uleb128	0
      000135 00                    1987 	.uleb128	0
      000136 03                    1988 	.uleb128	3
      000137 2E                    1989 	.uleb128	46
      000138 01                    1990 	.db	1
      000139 01                    1991 	.uleb128	1
      00013A 13                    1992 	.uleb128	19
      00013B 03                    1993 	.uleb128	3
      00013C 08                    1994 	.uleb128	8
      00013D 11                    1995 	.uleb128	17
      00013E 01                    1996 	.uleb128	1
      00013F 12                    1997 	.uleb128	18
      000140 01                    1998 	.uleb128	1
      000141 3F                    1999 	.uleb128	63
      000142 0C                    2000 	.uleb128	12
      000143 40                    2001 	.uleb128	64
      000144 06                    2002 	.uleb128	6
      000145 00                    2003 	.uleb128	0
      000146 00                    2004 	.uleb128	0
      000147 09                    2005 	.uleb128	9
      000148 34                    2006 	.uleb128	52
      000149 00                    2007 	.db	0
      00014A 02                    2008 	.uleb128	2
      00014B 0A                    2009 	.uleb128	10
      00014C 03                    2010 	.uleb128	3
      00014D 08                    2011 	.uleb128	8
      00014E 49                    2012 	.uleb128	73
      00014F 13                    2013 	.uleb128	19
      000150 00                    2014 	.uleb128	0
      000151 00                    2015 	.uleb128	0
      000152 0C                    2016 	.uleb128	12
      000153 26                    2017 	.uleb128	38
      000154 00                    2018 	.db	0
      000155 49                    2019 	.uleb128	73
      000156 13                    2020 	.uleb128	19
      000157 00                    2021 	.uleb128	0
      000158 00                    2022 	.uleb128	0
      000159 08                    2023 	.uleb128	8
      00015A 0B                    2024 	.uleb128	11
      00015B 01                    2025 	.db	1
      00015C 11                    2026 	.uleb128	17
      00015D 01                    2027 	.uleb128	1
      00015E 00                    2028 	.uleb128	0
      00015F 00                    2029 	.uleb128	0
      000160 01                    2030 	.uleb128	1
      000161 11                    2031 	.uleb128	17
      000162 01                    2032 	.db	1
      000163 03                    2033 	.uleb128	3
      000164 08                    2034 	.uleb128	8
      000165 10                    2035 	.uleb128	16
      000166 06                    2036 	.uleb128	6
      000167 13                    2037 	.uleb128	19
      000168 0B                    2038 	.uleb128	11
      000169 25                    2039 	.uleb128	37
      00016A 08                    2040 	.uleb128	8
      00016B 00                    2041 	.uleb128	0
      00016C 00                    2042 	.uleb128	0
      00016D 04                    2043 	.uleb128	4
      00016E 0B                    2044 	.uleb128	11
      00016F 01                    2045 	.db	1
      000170 11                    2046 	.uleb128	17
      000171 01                    2047 	.uleb128	1
      000172 12                    2048 	.uleb128	18
      000173 01                    2049 	.uleb128	1
      000174 00                    2050 	.uleb128	0
      000175 00                    2051 	.uleb128	0
      000176 07                    2052 	.uleb128	7
      000177 0B                    2053 	.uleb128	11
      000178 00                    2054 	.db	0
      000179 11                    2055 	.uleb128	17
      00017A 01                    2056 	.uleb128	1
      00017B 12                    2057 	.uleb128	18
      00017C 01                    2058 	.uleb128	1
      00017D 00                    2059 	.uleb128	0
      00017E 00                    2060 	.uleb128	0
      00017F 06                    2061 	.uleb128	6
      000180 0B                    2062 	.uleb128	11
      000181 01                    2063 	.db	1
      000182 01                    2064 	.uleb128	1
      000183 13                    2065 	.uleb128	19
      000184 11                    2066 	.uleb128	17
      000185 01                    2067 	.uleb128	1
      000186 00                    2068 	.uleb128	0
      000187 00                    2069 	.uleb128	0
      000188 02                    2070 	.uleb128	2
      000189 2E                    2071 	.uleb128	46
      00018A 00                    2072 	.db	0
      00018B 03                    2073 	.uleb128	3
      00018C 08                    2074 	.uleb128	8
      00018D 11                    2075 	.uleb128	17
      00018E 01                    2076 	.uleb128	1
      00018F 12                    2077 	.uleb128	18
      000190 01                    2078 	.uleb128	1
      000191 3F                    2079 	.uleb128	63
      000192 0C                    2080 	.uleb128	12
      000193 40                    2081 	.uleb128	64
      000194 06                    2082 	.uleb128	6
      000195 00                    2083 	.uleb128	0
      000196 00                    2084 	.uleb128	0
      000197 05                    2085 	.uleb128	5
      000198 0B                    2086 	.uleb128	11
      000199 01                    2087 	.db	1
      00019A 01                    2088 	.uleb128	1
      00019B 13                    2089 	.uleb128	19
      00019C 11                    2090 	.uleb128	17
      00019D 01                    2091 	.uleb128	1
      00019E 12                    2092 	.uleb128	18
      00019F 01                    2093 	.uleb128	1
      0001A0 00                    2094 	.uleb128	0
      0001A1 00                    2095 	.uleb128	0
      0001A2 0E                    2096 	.uleb128	14
      0001A3 21                    2097 	.uleb128	33
      0001A4 00                    2098 	.db	0
      0001A5 2F                    2099 	.uleb128	47
      0001A6 0B                    2100 	.uleb128	11
      0001A7 00                    2101 	.uleb128	0
      0001A8 00                    2102 	.uleb128	0
      0001A9 0A                    2103 	.uleb128	10
      0001AA 24                    2104 	.uleb128	36
      0001AB 00                    2105 	.db	0
      0001AC 03                    2106 	.uleb128	3
      0001AD 08                    2107 	.uleb128	8
      0001AE 0B                    2108 	.uleb128	11
      0001AF 0B                    2109 	.uleb128	11
      0001B0 3E                    2110 	.uleb128	62
      0001B1 0B                    2111 	.uleb128	11
      0001B2 00                    2112 	.uleb128	0
      0001B3 00                    2113 	.uleb128	0
      0001B4 00                    2114 	.uleb128	0
                                   2115 
                                   2116 	.area .debug_info (NOLOAD)
      000255 00 00 02 45           2117 	.dw	0,Ldebug_info_end-Ldebug_info_start
      000259                       2118 Ldebug_info_start:
      000259 00 02                 2119 	.dw	2
      00025B 00 00 01 1E           2120 	.dw	0,(Ldebug_abbrev)
      00025F 04                    2121 	.db	4
      000260 01                    2122 	.uleb128	1
      000261 61 70 70 2F 73 72 63  2123 	.ascii "app/src/main.c"
             2F 6D 61 69 6E 2E 63
      00026F 00                    2124 	.db	0
      000270 00 00 02 95           2125 	.dw	0,(Ldebug_line_start+-4)
      000274 01                    2126 	.db	1
      000275 53 44 43 43 20 76 65  2127 	.ascii "SDCC version 4.1.0 #12072"
             72 73 69 6F 6E 20 34
             2E 31 2E 30 20 23 31
             32 30 37 32
      00028E 00                    2128 	.db	0
      00028F 02                    2129 	.uleb128	2
      000290 70 72 65 70 6F 63 65  2130 	.ascii "prepocet"
             74
      000298 00                    2131 	.db	0
      000299 00 00 82 5F           2132 	.dw	0,(_prepocet)
      00029D 00 00 82 B6           2133 	.dw	0,(XG$prepocet$0$0+1)
      0002A1 01                    2134 	.db	1
      0002A2 00 00 09 24           2135 	.dw	0,(Ldebug_loc_start+1508)
      0002A6 03                    2136 	.uleb128	3
      0002A7 00 00 01 09           2137 	.dw	0,265
      0002AB 6D 61 69 6E           2138 	.ascii "main"
      0002AF 00                    2139 	.db	0
      0002B0 00 00 82 B6           2140 	.dw	0,(_main)
      0002B4 00 00 84 DA           2141 	.dw	0,(XG$main$0$0+1)
      0002B8 01                    2142 	.db	1
      0002B9 00 00 03 40           2143 	.dw	0,(Ldebug_loc_start)
      0002BD 04                    2144 	.uleb128	4
      0002BE 00 00 83 53           2145 	.dw	0,(Smain$main$107)
      0002C2 00 00 84 D8           2146 	.dw	0,(Smain$main$238)
      0002C6 05                    2147 	.uleb128	5
      0002C7 00 00 00 E1           2148 	.dw	0,225
      0002CB 00 00 83 5D           2149 	.dw	0,(Smain$main$115)
      0002CF 00 00 84 D5           2150 	.dw	0,(Smain$main$237)
      0002D3 06                    2151 	.uleb128	6
      0002D4 00 00 00 BE           2152 	.dw	0,190
      0002D8 00 00 83 6B           2153 	.dw	0,(Smain$main$120)
      0002DC 07                    2154 	.uleb128	7
      0002DD 00 00 83 75           2155 	.dw	0,(Smain$main$124)
      0002E1 00 00 83 C0           2156 	.dw	0,(Smain$main$148)
      0002E5 07                    2157 	.uleb128	7
      0002E6 00 00 83 C7           2158 	.dw	0,(Smain$main$151)
      0002EA 00 00 83 E4           2159 	.dw	0,(Smain$main$157)
      0002EE 07                    2160 	.uleb128	7
      0002EF 00 00 83 EB           2161 	.dw	0,(Smain$main$160)
      0002F3 00 00 84 08           2162 	.dw	0,(Smain$main$166)
      0002F7 07                    2163 	.uleb128	7
      0002F8 00 00 84 0F           2164 	.dw	0,(Smain$main$169)
      0002FC 00 00 84 2C           2165 	.dw	0,(Smain$main$175)
      000300 07                    2166 	.uleb128	7
      000301 00 00 84 32           2167 	.dw	0,(Smain$main$178)
      000305 00 00 84 4F           2168 	.dw	0,(Smain$main$184)
      000309 07                    2169 	.uleb128	7
      00030A 00 00 84 51           2170 	.dw	0,(Smain$main$185)
      00030E 00 00 84 59           2171 	.dw	0,(Smain$main$190)
      000312 00                    2172 	.uleb128	0
      000313 06                    2173 	.uleb128	6
      000314 00 00 00 D1           2174 	.dw	0,209
      000318 00 00 84 6C           2175 	.dw	0,(Smain$main$197)
      00031C 07                    2176 	.uleb128	7
      00031D 00 00 84 71           2177 	.dw	0,(Smain$main$200)
      000321 00 00 84 91           2178 	.dw	0,(Smain$main$216)
      000325 00                    2179 	.uleb128	0
      000326 08                    2180 	.uleb128	8
      000327 00 00 84 9A           2181 	.dw	0,(Smain$main$218)
      00032B 07                    2182 	.uleb128	7
      00032C 00 00 84 9E           2183 	.dw	0,(Smain$main$220)
      000330 00 00 84 CA           2184 	.dw	0,(Smain$main$232)
      000334 00                    2185 	.uleb128	0
      000335 00                    2186 	.uleb128	0
      000336 09                    2187 	.uleb128	9
      000337 01                    2188 	.db	1
      000338 50                    2189 	.db	80
      000339 6B 65 79              2190 	.ascii "key"
      00033C 00                    2191 	.db	0
      00033D 00 00 01 09           2192 	.dw	0,265
      000341 09                    2193 	.uleb128	9
      000342 02                    2194 	.db	2
      000343 91                    2195 	.db	145
      000344 7F                    2196 	.sleb128	-1
      000345 61 6C 70 68 61        2197 	.ascii "alpha"
      00034A 00                    2198 	.db	0
      00034B 00 00 01 09           2199 	.dw	0,265
      00034F 09                    2200 	.uleb128	9
      000350 02                    2201 	.db	2
      000351 91                    2202 	.db	145
      000352 7E                    2203 	.sleb128	-2
      000353 62 65 74 61           2204 	.ascii "beta"
      000357 00                    2205 	.db	0
      000358 00 00 01 09           2206 	.dw	0,265
      00035C 00                    2207 	.uleb128	0
      00035D 00                    2208 	.uleb128	0
      00035E 0A                    2209 	.uleb128	10
      00035F 75 6E 73 69 67 6E 65  2210 	.ascii "unsigned char"
             64 20 63 68 61 72
      00036C 00                    2211 	.db	0
      00036D 01                    2212 	.db	1
      00036E 08                    2213 	.db	8
      00036F 0A                    2214 	.uleb128	10
      000370 75 6E 73 69 67 6E 65  2215 	.ascii "unsigned long"
             64 20 6C 6F 6E 67
      00037D 00                    2216 	.db	0
      00037E 04                    2217 	.db	4
      00037F 07                    2218 	.db	7
      000380 0B                    2219 	.uleb128	11
      000381 05                    2220 	.db	5
      000382 03                    2221 	.db	3
      000383 00 00 00 03           2222 	.dw	0,(_js)
      000387 6A 73                 2223 	.ascii "js"
      000389 00                    2224 	.db	0
      00038A 01                    2225 	.db	1
      00038B 00 00 01 1A           2226 	.dw	0,282
      00038F 0B                    2227 	.uleb128	11
      000390 05                    2228 	.db	5
      000391 03                    2229 	.db	3
      000392 00 00 00 07           2230 	.dw	0,(_kroky)
      000396 6B 72 6F 6B 79        2231 	.ascii "kroky"
      00039B 00                    2232 	.db	0
      00039C 01                    2233 	.db	1
      00039D 00 00 01 1A           2234 	.dw	0,282
      0003A1 0B                    2235 	.uleb128	11
      0003A2 05                    2236 	.db	5
      0003A3 03                    2237 	.db	3
      0003A4 00 00 00 01           2238 	.dw	0,(_a)
      0003A8 61                    2239 	.ascii "a"
      0003A9 00                    2240 	.db	0
      0003AA 01                    2241 	.db	1
      0003AB 00 00 01 09           2242 	.dw	0,265
      0003AF 0B                    2243 	.uleb128	11
      0003B0 05                    2244 	.db	5
      0003B1 03                    2245 	.db	3
      0003B2 00 00 00 02           2246 	.dw	0,(_b)
      0003B6 62                    2247 	.ascii "b"
      0003B7 00                    2248 	.db	0
      0003B8 01                    2249 	.db	1
      0003B9 00 00 01 09           2250 	.dw	0,265
      0003BD 0C                    2251 	.uleb128	12
      0003BE 00 00 01 09           2252 	.dw	0,265
      0003C2 0D                    2253 	.uleb128	13
      0003C3 00 00 01 7A           2254 	.dw	0,378
      0003C7 1A                    2255 	.db	26
      0003C8 00 00 01 68           2256 	.dw	0,360
      0003CC 0E                    2257 	.uleb128	14
      0003CD 19                    2258 	.db	25
      0003CE 00                    2259 	.uleb128	0
      0003CF 09                    2260 	.uleb128	9
      0003D0 05                    2261 	.db	5
      0003D1 03                    2262 	.db	3
      0003D2 00 00 80 8C           2263 	.dw	0,(___str_0)
      0003D6 5F 5F 73 74 72 5F 30  2264 	.ascii "__str_0"
      0003DD 00                    2265 	.db	0
      0003DE 00 00 01 6D           2266 	.dw	0,365
      0003E2 0D                    2267 	.uleb128	13
      0003E3 00 00 01 9A           2268 	.dw	0,410
      0003E7 0D                    2269 	.db	13
      0003E8 00 00 01 68           2270 	.dw	0,360
      0003EC 0E                    2271 	.uleb128	14
      0003ED 0C                    2272 	.db	12
      0003EE 00                    2273 	.uleb128	0
      0003EF 09                    2274 	.uleb128	9
      0003F0 05                    2275 	.db	5
      0003F1 03                    2276 	.db	3
      0003F2 00 00 80 A6           2277 	.dw	0,(___str_1)
      0003F6 5F 5F 73 74 72 5F 31  2278 	.ascii "__str_1"
      0003FD 00                    2279 	.db	0
      0003FE 00 00 01 8D           2280 	.dw	0,397
      000402 0D                    2281 	.uleb128	13
      000403 00 00 01 BA           2282 	.dw	0,442
      000407 06                    2283 	.db	6
      000408 00 00 01 68           2284 	.dw	0,360
      00040C 0E                    2285 	.uleb128	14
      00040D 05                    2286 	.db	5
      00040E 00                    2287 	.uleb128	0
      00040F 09                    2288 	.uleb128	9
      000410 05                    2289 	.db	5
      000411 03                    2290 	.db	3
      000412 00 00 80 B3           2291 	.dw	0,(___str_2)
      000416 5F 5F 73 74 72 5F 32  2292 	.ascii "__str_2"
      00041D 00                    2293 	.db	0
      00041E 00 00 01 AD           2294 	.dw	0,429
      000422 09                    2295 	.uleb128	9
      000423 05                    2296 	.db	5
      000424 03                    2297 	.db	3
      000425 00 00 80 B9           2298 	.dw	0,(___str_3)
      000429 5F 5F 73 74 72 5F 33  2299 	.ascii "__str_3"
      000430 00                    2300 	.db	0
      000431 00 00 01 AD           2301 	.dw	0,429
      000435 0D                    2302 	.uleb128	13
      000436 00 00 01 ED           2303 	.dw	0,493
      00043A 05                    2304 	.db	5
      00043B 00 00 01 68           2305 	.dw	0,360
      00043F 0E                    2306 	.uleb128	14
      000440 04                    2307 	.db	4
      000441 00                    2308 	.uleb128	0
      000442 09                    2309 	.uleb128	9
      000443 05                    2310 	.db	5
      000444 03                    2311 	.db	3
      000445 00 00 80 BF           2312 	.dw	0,(___str_4)
      000449 5F 5F 73 74 72 5F 34  2313 	.ascii "__str_4"
      000450 00                    2314 	.db	0
      000451 00 00 01 E0           2315 	.dw	0,480
      000455 09                    2316 	.uleb128	9
      000456 05                    2317 	.db	5
      000457 03                    2318 	.db	3
      000458 00 00 80 C4           2319 	.dw	0,(___str_5)
      00045C 5F 5F 73 74 72 5F 35  2320 	.ascii "__str_5"
      000463 00                    2321 	.db	0
      000464 00 00 01 E0           2322 	.dw	0,480
      000468 0D                    2323 	.uleb128	13
      000469 00 00 02 20           2324 	.dw	0,544
      00046D 08                    2325 	.db	8
      00046E 00 00 01 68           2326 	.dw	0,360
      000472 0E                    2327 	.uleb128	14
      000473 07                    2328 	.db	7
      000474 00                    2329 	.uleb128	0
      000475 09                    2330 	.uleb128	9
      000476 05                    2331 	.db	5
      000477 03                    2332 	.db	3
      000478 00 00 80 C9           2333 	.dw	0,(___str_6)
      00047C 5F 5F 73 74 72 5F 36  2334 	.ascii "__str_6"
      000483 00                    2335 	.db	0
      000484 00 00 02 13           2336 	.dw	0,531
      000488 09                    2337 	.uleb128	9
      000489 05                    2338 	.db	5
      00048A 03                    2339 	.db	3
      00048B 00 00 80 D1           2340 	.dw	0,(___str_7)
      00048F 5F 5F 73 74 72 5F 37  2341 	.ascii "__str_7"
      000496 00                    2342 	.db	0
      000497 00 00 01 E0           2343 	.dw	0,480
      00049B 00                    2344 	.uleb128	0
      00049C 00                    2345 	.uleb128	0
      00049D 00                    2346 	.uleb128	0
      00049E                       2347 Ldebug_info_end:
                                   2348 
                                   2349 	.area .debug_pubnames (NOLOAD)
      0000AF 00 00 00 41           2350 	.dw	0,Ldebug_pubnames_end-Ldebug_pubnames_start
      0000B3                       2351 Ldebug_pubnames_start:
      0000B3 00 02                 2352 	.dw	2
      0000B5 00 00 02 55           2353 	.dw	0,(Ldebug_info_start-4)
      0000B9 00 00 02 49           2354 	.dw	0,4+Ldebug_info_end-Ldebug_info_start
      0000BD 00 00 00 3A           2355 	.dw	0,58
      0000C1 70 72 65 70 6F 63 65  2356 	.ascii "prepocet"
             74
      0000C9 00                    2357 	.db	0
      0000CA 00 00 00 51           2358 	.dw	0,81
      0000CE 6D 61 69 6E           2359 	.ascii "main"
      0000D2 00                    2360 	.db	0
      0000D3 00 00 01 2B           2361 	.dw	0,299
      0000D7 6A 73                 2362 	.ascii "js"
      0000D9 00                    2363 	.db	0
      0000DA 00 00 01 3A           2364 	.dw	0,314
      0000DE 6B 72 6F 6B 79        2365 	.ascii "kroky"
      0000E3 00                    2366 	.db	0
      0000E4 00 00 01 4C           2367 	.dw	0,332
      0000E8 61                    2368 	.ascii "a"
      0000E9 00                    2369 	.db	0
      0000EA 00 00 01 5A           2370 	.dw	0,346
      0000EE 62                    2371 	.ascii "b"
      0000EF 00                    2372 	.db	0
      0000F0 00 00 00 00           2373 	.dw	0,0
      0000F4                       2374 Ldebug_pubnames_end:
                                   2375 
                                   2376 	.area .debug_frame (NOLOAD)
      0002D0 00 00                 2377 	.dw	0
      0002D2 00 0E                 2378 	.dw	Ldebug_CIE0_end-Ldebug_CIE0_start
      0002D4                       2379 Ldebug_CIE0_start:
      0002D4 FF FF                 2380 	.dw	0xffff
      0002D6 FF FF                 2381 	.dw	0xffff
      0002D8 01                    2382 	.db	1
      0002D9 00                    2383 	.db	0
      0002DA 01                    2384 	.uleb128	1
      0002DB 7F                    2385 	.sleb128	-1
      0002DC 09                    2386 	.db	9
      0002DD 0C                    2387 	.db	12
      0002DE 08                    2388 	.uleb128	8
      0002DF 02                    2389 	.uleb128	2
      0002E0 89                    2390 	.db	137
      0002E1 01                    2391 	.uleb128	1
      0002E2                       2392 Ldebug_CIE0_end:
      0002E2 00 00 03 77           2393 	.dw	0,887
      0002E6 00 00 02 D0           2394 	.dw	0,(Ldebug_CIE0_start-4)
      0002EA 00 00 82 B6           2395 	.dw	0,(Smain$main$34)	;initial loc
      0002EE 00 00 02 24           2396 	.dw	0,Smain$main$242-Smain$main$34
      0002F2 01                    2397 	.db	1
      0002F3 00 00 82 B6           2398 	.dw	0,(Smain$main$34)
      0002F7 0E                    2399 	.db	14
      0002F8 02                    2400 	.uleb128	2
      0002F9 01                    2401 	.db	1
      0002FA 00 00 82 B7           2402 	.dw	0,(Smain$main$35)
      0002FE 0E                    2403 	.db	14
      0002FF 04                    2404 	.uleb128	4
      000300 01                    2405 	.db	1
      000301 00 00 82 B9           2406 	.dw	0,(Smain$main$37)
      000305 0E                    2407 	.db	14
      000306 05                    2408 	.uleb128	5
      000307 01                    2409 	.db	1
      000308 00 00 82 BD           2410 	.dw	0,(Smain$main$38)
      00030C 0E                    2411 	.db	14
      00030D 04                    2412 	.uleb128	4
      00030E 01                    2413 	.db	1
      00030F 00 00 82 BF           2414 	.dw	0,(Smain$main$40)
      000313 0E                    2415 	.db	14
      000314 05                    2416 	.uleb128	5
      000315 01                    2417 	.db	1
      000316 00 00 82 C1           2418 	.dw	0,(Smain$main$41)
      00031A 0E                    2419 	.db	14
      00031B 06                    2420 	.uleb128	6
      00031C 01                    2421 	.db	1
      00031D 00 00 82 C3           2422 	.dw	0,(Smain$main$42)
      000321 0E                    2423 	.db	14
      000322 07                    2424 	.uleb128	7
      000323 01                    2425 	.db	1
      000324 00 00 82 C5           2426 	.dw	0,(Smain$main$43)
      000328 0E                    2427 	.db	14
      000329 08                    2428 	.uleb128	8
      00032A 01                    2429 	.db	1
      00032B 00 00 82 CA           2430 	.dw	0,(Smain$main$44)
      00032F 0E                    2431 	.db	14
      000330 04                    2432 	.uleb128	4
      000331 01                    2433 	.db	1
      000332 00 00 82 CC           2434 	.dw	0,(Smain$main$46)
      000336 0E                    2435 	.db	14
      000337 05                    2436 	.uleb128	5
      000338 01                    2437 	.db	1
      000339 00 00 82 CE           2438 	.dw	0,(Smain$main$47)
      00033D 0E                    2439 	.db	14
      00033E 06                    2440 	.uleb128	6
      00033F 01                    2441 	.db	1
      000340 00 00 82 D0           2442 	.dw	0,(Smain$main$48)
      000344 0E                    2443 	.db	14
      000345 07                    2444 	.uleb128	7
      000346 01                    2445 	.db	1
      000347 00 00 82 D2           2446 	.dw	0,(Smain$main$49)
      00034B 0E                    2447 	.db	14
      00034C 08                    2448 	.uleb128	8
      00034D 01                    2449 	.db	1
      00034E 00 00 82 D7           2450 	.dw	0,(Smain$main$50)
      000352 0E                    2451 	.db	14
      000353 04                    2452 	.uleb128	4
      000354 01                    2453 	.db	1
      000355 00 00 82 D9           2454 	.dw	0,(Smain$main$52)
      000359 0E                    2455 	.db	14
      00035A 05                    2456 	.uleb128	5
      00035B 01                    2457 	.db	1
      00035C 00 00 82 DB           2458 	.dw	0,(Smain$main$53)
      000360 0E                    2459 	.db	14
      000361 06                    2460 	.uleb128	6
      000362 01                    2461 	.db	1
      000363 00 00 82 DD           2462 	.dw	0,(Smain$main$54)
      000367 0E                    2463 	.db	14
      000368 07                    2464 	.uleb128	7
      000369 01                    2465 	.db	1
      00036A 00 00 82 DF           2466 	.dw	0,(Smain$main$55)
      00036E 0E                    2467 	.db	14
      00036F 08                    2468 	.uleb128	8
      000370 01                    2469 	.db	1
      000371 00 00 82 E4           2470 	.dw	0,(Smain$main$56)
      000375 0E                    2471 	.db	14
      000376 04                    2472 	.uleb128	4
      000377 01                    2473 	.db	1
      000378 00 00 82 E6           2474 	.dw	0,(Smain$main$58)
      00037C 0E                    2475 	.db	14
      00037D 05                    2476 	.uleb128	5
      00037E 01                    2477 	.db	1
      00037F 00 00 82 E8           2478 	.dw	0,(Smain$main$59)
      000383 0E                    2479 	.db	14
      000384 06                    2480 	.uleb128	6
      000385 01                    2481 	.db	1
      000386 00 00 82 EA           2482 	.dw	0,(Smain$main$60)
      00038A 0E                    2483 	.db	14
      00038B 07                    2484 	.uleb128	7
      00038C 01                    2485 	.db	1
      00038D 00 00 82 EC           2486 	.dw	0,(Smain$main$61)
      000391 0E                    2487 	.db	14
      000392 08                    2488 	.uleb128	8
      000393 01                    2489 	.db	1
      000394 00 00 82 F1           2490 	.dw	0,(Smain$main$62)
      000398 0E                    2491 	.db	14
      000399 04                    2492 	.uleb128	4
      00039A 01                    2493 	.db	1
      00039B 00 00 82 F3           2494 	.dw	0,(Smain$main$64)
      00039F 0E                    2495 	.db	14
      0003A0 05                    2496 	.uleb128	5
      0003A1 01                    2497 	.db	1
      0003A2 00 00 82 F5           2498 	.dw	0,(Smain$main$65)
      0003A6 0E                    2499 	.db	14
      0003A7 06                    2500 	.uleb128	6
      0003A8 01                    2501 	.db	1
      0003A9 00 00 82 F7           2502 	.dw	0,(Smain$main$66)
      0003AD 0E                    2503 	.db	14
      0003AE 07                    2504 	.uleb128	7
      0003AF 01                    2505 	.db	1
      0003B0 00 00 82 F9           2506 	.dw	0,(Smain$main$67)
      0003B4 0E                    2507 	.db	14
      0003B5 08                    2508 	.uleb128	8
      0003B6 01                    2509 	.db	1
      0003B7 00 00 82 FE           2510 	.dw	0,(Smain$main$68)
      0003BB 0E                    2511 	.db	14
      0003BC 04                    2512 	.uleb128	4
      0003BD 01                    2513 	.db	1
      0003BE 00 00 83 00           2514 	.dw	0,(Smain$main$70)
      0003C2 0E                    2515 	.db	14
      0003C3 05                    2516 	.uleb128	5
      0003C4 01                    2517 	.db	1
      0003C5 00 00 83 02           2518 	.dw	0,(Smain$main$71)
      0003C9 0E                    2519 	.db	14
      0003CA 06                    2520 	.uleb128	6
      0003CB 01                    2521 	.db	1
      0003CC 00 00 83 04           2522 	.dw	0,(Smain$main$72)
      0003D0 0E                    2523 	.db	14
      0003D1 07                    2524 	.uleb128	7
      0003D2 01                    2525 	.db	1
      0003D3 00 00 83 06           2526 	.dw	0,(Smain$main$73)
      0003D7 0E                    2527 	.db	14
      0003D8 08                    2528 	.uleb128	8
      0003D9 01                    2529 	.db	1
      0003DA 00 00 83 0B           2530 	.dw	0,(Smain$main$74)
      0003DE 0E                    2531 	.db	14
      0003DF 04                    2532 	.uleb128	4
      0003E0 01                    2533 	.db	1
      0003E1 00 00 83 0D           2534 	.dw	0,(Smain$main$76)
      0003E5 0E                    2535 	.db	14
      0003E6 05                    2536 	.uleb128	5
      0003E7 01                    2537 	.db	1
      0003E8 00 00 83 0F           2538 	.dw	0,(Smain$main$77)
      0003EC 0E                    2539 	.db	14
      0003ED 06                    2540 	.uleb128	6
      0003EE 01                    2541 	.db	1
      0003EF 00 00 83 11           2542 	.dw	0,(Smain$main$78)
      0003F3 0E                    2543 	.db	14
      0003F4 07                    2544 	.uleb128	7
      0003F5 01                    2545 	.db	1
      0003F6 00 00 83 13           2546 	.dw	0,(Smain$main$79)
      0003FA 0E                    2547 	.db	14
      0003FB 08                    2548 	.uleb128	8
      0003FC 01                    2549 	.db	1
      0003FD 00 00 83 18           2550 	.dw	0,(Smain$main$80)
      000401 0E                    2551 	.db	14
      000402 04                    2552 	.uleb128	4
      000403 01                    2553 	.db	1
      000404 00 00 83 1A           2554 	.dw	0,(Smain$main$82)
      000408 0E                    2555 	.db	14
      000409 05                    2556 	.uleb128	5
      00040A 01                    2557 	.db	1
      00040B 00 00 83 1C           2558 	.dw	0,(Smain$main$83)
      00040F 0E                    2559 	.db	14
      000410 06                    2560 	.uleb128	6
      000411 01                    2561 	.db	1
      000412 00 00 83 1E           2562 	.dw	0,(Smain$main$84)
      000416 0E                    2563 	.db	14
      000417 07                    2564 	.uleb128	7
      000418 01                    2565 	.db	1
      000419 00 00 83 20           2566 	.dw	0,(Smain$main$85)
      00041D 0E                    2567 	.db	14
      00041E 08                    2568 	.uleb128	8
      00041F 01                    2569 	.db	1
      000420 00 00 83 25           2570 	.dw	0,(Smain$main$86)
      000424 0E                    2571 	.db	14
      000425 04                    2572 	.uleb128	4
      000426 01                    2573 	.db	1
      000427 00 00 83 27           2574 	.dw	0,(Smain$main$88)
      00042B 0E                    2575 	.db	14
      00042C 05                    2576 	.uleb128	5
      00042D 01                    2577 	.db	1
      00042E 00 00 83 29           2578 	.dw	0,(Smain$main$89)
      000432 0E                    2579 	.db	14
      000433 06                    2580 	.uleb128	6
      000434 01                    2581 	.db	1
      000435 00 00 83 2B           2582 	.dw	0,(Smain$main$90)
      000439 0E                    2583 	.db	14
      00043A 07                    2584 	.uleb128	7
      00043B 01                    2585 	.db	1
      00043C 00 00 83 2D           2586 	.dw	0,(Smain$main$91)
      000440 0E                    2587 	.db	14
      000441 08                    2588 	.uleb128	8
      000442 01                    2589 	.db	1
      000443 00 00 83 32           2590 	.dw	0,(Smain$main$92)
      000447 0E                    2591 	.db	14
      000448 04                    2592 	.uleb128	4
      000449 01                    2593 	.db	1
      00044A 00 00 83 34           2594 	.dw	0,(Smain$main$94)
      00044E 0E                    2595 	.db	14
      00044F 05                    2596 	.uleb128	5
      000450 01                    2597 	.db	1
      000451 00 00 83 36           2598 	.dw	0,(Smain$main$95)
      000455 0E                    2599 	.db	14
      000456 06                    2600 	.uleb128	6
      000457 01                    2601 	.db	1
      000458 00 00 83 38           2602 	.dw	0,(Smain$main$96)
      00045C 0E                    2603 	.db	14
      00045D 07                    2604 	.uleb128	7
      00045E 01                    2605 	.db	1
      00045F 00 00 83 3D           2606 	.dw	0,(Smain$main$97)
      000463 0E                    2607 	.db	14
      000464 04                    2608 	.uleb128	4
      000465 01                    2609 	.db	1
      000466 00 00 83 3F           2610 	.dw	0,(Smain$main$99)
      00046A 0E                    2611 	.db	14
      00046B 05                    2612 	.uleb128	5
      00046C 01                    2613 	.db	1
      00046D 00 00 83 41           2614 	.dw	0,(Smain$main$100)
      000471 0E                    2615 	.db	14
      000472 06                    2616 	.uleb128	6
      000473 01                    2617 	.db	1
      000474 00 00 83 43           2618 	.dw	0,(Smain$main$101)
      000478 0E                    2619 	.db	14
      000479 07                    2620 	.uleb128	7
      00047A 01                    2621 	.db	1
      00047B 00 00 83 45           2622 	.dw	0,(Smain$main$102)
      00047F 0E                    2623 	.db	14
      000480 08                    2624 	.uleb128	8
      000481 01                    2625 	.db	1
      000482 00 00 83 4A           2626 	.dw	0,(Smain$main$103)
      000486 0E                    2627 	.db	14
      000487 04                    2628 	.uleb128	4
      000488 01                    2629 	.db	1
      000489 00 00 83 57           2630 	.dw	0,(Smain$main$111)
      00048D 0E                    2631 	.db	14
      00048E 05                    2632 	.uleb128	5
      00048F 01                    2633 	.db	1
      000490 00 00 83 59           2634 	.dw	0,(Smain$main$112)
      000494 0E                    2635 	.db	14
      000495 06                    2636 	.uleb128	6
      000496 01                    2637 	.db	1
      000497 00 00 83 5D           2638 	.dw	0,(Smain$main$113)
      00049B 0E                    2639 	.db	14
      00049C 04                    2640 	.uleb128	4
      00049D 01                    2641 	.db	1
      00049E 00 00 83 5F           2642 	.dw	0,(Smain$main$117)
      0004A2 0E                    2643 	.db	14
      0004A3 05                    2644 	.uleb128	5
      0004A4 01                    2645 	.db	1
      0004A5 00 00 83 61           2646 	.dw	0,(Smain$main$118)
      0004A9 0E                    2647 	.db	14
      0004AA 06                    2648 	.uleb128	6
      0004AB 01                    2649 	.db	1
      0004AC 00 00 83 65           2650 	.dw	0,(Smain$main$119)
      0004B0 0E                    2651 	.db	14
      0004B1 04                    2652 	.uleb128	4
      0004B2 01                    2653 	.db	1
      0004B3 00 00 83 75           2654 	.dw	0,(Smain$main$123)
      0004B7 0E                    2655 	.db	14
      0004B8 04                    2656 	.uleb128	4
      0004B9 01                    2657 	.db	1
      0004BA 00 00 83 81           2658 	.dw	0,(Smain$main$127)
      0004BE 0E                    2659 	.db	14
      0004BF 05                    2660 	.uleb128	5
      0004C0 01                    2661 	.db	1
      0004C1 00 00 83 83           2662 	.dw	0,(Smain$main$128)
      0004C5 0E                    2663 	.db	14
      0004C6 07                    2664 	.uleb128	7
      0004C7 01                    2665 	.db	1
      0004C8 00 00 83 85           2666 	.dw	0,(Smain$main$129)
      0004CC 0E                    2667 	.db	14
      0004CD 08                    2668 	.uleb128	8
      0004CE 01                    2669 	.db	1
      0004CF 00 00 83 8A           2670 	.dw	0,(Smain$main$130)
      0004D3 0E                    2671 	.db	14
      0004D4 04                    2672 	.uleb128	4
      0004D5 01                    2673 	.db	1
      0004D6 00 00 83 93           2674 	.dw	0,(Smain$main$133)
      0004DA 0E                    2675 	.db	14
      0004DB 05                    2676 	.uleb128	5
      0004DC 01                    2677 	.db	1
      0004DD 00 00 83 95           2678 	.dw	0,(Smain$main$134)
      0004E1 0E                    2679 	.db	14
      0004E2 07                    2680 	.uleb128	7
      0004E3 01                    2681 	.db	1
      0004E4 00 00 83 97           2682 	.dw	0,(Smain$main$135)
      0004E8 0E                    2683 	.db	14
      0004E9 08                    2684 	.uleb128	8
      0004EA 01                    2685 	.db	1
      0004EB 00 00 83 9C           2686 	.dw	0,(Smain$main$136)
      0004EF 0E                    2687 	.db	14
      0004F0 04                    2688 	.uleb128	4
      0004F1 01                    2689 	.db	1
      0004F2 00 00 83 A8           2690 	.dw	0,(Smain$main$139)
      0004F6 0E                    2691 	.db	14
      0004F7 05                    2692 	.uleb128	5
      0004F8 01                    2693 	.db	1
      0004F9 00 00 83 AA           2694 	.dw	0,(Smain$main$140)
      0004FD 0E                    2695 	.db	14
      0004FE 07                    2696 	.uleb128	7
      0004FF 01                    2697 	.db	1
      000500 00 00 83 AC           2698 	.dw	0,(Smain$main$141)
      000504 0E                    2699 	.db	14
      000505 08                    2700 	.uleb128	8
      000506 01                    2701 	.db	1
      000507 00 00 83 B1           2702 	.dw	0,(Smain$main$142)
      00050B 0E                    2703 	.db	14
      00050C 04                    2704 	.uleb128	4
      00050D 01                    2705 	.db	1
      00050E 00 00 83 BA           2706 	.dw	0,(Smain$main$145)
      000512 0E                    2707 	.db	14
      000513 05                    2708 	.uleb128	5
      000514 01                    2709 	.db	1
      000515 00 00 83 BC           2710 	.dw	0,(Smain$main$146)
      000519 0E                    2711 	.db	14
      00051A 06                    2712 	.uleb128	6
      00051B 01                    2713 	.db	1
      00051C 00 00 83 C0           2714 	.dw	0,(Smain$main$147)
      000520 0E                    2715 	.db	14
      000521 04                    2716 	.uleb128	4
      000522 01                    2717 	.db	1
      000523 00 00 83 C7           2718 	.dw	0,(Smain$main$150)
      000527 0E                    2719 	.db	14
      000528 04                    2720 	.uleb128	4
      000529 01                    2721 	.db	1
      00052A 00 00 83 DE           2722 	.dw	0,(Smain$main$154)
      00052E 0E                    2723 	.db	14
      00052F 05                    2724 	.uleb128	5
      000530 01                    2725 	.db	1
      000531 00 00 83 E0           2726 	.dw	0,(Smain$main$155)
      000535 0E                    2727 	.db	14
      000536 06                    2728 	.uleb128	6
      000537 01                    2729 	.db	1
      000538 00 00 83 E4           2730 	.dw	0,(Smain$main$156)
      00053C 0E                    2731 	.db	14
      00053D 04                    2732 	.uleb128	4
      00053E 01                    2733 	.db	1
      00053F 00 00 83 EB           2734 	.dw	0,(Smain$main$159)
      000543 0E                    2735 	.db	14
      000544 04                    2736 	.uleb128	4
      000545 01                    2737 	.db	1
      000546 00 00 84 02           2738 	.dw	0,(Smain$main$163)
      00054A 0E                    2739 	.db	14
      00054B 05                    2740 	.uleb128	5
      00054C 01                    2741 	.db	1
      00054D 00 00 84 04           2742 	.dw	0,(Smain$main$164)
      000551 0E                    2743 	.db	14
      000552 06                    2744 	.uleb128	6
      000553 01                    2745 	.db	1
      000554 00 00 84 08           2746 	.dw	0,(Smain$main$165)
      000558 0E                    2747 	.db	14
      000559 04                    2748 	.uleb128	4
      00055A 01                    2749 	.db	1
      00055B 00 00 84 0F           2750 	.dw	0,(Smain$main$168)
      00055F 0E                    2751 	.db	14
      000560 04                    2752 	.uleb128	4
      000561 01                    2753 	.db	1
      000562 00 00 84 26           2754 	.dw	0,(Smain$main$172)
      000566 0E                    2755 	.db	14
      000567 05                    2756 	.uleb128	5
      000568 01                    2757 	.db	1
      000569 00 00 84 28           2758 	.dw	0,(Smain$main$173)
      00056D 0E                    2759 	.db	14
      00056E 06                    2760 	.uleb128	6
      00056F 01                    2761 	.db	1
      000570 00 00 84 2C           2762 	.dw	0,(Smain$main$174)
      000574 0E                    2763 	.db	14
      000575 04                    2764 	.uleb128	4
      000576 01                    2765 	.db	1
      000577 00 00 84 32           2766 	.dw	0,(Smain$main$177)
      00057B 0E                    2767 	.db	14
      00057C 04                    2768 	.uleb128	4
      00057D 01                    2769 	.db	1
      00057E 00 00 84 49           2770 	.dw	0,(Smain$main$181)
      000582 0E                    2771 	.db	14
      000583 05                    2772 	.uleb128	5
      000584 01                    2773 	.db	1
      000585 00 00 84 4B           2774 	.dw	0,(Smain$main$182)
      000589 0E                    2775 	.db	14
      00058A 06                    2776 	.uleb128	6
      00058B 01                    2777 	.db	1
      00058C 00 00 84 4F           2778 	.dw	0,(Smain$main$183)
      000590 0E                    2779 	.db	14
      000591 04                    2780 	.uleb128	4
      000592 01                    2781 	.db	1
      000593 00 00 84 53           2782 	.dw	0,(Smain$main$187)
      000597 0E                    2783 	.db	14
      000598 05                    2784 	.uleb128	5
      000599 01                    2785 	.db	1
      00059A 00 00 84 55           2786 	.dw	0,(Smain$main$188)
      00059E 0E                    2787 	.db	14
      00059F 06                    2788 	.uleb128	6
      0005A0 01                    2789 	.db	1
      0005A1 00 00 84 59           2790 	.dw	0,(Smain$main$189)
      0005A5 0E                    2791 	.db	14
      0005A6 04                    2792 	.uleb128	4
      0005A7 01                    2793 	.db	1
      0005A8 00 00 84 5B           2794 	.dw	0,(Smain$main$192)
      0005AC 0E                    2795 	.db	14
      0005AD 05                    2796 	.uleb128	5
      0005AE 01                    2797 	.db	1
      0005AF 00 00 84 5D           2798 	.dw	0,(Smain$main$193)
      0005B3 0E                    2799 	.db	14
      0005B4 06                    2800 	.uleb128	6
      0005B5 01                    2801 	.db	1
      0005B6 00 00 84 5F           2802 	.dw	0,(Smain$main$194)
      0005BA 0E                    2803 	.db	14
      0005BB 07                    2804 	.uleb128	7
      0005BC 01                    2805 	.db	1
      0005BD 00 00 84 64           2806 	.dw	0,(Smain$main$195)
      0005C1 0E                    2807 	.db	14
      0005C2 04                    2808 	.uleb128	4
      0005C3 01                    2809 	.db	1
      0005C4 00 00 84 71           2810 	.dw	0,(Smain$main$199)
      0005C8 0E                    2811 	.db	14
      0005C9 04                    2812 	.uleb128	4
      0005CA 01                    2813 	.db	1
      0005CB 00 00 84 75           2814 	.dw	0,(Smain$main$203)
      0005CF 0E                    2815 	.db	14
      0005D0 05                    2816 	.uleb128	5
      0005D1 01                    2817 	.db	1
      0005D2 00 00 84 77           2818 	.dw	0,(Smain$main$204)
      0005D6 0E                    2819 	.db	14
      0005D7 06                    2820 	.uleb128	6
      0005D8 01                    2821 	.db	1
      0005D9 00 00 84 79           2822 	.dw	0,(Smain$main$205)
      0005DD 0E                    2823 	.db	14
      0005DE 07                    2824 	.uleb128	7
      0005DF 01                    2825 	.db	1
      0005E0 00 00 84 7E           2826 	.dw	0,(Smain$main$206)
      0005E4 0E                    2827 	.db	14
      0005E5 04                    2828 	.uleb128	4
      0005E6 01                    2829 	.db	1
      0005E7 00 00 84 80           2830 	.dw	0,(Smain$main$208)
      0005EB 0E                    2831 	.db	14
      0005EC 05                    2832 	.uleb128	5
      0005ED 01                    2833 	.db	1
      0005EE 00 00 84 82           2834 	.dw	0,(Smain$main$209)
      0005F2 0E                    2835 	.db	14
      0005F3 06                    2836 	.uleb128	6
      0005F4 01                    2837 	.db	1
      0005F5 00 00 84 86           2838 	.dw	0,(Smain$main$210)
      0005F9 0E                    2839 	.db	14
      0005FA 04                    2840 	.uleb128	4
      0005FB 01                    2841 	.db	1
      0005FC 00 00 84 88           2842 	.dw	0,(Smain$main$212)
      000600 0E                    2843 	.db	14
      000601 05                    2844 	.uleb128	5
      000602 01                    2845 	.db	1
      000603 00 00 84 8A           2846 	.dw	0,(Smain$main$213)
      000607 0E                    2847 	.db	14
      000608 07                    2848 	.uleb128	7
      000609 01                    2849 	.db	1
      00060A 00 00 84 8C           2850 	.dw	0,(Smain$main$214)
      00060E 0E                    2851 	.db	14
      00060F 08                    2852 	.uleb128	8
      000610 01                    2853 	.db	1
      000611 00 00 84 91           2854 	.dw	0,(Smain$main$215)
      000615 0E                    2855 	.db	14
      000616 04                    2856 	.uleb128	4
      000617 01                    2857 	.db	1
      000618 00 00 84 B9           2858 	.dw	0,(Smain$main$224)
      00061C 0E                    2859 	.db	14
      00061D 05                    2860 	.uleb128	5
      00061E 01                    2861 	.db	1
      00061F 00 00 84 BB           2862 	.dw	0,(Smain$main$225)
      000623 0E                    2863 	.db	14
      000624 06                    2864 	.uleb128	6
      000625 01                    2865 	.db	1
      000626 00 00 84 BF           2866 	.dw	0,(Smain$main$226)
      00062A 0E                    2867 	.db	14
      00062B 04                    2868 	.uleb128	4
      00062C 01                    2869 	.db	1
      00062D 00 00 84 C1           2870 	.dw	0,(Smain$main$228)
      000631 0E                    2871 	.db	14
      000632 05                    2872 	.uleb128	5
      000633 01                    2873 	.db	1
      000634 00 00 84 C3           2874 	.dw	0,(Smain$main$229)
      000638 0E                    2875 	.db	14
      000639 07                    2876 	.uleb128	7
      00063A 01                    2877 	.db	1
      00063B 00 00 84 C5           2878 	.dw	0,(Smain$main$230)
      00063F 0E                    2879 	.db	14
      000640 08                    2880 	.uleb128	8
      000641 01                    2881 	.db	1
      000642 00 00 84 CA           2882 	.dw	0,(Smain$main$231)
      000646 0E                    2883 	.db	14
      000647 04                    2884 	.uleb128	4
      000648 01                    2885 	.db	1
      000649 00 00 84 CE           2886 	.dw	0,(Smain$main$234)
      00064D 0E                    2887 	.db	14
      00064E 05                    2888 	.uleb128	5
      00064F 01                    2889 	.db	1
      000650 00 00 84 D2           2890 	.dw	0,(Smain$main$235)
      000654 0E                    2891 	.db	14
      000655 04                    2892 	.uleb128	4
      000656 01                    2893 	.db	1
      000657 00 00 84 D9           2894 	.dw	0,(Smain$main$240)
      00065B 0E                    2895 	.db	14
      00065C 02                    2896 	.uleb128	2
                                   2897 
                                   2898 	.area .debug_frame (NOLOAD)
      00065D 00 00                 2899 	.dw	0
      00065F 00 0E                 2900 	.dw	Ldebug_CIE1_end-Ldebug_CIE1_start
      000661                       2901 Ldebug_CIE1_start:
      000661 FF FF                 2902 	.dw	0xffff
      000663 FF FF                 2903 	.dw	0xffff
      000665 01                    2904 	.db	1
      000666 00                    2905 	.db	0
      000667 01                    2906 	.uleb128	1
      000668 7F                    2907 	.sleb128	-1
      000669 09                    2908 	.db	9
      00066A 0C                    2909 	.db	12
      00066B 08                    2910 	.uleb128	8
      00066C 02                    2911 	.uleb128	2
      00066D 89                    2912 	.db	137
      00066E 01                    2913 	.uleb128	1
      00066F                       2914 Ldebug_CIE1_end:
      00066F 00 00 00 AD           2915 	.dw	0,173
      000673 00 00 06 5D           2916 	.dw	0,(Ldebug_CIE1_start-4)
      000677 00 00 82 5F           2917 	.dw	0,(Smain$prepocet$1)	;initial loc
      00067B 00 00 00 57           2918 	.dw	0,Smain$prepocet$32-Smain$prepocet$1
      00067F 01                    2919 	.db	1
      000680 00 00 82 5F           2920 	.dw	0,(Smain$prepocet$1)
      000684 0E                    2921 	.db	14
      000685 02                    2922 	.uleb128	2
      000686 01                    2923 	.db	1
      000687 00 00 82 61           2924 	.dw	0,(Smain$prepocet$3)
      00068B 0E                    2925 	.db	14
      00068C 03                    2926 	.uleb128	3
      00068D 01                    2927 	.db	1
      00068E 00 00 82 63           2928 	.dw	0,(Smain$prepocet$4)
      000692 0E                    2929 	.db	14
      000693 05                    2930 	.uleb128	5
      000694 01                    2931 	.db	1
      000695 00 00 82 65           2932 	.dw	0,(Smain$prepocet$5)
      000699 0E                    2933 	.db	14
      00069A 06                    2934 	.uleb128	6
      00069B 01                    2935 	.db	1
      00069C 00 00 82 69           2936 	.dw	0,(Smain$prepocet$6)
      0006A0 0E                    2937 	.db	14
      0006A1 08                    2938 	.uleb128	8
      0006A2 01                    2939 	.db	1
      0006A3 00 00 82 6D           2940 	.dw	0,(Smain$prepocet$7)
      0006A7 0E                    2941 	.db	14
      0006A8 0A                    2942 	.uleb128	10
      0006A9 01                    2943 	.db	1
      0006AA 00 00 82 72           2944 	.dw	0,(Smain$prepocet$8)
      0006AE 0E                    2945 	.db	14
      0006AF 02                    2946 	.uleb128	2
      0006B0 01                    2947 	.db	1
      0006B1 00 00 82 86           2948 	.dw	0,(Smain$prepocet$11)
      0006B5 0E                    2949 	.db	14
      0006B6 03                    2950 	.uleb128	3
      0006B7 01                    2951 	.db	1
      0006B8 00 00 82 88           2952 	.dw	0,(Smain$prepocet$12)
      0006BC 0E                    2953 	.db	14
      0006BD 04                    2954 	.uleb128	4
      0006BE 01                    2955 	.db	1
      0006BF 00 00 82 89           2956 	.dw	0,(Smain$prepocet$13)
      0006C3 0E                    2957 	.db	14
      0006C4 06                    2958 	.uleb128	6
      0006C5 01                    2959 	.db	1
      0006C6 00 00 82 8E           2960 	.dw	0,(Smain$prepocet$14)
      0006CA 0E                    2961 	.db	14
      0006CB 02                    2962 	.uleb128	2
      0006CC 01                    2963 	.db	1
      0006CD 00 00 82 95           2964 	.dw	0,(Smain$prepocet$16)
      0006D1 0E                    2965 	.db	14
      0006D2 03                    2966 	.uleb128	3
      0006D3 01                    2967 	.db	1
      0006D4 00 00 82 97           2968 	.dw	0,(Smain$prepocet$17)
      0006D8 0E                    2969 	.db	14
      0006D9 04                    2970 	.uleb128	4
      0006DA 01                    2971 	.db	1
      0006DB 00 00 82 99           2972 	.dw	0,(Smain$prepocet$18)
      0006DF 0E                    2973 	.db	14
      0006E0 05                    2974 	.uleb128	5
      0006E1 01                    2975 	.db	1
      0006E2 00 00 82 9E           2976 	.dw	0,(Smain$prepocet$19)
      0006E6 0E                    2977 	.db	14
      0006E7 02                    2978 	.uleb128	2
      0006E8 01                    2979 	.db	1
      0006E9 00 00 82 A1           2980 	.dw	0,(Smain$prepocet$21)
      0006ED 0E                    2981 	.db	14
      0006EE 03                    2982 	.uleb128	3
      0006EF 01                    2983 	.db	1
      0006F0 00 00 82 A3           2984 	.dw	0,(Smain$prepocet$22)
      0006F4 0E                    2985 	.db	14
      0006F5 04                    2986 	.uleb128	4
      0006F6 01                    2987 	.db	1
      0006F7 00 00 82 A5           2988 	.dw	0,(Smain$prepocet$23)
      0006FB 0E                    2989 	.db	14
      0006FC 05                    2990 	.uleb128	5
      0006FD 01                    2991 	.db	1
      0006FE 00 00 82 AA           2992 	.dw	0,(Smain$prepocet$24)
      000702 0E                    2993 	.db	14
      000703 02                    2994 	.uleb128	2
      000704 01                    2995 	.db	1
      000705 00 00 82 AC           2996 	.dw	0,(Smain$prepocet$26)
      000709 0E                    2997 	.db	14
      00070A 03                    2998 	.uleb128	3
      00070B 01                    2999 	.db	1
      00070C 00 00 82 AE           3000 	.dw	0,(Smain$prepocet$27)
      000710 0E                    3001 	.db	14
      000711 04                    3002 	.uleb128	4
      000712 01                    3003 	.db	1
      000713 00 00 82 B0           3004 	.dw	0,(Smain$prepocet$28)
      000717 0E                    3005 	.db	14
      000718 05                    3006 	.uleb128	5
      000719 01                    3007 	.db	1
      00071A 00 00 82 B5           3008 	.dw	0,(Smain$prepocet$29)
      00071E 0E                    3009 	.db	14
      00071F 02                    3010 	.uleb128	2
