#ifndef INC_CUSTOM_H
#define INC_CUSTOM_H

#include "stm8s.h"


void pc_init(void);
void pc_write_char(char c);
void pc_write_string(char str[]);
char pc_read_char(void);



#endif