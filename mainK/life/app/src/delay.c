#include "stm8s.h"
#include "delay.h"

void delay_05s_init(void)
    { //inicializace časovače tim4 tak aby přetekl jednou za 1s
    TIM3_TimeBaseInit(TIM3_PRESCALER_256,31499);
    TIM3_Cmd(ENABLE);
    
}
void delay_05s(uint32_t time_s){//kolikrát přeteče
    TIM3_SetCounter(0);
    for(uint32_t i=0;i<time_s;i++){
        while (TIM3_GetFlagStatus(TIM3_FLAG_UPDATE) != SET)
                {;}
        TIM3_ClearFlag (TIM3_FLAG_UPDATE);
    }
}