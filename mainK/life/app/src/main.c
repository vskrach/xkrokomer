#include "stm8s.h"
#include "custom.h"
#include "delay.h"
#include "jas.h"

uint32_t js = 250;            //inicializace globálních proměnných
uint32_t kroky = 0;
uint8_t a;
uint8_t b;




void prepocet(void){
    
    a = kroky % 10;
    b = kroky - a;
    b = b/10;
    GPIO_Write(GPIOB,a);
    GPIO_Write(GPIOG,b);
    GPIO_WriteLow(GPIOG, GPIO_PIN_3);
}

/**
  * PORT B 0-3 -> First Decoder -> First Segment
  * PORT G 0-3 -> Second Decoder -> Second Segment
  * PIN D4 -> Common Cathode
  * PIN E4 -> Input
  * PIN E4 -> SENSOR -> GND
  *
  */



void main(void)
{

    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // FREQ MCU 16MHz
    
    GPIO_Init(GPIOG, GPIO_PIN_0, GPIO_MODE_OUT_PP_LOW_SLOW);    //inicializace GPIOA = 1. SEGMENT
    GPIO_Init(GPIOG, GPIO_PIN_1, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOG, GPIO_PIN_2, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOG, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);
    
    GPIO_Init(GPIOB, GPIO_PIN_0, GPIO_MODE_OUT_PP_LOW_SLOW);     //inicializace GPIOB = 2. SEGMENT
    GPIO_Init(GPIOB, GPIO_PIN_1, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOB, GPIO_PIN_2, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOB, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);
    
    GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_5);

    GPIO_Init(GPIOE, GPIO_PIN_4, GPIO_MODE_IN_FL_IT);        //inicializace Senzoru



    jas_init();
    pc_init();
    delay_05s_init();
    char key;
    uint8_t alpha;
    uint8_t beta = 0;
    
    pc_write_string("\n6 >>> +krok/n4 >>> -krok");
    while (1)
    {
        
        
        if(UART1_GetFlagStatus(UART1_FLAG_RXNE))
        {   
            key = UART1_ReceiveData8();
            if(key == ('5'))
            {
                kroky = 99;
                delay_05s(1);
                kroky = 0;
                delay_05s(1);
                kroky = 99;
                delay_05s(1);
                kroky = 0;
                pc_write_string("Vynulování");
            }
            else if(key == ('6'))
            {
                kroky = kroky + 1;
                pc_write_string("+krok");
            }
            else if(key == ('4'))
            {
                kroky = kroky - 1;
                pc_write_string("-krok");
            }
            else if(key == ('2'))
            {
                js = js - 10;
                pc_write_string("-jas");
            }
            else if(key == ('8'))
            {
                js = js + 10;
                pc_write_string("+jas");
            }
            else
            {
                pc_write_string("UNKNOWN");
            } 
        }
        alpha = GPIO_ReadInputPin(GPIOE, GPIO_PIN_4);
        if(alpha==0)
        {   
            if(beta == 1)
            {
                
                beta = 0;
                GPIO_WriteReverse(GPIOC,GPIO_PIN_5);
                pc_write_string("+krok");
                delay_05s(1);
            }
                
        }
        if(alpha>1)
        {   
            if(beta == 0)
            {
                beta = 1;
                kroky = kroky + 1;
                pc_write_string("beta");
                delay_05s(1);
            }
                
        }
            
        jas(js);
        prepocet();
    }
}
//I AM ALIVE