#include "stm8s.h"
#include "custom.h"

void pc_init(void)
{
    UART1_Init(9600,UART1_WORDLENGTH_8D,UART1_STOPBITS_1,UART1_PARITY_NO,UART1_SYNCMODE_CLOCK_DISABLE,UART1_MODE_TXRX_ENABLE);
}

void pc_write_char(char c)
{
    while (!(UART1_GetFlagStatus(UART1_FLAG_TXE))){;}
    UART1_SendData8(c);
}
void pc_write_string(char str[])
{
    for(uint32_t i=0;str[i];i++)´+
    {pc_write_char(str[i]);}
}
char pc_read_char(void)
{   
    while (!(UART1_GetFlagStatus(UART1_FLAG_RXNE))){;}
    return UART1_ReceiveData8();
}