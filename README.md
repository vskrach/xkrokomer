# xKrokomer

## Co obsahuje?
- Input řešený otřesovým čidlem SW-520D
- Na výstupu jsou dékodéry, které pak ovládají 2 sedmi-segmenty
- UART pro úpravu nebo resertování počtů kroků i změnu jasu
- PWM signál pro úpravu jasu segmentů


## Kód

Pin D4 slouží pro regulaci jasu segmentů
Pin E4 slouží jako vstup senzoru otřesu
Piny B0, B1, B2, B3 slouží jako vstupy do dékodéru pro jednotky kroků
Piny G0, G1, G2, G3 slouží jako vstupy do dékodéru pro jednotky kroků
## Vývojový diagram
```mermaid
flowchart TD;
  A[START]-->B[INICIALIZACE]-->D
  D{Změna na vstupu?}--Ano-->E[kroky+]-->F[Přepočítání hodnot]-->X
  D{Změna na vstupu?}--Ne-->X{Byl přijat znak v UART?}--ANO-->I[Porovnání znaku]-->J[Vykonání příkazu]-->Y[Zapsání do výstupů]-->D
  X--NE-->D
```
## Kicad schemáta

Schéma zapojení 
![schéma](schema.png)
Návrh DPS
![dps](dps.png)
3D Návrh DPS
![3d](dps3d.png)
## Blokové schéma

```mermaid
graph TD;
  STM8 -->B0-B3-->1.Dekodér-->1.Sedmisegment;
  STM8 -->G0-G3-->2.Dekodér-->2.Sedmisegment;
  2.Sedmisegment-->D4 
  1.Sedmisegment-->D4--> PWM 
  STM8 --> PWM
  Sensor-->E4-->STM8;
  PC-->Program-->STM8
  PC --> UART --> STM8
  Ucc-->STM8-->GND
  RESET-->STM8
```

## List of components

|   Typ součástky |     Hodnota   |      Počet kusů      |  Cena  | Cena kusů |
|:---------------:|:-------------:|:--------------------:|:------:|:---------:|
|     SW-520D     |               | 1                    | 5 Kč   | 5 Kč      |
|   Rezistor      |    270R       |   14                 |  2 Kč  | 28 Kč     |
| Dekodér  E147D  |               |    2                 | 12 Kč  | 24 Kč     |
| Sedmi segment   |               |    2                 | 12 Kč  | 24 Kč     |
|                 |               |                      |Celkem        |  106 Kč         |


---
# Creators

Vojtěch Škrach - Maintainer


